<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.5.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="113" name="ReferenceLS" color="7" fill="1" visible="no" active="no"/>
<layer number="114" name="FRNTMAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="115" name="FRNTMAAT2" color="7" fill="1" visible="no" active="no"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="no" active="no"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="no" active="no"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="no" active="no"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="no" active="no"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
</layers>
<schematic xreflabel="%F%N/%S" xrefpart="/%S.%C%R">
<libraries>
<library name="VEIT_logotyp">
<packages>
</packages>
<symbols>
<symbol name="DINA3_L">
<wire x1="0" y1="0" x2="388.62" y2="0" width="0.4064" layer="94"/>
<wire x1="0" y1="264.16" x2="0" y2="0" width="0.4064" layer="94"/>
<wire x1="388.62" y1="264.16" x2="0" y2="264.16" width="0.4064" layer="94"/>
<wire x1="388.62" y1="264.16" x2="388.62" y2="0" width="0.4064" layer="94"/>
</symbol>
<symbol name="DOCFIELD-AUTA">
<wire x1="0" y1="0" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="101.6" y1="10.16" x2="87.63" y2="10.16" width="0.1016" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="71.12" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="10.16" width="0.1016" layer="94"/>
<wire x1="101.6" y1="10.16" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="0" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="87.63" y1="10.16" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="87.63" y1="10.16" x2="0" y2="10.16" width="0.1016" layer="94"/>
<wire x1="87.63" y1="5.08" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="101.6" y1="5.08" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="0" y1="10.16" x2="0" y2="22.86" width="0.1016" layer="94"/>
<wire x1="101.6" y1="38.1" x2="64.77" y2="38.1" width="0.1016" layer="94"/>
<wire x1="64.77" y1="38.1" x2="0" y2="38.1" width="0.1016" layer="94"/>
<wire x1="101.6" y1="38.1" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="64.77" y2="22.86" width="0.1016" layer="94"/>
<wire x1="64.77" y1="22.86" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="0" y2="38.1" width="0.1016" layer="94"/>
<wire x1="101.6" y1="22.86" x2="101.6" y2="10.16" width="0.1016" layer="94"/>
<text x="1.27" y="1.27" size="2.54" layer="94">Date:</text>
<text x="15.24" y="1.27" size="2.54" layer="94" font="vector">&gt;LAST_DATE_TIME</text>
<text x="72.39" y="1.27" size="2.54" layer="94">Sheet:</text>
<text x="86.36" y="1.27" size="2.54" layer="94">&gt;SHEET</text>
<text x="88.9" y="6.35" size="2.54" layer="94">REV:</text>
<text x="1.27" y="6.35" size="2.54" layer="94">TITLE:</text>
<text x="1.27" y="30.48" size="1.9304" layer="94" align="top-left">Vehicle Number:</text>
<text x="15.24" y="6.35" size="2.54" layer="94" font="vector">&gt;DRAWING_NAME</text>
<wire x1="64.77" y1="38.1" x2="64.77" y2="22.86" width="0.1016" layer="94"/>
<wire x1="97.487340625" y1="34.81106875" x2="90.66211875" y2="34.81106875" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="34.81106875" x2="90.66211875" y2="34.739" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="34.739" x2="90.66211875" y2="34.514" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="34.514" x2="90.66211875" y2="34.289" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="34.289" x2="90.66211875" y2="34.064" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="34.064" x2="90.66211875" y2="33.839" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="33.839" x2="90.66211875" y2="33.614" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="33.614" x2="90.66211875" y2="33.389" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="33.389" x2="90.66211875" y2="33.23621875" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="33.23621875" x2="93.287040625" y2="33.23621875" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="33.23621875" x2="93.287040625" y2="33.164" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="33.164" x2="93.287040625" y2="32.939" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="32.939" x2="93.287040625" y2="32.714" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="32.714" x2="93.287040625" y2="32.489" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="32.489" x2="93.287040625" y2="32.264" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="32.264" x2="93.287040625" y2="32.039" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="32.039" x2="93.287040625" y2="31.814" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="31.814" x2="93.287040625" y2="31.589" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="31.589" x2="93.287040625" y2="31.364" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="31.364" x2="93.287040625" y2="31.139" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="31.139" x2="93.287040625" y2="30.914" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="30.914" x2="93.287040625" y2="30.689" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="30.689" x2="93.287040625" y2="30.464" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="30.464" x2="93.287040625" y2="30.239" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="30.239" x2="93.287040625" y2="30.014" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="30.014" x2="93.287040625" y2="29.789" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="29.789" x2="93.287040625" y2="29.564" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="29.564" x2="93.287040625" y2="29.45076875" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="29.45076875" x2="94.862340625" y2="29.45076875" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="29.45076875" x2="94.862340625" y2="29.564" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="29.564" x2="94.862340625" y2="29.789" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="29.789" x2="94.862340625" y2="30.014" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="30.014" x2="94.862340625" y2="30.239" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="30.239" x2="94.862340625" y2="30.464" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="30.464" x2="94.862340625" y2="30.689" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="30.689" x2="94.862340625" y2="30.914" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="30.914" x2="94.862340625" y2="31.139" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="31.139" x2="94.862340625" y2="31.364" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="31.364" x2="94.862340625" y2="31.589" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="31.589" x2="94.862340625" y2="31.814" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="31.814" x2="94.862340625" y2="32.039" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="32.039" x2="94.862340625" y2="32.264" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="32.264" x2="94.862340625" y2="32.489" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="32.489" x2="94.862340625" y2="32.714" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="32.714" x2="94.862340625" y2="32.939" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="32.939" x2="94.862340625" y2="33.164" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="33.164" x2="94.862340625" y2="33.23621875" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="33.23621875" x2="97.487340625" y2="33.23621875" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="33.23621875" x2="97.487340625" y2="33.389" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="33.389" x2="97.487340625" y2="33.614" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="33.614" x2="97.487340625" y2="33.839" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="33.839" x2="97.487340625" y2="34.064" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="34.064" x2="97.487340625" y2="34.289" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="34.289" x2="97.487340625" y2="34.514" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="34.514" x2="97.487340625" y2="34.739" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="34.739" x2="97.487340625" y2="34.81106875" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="29.497059375" x2="87.068640625" y2="29.564" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="29.564" x2="87.068640625" y2="29.789" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="29.789" x2="87.068640625" y2="30.014" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="30.014" x2="87.068640625" y2="30.239" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="30.239" x2="87.068640625" y2="30.464" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="30.464" x2="87.068640625" y2="30.689" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="30.689" x2="87.068640625" y2="30.914" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="30.914" x2="87.068640625" y2="31.139" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="31.139" x2="87.068640625" y2="31.364" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="31.364" x2="87.068640625" y2="31.589" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="31.589" x2="87.068640625" y2="31.814" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="31.814" x2="87.068640625" y2="32.039" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="32.039" x2="87.068640625" y2="32.264" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="32.264" x2="87.068640625" y2="32.489" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="32.489" x2="87.068640625" y2="32.714" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="32.714" x2="87.068640625" y2="32.939" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="32.939" x2="87.068640625" y2="33.164" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="33.164" x2="87.068640625" y2="33.389" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="33.389" x2="87.068640625" y2="33.614" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="33.614" x2="87.068640625" y2="33.839" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="33.839" x2="87.068640625" y2="34.064" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="34.064" x2="87.068640625" y2="34.289" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="34.289" x2="87.068640625" y2="34.514" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="34.514" x2="87.068640625" y2="34.739" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="34.739" x2="87.068640625" y2="34.813409375" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="34.813409375" x2="88.64378125" y2="34.813409375" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="34.813409375" x2="88.64378125" y2="34.739" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="34.739" x2="88.64378125" y2="34.514" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="34.514" x2="88.64378125" y2="34.289" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="34.289" x2="88.64378125" y2="34.064" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="34.064" x2="88.64378125" y2="33.839" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="33.839" x2="88.64378125" y2="33.614" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="33.614" x2="88.64378125" y2="33.389" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="33.389" x2="88.64378125" y2="33.164" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="33.164" x2="88.64378125" y2="32.939" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="32.939" x2="88.64378125" y2="32.714" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="32.714" x2="88.64378125" y2="32.489" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="32.489" x2="88.64378125" y2="32.264" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="32.264" x2="88.64378125" y2="32.039" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="32.039" x2="88.64378125" y2="31.814" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="31.814" x2="88.64378125" y2="31.589" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="31.589" x2="88.64378125" y2="31.364" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="31.364" x2="88.64378125" y2="31.139" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="31.139" x2="88.64378125" y2="30.914" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="30.914" x2="88.64378125" y2="30.689" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="30.689" x2="88.64378125" y2="30.464" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="30.464" x2="88.64378125" y2="30.239" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="30.239" x2="88.64378125" y2="30.014" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="30.014" x2="88.64378125" y2="29.789" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="29.789" x2="88.64378125" y2="29.564" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="29.564" x2="88.64378125" y2="29.497059375" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="29.497059375" x2="87.068640625" y2="29.497059375" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="29.49851875" x2="78.286090625" y2="29.564" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="29.564" x2="78.286090625" y2="29.789" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="29.789" x2="78.286090625" y2="30.014" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="30.014" x2="78.286090625" y2="30.239" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="30.239" x2="78.286090625" y2="30.464" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="30.464" x2="78.286090625" y2="30.689" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="30.689" x2="78.286090625" y2="30.914" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="30.914" x2="78.286090625" y2="31.07366875" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="31.07366875" x2="84.81116875" y2="31.07366875" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="31.07366875" x2="84.81116875" y2="30.914" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="30.914" x2="84.81116875" y2="30.689" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="30.689" x2="84.81116875" y2="30.464" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="30.464" x2="84.81116875" y2="30.239" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="30.239" x2="84.81116875" y2="30.014" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="30.014" x2="84.81116875" y2="29.789" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="29.789" x2="84.81116875" y2="29.564" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="29.564" x2="84.81116875" y2="29.49851875" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="29.49851875" x2="78.286090625" y2="29.49851875" width="0.225" layer="94"/>
<wire x1="78.286240625" y1="31.44236875" x2="78.286240625" y2="31.589" width="0.225" layer="94"/>
<wire x1="78.286240625" y1="31.589" x2="78.286240625" y2="31.814" width="0.225" layer="94"/>
<wire x1="78.286240625" y1="31.814" x2="78.286240625" y2="32.039" width="0.225" layer="94"/>
<wire x1="78.286240625" y1="32.039" x2="78.286240625" y2="32.264" width="0.225" layer="94"/>
<wire x1="78.286240625" y1="32.264" x2="78.286240625" y2="32.489" width="0.225" layer="94"/>
<wire x1="78.286240625" y1="32.489" x2="78.286240625" y2="32.714" width="0.225" layer="94"/>
<wire x1="78.286240625" y1="32.714" x2="78.286240625" y2="32.86721875" width="0.225" layer="94"/>
<wire x1="78.286240625" y1="32.86721875" x2="81.96101875" y2="32.86721875" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="32.86721875" x2="81.96101875" y2="32.714" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="32.714" x2="81.96101875" y2="32.489" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="32.489" x2="81.96101875" y2="32.264" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="32.264" x2="81.96101875" y2="32.039" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="32.039" x2="81.96101875" y2="31.814" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="31.814" x2="81.96101875" y2="31.589" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="31.589" x2="81.96101875" y2="31.44236875" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="31.44236875" x2="78.286240625" y2="31.44236875" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="33.23621875" x2="78.286090625" y2="33.389" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="33.389" x2="78.286090625" y2="33.614" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="33.614" x2="78.286090625" y2="33.839" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="33.839" x2="78.286090625" y2="34.064" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="34.064" x2="78.286090625" y2="34.289" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="34.289" x2="78.286090625" y2="34.514" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="34.514" x2="78.286090625" y2="34.739" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="34.739" x2="78.286090625" y2="34.81106875" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="34.81106875" x2="84.81101875" y2="34.81106875" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="34.81106875" x2="84.81101875" y2="34.739" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="34.739" x2="84.81101875" y2="34.514" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="34.514" x2="84.81101875" y2="34.289" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="34.289" x2="84.81101875" y2="34.064" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="34.064" x2="84.81101875" y2="33.839" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="33.839" x2="84.81101875" y2="33.614" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="33.614" x2="84.81101875" y2="33.389" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="33.389" x2="84.81101875" y2="33.23621875" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="33.23621875" x2="78.286090625" y2="33.23621875" width="0.225" layer="94"/>
<wire x1="68.6921" y1="33.944190625" x2="69.073209375" y2="34.289" width="0.225" layer="94"/>
<wire x1="69.073209375" y1="34.289" x2="69.3219" y2="34.514" width="0.225" layer="94"/>
<wire x1="69.3219" y1="34.514" x2="69.570590625" y2="34.739" width="0.225" layer="94"/>
<wire x1="69.570590625" y1="34.739" x2="69.86021875" y2="35.001040625" width="0.225" layer="94"/>
<wire x1="85.115190625" y1="26.34016875" x2="85.115190625" y2="26.189" width="0.225" layer="94"/>
<wire x1="85.115190625" y1="26.189" x2="85.115190625" y2="25.97718125" width="0.225" layer="94"/>
<wire x1="84.32008125" y1="25.980109375" x2="84.341215625" y2="25.70643125" width="0.225" layer="94" curve="8.832242"/>
<wire x1="84.341209375" y1="25.70643125" x2="84.4424375" y2="25.45343125" width="0.225" layer="94" curve="25.94924"/>
<wire x1="84.442440625" y1="25.45343125" x2="84.662590625" y2="25.2962625" width="0.225" layer="94" curve="39.389578"/>
<wire x1="84.662590625" y1="25.296259375" x2="84.93363125" y2="25.258525" width="0.225" layer="94" curve="15.805998"/>
<wire x1="85.52626875" y1="25.706240625" x2="85.547246875" y2="25.980109375" width="0.225" layer="94" curve="8.760059"/>
<wire x1="85.42531875" y1="25.45295" x2="85.46248125" y2="25.514" width="0.225" layer="94" curve="6.74589"/>
<wire x1="85.46248125" y1="25.514" x2="85.526271875" y2="25.706240625" width="0.225" layer="94" curve="19.196299"/>
<wire x1="85.204909375" y1="25.295909375" x2="85.425315625" y2="25.452953125" width="0.225" layer="94" curve="39.656265"/>
<wire x1="84.93363125" y1="25.25853125" x2="85.204909375" y2="25.295909375" width="0.225" layer="94" curve="15.593829"/>
<wire x1="84.32008125" y1="25.980109375" x2="84.32008125" y2="26.189" width="0.225" layer="94"/>
<wire x1="84.32008125" y1="26.189" x2="84.32008125" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="84.341340625" y1="26.61343125" x2="84.32161875" y2="26.414" width="0.225" layer="94" curve="6.503067"/>
<wire x1="84.32161875" y1="26.414" x2="84.320078125" y2="26.34016875" width="0.225" layer="94" curve="2.395218"/>
<wire x1="84.44311875" y1="26.86573125" x2="84.341340625" y2="26.61343125" width="0.225" layer="94" curve="26.142719"/>
<wire x1="84.663740625" y1="27.02155" x2="84.443121875" y2="26.865728125" width="0.225" layer="94" curve="39.450584"/>
<wire x1="84.93363125" y1="27.05881875" x2="84.663740625" y2="27.02155" width="0.225" layer="94" curve="15.292594"/>
<wire x1="85.54725" y1="25.980109375" x2="85.54725" y2="26.189" width="0.225" layer="94"/>
<wire x1="85.54725" y1="26.189" x2="85.54725" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="85.54725" y1="26.34016875" x2="85.52646875" y2="26.61348125" width="0.225" layer="94" curve="8.69613"/>
<wire x1="85.52646875" y1="26.61348125" x2="85.4257" y2="26.8662" width="0.225" layer="94" curve="26.08534"/>
<wire x1="85.4257" y1="26.8662" x2="85.2053" y2="27.02220625" width="0.225" layer="94" curve="39.852867"/>
<wire x1="85.2053" y1="27.022209375" x2="84.93363125" y2="27.058825" width="0.225" layer="94" curve="15.379893"/>
<wire x1="84.752059375" y1="26.34016875" x2="84.752059375" y2="26.189" width="0.225" layer="94"/>
<wire x1="84.752059375" y1="26.189" x2="84.752059375" y2="25.97718125" width="0.225" layer="94"/>
<wire x1="85.115190625" y1="25.97718125" x2="85.11286875" y2="25.87078125" width="0.225" layer="94"/>
<wire x1="85.11286875" y1="25.87078125" x2="85.10523125" y2="25.791359375" width="0.225" layer="94"/>
<wire x1="85.10523125" y1="25.791359375" x2="85.100290625" y2="25.765209375" width="0.225" layer="94"/>
<wire x1="85.100290625" y1="25.765209375" x2="85.09306875" y2="25.73961875" width="0.225" layer="94"/>
<wire x1="85.09306875" y1="25.73961875" x2="85.081940625" y2="25.715509375" width="0.225" layer="94"/>
<wire x1="85.081940625" y1="25.715509375" x2="85.06321875" y2="25.696990625" width="0.225" layer="94"/>
<wire x1="85.06321875" y1="25.696990625" x2="85.0387" y2="25.68683125" width="0.225" layer="94"/>
<wire x1="85.0387" y1="25.68683125" x2="85.012740625" y2="25.68106875" width="0.225" layer="94"/>
<wire x1="85.012740625" y1="25.68106875" x2="84.986340625" y2="25.6778" width="0.225" layer="94"/>
<wire x1="84.986340625" y1="25.6778" x2="84.95978125" y2="25.67618125" width="0.225" layer="94"/>
<wire x1="84.752059375" y1="26.34016875" x2="84.75605" y2="26.47246875" width="0.225" layer="94"/>
<wire x1="84.75605" y1="26.47246875" x2="84.76195" y2="26.52508125" width="0.225" layer="94"/>
<wire x1="84.76195" y1="26.52508125" x2="84.77411875" y2="26.57655" width="0.225" layer="94"/>
<wire x1="84.77411875" y1="26.57655" x2="84.78536875" y2="26.600440625" width="0.225" layer="94"/>
<wire x1="84.78536875" y1="26.600440625" x2="84.804459375" y2="26.618359375" width="0.225" layer="94"/>
<wire x1="84.804459375" y1="26.618359375" x2="84.82903125" y2="26.62806875" width="0.225" layer="94"/>
<wire x1="84.82903125" y1="26.62806875" x2="84.854909375" y2="26.63358125" width="0.225" layer="94"/>
<wire x1="84.854909375" y1="26.63358125" x2="84.8812" y2="26.6367" width="0.225" layer="94"/>
<wire x1="84.8812" y1="26.6367" x2="84.90763125" y2="26.63826875" width="0.225" layer="94"/>
<wire x1="84.90763125" y1="26.63826875" x2="84.96056875" y2="26.63825" width="0.225" layer="94"/>
<wire x1="84.96056875" y1="26.63825" x2="84.987" y2="26.63666875" width="0.225" layer="94"/>
<wire x1="84.987" y1="26.63666875" x2="85.013290625" y2="26.63358125" width="0.225" layer="94"/>
<wire x1="85.013290625" y1="26.63358125" x2="85.039190625" y2="26.628140625" width="0.225" layer="94"/>
<wire x1="85.039190625" y1="26.628140625" x2="85.063809375" y2="26.618559375" width="0.225" layer="94"/>
<wire x1="85.063809375" y1="26.618559375" x2="85.0829" y2="26.60066875" width="0.225" layer="94"/>
<wire x1="85.0829" y1="26.60066875" x2="85.093940625" y2="26.57666875" width="0.225" layer="94"/>
<wire x1="85.093940625" y1="26.57666875" x2="85.105759375" y2="26.52511875" width="0.225" layer="94"/>
<wire x1="85.105759375" y1="26.52511875" x2="85.111390625" y2="26.47248125" width="0.225" layer="94"/>
<wire x1="85.111390625" y1="26.47248125" x2="85.11306875" y2="26.414" width="0.225" layer="94"/>
<wire x1="85.11306875" y1="26.414" x2="85.115190625" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="84.752059375" y1="25.97718125" x2="84.754290625" y2="25.87078125" width="0.225" layer="94"/>
<wire x1="84.754290625" y1="25.87078125" x2="84.76156875" y2="25.79131875" width="0.225" layer="94"/>
<wire x1="84.76156875" y1="25.79131875" x2="84.7664" y2="25.76515" width="0.225" layer="94"/>
<wire x1="84.7664" y1="25.76515" x2="84.773409375" y2="25.7395" width="0.225" layer="94"/>
<wire x1="84.773409375" y1="25.7395" x2="84.784340625" y2="25.7153" width="0.225" layer="94"/>
<wire x1="84.784340625" y1="25.7153" x2="84.803059375" y2="25.6968" width="0.225" layer="94"/>
<wire x1="84.803059375" y1="25.6968" x2="84.82763125" y2="25.686759375" width="0.225" layer="94"/>
<wire x1="84.82763125" y1="25.686759375" x2="84.853609375" y2="25.681059375" width="0.225" layer="94"/>
<wire x1="84.853609375" y1="25.681059375" x2="84.90656875" y2="25.6762" width="0.225" layer="94"/>
<wire x1="84.90656875" y1="25.6762" x2="84.95978125" y2="25.6762" width="0.225" layer="94"/>
<wire x1="88.41505" y1="27.03186875" x2="88.835090625" y2="27.03186875" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="27.03186875" x2="88.835090625" y2="26.864" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="26.864" x2="88.835090625" y2="26.639" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="26.639" x2="88.835090625" y2="26.414" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="26.414" x2="88.835090625" y2="26.189" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="26.189" x2="88.835090625" y2="25.964" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="25.964" x2="88.835090625" y2="25.739" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="25.739" x2="88.835090625" y2="25.514" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="25.514" x2="88.835090625" y2="25.289" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="25.289" x2="88.835090625" y2="25.28548125" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="25.28548125" x2="88.41505" y2="25.28548125" width="0.225" layer="94"/>
<wire x1="88.41505" y1="25.28548125" x2="88.41505" y2="25.289" width="0.225" layer="94"/>
<wire x1="88.41505" y1="25.289" x2="88.41505" y2="25.514" width="0.225" layer="94"/>
<wire x1="88.41505" y1="25.514" x2="88.41505" y2="25.739" width="0.225" layer="94"/>
<wire x1="88.41505" y1="25.739" x2="88.41505" y2="25.964" width="0.225" layer="94"/>
<wire x1="88.41505" y1="25.964" x2="88.41505" y2="26.189" width="0.225" layer="94"/>
<wire x1="88.41505" y1="26.189" x2="88.41505" y2="26.414" width="0.225" layer="94"/>
<wire x1="88.41505" y1="26.414" x2="88.41505" y2="26.639" width="0.225" layer="94"/>
<wire x1="88.41505" y1="26.639" x2="88.41505" y2="26.864" width="0.225" layer="94"/>
<wire x1="88.41505" y1="26.864" x2="88.41505" y2="27.03186875" width="0.225" layer="94"/>
<wire x1="88.41505" y1="27.205890625" x2="88.835090625" y2="27.205890625" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="27.205890625" x2="88.835090625" y2="27.314" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="27.314" x2="88.835090625" y2="27.539" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="27.539" x2="88.835090625" y2="27.68606875" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="27.68606875" x2="88.41505" y2="27.68606875" width="0.225" layer="94"/>
<wire x1="88.41505" y1="27.68606875" x2="88.41505" y2="27.539" width="0.225" layer="94"/>
<wire x1="88.41505" y1="27.539" x2="88.41505" y2="27.314" width="0.225" layer="94"/>
<wire x1="88.41505" y1="27.314" x2="88.41505" y2="27.205890625" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="27.725040625" x2="75.911209375" y2="27.725040625" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="27.725040625" x2="75.911209375" y2="27.539" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="27.539" x2="75.911209375" y2="27.314" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="27.314" x2="75.911209375" y2="27.089" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="27.089" x2="75.911209375" y2="26.864" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="26.864" x2="75.911209375" y2="26.639" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="26.639" x2="75.911209375" y2="26.414" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="26.414" x2="75.911209375" y2="26.189" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="26.189" x2="75.911209375" y2="25.964" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="25.964" x2="75.911209375" y2="25.739" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="25.739" x2="75.911209375" y2="25.514" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="25.514" x2="75.911209375" y2="25.289" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="25.289" x2="75.911209375" y2="25.28548125" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="25.28548125" x2="75.49116875" y2="25.28548125" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="25.28548125" x2="75.49116875" y2="25.289" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="25.289" x2="75.49116875" y2="25.514" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="25.514" x2="75.49116875" y2="25.739" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="25.739" x2="75.49116875" y2="25.964" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="25.964" x2="75.49116875" y2="26.189" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="26.189" x2="75.49116875" y2="26.414" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="26.414" x2="75.49116875" y2="26.639" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="26.639" x2="75.49116875" y2="26.864" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="26.864" x2="75.49116875" y2="27.089" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="27.089" x2="75.49116875" y2="27.314" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="27.314" x2="75.49116875" y2="27.539" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="27.539" x2="75.49116875" y2="27.725040625" width="0.225" layer="94"/>
<wire x1="90.092" y1="25.97718125" x2="90.092" y2="26.189" width="0.225" layer="94"/>
<wire x1="90.092" y1="26.189" x2="90.092" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="90.41016875" y1="26.44828125" x2="90.41016875" y2="26.414" width="0.225" layer="94"/>
<wire x1="90.41016875" y1="26.414" x2="90.41016875" y2="26.30881875" width="0.225" layer="94"/>
<wire x1="90.41016875" y1="26.30881875" x2="90.830209375" y2="26.30881875" width="0.225" layer="94"/>
<wire x1="90.43711875" y1="26.071809375" x2="90.43711875" y2="25.964" width="0.225" layer="94"/>
<wire x1="90.43711875" y1="25.964" x2="90.43711875" y2="25.89925" width="0.225" layer="94"/>
<wire x1="90.43711875" y1="26.071809375" x2="90.86316875" y2="26.071809375" width="0.225" layer="94"/>
<wire x1="90.092" y1="26.34016875" x2="90.09408125" y2="26.45038125" width="0.225" layer="94"/>
<wire x1="90.09408125" y1="26.45038125" x2="90.09926875" y2="26.5163" width="0.225" layer="94"/>
<wire x1="90.09926875" y1="26.5163" x2="90.106540625" y2="26.55976875" width="0.225" layer="94"/>
<wire x1="90.106540625" y1="26.55976875" x2="90.112390625" y2="26.58101875" width="0.225" layer="94"/>
<wire x1="90.112390625" y1="26.58101875" x2="90.12096875" y2="26.6013" width="0.225" layer="94"/>
<wire x1="90.12096875" y1="26.6013" x2="90.135190625" y2="26.617890625" width="0.225" layer="94"/>
<wire x1="90.135190625" y1="26.617890625" x2="90.1551" y2="26.6272" width="0.225" layer="94"/>
<wire x1="90.1551" y1="26.6272" x2="90.17645" y2="26.63263125" width="0.225" layer="94"/>
<wire x1="90.17645" y1="26.63263125" x2="90.19825" y2="26.6359" width="0.225" layer="94"/>
<wire x1="90.19825" y1="26.6359" x2="90.220209375" y2="26.637740625" width="0.225" layer="94"/>
<wire x1="90.220209375" y1="26.637740625" x2="90.242240625" y2="26.63858125" width="0.225" layer="94"/>
<wire x1="90.242240625" y1="26.63858125" x2="90.264290625" y2="26.638640625" width="0.225" layer="94"/>
<wire x1="90.264290625" y1="26.638640625" x2="90.28631875" y2="26.6378" width="0.225" layer="94"/>
<wire x1="90.28631875" y1="26.6378" x2="90.30826875" y2="26.63575" width="0.225" layer="94"/>
<wire x1="90.30826875" y1="26.63575" x2="90.33" y2="26.63208125" width="0.225" layer="94"/>
<wire x1="90.33" y1="26.63208125" x2="90.351140625" y2="26.625909375" width="0.225" layer="94"/>
<wire x1="90.351140625" y1="26.625909375" x2="90.3705" y2="26.615509375" width="0.225" layer="94"/>
<wire x1="90.3705" y1="26.615509375" x2="90.38486875" y2="26.598990625" width="0.225" layer="94"/>
<wire x1="90.38486875" y1="26.598990625" x2="90.40023125" y2="26.557840625" width="0.225" layer="94"/>
<wire x1="90.40023125" y1="26.557840625" x2="90.407140625" y2="26.51431875" width="0.225" layer="94"/>
<wire x1="90.407140625" y1="26.51431875" x2="90.41016875" y2="26.44828125" width="0.225" layer="94"/>
<wire x1="90.092" y1="25.97718125" x2="90.09478125" y2="25.85786875" width="0.225" layer="94"/>
<wire x1="90.09478125" y1="25.85786875" x2="90.09865" y2="25.810290625" width="0.225" layer="94"/>
<wire x1="90.09865" y1="25.810290625" x2="90.11223125" y2="25.74008125" width="0.225" layer="94"/>
<wire x1="90.11223125" y1="25.74008125" x2="90.121240625" y2="25.718009375" width="0.225" layer="94"/>
<wire x1="90.121240625" y1="25.718009375" x2="90.136040625" y2="25.6995" width="0.225" layer="94"/>
<wire x1="90.136040625" y1="25.6995" x2="90.15721875" y2="25.688690625" width="0.225" layer="94"/>
<wire x1="90.15721875" y1="25.688690625" x2="90.18025" y2="25.68248125" width="0.225" layer="94"/>
<wire x1="90.18025" y1="25.68248125" x2="90.20383125" y2="25.678809375" width="0.225" layer="94"/>
<wire x1="90.20383125" y1="25.678809375" x2="90.251459375" y2="25.675840625" width="0.225" layer="94"/>
<wire x1="90.251459375" y1="25.675840625" x2="90.299190625" y2="25.67656875" width="0.225" layer="94"/>
<wire x1="90.299190625" y1="25.67656875" x2="90.34663125" y2="25.681690625" width="0.225" layer="94"/>
<wire x1="90.34663125" y1="25.681690625" x2="90.391890625" y2="25.696190625" width="0.225" layer="94"/>
<wire x1="90.391890625" y1="25.696190625" x2="90.409190625" y2="25.71226875" width="0.225" layer="94"/>
<wire x1="90.409190625" y1="25.71226875" x2="90.419459375" y2="25.733759375" width="0.225" layer="94"/>
<wire x1="90.419459375" y1="25.733759375" x2="90.425940625" y2="25.75671875" width="0.225" layer="94"/>
<wire x1="90.425940625" y1="25.75671875" x2="90.4332" y2="25.80388125" width="0.225" layer="94"/>
<wire x1="90.4332" y1="25.80388125" x2="90.436309375" y2="25.85151875" width="0.225" layer="94"/>
<wire x1="90.436309375" y1="25.85151875" x2="90.43711875" y2="25.89925" width="0.225" layer="94"/>
<wire x1="89.66001875" y1="25.980109375" x2="89.67878125" y2="25.718659375" width="0.225" layer="94" curve="8.208716"/>
<wire x1="89.67878125" y1="25.718659375" x2="89.74463125" y2="25.514" width="0.225" layer="94" curve="19.254744"/>
<wire x1="89.74463125" y1="25.514" x2="89.76749375" y2="25.47364375" width="0.225" layer="94" curve="4.135389"/>
<wire x1="89.767490625" y1="25.473640625" x2="89.966896875" y2="25.309234375" width="0.225" layer="94" curve="37.791863"/>
<wire x1="89.9669" y1="25.309240625" x2="90.02831875" y2="25.289" width="0.225" layer="94" curve="4.744864"/>
<wire x1="90.02831875" y1="25.289" x2="90.22315" y2="25.259475" width="0.225" layer="94" curve="14.492988"/>
<wire x1="90.86316875" y1="26.071809375" x2="90.86316875" y2="25.964" width="0.225" layer="94"/>
<wire x1="90.86316875" y1="25.964" x2="90.86316875" y2="25.89603125" width="0.225" layer="94"/>
<wire x1="90.83383125" y1="25.63598125" x2="90.852559375" y2="25.739" width="0.225" layer="94" curve="5.141671"/>
<wire x1="90.852559375" y1="25.739" x2="90.86316875" y2="25.89603125" width="0.225" layer="94" curve="7.731903"/>
<wire x1="90.710259375" y1="25.40781875" x2="90.83383125" y2="25.63598125" width="0.225" layer="94" curve="31.131749"/>
<wire x1="90.483690625" y1="25.282109375" x2="90.710259375" y2="25.40781875" width="0.225" layer="94" curve="33.941784"/>
<wire x1="90.22315" y1="25.25946875" x2="90.483690625" y2="25.282103125" width="0.225" layer="94" curve="14.175253"/>
<wire x1="89.66001875" y1="25.980109375" x2="89.66001875" y2="26.189" width="0.225" layer="94"/>
<wire x1="89.66001875" y1="26.189" x2="89.66001875" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="89.6773" y1="26.590590625" x2="89.66001875" y2="26.34016875" width="0.225" layer="94" curve="7.895583"/>
<wire x1="89.75726875" y1="26.82721875" x2="89.685840625" y2="26.639" width="0.225" layer="94" curve="17.336884"/>
<wire x1="89.685840625" y1="26.639" x2="89.6773" y2="26.590590625" width="0.225" layer="94" curve="4.218105"/>
<wire x1="89.93936875" y1="26.995190625" x2="89.75726875" y2="26.82721875" width="0.225" layer="94" curve="35.721204"/>
<wire x1="90.18176875" y1="27.05536875" x2="89.93936875" y2="26.995190625" width="0.225" layer="94" curve="21.771833"/>
<wire x1="90.830209375" y1="26.30881875" x2="90.830209375" y2="26.414" width="0.225" layer="94"/>
<wire x1="90.830209375" y1="26.414" x2="90.830209375" y2="26.49631875" width="0.225" layer="94"/>
<wire x1="90.830209375" y1="26.49631875" x2="90.7978375" y2="26.744540625" width="0.225" layer="94" curve="14.861165"/>
<wire x1="90.797840625" y1="26.744540625" x2="90.6616625" y2="26.951284375" width="0.225" layer="94" curve="37.022842"/>
<wire x1="90.661659375" y1="26.95128125" x2="90.432340625" y2="27.047065625" width="0.225" layer="94" curve="30.892228"/>
<wire x1="90.432340625" y1="27.04706875" x2="90.18176875" y2="27.055365625" width="0.225" layer="94" curve="10.656803"/>
<wire x1="68.6921" y1="33.944190625" x2="68.78723125" y2="33.839" width="0.225" layer="94"/>
<wire x1="68.78723125" y1="33.839" x2="69.804659375" y2="32.714" width="0.225" layer="94"/>
<wire x1="69.804659375" y1="32.714" x2="70.21163125" y2="32.264" width="0.225" layer="94"/>
<wire x1="70.21163125" y1="32.264" x2="71.229059375" y2="31.139" width="0.225" layer="94"/>
<wire x1="71.229059375" y1="31.139" x2="71.63603125" y2="30.689" width="0.225" layer="94"/>
<wire x1="71.63603125" y1="30.689" x2="72.653459375" y2="29.564" width="0.225" layer="94"/>
<wire x1="72.653459375" y1="29.564" x2="72.94835" y2="29.23793125" width="0.225" layer="94"/>
<wire x1="72.94835" y1="29.23793125" x2="73.44671875" y2="29.789" width="0.225" layer="94"/>
<wire x1="73.44671875" y1="29.789" x2="73.6502" y2="30.014" width="0.225" layer="94"/>
<wire x1="73.6502" y1="30.014" x2="74.26065" y2="30.689" width="0.225" layer="94"/>
<wire x1="74.26065" y1="30.689" x2="74.46413125" y2="30.914" width="0.225" layer="94"/>
<wire x1="74.46413125" y1="30.914" x2="75.278059375" y2="31.814" width="0.225" layer="94"/>
<wire x1="75.278059375" y1="31.814" x2="75.481540625" y2="32.039" width="0.225" layer="94"/>
<wire x1="75.481540625" y1="32.039" x2="76.29546875" y2="32.939" width="0.225" layer="94"/>
<wire x1="76.29546875" y1="32.939" x2="76.49895" y2="33.164" width="0.225" layer="94"/>
<wire x1="76.49895" y1="33.164" x2="76.70243125" y2="33.389" width="0.225" layer="94"/>
<wire x1="76.70243125" y1="33.389" x2="77.204709375" y2="33.944390625" width="0.225" layer="94"/>
<wire x1="77.204709375" y1="33.944390625" x2="76.036390625" y2="35.00103125" width="0.225" layer="94"/>
<wire x1="76.036390625" y1="35.00103125" x2="74.57861875" y2="33.389" width="0.225" layer="94"/>
<wire x1="74.57861875" y1="33.389" x2="74.37515" y2="33.164" width="0.225" layer="94"/>
<wire x1="74.37515" y1="33.164" x2="74.17168125" y2="32.939" width="0.225" layer="94"/>
<wire x1="74.17168125" y1="32.939" x2="73.764740625" y2="32.489" width="0.225" layer="94"/>
<wire x1="73.764740625" y1="32.489" x2="72.94835" y2="31.58621875" width="0.225" layer="94"/>
<wire x1="72.94835" y1="31.58621875" x2="72.742359375" y2="31.814" width="0.225" layer="94"/>
<wire x1="72.742359375" y1="31.814" x2="72.131940625" y2="32.489" width="0.225" layer="94"/>
<wire x1="72.131940625" y1="32.489" x2="71.724990625" y2="32.939" width="0.225" layer="94"/>
<wire x1="71.724990625" y1="32.939" x2="71.318040625" y2="33.389" width="0.225" layer="94"/>
<wire x1="71.318040625" y1="33.389" x2="70.911090625" y2="33.839" width="0.225" layer="94"/>
<wire x1="70.911090625" y1="33.839" x2="70.504140625" y2="34.289" width="0.225" layer="94"/>
<wire x1="70.504140625" y1="34.289" x2="70.097190625" y2="34.739" width="0.225" layer="94"/>
<wire x1="70.097190625" y1="34.739" x2="69.86021875" y2="35.001040625" width="0.225" layer="94"/>
<wire x1="81.341240625" y1="26.03255" x2="81.74326875" y2="26.03255" width="0.225" layer="94"/>
<wire x1="81.74326875" y1="26.03255" x2="81.74326875" y2="25.964" width="0.225" layer="94"/>
<wire x1="81.74326875" y1="25.964" x2="81.74326875" y2="25.812240625" width="0.225" layer="94"/>
<wire x1="81.723590625" y1="25.602390625" x2="81.743275" y2="25.812240625" width="0.225" layer="94" curve="10.717258"/>
<wire x1="81.637890625" y1="25.41138125" x2="81.7235875" y2="25.602390625" width="0.225" layer="94" curve="26.892331"/>
<wire x1="81.467640625" y1="25.291140625" x2="81.6378875" y2="25.41138125" width="0.225" layer="94" curve="34.316783"/>
<wire x1="81.260009375" y1="25.258559375" x2="81.467640625" y2="25.291140625" width="0.225" layer="94" curve="18.312496"/>
<wire x1="81.05158125" y1="25.28623125" x2="81.260009375" y2="25.258559375" width="0.225" layer="94" curve="14.64866"/>
<wire x1="80.880909375" y1="25.405009375" x2="81.05158125" y2="25.286234375" width="0.225" layer="94" curve="39.895242"/>
<wire x1="80.80178125" y1="25.5987" x2="80.880903125" y2="25.40500625" width="0.225" layer="94" curve="25.996239"/>
<wire x1="80.786140625" y1="25.80901875" x2="80.786140625" y2="25.964" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="25.964" x2="80.786140625" y2="26.189" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="26.189" x2="80.786140625" y2="26.414" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="26.414" x2="80.786140625" y2="26.626990625" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="25.80901875" x2="80.78728125" y2="25.739" width="0.225" layer="94" curve="3.295417"/>
<wire x1="80.78728125" y1="25.739" x2="80.801784375" y2="25.5987" width="0.225" layer="94" curve="6.640297"/>
<wire x1="81.206190625" y1="25.86908125" x2="81.206190625" y2="25.964" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="25.964" x2="81.206190625" y2="26.189" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="26.189" x2="81.206190625" y2="26.414" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="26.414" x2="81.206190625" y2="26.626990625" width="0.225" layer="94"/>
<wire x1="81.341240625" y1="26.03255" x2="81.341240625" y2="25.964" width="0.225" layer="94"/>
<wire x1="81.341240625" y1="25.964" x2="81.341240625" y2="25.779140625" width="0.225" layer="94"/>
<wire x1="81.317509375" y1="25.6654" x2="81.327084375" y2="25.6739125" width="0.225" layer="94" curve="49.818215"/>
<wire x1="81.30451875" y1="25.66235" x2="81.3175125" y2="25.66539375" width="0.225" layer="94" curve="7.103613"/>
<wire x1="81.291240625" y1="25.66093125" x2="81.30451875" y2="25.66235625" width="0.225" layer="94" curve="7.003084"/>
<wire x1="81.277890625" y1="25.660490625" x2="81.291240625" y2="25.660934375" width="0.225" layer="94" curve="1.449937"/>
<wire x1="81.26453125" y1="25.66073125" x2="81.277890625" y2="25.6604875" width="0.225" layer="94" curve="4.441603"/>
<wire x1="81.33501875" y1="25.69935" x2="81.33183125" y2="25.68638125" width="0.225" layer="94"/>
<wire x1="81.33183125" y1="25.68638125" x2="81.327090625" y2="25.673909375" width="0.225" layer="94"/>
<wire x1="81.33501875" y1="25.69935" x2="81.3373" y2="25.712509375" width="0.225" layer="94"/>
<wire x1="81.3373" y1="25.712509375" x2="81.340009375" y2="25.739" width="0.225" layer="94"/>
<wire x1="81.340009375" y1="25.739" x2="81.34001875" y2="25.739090625" width="0.225" layer="94"/>
<wire x1="81.34001875" y1="25.739090625" x2="81.341240625" y2="25.779140625" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="25.86908125" x2="81.20651875" y2="25.818159375" width="0.225" layer="94"/>
<wire x1="81.20651875" y1="25.818159375" x2="81.208009375" y2="25.76726875" width="0.225" layer="94"/>
<wire x1="81.208009375" y1="25.76726875" x2="81.209609375" y2="25.741859375" width="0.225" layer="94"/>
<wire x1="81.209609375" y1="25.741859375" x2="81.21228125" y2="25.716540625" width="0.225" layer="94"/>
<wire x1="81.21228125" y1="25.716540625" x2="81.21671875" y2="25.69148125" width="0.225" layer="94"/>
<wire x1="81.21671875" y1="25.69148125" x2="81.225321875" y2="25.66765" width="0.225" layer="94" curve="19.627877"/>
<wire x1="81.26453125" y1="25.66073125" x2="81.251209375" y2="25.66166875" width="0.225" layer="94"/>
<wire x1="81.238" y1="25.663609375" x2="81.251209375" y2="25.661671875" width="0.225" layer="94" curve="8.648156"/>
<wire x1="81.22531875" y1="25.66765" x2="81.238" y2="25.66360625" width="0.225" layer="94" curve="10.044676"/>
<wire x1="81.7013" y1="26.626990625" x2="81.206190625" y2="26.626990625" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="27.03186875" x2="81.7013" y2="27.03186875" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="27.401009375" x2="81.206190625" y2="27.314" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="27.314" x2="81.206190625" y2="27.089" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="27.089" x2="81.206190625" y2="27.03186875" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="27.03186875" x2="80.786140625" y2="27.089" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="27.089" x2="80.786140625" y2="27.314" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="27.314" x2="80.786140625" y2="27.401009375" width="0.225" layer="94"/>
<wire x1="80.627059375" y1="27.03186875" x2="80.786140625" y2="27.03186875" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="26.626990625" x2="80.627059375" y2="26.626990625" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="27.401009375" x2="81.206190625" y2="27.401009375" width="0.225" layer="94"/>
<wire x1="81.7013" y1="27.03186875" x2="81.7013" y2="26.864" width="0.225" layer="94"/>
<wire x1="81.7013" y1="26.864" x2="81.7013" y2="26.639" width="0.225" layer="94"/>
<wire x1="81.7013" y1="26.639" x2="81.7013" y2="26.626990625" width="0.225" layer="94"/>
<wire x1="80.627059375" y1="27.03186875" x2="80.627059375" y2="26.864" width="0.225" layer="94"/>
<wire x1="80.627059375" y1="26.864" x2="80.627059375" y2="26.639" width="0.225" layer="94"/>
<wire x1="80.627059375" y1="26.639" x2="80.627059375" y2="26.626990625" width="0.225" layer="94"/>
<wire x1="92.6429" y1="26.95253125" x2="92.469709375" y2="27.040159375" width="0.225" layer="94" curve="32.338936"/>
<wire x1="79.95228125" y1="26.071809375" x2="79.52623125" y2="26.071809375" width="0.225" layer="94"/>
<wire x1="79.49928125" y1="26.30881875" x2="79.91931875" y2="26.30881875" width="0.225" layer="94"/>
<wire x1="79.91931875" y1="26.30881875" x2="79.91931875" y2="26.414" width="0.225" layer="94"/>
<wire x1="79.91931875" y1="26.414" x2="79.91931875" y2="26.49631875" width="0.225" layer="94"/>
<wire x1="79.91931875" y1="26.49631875" x2="79.886946875" y2="26.744540625" width="0.225" layer="94" curve="14.861165"/>
<wire x1="79.88695" y1="26.744540625" x2="79.750771875" y2="26.951284375" width="0.225" layer="94" curve="37.022842"/>
<wire x1="79.75076875" y1="26.95128125" x2="79.521459375" y2="27.047065625" width="0.225" layer="94" curve="30.891086"/>
<wire x1="79.521459375" y1="27.04706875" x2="79.27088125" y2="27.055365625" width="0.225" layer="94" curve="10.657067"/>
<wire x1="79.27088125" y1="27.05536875" x2="79.02848125" y2="26.995190625" width="0.225" layer="94" curve="21.019898"/>
<wire x1="79.02848125" y1="26.995190625" x2="78.84638125" y2="26.82721875" width="0.225" layer="94" curve="36.47327"/>
<wire x1="78.84638125" y1="26.82721875" x2="78.77521875" y2="26.639" width="0.225" layer="94" curve="16.726592"/>
<wire x1="78.77521875" y1="26.639" x2="78.7664125" y2="26.590590625" width="0.225" layer="94" curve="4.076419"/>
<wire x1="78.766409375" y1="26.590590625" x2="78.75028125" y2="26.414" width="0.225" layer="94" curve="6.105964"/>
<wire x1="78.75028125" y1="26.414" x2="78.749128125" y2="26.34016875" width="0.225" layer="94" curve="2.541594"/>
<wire x1="79.95228125" y1="26.071809375" x2="79.95228125" y2="25.964" width="0.225" layer="94"/>
<wire x1="79.95228125" y1="25.964" x2="79.95228125" y2="25.89603125" width="0.225" layer="94"/>
<wire x1="79.922940625" y1="25.63598125" x2="79.94166875" y2="25.739" width="0.225" layer="94" curve="5.141671"/>
<wire x1="79.94166875" y1="25.739" x2="79.952278125" y2="25.89603125" width="0.225" layer="94" curve="7.731903"/>
<wire x1="79.79936875" y1="25.40781875" x2="79.922940625" y2="25.63598125" width="0.225" layer="94" curve="31.131749"/>
<wire x1="79.572809375" y1="25.282109375" x2="79.60065" y2="25.289" width="0.225" layer="94" curve="3.702975"/>
<wire x1="79.60065" y1="25.289" x2="79.799371875" y2="25.407815625" width="0.225" layer="94" curve="30.237845"/>
<wire x1="79.312259375" y1="25.25946875" x2="79.572809375" y2="25.28210625" width="0.225" layer="94" curve="14.175788"/>
<wire x1="79.056009375" y1="25.309240625" x2="79.119840625" y2="25.289" width="0.225" layer="94" curve="4.53295"/>
<wire x1="79.119840625" y1="25.289" x2="79.312259375" y2="25.259475" width="0.225" layer="94" curve="13.203582"/>
<wire x1="78.856609375" y1="25.473640625" x2="79.056009375" y2="25.3092375" width="0.225" layer="94" curve="39.29237"/>
<wire x1="78.767890625" y1="25.718659375" x2="78.85660625" y2="25.473640625" width="0.225" layer="94" curve="21.889189"/>
<wire x1="78.74913125" y1="25.980109375" x2="78.76789375" y2="25.718659375" width="0.225" layer="94" curve="9.71004"/>
<wire x1="78.74913125" y1="25.980109375" x2="78.74913125" y2="26.189" width="0.225" layer="94"/>
<wire x1="78.74913125" y1="26.189" x2="78.74913125" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="79.49928125" y1="26.30881875" x2="79.49928125" y2="26.414" width="0.225" layer="94"/>
<wire x1="79.49928125" y1="26.414" x2="79.49928125" y2="26.44828125" width="0.225" layer="94"/>
<wire x1="79.49928125" y1="26.44828125" x2="79.496253125" y2="26.51431875" width="0.225" layer="94" curve="5.249884"/>
<wire x1="79.49625" y1="26.51431875" x2="79.48934375" y2="26.557840625" width="0.225" layer="94" curve="7.529032"/>
<wire x1="79.489340625" y1="26.557840625" x2="79.483196875" y2="26.579" width="0.225" layer="94" curve="6.832689"/>
<wire x1="79.4832" y1="26.579" x2="79.47398125" y2="26.598990625" width="0.225" layer="94" curve="10.282953"/>
<wire x1="79.47398125" y1="26.598990625" x2="79.459609375" y2="26.615509375" width="0.225" layer="94" curve="22.257449"/>
<wire x1="79.459609375" y1="26.615509375" x2="79.440259375" y2="26.625909375" width="0.225" layer="94" curve="19.188066"/>
<wire x1="79.440259375" y1="26.625909375" x2="79.39738125" y2="26.63575" width="0.225" layer="94" curve="11.463239"/>
<wire x1="79.39738125" y1="26.63575" x2="79.3534" y2="26.6386375" width="0.225" layer="94" curve="6.876925"/>
<wire x1="79.3534" y1="26.638640625" x2="79.30931875" y2="26.63774375" width="0.225" layer="94" curve="2.970191"/>
<wire x1="79.30931875" y1="26.637740625" x2="79.265559375" y2="26.632625" width="0.225" layer="94" curve="8.0302"/>
<wire x1="79.265559375" y1="26.63263125" x2="79.244209375" y2="26.6272" width="0.225" layer="94" curve="7.184068"/>
<wire x1="79.244209375" y1="26.6272" x2="79.2243" y2="26.617890625" width="0.225" layer="94" curve="14.385749"/>
<wire x1="79.2243" y1="26.617890625" x2="79.210078125" y2="26.6013" width="0.225" layer="94" curve="34.295039"/>
<wire x1="79.21008125" y1="26.6013" x2="79.191453125" y2="26.53813125" width="0.225" layer="94" curve="14.037627"/>
<wire x1="79.19145" y1="26.53813125" x2="79.1844125" y2="26.472390625" width="0.225" layer="94" curve="6.60122"/>
<wire x1="79.52623125" y1="26.071809375" x2="79.52623125" y2="25.964" width="0.225" layer="94"/>
<wire x1="79.52623125" y1="25.964" x2="79.52623125" y2="25.89925" width="0.225" layer="94"/>
<wire x1="79.51938125" y1="25.780190625" x2="79.526234375" y2="25.89925" width="0.225" layer="94" curve="6.589587"/>
<wire x1="79.50856875" y1="25.733759375" x2="79.519378125" y2="25.780190625" width="0.225" layer="94" curve="13.026604"/>
<wire x1="79.45898125" y1="25.6871" x2="79.508565625" y2="25.733759375" width="0.225" layer="94" curve="54.248333"/>
<wire x1="79.4121" y1="25.6784" x2="79.45898125" y2="25.687103125" width="0.225" layer="94" curve="11.235116"/>
<wire x1="79.292940625" y1="25.678809375" x2="79.4121" y2="25.678396875" width="0.225" layer="94" curve="10.193817"/>
<wire x1="79.24633125" y1="25.688690625" x2="79.292940625" y2="25.6788125" width="0.225" layer="94" curve="13.338819"/>
<wire x1="79.22515" y1="25.6995" x2="79.24633125" y2="25.6886875" width="0.225" layer="94" curve="16.819291"/>
<wire x1="79.21035" y1="25.718009375" x2="79.225146875" y2="25.69949375" width="0.225" layer="94" curve="31.833994"/>
<wire x1="79.195259375" y1="25.763159375" x2="79.21035625" y2="25.7180125" width="0.225" layer="94" curve="8.452794"/>
<wire x1="79.187759375" y1="25.810290625" x2="79.19525625" y2="25.763159375" width="0.225" layer="94" curve="10.449934"/>
<wire x1="79.187759375" y1="25.810290625" x2="79.18275" y2="25.881709375" width="0.225" layer="94"/>
<wire x1="79.18441875" y1="26.472390625" x2="79.181740625" y2="26.406309375" width="0.225" layer="94"/>
<wire x1="79.181740625" y1="26.406309375" x2="79.181109375" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="79.181109375" y1="26.34016875" x2="79.181109375" y2="26.189" width="0.225" layer="94"/>
<wire x1="79.181109375" y1="26.189" x2="79.181109375" y2="25.97718125" width="0.225" layer="94"/>
<wire x1="79.181109375" y1="25.97718125" x2="79.18275" y2="25.881709375" width="0.225" layer="94"/>
<wire x1="77.1742" y1="26.422790625" x2="77.54935" y2="26.422790625" width="0.225" layer="94"/>
<wire x1="77.1742" y1="26.00881875" x2="77.969390625" y2="26.00881875" width="0.225" layer="94"/>
<wire x1="77.969390625" y1="26.00881875" x2="77.969390625" y2="26.189" width="0.225" layer="94"/>
<wire x1="77.969390625" y1="26.189" x2="77.969390625" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="77.969390625" y1="26.34016875" x2="77.948609375" y2="26.61348125" width="0.225" layer="94" curve="8.696144"/>
<wire x1="77.948609375" y1="26.61348125" x2="77.84784375" y2="26.866196875" width="0.225" layer="94" curve="26.085225"/>
<wire x1="77.84785" y1="26.8662" x2="77.62745" y2="27.022209375" width="0.225" layer="94" curve="39.85306"/>
<wire x1="77.62745" y1="27.022209375" x2="77.35661875" y2="27.058828125" width="0.225" layer="94" curve="15.332321"/>
<wire x1="77.35661875" y1="27.05881875" x2="77.08588125" y2="27.02155" width="0.225" layer="94" curve="15.743623"/>
<wire x1="77.08588125" y1="27.02155" x2="76.865265625" y2="26.865728125" width="0.225" layer="94" curve="39.046943"/>
<wire x1="76.865259375" y1="26.86573125" x2="76.864040625" y2="26.864" width="0.225" layer="94" curve="0.204743"/>
<wire x1="76.864040625" y1="26.864" x2="76.763478125" y2="26.61343125" width="0.225" layer="94" curve="26.341038"/>
<wire x1="76.76348125" y1="26.61343125" x2="76.74395" y2="26.414" width="0.225" layer="94" curve="6.208124"/>
<wire x1="76.74395" y1="26.414" x2="76.74221875" y2="26.34016875" width="0.225" layer="94" curve="2.287022"/>
<wire x1="77.561359375" y1="25.98158125" x2="77.966390625" y2="25.98158125" width="0.225" layer="94"/>
<wire x1="77.966390625" y1="25.98158125" x2="77.966390625" y2="25.964" width="0.225" layer="94"/>
<wire x1="77.966390625" y1="25.964" x2="77.966390625" y2="25.8181" width="0.225" layer="94"/>
<wire x1="77.92765" y1="25.565709375" x2="77.966390625" y2="25.8181" width="0.225" layer="94" curve="17.452125"/>
<wire x1="77.77643125" y1="25.363190625" x2="77.92765" y2="25.565709375" width="0.225" layer="94" curve="38.592139"/>
<wire x1="77.53896875" y1="25.271940625" x2="77.618090625" y2="25.289" width="0.225" layer="94" curve="8.168491"/>
<wire x1="77.618090625" y1="25.289" x2="77.77643125" y2="25.363190625" width="0.225" layer="94" curve="17.702304"/>
<wire x1="77.28301875" y1="25.26058125" x2="77.53896875" y2="25.271940625" width="0.225" layer="94" curve="11.086795"/>
<wire x1="77.03403125" y1="25.31643125" x2="77.11308125" y2="25.289" width="0.225" layer="94" curve="6.296209"/>
<wire x1="77.11308125" y1="25.289" x2="77.28301875" y2="25.260578125" width="0.225" layer="94" curve="12.986111"/>
<wire x1="76.844159375" y1="25.483540625" x2="77.034028125" y2="25.316421875" width="0.225" layer="94" curve="38.138006"/>
<wire x1="76.760209375" y1="25.724359375" x2="76.8441625" y2="25.483540625" width="0.225" layer="94" curve="20.717901"/>
<wire x1="76.74221875" y1="25.980109375" x2="76.758" y2="25.739" width="0.225" layer="94" curve="9.114768"/>
<wire x1="76.758" y1="25.739" x2="76.760209375" y2="25.724359375" width="0.225" layer="94" curve="0.557951"/>
<wire x1="76.74221875" y1="25.980109375" x2="76.74221875" y2="26.189" width="0.225" layer="94"/>
<wire x1="76.74221875" y1="26.189" x2="76.74221875" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="77.1742" y1="26.00881875" x2="77.1742" y2="25.97718125" width="0.225" layer="94"/>
<wire x1="77.1742" y1="25.97718125" x2="77.17648125" y2="25.863140625" width="0.225" layer="94"/>
<wire x1="77.17648125" y1="25.863140625" x2="77.179640625" y2="25.81763125" width="0.225" layer="94"/>
<wire x1="77.179640625" y1="25.81763125" x2="77.185759375" y2="25.77241875" width="0.225" layer="94"/>
<wire x1="77.185759375" y1="25.77241875" x2="77.19780625" y2="25.728490625" width="0.225" layer="94" curve="15.249939"/>
<wire x1="77.197809375" y1="25.728490625" x2="77.22771875" y2="25.69585" width="0.225" layer="94" curve="39.077584"/>
<wire x1="77.22771875" y1="25.69585" x2="77.2493" y2="25.688528125" width="0.225" layer="94" curve="18.445063"/>
<wire x1="77.2493" y1="25.68853125" x2="77.45375" y2="25.6834" width="0.225" layer="94" curve="16.158964"/>
<wire x1="77.45375" y1="25.6834" x2="77.498609375" y2="25.691559375" width="0.225" layer="94" curve="7.337797"/>
<wire x1="77.498609375" y1="25.691559375" x2="77.520209375" y2="25.6988375" width="0.225" layer="94" curve="9.286376"/>
<wire x1="77.561359375" y1="25.98158125" x2="77.561359375" y2="25.964" width="0.225" layer="94"/>
<wire x1="77.561359375" y1="25.964" x2="77.561359375" y2="25.82103125" width="0.225" layer="94"/>
<wire x1="77.561359375" y1="25.82103125" x2="77.559340625" y2="25.775459375" width="0.225" layer="94"/>
<wire x1="77.559340625" y1="25.775459375" x2="77.55613125" y2="25.75288125" width="0.225" layer="94"/>
<wire x1="77.520209375" y1="25.698840625" x2="77.556125" y2="25.75288125" width="0.225" layer="94" curve="51.030296"/>
<wire x1="77.184159375" y1="26.547240625" x2="77.174196875" y2="26.422790625" width="0.225" layer="94" curve="9.151585"/>
<wire x1="77.2077" y1="26.6134" x2="77.184159375" y2="26.547240625" width="0.225" layer="94" curve="20.872634"/>
<wire x1="77.54935" y1="26.422790625" x2="77.539775" y2="26.547290625" width="0.225" layer="94" curve="8.794744"/>
<wire x1="77.53978125" y1="26.547290625" x2="77.527525" y2="26.592734375" width="0.225" layer="94" curve="12.594721"/>
<wire x1="77.52751875" y1="26.59273125" x2="77.516803125" y2="26.613665625" width="0.225" layer="94" curve="11.429217"/>
<wire x1="77.516809375" y1="26.61366875" x2="77.5000125" y2="26.629975" width="0.225" layer="94" curve="26.064892"/>
<wire x1="77.500009375" y1="26.62996875" x2="77.47873125" y2="26.63995" width="0.225" layer="94" curve="11.965711"/>
<wire x1="77.47873125" y1="26.63995" x2="77.455990625" y2="26.64610625" width="0.225" layer="94" curve="8.00028"/>
<wire x1="77.455990625" y1="26.646109375" x2="77.38576875" y2="26.653571875" width="0.225" layer="94" curve="10.160203"/>
<wire x1="77.38576875" y1="26.65356875" x2="77.315109375" y2="26.652346875" width="0.225" layer="94" curve="3.952821"/>
<wire x1="77.315109375" y1="26.65235" x2="77.268440625" y2="26.646071875" width="0.225" layer="94" curve="9.390964"/>
<wire x1="77.268440625" y1="26.64606875" x2="77.245740625" y2="26.639796875" width="0.225" layer="94" curve="6.177099"/>
<wire x1="77.245740625" y1="26.6398" x2="77.24345" y2="26.639" width="0.225" layer="94" curve="1.397304"/>
<wire x1="77.24345" y1="26.639" x2="77.22448125" y2="26.629740625" width="0.225" layer="94" curve="12.178653"/>
<wire x1="77.22448125" y1="26.629740625" x2="77.207703125" y2="26.613396875" width="0.225" layer="94" curve="24.279547"/>
<wire x1="73.9395" y1="26.629740625" x2="73.922721875" y2="26.613396875" width="0.225" layer="94" curve="24.279547"/>
<wire x1="73.96075" y1="26.6398" x2="73.958459375" y2="26.639" width="0.225" layer="94" curve="1.397249"/>
<wire x1="73.958459375" y1="26.639" x2="73.939496875" y2="26.62974375" width="0.225" layer="94" curve="12.174138"/>
<wire x1="73.98345" y1="26.64606875" x2="73.96075" y2="26.639796875" width="0.225" layer="94" curve="6.177099"/>
<wire x1="74.03013125" y1="26.65235" x2="73.98345" y2="26.64606875" width="0.225" layer="94" curve="9.393557"/>
<wire x1="74.100790625" y1="26.65356875" x2="74.03013125" y2="26.652346875" width="0.225" layer="94" curve="3.952821"/>
<wire x1="74.171009375" y1="26.646109375" x2="74.100790625" y2="26.653571875" width="0.225" layer="94" curve="10.159745"/>
<wire x1="74.193740625" y1="26.63995" x2="74.171009375" y2="26.646103125" width="0.225" layer="94" curve="7.996993"/>
<wire x1="74.21501875" y1="26.62996875" x2="74.193740625" y2="26.63995" width="0.225" layer="94" curve="11.965711"/>
<wire x1="74.23181875" y1="26.61366875" x2="74.215021875" y2="26.629975" width="0.225" layer="94" curve="26.067092"/>
<wire x1="74.242540625" y1="26.59273125" x2="74.231821875" y2="26.613671875" width="0.225" layer="94" curve="11.432049"/>
<wire x1="74.254790625" y1="26.547290625" x2="74.2425375" y2="26.59273125" width="0.225" layer="94" curve="12.593993"/>
<wire x1="74.264359375" y1="26.422790625" x2="74.254784375" y2="26.547290625" width="0.225" layer="94" curve="8.794744"/>
<wire x1="73.92271875" y1="26.6134" x2="73.899178125" y2="26.547240625" width="0.225" layer="94" curve="20.872634"/>
<wire x1="73.89918125" y1="26.547240625" x2="73.88921875" y2="26.422790625" width="0.225" layer="94" curve="9.151622"/>
<wire x1="74.23521875" y1="25.698840625" x2="74.2678" y2="25.739" width="0.225" layer="94" curve="40.155698"/>
<wire x1="74.2678" y1="25.739" x2="74.271134375" y2="25.75288125" width="0.225" layer="94" curve="10.876304"/>
<wire x1="74.27435" y1="25.775459375" x2="74.271140625" y2="25.75288125" width="0.225" layer="94"/>
<wire x1="74.27638125" y1="25.82103125" x2="74.27435" y2="25.775459375" width="0.225" layer="94"/>
<wire x1="74.27638125" y1="25.98158125" x2="74.27638125" y2="25.964" width="0.225" layer="94"/>
<wire x1="74.27638125" y1="25.964" x2="74.27638125" y2="25.82103125" width="0.225" layer="94"/>
<wire x1="74.21361875" y1="25.691559375" x2="74.23521875" y2="25.6988375" width="0.225" layer="94" curve="9.286507"/>
<wire x1="74.16876875" y1="25.6834" x2="74.21361875" y2="25.691559375" width="0.225" layer="94" curve="7.336311"/>
<wire x1="73.964309375" y1="25.68853125" x2="74.16876875" y2="25.6834" width="0.225" layer="94" curve="16.159693"/>
<wire x1="73.94273125" y1="25.69585" x2="73.964309375" y2="25.68853125" width="0.225" layer="94" curve="18.441658"/>
<wire x1="73.91281875" y1="25.728490625" x2="73.942728125" y2="25.695846875" width="0.225" layer="94" curve="39.07874"/>
<wire x1="73.90078125" y1="25.77241875" x2="73.912828125" y2="25.72849375" width="0.225" layer="94" curve="15.249051"/>
<wire x1="73.894659375" y1="25.81763125" x2="73.90078125" y2="25.77241875" width="0.225" layer="94"/>
<wire x1="73.891490625" y1="25.863140625" x2="73.894659375" y2="25.81763125" width="0.225" layer="94"/>
<wire x1="73.88921875" y1="25.97718125" x2="73.88948125" y2="25.964" width="0.225" layer="94"/>
<wire x1="73.88948125" y1="25.964" x2="73.891490625" y2="25.863140625" width="0.225" layer="94"/>
<wire x1="73.88921875" y1="26.00881875" x2="73.88921875" y2="25.97718125" width="0.225" layer="94"/>
<wire x1="73.457240625" y1="25.980109375" x2="73.457240625" y2="26.189" width="0.225" layer="94"/>
<wire x1="73.457240625" y1="26.189" x2="73.457240625" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="73.457240625" y1="25.980109375" x2="73.47523125" y2="25.724359375" width="0.225" layer="94" curve="9.672758"/>
<wire x1="73.47523125" y1="25.724359375" x2="73.559184375" y2="25.48354375" width="0.225" layer="94" curve="20.717853"/>
<wire x1="73.55918125" y1="25.483540625" x2="73.749046875" y2="25.316425" width="0.225" layer="94" curve="38.137176"/>
<wire x1="73.74905" y1="25.31643125" x2="73.998040625" y2="25.26058125" width="0.225" layer="94" curve="19.282594"/>
<wire x1="73.998040625" y1="25.26058125" x2="74.25398125" y2="25.271940625" width="0.225" layer="94" curve="11.086392"/>
<wire x1="74.25398125" y1="25.271940625" x2="74.3331" y2="25.289" width="0.225" layer="94" curve="8.168179"/>
<wire x1="74.3331" y1="25.289" x2="74.491446875" y2="25.36319375" width="0.225" layer="94" curve="17.703003"/>
<wire x1="74.49145" y1="25.363190625" x2="74.642665625" y2="25.565709375" width="0.225" layer="94" curve="38.59175"/>
<wire x1="74.64266875" y1="25.565709375" x2="74.681409375" y2="25.8181" width="0.225" layer="94" curve="17.451998"/>
<wire x1="74.681409375" y1="25.98158125" x2="74.681409375" y2="25.964" width="0.225" layer="94"/>
<wire x1="74.681409375" y1="25.964" x2="74.681409375" y2="25.8181" width="0.225" layer="94"/>
<wire x1="74.27638125" y1="25.98158125" x2="74.681409375" y2="25.98158125" width="0.225" layer="94"/>
<wire x1="73.4785" y1="26.61343125" x2="73.45896875" y2="26.414" width="0.225" layer="94" curve="6.208134"/>
<wire x1="73.45896875" y1="26.414" x2="73.4572375" y2="26.34016875" width="0.225" layer="94" curve="2.287026"/>
<wire x1="73.58028125" y1="26.86573125" x2="73.4785" y2="26.61343125" width="0.225" layer="94" curve="26.545833"/>
<wire x1="73.8009" y1="27.02155" x2="73.58028125" y2="26.86573125" width="0.225" layer="94" curve="39.047008"/>
<wire x1="74.071640625" y1="27.05881875" x2="73.8009" y2="27.02155" width="0.225" layer="94" curve="15.743809"/>
<wire x1="74.342459375" y1="27.022209375" x2="74.071640625" y2="27.058825" width="0.225" layer="94" curve="15.331635"/>
<wire x1="74.562859375" y1="26.8662" x2="74.342459375" y2="27.022209375" width="0.225" layer="94" curve="39.85306"/>
<wire x1="74.66363125" y1="26.61348125" x2="74.5628625" y2="26.866203125" width="0.225" layer="94" curve="26.085633"/>
<wire x1="74.684409375" y1="26.34016875" x2="74.663628125" y2="26.61348125" width="0.225" layer="94" curve="8.69613"/>
<wire x1="74.684409375" y1="26.00881875" x2="74.684409375" y2="26.189" width="0.225" layer="94"/>
<wire x1="74.684409375" y1="26.189" x2="74.684409375" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="73.88921875" y1="26.00881875" x2="74.684409375" y2="26.00881875" width="0.225" layer="94"/>
<wire x1="73.88921875" y1="26.422790625" x2="74.264359375" y2="26.422790625" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="25.28548125" x2="82.92818125" y2="25.28548125" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="27.03186875" x2="82.80066875" y2="27.03186875" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="27.03186875" x2="82.508140625" y2="26.864" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="26.864" x2="82.508140625" y2="26.639" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="26.639" x2="82.508140625" y2="26.414" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="26.414" x2="82.508140625" y2="26.189" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="26.189" x2="82.508140625" y2="25.964" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="25.964" x2="82.508140625" y2="25.739" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="25.739" x2="82.508140625" y2="25.514" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="25.514" x2="82.508140625" y2="25.289" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="25.289" x2="82.508140625" y2="25.28548125" width="0.225" layer="94"/>
<wire x1="83.18028125" y1="26.30266875" x2="83.585309375" y2="26.30266875" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="26.358340625" x2="82.92818125" y2="26.189" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="26.189" x2="82.92818125" y2="25.964" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="25.964" x2="82.92818125" y2="25.739" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="25.739" x2="82.92818125" y2="25.514" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="25.514" x2="82.92818125" y2="25.289" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="25.289" x2="82.92818125" y2="25.28548125" width="0.225" layer="94"/>
<wire x1="82.8883" y1="26.98991875" x2="82.80066875" y2="27.03186875" width="0.225" layer="94" curve="51.162574"/>
<wire x1="86.810090625" y1="25.28548125" x2="86.810090625" y2="25.289" width="0.225" layer="94"/>
<wire x1="86.810090625" y1="25.289" x2="86.810090625" y2="25.514" width="0.225" layer="94"/>
<wire x1="86.810090625" y1="25.514" x2="86.810090625" y2="25.739" width="0.225" layer="94"/>
<wire x1="86.810090625" y1="25.739" x2="86.810090625" y2="25.964" width="0.225" layer="94"/>
<wire x1="86.810090625" y1="25.964" x2="86.810090625" y2="26.189" width="0.225" layer="94"/>
<wire x1="86.810090625" y1="26.189" x2="86.810090625" y2="26.25023125" width="0.225" layer="94"/>
<wire x1="86.39005" y1="25.28548125" x2="86.39005" y2="25.289" width="0.225" layer="94"/>
<wire x1="86.39005" y1="25.289" x2="86.39005" y2="25.514" width="0.225" layer="94"/>
<wire x1="86.39005" y1="25.514" x2="86.39005" y2="25.739" width="0.225" layer="94"/>
<wire x1="86.39005" y1="25.739" x2="86.39005" y2="25.964" width="0.225" layer="94"/>
<wire x1="86.39005" y1="25.964" x2="86.39005" y2="26.189" width="0.225" layer="94"/>
<wire x1="86.39005" y1="26.189" x2="86.39005" y2="26.414" width="0.225" layer="94"/>
<wire x1="86.39005" y1="26.414" x2="86.39005" y2="26.639" width="0.225" layer="94"/>
<wire x1="86.39005" y1="26.639" x2="86.39005" y2="26.864" width="0.225" layer="94"/>
<wire x1="86.39005" y1="26.864" x2="86.39005" y2="27.03186875" width="0.225" layer="94"/>
<wire x1="86.39005" y1="25.28548125" x2="86.810090625" y2="25.28548125" width="0.225" layer="94"/>
<wire x1="87.1462" y1="25.28548125" x2="87.566240625" y2="25.28548125" width="0.225" layer="94"/>
<wire x1="86.68258125" y1="27.03186875" x2="86.39005" y2="27.03186875" width="0.225" layer="94"/>
<wire x1="86.770490625" y1="26.98956875" x2="86.68258125" y2="27.03186875" width="0.225" layer="94" curve="51.389654"/>
<wire x1="83.585309375" y1="26.30266875" x2="83.585309375" y2="26.414" width="0.225" layer="94"/>
<wire x1="83.585309375" y1="26.414" x2="83.585309375" y2="26.532359375" width="0.225" layer="94"/>
<wire x1="83.585309375" y1="26.532359375" x2="83.57331875" y2="26.7009" width="0.225" layer="94" curve="8.13878"/>
<wire x1="83.57331875" y1="26.7009" x2="83.52315625" y2="26.8617" width="0.225" layer="94" curve="18.374099"/>
<wire x1="83.523159375" y1="26.8617" x2="83.41396875" y2="26.9887" width="0.225" layer="94" curve="28.349931"/>
<wire x1="83.41396875" y1="26.9887" x2="83.25806875" y2="27.050721875" width="0.225" layer="94" curve="26.884991"/>
<wire x1="83.25806875" y1="27.05071875" x2="83.0895" y2="27.055371875" width="0.225" layer="94" curve="13.343084"/>
<wire x1="83.0895" y1="27.05538125" x2="82.888296875" y2="26.989925" width="0.225" layer="94" curve="25.861473"/>
<wire x1="87.566240625" y1="25.28548125" x2="87.566240625" y2="25.289" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="25.289" x2="87.566240625" y2="25.514" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="25.514" x2="87.566240625" y2="25.739" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="25.739" x2="87.566240625" y2="25.964" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="25.964" x2="87.566240625" y2="26.189" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="26.189" x2="87.566240625" y2="26.414" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="26.414" x2="87.566240625" y2="26.418390625" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="26.418390625" x2="87.555215625" y2="26.606540625" width="0.225" layer="94" curve="6.706552"/>
<wire x1="87.55521875" y1="26.606540625" x2="87.509115625" y2="26.788871875" width="0.225" layer="94" curve="14.968121"/>
<wire x1="87.509109375" y1="26.78886875" x2="87.403925" y2="26.943603125" width="0.225" layer="94" curve="25.064472"/>
<wire x1="87.40393125" y1="26.943609375" x2="87.240590625" y2="27.034540625" width="0.225" layer="94" curve="28.312195"/>
<wire x1="87.240590625" y1="27.034540625" x2="87.05421875" y2="27.058825" width="0.225" layer="94" curve="15.049352"/>
<wire x1="87.05421875" y1="27.05881875" x2="86.770490625" y2="26.98956875" width="0.225" layer="94" curve="27.231353"/>
<wire x1="87.1462" y1="25.28548125" x2="87.1462" y2="25.289" width="0.225" layer="94"/>
<wire x1="87.1462" y1="25.289" x2="87.1462" y2="25.514" width="0.225" layer="94"/>
<wire x1="87.1462" y1="25.514" x2="87.1462" y2="25.739" width="0.225" layer="94"/>
<wire x1="87.1462" y1="25.739" x2="87.1462" y2="25.964" width="0.225" layer="94"/>
<wire x1="87.1462" y1="25.964" x2="87.1462" y2="26.189" width="0.225" layer="94"/>
<wire x1="87.1462" y1="26.189" x2="87.1462" y2="26.3642" width="0.225" layer="94"/>
<wire x1="87.1462" y1="26.3642" x2="87.139771875" y2="26.502909375" width="0.225" layer="94" curve="5.307835"/>
<wire x1="87.13976875" y1="26.502909375" x2="87.123915625" y2="26.584609375" width="0.225" layer="94" curve="11.346275"/>
<wire x1="87.12391875" y1="26.584609375" x2="87.11383125" y2="26.61046875" width="0.225" layer="94" curve="9.30383"/>
<wire x1="87.11383125" y1="26.61046875" x2="87.098490625" y2="26.633490625" width="0.225" layer="94" curve="15.447128"/>
<wire x1="87.098490625" y1="26.633490625" x2="87.0752" y2="26.648190625" width="0.225" layer="94" curve="32.662799"/>
<wire x1="87.0752" y1="26.648190625" x2="86.99275" y2="26.656371875" width="0.225" layer="94" curve="20.527762"/>
<wire x1="86.99275" y1="26.65636875" x2="86.9651" y2="26.653715625" width="0.225" layer="94" curve="1.764695"/>
<wire x1="86.9651" y1="26.65371875" x2="86.912034375" y2="26.638084375" width="0.225" layer="94" curve="20.107123"/>
<wire x1="86.91203125" y1="26.638090625" x2="86.870678125" y2="26.601821875" width="0.225" layer="94" curve="29.565942"/>
<wire x1="86.87068125" y1="26.60181875" x2="86.84560625" y2="26.55240625" width="0.225" layer="94" curve="14.114289"/>
<wire x1="86.8456" y1="26.552409375" x2="86.824978125" y2="26.471759375" width="0.225" layer="94" curve="11.013017"/>
<wire x1="86.82498125" y1="26.471759375" x2="86.813053125" y2="26.36131875" width="0.225" layer="94" curve="5.344301"/>
<wire x1="86.81305" y1="26.36131875" x2="86.810090625" y2="26.25023125" width="0.225" layer="94" curve="3.932748"/>
<wire x1="83.18028125" y1="26.30266875" x2="83.18028125" y2="26.414" width="0.225" layer="94"/>
<wire x1="83.18028125" y1="26.414" x2="83.18028125" y2="26.487240625" width="0.225" layer="94"/>
<wire x1="83.18028125" y1="26.487240625" x2="83.176690625" y2="26.56661875" width="0.225" layer="94" curve="5.181151"/>
<wire x1="83.176690625" y1="26.56661875" x2="83.16605625" y2="26.625190625" width="0.225" layer="94" curve="10.220705"/>
<wire x1="83.166059375" y1="26.625190625" x2="83.12728125" y2="26.664134375" width="0.225" layer="94" curve="58.95127"/>
<wire x1="83.12728125" y1="26.66413125" x2="82.99755" y2="26.639" width="0.225" layer="94" curve="53.217192"/>
<wire x1="82.99755" y1="26.639" x2="82.95845" y2="26.593540625" width="0.225" layer="94" curve="23.452395"/>
<wire x1="82.95845" y1="26.593540625" x2="82.95161875" y2="26.574890625" width="0.225" layer="94"/>
<wire x1="82.95161875" y1="26.574890625" x2="82.946159375" y2="26.55578125" width="0.225" layer="94"/>
<wire x1="82.946159375" y1="26.55578125" x2="82.941775" y2="26.5364" width="0.225" layer="94" curve="6.402047"/>
<wire x1="82.94176875" y1="26.5364" x2="82.931515625" y2="26.45761875" width="0.225" layer="94" curve="4.264451"/>
<wire x1="82.93151875" y1="26.45761875" x2="82.930209375" y2="26.437790625" width="0.225" layer="94"/>
<wire x1="82.930209375" y1="26.437790625" x2="82.92865" y2="26.39808125" width="0.225" layer="94"/>
<wire x1="82.92865" y1="26.39808125" x2="82.92818125" y2="26.358340625" width="0.225" layer="94"/>
<wire x1="92.67961875" y1="26.22455" x2="92.54725" y2="26.31605" width="0.225" layer="94" curve="24.406928"/>
<wire x1="92.54725" y1="26.31605" x2="92.391190625" y2="26.358428125" width="0.225" layer="94" curve="14.517731"/>
<wire x1="92.391190625" y1="26.35841875" x2="92.2218" y2="26.3835" width="0.225" layer="94"/>
<wire x1="92.2218" y1="26.3835" x2="92.13726875" y2="26.39681875" width="0.225" layer="94"/>
<wire x1="92.09533125" y1="26.40531875" x2="92.13726875" y2="26.39681875" width="0.225" layer="94" curve="5.000072"/>
<wire x1="92.0546" y1="26.41821875" x2="92.0661" y2="26.414" width="0.225" layer="94" curve="2.068384"/>
<wire x1="92.0661" y1="26.414" x2="92.09533125" y2="26.405321875" width="0.225" layer="94" curve="5.150249"/>
<wire x1="92.054009375" y1="26.430409375" x2="92.054596875" y2="26.41821875" width="0.225" layer="94" curve="5.514941"/>
<wire x1="92.054009375" y1="26.430409375" x2="92.054009375" y2="26.57718125" width="0.225" layer="94"/>
<wire x1="92.05665" y1="26.61703125" x2="92.0540125" y2="26.57718125" width="0.225" layer="94" curve="7.573746"/>
<wire x1="92.061659375" y1="26.63633125" x2="92.056646875" y2="26.61703125" width="0.225" layer="94" curve="13.971436"/>
<wire x1="92.068809375" y1="26.642740625" x2="92.0616625" y2="26.636328125" width="0.225" layer="94" curve="41.625207"/>
<wire x1="92.097790625" y1="26.650090625" x2="92.068809375" y2="26.642740625" width="0.225" layer="94" curve="13.721902"/>
<wire x1="92.15758125" y1="26.653940625" x2="92.097790625" y2="26.650090625" width="0.225" layer="94" curve="7.371099"/>
<wire x1="92.15758125" y1="26.653940625" x2="92.2566" y2="26.653940625" width="0.225" layer="94"/>
<wire x1="92.29483125" y1="26.652240625" x2="92.2566" y2="26.653940625" width="0.225" layer="94" curve="5.089448"/>
<wire x1="92.32143125" y1="26.647590625" x2="92.29483125" y2="26.652240625" width="0.225" layer="94" curve="9.654072"/>
<wire x1="92.331859375" y1="26.64346875" x2="92.32143125" y2="26.6475875" width="0.225" layer="94" curve="13.639596"/>
<wire x1="92.33591875" y1="26.63303125" x2="92.33185625" y2="26.64346875" width="0.225" layer="94" curve="15.08669"/>
<wire x1="92.34036875" y1="26.6065" x2="92.335915625" y2="26.63303125" width="0.225" layer="94" curve="8.390546"/>
<wire x1="92.34215" y1="26.568390625" x2="92.340375" y2="26.6065" width="0.225" layer="94" curve="5.332675"/>
<wire x1="92.34215" y1="26.568390625" x2="92.34215" y2="26.414" width="0.225" layer="94"/>
<wire x1="92.34215" y1="26.414" x2="92.34215" y2="26.413709375" width="0.225" layer="94"/>
<wire x1="92.34215" y1="26.413709375" x2="92.759190625" y2="26.413709375" width="0.225" layer="94"/>
<wire x1="92.759190625" y1="26.413709375" x2="92.759190625" y2="26.414" width="0.225" layer="94"/>
<wire x1="92.759190625" y1="26.414" x2="92.759190625" y2="26.5892" width="0.225" layer="94"/>
<wire x1="92.759190625" y1="26.5892" x2="92.738496875" y2="26.78398125" width="0.225" layer="94" curve="12.129007"/>
<wire x1="92.738490625" y1="26.78398125" x2="92.642896875" y2="26.952528125" width="0.225" layer="94" curve="34.862523"/>
<wire x1="92.469709375" y1="27.040159375" x2="92.27461875" y2="27.058825" width="0.225" layer="94" curve="10.407762"/>
<wire x1="92.27461875" y1="27.05881875" x2="92.139559375" y2="27.05881875" width="0.225" layer="94"/>
<wire x1="92.139559375" y1="27.05881875" x2="91.94439375" y2="27.0367625" width="0.225" layer="94" curve="15.066235"/>
<wire x1="91.944390625" y1="27.03676875" x2="91.76905625" y2="26.950671875" width="0.225" layer="94" curve="24.344315"/>
<wire x1="91.769059375" y1="26.95066875" x2="91.656853125" y2="26.79153125" width="0.225" layer="94" curve="32.9752"/>
<wire x1="91.65685" y1="26.79153125" x2="91.626409375" y2="26.639" width="0.225" layer="94" curve="14.82531"/>
<wire x1="91.626409375" y1="26.639" x2="91.62503125" y2="26.59828125" width="0.225" layer="94" curve="3.873269"/>
<wire x1="91.62503125" y1="26.59828125" x2="91.62503125" y2="26.414" width="0.225" layer="94"/>
<wire x1="91.62503125" y1="26.414" x2="91.62503125" y2="26.39436875" width="0.225" layer="94"/>
<wire x1="91.62503125" y1="26.39436875" x2="91.654403125" y2="26.22111875" width="0.225" layer="94" curve="19.244585"/>
<wire x1="91.6544" y1="26.22111875" x2="91.767428125" y2="26.088775" width="0.225" layer="94" curve="42.510013"/>
<wire x1="91.76743125" y1="26.08878125" x2="91.931890625" y2="26.02583125" width="0.225" layer="94" curve="14.601117"/>
<wire x1="91.931890625" y1="26.02583125" x2="92.105790625" y2="25.992246875" width="0.225" layer="94" curve="5.428189"/>
<wire x1="92.105790625" y1="25.99225" x2="92.16531875" y2="25.98243125" width="0.225" layer="94"/>
<wire x1="92.16531875" y1="25.98243125" x2="92.22491875" y2="25.97266875" width="0.225" layer="94"/>
<wire x1="92.22491875" y1="25.97266875" x2="92.28428125" y2="25.96156875" width="0.225" layer="94"/>
<wire x1="92.327340625" y1="25.94943125" x2="92.28428125" y2="25.9615625" width="0.225" layer="94" curve="10.274433"/>
<wire x1="92.34125" y1="25.94243125" x2="92.32734375" y2="25.9494375" width="0.225" layer="94" curve="11.730311"/>
<wire x1="92.346959375" y1="25.91516875" x2="92.34124375" y2="25.942428125" width="0.225" layer="94" curve="13.906979"/>
<wire x1="92.34815" y1="25.887240625" x2="92.346959375" y2="25.91516875" width="0.225" layer="94" curve="4.888175"/>
<wire x1="92.34815" y1="25.887240625" x2="92.34815" y2="25.7431" width="0.225" layer="94"/>
<wire x1="92.346759375" y1="25.71286875" x2="92.348153125" y2="25.7431" width="0.225" layer="94" curve="5.283856"/>
<wire x1="92.34045" y1="25.683359375" x2="92.34675625" y2="25.71286875" width="0.225" layer="94" curve="13.55484"/>
<wire x1="92.333959375" y1="25.67608125" x2="92.340453125" y2="25.683359375" width="0.225" layer="94" curve="45.791754"/>
<wire x1="92.314840625" y1="25.66976875" x2="92.333959375" y2="25.676084375" width="0.225" layer="94" curve="14.183972"/>
<wire x1="92.274840625" y1="25.66463125" x2="92.314840625" y2="25.669771875" width="0.225" layer="94" curve="7.7277"/>
<wire x1="92.24416875" y1="25.663709375" x2="92.274840625" y2="25.664628125" width="0.225" layer="94" curve="3.484382"/>
<wire x1="92.24416875" y1="25.663709375" x2="92.1096" y2="25.663709375" width="0.225" layer="94"/>
<wire x1="92.07173125" y1="25.66515" x2="92.1096" y2="25.66370625" width="0.225" layer="94" curve="4.363033"/>
<wire x1="92.04318125" y1="25.66956875" x2="92.07173125" y2="25.66514375" width="0.225" layer="94" curve="8.889144"/>
<wire x1="92.03463125" y1="25.672309375" x2="92.04318125" y2="25.66956875" width="0.225" layer="94" curve="9.043139"/>
<wire x1="92.03186875" y1="25.680859375" x2="92.034628125" y2="25.672309375" width="0.225" layer="94" curve="7.986668"/>
<wire x1="92.02805" y1="25.70145" x2="92.031871875" y2="25.680859375" width="0.225" layer="94" curve="6.761788"/>
<wire x1="92.02548125" y1="25.73128125" x2="92.028053125" y2="25.70145" width="0.225" layer="94" curve="4.406702"/>
<wire x1="92.024059375" y1="25.79115" x2="92.025484375" y2="25.73128125" width="0.225" layer="94" curve="2.724283"/>
<wire x1="92.024059375" y1="25.79115" x2="92.024059375" y2="25.964" width="0.225" layer="94"/>
<wire x1="92.024059375" y1="25.964" x2="92.024059375" y2="25.99651875" width="0.225" layer="94"/>
<wire x1="92.024059375" y1="25.99651875" x2="91.60701875" y2="25.99651875" width="0.225" layer="94"/>
<wire x1="91.60701875" y1="25.99651875" x2="91.60701875" y2="25.964" width="0.225" layer="94"/>
<wire x1="91.60701875" y1="25.964" x2="91.60701875" y2="25.739" width="0.225" layer="94"/>
<wire x1="91.60701875" y1="25.739" x2="91.60701875" y2="25.728159375" width="0.225" layer="94"/>
<wire x1="91.60701875" y1="25.728159375" x2="91.6277125" y2="25.53338125" width="0.225" layer="94" curve="12.128778"/>
<wire x1="91.627709375" y1="25.53338125" x2="91.72330625" y2="25.364828125" width="0.225" layer="94" curve="34.863434"/>
<wire x1="91.723309375" y1="25.36483125" x2="91.85098125" y2="25.289" width="0.225" layer="94" curve="24.602823"/>
<wire x1="91.85098125" y1="25.289" x2="91.896490625" y2="25.277203125" width="0.225" layer="94" curve="7.735491"/>
<wire x1="91.896490625" y1="25.2772" x2="92.091590625" y2="25.258534375" width="0.225" layer="94" curve="10.40831"/>
<wire x1="92.091590625" y1="25.25853125" x2="92.262609375" y2="25.25853125" width="0.225" layer="94"/>
<wire x1="92.262609375" y1="25.25853125" x2="92.457790625" y2="25.2805875" width="0.225" layer="94" curve="15.067174"/>
<wire x1="92.457790625" y1="25.280590625" x2="92.633140625" y2="25.36668125" width="0.225" layer="94" curve="24.336812"/>
<wire x1="92.633140625" y1="25.36668125" x2="92.74113125" y2="25.514" width="0.225" layer="94" curve="30.878359"/>
<wire x1="92.74113125" y1="25.514" x2="92.74536875" y2="25.525809375" width="0.225" layer="94" curve="2.095521"/>
<wire x1="92.74536875" y1="25.525809375" x2="92.777203125" y2="25.71908125" width="0.225" layer="94" curve="18.707757"/>
<wire x1="92.777209375" y1="25.71908125" x2="92.777209375" y2="25.739" width="0.225" layer="94"/>
<wire x1="92.777209375" y1="25.739" x2="92.777209375" y2="25.92328125" width="0.225" layer="94"/>
<wire x1="92.777209375" y1="25.92328125" x2="92.756978125" y2="26.083621875" width="0.225" layer="94" curve="14.383055"/>
<wire x1="92.75696875" y1="26.08361875" x2="92.6796125" y2="26.22454375" width="0.225" layer="94" curve="28.759193"/>
<wire x1="64.9221" y1="37.86731875" x2="101.4471" y2="37.86731875" width="0.225" layer="94"/>
<wire x1="101.4471" y1="37.86731875" x2="101.4471" y2="37.664" width="0.225" layer="94"/>
<wire x1="101.4471" y1="37.664" x2="101.4471" y2="37.439" width="0.225" layer="94"/>
<wire x1="101.4471" y1="37.439" x2="101.4471" y2="37.214" width="0.225" layer="94"/>
<wire x1="101.4471" y1="37.214" x2="101.4471" y2="36.989" width="0.225" layer="94"/>
<wire x1="101.4471" y1="36.989" x2="101.4471" y2="36.764" width="0.225" layer="94"/>
<wire x1="101.4471" y1="36.764" x2="101.4471" y2="36.539" width="0.225" layer="94"/>
<wire x1="101.4471" y1="36.539" x2="101.4471" y2="36.314" width="0.225" layer="94"/>
<wire x1="101.4471" y1="36.314" x2="101.4471" y2="36.089" width="0.225" layer="94"/>
<wire x1="101.4471" y1="36.089" x2="101.4471" y2="35.864" width="0.225" layer="94"/>
<wire x1="101.4471" y1="35.864" x2="101.4471" y2="35.639" width="0.225" layer="94"/>
<wire x1="101.4471" y1="35.639" x2="101.4471" y2="35.414" width="0.225" layer="94"/>
<wire x1="101.4471" y1="35.414" x2="101.4471" y2="35.189" width="0.225" layer="94"/>
<wire x1="101.4471" y1="35.189" x2="101.4471" y2="34.964" width="0.225" layer="94"/>
<wire x1="101.4471" y1="34.964" x2="101.4471" y2="34.739" width="0.225" layer="94"/>
<wire x1="101.4471" y1="34.739" x2="101.4471" y2="34.514" width="0.225" layer="94"/>
<wire x1="101.4471" y1="34.514" x2="101.4471" y2="34.289" width="0.225" layer="94"/>
<wire x1="101.4471" y1="34.289" x2="101.4471" y2="34.064" width="0.225" layer="94"/>
<wire x1="101.4471" y1="34.064" x2="101.4471" y2="33.839" width="0.225" layer="94"/>
<wire x1="101.4471" y1="33.839" x2="101.4471" y2="33.614" width="0.225" layer="94"/>
<wire x1="101.4471" y1="33.614" x2="101.4471" y2="33.389" width="0.225" layer="94"/>
<wire x1="101.4471" y1="33.389" x2="101.4471" y2="33.164" width="0.225" layer="94"/>
<wire x1="101.4471" y1="33.164" x2="101.4471" y2="32.939" width="0.225" layer="94"/>
<wire x1="101.4471" y1="32.939" x2="101.4471" y2="32.714" width="0.225" layer="94"/>
<wire x1="101.4471" y1="32.714" x2="101.4471" y2="32.489" width="0.225" layer="94"/>
<wire x1="101.4471" y1="32.489" x2="101.4471" y2="32.264" width="0.225" layer="94"/>
<wire x1="101.4471" y1="32.264" x2="101.4471" y2="32.039" width="0.225" layer="94"/>
<wire x1="101.4471" y1="32.039" x2="101.4471" y2="31.814" width="0.225" layer="94"/>
<wire x1="101.4471" y1="31.814" x2="101.4471" y2="31.589" width="0.225" layer="94"/>
<wire x1="101.4471" y1="31.589" x2="101.4471" y2="31.364" width="0.225" layer="94"/>
<wire x1="101.4471" y1="31.364" x2="101.4471" y2="31.139" width="0.225" layer="94"/>
<wire x1="101.4471" y1="31.139" x2="101.4471" y2="30.914" width="0.225" layer="94"/>
<wire x1="101.4471" y1="30.914" x2="101.4471" y2="30.689" width="0.225" layer="94"/>
<wire x1="101.4471" y1="30.689" x2="101.4471" y2="30.464" width="0.225" layer="94"/>
<wire x1="101.4471" y1="30.464" x2="101.4471" y2="30.239" width="0.225" layer="94"/>
<wire x1="101.4471" y1="30.239" x2="101.4471" y2="30.014" width="0.225" layer="94"/>
<wire x1="101.4471" y1="30.014" x2="101.4471" y2="29.789" width="0.225" layer="94"/>
<wire x1="101.4471" y1="29.789" x2="101.4471" y2="29.564" width="0.225" layer="94"/>
<wire x1="101.4471" y1="29.564" x2="101.4471" y2="29.339" width="0.225" layer="94"/>
<wire x1="101.4471" y1="29.339" x2="101.4471" y2="29.114" width="0.225" layer="94"/>
<wire x1="101.4471" y1="29.114" x2="101.4471" y2="28.889" width="0.225" layer="94"/>
<wire x1="101.4471" y1="28.889" x2="101.4471" y2="28.664" width="0.225" layer="94"/>
<wire x1="101.4471" y1="28.664" x2="101.4471" y2="28.439" width="0.225" layer="94"/>
<wire x1="101.4471" y1="28.439" x2="101.4471" y2="28.214" width="0.225" layer="94"/>
<wire x1="101.4471" y1="28.214" x2="101.4471" y2="27.989" width="0.225" layer="94"/>
<wire x1="101.4471" y1="27.989" x2="101.4471" y2="27.764" width="0.225" layer="94"/>
<wire x1="101.4471" y1="27.764" x2="101.4471" y2="27.539" width="0.225" layer="94"/>
<wire x1="101.4471" y1="27.539" x2="101.4471" y2="27.314" width="0.225" layer="94"/>
<wire x1="101.4471" y1="27.314" x2="101.4471" y2="27.089" width="0.225" layer="94"/>
<wire x1="101.4471" y1="27.089" x2="101.4471" y2="26.864" width="0.225" layer="94"/>
<wire x1="101.4471" y1="26.864" x2="101.4471" y2="26.639" width="0.225" layer="94"/>
<wire x1="101.4471" y1="26.639" x2="101.4471" y2="26.414" width="0.225" layer="94"/>
<wire x1="101.4471" y1="26.414" x2="101.4471" y2="26.189" width="0.225" layer="94"/>
<wire x1="101.4471" y1="26.189" x2="101.4471" y2="25.964" width="0.225" layer="94"/>
<wire x1="101.4471" y1="25.964" x2="101.4471" y2="25.739" width="0.225" layer="94"/>
<wire x1="101.4471" y1="25.739" x2="101.4471" y2="25.514" width="0.225" layer="94"/>
<wire x1="101.4471" y1="25.514" x2="101.4471" y2="25.289" width="0.225" layer="94"/>
<wire x1="101.4471" y1="25.289" x2="101.4471" y2="25.064" width="0.225" layer="94"/>
<wire x1="101.4471" y1="25.064" x2="101.4471" y2="24.839" width="0.225" layer="94"/>
<wire x1="101.4471" y1="24.839" x2="101.4471" y2="24.614" width="0.225" layer="94"/>
<wire x1="101.4471" y1="24.614" x2="101.4471" y2="24.389" width="0.225" layer="94"/>
<wire x1="101.4471" y1="24.389" x2="101.4471" y2="24.164" width="0.225" layer="94"/>
<wire x1="101.4471" y1="24.164" x2="101.4471" y2="23.939" width="0.225" layer="94"/>
<wire x1="101.4471" y1="23.939" x2="101.4471" y2="23.714" width="0.225" layer="94"/>
<wire x1="101.4471" y1="23.714" x2="101.4471" y2="23.489" width="0.225" layer="94"/>
<wire x1="101.4471" y1="23.489" x2="101.4471" y2="23.264" width="0.225" layer="94"/>
<wire x1="101.4471" y1="23.264" x2="101.4471" y2="23.09231875" width="0.225" layer="94"/>
<wire x1="101.4471" y1="23.09231875" x2="64.9221" y2="23.09231875" width="0.225" layer="94"/>
<wire x1="64.9221" y1="23.09231875" x2="64.9221" y2="23.264" width="0.225" layer="94"/>
<wire x1="64.9221" y1="23.264" x2="64.9221" y2="23.489" width="0.225" layer="94"/>
<wire x1="64.9221" y1="23.489" x2="64.9221" y2="23.714" width="0.225" layer="94"/>
<wire x1="64.9221" y1="23.714" x2="64.9221" y2="23.939" width="0.225" layer="94"/>
<wire x1="64.9221" y1="23.939" x2="64.9221" y2="24.164" width="0.225" layer="94"/>
<wire x1="64.9221" y1="24.164" x2="64.9221" y2="24.389" width="0.225" layer="94"/>
<wire x1="64.9221" y1="24.389" x2="64.9221" y2="24.614" width="0.225" layer="94"/>
<wire x1="64.9221" y1="24.614" x2="64.9221" y2="24.839" width="0.225" layer="94"/>
<wire x1="64.9221" y1="24.839" x2="64.9221" y2="25.064" width="0.225" layer="94"/>
<wire x1="64.9221" y1="25.064" x2="64.9221" y2="25.289" width="0.225" layer="94"/>
<wire x1="64.9221" y1="25.289" x2="64.9221" y2="25.514" width="0.225" layer="94"/>
<wire x1="64.9221" y1="25.514" x2="64.9221" y2="25.739" width="0.225" layer="94"/>
<wire x1="64.9221" y1="25.739" x2="64.9221" y2="25.964" width="0.225" layer="94"/>
<wire x1="64.9221" y1="25.964" x2="64.9221" y2="26.189" width="0.225" layer="94"/>
<wire x1="64.9221" y1="26.189" x2="64.9221" y2="26.414" width="0.225" layer="94"/>
<wire x1="64.9221" y1="26.414" x2="64.9221" y2="26.639" width="0.225" layer="94"/>
<wire x1="64.9221" y1="26.639" x2="64.9221" y2="26.864" width="0.225" layer="94"/>
<wire x1="64.9221" y1="26.864" x2="64.9221" y2="27.089" width="0.225" layer="94"/>
<wire x1="64.9221" y1="27.089" x2="64.9221" y2="27.314" width="0.225" layer="94"/>
<wire x1="64.9221" y1="27.314" x2="64.9221" y2="27.539" width="0.225" layer="94"/>
<wire x1="64.9221" y1="27.539" x2="64.9221" y2="27.764" width="0.225" layer="94"/>
<wire x1="64.9221" y1="27.764" x2="64.9221" y2="27.989" width="0.225" layer="94"/>
<wire x1="64.9221" y1="27.989" x2="64.9221" y2="28.214" width="0.225" layer="94"/>
<wire x1="64.9221" y1="28.214" x2="64.9221" y2="28.439" width="0.225" layer="94"/>
<wire x1="64.9221" y1="28.439" x2="64.9221" y2="28.664" width="0.225" layer="94"/>
<wire x1="64.9221" y1="28.664" x2="64.9221" y2="28.889" width="0.225" layer="94"/>
<wire x1="64.9221" y1="28.889" x2="64.9221" y2="29.114" width="0.225" layer="94"/>
<wire x1="64.9221" y1="29.114" x2="64.9221" y2="29.339" width="0.225" layer="94"/>
<wire x1="64.9221" y1="29.339" x2="64.9221" y2="29.564" width="0.225" layer="94"/>
<wire x1="64.9221" y1="29.564" x2="64.9221" y2="29.789" width="0.225" layer="94"/>
<wire x1="64.9221" y1="29.789" x2="64.9221" y2="30.014" width="0.225" layer="94"/>
<wire x1="64.9221" y1="30.014" x2="64.9221" y2="30.239" width="0.225" layer="94"/>
<wire x1="64.9221" y1="30.239" x2="64.9221" y2="30.464" width="0.225" layer="94"/>
<wire x1="64.9221" y1="30.464" x2="64.9221" y2="30.689" width="0.225" layer="94"/>
<wire x1="64.9221" y1="30.689" x2="64.9221" y2="30.914" width="0.225" layer="94"/>
<wire x1="64.9221" y1="30.914" x2="64.9221" y2="31.139" width="0.225" layer="94"/>
<wire x1="64.9221" y1="31.139" x2="64.9221" y2="31.364" width="0.225" layer="94"/>
<wire x1="64.9221" y1="31.364" x2="64.9221" y2="31.589" width="0.225" layer="94"/>
<wire x1="64.9221" y1="31.589" x2="64.9221" y2="31.814" width="0.225" layer="94"/>
<wire x1="64.9221" y1="31.814" x2="64.9221" y2="32.039" width="0.225" layer="94"/>
<wire x1="64.9221" y1="32.039" x2="64.9221" y2="32.264" width="0.225" layer="94"/>
<wire x1="64.9221" y1="32.264" x2="64.9221" y2="32.489" width="0.225" layer="94"/>
<wire x1="64.9221" y1="32.489" x2="64.9221" y2="32.714" width="0.225" layer="94"/>
<wire x1="64.9221" y1="32.714" x2="64.9221" y2="32.939" width="0.225" layer="94"/>
<wire x1="64.9221" y1="32.939" x2="64.9221" y2="33.164" width="0.225" layer="94"/>
<wire x1="64.9221" y1="33.164" x2="64.9221" y2="33.389" width="0.225" layer="94"/>
<wire x1="64.9221" y1="33.389" x2="64.9221" y2="33.614" width="0.225" layer="94"/>
<wire x1="64.9221" y1="33.614" x2="64.9221" y2="33.839" width="0.225" layer="94"/>
<wire x1="64.9221" y1="33.839" x2="64.9221" y2="34.064" width="0.225" layer="94"/>
<wire x1="64.9221" y1="34.064" x2="64.9221" y2="34.289" width="0.225" layer="94"/>
<wire x1="64.9221" y1="34.289" x2="64.9221" y2="34.514" width="0.225" layer="94"/>
<wire x1="64.9221" y1="34.514" x2="64.9221" y2="34.739" width="0.225" layer="94"/>
<wire x1="64.9221" y1="34.739" x2="64.9221" y2="34.964" width="0.225" layer="94"/>
<wire x1="64.9221" y1="34.964" x2="64.9221" y2="35.189" width="0.225" layer="94"/>
<wire x1="64.9221" y1="35.189" x2="64.9221" y2="35.414" width="0.225" layer="94"/>
<wire x1="64.9221" y1="35.414" x2="64.9221" y2="35.639" width="0.225" layer="94"/>
<wire x1="64.9221" y1="35.639" x2="64.9221" y2="35.864" width="0.225" layer="94"/>
<wire x1="64.9221" y1="35.864" x2="64.9221" y2="36.089" width="0.225" layer="94"/>
<wire x1="64.9221" y1="36.089" x2="64.9221" y2="36.314" width="0.225" layer="94"/>
<wire x1="64.9221" y1="36.314" x2="64.9221" y2="36.539" width="0.225" layer="94"/>
<wire x1="64.9221" y1="36.539" x2="64.9221" y2="36.764" width="0.225" layer="94"/>
<wire x1="64.9221" y1="36.764" x2="64.9221" y2="36.989" width="0.225" layer="94"/>
<wire x1="64.9221" y1="36.989" x2="64.9221" y2="37.214" width="0.225" layer="94"/>
<wire x1="64.9221" y1="37.214" x2="64.9221" y2="37.439" width="0.225" layer="94"/>
<wire x1="64.9221" y1="37.439" x2="64.9221" y2="37.664" width="0.225" layer="94"/>
<wire x1="64.9221" y1="37.664" x2="64.9221" y2="37.86731875" width="0.225" layer="94"/>
<wire x1="64.9221" y1="37.664" x2="101.4471" y2="37.664" width="0.225" layer="94"/>
<wire x1="64.9221" y1="37.439" x2="101.4471" y2="37.439" width="0.225" layer="94"/>
<wire x1="64.9221" y1="37.214" x2="101.4471" y2="37.214" width="0.225" layer="94"/>
<wire x1="64.9221" y1="36.989" x2="101.4471" y2="36.989" width="0.225" layer="94"/>
<wire x1="64.9221" y1="36.764" x2="101.4471" y2="36.764" width="0.225" layer="94"/>
<wire x1="64.9221" y1="36.539" x2="101.4471" y2="36.539" width="0.225" layer="94"/>
<wire x1="64.9221" y1="36.314" x2="101.4471" y2="36.314" width="0.225" layer="94"/>
<wire x1="64.9221" y1="36.089" x2="101.4471" y2="36.089" width="0.225" layer="94"/>
<wire x1="64.9221" y1="35.864" x2="101.4471" y2="35.864" width="0.225" layer="94"/>
<wire x1="64.9221" y1="35.639" x2="101.4471" y2="35.639" width="0.225" layer="94"/>
<wire x1="64.9221" y1="35.414" x2="101.4471" y2="35.414" width="0.225" layer="94"/>
<wire x1="64.9221" y1="35.189" x2="101.4471" y2="35.189" width="0.225" layer="94"/>
<wire x1="64.9221" y1="34.964" x2="69.81928125" y2="34.964" width="0.225" layer="94"/>
<wire x1="76.07733125" y1="34.964" x2="101.4471" y2="34.964" width="0.225" layer="94"/>
<wire x1="69.89371875" y1="34.964" x2="76.0029" y2="34.964" width="0.225" layer="94"/>
<wire x1="64.9221" y1="34.739" x2="69.570590625" y2="34.739" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="34.739" x2="101.4471" y2="34.739" width="0.225" layer="94"/>
<wire x1="70.097190625" y1="34.739" x2="75.79943125" y2="34.739" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="34.739" x2="90.66211875" y2="34.739" width="0.225" layer="94"/>
<wire x1="76.326109375" y1="34.739" x2="78.286090625" y2="34.739" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="34.739" x2="87.068640625" y2="34.739" width="0.225" layer="94"/>
<wire x1="64.9221" y1="34.514" x2="69.3219" y2="34.514" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="34.514" x2="101.4471" y2="34.514" width="0.225" layer="94"/>
<wire x1="70.30066875" y1="34.514" x2="75.595959375" y2="34.514" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="34.514" x2="90.66211875" y2="34.514" width="0.225" layer="94"/>
<wire x1="76.574890625" y1="34.514" x2="78.286090625" y2="34.514" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="34.514" x2="87.068640625" y2="34.514" width="0.225" layer="94"/>
<wire x1="64.9221" y1="34.289" x2="69.073209375" y2="34.289" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="34.289" x2="101.4471" y2="34.289" width="0.225" layer="94"/>
<wire x1="70.504140625" y1="34.289" x2="75.3925" y2="34.289" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="34.289" x2="90.66211875" y2="34.289" width="0.225" layer="94"/>
<wire x1="76.82366875" y1="34.289" x2="78.286090625" y2="34.289" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="34.289" x2="87.068640625" y2="34.289" width="0.225" layer="94"/>
<wire x1="64.9221" y1="34.064" x2="68.82451875" y2="34.064" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="34.064" x2="101.4471" y2="34.064" width="0.225" layer="94"/>
<wire x1="70.70761875" y1="34.064" x2="75.18903125" y2="34.064" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="34.064" x2="90.66211875" y2="34.064" width="0.225" layer="94"/>
<wire x1="77.07245" y1="34.064" x2="78.286090625" y2="34.064" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="34.064" x2="87.068640625" y2="34.064" width="0.225" layer="94"/>
<wire x1="64.9221" y1="33.839" x2="68.78723125" y2="33.839" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="33.839" x2="101.4471" y2="33.839" width="0.225" layer="94"/>
<wire x1="70.911090625" y1="33.839" x2="74.985559375" y2="33.839" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="33.839" x2="90.66211875" y2="33.839" width="0.225" layer="94"/>
<wire x1="77.1094" y1="33.839" x2="78.286090625" y2="33.839" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="33.839" x2="87.068640625" y2="33.839" width="0.225" layer="94"/>
<wire x1="64.9221" y1="33.614" x2="68.990709375" y2="33.614" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="33.614" x2="101.4471" y2="33.614" width="0.225" layer="94"/>
<wire x1="71.114559375" y1="33.614" x2="74.782090625" y2="33.614" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="33.614" x2="90.66211875" y2="33.614" width="0.225" layer="94"/>
<wire x1="76.905909375" y1="33.614" x2="78.286090625" y2="33.614" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="33.614" x2="87.068640625" y2="33.614" width="0.225" layer="94"/>
<wire x1="64.9221" y1="33.389" x2="69.1942" y2="33.389" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="33.389" x2="101.4471" y2="33.389" width="0.225" layer="94"/>
<wire x1="71.318040625" y1="33.389" x2="74.57861875" y2="33.389" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="33.389" x2="90.66211875" y2="33.389" width="0.225" layer="94"/>
<wire x1="76.70243125" y1="33.389" x2="78.286090625" y2="33.389" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="33.389" x2="87.068640625" y2="33.389" width="0.225" layer="94"/>
<wire x1="64.9221" y1="33.164" x2="69.397690625" y2="33.164" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="33.164" x2="101.4471" y2="33.164" width="0.225" layer="94"/>
<wire x1="71.521509375" y1="33.164" x2="74.37515" y2="33.164" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="33.164" x2="93.287040625" y2="33.164" width="0.225" layer="94"/>
<wire x1="76.49895" y1="33.164" x2="87.068640625" y2="33.164" width="0.225" layer="94"/>
<wire x1="64.9221" y1="32.939" x2="69.60116875" y2="32.939" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="32.939" x2="101.4471" y2="32.939" width="0.225" layer="94"/>
<wire x1="71.724990625" y1="32.939" x2="74.17168125" y2="32.939" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="32.939" x2="93.287040625" y2="32.939" width="0.225" layer="94"/>
<wire x1="76.29546875" y1="32.939" x2="87.068640625" y2="32.939" width="0.225" layer="94"/>
<wire x1="64.9221" y1="32.714" x2="69.804659375" y2="32.714" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="32.714" x2="101.4471" y2="32.714" width="0.225" layer="94"/>
<wire x1="71.928459375" y1="32.714" x2="73.968209375" y2="32.714" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="32.714" x2="93.287040625" y2="32.714" width="0.225" layer="94"/>
<wire x1="76.091990625" y1="32.714" x2="78.286240625" y2="32.714" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="32.714" x2="87.068640625" y2="32.714" width="0.225" layer="94"/>
<wire x1="64.9221" y1="32.489" x2="70.008140625" y2="32.489" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="32.489" x2="101.4471" y2="32.489" width="0.225" layer="94"/>
<wire x1="72.131940625" y1="32.489" x2="73.764740625" y2="32.489" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="32.489" x2="93.287040625" y2="32.489" width="0.225" layer="94"/>
<wire x1="75.8885" y1="32.489" x2="78.286240625" y2="32.489" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="32.489" x2="87.068640625" y2="32.489" width="0.225" layer="94"/>
<wire x1="64.9221" y1="32.264" x2="70.21163125" y2="32.264" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="32.264" x2="101.4471" y2="32.264" width="0.225" layer="94"/>
<wire x1="72.335409375" y1="32.264" x2="73.56126875" y2="32.264" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="32.264" x2="93.287040625" y2="32.264" width="0.225" layer="94"/>
<wire x1="75.68501875" y1="32.264" x2="78.286240625" y2="32.264" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="32.264" x2="87.068640625" y2="32.264" width="0.225" layer="94"/>
<wire x1="64.9221" y1="32.039" x2="70.415109375" y2="32.039" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="32.039" x2="101.4471" y2="32.039" width="0.225" layer="94"/>
<wire x1="72.53888125" y1="32.039" x2="73.3578" y2="32.039" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="32.039" x2="93.287040625" y2="32.039" width="0.225" layer="94"/>
<wire x1="75.481540625" y1="32.039" x2="78.286240625" y2="32.039" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="32.039" x2="87.068640625" y2="32.039" width="0.225" layer="94"/>
<wire x1="64.9221" y1="31.814" x2="70.6186" y2="31.814" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="31.814" x2="101.4471" y2="31.814" width="0.225" layer="94"/>
<wire x1="72.742359375" y1="31.814" x2="73.15433125" y2="31.814" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="31.814" x2="93.287040625" y2="31.814" width="0.225" layer="94"/>
<wire x1="75.278059375" y1="31.814" x2="78.286240625" y2="31.814" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="31.814" x2="87.068640625" y2="31.814" width="0.225" layer="94"/>
<wire x1="64.9221" y1="31.589" x2="70.822090625" y2="31.589" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="31.589" x2="101.4471" y2="31.589" width="0.225" layer="94"/>
<wire x1="72.94583125" y1="31.589" x2="72.950859375" y2="31.589" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="31.589" x2="93.287040625" y2="31.589" width="0.225" layer="94"/>
<wire x1="75.07458125" y1="31.589" x2="78.286240625" y2="31.589" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="31.589" x2="87.068640625" y2="31.589" width="0.225" layer="94"/>
<wire x1="64.9221" y1="31.364" x2="71.02556875" y2="31.364" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="31.364" x2="101.4471" y2="31.364" width="0.225" layer="94"/>
<wire x1="74.871090625" y1="31.364" x2="87.068640625" y2="31.364" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="31.364" x2="93.287040625" y2="31.364" width="0.225" layer="94"/>
<wire x1="64.9221" y1="31.139" x2="71.229059375" y2="31.139" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="31.139" x2="101.4471" y2="31.139" width="0.225" layer="94"/>
<wire x1="74.667609375" y1="31.139" x2="87.068640625" y2="31.139" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="31.139" x2="93.287040625" y2="31.139" width="0.225" layer="94"/>
<wire x1="64.9221" y1="30.914" x2="71.432540625" y2="30.914" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="30.914" x2="101.4471" y2="30.914" width="0.225" layer="94"/>
<wire x1="74.46413125" y1="30.914" x2="78.286090625" y2="30.914" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="30.914" x2="93.287040625" y2="30.914" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="30.914" x2="87.068640625" y2="30.914" width="0.225" layer="94"/>
<wire x1="64.9221" y1="30.689" x2="71.63603125" y2="30.689" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="30.689" x2="101.4471" y2="30.689" width="0.225" layer="94"/>
<wire x1="74.26065" y1="30.689" x2="78.286090625" y2="30.689" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="30.689" x2="93.287040625" y2="30.689" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="30.689" x2="87.068640625" y2="30.689" width="0.225" layer="94"/>
<wire x1="64.9221" y1="30.464" x2="71.839509375" y2="30.464" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="30.464" x2="101.4471" y2="30.464" width="0.225" layer="94"/>
<wire x1="74.057159375" y1="30.464" x2="78.286090625" y2="30.464" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="30.464" x2="93.287040625" y2="30.464" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="30.464" x2="87.068640625" y2="30.464" width="0.225" layer="94"/>
<wire x1="64.9221" y1="30.239" x2="72.043" y2="30.239" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="30.239" x2="101.4471" y2="30.239" width="0.225" layer="94"/>
<wire x1="73.85368125" y1="30.239" x2="78.286090625" y2="30.239" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="30.239" x2="93.287040625" y2="30.239" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="30.239" x2="87.068640625" y2="30.239" width="0.225" layer="94"/>
<wire x1="64.9221" y1="30.014" x2="72.246490625" y2="30.014" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="30.014" x2="101.4471" y2="30.014" width="0.225" layer="94"/>
<wire x1="73.6502" y1="30.014" x2="78.286090625" y2="30.014" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="30.014" x2="93.287040625" y2="30.014" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="30.014" x2="87.068640625" y2="30.014" width="0.225" layer="94"/>
<wire x1="64.9221" y1="29.789" x2="72.44996875" y2="29.789" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="29.789" x2="101.4471" y2="29.789" width="0.225" layer="94"/>
<wire x1="73.44671875" y1="29.789" x2="78.286090625" y2="29.789" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="29.789" x2="93.287040625" y2="29.789" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="29.789" x2="87.068640625" y2="29.789" width="0.225" layer="94"/>
<wire x1="64.9221" y1="29.564" x2="72.653459375" y2="29.564" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="29.564" x2="101.4471" y2="29.564" width="0.225" layer="94"/>
<wire x1="73.243240625" y1="29.564" x2="78.286090625" y2="29.564" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="29.564" x2="93.287040625" y2="29.564" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="29.564" x2="87.068640625" y2="29.564" width="0.225" layer="94"/>
<wire x1="64.9221" y1="29.339" x2="72.856940625" y2="29.339" width="0.225" layer="94"/>
<wire x1="73.03975" y1="29.339" x2="101.4471" y2="29.339" width="0.225" layer="94"/>
<wire x1="64.9221" y1="29.114" x2="101.4471" y2="29.114" width="0.225" layer="94"/>
<wire x1="64.9221" y1="28.889" x2="101.4471" y2="28.889" width="0.225" layer="94"/>
<wire x1="64.9221" y1="28.664" x2="101.4471" y2="28.664" width="0.225" layer="94"/>
<wire x1="64.9221" y1="28.439" x2="101.4471" y2="28.439" width="0.225" layer="94"/>
<wire x1="64.9221" y1="28.214" x2="101.4471" y2="28.214" width="0.225" layer="94"/>
<wire x1="64.9221" y1="27.989" x2="101.4471" y2="27.989" width="0.225" layer="94"/>
<wire x1="64.9221" y1="27.764" x2="101.4471" y2="27.764" width="0.225" layer="94"/>
<wire x1="64.9221" y1="27.539" x2="75.49116875" y2="27.539" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="27.539" x2="101.4471" y2="27.539" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="27.539" x2="88.41505" y2="27.539" width="0.225" layer="94"/>
<wire x1="64.9221" y1="27.314" x2="75.49116875" y2="27.314" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="27.314" x2="101.4471" y2="27.314" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="27.314" x2="80.786140625" y2="27.314" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="27.314" x2="88.41505" y2="27.314" width="0.225" layer="94"/>
<wire x1="64.9221" y1="27.089" x2="75.49116875" y2="27.089" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="27.089" x2="101.4471" y2="27.089" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="27.089" x2="80.786140625" y2="27.089" width="0.225" layer="94"/>
<wire x1="64.9221" y1="26.864" x2="73.579059375" y2="26.864" width="0.225" layer="94"/>
<wire x1="92.70988125" y1="26.864" x2="101.4471" y2="26.864" width="0.225" layer="94"/>
<wire x1="74.56438125" y1="26.864" x2="75.49116875" y2="26.864" width="0.225" layer="94"/>
<wire x1="90.74325" y1="26.864" x2="91.69128125" y2="26.864" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="26.864" x2="76.864040625" y2="26.864" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="26.864" x2="89.78075" y2="26.864" width="0.225" layer="94"/>
<wire x1="77.84936875" y1="26.864" x2="78.869559375" y2="26.864" width="0.225" layer="94"/>
<wire x1="87.470340625" y1="26.864" x2="88.41505" y2="26.864" width="0.225" layer="94"/>
<wire x1="79.832359375" y1="26.864" x2="80.627059375" y2="26.864" width="0.225" layer="94"/>
<wire x1="85.42721875" y1="26.864" x2="86.39005" y2="26.864" width="0.225" layer="94"/>
<wire x1="81.7013" y1="26.864" x2="82.508140625" y2="26.864" width="0.225" layer="94"/>
<wire x1="83.522" y1="26.864" x2="84.441909375" y2="26.864" width="0.225" layer="94"/>
<wire x1="64.9221" y1="26.639" x2="73.48298125" y2="26.639" width="0.225" layer="94"/>
<wire x1="92.75785" y1="26.639" x2="101.4471" y2="26.639" width="0.225" layer="94"/>
<wire x1="73.958459375" y1="26.639" x2="74.19638125" y2="26.639" width="0.225" layer="94"/>
<wire x1="92.06346875" y1="26.639" x2="92.334359375" y2="26.639" width="0.225" layer="94"/>
<wire x1="74.659159375" y1="26.639" x2="75.49116875" y2="26.639" width="0.225" layer="94"/>
<wire x1="90.81963125" y1="26.639" x2="91.626409375" y2="26.639" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="26.639" x2="76.76796875" y2="26.639" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="26.639" x2="89.685840625" y2="26.639" width="0.225" layer="94"/>
<wire x1="77.24345" y1="26.639" x2="77.48136875" y2="26.639" width="0.225" layer="94"/>
<wire x1="87.55065" y1="26.639" x2="88.41505" y2="26.639" width="0.225" layer="94"/>
<wire x1="77.94415" y1="26.639" x2="78.77521875" y2="26.639" width="0.225" layer="94"/>
<wire x1="86.913890625" y1="26.639" x2="87.09276875" y2="26.639" width="0.225" layer="94"/>
<wire x1="79.90875" y1="26.639" x2="80.627059375" y2="26.639" width="0.225" layer="94"/>
<wire x1="85.522" y1="26.639" x2="86.39005" y2="26.639" width="0.225" layer="94"/>
<wire x1="81.7013" y1="26.639" x2="82.508140625" y2="26.639" width="0.225" layer="94"/>
<wire x1="83.58053125" y1="26.639" x2="84.345909375" y2="26.639" width="0.225" layer="94"/>
<wire x1="82.99755" y1="26.639" x2="83.160159375" y2="26.639" width="0.225" layer="94"/>
<wire x1="64.9221" y1="26.414" x2="73.45896875" y2="26.414" width="0.225" layer="94"/>
<wire x1="92.759190625" y1="26.414" x2="101.4471" y2="26.414" width="0.225" layer="94"/>
<wire x1="74.6829" y1="26.414" x2="75.49116875" y2="26.414" width="0.225" layer="94"/>
<wire x1="92.0661" y1="26.414" x2="92.34215" y2="26.414" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="26.414" x2="76.74395" y2="26.414" width="0.225" layer="94"/>
<wire x1="90.830209375" y1="26.414" x2="91.62503125" y2="26.414" width="0.225" layer="94"/>
<wire x1="77.967890625" y1="26.414" x2="78.75028125" y2="26.414" width="0.225" layer="94"/>
<wire x1="90.0934" y1="26.414" x2="90.41016875" y2="26.414" width="0.225" layer="94"/>
<wire x1="79.18205" y1="26.414" x2="79.49928125" y2="26.414" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="26.414" x2="89.661509375" y2="26.414" width="0.225" layer="94"/>
<wire x1="79.91931875" y1="26.414" x2="80.786140625" y2="26.414" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="26.414" x2="88.41505" y2="26.414" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="26.414" x2="82.508140625" y2="26.414" width="0.225" layer="94"/>
<wire x1="86.817440625" y1="26.414" x2="87.14536875" y2="26.414" width="0.225" layer="94"/>
<wire x1="82.92926875" y1="26.414" x2="83.18028125" y2="26.414" width="0.225" layer="94"/>
<wire x1="85.545740625" y1="26.414" x2="86.39005" y2="26.414" width="0.225" layer="94"/>
<wire x1="83.585309375" y1="26.414" x2="84.32161875" y2="26.414" width="0.225" layer="94"/>
<wire x1="84.754290625" y1="26.414" x2="85.11306875" y2="26.414" width="0.225" layer="94"/>
<wire x1="64.9221" y1="26.189" x2="73.457240625" y2="26.189" width="0.225" layer="94"/>
<wire x1="92.708490625" y1="26.189" x2="101.4471" y2="26.189" width="0.225" layer="94"/>
<wire x1="74.684409375" y1="26.189" x2="75.49116875" y2="26.189" width="0.225" layer="94"/>
<wire x1="90.092" y1="26.189" x2="91.66831875" y2="26.189" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="26.189" x2="76.74221875" y2="26.189" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="26.189" x2="89.66001875" y2="26.189" width="0.225" layer="94"/>
<wire x1="77.969390625" y1="26.189" x2="78.74913125" y2="26.189" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="26.189" x2="88.41505" y2="26.189" width="0.225" layer="94"/>
<wire x1="79.181109375" y1="26.189" x2="80.786140625" y2="26.189" width="0.225" layer="94"/>
<wire x1="86.810090625" y1="26.189" x2="87.1462" y2="26.189" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="26.189" x2="82.508140625" y2="26.189" width="0.225" layer="94"/>
<wire x1="85.54725" y1="26.189" x2="86.39005" y2="26.189" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="26.189" x2="84.32008125" y2="26.189" width="0.225" layer="94"/>
<wire x1="84.752059375" y1="26.189" x2="85.115190625" y2="26.189" width="0.225" layer="94"/>
<wire x1="64.9221" y1="25.964" x2="73.457090625" y2="25.964" width="0.225" layer="94"/>
<wire x1="92.77591875" y1="25.964" x2="101.4471" y2="25.964" width="0.225" layer="94"/>
<wire x1="73.88948125" y1="25.964" x2="74.27638125" y2="25.964" width="0.225" layer="94"/>
<wire x1="92.024059375" y1="25.964" x2="92.27126875" y2="25.964" width="0.225" layer="94"/>
<wire x1="74.681409375" y1="25.964" x2="75.49116875" y2="25.964" width="0.225" layer="94"/>
<wire x1="90.86316875" y1="25.964" x2="91.60701875" y2="25.964" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="25.964" x2="76.74208125" y2="25.964" width="0.225" layer="94"/>
<wire x1="90.092309375" y1="25.964" x2="90.43711875" y2="25.964" width="0.225" layer="94"/>
<wire x1="77.17446875" y1="25.964" x2="77.561359375" y2="25.964" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="25.964" x2="89.660090625" y2="25.964" width="0.225" layer="94"/>
<wire x1="77.966390625" y1="25.964" x2="78.749" y2="25.964" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="25.964" x2="88.41505" y2="25.964" width="0.225" layer="94"/>
<wire x1="79.181340625" y1="25.964" x2="79.52623125" y2="25.964" width="0.225" layer="94"/>
<wire x1="86.810090625" y1="25.964" x2="87.1462" y2="25.964" width="0.225" layer="94"/>
<wire x1="79.95228125" y1="25.964" x2="80.786140625" y2="25.964" width="0.225" layer="94"/>
<wire x1="85.54718125" y1="25.964" x2="86.39005" y2="25.964" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="25.964" x2="81.341240625" y2="25.964" width="0.225" layer="94"/>
<wire x1="84.752340625" y1="25.964" x2="85.114909375" y2="25.964" width="0.225" layer="94"/>
<wire x1="81.74326875" y1="25.964" x2="82.508140625" y2="25.964" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="25.964" x2="84.32015" y2="25.964" width="0.225" layer="94"/>
<wire x1="64.9221" y1="25.739" x2="73.47301875" y2="25.739" width="0.225" layer="94"/>
<wire x1="92.777209375" y1="25.739" x2="101.4471" y2="25.739" width="0.225" layer="94"/>
<wire x1="73.90876875" y1="25.739" x2="74.2678" y2="25.739" width="0.225" layer="94"/>
<wire x1="92.025140625" y1="25.739" x2="92.34813125" y2="25.739" width="0.225" layer="94"/>
<wire x1="74.67768125" y1="25.739" x2="75.49116875" y2="25.739" width="0.225" layer="94"/>
<wire x1="90.852559375" y1="25.739" x2="91.60701875" y2="25.739" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="25.739" x2="76.758" y2="25.739" width="0.225" layer="94"/>
<wire x1="90.11266875" y1="25.739" x2="90.420940625" y2="25.739" width="0.225" layer="94"/>
<wire x1="77.193759375" y1="25.739" x2="77.55278125" y2="25.739" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="25.739" x2="89.675959375" y2="25.739" width="0.225" layer="94"/>
<wire x1="77.962659375" y1="25.739" x2="78.76481875" y2="25.739" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="25.739" x2="88.41505" y2="25.739" width="0.225" layer="94"/>
<wire x1="79.202409375" y1="25.739" x2="79.510359375" y2="25.739" width="0.225" layer="94"/>
<wire x1="86.810090625" y1="25.739" x2="87.1462" y2="25.739" width="0.225" layer="94"/>
<wire x1="79.94166875" y1="25.739" x2="80.78728125" y2="25.739" width="0.225" layer="94"/>
<wire x1="85.531009375" y1="25.739" x2="86.39005" y2="25.739" width="0.225" layer="94"/>
<wire x1="81.209909375" y1="25.739" x2="81.340009375" y2="25.739" width="0.225" layer="94"/>
<wire x1="84.77363125" y1="25.739" x2="85.092790625" y2="25.739" width="0.225" layer="94"/>
<wire x1="81.740890625" y1="25.739" x2="82.508140625" y2="25.739" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="25.739" x2="84.336459375" y2="25.739" width="0.225" layer="94"/>
<wire x1="64.9221" y1="25.514" x2="73.542859375" y2="25.514" width="0.225" layer="94"/>
<wire x1="92.74113125" y1="25.514" x2="101.4471" y2="25.514" width="0.225" layer="94"/>
<wire x1="74.62216875" y1="25.514" x2="75.49116875" y2="25.514" width="0.225" layer="94"/>
<wire x1="90.78793125" y1="25.514" x2="91.6325" y2="25.514" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="25.514" x2="76.82785" y2="25.514" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="25.514" x2="89.74463125" y2="25.514" width="0.225" layer="94"/>
<wire x1="77.90715" y1="25.514" x2="78.834309375" y2="25.514" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="25.514" x2="88.41505" y2="25.514" width="0.225" layer="94"/>
<wire x1="79.877040625" y1="25.514" x2="80.82388125" y2="25.514" width="0.225" layer="94"/>
<wire x1="86.810090625" y1="25.514" x2="87.1462" y2="25.514" width="0.225" layer="94"/>
<wire x1="81.697240625" y1="25.514" x2="82.508140625" y2="25.514" width="0.225" layer="94"/>
<wire x1="85.46248125" y1="25.514" x2="86.39005" y2="25.514" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="25.514" x2="84.40543125" y2="25.514" width="0.225" layer="94"/>
<wire x1="64.9221" y1="25.289" x2="73.828090625" y2="25.289" width="0.225" layer="94"/>
<wire x1="92.48735" y1="25.289" x2="101.4471" y2="25.289" width="0.225" layer="94"/>
<wire x1="74.3331" y1="25.289" x2="75.49116875" y2="25.289" width="0.225" layer="94"/>
<wire x1="90.51153125" y1="25.289" x2="91.85098125" y2="25.289" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="25.289" x2="77.11308125" y2="25.289" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="25.289" x2="90.02831875" y2="25.289" width="0.225" layer="94"/>
<wire x1="77.618090625" y1="25.289" x2="79.119840625" y2="25.289" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="25.289" x2="88.41505" y2="25.289" width="0.225" layer="94"/>
<wire x1="79.60065" y1="25.289" x2="81.04181875" y2="25.289" width="0.225" layer="94"/>
<wire x1="86.810090625" y1="25.289" x2="87.1462" y2="25.289" width="0.225" layer="94"/>
<wire x1="81.460959375" y1="25.289" x2="82.508140625" y2="25.289" width="0.225" layer="94"/>
<wire x1="85.1789" y1="25.289" x2="86.39005" y2="25.289" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="25.289" x2="84.689659375" y2="25.289" width="0.225" layer="94"/>
<wire x1="64.9221" y1="25.064" x2="101.4471" y2="25.064" width="0.225" layer="94"/>
<wire x1="64.9221" y1="24.839" x2="101.4471" y2="24.839" width="0.225" layer="94"/>
<wire x1="64.9221" y1="24.614" x2="101.4471" y2="24.614" width="0.225" layer="94"/>
<wire x1="64.9221" y1="24.389" x2="101.4471" y2="24.389" width="0.225" layer="94"/>
<wire x1="64.9221" y1="24.164" x2="101.4471" y2="24.164" width="0.225" layer="94"/>
<wire x1="64.9221" y1="23.939" x2="101.4471" y2="23.939" width="0.225" layer="94"/>
<wire x1="64.9221" y1="23.714" x2="101.4471" y2="23.714" width="0.225" layer="94"/>
<wire x1="64.9221" y1="23.489" x2="101.4471" y2="23.489" width="0.225" layer="94"/>
<wire x1="64.9221" y1="23.264" x2="101.4471" y2="23.264" width="0.225" layer="94"/>
<rectangle x1="93.287040625" y1="29.45076875" x2="93.399540625" y2="33.34871875" layer="94"/>
<rectangle x1="94.749840625" y1="29.45076875" x2="94.862340625" y2="33.34871875" layer="94"/>
<rectangle x1="73.776721875" y1="26.310290625" x2="74.376859375" y2="26.422790625" layer="94"/>
<rectangle x1="77.0617" y1="26.310290625" x2="77.66185" y2="26.422790625" layer="94"/>
<rectangle x1="73.77671875" y1="26.00881875" x2="74.685" y2="26.12131875" layer="94"/>
<rectangle x1="77.0617" y1="26.00881875" x2="77.985" y2="26.12131875" layer="94"/>
<rectangle x1="81.09369375" y1="26.91936875" x2="81.7013" y2="27.314" layer="94"/>
<rectangle x1="80.627059375" y1="26.91936875" x2="80.898640625" y2="27.314" layer="94"/>
<rectangle x1="80.627059375" y1="26.319371875" x2="80.898640625" y2="26.739490625" layer="94"/>
<rectangle x1="81.09369375" y1="26.319371875" x2="81.7013" y2="26.739490625" layer="94"/>
<rectangle x1="64.77" y1="37.7825" x2="101.6" y2="38.1" layer="94"/>
<rectangle x1="101.2825" y1="22.86" x2="101.6" y2="38.1" layer="94"/>
<rectangle x1="64.77" y1="22.86" x2="65.0875" y2="38.1" layer="94"/>
<rectangle x1="64.77" y1="22.86" x2="101.6" y2="23.1775" layer="94"/>
<text x="50.8" y="12.7" size="5.08" layer="94" ratio="10" align="bottom-center">&gt;VALUE</text>
<text x="1.27" y="33.02" size="1.9304" layer="94">Vehicle Name:</text>
<text x="41.91" y="30.48" size="5.08" layer="94" align="top-center">&gt;VEHICLE_NUMBER</text>
<text x="41.91" y="33.02" size="2.54" layer="94" align="bottom-center">&gt;VEHICLE_NAME</text>
<text x="1.27" y="21.59" size="1.9304" layer="94" align="top-left">Sheet Name:</text>
<text x="1.27" y="24.13" size="1.9304" layer="94">ABRA:</text>
<text x="10.16" y="24.13" size="1.9304" layer="94">&gt;ABRA_GLOBAL</text>
</symbol>
<symbol name="DOCFIELD-AUTA-ECO-LOGO">
<wire x1="0" y1="0" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="101.6" y1="10.16" x2="87.63" y2="10.16" width="0.1016" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="71.12" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="10.16" width="0.1016" layer="94"/>
<wire x1="101.6" y1="10.16" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="0" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="87.63" y1="10.16" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="87.63" y1="10.16" x2="0" y2="10.16" width="0.1016" layer="94"/>
<wire x1="87.63" y1="5.08" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="101.6" y1="5.08" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="0" y1="10.16" x2="0" y2="22.86" width="0.1016" layer="94"/>
<wire x1="101.6" y1="38.1" x2="64.77" y2="38.1" width="0.1016" layer="94"/>
<wire x1="64.77" y1="38.1" x2="0" y2="38.1" width="0.1016" layer="94"/>
<wire x1="101.6" y1="38.1" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="64.77" y2="22.86" width="0.1016" layer="94"/>
<wire x1="64.77" y1="22.86" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="0" y2="38.1" width="0.1016" layer="94"/>
<wire x1="101.6" y1="22.86" x2="101.6" y2="10.16" width="0.1016" layer="94"/>
<text x="1.27" y="1.27" size="2.54" layer="94">Date:</text>
<text x="15.24" y="1.27" size="2.54" layer="94" font="vector">&gt;LAST_DATE_TIME</text>
<text x="72.39" y="1.27" size="2.54" layer="94">Sheet:</text>
<text x="86.36" y="1.27" size="2.54" layer="94">&gt;SHEET</text>
<text x="88.9" y="6.35" size="2.54" layer="94">REV:</text>
<text x="1.27" y="6.35" size="2.54" layer="94">TITLE:</text>
<text x="1.27" y="30.48" size="1.9304" layer="94" align="top-left">Vehicle Number:</text>
<text x="15.24" y="6.35" size="2.54" layer="94" font="vector">&gt;DRAWING_NAME</text>
<wire x1="64.77" y1="38.1" x2="64.77" y2="22.86" width="0.1016" layer="94"/>
<wire x1="97.487340625" y1="34.81106875" x2="90.66211875" y2="34.81106875" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="34.81106875" x2="90.66211875" y2="34.739" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="34.739" x2="90.66211875" y2="34.514" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="34.514" x2="90.66211875" y2="34.289" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="34.289" x2="90.66211875" y2="34.064" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="34.064" x2="90.66211875" y2="33.839" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="33.839" x2="90.66211875" y2="33.614" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="33.614" x2="90.66211875" y2="33.389" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="33.389" x2="90.66211875" y2="33.23621875" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="33.23621875" x2="93.287040625" y2="33.23621875" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="33.23621875" x2="93.287040625" y2="33.164" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="33.164" x2="93.287040625" y2="32.939" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="32.939" x2="93.287040625" y2="32.714" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="32.714" x2="93.287040625" y2="32.489" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="32.489" x2="93.287040625" y2="32.264" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="32.264" x2="93.287040625" y2="32.039" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="32.039" x2="93.287040625" y2="31.814" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="31.814" x2="93.287040625" y2="31.589" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="31.589" x2="93.287040625" y2="31.364" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="31.364" x2="93.287040625" y2="31.139" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="31.139" x2="93.287040625" y2="30.914" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="30.914" x2="93.287040625" y2="30.689" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="30.689" x2="93.287040625" y2="30.464" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="30.464" x2="93.287040625" y2="30.239" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="30.239" x2="93.287040625" y2="30.014" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="30.014" x2="93.287040625" y2="29.789" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="29.789" x2="93.287040625" y2="29.564" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="29.564" x2="93.287040625" y2="29.45076875" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="29.45076875" x2="94.862340625" y2="29.45076875" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="29.45076875" x2="94.862340625" y2="29.564" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="29.564" x2="94.862340625" y2="29.789" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="29.789" x2="94.862340625" y2="30.014" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="30.014" x2="94.862340625" y2="30.239" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="30.239" x2="94.862340625" y2="30.464" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="30.464" x2="94.862340625" y2="30.689" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="30.689" x2="94.862340625" y2="30.914" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="30.914" x2="94.862340625" y2="31.139" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="31.139" x2="94.862340625" y2="31.364" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="31.364" x2="94.862340625" y2="31.589" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="31.589" x2="94.862340625" y2="31.814" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="31.814" x2="94.862340625" y2="32.039" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="32.039" x2="94.862340625" y2="32.264" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="32.264" x2="94.862340625" y2="32.489" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="32.489" x2="94.862340625" y2="32.714" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="32.714" x2="94.862340625" y2="32.939" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="32.939" x2="94.862340625" y2="33.164" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="33.164" x2="94.862340625" y2="33.23621875" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="33.23621875" x2="97.487340625" y2="33.23621875" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="33.23621875" x2="97.487340625" y2="33.389" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="33.389" x2="97.487340625" y2="33.614" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="33.614" x2="97.487340625" y2="33.839" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="33.839" x2="97.487340625" y2="34.064" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="34.064" x2="97.487340625" y2="34.289" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="34.289" x2="97.487340625" y2="34.514" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="34.514" x2="97.487340625" y2="34.739" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="34.739" x2="97.487340625" y2="34.81106875" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="29.497059375" x2="87.068640625" y2="29.564" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="29.564" x2="87.068640625" y2="29.789" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="29.789" x2="87.068640625" y2="30.014" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="30.014" x2="87.068640625" y2="30.239" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="30.239" x2="87.068640625" y2="30.464" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="30.464" x2="87.068640625" y2="30.689" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="30.689" x2="87.068640625" y2="30.914" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="30.914" x2="87.068640625" y2="31.139" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="31.139" x2="87.068640625" y2="31.364" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="31.364" x2="87.068640625" y2="31.589" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="31.589" x2="87.068640625" y2="31.814" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="31.814" x2="87.068640625" y2="32.039" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="32.039" x2="87.068640625" y2="32.264" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="32.264" x2="87.068640625" y2="32.489" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="32.489" x2="87.068640625" y2="32.714" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="32.714" x2="87.068640625" y2="32.939" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="32.939" x2="87.068640625" y2="33.164" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="33.164" x2="87.068640625" y2="33.389" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="33.389" x2="87.068640625" y2="33.614" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="33.614" x2="87.068640625" y2="33.839" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="33.839" x2="87.068640625" y2="34.064" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="34.064" x2="87.068640625" y2="34.289" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="34.289" x2="87.068640625" y2="34.514" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="34.514" x2="87.068640625" y2="34.739" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="34.739" x2="87.068640625" y2="34.813409375" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="34.813409375" x2="88.64378125" y2="34.813409375" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="34.813409375" x2="88.64378125" y2="34.739" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="34.739" x2="88.64378125" y2="34.514" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="34.514" x2="88.64378125" y2="34.289" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="34.289" x2="88.64378125" y2="34.064" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="34.064" x2="88.64378125" y2="33.839" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="33.839" x2="88.64378125" y2="33.614" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="33.614" x2="88.64378125" y2="33.389" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="33.389" x2="88.64378125" y2="33.164" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="33.164" x2="88.64378125" y2="32.939" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="32.939" x2="88.64378125" y2="32.714" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="32.714" x2="88.64378125" y2="32.489" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="32.489" x2="88.64378125" y2="32.264" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="32.264" x2="88.64378125" y2="32.039" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="32.039" x2="88.64378125" y2="31.814" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="31.814" x2="88.64378125" y2="31.589" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="31.589" x2="88.64378125" y2="31.364" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="31.364" x2="88.64378125" y2="31.139" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="31.139" x2="88.64378125" y2="30.914" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="30.914" x2="88.64378125" y2="30.689" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="30.689" x2="88.64378125" y2="30.464" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="30.464" x2="88.64378125" y2="30.239" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="30.239" x2="88.64378125" y2="30.014" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="30.014" x2="88.64378125" y2="29.789" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="29.789" x2="88.64378125" y2="29.564" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="29.564" x2="88.64378125" y2="29.497059375" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="29.497059375" x2="87.068640625" y2="29.497059375" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="29.49851875" x2="78.286090625" y2="29.564" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="29.564" x2="78.286090625" y2="29.789" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="29.789" x2="78.286090625" y2="30.014" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="30.014" x2="78.286090625" y2="30.239" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="30.239" x2="78.286090625" y2="30.464" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="30.464" x2="78.286090625" y2="30.689" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="30.689" x2="78.286090625" y2="30.914" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="30.914" x2="78.286090625" y2="31.07366875" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="31.07366875" x2="84.81116875" y2="31.07366875" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="31.07366875" x2="84.81116875" y2="30.914" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="30.914" x2="84.81116875" y2="30.689" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="30.689" x2="84.81116875" y2="30.464" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="30.464" x2="84.81116875" y2="30.239" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="30.239" x2="84.81116875" y2="30.014" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="30.014" x2="84.81116875" y2="29.789" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="29.789" x2="84.81116875" y2="29.564" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="29.564" x2="84.81116875" y2="29.49851875" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="29.49851875" x2="78.286090625" y2="29.49851875" width="0.225" layer="94"/>
<wire x1="78.286240625" y1="31.44236875" x2="78.286240625" y2="31.589" width="0.225" layer="94"/>
<wire x1="78.286240625" y1="31.589" x2="78.286240625" y2="31.814" width="0.225" layer="94"/>
<wire x1="78.286240625" y1="31.814" x2="78.286240625" y2="32.039" width="0.225" layer="94"/>
<wire x1="78.286240625" y1="32.039" x2="78.286240625" y2="32.264" width="0.225" layer="94"/>
<wire x1="78.286240625" y1="32.264" x2="78.286240625" y2="32.489" width="0.225" layer="94"/>
<wire x1="78.286240625" y1="32.489" x2="78.286240625" y2="32.714" width="0.225" layer="94"/>
<wire x1="78.286240625" y1="32.714" x2="78.286240625" y2="32.86721875" width="0.225" layer="94"/>
<wire x1="78.286240625" y1="32.86721875" x2="81.96101875" y2="32.86721875" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="32.86721875" x2="81.96101875" y2="32.714" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="32.714" x2="81.96101875" y2="32.489" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="32.489" x2="81.96101875" y2="32.264" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="32.264" x2="81.96101875" y2="32.039" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="32.039" x2="81.96101875" y2="31.814" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="31.814" x2="81.96101875" y2="31.589" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="31.589" x2="81.96101875" y2="31.44236875" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="31.44236875" x2="78.286240625" y2="31.44236875" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="33.23621875" x2="78.286090625" y2="33.389" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="33.389" x2="78.286090625" y2="33.614" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="33.614" x2="78.286090625" y2="33.839" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="33.839" x2="78.286090625" y2="34.064" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="34.064" x2="78.286090625" y2="34.289" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="34.289" x2="78.286090625" y2="34.514" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="34.514" x2="78.286090625" y2="34.739" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="34.739" x2="78.286090625" y2="34.81106875" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="34.81106875" x2="84.81101875" y2="34.81106875" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="34.81106875" x2="84.81101875" y2="34.739" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="34.739" x2="84.81101875" y2="34.514" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="34.514" x2="84.81101875" y2="34.289" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="34.289" x2="84.81101875" y2="34.064" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="34.064" x2="84.81101875" y2="33.839" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="33.839" x2="84.81101875" y2="33.614" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="33.614" x2="84.81101875" y2="33.389" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="33.389" x2="84.81101875" y2="33.23621875" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="33.23621875" x2="78.286090625" y2="33.23621875" width="0.225" layer="94"/>
<wire x1="68.6921" y1="33.944190625" x2="69.073209375" y2="34.289" width="0.225" layer="94"/>
<wire x1="69.073209375" y1="34.289" x2="69.3219" y2="34.514" width="0.225" layer="94"/>
<wire x1="69.3219" y1="34.514" x2="69.570590625" y2="34.739" width="0.225" layer="94"/>
<wire x1="69.570590625" y1="34.739" x2="69.86021875" y2="35.001040625" width="0.225" layer="94"/>
<wire x1="85.115190625" y1="26.34016875" x2="85.115190625" y2="26.189" width="0.225" layer="94"/>
<wire x1="85.115190625" y1="26.189" x2="85.115190625" y2="25.97718125" width="0.225" layer="94"/>
<wire x1="84.32008125" y1="25.980109375" x2="84.341215625" y2="25.70643125" width="0.225" layer="94" curve="8.832242"/>
<wire x1="84.341209375" y1="25.70643125" x2="84.4424375" y2="25.45343125" width="0.225" layer="94" curve="25.94924"/>
<wire x1="84.442440625" y1="25.45343125" x2="84.662590625" y2="25.2962625" width="0.225" layer="94" curve="39.389578"/>
<wire x1="84.662590625" y1="25.296259375" x2="84.93363125" y2="25.258525" width="0.225" layer="94" curve="15.805998"/>
<wire x1="85.52626875" y1="25.706240625" x2="85.547246875" y2="25.980109375" width="0.225" layer="94" curve="8.760059"/>
<wire x1="85.42531875" y1="25.45295" x2="85.46248125" y2="25.514" width="0.225" layer="94" curve="6.74589"/>
<wire x1="85.46248125" y1="25.514" x2="85.526271875" y2="25.706240625" width="0.225" layer="94" curve="19.196299"/>
<wire x1="85.204909375" y1="25.295909375" x2="85.425315625" y2="25.452953125" width="0.225" layer="94" curve="39.656265"/>
<wire x1="84.93363125" y1="25.25853125" x2="85.204909375" y2="25.295909375" width="0.225" layer="94" curve="15.593829"/>
<wire x1="84.32008125" y1="25.980109375" x2="84.32008125" y2="26.189" width="0.225" layer="94"/>
<wire x1="84.32008125" y1="26.189" x2="84.32008125" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="84.341340625" y1="26.61343125" x2="84.32161875" y2="26.414" width="0.225" layer="94" curve="6.503067"/>
<wire x1="84.32161875" y1="26.414" x2="84.320078125" y2="26.34016875" width="0.225" layer="94" curve="2.395218"/>
<wire x1="84.44311875" y1="26.86573125" x2="84.341340625" y2="26.61343125" width="0.225" layer="94" curve="26.142719"/>
<wire x1="84.663740625" y1="27.02155" x2="84.443121875" y2="26.865728125" width="0.225" layer="94" curve="39.450584"/>
<wire x1="84.93363125" y1="27.05881875" x2="84.663740625" y2="27.02155" width="0.225" layer="94" curve="15.292594"/>
<wire x1="85.54725" y1="25.980109375" x2="85.54725" y2="26.189" width="0.225" layer="94"/>
<wire x1="85.54725" y1="26.189" x2="85.54725" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="85.54725" y1="26.34016875" x2="85.52646875" y2="26.61348125" width="0.225" layer="94" curve="8.69613"/>
<wire x1="85.52646875" y1="26.61348125" x2="85.4257" y2="26.8662" width="0.225" layer="94" curve="26.08534"/>
<wire x1="85.4257" y1="26.8662" x2="85.2053" y2="27.02220625" width="0.225" layer="94" curve="39.852867"/>
<wire x1="85.2053" y1="27.022209375" x2="84.93363125" y2="27.058825" width="0.225" layer="94" curve="15.379893"/>
<wire x1="84.752059375" y1="26.34016875" x2="84.752059375" y2="26.189" width="0.225" layer="94"/>
<wire x1="84.752059375" y1="26.189" x2="84.752059375" y2="25.97718125" width="0.225" layer="94"/>
<wire x1="85.115190625" y1="25.97718125" x2="85.11286875" y2="25.87078125" width="0.225" layer="94"/>
<wire x1="85.11286875" y1="25.87078125" x2="85.10523125" y2="25.791359375" width="0.225" layer="94"/>
<wire x1="85.10523125" y1="25.791359375" x2="85.100290625" y2="25.765209375" width="0.225" layer="94"/>
<wire x1="85.100290625" y1="25.765209375" x2="85.09306875" y2="25.73961875" width="0.225" layer="94"/>
<wire x1="85.09306875" y1="25.73961875" x2="85.081940625" y2="25.715509375" width="0.225" layer="94"/>
<wire x1="85.081940625" y1="25.715509375" x2="85.06321875" y2="25.696990625" width="0.225" layer="94"/>
<wire x1="85.06321875" y1="25.696990625" x2="85.0387" y2="25.68683125" width="0.225" layer="94"/>
<wire x1="85.0387" y1="25.68683125" x2="85.012740625" y2="25.68106875" width="0.225" layer="94"/>
<wire x1="85.012740625" y1="25.68106875" x2="84.986340625" y2="25.6778" width="0.225" layer="94"/>
<wire x1="84.986340625" y1="25.6778" x2="84.95978125" y2="25.67618125" width="0.225" layer="94"/>
<wire x1="84.752059375" y1="26.34016875" x2="84.75605" y2="26.47246875" width="0.225" layer="94"/>
<wire x1="84.75605" y1="26.47246875" x2="84.76195" y2="26.52508125" width="0.225" layer="94"/>
<wire x1="84.76195" y1="26.52508125" x2="84.77411875" y2="26.57655" width="0.225" layer="94"/>
<wire x1="84.77411875" y1="26.57655" x2="84.78536875" y2="26.600440625" width="0.225" layer="94"/>
<wire x1="84.78536875" y1="26.600440625" x2="84.804459375" y2="26.618359375" width="0.225" layer="94"/>
<wire x1="84.804459375" y1="26.618359375" x2="84.82903125" y2="26.62806875" width="0.225" layer="94"/>
<wire x1="84.82903125" y1="26.62806875" x2="84.854909375" y2="26.63358125" width="0.225" layer="94"/>
<wire x1="84.854909375" y1="26.63358125" x2="84.8812" y2="26.6367" width="0.225" layer="94"/>
<wire x1="84.8812" y1="26.6367" x2="84.90763125" y2="26.63826875" width="0.225" layer="94"/>
<wire x1="84.90763125" y1="26.63826875" x2="84.96056875" y2="26.63825" width="0.225" layer="94"/>
<wire x1="84.96056875" y1="26.63825" x2="84.987" y2="26.63666875" width="0.225" layer="94"/>
<wire x1="84.987" y1="26.63666875" x2="85.013290625" y2="26.63358125" width="0.225" layer="94"/>
<wire x1="85.013290625" y1="26.63358125" x2="85.039190625" y2="26.628140625" width="0.225" layer="94"/>
<wire x1="85.039190625" y1="26.628140625" x2="85.063809375" y2="26.618559375" width="0.225" layer="94"/>
<wire x1="85.063809375" y1="26.618559375" x2="85.0829" y2="26.60066875" width="0.225" layer="94"/>
<wire x1="85.0829" y1="26.60066875" x2="85.093940625" y2="26.57666875" width="0.225" layer="94"/>
<wire x1="85.093940625" y1="26.57666875" x2="85.105759375" y2="26.52511875" width="0.225" layer="94"/>
<wire x1="85.105759375" y1="26.52511875" x2="85.111390625" y2="26.47248125" width="0.225" layer="94"/>
<wire x1="85.111390625" y1="26.47248125" x2="85.11306875" y2="26.414" width="0.225" layer="94"/>
<wire x1="85.11306875" y1="26.414" x2="85.115190625" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="84.752059375" y1="25.97718125" x2="84.754290625" y2="25.87078125" width="0.225" layer="94"/>
<wire x1="84.754290625" y1="25.87078125" x2="84.76156875" y2="25.79131875" width="0.225" layer="94"/>
<wire x1="84.76156875" y1="25.79131875" x2="84.7664" y2="25.76515" width="0.225" layer="94"/>
<wire x1="84.7664" y1="25.76515" x2="84.773409375" y2="25.7395" width="0.225" layer="94"/>
<wire x1="84.773409375" y1="25.7395" x2="84.784340625" y2="25.7153" width="0.225" layer="94"/>
<wire x1="84.784340625" y1="25.7153" x2="84.803059375" y2="25.6968" width="0.225" layer="94"/>
<wire x1="84.803059375" y1="25.6968" x2="84.82763125" y2="25.686759375" width="0.225" layer="94"/>
<wire x1="84.82763125" y1="25.686759375" x2="84.853609375" y2="25.681059375" width="0.225" layer="94"/>
<wire x1="84.853609375" y1="25.681059375" x2="84.90656875" y2="25.6762" width="0.225" layer="94"/>
<wire x1="84.90656875" y1="25.6762" x2="84.95978125" y2="25.6762" width="0.225" layer="94"/>
<wire x1="88.41505" y1="27.03186875" x2="88.835090625" y2="27.03186875" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="27.03186875" x2="88.835090625" y2="26.864" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="26.864" x2="88.835090625" y2="26.639" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="26.639" x2="88.835090625" y2="26.414" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="26.414" x2="88.835090625" y2="26.189" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="26.189" x2="88.835090625" y2="25.964" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="25.964" x2="88.835090625" y2="25.739" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="25.739" x2="88.835090625" y2="25.514" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="25.514" x2="88.835090625" y2="25.289" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="25.289" x2="88.835090625" y2="25.28548125" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="25.28548125" x2="88.41505" y2="25.28548125" width="0.225" layer="94"/>
<wire x1="88.41505" y1="25.28548125" x2="88.41505" y2="25.289" width="0.225" layer="94"/>
<wire x1="88.41505" y1="25.289" x2="88.41505" y2="25.514" width="0.225" layer="94"/>
<wire x1="88.41505" y1="25.514" x2="88.41505" y2="25.739" width="0.225" layer="94"/>
<wire x1="88.41505" y1="25.739" x2="88.41505" y2="25.964" width="0.225" layer="94"/>
<wire x1="88.41505" y1="25.964" x2="88.41505" y2="26.189" width="0.225" layer="94"/>
<wire x1="88.41505" y1="26.189" x2="88.41505" y2="26.414" width="0.225" layer="94"/>
<wire x1="88.41505" y1="26.414" x2="88.41505" y2="26.639" width="0.225" layer="94"/>
<wire x1="88.41505" y1="26.639" x2="88.41505" y2="26.864" width="0.225" layer="94"/>
<wire x1="88.41505" y1="26.864" x2="88.41505" y2="27.03186875" width="0.225" layer="94"/>
<wire x1="88.41505" y1="27.205890625" x2="88.835090625" y2="27.205890625" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="27.205890625" x2="88.835090625" y2="27.314" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="27.314" x2="88.835090625" y2="27.539" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="27.539" x2="88.835090625" y2="27.68606875" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="27.68606875" x2="88.41505" y2="27.68606875" width="0.225" layer="94"/>
<wire x1="88.41505" y1="27.68606875" x2="88.41505" y2="27.539" width="0.225" layer="94"/>
<wire x1="88.41505" y1="27.539" x2="88.41505" y2="27.314" width="0.225" layer="94"/>
<wire x1="88.41505" y1="27.314" x2="88.41505" y2="27.205890625" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="27.725040625" x2="75.911209375" y2="27.725040625" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="27.725040625" x2="75.911209375" y2="27.539" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="27.539" x2="75.911209375" y2="27.314" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="27.314" x2="75.911209375" y2="27.089" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="27.089" x2="75.911209375" y2="26.864" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="26.864" x2="75.911209375" y2="26.639" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="26.639" x2="75.911209375" y2="26.414" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="26.414" x2="75.911209375" y2="26.189" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="26.189" x2="75.911209375" y2="25.964" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="25.964" x2="75.911209375" y2="25.739" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="25.739" x2="75.911209375" y2="25.514" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="25.514" x2="75.911209375" y2="25.289" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="25.28548125" x2="75.49116875" y2="25.28548125" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="25.28548125" x2="75.49116875" y2="25.289" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="25.289" x2="75.49116875" y2="25.514" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="25.514" x2="75.49116875" y2="25.739" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="25.739" x2="75.49116875" y2="25.964" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="25.964" x2="75.49116875" y2="26.189" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="26.189" x2="75.49116875" y2="26.414" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="26.414" x2="75.49116875" y2="26.639" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="26.639" x2="75.49116875" y2="26.864" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="26.864" x2="75.49116875" y2="27.089" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="27.089" x2="75.49116875" y2="27.314" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="27.314" x2="75.49116875" y2="27.539" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="27.539" x2="75.49116875" y2="27.725040625" width="0.225" layer="94"/>
<wire x1="90.092" y1="25.97718125" x2="90.092" y2="26.189" width="0.225" layer="94"/>
<wire x1="90.092" y1="26.189" x2="90.092" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="90.41016875" y1="26.44828125" x2="90.41016875" y2="26.414" width="0.225" layer="94"/>
<wire x1="90.41016875" y1="26.414" x2="90.41016875" y2="26.30881875" width="0.225" layer="94"/>
<wire x1="90.41016875" y1="26.30881875" x2="90.830209375" y2="26.30881875" width="0.225" layer="94"/>
<wire x1="90.43711875" y1="26.071809375" x2="90.43711875" y2="25.964" width="0.225" layer="94"/>
<wire x1="90.43711875" y1="25.964" x2="90.43711875" y2="25.89925" width="0.225" layer="94"/>
<wire x1="90.43711875" y1="26.071809375" x2="90.86316875" y2="26.071809375" width="0.225" layer="94"/>
<wire x1="90.092" y1="26.34016875" x2="90.09408125" y2="26.45038125" width="0.225" layer="94"/>
<wire x1="90.09408125" y1="26.45038125" x2="90.09926875" y2="26.5163" width="0.225" layer="94"/>
<wire x1="90.09926875" y1="26.5163" x2="90.106540625" y2="26.55976875" width="0.225" layer="94"/>
<wire x1="90.106540625" y1="26.55976875" x2="90.112390625" y2="26.58101875" width="0.225" layer="94"/>
<wire x1="90.112390625" y1="26.58101875" x2="90.12096875" y2="26.6013" width="0.225" layer="94"/>
<wire x1="90.12096875" y1="26.6013" x2="90.135190625" y2="26.617890625" width="0.225" layer="94"/>
<wire x1="90.135190625" y1="26.617890625" x2="90.1551" y2="26.6272" width="0.225" layer="94"/>
<wire x1="90.1551" y1="26.6272" x2="90.17645" y2="26.63263125" width="0.225" layer="94"/>
<wire x1="90.17645" y1="26.63263125" x2="90.19825" y2="26.6359" width="0.225" layer="94"/>
<wire x1="90.19825" y1="26.6359" x2="90.220209375" y2="26.637740625" width="0.225" layer="94"/>
<wire x1="90.220209375" y1="26.637740625" x2="90.242240625" y2="26.63858125" width="0.225" layer="94"/>
<wire x1="90.242240625" y1="26.63858125" x2="90.264290625" y2="26.638640625" width="0.225" layer="94"/>
<wire x1="90.264290625" y1="26.638640625" x2="90.28631875" y2="26.6378" width="0.225" layer="94"/>
<wire x1="90.28631875" y1="26.6378" x2="90.30826875" y2="26.63575" width="0.225" layer="94"/>
<wire x1="90.30826875" y1="26.63575" x2="90.33" y2="26.63208125" width="0.225" layer="94"/>
<wire x1="90.33" y1="26.63208125" x2="90.351140625" y2="26.625909375" width="0.225" layer="94"/>
<wire x1="90.351140625" y1="26.625909375" x2="90.3705" y2="26.615509375" width="0.225" layer="94"/>
<wire x1="90.3705" y1="26.615509375" x2="90.38486875" y2="26.598990625" width="0.225" layer="94"/>
<wire x1="90.38486875" y1="26.598990625" x2="90.40023125" y2="26.557840625" width="0.225" layer="94"/>
<wire x1="90.40023125" y1="26.557840625" x2="90.407140625" y2="26.51431875" width="0.225" layer="94"/>
<wire x1="90.407140625" y1="26.51431875" x2="90.41016875" y2="26.44828125" width="0.225" layer="94"/>
<wire x1="90.092" y1="25.97718125" x2="90.09478125" y2="25.85786875" width="0.225" layer="94"/>
<wire x1="90.09478125" y1="25.85786875" x2="90.09865" y2="25.810290625" width="0.225" layer="94"/>
<wire x1="90.09865" y1="25.810290625" x2="90.11223125" y2="25.74008125" width="0.225" layer="94"/>
<wire x1="90.11223125" y1="25.74008125" x2="90.121240625" y2="25.718009375" width="0.225" layer="94"/>
<wire x1="90.121240625" y1="25.718009375" x2="90.136040625" y2="25.6995" width="0.225" layer="94"/>
<wire x1="90.136040625" y1="25.6995" x2="90.15721875" y2="25.688690625" width="0.225" layer="94"/>
<wire x1="90.15721875" y1="25.688690625" x2="90.18025" y2="25.68248125" width="0.225" layer="94"/>
<wire x1="90.18025" y1="25.68248125" x2="90.20383125" y2="25.678809375" width="0.225" layer="94"/>
<wire x1="90.20383125" y1="25.678809375" x2="90.251459375" y2="25.675840625" width="0.225" layer="94"/>
<wire x1="90.251459375" y1="25.675840625" x2="90.299190625" y2="25.67656875" width="0.225" layer="94"/>
<wire x1="90.299190625" y1="25.67656875" x2="90.34663125" y2="25.681690625" width="0.225" layer="94"/>
<wire x1="90.34663125" y1="25.681690625" x2="90.391890625" y2="25.696190625" width="0.225" layer="94"/>
<wire x1="90.391890625" y1="25.696190625" x2="90.409190625" y2="25.71226875" width="0.225" layer="94"/>
<wire x1="90.409190625" y1="25.71226875" x2="90.419459375" y2="25.733759375" width="0.225" layer="94"/>
<wire x1="90.419459375" y1="25.733759375" x2="90.425940625" y2="25.75671875" width="0.225" layer="94"/>
<wire x1="90.425940625" y1="25.75671875" x2="90.4332" y2="25.80388125" width="0.225" layer="94"/>
<wire x1="90.4332" y1="25.80388125" x2="90.436309375" y2="25.85151875" width="0.225" layer="94"/>
<wire x1="90.436309375" y1="25.85151875" x2="90.43711875" y2="25.89925" width="0.225" layer="94"/>
<wire x1="89.66001875" y1="25.980109375" x2="89.67878125" y2="25.718659375" width="0.225" layer="94" curve="8.208716"/>
<wire x1="89.67878125" y1="25.718659375" x2="89.74463125" y2="25.514" width="0.225" layer="94" curve="19.254744"/>
<wire x1="89.74463125" y1="25.514" x2="89.76749375" y2="25.47364375" width="0.225" layer="94" curve="4.135389"/>
<wire x1="89.767490625" y1="25.473640625" x2="89.966896875" y2="25.309234375" width="0.225" layer="94" curve="37.791863"/>
<wire x1="89.9669" y1="25.309240625" x2="90.02831875" y2="25.289" width="0.225" layer="94" curve="4.744864"/>
<wire x1="90.02831875" y1="25.289" x2="90.22315" y2="25.259475" width="0.225" layer="94" curve="14.492988"/>
<wire x1="90.86316875" y1="26.071809375" x2="90.86316875" y2="25.964" width="0.225" layer="94"/>
<wire x1="90.86316875" y1="25.964" x2="90.86316875" y2="25.89603125" width="0.225" layer="94"/>
<wire x1="90.83383125" y1="25.63598125" x2="90.852559375" y2="25.739" width="0.225" layer="94" curve="5.141671"/>
<wire x1="90.852559375" y1="25.739" x2="90.86316875" y2="25.89603125" width="0.225" layer="94" curve="7.731903"/>
<wire x1="90.710259375" y1="25.40781875" x2="90.83383125" y2="25.63598125" width="0.225" layer="94" curve="31.131749"/>
<wire x1="90.483690625" y1="25.282109375" x2="90.710259375" y2="25.40781875" width="0.225" layer="94" curve="33.941784"/>
<wire x1="90.22315" y1="25.25946875" x2="90.483690625" y2="25.282103125" width="0.225" layer="94" curve="14.175253"/>
<wire x1="89.66001875" y1="25.980109375" x2="89.66001875" y2="26.189" width="0.225" layer="94"/>
<wire x1="89.66001875" y1="26.189" x2="89.66001875" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="89.6773" y1="26.590590625" x2="89.66001875" y2="26.34016875" width="0.225" layer="94" curve="7.895583"/>
<wire x1="89.75726875" y1="26.82721875" x2="89.685840625" y2="26.639" width="0.225" layer="94" curve="17.336884"/>
<wire x1="89.685840625" y1="26.639" x2="89.6773" y2="26.590590625" width="0.225" layer="94" curve="4.218105"/>
<wire x1="89.93936875" y1="26.995190625" x2="89.75726875" y2="26.82721875" width="0.225" layer="94" curve="35.721204"/>
<wire x1="90.18176875" y1="27.05536875" x2="89.93936875" y2="26.995190625" width="0.225" layer="94" curve="21.771833"/>
<wire x1="90.830209375" y1="26.30881875" x2="90.830209375" y2="26.414" width="0.225" layer="94"/>
<wire x1="90.830209375" y1="26.414" x2="90.830209375" y2="26.49631875" width="0.225" layer="94"/>
<wire x1="90.830209375" y1="26.49631875" x2="90.7978375" y2="26.744540625" width="0.225" layer="94" curve="14.861165"/>
<wire x1="90.797840625" y1="26.744540625" x2="90.6616625" y2="26.951284375" width="0.225" layer="94" curve="37.022842"/>
<wire x1="90.661659375" y1="26.95128125" x2="90.432340625" y2="27.047065625" width="0.225" layer="94" curve="30.892228"/>
<wire x1="90.432340625" y1="27.04706875" x2="90.18176875" y2="27.055365625" width="0.225" layer="94" curve="10.656803"/>
<wire x1="68.6921" y1="33.944190625" x2="68.78723125" y2="33.839" width="0.225" layer="94"/>
<wire x1="68.78723125" y1="33.839" x2="69.804659375" y2="32.714" width="0.225" layer="94"/>
<wire x1="69.804659375" y1="32.714" x2="70.21163125" y2="32.264" width="0.225" layer="94"/>
<wire x1="70.21163125" y1="32.264" x2="71.229059375" y2="31.139" width="0.225" layer="94"/>
<wire x1="71.229059375" y1="31.139" x2="71.63603125" y2="30.689" width="0.225" layer="94"/>
<wire x1="71.63603125" y1="30.689" x2="72.653459375" y2="29.564" width="0.225" layer="94"/>
<wire x1="72.653459375" y1="29.564" x2="72.94835" y2="29.23793125" width="0.225" layer="94"/>
<wire x1="72.94835" y1="29.23793125" x2="73.44671875" y2="29.789" width="0.225" layer="94"/>
<wire x1="73.44671875" y1="29.789" x2="73.6502" y2="30.014" width="0.225" layer="94"/>
<wire x1="73.6502" y1="30.014" x2="74.26065" y2="30.689" width="0.225" layer="94"/>
<wire x1="74.26065" y1="30.689" x2="74.46413125" y2="30.914" width="0.225" layer="94"/>
<wire x1="74.46413125" y1="30.914" x2="75.278059375" y2="31.814" width="0.225" layer="94"/>
<wire x1="75.278059375" y1="31.814" x2="75.481540625" y2="32.039" width="0.225" layer="94"/>
<wire x1="75.481540625" y1="32.039" x2="76.29546875" y2="32.939" width="0.225" layer="94"/>
<wire x1="76.29546875" y1="32.939" x2="76.49895" y2="33.164" width="0.225" layer="94"/>
<wire x1="76.49895" y1="33.164" x2="76.70243125" y2="33.389" width="0.225" layer="94"/>
<wire x1="76.70243125" y1="33.389" x2="77.204709375" y2="33.944390625" width="0.225" layer="94"/>
<wire x1="77.204709375" y1="33.944390625" x2="76.036390625" y2="35.00103125" width="0.225" layer="94"/>
<wire x1="76.036390625" y1="35.00103125" x2="74.57861875" y2="33.389" width="0.225" layer="94"/>
<wire x1="74.57861875" y1="33.389" x2="74.37515" y2="33.164" width="0.225" layer="94"/>
<wire x1="74.37515" y1="33.164" x2="74.17168125" y2="32.939" width="0.225" layer="94"/>
<wire x1="74.17168125" y1="32.939" x2="73.764740625" y2="32.489" width="0.225" layer="94"/>
<wire x1="73.764740625" y1="32.489" x2="72.94835" y2="31.58621875" width="0.225" layer="94"/>
<wire x1="72.94835" y1="31.58621875" x2="72.742359375" y2="31.814" width="0.225" layer="94"/>
<wire x1="72.742359375" y1="31.814" x2="72.131940625" y2="32.489" width="0.225" layer="94"/>
<wire x1="72.131940625" y1="32.489" x2="71.724990625" y2="32.939" width="0.225" layer="94"/>
<wire x1="71.724990625" y1="32.939" x2="71.318040625" y2="33.389" width="0.225" layer="94"/>
<wire x1="71.318040625" y1="33.389" x2="70.911090625" y2="33.839" width="0.225" layer="94"/>
<wire x1="70.911090625" y1="33.839" x2="70.504140625" y2="34.289" width="0.225" layer="94"/>
<wire x1="70.504140625" y1="34.289" x2="70.097190625" y2="34.739" width="0.225" layer="94"/>
<wire x1="70.097190625" y1="34.739" x2="69.86021875" y2="35.001040625" width="0.225" layer="94"/>
<wire x1="81.341240625" y1="26.03255" x2="81.74326875" y2="26.03255" width="0.225" layer="94"/>
<wire x1="81.74326875" y1="26.03255" x2="81.74326875" y2="25.964" width="0.225" layer="94"/>
<wire x1="81.74326875" y1="25.964" x2="81.74326875" y2="25.812240625" width="0.225" layer="94"/>
<wire x1="81.723590625" y1="25.602390625" x2="81.743275" y2="25.812240625" width="0.225" layer="94" curve="10.717258"/>
<wire x1="81.637890625" y1="25.41138125" x2="81.7235875" y2="25.602390625" width="0.225" layer="94" curve="26.892331"/>
<wire x1="81.467640625" y1="25.291140625" x2="81.6378875" y2="25.41138125" width="0.225" layer="94" curve="34.316783"/>
<wire x1="81.260009375" y1="25.258559375" x2="81.467640625" y2="25.291140625" width="0.225" layer="94" curve="18.312496"/>
<wire x1="81.05158125" y1="25.28623125" x2="81.260009375" y2="25.258559375" width="0.225" layer="94" curve="14.64866"/>
<wire x1="80.880909375" y1="25.405009375" x2="81.05158125" y2="25.286234375" width="0.225" layer="94" curve="39.895242"/>
<wire x1="80.80178125" y1="25.5987" x2="80.880903125" y2="25.40500625" width="0.225" layer="94" curve="25.996239"/>
<wire x1="80.786140625" y1="25.80901875" x2="80.786140625" y2="25.964" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="25.964" x2="80.786140625" y2="26.189" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="26.189" x2="80.786140625" y2="26.414" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="26.414" x2="80.786140625" y2="26.626990625" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="25.80901875" x2="80.78728125" y2="25.739" width="0.225" layer="94" curve="3.295417"/>
<wire x1="80.78728125" y1="25.739" x2="80.801784375" y2="25.5987" width="0.225" layer="94" curve="6.640297"/>
<wire x1="81.206190625" y1="25.86908125" x2="81.206190625" y2="25.964" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="25.964" x2="81.206190625" y2="26.189" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="26.189" x2="81.206190625" y2="26.414" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="26.414" x2="81.206190625" y2="26.626990625" width="0.225" layer="94"/>
<wire x1="81.341240625" y1="26.03255" x2="81.341240625" y2="25.964" width="0.225" layer="94"/>
<wire x1="81.341240625" y1="25.964" x2="81.341240625" y2="25.779140625" width="0.225" layer="94"/>
<wire x1="81.317509375" y1="25.6654" x2="81.327084375" y2="25.6739125" width="0.225" layer="94" curve="49.818215"/>
<wire x1="81.30451875" y1="25.66235" x2="81.3175125" y2="25.66539375" width="0.225" layer="94" curve="7.103613"/>
<wire x1="81.291240625" y1="25.66093125" x2="81.30451875" y2="25.66235625" width="0.225" layer="94" curve="7.003084"/>
<wire x1="81.277890625" y1="25.660490625" x2="81.291240625" y2="25.660934375" width="0.225" layer="94" curve="1.449937"/>
<wire x1="81.26453125" y1="25.66073125" x2="81.277890625" y2="25.6604875" width="0.225" layer="94" curve="4.441603"/>
<wire x1="81.33501875" y1="25.69935" x2="81.33183125" y2="25.68638125" width="0.225" layer="94"/>
<wire x1="81.33183125" y1="25.68638125" x2="81.327090625" y2="25.673909375" width="0.225" layer="94"/>
<wire x1="81.33501875" y1="25.69935" x2="81.3373" y2="25.712509375" width="0.225" layer="94"/>
<wire x1="81.3373" y1="25.712509375" x2="81.340009375" y2="25.739" width="0.225" layer="94"/>
<wire x1="81.340009375" y1="25.739" x2="81.34001875" y2="25.739090625" width="0.225" layer="94"/>
<wire x1="81.34001875" y1="25.739090625" x2="81.341240625" y2="25.779140625" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="25.86908125" x2="81.20651875" y2="25.818159375" width="0.225" layer="94"/>
<wire x1="81.20651875" y1="25.818159375" x2="81.208009375" y2="25.76726875" width="0.225" layer="94"/>
<wire x1="81.208009375" y1="25.76726875" x2="81.209609375" y2="25.741859375" width="0.225" layer="94"/>
<wire x1="81.209609375" y1="25.741859375" x2="81.21228125" y2="25.716540625" width="0.225" layer="94"/>
<wire x1="81.21228125" y1="25.716540625" x2="81.21671875" y2="25.69148125" width="0.225" layer="94"/>
<wire x1="81.21671875" y1="25.69148125" x2="81.225321875" y2="25.66765" width="0.225" layer="94" curve="19.627877"/>
<wire x1="81.26453125" y1="25.66073125" x2="81.251209375" y2="25.66166875" width="0.225" layer="94"/>
<wire x1="81.238" y1="25.663609375" x2="81.251209375" y2="25.661671875" width="0.225" layer="94" curve="8.648156"/>
<wire x1="81.22531875" y1="25.66765" x2="81.238" y2="25.66360625" width="0.225" layer="94" curve="10.044676"/>
<wire x1="81.7013" y1="26.626990625" x2="81.206190625" y2="26.626990625" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="27.03186875" x2="81.7013" y2="27.03186875" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="27.401009375" x2="81.206190625" y2="27.314" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="27.314" x2="81.206190625" y2="27.089" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="27.089" x2="81.206190625" y2="27.03186875" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="27.03186875" x2="80.786140625" y2="27.089" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="27.089" x2="80.786140625" y2="27.314" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="27.314" x2="80.786140625" y2="27.401009375" width="0.225" layer="94"/>
<wire x1="80.627059375" y1="27.03186875" x2="80.786140625" y2="27.03186875" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="26.626990625" x2="80.627059375" y2="26.626990625" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="27.401009375" x2="81.206190625" y2="27.401009375" width="0.225" layer="94"/>
<wire x1="81.7013" y1="27.03186875" x2="81.7013" y2="26.864" width="0.225" layer="94"/>
<wire x1="81.7013" y1="26.864" x2="81.7013" y2="26.639" width="0.225" layer="94"/>
<wire x1="81.7013" y1="26.639" x2="81.7013" y2="26.626990625" width="0.225" layer="94"/>
<wire x1="80.627059375" y1="27.03186875" x2="80.627059375" y2="26.864" width="0.225" layer="94"/>
<wire x1="80.627059375" y1="26.864" x2="80.627059375" y2="26.639" width="0.225" layer="94"/>
<wire x1="80.627059375" y1="26.639" x2="80.627059375" y2="26.626990625" width="0.225" layer="94"/>
<wire x1="92.6429" y1="26.95253125" x2="92.469709375" y2="27.040159375" width="0.225" layer="94" curve="32.338936"/>
<wire x1="79.95228125" y1="26.071809375" x2="79.52623125" y2="26.071809375" width="0.225" layer="94"/>
<wire x1="79.49928125" y1="26.30881875" x2="79.91931875" y2="26.30881875" width="0.225" layer="94"/>
<wire x1="79.91931875" y1="26.30881875" x2="79.91931875" y2="26.414" width="0.225" layer="94"/>
<wire x1="79.91931875" y1="26.414" x2="79.91931875" y2="26.49631875" width="0.225" layer="94"/>
<wire x1="79.91931875" y1="26.49631875" x2="79.886946875" y2="26.744540625" width="0.225" layer="94" curve="14.861165"/>
<wire x1="79.88695" y1="26.744540625" x2="79.750771875" y2="26.951284375" width="0.225" layer="94" curve="37.022842"/>
<wire x1="79.75076875" y1="26.95128125" x2="79.521459375" y2="27.047065625" width="0.225" layer="94" curve="30.891086"/>
<wire x1="79.521459375" y1="27.04706875" x2="79.27088125" y2="27.055365625" width="0.225" layer="94" curve="10.657067"/>
<wire x1="79.27088125" y1="27.05536875" x2="79.02848125" y2="26.995190625" width="0.225" layer="94" curve="21.019898"/>
<wire x1="79.02848125" y1="26.995190625" x2="78.84638125" y2="26.82721875" width="0.225" layer="94" curve="36.47327"/>
<wire x1="78.84638125" y1="26.82721875" x2="78.77521875" y2="26.639" width="0.225" layer="94" curve="16.726592"/>
<wire x1="78.77521875" y1="26.639" x2="78.7664125" y2="26.590590625" width="0.225" layer="94" curve="4.076419"/>
<wire x1="78.766409375" y1="26.590590625" x2="78.75028125" y2="26.414" width="0.225" layer="94" curve="6.105964"/>
<wire x1="78.75028125" y1="26.414" x2="78.749128125" y2="26.34016875" width="0.225" layer="94" curve="2.541594"/>
<wire x1="79.95228125" y1="26.071809375" x2="79.95228125" y2="25.964" width="0.225" layer="94"/>
<wire x1="79.95228125" y1="25.964" x2="79.95228125" y2="25.89603125" width="0.225" layer="94"/>
<wire x1="79.922940625" y1="25.63598125" x2="79.94166875" y2="25.739" width="0.225" layer="94" curve="5.141671"/>
<wire x1="79.94166875" y1="25.739" x2="79.952278125" y2="25.89603125" width="0.225" layer="94" curve="7.731903"/>
<wire x1="79.79936875" y1="25.40781875" x2="79.922940625" y2="25.63598125" width="0.225" layer="94" curve="31.131749"/>
<wire x1="79.572809375" y1="25.282109375" x2="79.60065" y2="25.289" width="0.225" layer="94" curve="3.702975"/>
<wire x1="79.60065" y1="25.289" x2="79.799371875" y2="25.407815625" width="0.225" layer="94" curve="30.237845"/>
<wire x1="79.312259375" y1="25.25946875" x2="79.572809375" y2="25.28210625" width="0.225" layer="94" curve="14.175788"/>
<wire x1="79.056009375" y1="25.309240625" x2="79.119840625" y2="25.289" width="0.225" layer="94" curve="4.53295"/>
<wire x1="79.119840625" y1="25.289" x2="79.312259375" y2="25.259475" width="0.225" layer="94" curve="13.203582"/>
<wire x1="78.856609375" y1="25.473640625" x2="79.056009375" y2="25.3092375" width="0.225" layer="94" curve="39.29237"/>
<wire x1="78.767890625" y1="25.718659375" x2="78.85660625" y2="25.473640625" width="0.225" layer="94" curve="21.889189"/>
<wire x1="78.74913125" y1="25.980109375" x2="78.76789375" y2="25.718659375" width="0.225" layer="94" curve="9.71004"/>
<wire x1="78.74913125" y1="25.980109375" x2="78.74913125" y2="26.189" width="0.225" layer="94"/>
<wire x1="78.74913125" y1="26.189" x2="78.74913125" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="79.49928125" y1="26.30881875" x2="79.49928125" y2="26.414" width="0.225" layer="94"/>
<wire x1="79.49928125" y1="26.414" x2="79.49928125" y2="26.44828125" width="0.225" layer="94"/>
<wire x1="79.49928125" y1="26.44828125" x2="79.496253125" y2="26.51431875" width="0.225" layer="94" curve="5.249884"/>
<wire x1="79.49625" y1="26.51431875" x2="79.48934375" y2="26.557840625" width="0.225" layer="94" curve="7.529032"/>
<wire x1="79.489340625" y1="26.557840625" x2="79.483196875" y2="26.579" width="0.225" layer="94" curve="6.832689"/>
<wire x1="79.4832" y1="26.579" x2="79.47398125" y2="26.598990625" width="0.225" layer="94" curve="10.282953"/>
<wire x1="79.47398125" y1="26.598990625" x2="79.459609375" y2="26.615509375" width="0.225" layer="94" curve="22.257449"/>
<wire x1="79.459609375" y1="26.615509375" x2="79.440259375" y2="26.625909375" width="0.225" layer="94" curve="19.188066"/>
<wire x1="79.440259375" y1="26.625909375" x2="79.39738125" y2="26.63575" width="0.225" layer="94" curve="11.463239"/>
<wire x1="79.39738125" y1="26.63575" x2="79.3534" y2="26.6386375" width="0.225" layer="94" curve="6.876925"/>
<wire x1="79.3534" y1="26.638640625" x2="79.30931875" y2="26.63774375" width="0.225" layer="94" curve="2.970191"/>
<wire x1="79.30931875" y1="26.637740625" x2="79.265559375" y2="26.632625" width="0.225" layer="94" curve="8.0302"/>
<wire x1="79.265559375" y1="26.63263125" x2="79.244209375" y2="26.6272" width="0.225" layer="94" curve="7.184068"/>
<wire x1="79.244209375" y1="26.6272" x2="79.2243" y2="26.617890625" width="0.225" layer="94" curve="14.385749"/>
<wire x1="79.2243" y1="26.617890625" x2="79.210078125" y2="26.6013" width="0.225" layer="94" curve="34.295039"/>
<wire x1="79.21008125" y1="26.6013" x2="79.191453125" y2="26.53813125" width="0.225" layer="94" curve="14.037627"/>
<wire x1="79.19145" y1="26.53813125" x2="79.1844125" y2="26.472390625" width="0.225" layer="94" curve="6.60122"/>
<wire x1="79.52623125" y1="26.071809375" x2="79.52623125" y2="25.964" width="0.225" layer="94"/>
<wire x1="79.52623125" y1="25.964" x2="79.52623125" y2="25.89925" width="0.225" layer="94"/>
<wire x1="79.51938125" y1="25.780190625" x2="79.526234375" y2="25.89925" width="0.225" layer="94" curve="6.589587"/>
<wire x1="79.50856875" y1="25.733759375" x2="79.519378125" y2="25.780190625" width="0.225" layer="94" curve="13.026604"/>
<wire x1="79.45898125" y1="25.6871" x2="79.508565625" y2="25.733759375" width="0.225" layer="94" curve="54.248333"/>
<wire x1="79.4121" y1="25.6784" x2="79.45898125" y2="25.687103125" width="0.225" layer="94" curve="11.235116"/>
<wire x1="79.292940625" y1="25.678809375" x2="79.4121" y2="25.678396875" width="0.225" layer="94" curve="10.193817"/>
<wire x1="79.24633125" y1="25.688690625" x2="79.292940625" y2="25.6788125" width="0.225" layer="94" curve="13.338819"/>
<wire x1="79.22515" y1="25.6995" x2="79.24633125" y2="25.6886875" width="0.225" layer="94" curve="16.819291"/>
<wire x1="79.21035" y1="25.718009375" x2="79.225146875" y2="25.69949375" width="0.225" layer="94" curve="31.833994"/>
<wire x1="79.195259375" y1="25.763159375" x2="79.21035625" y2="25.7180125" width="0.225" layer="94" curve="8.452794"/>
<wire x1="79.187759375" y1="25.810290625" x2="79.19525625" y2="25.763159375" width="0.225" layer="94" curve="10.449934"/>
<wire x1="79.187759375" y1="25.810290625" x2="79.18275" y2="25.881709375" width="0.225" layer="94"/>
<wire x1="79.18441875" y1="26.472390625" x2="79.181740625" y2="26.406309375" width="0.225" layer="94"/>
<wire x1="79.181740625" y1="26.406309375" x2="79.181109375" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="79.181109375" y1="26.34016875" x2="79.181109375" y2="26.189" width="0.225" layer="94"/>
<wire x1="79.181109375" y1="26.189" x2="79.181109375" y2="25.97718125" width="0.225" layer="94"/>
<wire x1="79.181109375" y1="25.97718125" x2="79.18275" y2="25.881709375" width="0.225" layer="94"/>
<wire x1="77.1742" y1="26.422790625" x2="77.54935" y2="26.422790625" width="0.225" layer="94"/>
<wire x1="77.1742" y1="26.00881875" x2="77.969390625" y2="26.00881875" width="0.225" layer="94"/>
<wire x1="77.969390625" y1="26.00881875" x2="77.969390625" y2="26.189" width="0.225" layer="94"/>
<wire x1="77.969390625" y1="26.189" x2="77.969390625" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="77.969390625" y1="26.34016875" x2="77.948609375" y2="26.61348125" width="0.225" layer="94" curve="8.696144"/>
<wire x1="77.948609375" y1="26.61348125" x2="77.84784375" y2="26.866196875" width="0.225" layer="94" curve="26.085225"/>
<wire x1="77.84785" y1="26.8662" x2="77.62745" y2="27.022209375" width="0.225" layer="94" curve="39.85306"/>
<wire x1="77.62745" y1="27.022209375" x2="77.35661875" y2="27.058828125" width="0.225" layer="94" curve="15.332321"/>
<wire x1="77.35661875" y1="27.05881875" x2="77.08588125" y2="27.02155" width="0.225" layer="94" curve="15.743623"/>
<wire x1="77.08588125" y1="27.02155" x2="76.865265625" y2="26.865728125" width="0.225" layer="94" curve="39.046943"/>
<wire x1="76.865259375" y1="26.86573125" x2="76.864040625" y2="26.864" width="0.225" layer="94" curve="0.204743"/>
<wire x1="76.864040625" y1="26.864" x2="76.763478125" y2="26.61343125" width="0.225" layer="94" curve="26.341038"/>
<wire x1="76.76348125" y1="26.61343125" x2="76.74395" y2="26.414" width="0.225" layer="94" curve="6.208124"/>
<wire x1="76.74395" y1="26.414" x2="76.74221875" y2="26.34016875" width="0.225" layer="94" curve="2.287022"/>
<wire x1="77.561359375" y1="25.98158125" x2="77.966390625" y2="25.98158125" width="0.225" layer="94"/>
<wire x1="77.966390625" y1="25.98158125" x2="77.966390625" y2="25.964" width="0.225" layer="94"/>
<wire x1="77.966390625" y1="25.964" x2="77.966390625" y2="25.8181" width="0.225" layer="94"/>
<wire x1="77.92765" y1="25.565709375" x2="77.966390625" y2="25.8181" width="0.225" layer="94" curve="17.452125"/>
<wire x1="77.77643125" y1="25.363190625" x2="77.92765" y2="25.565709375" width="0.225" layer="94" curve="38.592139"/>
<wire x1="77.53896875" y1="25.271940625" x2="77.618090625" y2="25.289" width="0.225" layer="94" curve="8.168491"/>
<wire x1="77.618090625" y1="25.289" x2="77.77643125" y2="25.363190625" width="0.225" layer="94" curve="17.702304"/>
<wire x1="77.28301875" y1="25.26058125" x2="77.53896875" y2="25.271940625" width="0.225" layer="94" curve="11.086795"/>
<wire x1="77.03403125" y1="25.31643125" x2="77.11308125" y2="25.289" width="0.225" layer="94" curve="6.296209"/>
<wire x1="77.11308125" y1="25.289" x2="77.28301875" y2="25.260578125" width="0.225" layer="94" curve="12.986111"/>
<wire x1="76.844159375" y1="25.483540625" x2="77.034028125" y2="25.316421875" width="0.225" layer="94" curve="38.138006"/>
<wire x1="76.760209375" y1="25.724359375" x2="76.8441625" y2="25.483540625" width="0.225" layer="94" curve="20.717901"/>
<wire x1="76.74221875" y1="25.980109375" x2="76.758" y2="25.739" width="0.225" layer="94" curve="9.114768"/>
<wire x1="76.758" y1="25.739" x2="76.760209375" y2="25.724359375" width="0.225" layer="94" curve="0.557951"/>
<wire x1="76.74221875" y1="25.980109375" x2="76.74221875" y2="26.189" width="0.225" layer="94"/>
<wire x1="76.74221875" y1="26.189" x2="76.74221875" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="77.1742" y1="26.00881875" x2="77.1742" y2="25.97718125" width="0.225" layer="94"/>
<wire x1="77.1742" y1="25.97718125" x2="77.17648125" y2="25.863140625" width="0.225" layer="94"/>
<wire x1="77.17648125" y1="25.863140625" x2="77.179640625" y2="25.81763125" width="0.225" layer="94"/>
<wire x1="77.179640625" y1="25.81763125" x2="77.185759375" y2="25.77241875" width="0.225" layer="94"/>
<wire x1="77.185759375" y1="25.77241875" x2="77.19780625" y2="25.728490625" width="0.225" layer="94" curve="15.249939"/>
<wire x1="77.197809375" y1="25.728490625" x2="77.22771875" y2="25.69585" width="0.225" layer="94" curve="39.077584"/>
<wire x1="77.22771875" y1="25.69585" x2="77.2493" y2="25.688528125" width="0.225" layer="94" curve="18.445063"/>
<wire x1="77.2493" y1="25.68853125" x2="77.45375" y2="25.6834" width="0.225" layer="94" curve="16.158964"/>
<wire x1="77.45375" y1="25.6834" x2="77.498609375" y2="25.691559375" width="0.225" layer="94" curve="7.337797"/>
<wire x1="77.498609375" y1="25.691559375" x2="77.520209375" y2="25.6988375" width="0.225" layer="94" curve="9.286376"/>
<wire x1="77.561359375" y1="25.98158125" x2="77.561359375" y2="25.964" width="0.225" layer="94"/>
<wire x1="77.561359375" y1="25.964" x2="77.561359375" y2="25.82103125" width="0.225" layer="94"/>
<wire x1="77.561359375" y1="25.82103125" x2="77.559340625" y2="25.775459375" width="0.225" layer="94"/>
<wire x1="77.559340625" y1="25.775459375" x2="77.55613125" y2="25.75288125" width="0.225" layer="94"/>
<wire x1="77.520209375" y1="25.698840625" x2="77.556125" y2="25.75288125" width="0.225" layer="94" curve="51.030296"/>
<wire x1="77.184159375" y1="26.547240625" x2="77.174196875" y2="26.422790625" width="0.225" layer="94" curve="9.151585"/>
<wire x1="77.2077" y1="26.6134" x2="77.184159375" y2="26.547240625" width="0.225" layer="94" curve="20.872634"/>
<wire x1="77.54935" y1="26.422790625" x2="77.539775" y2="26.547290625" width="0.225" layer="94" curve="8.794744"/>
<wire x1="77.53978125" y1="26.547290625" x2="77.527525" y2="26.592734375" width="0.225" layer="94" curve="12.594721"/>
<wire x1="77.52751875" y1="26.59273125" x2="77.516803125" y2="26.613665625" width="0.225" layer="94" curve="11.429217"/>
<wire x1="77.516809375" y1="26.61366875" x2="77.5000125" y2="26.629975" width="0.225" layer="94" curve="26.064892"/>
<wire x1="77.500009375" y1="26.62996875" x2="77.47873125" y2="26.63995" width="0.225" layer="94" curve="11.965711"/>
<wire x1="77.47873125" y1="26.63995" x2="77.455990625" y2="26.64610625" width="0.225" layer="94" curve="8.00028"/>
<wire x1="77.455990625" y1="26.646109375" x2="77.38576875" y2="26.653571875" width="0.225" layer="94" curve="10.160203"/>
<wire x1="77.38576875" y1="26.65356875" x2="77.315109375" y2="26.652346875" width="0.225" layer="94" curve="3.952821"/>
<wire x1="77.315109375" y1="26.65235" x2="77.268440625" y2="26.646071875" width="0.225" layer="94" curve="9.390964"/>
<wire x1="77.268440625" y1="26.64606875" x2="77.245740625" y2="26.639796875" width="0.225" layer="94" curve="6.177099"/>
<wire x1="77.245740625" y1="26.6398" x2="77.24345" y2="26.639" width="0.225" layer="94" curve="1.397304"/>
<wire x1="77.24345" y1="26.639" x2="77.22448125" y2="26.629740625" width="0.225" layer="94" curve="12.178653"/>
<wire x1="77.22448125" y1="26.629740625" x2="77.207703125" y2="26.613396875" width="0.225" layer="94" curve="24.279547"/>
<wire x1="73.9395" y1="26.629740625" x2="73.922721875" y2="26.613396875" width="0.225" layer="94" curve="24.279547"/>
<wire x1="73.96075" y1="26.6398" x2="73.958459375" y2="26.639" width="0.225" layer="94" curve="1.397249"/>
<wire x1="73.958459375" y1="26.639" x2="73.939496875" y2="26.62974375" width="0.225" layer="94" curve="12.174138"/>
<wire x1="73.98345" y1="26.64606875" x2="73.96075" y2="26.639796875" width="0.225" layer="94" curve="6.177099"/>
<wire x1="74.03013125" y1="26.65235" x2="73.98345" y2="26.64606875" width="0.225" layer="94" curve="9.393557"/>
<wire x1="74.100790625" y1="26.65356875" x2="74.03013125" y2="26.652346875" width="0.225" layer="94" curve="3.952821"/>
<wire x1="74.171009375" y1="26.646109375" x2="74.100790625" y2="26.653571875" width="0.225" layer="94" curve="10.159745"/>
<wire x1="74.193740625" y1="26.63995" x2="74.171009375" y2="26.646103125" width="0.225" layer="94" curve="7.996993"/>
<wire x1="74.21501875" y1="26.62996875" x2="74.193740625" y2="26.63995" width="0.225" layer="94" curve="11.965711"/>
<wire x1="74.23181875" y1="26.61366875" x2="74.215021875" y2="26.629975" width="0.225" layer="94" curve="26.067092"/>
<wire x1="74.242540625" y1="26.59273125" x2="74.231821875" y2="26.613671875" width="0.225" layer="94" curve="11.432049"/>
<wire x1="74.254790625" y1="26.547290625" x2="74.2425375" y2="26.59273125" width="0.225" layer="94" curve="12.593993"/>
<wire x1="74.264359375" y1="26.422790625" x2="74.254784375" y2="26.547290625" width="0.225" layer="94" curve="8.794744"/>
<wire x1="73.92271875" y1="26.6134" x2="73.899178125" y2="26.547240625" width="0.225" layer="94" curve="20.872634"/>
<wire x1="73.89918125" y1="26.547240625" x2="73.88921875" y2="26.422790625" width="0.225" layer="94" curve="9.151622"/>
<wire x1="74.23521875" y1="25.698840625" x2="74.2678" y2="25.739" width="0.225" layer="94" curve="40.155698"/>
<wire x1="74.2678" y1="25.739" x2="74.271134375" y2="25.75288125" width="0.225" layer="94" curve="10.876304"/>
<wire x1="74.27435" y1="25.775459375" x2="74.271140625" y2="25.75288125" width="0.225" layer="94"/>
<wire x1="74.27638125" y1="25.82103125" x2="74.27435" y2="25.775459375" width="0.225" layer="94"/>
<wire x1="74.27638125" y1="25.98158125" x2="74.27638125" y2="25.964" width="0.225" layer="94"/>
<wire x1="74.27638125" y1="25.964" x2="74.27638125" y2="25.82103125" width="0.225" layer="94"/>
<wire x1="74.21361875" y1="25.691559375" x2="74.23521875" y2="25.6988375" width="0.225" layer="94" curve="9.286507"/>
<wire x1="74.16876875" y1="25.6834" x2="74.21361875" y2="25.691559375" width="0.225" layer="94" curve="7.336311"/>
<wire x1="73.964309375" y1="25.68853125" x2="74.16876875" y2="25.6834" width="0.225" layer="94" curve="16.159693"/>
<wire x1="73.94273125" y1="25.69585" x2="73.964309375" y2="25.68853125" width="0.225" layer="94" curve="18.441658"/>
<wire x1="73.91281875" y1="25.728490625" x2="73.942728125" y2="25.695846875" width="0.225" layer="94" curve="39.07874"/>
<wire x1="73.90078125" y1="25.77241875" x2="73.912828125" y2="25.72849375" width="0.225" layer="94" curve="15.249051"/>
<wire x1="73.894659375" y1="25.81763125" x2="73.90078125" y2="25.77241875" width="0.225" layer="94"/>
<wire x1="73.891490625" y1="25.863140625" x2="73.894659375" y2="25.81763125" width="0.225" layer="94"/>
<wire x1="73.88921875" y1="25.97718125" x2="73.88948125" y2="25.964" width="0.225" layer="94"/>
<wire x1="73.88948125" y1="25.964" x2="73.891490625" y2="25.863140625" width="0.225" layer="94"/>
<wire x1="73.88921875" y1="26.00881875" x2="73.88921875" y2="25.97718125" width="0.225" layer="94"/>
<wire x1="73.457240625" y1="25.980109375" x2="73.457240625" y2="26.189" width="0.225" layer="94"/>
<wire x1="73.457240625" y1="26.189" x2="73.457240625" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="73.457240625" y1="25.980109375" x2="73.47523125" y2="25.724359375" width="0.225" layer="94" curve="9.672758"/>
<wire x1="73.47523125" y1="25.724359375" x2="73.559184375" y2="25.48354375" width="0.225" layer="94" curve="20.717853"/>
<wire x1="73.55918125" y1="25.483540625" x2="73.749046875" y2="25.316425" width="0.225" layer="94" curve="38.137176"/>
<wire x1="73.74905" y1="25.31643125" x2="73.998040625" y2="25.26058125" width="0.225" layer="94" curve="19.282594"/>
<wire x1="73.998040625" y1="25.26058125" x2="74.25398125" y2="25.271940625" width="0.225" layer="94" curve="11.086392"/>
<wire x1="74.25398125" y1="25.271940625" x2="74.3331" y2="25.289" width="0.225" layer="94" curve="8.168179"/>
<wire x1="74.3331" y1="25.289" x2="74.491446875" y2="25.36319375" width="0.225" layer="94" curve="17.703003"/>
<wire x1="74.49145" y1="25.363190625" x2="74.642665625" y2="25.565709375" width="0.225" layer="94" curve="38.59175"/>
<wire x1="74.64266875" y1="25.565709375" x2="74.681409375" y2="25.8181" width="0.225" layer="94" curve="17.451998"/>
<wire x1="74.681409375" y1="25.98158125" x2="74.681409375" y2="25.964" width="0.225" layer="94"/>
<wire x1="74.681409375" y1="25.964" x2="74.681409375" y2="25.8181" width="0.225" layer="94"/>
<wire x1="74.27638125" y1="25.98158125" x2="74.681409375" y2="25.98158125" width="0.225" layer="94"/>
<wire x1="73.4785" y1="26.61343125" x2="73.45896875" y2="26.414" width="0.225" layer="94" curve="6.208134"/>
<wire x1="73.45896875" y1="26.414" x2="73.4572375" y2="26.34016875" width="0.225" layer="94" curve="2.287026"/>
<wire x1="73.58028125" y1="26.86573125" x2="73.4785" y2="26.61343125" width="0.225" layer="94" curve="26.545833"/>
<wire x1="73.8009" y1="27.02155" x2="73.58028125" y2="26.86573125" width="0.225" layer="94" curve="39.047008"/>
<wire x1="74.071640625" y1="27.05881875" x2="73.8009" y2="27.02155" width="0.225" layer="94" curve="15.743809"/>
<wire x1="74.342459375" y1="27.022209375" x2="74.071640625" y2="27.058825" width="0.225" layer="94" curve="15.331635"/>
<wire x1="74.562859375" y1="26.8662" x2="74.342459375" y2="27.022209375" width="0.225" layer="94" curve="39.85306"/>
<wire x1="74.66363125" y1="26.61348125" x2="74.5628625" y2="26.866203125" width="0.225" layer="94" curve="26.085633"/>
<wire x1="74.684409375" y1="26.34016875" x2="74.663628125" y2="26.61348125" width="0.225" layer="94" curve="8.69613"/>
<wire x1="74.684409375" y1="26.00881875" x2="74.684409375" y2="26.189" width="0.225" layer="94"/>
<wire x1="74.684409375" y1="26.189" x2="74.684409375" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="73.88921875" y1="26.00881875" x2="74.684409375" y2="26.00881875" width="0.225" layer="94"/>
<wire x1="73.88921875" y1="26.422790625" x2="74.264359375" y2="26.422790625" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="25.28548125" x2="82.92818125" y2="25.28548125" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="27.03186875" x2="82.80066875" y2="27.03186875" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="27.03186875" x2="82.508140625" y2="26.864" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="26.864" x2="82.508140625" y2="26.639" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="26.639" x2="82.508140625" y2="26.414" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="26.414" x2="82.508140625" y2="26.189" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="26.189" x2="82.508140625" y2="25.964" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="25.964" x2="82.508140625" y2="25.739" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="25.739" x2="82.508140625" y2="25.514" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="25.514" x2="82.508140625" y2="25.289" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="25.289" x2="82.508140625" y2="25.28548125" width="0.225" layer="94"/>
<wire x1="83.18028125" y1="26.30266875" x2="83.585309375" y2="26.30266875" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="26.358340625" x2="82.92818125" y2="26.189" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="26.189" x2="82.92818125" y2="25.964" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="25.964" x2="82.92818125" y2="25.739" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="25.739" x2="82.92818125" y2="25.514" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="25.514" x2="82.92818125" y2="25.289" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="25.289" x2="82.92818125" y2="25.28548125" width="0.225" layer="94"/>
<wire x1="82.8883" y1="26.98991875" x2="82.80066875" y2="27.03186875" width="0.225" layer="94" curve="51.162574"/>
<wire x1="86.810090625" y1="25.28548125" x2="86.810090625" y2="25.289" width="0.225" layer="94"/>
<wire x1="86.810090625" y1="25.289" x2="86.810090625" y2="25.514" width="0.225" layer="94"/>
<wire x1="86.810090625" y1="25.514" x2="86.810090625" y2="25.739" width="0.225" layer="94"/>
<wire x1="86.810090625" y1="25.739" x2="86.810090625" y2="25.964" width="0.225" layer="94"/>
<wire x1="86.810090625" y1="25.964" x2="86.810090625" y2="26.189" width="0.225" layer="94"/>
<wire x1="86.810090625" y1="26.189" x2="86.810090625" y2="26.25023125" width="0.225" layer="94"/>
<wire x1="86.39005" y1="25.28548125" x2="86.39005" y2="25.289" width="0.225" layer="94"/>
<wire x1="86.39005" y1="25.289" x2="86.39005" y2="25.514" width="0.225" layer="94"/>
<wire x1="86.39005" y1="25.514" x2="86.39005" y2="25.739" width="0.225" layer="94"/>
<wire x1="86.39005" y1="25.739" x2="86.39005" y2="25.964" width="0.225" layer="94"/>
<wire x1="86.39005" y1="25.964" x2="86.39005" y2="26.189" width="0.225" layer="94"/>
<wire x1="86.39005" y1="26.189" x2="86.39005" y2="26.414" width="0.225" layer="94"/>
<wire x1="86.39005" y1="26.414" x2="86.39005" y2="26.639" width="0.225" layer="94"/>
<wire x1="86.39005" y1="26.639" x2="86.39005" y2="26.864" width="0.225" layer="94"/>
<wire x1="86.39005" y1="26.864" x2="86.39005" y2="27.03186875" width="0.225" layer="94"/>
<wire x1="86.39005" y1="25.28548125" x2="86.810090625" y2="25.28548125" width="0.225" layer="94"/>
<wire x1="87.1462" y1="25.28548125" x2="87.566240625" y2="25.28548125" width="0.225" layer="94"/>
<wire x1="86.68258125" y1="27.03186875" x2="86.39005" y2="27.03186875" width="0.225" layer="94"/>
<wire x1="86.770490625" y1="26.98956875" x2="86.68258125" y2="27.03186875" width="0.225" layer="94" curve="51.389654"/>
<wire x1="83.585309375" y1="26.30266875" x2="83.585309375" y2="26.414" width="0.225" layer="94"/>
<wire x1="83.585309375" y1="26.414" x2="83.585309375" y2="26.532359375" width="0.225" layer="94"/>
<wire x1="83.585309375" y1="26.532359375" x2="83.57331875" y2="26.7009" width="0.225" layer="94" curve="8.13878"/>
<wire x1="83.57331875" y1="26.7009" x2="83.52315625" y2="26.8617" width="0.225" layer="94" curve="18.374099"/>
<wire x1="83.523159375" y1="26.8617" x2="83.41396875" y2="26.9887" width="0.225" layer="94" curve="28.349931"/>
<wire x1="83.41396875" y1="26.9887" x2="83.25806875" y2="27.050721875" width="0.225" layer="94" curve="26.884991"/>
<wire x1="83.25806875" y1="27.05071875" x2="83.0895" y2="27.055371875" width="0.225" layer="94" curve="13.343084"/>
<wire x1="83.0895" y1="27.05538125" x2="82.888296875" y2="26.989925" width="0.225" layer="94" curve="25.861473"/>
<wire x1="87.566240625" y1="25.28548125" x2="87.566240625" y2="25.289" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="25.289" x2="87.566240625" y2="25.514" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="25.514" x2="87.566240625" y2="25.739" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="25.739" x2="87.566240625" y2="25.964" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="25.964" x2="87.566240625" y2="26.189" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="26.189" x2="87.566240625" y2="26.414" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="26.414" x2="87.566240625" y2="26.418390625" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="26.418390625" x2="87.555215625" y2="26.606540625" width="0.225" layer="94" curve="6.706552"/>
<wire x1="87.55521875" y1="26.606540625" x2="87.509115625" y2="26.788871875" width="0.225" layer="94" curve="14.968121"/>
<wire x1="87.509109375" y1="26.78886875" x2="87.403925" y2="26.943603125" width="0.225" layer="94" curve="25.064472"/>
<wire x1="87.40393125" y1="26.943609375" x2="87.240590625" y2="27.034540625" width="0.225" layer="94" curve="28.312195"/>
<wire x1="87.240590625" y1="27.034540625" x2="87.05421875" y2="27.058825" width="0.225" layer="94" curve="15.049352"/>
<wire x1="87.05421875" y1="27.05881875" x2="86.770490625" y2="26.98956875" width="0.225" layer="94" curve="27.231353"/>
<wire x1="87.1462" y1="25.28548125" x2="87.1462" y2="25.289" width="0.225" layer="94"/>
<wire x1="87.1462" y1="25.289" x2="87.1462" y2="25.514" width="0.225" layer="94"/>
<wire x1="87.1462" y1="25.514" x2="87.1462" y2="25.739" width="0.225" layer="94"/>
<wire x1="87.1462" y1="25.739" x2="87.1462" y2="25.964" width="0.225" layer="94"/>
<wire x1="87.1462" y1="25.964" x2="87.1462" y2="26.189" width="0.225" layer="94"/>
<wire x1="87.1462" y1="26.189" x2="87.1462" y2="26.3642" width="0.225" layer="94"/>
<wire x1="87.1462" y1="26.3642" x2="87.139771875" y2="26.502909375" width="0.225" layer="94" curve="5.307835"/>
<wire x1="87.13976875" y1="26.502909375" x2="87.123915625" y2="26.584609375" width="0.225" layer="94" curve="11.346275"/>
<wire x1="87.12391875" y1="26.584609375" x2="87.11383125" y2="26.61046875" width="0.225" layer="94" curve="9.30383"/>
<wire x1="87.11383125" y1="26.61046875" x2="87.098490625" y2="26.633490625" width="0.225" layer="94" curve="15.447128"/>
<wire x1="87.098490625" y1="26.633490625" x2="87.0752" y2="26.648190625" width="0.225" layer="94" curve="32.662799"/>
<wire x1="87.0752" y1="26.648190625" x2="86.99275" y2="26.656371875" width="0.225" layer="94" curve="20.527762"/>
<wire x1="86.99275" y1="26.65636875" x2="86.9651" y2="26.653715625" width="0.225" layer="94" curve="1.764695"/>
<wire x1="86.9651" y1="26.65371875" x2="86.912034375" y2="26.638084375" width="0.225" layer="94" curve="20.107123"/>
<wire x1="86.91203125" y1="26.638090625" x2="86.870678125" y2="26.601821875" width="0.225" layer="94" curve="29.565942"/>
<wire x1="86.87068125" y1="26.60181875" x2="86.84560625" y2="26.55240625" width="0.225" layer="94" curve="14.114289"/>
<wire x1="86.8456" y1="26.552409375" x2="86.824978125" y2="26.471759375" width="0.225" layer="94" curve="11.013017"/>
<wire x1="86.82498125" y1="26.471759375" x2="86.813053125" y2="26.36131875" width="0.225" layer="94" curve="5.344301"/>
<wire x1="86.81305" y1="26.36131875" x2="86.810090625" y2="26.25023125" width="0.225" layer="94" curve="3.932748"/>
<wire x1="83.18028125" y1="26.30266875" x2="83.18028125" y2="26.414" width="0.225" layer="94"/>
<wire x1="83.18028125" y1="26.414" x2="83.18028125" y2="26.487240625" width="0.225" layer="94"/>
<wire x1="83.18028125" y1="26.487240625" x2="83.176690625" y2="26.56661875" width="0.225" layer="94" curve="5.181151"/>
<wire x1="83.176690625" y1="26.56661875" x2="83.16605625" y2="26.625190625" width="0.225" layer="94" curve="10.220705"/>
<wire x1="83.166059375" y1="26.625190625" x2="83.12728125" y2="26.664134375" width="0.225" layer="94" curve="58.95127"/>
<wire x1="83.12728125" y1="26.66413125" x2="82.99755" y2="26.639" width="0.225" layer="94" curve="53.217192"/>
<wire x1="82.99755" y1="26.639" x2="82.95845" y2="26.593540625" width="0.225" layer="94" curve="23.452395"/>
<wire x1="82.95845" y1="26.593540625" x2="82.95161875" y2="26.574890625" width="0.225" layer="94"/>
<wire x1="82.95161875" y1="26.574890625" x2="82.946159375" y2="26.55578125" width="0.225" layer="94"/>
<wire x1="82.946159375" y1="26.55578125" x2="82.941775" y2="26.5364" width="0.225" layer="94" curve="6.402047"/>
<wire x1="82.94176875" y1="26.5364" x2="82.931515625" y2="26.45761875" width="0.225" layer="94" curve="4.264451"/>
<wire x1="82.93151875" y1="26.45761875" x2="82.930209375" y2="26.437790625" width="0.225" layer="94"/>
<wire x1="82.930209375" y1="26.437790625" x2="82.92865" y2="26.39808125" width="0.225" layer="94"/>
<wire x1="82.92865" y1="26.39808125" x2="82.92818125" y2="26.358340625" width="0.225" layer="94"/>
<wire x1="92.67961875" y1="26.22455" x2="92.54725" y2="26.31605" width="0.225" layer="94" curve="24.406928"/>
<wire x1="92.54725" y1="26.31605" x2="92.391190625" y2="26.358428125" width="0.225" layer="94" curve="14.517731"/>
<wire x1="92.391190625" y1="26.35841875" x2="92.2218" y2="26.3835" width="0.225" layer="94"/>
<wire x1="92.2218" y1="26.3835" x2="92.13726875" y2="26.39681875" width="0.225" layer="94"/>
<wire x1="92.09533125" y1="26.40531875" x2="92.13726875" y2="26.39681875" width="0.225" layer="94" curve="5.000072"/>
<wire x1="92.0546" y1="26.41821875" x2="92.0661" y2="26.414" width="0.225" layer="94" curve="2.068384"/>
<wire x1="92.0661" y1="26.414" x2="92.09533125" y2="26.405321875" width="0.225" layer="94" curve="5.150249"/>
<wire x1="92.054009375" y1="26.430409375" x2="92.054596875" y2="26.41821875" width="0.225" layer="94" curve="5.514941"/>
<wire x1="92.054009375" y1="26.430409375" x2="92.054009375" y2="26.57718125" width="0.225" layer="94"/>
<wire x1="92.05665" y1="26.61703125" x2="92.0540125" y2="26.57718125" width="0.225" layer="94" curve="7.573746"/>
<wire x1="92.061659375" y1="26.63633125" x2="92.056646875" y2="26.61703125" width="0.225" layer="94" curve="13.971436"/>
<wire x1="92.068809375" y1="26.642740625" x2="92.0616625" y2="26.636328125" width="0.225" layer="94" curve="41.625207"/>
<wire x1="92.097790625" y1="26.650090625" x2="92.068809375" y2="26.642740625" width="0.225" layer="94" curve="13.721902"/>
<wire x1="92.15758125" y1="26.653940625" x2="92.097790625" y2="26.650090625" width="0.225" layer="94" curve="7.371099"/>
<wire x1="92.15758125" y1="26.653940625" x2="92.2566" y2="26.653940625" width="0.225" layer="94"/>
<wire x1="92.29483125" y1="26.652240625" x2="92.2566" y2="26.653940625" width="0.225" layer="94" curve="5.089448"/>
<wire x1="92.32143125" y1="26.647590625" x2="92.29483125" y2="26.652240625" width="0.225" layer="94" curve="9.654072"/>
<wire x1="92.331859375" y1="26.64346875" x2="92.32143125" y2="26.6475875" width="0.225" layer="94" curve="13.639596"/>
<wire x1="92.33591875" y1="26.63303125" x2="92.33185625" y2="26.64346875" width="0.225" layer="94" curve="15.08669"/>
<wire x1="92.34036875" y1="26.6065" x2="92.335915625" y2="26.63303125" width="0.225" layer="94" curve="8.390546"/>
<wire x1="92.34215" y1="26.568390625" x2="92.340375" y2="26.6065" width="0.225" layer="94" curve="5.332675"/>
<wire x1="92.34215" y1="26.568390625" x2="92.34215" y2="26.414" width="0.225" layer="94"/>
<wire x1="92.34215" y1="26.414" x2="92.34215" y2="26.413709375" width="0.225" layer="94"/>
<wire x1="92.34215" y1="26.413709375" x2="92.759190625" y2="26.413709375" width="0.225" layer="94"/>
<wire x1="92.759190625" y1="26.413709375" x2="92.759190625" y2="26.414" width="0.225" layer="94"/>
<wire x1="92.759190625" y1="26.414" x2="92.759190625" y2="26.5892" width="0.225" layer="94"/>
<wire x1="92.759190625" y1="26.5892" x2="92.738496875" y2="26.78398125" width="0.225" layer="94" curve="12.129007"/>
<wire x1="92.738490625" y1="26.78398125" x2="92.642896875" y2="26.952528125" width="0.225" layer="94" curve="34.862523"/>
<wire x1="92.469709375" y1="27.040159375" x2="92.27461875" y2="27.058825" width="0.225" layer="94" curve="10.407762"/>
<wire x1="92.27461875" y1="27.05881875" x2="92.139559375" y2="27.05881875" width="0.225" layer="94"/>
<wire x1="92.139559375" y1="27.05881875" x2="91.94439375" y2="27.0367625" width="0.225" layer="94" curve="15.066235"/>
<wire x1="91.944390625" y1="27.03676875" x2="91.76905625" y2="26.950671875" width="0.225" layer="94" curve="24.344315"/>
<wire x1="91.769059375" y1="26.95066875" x2="91.656853125" y2="26.79153125" width="0.225" layer="94" curve="32.9752"/>
<wire x1="91.65685" y1="26.79153125" x2="91.626409375" y2="26.639" width="0.225" layer="94" curve="14.82531"/>
<wire x1="91.626409375" y1="26.639" x2="91.62503125" y2="26.59828125" width="0.225" layer="94" curve="3.873269"/>
<wire x1="91.62503125" y1="26.59828125" x2="91.62503125" y2="26.414" width="0.225" layer="94"/>
<wire x1="91.62503125" y1="26.414" x2="91.62503125" y2="26.39436875" width="0.225" layer="94"/>
<wire x1="91.62503125" y1="26.39436875" x2="91.654403125" y2="26.22111875" width="0.225" layer="94" curve="19.244585"/>
<wire x1="91.6544" y1="26.22111875" x2="91.767428125" y2="26.088775" width="0.225" layer="94" curve="42.510013"/>
<wire x1="91.76743125" y1="26.08878125" x2="91.931890625" y2="26.02583125" width="0.225" layer="94" curve="14.601117"/>
<wire x1="91.931890625" y1="26.02583125" x2="92.105790625" y2="25.992246875" width="0.225" layer="94" curve="5.428189"/>
<wire x1="92.105790625" y1="25.99225" x2="92.16531875" y2="25.98243125" width="0.225" layer="94"/>
<wire x1="92.16531875" y1="25.98243125" x2="92.22491875" y2="25.97266875" width="0.225" layer="94"/>
<wire x1="92.22491875" y1="25.97266875" x2="92.28428125" y2="25.96156875" width="0.225" layer="94"/>
<wire x1="92.327340625" y1="25.94943125" x2="92.28428125" y2="25.9615625" width="0.225" layer="94" curve="10.274433"/>
<wire x1="92.34125" y1="25.94243125" x2="92.32734375" y2="25.9494375" width="0.225" layer="94" curve="11.730311"/>
<wire x1="92.346959375" y1="25.91516875" x2="92.34124375" y2="25.942428125" width="0.225" layer="94" curve="13.906979"/>
<wire x1="92.34815" y1="25.887240625" x2="92.346959375" y2="25.91516875" width="0.225" layer="94" curve="4.888175"/>
<wire x1="92.34815" y1="25.887240625" x2="92.34815" y2="25.7431" width="0.225" layer="94"/>
<wire x1="92.346759375" y1="25.71286875" x2="92.348153125" y2="25.7431" width="0.225" layer="94" curve="5.283856"/>
<wire x1="92.34045" y1="25.683359375" x2="92.34675625" y2="25.71286875" width="0.225" layer="94" curve="13.55484"/>
<wire x1="92.333959375" y1="25.67608125" x2="92.340453125" y2="25.683359375" width="0.225" layer="94" curve="45.791754"/>
<wire x1="92.314840625" y1="25.66976875" x2="92.333959375" y2="25.676084375" width="0.225" layer="94" curve="14.183972"/>
<wire x1="92.274840625" y1="25.66463125" x2="92.314840625" y2="25.669771875" width="0.225" layer="94" curve="7.7277"/>
<wire x1="92.24416875" y1="25.663709375" x2="92.274840625" y2="25.664628125" width="0.225" layer="94" curve="3.484382"/>
<wire x1="92.24416875" y1="25.663709375" x2="92.1096" y2="25.663709375" width="0.225" layer="94"/>
<wire x1="92.07173125" y1="25.66515" x2="92.1096" y2="25.66370625" width="0.225" layer="94" curve="4.363033"/>
<wire x1="92.04318125" y1="25.66956875" x2="92.07173125" y2="25.66514375" width="0.225" layer="94" curve="8.889144"/>
<wire x1="92.03463125" y1="25.672309375" x2="92.04318125" y2="25.66956875" width="0.225" layer="94" curve="9.043139"/>
<wire x1="92.03186875" y1="25.680859375" x2="92.034628125" y2="25.672309375" width="0.225" layer="94" curve="7.986668"/>
<wire x1="92.02805" y1="25.70145" x2="92.031871875" y2="25.680859375" width="0.225" layer="94" curve="6.761788"/>
<wire x1="92.02548125" y1="25.73128125" x2="92.028053125" y2="25.70145" width="0.225" layer="94" curve="4.406702"/>
<wire x1="92.024059375" y1="25.79115" x2="92.025484375" y2="25.73128125" width="0.225" layer="94" curve="2.724283"/>
<wire x1="92.024059375" y1="25.79115" x2="92.024059375" y2="25.964" width="0.225" layer="94"/>
<wire x1="92.024059375" y1="25.964" x2="92.024059375" y2="25.99651875" width="0.225" layer="94"/>
<wire x1="92.024059375" y1="25.99651875" x2="91.60701875" y2="25.99651875" width="0.225" layer="94"/>
<wire x1="91.60701875" y1="25.99651875" x2="91.60701875" y2="25.964" width="0.225" layer="94"/>
<wire x1="91.60701875" y1="25.964" x2="91.60701875" y2="25.739" width="0.225" layer="94"/>
<wire x1="91.60701875" y1="25.739" x2="91.60701875" y2="25.728159375" width="0.225" layer="94"/>
<wire x1="91.60701875" y1="25.728159375" x2="91.6277125" y2="25.53338125" width="0.225" layer="94" curve="12.128778"/>
<wire x1="91.627709375" y1="25.53338125" x2="91.72330625" y2="25.364828125" width="0.225" layer="94" curve="34.863434"/>
<wire x1="91.723309375" y1="25.36483125" x2="91.85098125" y2="25.289" width="0.225" layer="94" curve="24.602823"/>
<wire x1="91.85098125" y1="25.289" x2="91.896490625" y2="25.277203125" width="0.225" layer="94" curve="7.735491"/>
<wire x1="91.896490625" y1="25.2772" x2="92.091590625" y2="25.258534375" width="0.225" layer="94" curve="10.40831"/>
<wire x1="92.091590625" y1="25.25853125" x2="92.262609375" y2="25.25853125" width="0.225" layer="94"/>
<wire x1="92.262609375" y1="25.25853125" x2="92.457790625" y2="25.2805875" width="0.225" layer="94" curve="15.067174"/>
<wire x1="92.457790625" y1="25.280590625" x2="92.633140625" y2="25.36668125" width="0.225" layer="94" curve="24.336812"/>
<wire x1="92.633140625" y1="25.36668125" x2="92.74113125" y2="25.514" width="0.225" layer="94" curve="30.878359"/>
<wire x1="92.74113125" y1="25.514" x2="92.74536875" y2="25.525809375" width="0.225" layer="94" curve="2.095521"/>
<wire x1="92.74536875" y1="25.525809375" x2="92.777203125" y2="25.71908125" width="0.225" layer="94" curve="18.707757"/>
<wire x1="92.777209375" y1="25.71908125" x2="92.777209375" y2="25.739" width="0.225" layer="94"/>
<wire x1="92.777209375" y1="25.739" x2="92.777209375" y2="25.92328125" width="0.225" layer="94"/>
<wire x1="92.777209375" y1="25.92328125" x2="92.756978125" y2="26.083621875" width="0.225" layer="94" curve="14.383055"/>
<wire x1="92.75696875" y1="26.08361875" x2="92.6796125" y2="26.22454375" width="0.225" layer="94" curve="28.759193"/>
<wire x1="72.94583125" y1="31.589" x2="72.950859375" y2="31.589" width="0.225" layer="94"/>
<wire x1="73.958459375" y1="26.639" x2="74.19638125" y2="26.639" width="0.225" layer="94"/>
<wire x1="92.06346875" y1="26.639" x2="92.334359375" y2="26.639" width="0.225" layer="94"/>
<wire x1="77.24345" y1="26.639" x2="77.48136875" y2="26.639" width="0.225" layer="94"/>
<wire x1="86.913890625" y1="26.639" x2="87.09276875" y2="26.639" width="0.225" layer="94"/>
<wire x1="82.99755" y1="26.639" x2="83.160159375" y2="26.639" width="0.225" layer="94"/>
<wire x1="92.0661" y1="26.414" x2="92.34215" y2="26.414" width="0.225" layer="94"/>
<wire x1="82.92926875" y1="26.414" x2="83.18028125" y2="26.414" width="0.225" layer="94"/>
<wire x1="73.88948125" y1="25.964" x2="74.27638125" y2="25.964" width="0.225" layer="94"/>
<wire x1="92.024059375" y1="25.964" x2="92.27126875" y2="25.964" width="0.225" layer="94"/>
<wire x1="77.17446875" y1="25.964" x2="77.561359375" y2="25.964" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="25.964" x2="81.341240625" y2="25.964" width="0.225" layer="94"/>
<wire x1="73.90876875" y1="25.739" x2="74.2678" y2="25.739" width="0.225" layer="94"/>
<wire x1="92.025140625" y1="25.739" x2="92.34813125" y2="25.739" width="0.225" layer="94"/>
<wire x1="77.193759375" y1="25.739" x2="77.55278125" y2="25.739" width="0.225" layer="94"/>
<wire x1="81.209909375" y1="25.739" x2="81.340009375" y2="25.739" width="0.225" layer="94"/>
<rectangle x1="93.287040625" y1="29.45076875" x2="93.399540625" y2="33.34871875" layer="94"/>
<rectangle x1="94.749840625" y1="29.45076875" x2="94.862340625" y2="33.34871875" layer="94"/>
<rectangle x1="73.776721875" y1="26.310290625" x2="74.376859375" y2="26.422790625" layer="94"/>
<rectangle x1="77.0617" y1="26.310290625" x2="77.66185" y2="26.422790625" layer="94"/>
<rectangle x1="73.77671875" y1="26.00881875" x2="74.685" y2="26.12131875" layer="94"/>
<rectangle x1="77.0617" y1="26.00881875" x2="77.985" y2="26.12131875" layer="94"/>
<text x="50.8" y="12.7" size="5.08" layer="94" ratio="10" align="bottom-center">&gt;VALUE</text>
<text x="1.27" y="33.02" size="1.9304" layer="94">Vehicle Name:</text>
<text x="41.91" y="30.48" size="5.08" layer="94" align="top-center">&gt;VEHICLE_NUMBER</text>
<text x="41.91" y="33.02" size="2.54" layer="94" align="bottom-center">&gt;VEHICLE_NAME</text>
<text x="1.27" y="21.59" size="1.9304" layer="94" align="top-left">Sheet Name:</text>
<text x="1.27" y="24.13" size="1.9304" layer="94">ABRA:</text>
<text x="10.16" y="24.13" size="1.9304" layer="94">&gt;ABRA_GLOBAL</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="DINA3_L-AUTO" prefix="FRAME" uservalue="yes">
<description>&lt;b&gt;VEIT FRAME&lt;/b&gt;&lt;p&gt;
DIN A3, landscape with extra doc field,&lt;br&gt;
customized for vehicle schematics</description>
<gates>
<gate name="G$1" symbol="DINA3_L" x="0" y="0"/>
<gate name="G$2" symbol="DOCFIELD-AUTA" x="287.02" y="0" addlevel="request"/>
<gate name="G$3" symbol="DOCFIELD-AUTA-ECO-LOGO" x="388.62" y="0" addlevel="request"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="AUTO-schema">
<packages>
<package name="1X02">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-1.905" y1="1.27" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<pad name="L" x="-1.27" y="0" drill="1.1" diameter="1.8" shape="octagon" rot="R90"/>
<pad name="H" x="1.27" y="0" drill="1.1" diameter="1.8" rot="R90"/>
<text x="-2.6162" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
</package>
<package name="OTOCNY_PREPINAC">
<pad name="1" x="0" y="0" drill="0.8" shape="square"/>
<pad name="2" x="0" y="-2.54" drill="0.8" shape="square"/>
<pad name="3" x="0" y="-5.08" drill="0.8" shape="square"/>
<pad name="4" x="0" y="-7.62" drill="0.8" shape="square"/>
<pad name="5" x="0" y="-10.16" drill="0.8" shape="square"/>
<pad name="6" x="0" y="-12.7" drill="0.8" shape="square"/>
<pad name="7" x="0" y="-15.24" drill="0.8" shape="square"/>
<pad name="8" x="0" y="-17.78" drill="0.8" shape="square"/>
<pad name="9" x="0" y="-20.32" drill="0.8" shape="square"/>
<pad name="10" x="0" y="-22.86" drill="0.8" shape="square"/>
<pad name="11" x="6.35" y="0" drill="0.8" shape="square"/>
<pad name="12" x="6.35" y="-2.54" drill="0.8" shape="square"/>
<pad name="13" x="6.35" y="-5.08" drill="0.8" shape="square"/>
<pad name="14" x="6.35" y="-7.62" drill="0.8" shape="square"/>
<pad name="15" x="6.35" y="-10.16" drill="0.8" shape="square"/>
<pad name="16" x="6.35" y="-12.7" drill="0.8" shape="square"/>
<pad name="17" x="6.35" y="-15.24" drill="0.8" shape="square"/>
<pad name="18" x="6.35" y="-17.78" drill="0.8" shape="square"/>
<pad name="19" x="6.35" y="-20.32" drill="0.8" shape="square"/>
<pad name="20" x="6.35" y="-22.86" drill="0.8" shape="square"/>
<pad name="21" x="12.7" y="0" drill="0.8" shape="square"/>
<pad name="22" x="12.7" y="-2.54" drill="0.8" shape="square"/>
<pad name="23" x="12.7" y="-5.08" drill="0.8" shape="square"/>
<pad name="24" x="12.7" y="-7.62" drill="0.8" shape="square"/>
<pad name="25" x="12.7" y="-10.16" drill="0.8" shape="square"/>
<pad name="26" x="12.7" y="-12.7" drill="0.8" shape="square"/>
<pad name="27" x="12.7" y="-15.24" drill="0.8" shape="square"/>
<pad name="28" x="12.7" y="-17.78" drill="0.8" shape="square"/>
<pad name="29" x="12.7" y="-20.32" drill="0.8" shape="square"/>
<pad name="30" x="12.7" y="-22.86" drill="0.8" shape="square"/>
<pad name="31" x="19.05" y="0" drill="0.8" shape="square"/>
<pad name="32" x="19.05" y="-2.54" drill="0.8" shape="square"/>
<pad name="33" x="19.05" y="-5.08" drill="0.8" shape="square"/>
<pad name="34" x="19.05" y="-7.62" drill="0.8" shape="square"/>
<pad name="35" x="19.05" y="-10.16" drill="0.8" shape="square"/>
<pad name="36" x="19.05" y="-12.7" drill="0.8" shape="square"/>
<pad name="37" x="19.05" y="-15.24" drill="0.8" shape="square"/>
<pad name="38" x="19.05" y="-17.78" drill="0.8" shape="square"/>
<pad name="39" x="19.05" y="-20.32" drill="0.8" shape="square"/>
<pad name="40" x="19.05" y="-22.86" drill="0.8" shape="square"/>
</package>
</packages>
<symbols>
<symbol name="AUTOSTART-KUBOTA-KONEKTORY">
<wire x1="31.75" y1="27.94" x2="31.75" y2="28.2575" width="0.254" layer="94"/>
<wire x1="31.75" y1="28.2575" x2="31.75" y2="28.575" width="0.254" layer="94"/>
<wire x1="31.75" y1="28.575" x2="31.75" y2="30.48" width="0.254" layer="94"/>
<wire x1="31.75" y1="30.48" x2="29.21" y2="30.48" width="0.254" layer="94"/>
<wire x1="29.21" y1="30.48" x2="26.67" y2="30.48" width="0.254" layer="94"/>
<wire x1="26.67" y1="30.48" x2="24.13" y2="30.48" width="0.254" layer="94"/>
<wire x1="24.13" y1="30.48" x2="21.59" y2="30.48" width="0.254" layer="94"/>
<wire x1="21.59" y1="30.48" x2="19.05" y2="30.48" width="0.254" layer="94"/>
<wire x1="19.05" y1="30.48" x2="16.51" y2="30.48" width="0.254" layer="94"/>
<wire x1="16.51" y1="30.48" x2="16.51" y2="27.94" width="0.254" layer="94"/>
<text x="30.48" y="26.67" size="1.4224" layer="94" rot="R90" align="center">1</text>
<text x="27.94" y="26.67" size="1.4224" layer="94" rot="R90" align="center">2</text>
<text x="25.4" y="26.67" size="1.4224" layer="94" rot="R90" align="center">3</text>
<text x="22.86" y="26.67" size="1.4224" layer="94" rot="R90" align="center">4</text>
<text x="20.32" y="26.67" size="1.4224" layer="94" rot="R90" align="center">5</text>
<text x="17.78" y="26.67" size="1.4224" layer="94" rot="R90" align="center">6</text>
<text x="30.48" y="29.21" size="1.4224" layer="94" rot="R90" align="center">7</text>
<text x="27.94" y="29.21" size="1.4224" layer="94" rot="R90" align="center">8</text>
<text x="25.4" y="29.21" size="1.4224" layer="94" rot="R90" align="center">9</text>
<text x="22.86" y="29.21" size="1.4224" layer="94" rot="R90" align="center">10</text>
<text x="33.02" y="27.94" size="1.27" layer="96" rot="R90" align="top-center">MINIFIT12</text>
<text x="15.24" y="27.94" size="1.778" layer="95" align="center-right">JP2</text>
<text x="20.32" y="29.21" size="1.4224" layer="94" rot="R90" align="center">11</text>
<text x="17.78" y="29.21" size="1.4224" layer="94" rot="R90" align="center">12</text>
<pin name="JP2.1" x="30.48" y="22.86" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="JP2.2" x="27.94" y="22.86" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="JP2.3" x="25.4" y="22.86" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="JP2.4" x="22.86" y="22.86" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="JP2.5" x="20.32" y="22.86" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="JP2.6" x="17.78" y="22.86" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="JP2.7" x="30.48" y="33.02" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="JP2.8" x="27.94" y="33.02" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="JP2.9" x="25.4" y="33.02" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="JP2.10" x="22.86" y="33.02" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="JP2.11" x="20.32" y="33.02" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="JP2.12" x="17.78" y="33.02" visible="off" length="short" direction="pas" rot="R270"/>
<rectangle x1="23.749" y1="29.845" x2="24.511" y2="31.877" layer="94" rot="R90"/>
<wire x1="31.75" y1="27.94" x2="31.75" y2="25.4" width="0.254" layer="94"/>
<wire x1="31.75" y1="25.4" x2="29.21" y2="25.4" width="0.254" layer="94"/>
<wire x1="29.21" y1="25.4" x2="28.8925" y2="25.4" width="0.254" layer="94"/>
<wire x1="28.8925" y1="25.4" x2="28.575" y2="25.4" width="0.254" layer="94"/>
<wire x1="28.575" y1="25.4" x2="27.305" y2="25.4" width="0.254" layer="94"/>
<wire x1="27.305" y1="25.4" x2="26.9875" y2="25.4" width="0.254" layer="94"/>
<wire x1="26.9875" y1="25.4" x2="26.67" y2="25.4" width="0.254" layer="94"/>
<wire x1="26.67" y1="25.4" x2="26.3525" y2="25.4" width="0.254" layer="94"/>
<wire x1="26.3525" y1="25.4" x2="26.035" y2="25.4" width="0.254" layer="94"/>
<wire x1="26.035" y1="25.4" x2="24.765" y2="25.4" width="0.254" layer="94"/>
<wire x1="24.765" y1="25.4" x2="24.4475" y2="25.4" width="0.254" layer="94"/>
<wire x1="24.4475" y1="25.4" x2="24.13" y2="25.4" width="0.254" layer="94"/>
<wire x1="24.13" y1="25.4" x2="21.59" y2="25.4" width="0.254" layer="94"/>
<wire x1="21.59" y1="25.4" x2="19.05" y2="25.4" width="0.254" layer="94"/>
<wire x1="19.05" y1="25.4" x2="18.7325" y2="25.4" width="0.254" layer="94"/>
<wire x1="18.7325" y1="25.4" x2="18.415" y2="25.4" width="0.254" layer="94"/>
<wire x1="18.415" y1="25.4" x2="17.145" y2="25.4" width="0.254" layer="94"/>
<wire x1="17.145" y1="25.4" x2="16.8275" y2="25.4" width="0.254" layer="94"/>
<wire x1="16.8275" y1="25.4" x2="16.51" y2="25.4" width="0.254" layer="94"/>
<wire x1="16.51" y1="25.4" x2="16.51" y2="25.7175" width="0.254" layer="94"/>
<wire x1="16.51" y1="25.7175" x2="16.51" y2="26.035" width="0.254" layer="94"/>
<wire x1="16.51" y1="26.035" x2="16.51" y2="27.94" width="0.254" layer="94"/>
<wire x1="16.51" y1="27.94" x2="19.05" y2="27.94" width="0.254" layer="94"/>
<wire x1="19.05" y1="27.94" x2="19.3675" y2="27.94" width="0.254" layer="94"/>
<wire x1="19.3675" y1="27.94" x2="19.685" y2="27.94" width="0.254" layer="94"/>
<wire x1="19.685" y1="27.94" x2="20.955" y2="27.94" width="0.254" layer="94"/>
<wire x1="20.955" y1="27.94" x2="21.2725" y2="27.94" width="0.254" layer="94"/>
<wire x1="21.2725" y1="27.94" x2="21.59" y2="27.94" width="0.254" layer="94"/>
<wire x1="21.59" y1="27.94" x2="21.9075" y2="27.94" width="0.254" layer="94"/>
<wire x1="21.9075" y1="27.94" x2="22.225" y2="27.94" width="0.254" layer="94"/>
<wire x1="22.225" y1="27.94" x2="23.495" y2="27.94" width="0.254" layer="94"/>
<wire x1="23.495" y1="27.94" x2="23.8125" y2="27.94" width="0.254" layer="94"/>
<wire x1="23.8125" y1="27.94" x2="24.13" y2="27.94" width="0.254" layer="94"/>
<wire x1="24.13" y1="27.94" x2="26.67" y2="27.94" width="0.254" layer="94"/>
<wire x1="26.67" y1="27.94" x2="29.21" y2="27.94" width="0.254" layer="94"/>
<wire x1="29.21" y1="27.94" x2="29.5275" y2="27.94" width="0.254" layer="94"/>
<wire x1="29.5275" y1="27.94" x2="29.845" y2="27.94" width="0.254" layer="94"/>
<wire x1="29.845" y1="27.94" x2="31.115" y2="27.94" width="0.254" layer="94"/>
<wire x1="31.115" y1="27.94" x2="31.4325" y2="27.94" width="0.254" layer="94"/>
<wire x1="31.4325" y1="27.94" x2="31.75" y2="27.94" width="0.254" layer="94"/>
<wire x1="29.21" y1="25.4" x2="29.21" y2="25.7175" width="0.254" layer="94"/>
<wire x1="29.21" y1="25.7175" x2="29.21" y2="26.035" width="0.254" layer="94"/>
<wire x1="29.21" y1="26.035" x2="29.21" y2="27.94" width="0.254" layer="94"/>
<wire x1="29.21" y1="27.94" x2="29.21" y2="28.2575" width="0.254" layer="94"/>
<wire x1="29.21" y1="28.2575" x2="29.21" y2="28.575" width="0.254" layer="94"/>
<wire x1="29.21" y1="28.575" x2="29.21" y2="30.48" width="0.254" layer="94"/>
<wire x1="26.67" y1="25.4" x2="26.67" y2="25.7175" width="0.254" layer="94"/>
<wire x1="26.67" y1="25.7175" x2="26.67" y2="26.035" width="0.254" layer="94"/>
<wire x1="26.67" y1="26.035" x2="26.67" y2="27.94" width="0.254" layer="94"/>
<wire x1="26.67" y1="27.94" x2="26.67" y2="30.48" width="0.254" layer="94"/>
<wire x1="24.13" y1="25.4" x2="24.13" y2="25.7175" width="0.254" layer="94"/>
<wire x1="24.13" y1="25.7175" x2="24.13" y2="26.035" width="0.254" layer="94"/>
<wire x1="24.13" y1="26.035" x2="24.13" y2="27.94" width="0.254" layer="94"/>
<wire x1="24.13" y1="27.94" x2="24.13" y2="28.2575" width="0.254" layer="94"/>
<wire x1="24.13" y1="28.2575" x2="24.13" y2="28.575" width="0.254" layer="94"/>
<wire x1="24.13" y1="28.575" x2="24.13" y2="30.48" width="0.254" layer="94"/>
<wire x1="21.59" y1="25.4" x2="21.59" y2="27.94" width="0.254" layer="94"/>
<wire x1="21.59" y1="27.94" x2="21.59" y2="28.2575" width="0.254" layer="94"/>
<wire x1="21.59" y1="28.2575" x2="21.59" y2="28.575" width="0.254" layer="94"/>
<wire x1="21.59" y1="28.575" x2="21.59" y2="30.48" width="0.254" layer="94"/>
<wire x1="19.05" y1="25.4" x2="19.05" y2="25.7175" width="0.254" layer="94"/>
<wire x1="19.05" y1="25.7175" x2="19.05" y2="26.035" width="0.254" layer="94"/>
<wire x1="19.05" y1="26.035" x2="19.05" y2="27.94" width="0.254" layer="94"/>
<wire x1="19.05" y1="27.94" x2="19.05" y2="28.2575" width="0.254" layer="94"/>
<wire x1="19.05" y1="28.2575" x2="19.05" y2="28.575" width="0.254" layer="94"/>
<wire x1="19.05" y1="28.575" x2="19.05" y2="30.48" width="0.254" layer="94"/>
<wire x1="31.75" y1="28.2575" x2="31.4325" y2="27.94" width="0.254" layer="94"/>
<wire x1="31.115" y1="27.94" x2="31.75" y2="28.575" width="0.254" layer="94"/>
<wire x1="29.5275" y1="27.94" x2="29.21" y2="28.2575" width="0.254" layer="94"/>
<wire x1="29.845" y1="27.94" x2="29.21" y2="28.575" width="0.254" layer="94"/>
<wire x1="29.21" y1="25.7175" x2="28.8925" y2="25.4" width="0.254" layer="94"/>
<wire x1="29.21" y1="26.035" x2="28.575" y2="25.4" width="0.254" layer="94"/>
<wire x1="26.9875" y1="25.4" x2="26.67" y2="25.7175" width="0.254" layer="94"/>
<wire x1="27.305" y1="25.4" x2="26.67" y2="26.035" width="0.254" layer="94"/>
<wire x1="24.4475" y1="25.4" x2="24.13" y2="25.7175" width="0.254" layer="94"/>
<wire x1="24.765" y1="25.4" x2="24.13" y2="26.035" width="0.254" layer="94"/>
<wire x1="26.67" y1="25.7175" x2="26.3525" y2="25.4" width="0.254" layer="94"/>
<wire x1="26.67" y1="26.035" x2="26.035" y2="25.4" width="0.254" layer="94"/>
<wire x1="24.13" y1="28.2575" x2="23.8125" y2="27.94" width="0.254" layer="94"/>
<wire x1="24.13" y1="28.575" x2="23.495" y2="27.94" width="0.254" layer="94"/>
<wire x1="21.9075" y1="27.94" x2="21.59" y2="28.2575" width="0.254" layer="94"/>
<wire x1="22.225" y1="27.94" x2="21.59" y2="28.575" width="0.254" layer="94"/>
<wire x1="21.59" y1="28.2575" x2="21.2725" y2="27.94" width="0.254" layer="94"/>
<wire x1="21.59" y1="28.575" x2="20.955" y2="27.94" width="0.254" layer="94"/>
<wire x1="19.3675" y1="27.94" x2="19.05" y2="28.2575" width="0.254" layer="94"/>
<wire x1="19.685" y1="27.94" x2="19.05" y2="28.575" width="0.254" layer="94"/>
<wire x1="19.05" y1="25.7175" x2="18.7325" y2="25.4" width="0.254" layer="94"/>
<wire x1="19.05" y1="26.035" x2="18.415" y2="25.4" width="0.254" layer="94"/>
<wire x1="16.8275" y1="25.4" x2="16.51" y2="25.7175" width="0.254" layer="94"/>
<wire x1="17.145" y1="25.4" x2="16.51" y2="26.035" width="0.254" layer="94"/>
<wire x1="-19.05" y1="-25.4" x2="-21.59" y2="-25.4" width="0.254" layer="94"/>
<wire x1="-31.75" y1="-25.4" x2="-31.75" y2="-27.94" width="0.254" layer="94"/>
<wire x1="-31.75" y1="-27.94" x2="-31.75" y2="-28.2575" width="0.254" layer="94"/>
<wire x1="-31.75" y1="-28.2575" x2="-31.75" y2="-28.575" width="0.254" layer="94"/>
<wire x1="-31.75" y1="-28.575" x2="-31.75" y2="-30.48" width="0.254" layer="94"/>
<wire x1="-31.75" y1="-30.48" x2="-29.21" y2="-30.48" width="0.254" layer="94"/>
<wire x1="-29.21" y1="-30.48" x2="-26.67" y2="-30.48" width="0.254" layer="94"/>
<wire x1="-26.67" y1="-30.48" x2="-24.13" y2="-30.48" width="0.254" layer="94"/>
<wire x1="-24.13" y1="-30.48" x2="-21.59" y2="-30.48" width="0.254" layer="94"/>
<wire x1="-21.59" y1="-30.48" x2="-19.05" y2="-30.48" width="0.254" layer="94"/>
<wire x1="-19.05" y1="-30.48" x2="-19.05" y2="-28.575" width="0.254" layer="94"/>
<wire x1="-19.05" y1="-28.575" x2="-19.05" y2="-28.2575" width="0.254" layer="94"/>
<wire x1="-19.05" y1="-27.94" x2="-19.05" y2="-26.035" width="0.254" layer="94"/>
<wire x1="-19.05" y1="-26.035" x2="-19.05" y2="-25.7175" width="0.254" layer="94"/>
<wire x1="-19.05" y1="-25.7175" x2="-19.05" y2="-25.4" width="0.254" layer="94"/>
<wire x1="-19.05" y1="-27.94" x2="-19.304" y2="-27.94" width="0.254" layer="94"/>
<wire x1="-19.3675" y1="-27.94" x2="-19.685" y2="-27.94" width="0.254" layer="94"/>
<wire x1="-19.685" y1="-27.94" x2="-20.955" y2="-27.94" width="0.254" layer="94"/>
<wire x1="-20.955" y1="-27.94" x2="-21.2725" y2="-27.94" width="0.254" layer="94"/>
<wire x1="-21.2725" y1="-27.94" x2="-21.9075" y2="-27.94" width="0.254" layer="94"/>
<wire x1="-21.9075" y1="-27.94" x2="-22.225" y2="-27.94" width="0.254" layer="94"/>
<wire x1="-22.225" y1="-27.94" x2="-23.495" y2="-27.94" width="0.254" layer="94"/>
<wire x1="-23.495" y1="-27.94" x2="-23.8125" y2="-27.94" width="0.254" layer="94"/>
<wire x1="-21.59" y1="-25.4" x2="-21.59" y2="-28.2575" width="0.254" layer="94"/>
<wire x1="-21.59" y1="-28.2575" x2="-21.59" y2="-28.575" width="0.254" layer="94"/>
<wire x1="-21.59" y1="-28.575" x2="-21.59" y2="-30.48" width="0.254" layer="94"/>
<wire x1="-24.13" y1="-30.48" x2="-24.13" y2="-28.575" width="0.254" layer="94"/>
<wire x1="-24.13" y1="-28.575" x2="-24.13" y2="-28.2575" width="0.254" layer="94"/>
<wire x1="-19.05" y1="-28.2575" x2="-19.05" y2="-27.94" width="0.254" layer="94"/>
<wire x1="-16.51" y1="-27.94" x2="-16.51" y2="-26.035" width="0.254" layer="94"/>
<wire x1="-16.51" y1="-26.035" x2="-16.51" y2="-25.7175" width="0.254" layer="94"/>
<wire x1="-16.51" y1="-25.7175" x2="-16.51" y2="-25.4" width="0.254" layer="94"/>
<wire x1="-16.51" y1="-30.48" x2="-16.51" y2="-27.94" width="0.254" layer="94"/>
<wire x1="-16.51" y1="-25.4" x2="-16.8275" y2="-25.4" width="0.254" layer="94"/>
<wire x1="-16.8275" y1="-25.4" x2="-17.145" y2="-25.4" width="0.254" layer="94"/>
<wire x1="-17.145" y1="-25.4" x2="-18.415" y2="-25.4" width="0.254" layer="94"/>
<wire x1="-18.415" y1="-25.4" x2="-18.7325" y2="-25.4" width="0.254" layer="94"/>
<wire x1="-18.7325" y1="-25.4" x2="-19.05" y2="-25.4" width="0.254" layer="94"/>
<wire x1="-19.05" y1="-30.48" x2="-16.51" y2="-30.48" width="0.254" layer="94"/>
<wire x1="-16.51" y1="-27.94" x2="-19.3675" y2="-27.94" width="0.254" layer="94"/>
<text x="-30.48" y="-26.67" size="1.4224" layer="94" rot="R270" align="center">1</text>
<text x="-27.94" y="-26.67" size="1.4224" layer="94" rot="R270" align="center">2</text>
<text x="-25.4" y="-26.67" size="1.4224" layer="94" rot="R270" align="center">3</text>
<text x="-22.86" y="-26.67" size="1.4224" layer="94" rot="R270" align="center">4</text>
<text x="-20.32" y="-26.67" size="1.4224" layer="94" rot="R270" align="center">5</text>
<text x="-17.78" y="-26.67" size="1.4224" layer="94" rot="R270" align="center">6</text>
<text x="-15.24" y="-26.67" size="1.4224" layer="94" rot="R270" align="center">7</text>
<text x="-30.48" y="-29.21" size="1.4224" layer="94" rot="R270" align="center">8</text>
<text x="-27.94" y="-29.21" size="1.4224" layer="94" rot="R270" align="center">9</text>
<text x="-25.4" y="-29.21" size="1.4224" layer="94" rot="R270" align="center">10</text>
<text x="-12.7" y="-27.94" size="1.27" layer="96" rot="R90" align="top-center">MINIFIT14</text>
<text x="-33.02" y="-27.94" size="1.778" layer="95" align="center-right">K1</text>
<text x="-22.86" y="-29.21" size="1.4224" layer="94" rot="R270" align="center">11</text>
<text x="-20.32" y="-29.21" size="1.4224" layer="94" rot="R270" align="center">12</text>
<pin name="K1.1" x="-30.48" y="-22.86" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="K1.2" x="-27.94" y="-22.86" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="K1.3" x="-25.4" y="-22.86" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="K1.4" x="-22.86" y="-22.86" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="K1.5" x="-20.32" y="-22.86" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="K1.6" x="-17.78" y="-22.86" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="K1.7" x="-15.24" y="-22.86" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="K1.8" x="-30.48" y="-33.02" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="K1.9" x="-27.94" y="-33.02" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="K1.10" x="-25.4" y="-33.02" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="K1.11" x="-22.86" y="-33.02" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="K1.12" x="-20.32" y="-33.02" visible="off" length="short" direction="pas" rot="R90"/>
<rectangle x1="-23.241" y1="-31.877" x2="-22.479" y2="-29.845" layer="94" rot="R270"/>
<text x="-15.24" y="-29.21" size="1.4224" layer="94" rot="R270" align="center">14</text>
<text x="-17.78" y="-29.21" size="1.4224" layer="94" rot="R270" align="center">13</text>
<pin name="K1.13" x="-17.78" y="-33.02" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="K1.14" x="-15.24" y="-33.02" visible="off" length="short" direction="pas" rot="R90"/>
<wire x1="-13.97" y1="-30.48" x2="-13.97" y2="-27.94" width="0.254" layer="94"/>
<wire x1="-13.97" y1="-27.94" x2="-13.97" y2="-26.035" width="0.254" layer="94"/>
<wire x1="-13.97" y1="-26.035" x2="-13.97" y2="-25.7175" width="0.254" layer="94"/>
<wire x1="-13.97" y1="-25.7175" x2="-13.97" y2="-25.4" width="0.254" layer="94"/>
<wire x1="-16.51" y1="-30.48" x2="-13.97" y2="-30.48" width="0.254" layer="94"/>
<wire x1="-16.51" y1="-27.94" x2="-13.97" y2="-27.94" width="0.254" layer="94"/>
<wire x1="-16.51" y1="-25.4" x2="-16.1925" y2="-25.4" width="0.254" layer="94"/>
<wire x1="-16.1925" y1="-25.4" x2="-15.875" y2="-25.4" width="0.254" layer="94"/>
<wire x1="-15.875" y1="-25.4" x2="-14.605" y2="-25.4" width="0.254" layer="94"/>
<wire x1="-14.605" y1="-25.4" x2="-14.2875" y2="-25.4" width="0.254" layer="94"/>
<wire x1="-14.2875" y1="-25.4" x2="-13.97" y2="-25.4" width="0.254" layer="94"/>
<wire x1="-14.605" y1="-25.4" x2="-13.97" y2="-26.035" width="0.254" layer="94"/>
<wire x1="-15.875" y1="-25.4" x2="-16.51" y2="-26.035" width="0.254" layer="94"/>
<wire x1="-17.145" y1="-25.4" x2="-16.51" y2="-26.035" width="0.254" layer="94"/>
<wire x1="-19.05" y1="-26.035" x2="-18.415" y2="-25.4" width="0.254" layer="94"/>
<wire x1="-16.51" y1="-25.7175" x2="-16.8275" y2="-25.4" width="0.254" layer="94"/>
<wire x1="-16.51" y1="-25.7175" x2="-16.1925" y2="-25.4" width="0.254" layer="94"/>
<wire x1="-14.2875" y1="-25.4" x2="-13.97" y2="-25.7175" width="0.254" layer="94"/>
<wire x1="-18.7325" y1="-25.4" x2="-19.05" y2="-25.7175" width="0.254" layer="94"/>
<wire x1="-19.3675" y1="-27.94" x2="-19.05" y2="-28.2575" width="0.254" layer="94"/>
<wire x1="-19.685" y1="-27.94" x2="-19.05" y2="-28.575" width="0.254" layer="94"/>
<wire x1="-21.59" y1="-28.2575" x2="-21.2725" y2="-27.94" width="0.254" layer="94"/>
<wire x1="-21.59" y1="-28.575" x2="-20.955" y2="-27.94" width="0.254" layer="94"/>
<wire x1="-22.225" y1="-27.94" x2="-21.59" y2="-28.575" width="0.254" layer="94"/>
<wire x1="-21.9075" y1="-27.94" x2="-21.59" y2="-28.2575" width="0.254" layer="94"/>
<wire x1="-24.13" y1="-28.2575" x2="-23.8125" y2="-27.94" width="0.254" layer="94"/>
<wire x1="-24.13" y1="-28.575" x2="-23.495" y2="-27.94" width="0.254" layer="94"/>
<wire x1="-31.75" y1="-25.4" x2="-29.21" y2="-25.4" width="0.254" layer="94"/>
<wire x1="-29.21" y1="-25.4" x2="-28.8925" y2="-25.4" width="0.254" layer="94"/>
<wire x1="-28.8925" y1="-25.4" x2="-28.575" y2="-25.4" width="0.254" layer="94"/>
<wire x1="-28.575" y1="-25.4" x2="-27.305" y2="-25.4" width="0.254" layer="94"/>
<wire x1="-27.305" y1="-25.4" x2="-26.9875" y2="-25.4" width="0.254" layer="94"/>
<wire x1="-26.9875" y1="-25.4" x2="-26.67" y2="-25.4" width="0.254" layer="94"/>
<wire x1="-26.67" y1="-25.4" x2="-26.3525" y2="-25.4" width="0.254" layer="94"/>
<wire x1="-26.3525" y1="-25.4" x2="-26.035" y2="-25.4" width="0.254" layer="94"/>
<wire x1="-26.035" y1="-25.4" x2="-24.765" y2="-25.4" width="0.254" layer="94"/>
<wire x1="-24.765" y1="-25.4" x2="-24.4475" y2="-25.4" width="0.254" layer="94"/>
<wire x1="-24.4475" y1="-25.4" x2="-24.13" y2="-25.4" width="0.254" layer="94"/>
<wire x1="-24.13" y1="-25.4" x2="-21.59" y2="-25.4" width="0.254" layer="94"/>
<wire x1="-24.13" y1="-28.2575" x2="-24.13" y2="-27.94" width="0.254" layer="94"/>
<wire x1="-24.13" y1="-27.94" x2="-23.8125" y2="-27.94" width="0.254" layer="94"/>
<wire x1="-24.13" y1="-27.94" x2="-24.13" y2="-26.035" width="0.254" layer="94"/>
<wire x1="-24.13" y1="-26.035" x2="-24.13" y2="-25.7175" width="0.254" layer="94"/>
<wire x1="-24.13" y1="-25.7175" x2="-24.13" y2="-25.4" width="0.254" layer="94"/>
<wire x1="-31.75" y1="-27.94" x2="-31.4325" y2="-27.94" width="0.254" layer="94"/>
<wire x1="-31.4325" y1="-27.94" x2="-31.115" y2="-27.94" width="0.254" layer="94"/>
<wire x1="-31.115" y1="-27.94" x2="-29.845" y2="-27.94" width="0.254" layer="94"/>
<wire x1="-29.845" y1="-27.94" x2="-29.5275" y2="-27.94" width="0.254" layer="94"/>
<wire x1="-29.5275" y1="-27.94" x2="-29.21" y2="-27.94" width="0.254" layer="94"/>
<wire x1="-29.21" y1="-27.94" x2="-26.67" y2="-27.94" width="0.254" layer="94"/>
<wire x1="-26.67" y1="-27.94" x2="-24.13" y2="-27.94" width="0.254" layer="94"/>
<wire x1="-26.67" y1="-25.4" x2="-26.67" y2="-25.7175" width="0.254" layer="94"/>
<wire x1="-26.67" y1="-25.7175" x2="-26.67" y2="-26.035" width="0.254" layer="94"/>
<wire x1="-26.67" y1="-26.035" x2="-26.67" y2="-27.94" width="0.254" layer="94"/>
<wire x1="-26.67" y1="-27.94" x2="-26.67" y2="-30.48" width="0.254" layer="94"/>
<wire x1="-29.21" y1="-25.4" x2="-29.21" y2="-25.7175" width="0.254" layer="94"/>
<wire x1="-29.21" y1="-25.7175" x2="-29.21" y2="-26.035" width="0.254" layer="94"/>
<wire x1="-29.21" y1="-26.035" x2="-29.21" y2="-27.94" width="0.254" layer="94"/>
<wire x1="-29.21" y1="-27.94" x2="-29.21" y2="-28.2575" width="0.254" layer="94"/>
<wire x1="-29.21" y1="-28.2575" x2="-29.21" y2="-28.575" width="0.254" layer="94"/>
<wire x1="-29.21" y1="-28.575" x2="-29.21" y2="-30.48" width="0.254" layer="94"/>
<wire x1="-31.75" y1="-28.2575" x2="-31.4325" y2="-27.94" width="0.254" layer="94"/>
<wire x1="-31.115" y1="-27.94" x2="-31.75" y2="-28.575" width="0.254" layer="94"/>
<wire x1="-29.5275" y1="-27.94" x2="-29.21" y2="-28.2575" width="0.254" layer="94"/>
<wire x1="-29.845" y1="-27.94" x2="-29.21" y2="-28.575" width="0.254" layer="94"/>
<wire x1="-29.21" y1="-25.7175" x2="-28.8925" y2="-25.4" width="0.254" layer="94"/>
<wire x1="-28.575" y1="-25.4" x2="-29.21" y2="-26.035" width="0.254" layer="94"/>
<wire x1="-26.9875" y1="-25.4" x2="-26.67" y2="-25.7175" width="0.254" layer="94"/>
<wire x1="-27.305" y1="-25.4" x2="-26.67" y2="-26.035" width="0.254" layer="94"/>
<wire x1="-26.67" y1="-25.7175" x2="-26.3525" y2="-25.4" width="0.254" layer="94"/>
<wire x1="-26.67" y1="-26.035" x2="-26.035" y2="-25.4" width="0.254" layer="94"/>
<wire x1="-24.4475" y1="-25.4" x2="-24.13" y2="-25.7175" width="0.254" layer="94"/>
<wire x1="-24.765" y1="-25.4" x2="-24.13" y2="-26.035" width="0.254" layer="94"/>
<wire x1="-26.67" y1="25.4" x2="-24.13" y2="25.4" width="0.254" layer="94"/>
<wire x1="-13.97" y1="25.4" x2="-13.97" y2="27.94" width="0.254" layer="94"/>
<wire x1="-13.97" y1="27.94" x2="-13.97" y2="28.2575" width="0.254" layer="94"/>
<wire x1="-13.97" y1="28.2575" x2="-13.97" y2="28.575" width="0.254" layer="94"/>
<wire x1="-13.97" y1="28.575" x2="-13.97" y2="30.48" width="0.254" layer="94"/>
<wire x1="-13.97" y1="30.48" x2="-16.51" y2="30.48" width="0.254" layer="94"/>
<wire x1="-16.51" y1="30.48" x2="-19.05" y2="30.48" width="0.254" layer="94"/>
<wire x1="-19.05" y1="30.48" x2="-21.59" y2="30.48" width="0.254" layer="94"/>
<wire x1="-21.59" y1="30.48" x2="-24.13" y2="30.48" width="0.254" layer="94"/>
<wire x1="-24.13" y1="30.48" x2="-26.67" y2="30.48" width="0.254" layer="94"/>
<wire x1="-26.67" y1="30.48" x2="-26.67" y2="28.575" width="0.254" layer="94"/>
<wire x1="-26.67" y1="28.575" x2="-26.67" y2="28.2575" width="0.254" layer="94"/>
<wire x1="-26.67" y1="27.94" x2="-26.67" y2="26.035" width="0.254" layer="94"/>
<wire x1="-26.67" y1="26.035" x2="-26.67" y2="25.7175" width="0.254" layer="94"/>
<wire x1="-26.67" y1="25.7175" x2="-26.67" y2="25.4" width="0.254" layer="94"/>
<wire x1="-26.67" y1="27.94" x2="-26.416" y2="27.94" width="0.254" layer="94"/>
<wire x1="-26.3525" y1="27.94" x2="-26.035" y2="27.94" width="0.254" layer="94"/>
<wire x1="-26.035" y1="27.94" x2="-24.765" y2="27.94" width="0.254" layer="94"/>
<wire x1="-24.765" y1="27.94" x2="-24.4475" y2="27.94" width="0.254" layer="94"/>
<wire x1="-24.4475" y1="27.94" x2="-23.8125" y2="27.94" width="0.254" layer="94"/>
<wire x1="-23.8125" y1="27.94" x2="-23.495" y2="27.94" width="0.254" layer="94"/>
<wire x1="-23.495" y1="27.94" x2="-22.225" y2="27.94" width="0.254" layer="94"/>
<wire x1="-22.225" y1="27.94" x2="-21.9075" y2="27.94" width="0.254" layer="94"/>
<wire x1="-24.13" y1="25.4" x2="-24.13" y2="28.2575" width="0.254" layer="94"/>
<wire x1="-24.13" y1="28.2575" x2="-24.13" y2="28.575" width="0.254" layer="94"/>
<wire x1="-24.13" y1="28.575" x2="-24.13" y2="30.48" width="0.254" layer="94"/>
<wire x1="-21.59" y1="30.48" x2="-21.59" y2="28.575" width="0.254" layer="94"/>
<wire x1="-21.59" y1="28.575" x2="-21.59" y2="28.2575" width="0.254" layer="94"/>
<wire x1="-26.67" y1="28.2575" x2="-26.67" y2="27.94" width="0.254" layer="94"/>
<wire x1="-29.21" y1="27.94" x2="-29.21" y2="26.035" width="0.254" layer="94"/>
<wire x1="-29.21" y1="26.035" x2="-29.21" y2="25.7175" width="0.254" layer="94"/>
<wire x1="-29.21" y1="25.7175" x2="-29.21" y2="25.4" width="0.254" layer="94"/>
<wire x1="-29.21" y1="30.48" x2="-29.21" y2="27.94" width="0.254" layer="94"/>
<wire x1="-29.21" y1="25.4" x2="-28.8925" y2="25.4" width="0.254" layer="94"/>
<wire x1="-28.8925" y1="25.4" x2="-28.575" y2="25.4" width="0.254" layer="94"/>
<wire x1="-28.575" y1="25.4" x2="-27.305" y2="25.4" width="0.254" layer="94"/>
<wire x1="-27.305" y1="25.4" x2="-26.9875" y2="25.4" width="0.254" layer="94"/>
<wire x1="-26.9875" y1="25.4" x2="-26.67" y2="25.4" width="0.254" layer="94"/>
<wire x1="-26.67" y1="30.48" x2="-29.21" y2="30.48" width="0.254" layer="94"/>
<wire x1="-29.21" y1="27.94" x2="-26.3525" y2="27.94" width="0.254" layer="94"/>
<text x="-15.24" y="26.67" size="1.4224" layer="94" rot="R90" align="center">1</text>
<text x="-17.78" y="26.67" size="1.4224" layer="94" rot="R90" align="center">2</text>
<text x="-20.32" y="26.67" size="1.4224" layer="94" rot="R90" align="center">3</text>
<text x="-22.86" y="26.67" size="1.4224" layer="94" rot="R90" align="center">4</text>
<text x="-25.4" y="26.67" size="1.4224" layer="94" rot="R90" align="center">5</text>
<text x="-27.94" y="26.67" size="1.4224" layer="94" rot="R90" align="center">6</text>
<text x="-30.48" y="26.67" size="1.4224" layer="94" rot="R90" align="center">7</text>
<text x="-15.24" y="29.21" size="1.4224" layer="94" rot="R90" align="center">8</text>
<text x="-17.78" y="29.21" size="1.4224" layer="94" rot="R90" align="center">9</text>
<text x="-20.32" y="29.21" size="1.4224" layer="94" rot="R90" align="center">10</text>
<text x="-12.7" y="27.94" size="1.27" layer="96" rot="R90" align="top-center">MINIFIT14</text>
<text x="-33.02" y="27.94" size="1.778" layer="95" align="center-right">K2</text>
<text x="-22.86" y="29.21" size="1.4224" layer="94" rot="R90" align="center">11</text>
<text x="-25.4" y="29.21" size="1.4224" layer="94" rot="R90" align="center">12</text>
<pin name="K2.1" x="-15.24" y="22.86" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="K2.2" x="-17.78" y="22.86" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="K2.3" x="-20.32" y="22.86" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="K2.4" x="-22.86" y="22.86" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="K2.5" x="-25.4" y="22.86" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="K2.6" x="-27.94" y="22.86" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="K2.7" x="-30.48" y="22.86" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="K2.8" x="-15.24" y="33.02" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="K2.9" x="-17.78" y="33.02" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="K2.10" x="-20.32" y="33.02" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="K2.11" x="-22.86" y="33.02" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="K2.12" x="-25.4" y="33.02" visible="off" length="short" direction="pas" rot="R270"/>
<rectangle x1="-23.241" y1="29.845" x2="-22.479" y2="31.877" layer="94" rot="R90"/>
<text x="-30.48" y="29.21" size="1.4224" layer="94" rot="R90" align="center">14</text>
<text x="-27.94" y="29.21" size="1.4224" layer="94" rot="R90" align="center">13</text>
<pin name="K2.13" x="-27.94" y="33.02" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="K2.14" x="-30.48" y="33.02" visible="off" length="short" direction="pas" rot="R270"/>
<wire x1="-31.75" y1="30.48" x2="-31.75" y2="27.94" width="0.254" layer="94"/>
<wire x1="-31.75" y1="27.94" x2="-31.75" y2="26.035" width="0.254" layer="94"/>
<wire x1="-31.75" y1="26.035" x2="-31.75" y2="25.7175" width="0.254" layer="94"/>
<wire x1="-31.75" y1="25.7175" x2="-31.75" y2="25.4" width="0.254" layer="94"/>
<wire x1="-29.21" y1="30.48" x2="-31.75" y2="30.48" width="0.254" layer="94"/>
<wire x1="-29.21" y1="27.94" x2="-31.75" y2="27.94" width="0.254" layer="94"/>
<wire x1="-29.21" y1="25.4" x2="-29.5275" y2="25.4" width="0.254" layer="94"/>
<wire x1="-29.5275" y1="25.4" x2="-29.845" y2="25.4" width="0.254" layer="94"/>
<wire x1="-29.845" y1="25.4" x2="-31.115" y2="25.4" width="0.254" layer="94"/>
<wire x1="-31.115" y1="25.4" x2="-31.4325" y2="25.4" width="0.254" layer="94"/>
<wire x1="-31.4325" y1="25.4" x2="-31.75" y2="25.4" width="0.254" layer="94"/>
<wire x1="-31.115" y1="25.4" x2="-31.75" y2="26.035" width="0.254" layer="94"/>
<wire x1="-29.845" y1="25.4" x2="-29.21" y2="26.035" width="0.254" layer="94"/>
<wire x1="-28.575" y1="25.4" x2="-29.21" y2="26.035" width="0.254" layer="94"/>
<wire x1="-26.67" y1="26.035" x2="-27.305" y2="25.4" width="0.254" layer="94"/>
<wire x1="-29.21" y1="25.7175" x2="-28.8925" y2="25.4" width="0.254" layer="94"/>
<wire x1="-29.21" y1="25.7175" x2="-29.5275" y2="25.4" width="0.254" layer="94"/>
<wire x1="-31.4325" y1="25.4" x2="-31.75" y2="25.7175" width="0.254" layer="94"/>
<wire x1="-26.9875" y1="25.4" x2="-26.67" y2="25.7175" width="0.254" layer="94"/>
<wire x1="-26.3525" y1="27.94" x2="-26.67" y2="28.2575" width="0.254" layer="94"/>
<wire x1="-26.035" y1="27.94" x2="-26.67" y2="28.575" width="0.254" layer="94"/>
<wire x1="-24.13" y1="28.2575" x2="-24.4475" y2="27.94" width="0.254" layer="94"/>
<wire x1="-24.13" y1="28.575" x2="-24.765" y2="27.94" width="0.254" layer="94"/>
<wire x1="-23.495" y1="27.94" x2="-24.13" y2="28.575" width="0.254" layer="94"/>
<wire x1="-23.8125" y1="27.94" x2="-24.13" y2="28.2575" width="0.254" layer="94"/>
<wire x1="-21.59" y1="28.2575" x2="-21.9075" y2="27.94" width="0.254" layer="94"/>
<wire x1="-21.59" y1="28.575" x2="-22.225" y2="27.94" width="0.254" layer="94"/>
<wire x1="-13.97" y1="25.4" x2="-16.51" y2="25.4" width="0.254" layer="94"/>
<wire x1="-16.51" y1="25.4" x2="-16.8275" y2="25.4" width="0.254" layer="94"/>
<wire x1="-16.8275" y1="25.4" x2="-17.145" y2="25.4" width="0.254" layer="94"/>
<wire x1="-17.145" y1="25.4" x2="-18.415" y2="25.4" width="0.254" layer="94"/>
<wire x1="-18.415" y1="25.4" x2="-18.7325" y2="25.4" width="0.254" layer="94"/>
<wire x1="-18.7325" y1="25.4" x2="-19.05" y2="25.4" width="0.254" layer="94"/>
<wire x1="-19.05" y1="25.4" x2="-19.3675" y2="25.4" width="0.254" layer="94"/>
<wire x1="-19.3675" y1="25.4" x2="-19.685" y2="25.4" width="0.254" layer="94"/>
<wire x1="-19.685" y1="25.4" x2="-20.955" y2="25.4" width="0.254" layer="94"/>
<wire x1="-20.955" y1="25.4" x2="-21.2725" y2="25.4" width="0.254" layer="94"/>
<wire x1="-21.2725" y1="25.4" x2="-21.59" y2="25.4" width="0.254" layer="94"/>
<wire x1="-21.59" y1="25.4" x2="-24.13" y2="25.4" width="0.254" layer="94"/>
<wire x1="-21.59" y1="28.2575" x2="-21.59" y2="27.94" width="0.254" layer="94"/>
<wire x1="-21.59" y1="27.94" x2="-21.9075" y2="27.94" width="0.254" layer="94"/>
<wire x1="-21.59" y1="27.94" x2="-21.59" y2="26.035" width="0.254" layer="94"/>
<wire x1="-21.59" y1="26.035" x2="-21.59" y2="25.7175" width="0.254" layer="94"/>
<wire x1="-21.59" y1="25.7175" x2="-21.59" y2="25.4" width="0.254" layer="94"/>
<wire x1="-13.97" y1="27.94" x2="-14.2875" y2="27.94" width="0.254" layer="94"/>
<wire x1="-14.2875" y1="27.94" x2="-14.605" y2="27.94" width="0.254" layer="94"/>
<wire x1="-14.605" y1="27.94" x2="-15.875" y2="27.94" width="0.254" layer="94"/>
<wire x1="-15.875" y1="27.94" x2="-16.1925" y2="27.94" width="0.254" layer="94"/>
<wire x1="-16.1925" y1="27.94" x2="-16.51" y2="27.94" width="0.254" layer="94"/>
<wire x1="-16.51" y1="27.94" x2="-19.05" y2="27.94" width="0.254" layer="94"/>
<wire x1="-19.05" y1="27.94" x2="-21.59" y2="27.94" width="0.254" layer="94"/>
<wire x1="-19.05" y1="25.4" x2="-19.05" y2="25.7175" width="0.254" layer="94"/>
<wire x1="-19.05" y1="25.7175" x2="-19.05" y2="26.035" width="0.254" layer="94"/>
<wire x1="-19.05" y1="26.035" x2="-19.05" y2="27.94" width="0.254" layer="94"/>
<wire x1="-19.05" y1="27.94" x2="-19.05" y2="30.48" width="0.254" layer="94"/>
<wire x1="-16.51" y1="25.4" x2="-16.51" y2="25.7175" width="0.254" layer="94"/>
<wire x1="-16.51" y1="25.7175" x2="-16.51" y2="26.035" width="0.254" layer="94"/>
<wire x1="-16.51" y1="26.035" x2="-16.51" y2="27.94" width="0.254" layer="94"/>
<wire x1="-16.51" y1="27.94" x2="-16.51" y2="28.2575" width="0.254" layer="94"/>
<wire x1="-16.51" y1="28.2575" x2="-16.51" y2="28.575" width="0.254" layer="94"/>
<wire x1="-16.51" y1="28.575" x2="-16.51" y2="30.48" width="0.254" layer="94"/>
<wire x1="-13.97" y1="28.2575" x2="-14.2875" y2="27.94" width="0.254" layer="94"/>
<wire x1="-14.605" y1="27.94" x2="-13.97" y2="28.575" width="0.254" layer="94"/>
<wire x1="-16.1925" y1="27.94" x2="-16.51" y2="28.2575" width="0.254" layer="94"/>
<wire x1="-15.875" y1="27.94" x2="-16.51" y2="28.575" width="0.254" layer="94"/>
<wire x1="-16.51" y1="25.7175" x2="-16.8275" y2="25.4" width="0.254" layer="94"/>
<wire x1="-17.145" y1="25.4" x2="-16.51" y2="26.035" width="0.254" layer="94"/>
<wire x1="-18.7325" y1="25.4" x2="-19.05" y2="25.7175" width="0.254" layer="94"/>
<wire x1="-18.415" y1="25.4" x2="-19.05" y2="26.035" width="0.254" layer="94"/>
<wire x1="-19.05" y1="25.7175" x2="-19.3675" y2="25.4" width="0.254" layer="94"/>
<wire x1="-19.05" y1="26.035" x2="-19.685" y2="25.4" width="0.254" layer="94"/>
<wire x1="-21.2725" y1="25.4" x2="-21.59" y2="25.7175" width="0.254" layer="94"/>
<wire x1="-20.955" y1="25.4" x2="-21.59" y2="26.035" width="0.254" layer="94"/>
<wire x1="-38.1" y1="-34.925" x2="-37.465" y2="-35.56" width="0.3048" layer="94"/>
<wire x1="-37.465" y1="-35.56" x2="38.1" y2="-35.56" width="0.3048" layer="94"/>
<wire x1="38.1" y1="-35.56" x2="38.1" y2="34.925" width="0.3048" layer="94"/>
<wire x1="38.1" y1="34.925" x2="37.465" y2="35.56" width="0.3048" layer="94"/>
<wire x1="-38.1" y1="-34.925" x2="37.465" y2="-34.925" width="0.3048" layer="94"/>
<wire x1="37.465" y1="-34.925" x2="37.465" y2="35.56" width="0.3048" layer="94"/>
<wire x1="37.465" y1="-34.925" x2="38.1" y2="-35.56" width="0.3048" layer="94"/>
<text x="0" y="17.78" size="1.778" layer="94" rot="MR0" align="top-center">Deska
Autostart KUBOTA
- konektory</text>
<text x="-12.7" y="-5.08" size="1.27" layer="94">Část desky:</text>
<text x="2.54" y="-5.08" size="1.27" layer="94">&gt;GATE</text>
<text x="-12.7" y="-2.54" size="1.27" layer="94">Karta ABRA:</text>
<text x="2.54" y="-2.54" size="1.27" layer="94">&gt;ABRA</text>
<text x="-12.7" y="0" size="1.27" layer="94">Označení desky:</text>
<text x="2.54" y="0" size="1.27" layer="94">&gt;VALUE</text>
<text x="36.195" y="34.29" size="1.27" layer="95" align="top-right">&gt;PART</text>
<text x="0" y="5.08" size="1.27" layer="94" align="center">Čislování konektorů na desce
při horním pohledu</text>
<wire x1="24.765" y1="-32.385" x2="20.955" y2="-32.385" width="0.254" layer="94"/>
<wire x1="20.955" y1="-32.385" x2="20.955" y2="-3.175" width="0.254" layer="94"/>
<wire x1="20.955" y1="-3.175" x2="24.765" y2="-3.175" width="0.254" layer="94"/>
<wire x1="24.765" y1="-3.175" x2="24.765" y2="-5.08" width="0.254" layer="94"/>
<wire x1="24.765" y1="-5.08" x2="24.765" y2="-10.16" width="0.254" layer="94"/>
<wire x1="24.765" y1="-10.16" x2="24.765" y2="-15.24" width="0.254" layer="94"/>
<wire x1="24.765" y1="-15.24" x2="24.765" y2="-20.32" width="0.254" layer="94"/>
<wire x1="24.765" y1="-20.32" x2="24.765" y2="-25.4" width="0.254" layer="94"/>
<wire x1="24.765" y1="-25.4" x2="24.765" y2="-30.48" width="0.254" layer="94"/>
<wire x1="24.765" y1="-30.48" x2="24.765" y2="-32.385" width="0.254" layer="94"/>
<wire x1="22.86" y1="-29.21" x2="22.86" y2="-31.75" width="0.254" layer="94" curve="180"/>
<wire x1="22.86" y1="-3.81" x2="22.86" y2="-6.35" width="0.254" layer="94" curve="180"/>
<wire x1="22.86" y1="-29.21" x2="24.13" y2="-29.21" width="0.254" layer="94"/>
<wire x1="24.13" y1="-29.21" x2="24.13" y2="-31.75" width="0.254" layer="94"/>
<wire x1="24.13" y1="-31.75" x2="22.86" y2="-31.75" width="0.254" layer="94"/>
<wire x1="22.86" y1="-3.81" x2="24.13" y2="-3.81" width="0.254" layer="94"/>
<wire x1="24.13" y1="-3.81" x2="24.13" y2="-6.35" width="0.254" layer="94"/>
<wire x1="24.13" y1="-6.35" x2="22.86" y2="-6.35" width="0.254" layer="94"/>
<circle x="22.86" y="-25.4" radius="1.4199" width="0.254" layer="94"/>
<circle x="22.86" y="-20.32" radius="1.4199" width="0.254" layer="94"/>
<circle x="22.86" y="-10.16" radius="1.4199" width="0.254" layer="94"/>
<circle x="22.86" y="-15.24" radius="1.4199" width="0.254" layer="94"/>
<text x="22.86" y="-33.02" size="1.27" layer="96" rot="MR0" align="top-center">&gt;VALUE</text>
<text x="22.86" y="-2.54" size="1.778" layer="95" rot="MR0" align="bottom-center">JP4</text>
<text x="22.86" y="-30.48" size="1.27" layer="94" rot="R270" align="center">1</text>
<text x="22.86" y="-25.4" size="1.27" layer="94" rot="R90" align="center">2</text>
<text x="22.86" y="-20.32" size="1.27" layer="94" rot="R90" align="center">3</text>
<text x="22.86" y="-15.24" size="1.27" layer="94" rot="R90" align="center">4</text>
<text x="22.86" y="-10.16" size="1.27" layer="94" rot="R90" align="center">5</text>
<text x="22.86" y="-5.08" size="1.27" layer="94" rot="R270" align="center">6</text>
<pin name="JP4.1" x="27.94" y="-30.48" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="JP4.2" x="27.94" y="-25.4" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="JP4.3" x="27.94" y="-20.32" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="JP4.4" x="27.94" y="-15.24" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="JP4.5" x="27.94" y="-10.16" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="JP4.6" x="27.94" y="-5.08" visible="off" length="short" direction="pas" rot="R180"/>
<wire x1="25.4" y1="-5.08" x2="24.765" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="25.4" y1="-10.16" x2="24.765" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="25.4" y1="-15.24" x2="24.765" y2="-15.24" width="0.1524" layer="94"/>
<wire x1="25.4" y1="-20.32" x2="24.765" y2="-20.32" width="0.1524" layer="94"/>
<wire x1="25.4" y1="-25.4" x2="24.765" y2="-25.4" width="0.1524" layer="94"/>
<wire x1="25.4" y1="-30.48" x2="24.765" y2="-30.48" width="0.1524" layer="94"/>
<wire x1="-38.1" y1="35.56" x2="-38.1" y2="35.2425" width="0.3048" layer="94"/>
<wire x1="-38.1" y1="35.2425" x2="-38.1" y2="34.925" width="0.3048" layer="94"/>
<wire x1="-38.1" y1="34.925" x2="-38.1" y2="34.6075" width="0.3048" layer="94"/>
<wire x1="-38.1" y1="34.6075" x2="-38.1" y2="34.29" width="0.3048" layer="94"/>
<wire x1="-38.1" y1="34.29" x2="-38.1" y2="-34.925" width="0.3048" layer="94"/>
<wire x1="37.465" y1="35.56" x2="-36.83" y2="35.56" width="0.3048" layer="94"/>
<wire x1="-36.83" y1="35.56" x2="-37.1475" y2="35.56" width="0.3048" layer="94"/>
<wire x1="-37.1475" y1="35.56" x2="-37.465" y2="35.56" width="0.3048" layer="94"/>
<wire x1="-37.465" y1="35.56" x2="-37.7825" y2="35.56" width="0.3048" layer="94"/>
<wire x1="-37.7825" y1="35.56" x2="-38.1" y2="35.56" width="0.3048" layer="94"/>
<wire x1="-38.1" y1="34.29" x2="-36.83" y2="35.56" width="0.3048" layer="94"/>
<wire x1="-38.1" y1="34.6075" x2="-37.1475" y2="35.56" width="0.3048" layer="94"/>
<wire x1="-38.1" y1="34.925" x2="-37.465" y2="35.56" width="0.3048" layer="94"/>
<wire x1="-38.1" y1="35.2425" x2="-37.7825" y2="35.56" width="0.3048" layer="94"/>
</symbol>
<symbol name="PRUCHODKA">
<wire x1="0.635" y1="0.635" x2="0.9525" y2="0.635" width="0.254" layer="94"/>
<wire x1="0.9525" y1="0.635" x2="1.27" y2="0.635" width="0.254" layer="94"/>
<wire x1="-0.635" y1="-0.635" x2="0.635" y2="-0.635" width="0.254" layer="94"/>
<wire x1="0.635" y1="-0.635" x2="0.9525" y2="-0.635" width="0.254" layer="94"/>
<wire x1="0.9525" y1="-0.635" x2="1.27" y2="-0.635" width="0.254" layer="94"/>
<wire x1="0.635" y1="-1.27" x2="1.27" y2="-0.635" width="0.254" layer="94" curve="90"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.254" layer="94" curve="-90"/>
<wire x1="0.635" y1="0.635" x2="-0.635" y2="0.635" width="0.254" layer="94"/>
<wire x1="-0.635" y1="0.635" x2="-0.635" y2="1.27" width="0.254" layer="94"/>
<wire x1="0.635" y1="0.635" x2="0.635" y2="0.9525" width="0.254" layer="94"/>
<wire x1="0.635" y1="0.9525" x2="0.635" y2="1.27" width="0.254" layer="94"/>
<wire x1="0.635" y1="-1.27" x2="0.635" y2="-0.9525" width="0.254" layer="94"/>
<wire x1="0.635" y1="-0.9525" x2="0.635" y2="-0.635" width="0.254" layer="94"/>
<wire x1="-0.635" y1="-1.27" x2="-0.635" y2="-0.635" width="0.254" layer="94"/>
<wire x1="0.635" y1="-0.9525" x2="0.9525" y2="-0.635" width="0.254" layer="94"/>
<wire x1="0.635" y1="-1.27" x2="1.27" y2="-0.635" width="0.254" layer="94"/>
<wire x1="1.27" y1="0.635" x2="0.635" y2="1.27" width="0.254" layer="94"/>
<wire x1="0.9525" y1="0.635" x2="0.635" y2="0.9525" width="0.254" layer="94"/>
</symbol>
<symbol name="SVORKA_PULENA_A">
<text x="0" y="0" size="1.778" layer="96" align="center">&gt;VALUE</text>
<wire x1="-5.08" y1="1.27" x2="0" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.1524" layer="94" style="shortdash"/>
<wire x1="5.08" y1="1.27" x2="5.08" y2="-1.27" width="0.1524" layer="94" style="shortdash"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.1524" layer="94" style="shortdash"/>
<wire x1="0" y1="-1.27" x2="-5.08" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-1.27" x2="-5.08" y2="1.27" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.635" x2="3.81" y2="0.635" width="0.1524" layer="94" style="shortdash"/>
<circle x="3.175" y="0" radius="0.898025" width="0.1524" layer="94"/>
<circle x="-3.175" y="0" radius="0.898025" width="0.254" layer="94"/>
<wire x1="-3.81" y1="-0.635" x2="-2.54" y2="0.635" width="0.254" layer="94"/>
<pin name="A" x="-7.62" y="0" visible="pad" length="short" direction="pas"/>
</symbol>
<symbol name="SVORKA_PULENA_B">
<text x="0" y="0" size="1.778" layer="96" align="center">&gt;VALUE</text>
<wire x1="-5.08" y1="1.27" x2="0" y2="1.27" width="0.1524" layer="94" style="shortdash"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.254" layer="94"/>
<wire x1="5.08" y1="1.27" x2="5.08" y2="-1.27" width="0.254" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.254" layer="94"/>
<wire x1="0" y1="-1.27" x2="-5.08" y2="-1.27" width="0.1524" layer="94" style="shortdash"/>
<wire x1="-5.08" y1="-1.27" x2="-5.08" y2="1.27" width="0.1524" layer="94" style="shortdash"/>
<wire x1="2.54" y1="-0.635" x2="3.81" y2="0.635" width="0.254" layer="94"/>
<circle x="3.175" y="0" radius="0.898025" width="0.254" layer="94"/>
<circle x="-3.175" y="0" radius="0.898025" width="0.1524" layer="94"/>
<wire x1="-3.81" y1="-0.635" x2="-2.54" y2="0.635" width="0.1524" layer="94" style="shortdash"/>
<pin name="B" x="7.62" y="0" visible="pad" length="short" direction="pas" rot="R180"/>
</symbol>
<symbol name="HORN">
<wire x1="-1.27" y1="2.54" x2="-1.27" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="-2.54" x2="1.27" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.27" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-1.27" x2="1.27" y2="1.27" width="0.1524" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="2.54" width="0.1524" layer="94"/>
<wire x1="1.27" y1="2.54" x2="-1.27" y2="2.54" width="0.1524" layer="94"/>
<wire x1="1.27" y1="1.27" x2="6.985" y2="1.905" width="0.1524" layer="94"/>
<wire x1="6.985" y1="1.905" x2="10.795" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="10.795" y1="-1.905" x2="1.27" y2="-1.27" width="0.1524" layer="94"/>
<pin name="P$1" x="0" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
<pin name="P$2" x="0" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
</symbol>
<symbol name="OTOCNY_PREPINAC">
<text x="2.54" y="7.62" size="1.778" layer="94" rot="R180" align="bottom-center">&gt;GATE</text>
<wire x1="-1.905" y1="8.89" x2="-1.905" y2="7.62" width="0.254" layer="94"/>
<wire x1="-1.905" y1="8.89" x2="0" y2="8.89" width="0.254" layer="94"/>
<wire x1="1.905" y1="8.89" x2="1.905" y2="10.16" width="0.254" layer="94"/>
<wire x1="0" y1="8.89" x2="0" y2="6.985" width="0.1524" layer="94"/>
<wire x1="0" y1="8.89" x2="1.905" y2="8.89" width="0.254" layer="94"/>
<wire x1="0" y1="6.35" x2="0" y2="5.842" width="0.1524" layer="94"/>
<wire x1="0" y1="4.826" x2="0" y2="4.445" width="0.1524" layer="94"/>
<wire x1="0" y1="3.81" x2="0" y2="2.667" width="0.1524" layer="94"/>
<wire x1="0" y1="5.842" x2="-0.762" y2="5.334" width="0.1524" layer="94"/>
<wire x1="-0.762" y1="5.334" x2="0" y2="4.826" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="-2.54" x2="-5.08" y2="0" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="-2.54" x2="-5.08" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="-3.048" y2="2.54" width="0.1524" layer="94"/>
<wire x1="3.048" y1="2.54" x2="5.08" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="0" x2="-5.08" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="2.54" x2="2.032" y2="2.54" width="0.3048" layer="94"/>
<circle x="-2.54" y="2.54" radius="0.508" width="0.3048" layer="94"/>
<circle x="2.54" y="2.54" radius="0.508" width="0.3048" layer="94"/>
<text x="1.778" y="3.048" size="1.27" layer="94" rot="MR0">I</text>
<text x="1.778" y="0.508" size="1.27" layer="94" rot="MR0">II</text>
<text x="1.778" y="-2.032" size="1.27" layer="94" rot="MR0">III</text>
<text x="1.778" y="-4.572" size="1.27" layer="94" rot="MR0">IV</text>
<circle x="-5.08" y="-2.54" radius="0.254" width="0.508" layer="94"/>
<circle x="-5.08" y="0" radius="0.254" width="0.508" layer="94"/>
<circle x="-5.08" y="2.54" radius="0.254" width="0.508" layer="94"/>
<wire x1="-5.08" y1="2.54" x2="-7.62" y2="2.54" width="0.1524" layer="94"/>
<wire x1="3.048" y1="0" x2="5.08" y2="0" width="0.1524" layer="94"/>
<wire x1="3.048" y1="-2.54" x2="5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="3.048" y1="-5.08" x2="5.08" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="0" x2="-3.048" y2="0" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="-2.54" x2="-3.048" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="-3.048" y2="-5.08" width="0.1524" layer="94"/>
<circle x="-2.54" y="0" radius="0.508" width="0.3048" layer="94"/>
<circle x="-2.54" y="-2.54" radius="0.508" width="0.3048" layer="94"/>
<circle x="-2.54" y="-5.08" radius="0.508" width="0.3048" layer="94"/>
<circle x="2.54" y="0" radius="0.508" width="0.3048" layer="94"/>
<circle x="2.54" y="-2.54" radius="0.508" width="0.3048" layer="94"/>
<circle x="2.54" y="-5.08" radius="0.508" width="0.3048" layer="94"/>
<pin name="0" x="-10.16" y="2.54" visible="pad" length="short" direction="pas"/>
<pin name="I" x="10.16" y="2.54" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="II" x="10.16" y="0" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="III" x="10.16" y="-2.54" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="IV" x="10.16" y="-5.08" visible="pad" length="middle" direction="pas" rot="R180"/>
<wire x1="-6.35" y1="11.43" x2="-6.35" y2="-7.62" width="0.1524" layer="94" style="shortdash"/>
<wire x1="-6.35" y1="-7.62" x2="6.35" y2="-7.62" width="0.1524" layer="94" style="shortdash"/>
<wire x1="6.35" y1="-7.62" x2="6.35" y2="11.43" width="0.1524" layer="94" style="shortdash"/>
<wire x1="-6.35" y1="11.43" x2="6.35" y2="11.43" width="0.1524" layer="94" style="shortdash"/>
</symbol>
<symbol name="OTOCNY_PREPINAC_STITEK">
<wire x1="-0.635" y1="3.81" x2="0" y2="4.445" width="0.1524" layer="97"/>
<wire x1="0" y1="4.445" x2="0.635" y2="3.81" width="0.1524" layer="97"/>
<wire x1="1.5875" y1="-4.445" x2="-1.5875" y2="-4.445" width="0.1524" layer="97"/>
<wire x1="0.635" y1="3.81" x2="1.5875" y2="-4.445" width="0.1524" layer="97" curve="18"/>
<wire x1="-0.635" y1="3.81" x2="-1.5875" y2="-4.445" width="0.1524" layer="97" curve="-18"/>
<wire x1="0.635" y1="3.81" x2="1.27" y2="-3.175" width="0.1524" layer="97" curve="-139.915781"/>
<wire x1="-0.635" y1="3.81" x2="-1.27" y2="-3.175" width="0.1524" layer="97" curve="139.915781"/>
<text x="0" y="5.08" size="0.8128" layer="97" align="center">I</text>
<text x="3.4925" y="3.4925" size="0.8128" layer="97" align="center">II</text>
<text x="5.08" y="0" size="0.8128" layer="97" align="center">III</text>
<text x="3.4925" y="-3.4925" size="0.8128" layer="97" align="center">IV</text>
<wire x1="-6.35" y1="6.35" x2="-6.35" y2="-6.35" width="0.1524" layer="94" style="shortdash"/>
<wire x1="-6.35" y1="-6.35" x2="6.35" y2="-6.35" width="0.1524" layer="94" style="shortdash"/>
<wire x1="6.35" y1="-6.35" x2="6.35" y2="6.35" width="0.1524" layer="94" style="shortdash"/>
<wire x1="-6.35" y1="6.35" x2="6.35" y2="6.35" width="0.1524" layer="94" style="shortdash"/>
<text x="-33.02" y="-5.08" size="1.27" layer="97">Karta ABRA:</text>
<text x="-20.32" y="-5.08" size="1.27" layer="97">&gt;ABRA</text>
<text x="-33.02" y="0" size="1.27" layer="95">Paketový přepínač TMC/manuál</text>
<text x="-33.02" y="-2.54" size="1.27" layer="97">TYP:</text>
<text x="-27.94" y="-2.54" size="1.27" layer="97">&gt;TYP</text>
</symbol>
<symbol name="OTOCNY_PREPINAC_SEKCE">
<text x="2.54" y="7.62" size="1.778" layer="94" rot="R180" align="bottom-center">&gt;GATE</text>
<wire x1="-4.445" y1="-2.54" x2="-4.445" y2="0" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="-2.54" x2="-4.445" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="2.54" x2="-3.048" y2="2.54" width="0.1524" layer="94"/>
<wire x1="3.048" y1="2.54" x2="5.08" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="0" x2="-4.445" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="2.54" x2="0" y2="2.54" width="0.3048" layer="94"/>
<circle x="-2.54" y="2.54" radius="0.508" width="0.3048" layer="94"/>
<circle x="2.54" y="2.54" radius="0.508" width="0.3048" layer="94"/>
<text x="1.778" y="3.048" size="1.27" layer="94" rot="MR0">I</text>
<text x="1.778" y="0.508" size="1.27" layer="94" rot="MR0">II</text>
<text x="1.778" y="-2.032" size="1.27" layer="94" rot="MR0">III</text>
<text x="1.778" y="-4.572" size="1.27" layer="94" rot="MR0">IV</text>
<circle x="-4.445" y="-2.54" radius="0.254" width="0.508" layer="94"/>
<circle x="-4.445" y="0" radius="0.254" width="0.508" layer="94"/>
<circle x="-4.445" y="2.54" radius="0.254" width="0.508" layer="94"/>
<wire x1="0" y1="2.54" x2="2.032" y2="2.54" width="0.3048" layer="94"/>
<wire x1="-4.445" y1="2.54" x2="-7.62" y2="2.54" width="0.1524" layer="94"/>
<wire x1="3.048" y1="0" x2="5.08" y2="0" width="0.1524" layer="94"/>
<wire x1="3.048" y1="-2.54" x2="5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="3.048" y1="-5.08" x2="5.08" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="0" x2="-3.048" y2="0" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="-2.54" x2="-3.048" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="-5.08" x2="-3.048" y2="-5.08" width="0.1524" layer="94"/>
<circle x="-2.54" y="0" radius="0.508" width="0.3048" layer="94"/>
<circle x="-2.54" y="-2.54" radius="0.508" width="0.3048" layer="94"/>
<circle x="-2.54" y="-5.08" radius="0.508" width="0.3048" layer="94"/>
<circle x="2.54" y="0" radius="0.508" width="0.3048" layer="94"/>
<circle x="2.54" y="-2.54" radius="0.508" width="0.3048" layer="94"/>
<circle x="2.54" y="-5.08" radius="0.508" width="0.3048" layer="94"/>
<pin name="0" x="-10.16" y="2.54" visible="pad" length="short" direction="pas"/>
<pin name="I" x="10.16" y="2.54" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="II" x="10.16" y="0" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="III" x="10.16" y="-2.54" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="IV" x="10.16" y="-5.08" visible="pad" length="middle" direction="pas" rot="R180"/>
<wire x1="-6.35" y1="8.89" x2="-6.35" y2="-7.62" width="0.1524" layer="94" style="shortdash"/>
<wire x1="-6.35" y1="-7.62" x2="6.35" y2="-7.62" width="0.1524" layer="94" style="shortdash"/>
<wire x1="6.35" y1="-7.62" x2="6.35" y2="8.89" width="0.1524" layer="94" style="shortdash"/>
<wire x1="0" y1="2.54" x2="0" y2="8.89" width="0.1524" layer="94" style="shortdash"/>
</symbol>
<symbol name="DESKA-RELE">
<wire x1="277.495" y1="-100.965" x2="281.305" y2="-100.965" width="0.254" layer="94"/>
<wire x1="281.305" y1="-100.965" x2="281.305" y2="-81.915" width="0.254" layer="94"/>
<wire x1="281.305" y1="-81.915" x2="277.495" y2="-81.915" width="0.254" layer="94"/>
<wire x1="277.495" y1="-81.915" x2="277.495" y2="-83.82" width="0.254" layer="94"/>
<wire x1="277.495" y1="-83.82" x2="277.495" y2="-88.9" width="0.254" layer="94"/>
<wire x1="277.495" y1="-88.9" x2="277.495" y2="-93.98" width="0.254" layer="94"/>
<wire x1="277.495" y1="-93.98" x2="277.495" y2="-99.06" width="0.254" layer="94"/>
<wire x1="277.495" y1="-99.06" x2="277.495" y2="-100.965" width="0.254" layer="94"/>
<wire x1="279.4" y1="-100.33" x2="279.4" y2="-97.79" width="0.254" layer="94" curve="-180"/>
<wire x1="279.4" y1="-85.09" x2="279.4" y2="-82.55" width="0.254" layer="94" curve="-180"/>
<wire x1="279.4" y1="-100.33" x2="280.67" y2="-100.33" width="0.254" layer="94"/>
<wire x1="280.67" y1="-100.33" x2="280.67" y2="-97.79" width="0.254" layer="94"/>
<wire x1="280.67" y1="-97.79" x2="279.4" y2="-97.79" width="0.254" layer="94"/>
<wire x1="279.4" y1="-85.09" x2="280.67" y2="-85.09" width="0.254" layer="94"/>
<wire x1="280.67" y1="-85.09" x2="280.67" y2="-82.55" width="0.254" layer="94"/>
<wire x1="280.67" y1="-82.55" x2="279.4" y2="-82.55" width="0.254" layer="94"/>
<circle x="279.4" y="-93.98" radius="1.4199" width="0.254" layer="94"/>
<circle x="279.4" y="-88.9" radius="1.4199" width="0.254" layer="94"/>
<text x="279.4" y="-101.6" size="1.778" layer="96" align="top-center">INLET1</text>
<text x="279.4" y="-81.28" size="1.778" layer="95" align="bottom-center">JP6A</text>
<text x="279.4" y="-99.06" size="1.27" layer="94" rot="R90" align="center">1</text>
<text x="279.4" y="-93.98" size="1.27" layer="94" rot="R90" align="center">2</text>
<text x="279.4" y="-88.9" size="1.27" layer="94" rot="R90" align="center">3</text>
<text x="279.4" y="-83.82" size="1.27" layer="94" rot="R90" align="center">4</text>
<pin name="JP6A-1" x="274.32" y="-99.06" visible="off" length="short" direction="pas"/>
<pin name="JP6A-2" x="274.32" y="-93.98" visible="off" length="short" direction="pas"/>
<pin name="JP6A-3" x="274.32" y="-88.9" visible="off" length="short" direction="pas"/>
<pin name="JP6A-4" x="274.32" y="-83.82" visible="off" length="short" direction="pas"/>
<wire x1="276.86" y1="-83.82" x2="277.495" y2="-83.82" width="0.1524" layer="94"/>
<wire x1="276.86" y1="-88.9" x2="277.495" y2="-88.9" width="0.1524" layer="94"/>
<wire x1="276.86" y1="-93.98" x2="277.495" y2="-93.98" width="0.1524" layer="94"/>
<wire x1="276.86" y1="-99.06" x2="277.495" y2="-99.06" width="0.1524" layer="94"/>
<wire x1="23.495" y1="-37.465" x2="27.305" y2="-37.465" width="0.254" layer="94"/>
<wire x1="27.305" y1="-37.465" x2="27.305" y2="-8.255" width="0.254" layer="94"/>
<wire x1="27.305" y1="-8.255" x2="23.495" y2="-8.255" width="0.254" layer="94"/>
<wire x1="23.495" y1="-8.255" x2="23.495" y2="-10.16" width="0.254" layer="94"/>
<wire x1="23.495" y1="-10.16" x2="23.495" y2="-15.24" width="0.254" layer="94"/>
<wire x1="23.495" y1="-15.24" x2="23.495" y2="-20.32" width="0.254" layer="94"/>
<wire x1="23.495" y1="-20.32" x2="23.495" y2="-25.4" width="0.254" layer="94"/>
<wire x1="23.495" y1="-25.4" x2="23.495" y2="-30.48" width="0.254" layer="94"/>
<wire x1="23.495" y1="-30.48" x2="23.495" y2="-35.56" width="0.254" layer="94"/>
<wire x1="23.495" y1="-35.56" x2="23.495" y2="-37.465" width="0.254" layer="94"/>
<wire x1="25.4" y1="-36.83" x2="25.4" y2="-34.29" width="0.254" layer="94" curve="-180"/>
<wire x1="25.4" y1="-11.43" x2="25.4" y2="-8.89" width="0.254" layer="94" curve="-180"/>
<wire x1="25.4" y1="-36.83" x2="26.67" y2="-36.83" width="0.254" layer="94"/>
<wire x1="26.67" y1="-36.83" x2="26.67" y2="-34.29" width="0.254" layer="94"/>
<wire x1="26.67" y1="-34.29" x2="25.4" y2="-34.29" width="0.254" layer="94"/>
<wire x1="25.4" y1="-11.43" x2="26.67" y2="-11.43" width="0.254" layer="94"/>
<wire x1="26.67" y1="-11.43" x2="26.67" y2="-8.89" width="0.254" layer="94"/>
<wire x1="26.67" y1="-8.89" x2="25.4" y2="-8.89" width="0.254" layer="94"/>
<circle x="25.4" y="-30.48" radius="1.4199" width="0.254" layer="94"/>
<circle x="25.4" y="-25.4" radius="1.4199" width="0.254" layer="94"/>
<circle x="25.4" y="-15.24" radius="1.4199" width="0.254" layer="94"/>
<circle x="25.4" y="-20.32" radius="1.4199" width="0.254" layer="94"/>
<text x="25.4" y="-7.62" size="1.778" layer="95" align="bottom-center">JP1</text>
<text x="25.4" y="-35.56" size="1.27" layer="94" rot="R90" align="center">1</text>
<text x="25.4" y="-30.48" size="1.27" layer="94" rot="R90" align="center">2</text>
<text x="25.4" y="-25.4" size="1.27" layer="94" rot="R90" align="center">3</text>
<text x="25.4" y="-20.32" size="1.27" layer="94" rot="R90" align="center">4</text>
<text x="25.4" y="-15.24" size="1.27" layer="94" rot="R90" align="center">5</text>
<text x="25.4" y="-10.16" size="1.27" layer="94" rot="R90" align="center">6</text>
<pin name="JP1-1" x="20.32" y="-35.56" visible="off" length="short" direction="pas"/>
<pin name="JP1-2" x="20.32" y="-30.48" visible="off" length="short" direction="pas"/>
<pin name="JP1-3" x="20.32" y="-25.4" visible="off" length="short" direction="pas"/>
<pin name="JP1-4" x="20.32" y="-20.32" visible="off" length="short" direction="pas"/>
<pin name="JP1-5" x="20.32" y="-15.24" visible="off" length="short" direction="pas"/>
<pin name="JP1-6" x="20.32" y="-10.16" visible="off" length="short" direction="pas"/>
<wire x1="22.86" y1="-10.16" x2="23.495" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="22.86" y1="-15.24" x2="23.495" y2="-15.24" width="0.1524" layer="94"/>
<wire x1="22.86" y1="-20.32" x2="23.495" y2="-20.32" width="0.1524" layer="94"/>
<wire x1="22.86" y1="-25.4" x2="23.495" y2="-25.4" width="0.1524" layer="94"/>
<wire x1="22.86" y1="-30.48" x2="23.495" y2="-30.48" width="0.1524" layer="94"/>
<wire x1="22.86" y1="-35.56" x2="23.495" y2="-35.56" width="0.1524" layer="94"/>
<text x="53.34" y="-50.8" size="1.778" layer="95" rot="R180">MTEMP</text>
<text x="129.54" y="-50.8" size="1.778" layer="95" rot="R180">COMPRESSOR</text>
<text x="180.34" y="-40.64" size="1.778" layer="95" rot="R180">GND</text>
<text x="180.34" y="-48.26" size="1.778" layer="95" rot="R180">GND</text>
<text x="78.74" y="-43.18" size="1.778" layer="95" rot="R180">+24V</text>
<text x="53.34" y="-45.72" size="1.778" layer="95" rot="R180">CONHEAT</text>
<text x="27.94" y="-40.64" size="1.778" layer="95" rot="R180">HEAT</text>
<text x="27.94" y="-48.26" size="1.778" layer="95" rot="R180">HEATPWR</text>
<text x="27.94" y="-53.34" size="1.778" layer="95" rot="R180">FAILHEAT</text>
<text x="129.54" y="-53.34" size="1.778" layer="95" rot="R180">H/LSWITCH2</text>
<text x="129.54" y="-45.72" size="1.778" layer="95" rot="R180">LIGHT</text>
<text x="27.94" y="-43.18" size="1.778" layer="95" rot="R180">HEATSTART</text>
<text x="27.94" y="-45.72" size="1.778" layer="95" rot="R180">HEATSTART</text>
<text x="154.94" y="-43.18" size="1.778" layer="96" rot="R180">CLAMP+</text>
<text x="154.94" y="-48.26" size="1.778" layer="96" rot="R180">D+</text>
<text x="78.74" y="-53.34" size="1.778" layer="96" rot="R180">VENTILATION</text>
<text x="180.34" y="-43.18" size="1.778" layer="95" rot="R180">STARTACCU</text>
<text x="27.94" y="-50.8" size="1.778" layer="95" rot="R180">FAILH/C</text>
<text x="53.34" y="-43.18" size="1.778" layer="95" rot="R180">PUMPSTART</text>
<text x="53.34" y="-40.64" size="1.778" layer="95" rot="R180">PUMP</text>
<text x="78.74" y="-45.72" size="1.778" layer="95" rot="R180">AKU</text>
<text x="53.34" y="-48.26" size="1.778" layer="95" rot="R180">CHKHEAT</text>
<text x="129.54" y="-40.64" size="1.778" layer="95" rot="R180">POWERTMC</text>
<text x="129.54" y="-43.18" size="1.778" layer="95" rot="R180">VENTCOOL0</text>
<text x="53.34" y="-53.34" size="1.778" layer="95" rot="R180">10V</text>
<text x="78.74" y="-50.8" size="1.778" layer="95" rot="R180">VENTSTART</text>
<text x="154.94" y="-40.64" size="1.778" layer="96" rot="R180">CLAMP-</text>
<text x="78.74" y="-40.64" size="1.778" layer="95" rot="R180">+24V</text>
<text x="78.74" y="-48.26" size="1.778" layer="95" rot="R180">COOL</text>
<text x="129.54" y="-48.26" size="1.778" layer="95" rot="R180">COOLSTART</text>
<text x="180.34" y="-45.72" size="1.778" layer="95" rot="R180">ALT</text>
<text x="180.34" y="-50.8" size="1.778" layer="96" rot="R180">NOTUSED</text>
<text x="180.34" y="-53.34" size="1.778" layer="96" rot="R180">NOTUSED</text>
<text x="154.94" y="-50.8" size="1.778" layer="96" rot="R180">R2</text>
<text x="154.94" y="-53.34" size="1.778" layer="96" rot="R180">R1</text>
<text x="256.54" y="-53.34" size="1.778" layer="95" rot="R180">CHKFLOOR</text>
<text x="256.54" y="-48.26" size="1.778" layer="95" rot="R180">CHKREC</text>
<text x="256.54" y="-43.18" size="1.778" layer="95" rot="R180">CHKI1</text>
<text x="256.54" y="-45.72" size="1.778" layer="95" rot="R180">CHKI2</text>
<text x="256.54" y="-40.64" size="1.778" layer="95" rot="R180">CONI,R</text>
<text x="154.94" y="-45.72" size="1.778" layer="96" rot="R180">+24COOL</text>
<text x="205.74" y="-43.18" size="1.778" layer="96" rot="R180">NOTUSED</text>
<text x="205.74" y="-45.72" size="1.778" layer="96" rot="R180">NOTUSED</text>
<text x="205.74" y="-40.64" size="1.778" layer="95" rot="R180">CON5</text>
<text x="205.74" y="-50.8" size="1.778" layer="95" rot="R180">HP</text>
<text x="205.74" y="-53.34" size="1.778" layer="95" rot="R180">LP</text>
<text x="205.74" y="-48.26" size="1.778" layer="95" rot="R180">HEATTEMP</text>
<text x="231.14" y="-40.64" size="1.778" layer="95" rot="R180">HEATTEMP</text>
<text x="231.14" y="-43.18" size="1.778" layer="95" rot="R180">GND</text>
<text x="231.14" y="-45.72" size="1.778" layer="95" rot="R180">HP+</text>
<text x="231.14" y="-48.26" size="1.778" layer="95" rot="R180">HP-</text>
<text x="231.14" y="-50.8" size="1.778" layer="95" rot="R180">LP+</text>
<text x="231.14" y="-53.34" size="1.778" layer="95" rot="R180">LP-</text>
<text x="256.54" y="-50.8" size="1.778" layer="96" rot="R180">NOTUSED</text>
<text x="281.94" y="-48.26" size="1.778" layer="95" rot="R180">GND</text>
<text x="281.94" y="-45.72" size="1.778" layer="95" rot="R180">FAN_100%</text>
<text x="281.94" y="-43.18" size="1.778" layer="95" rot="R180">FAN_50%</text>
<text x="281.94" y="-40.64" size="1.778" layer="95" rot="R180">FAN_REG</text>
<text x="3.81" y="-104.775" size="2.54" layer="95">ver.: 09-00</text>
<wire x1="0.635" y1="-109.22" x2="292.1" y2="-109.22" width="0.3048" layer="94"/>
<wire x1="292.1" y1="-109.22" x2="292.1" y2="-0.635" width="0.3048" layer="94"/>
<wire x1="292.1" y1="-0.635" x2="291.465" y2="0" width="0.3048" layer="94"/>
<wire x1="291.465" y1="0" x2="1.27" y2="0" width="0.3048" layer="94"/>
<wire x1="1.27" y1="0" x2="0.9525" y2="0" width="0.3048" layer="94"/>
<wire x1="0.9525" y1="0" x2="0.635" y2="0" width="0.3048" layer="94"/>
<wire x1="0.635" y1="0" x2="0.3175" y2="0" width="0.3048" layer="94"/>
<wire x1="0.3175" y1="0" x2="0" y2="0" width="0.3048" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-0.3175" width="0.3048" layer="94"/>
<wire x1="0" y1="-0.3175" x2="0" y2="-0.635" width="0.3048" layer="94"/>
<wire x1="0" y1="-0.635" x2="0" y2="-0.9525" width="0.3048" layer="94"/>
<wire x1="0" y1="-0.9525" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="-108.585" width="0.3048" layer="94"/>
<wire x1="0" y1="-108.585" x2="0.635" y2="-109.22" width="0.3048" layer="94"/>
<wire x1="291.465" y1="0" x2="291.465" y2="-108.585" width="0.3048" layer="94"/>
<wire x1="291.465" y1="-108.585" x2="0" y2="-108.585" width="0.3048" layer="94"/>
<wire x1="291.465" y1="-108.585" x2="292.1" y2="-109.22" width="0.3048" layer="94"/>
<text x="5.08" y="-96.52" size="1.27" layer="94">Část desky:</text>
<text x="20.32" y="-96.52" size="1.27" layer="94">&gt;GATE</text>
<text x="5.08" y="-93.98" size="1.27" layer="94">Karta ABRA:</text>
<text x="20.32" y="-93.98" size="1.27" layer="94">&gt;ABRA</text>
<text x="5.08" y="-91.44" size="1.27" layer="94">Označení desky:</text>
<text x="20.32" y="-91.44" size="1.27" layer="94">&gt;VALUE</text>
<text x="5.08" y="-81.28" size="1.778" layer="94" rot="MR180">Deska Relé</text>
<wire x1="48.895" y1="-37.465" x2="52.705" y2="-37.465" width="0.254" layer="94"/>
<wire x1="52.705" y1="-37.465" x2="52.705" y2="-8.255" width="0.254" layer="94"/>
<wire x1="52.705" y1="-8.255" x2="48.895" y2="-8.255" width="0.254" layer="94"/>
<wire x1="48.895" y1="-8.255" x2="48.895" y2="-10.16" width="0.254" layer="94"/>
<wire x1="48.895" y1="-10.16" x2="48.895" y2="-15.24" width="0.254" layer="94"/>
<wire x1="48.895" y1="-15.24" x2="48.895" y2="-20.32" width="0.254" layer="94"/>
<wire x1="48.895" y1="-20.32" x2="48.895" y2="-25.4" width="0.254" layer="94"/>
<wire x1="48.895" y1="-25.4" x2="48.895" y2="-30.48" width="0.254" layer="94"/>
<wire x1="48.895" y1="-30.48" x2="48.895" y2="-35.56" width="0.254" layer="94"/>
<wire x1="48.895" y1="-35.56" x2="48.895" y2="-37.465" width="0.254" layer="94"/>
<wire x1="50.8" y1="-36.83" x2="50.8" y2="-34.29" width="0.254" layer="94" curve="-180"/>
<wire x1="50.8" y1="-11.43" x2="50.8" y2="-8.89" width="0.254" layer="94" curve="-180"/>
<wire x1="50.8" y1="-36.83" x2="52.07" y2="-36.83" width="0.254" layer="94"/>
<wire x1="52.07" y1="-36.83" x2="52.07" y2="-34.29" width="0.254" layer="94"/>
<wire x1="52.07" y1="-34.29" x2="50.8" y2="-34.29" width="0.254" layer="94"/>
<wire x1="50.8" y1="-11.43" x2="52.07" y2="-11.43" width="0.254" layer="94"/>
<wire x1="52.07" y1="-11.43" x2="52.07" y2="-8.89" width="0.254" layer="94"/>
<wire x1="52.07" y1="-8.89" x2="50.8" y2="-8.89" width="0.254" layer="94"/>
<circle x="50.8" y="-30.48" radius="1.4199" width="0.254" layer="94"/>
<circle x="50.8" y="-25.4" radius="1.4199" width="0.254" layer="94"/>
<circle x="50.8" y="-15.24" radius="1.4199" width="0.254" layer="94"/>
<circle x="50.8" y="-20.32" radius="1.4199" width="0.254" layer="94"/>
<text x="50.8" y="-7.62" size="1.778" layer="95" align="bottom-center">JP2</text>
<text x="50.8" y="-35.56" size="1.27" layer="94" rot="R90" align="center">1</text>
<text x="50.8" y="-30.48" size="1.27" layer="94" rot="R90" align="center">2</text>
<text x="50.8" y="-25.4" size="1.27" layer="94" rot="R90" align="center">3</text>
<text x="50.8" y="-20.32" size="1.27" layer="94" rot="R90" align="center">4</text>
<text x="50.8" y="-15.24" size="1.27" layer="94" rot="R90" align="center">5</text>
<text x="50.8" y="-10.16" size="1.27" layer="94" rot="R90" align="center">6</text>
<pin name="JP2-1" x="45.72" y="-35.56" visible="off" length="short" direction="pas"/>
<pin name="JP2-2" x="45.72" y="-30.48" visible="off" length="short" direction="pas"/>
<pin name="JP2-3" x="45.72" y="-25.4" visible="off" length="short" direction="pas"/>
<pin name="JP2-4" x="45.72" y="-20.32" visible="off" length="short" direction="pas"/>
<pin name="JP2-5" x="45.72" y="-15.24" visible="off" length="short" direction="pas"/>
<pin name="JP2-6" x="45.72" y="-10.16" visible="off" length="short" direction="pas"/>
<wire x1="48.26" y1="-10.16" x2="48.895" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="48.26" y1="-15.24" x2="48.895" y2="-15.24" width="0.1524" layer="94"/>
<wire x1="48.26" y1="-20.32" x2="48.895" y2="-20.32" width="0.1524" layer="94"/>
<wire x1="48.26" y1="-25.4" x2="48.895" y2="-25.4" width="0.1524" layer="94"/>
<wire x1="48.26" y1="-30.48" x2="48.895" y2="-30.48" width="0.1524" layer="94"/>
<wire x1="48.26" y1="-35.56" x2="48.895" y2="-35.56" width="0.1524" layer="94"/>
<wire x1="74.295" y1="-37.465" x2="78.105" y2="-37.465" width="0.254" layer="94"/>
<wire x1="78.105" y1="-37.465" x2="78.105" y2="-8.255" width="0.254" layer="94"/>
<wire x1="78.105" y1="-8.255" x2="74.295" y2="-8.255" width="0.254" layer="94"/>
<wire x1="74.295" y1="-8.255" x2="74.295" y2="-10.16" width="0.254" layer="94"/>
<wire x1="74.295" y1="-10.16" x2="74.295" y2="-15.24" width="0.254" layer="94"/>
<wire x1="74.295" y1="-15.24" x2="74.295" y2="-20.32" width="0.254" layer="94"/>
<wire x1="74.295" y1="-20.32" x2="74.295" y2="-25.4" width="0.254" layer="94"/>
<wire x1="74.295" y1="-25.4" x2="74.295" y2="-30.48" width="0.254" layer="94"/>
<wire x1="74.295" y1="-30.48" x2="74.295" y2="-35.56" width="0.254" layer="94"/>
<wire x1="74.295" y1="-35.56" x2="74.295" y2="-37.465" width="0.254" layer="94"/>
<wire x1="76.2" y1="-36.83" x2="76.2" y2="-34.29" width="0.254" layer="94" curve="-180"/>
<wire x1="76.2" y1="-11.43" x2="76.2" y2="-8.89" width="0.254" layer="94" curve="-180"/>
<wire x1="76.2" y1="-36.83" x2="77.47" y2="-36.83" width="0.254" layer="94"/>
<wire x1="77.47" y1="-36.83" x2="77.47" y2="-34.29" width="0.254" layer="94"/>
<wire x1="77.47" y1="-34.29" x2="76.2" y2="-34.29" width="0.254" layer="94"/>
<wire x1="76.2" y1="-11.43" x2="77.47" y2="-11.43" width="0.254" layer="94"/>
<wire x1="77.47" y1="-11.43" x2="77.47" y2="-8.89" width="0.254" layer="94"/>
<wire x1="77.47" y1="-8.89" x2="76.2" y2="-8.89" width="0.254" layer="94"/>
<circle x="76.2" y="-30.48" radius="1.4199" width="0.254" layer="94"/>
<circle x="76.2" y="-25.4" radius="1.4199" width="0.254" layer="94"/>
<circle x="76.2" y="-15.24" radius="1.4199" width="0.254" layer="94"/>
<circle x="76.2" y="-20.32" radius="1.4199" width="0.254" layer="94"/>
<text x="76.2" y="-7.62" size="1.778" layer="95" align="bottom-center">JP3</text>
<text x="76.2" y="-35.56" size="1.27" layer="94" rot="R90" align="center">1</text>
<text x="76.2" y="-30.48" size="1.27" layer="94" rot="R90" align="center">2</text>
<text x="76.2" y="-25.4" size="1.27" layer="94" rot="R90" align="center">3</text>
<text x="76.2" y="-20.32" size="1.27" layer="94" rot="R90" align="center">4</text>
<text x="76.2" y="-15.24" size="1.27" layer="94" rot="R90" align="center">5</text>
<text x="76.2" y="-10.16" size="1.27" layer="94" rot="R90" align="center">6</text>
<pin name="JP3-1" x="71.12" y="-35.56" visible="off" length="short" direction="pas"/>
<pin name="JP3-2" x="71.12" y="-30.48" visible="off" length="short" direction="pas"/>
<pin name="JP3-3" x="71.12" y="-25.4" visible="off" length="short" direction="pas"/>
<pin name="JP3-4" x="71.12" y="-20.32" visible="off" length="short" direction="pas"/>
<pin name="JP3-5" x="71.12" y="-15.24" visible="off" length="short" direction="pas"/>
<pin name="JP3-6" x="71.12" y="-10.16" visible="off" length="short" direction="pas"/>
<wire x1="73.66" y1="-10.16" x2="74.295" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="73.66" y1="-15.24" x2="74.295" y2="-15.24" width="0.1524" layer="94"/>
<wire x1="73.66" y1="-20.32" x2="74.295" y2="-20.32" width="0.1524" layer="94"/>
<wire x1="73.66" y1="-25.4" x2="74.295" y2="-25.4" width="0.1524" layer="94"/>
<wire x1="73.66" y1="-30.48" x2="74.295" y2="-30.48" width="0.1524" layer="94"/>
<wire x1="73.66" y1="-35.56" x2="74.295" y2="-35.56" width="0.1524" layer="94"/>
<wire x1="125.095" y1="-37.465" x2="128.905" y2="-37.465" width="0.254" layer="94"/>
<wire x1="128.905" y1="-37.465" x2="128.905" y2="-8.255" width="0.254" layer="94"/>
<wire x1="128.905" y1="-8.255" x2="125.095" y2="-8.255" width="0.254" layer="94"/>
<wire x1="125.095" y1="-8.255" x2="125.095" y2="-10.16" width="0.254" layer="94"/>
<wire x1="125.095" y1="-10.16" x2="125.095" y2="-15.24" width="0.254" layer="94"/>
<wire x1="125.095" y1="-15.24" x2="125.095" y2="-20.32" width="0.254" layer="94"/>
<wire x1="125.095" y1="-20.32" x2="125.095" y2="-25.4" width="0.254" layer="94"/>
<wire x1="125.095" y1="-25.4" x2="125.095" y2="-30.48" width="0.254" layer="94"/>
<wire x1="125.095" y1="-30.48" x2="125.095" y2="-35.56" width="0.254" layer="94"/>
<wire x1="125.095" y1="-35.56" x2="125.095" y2="-37.465" width="0.254" layer="94"/>
<wire x1="127" y1="-36.83" x2="127" y2="-34.29" width="0.254" layer="94" curve="-180"/>
<wire x1="127" y1="-11.43" x2="127" y2="-8.89" width="0.254" layer="94" curve="-180"/>
<wire x1="127" y1="-36.83" x2="128.27" y2="-36.83" width="0.254" layer="94"/>
<wire x1="128.27" y1="-36.83" x2="128.27" y2="-34.29" width="0.254" layer="94"/>
<wire x1="128.27" y1="-34.29" x2="127" y2="-34.29" width="0.254" layer="94"/>
<wire x1="127" y1="-11.43" x2="128.27" y2="-11.43" width="0.254" layer="94"/>
<wire x1="128.27" y1="-11.43" x2="128.27" y2="-8.89" width="0.254" layer="94"/>
<wire x1="128.27" y1="-8.89" x2="127" y2="-8.89" width="0.254" layer="94"/>
<circle x="127" y="-30.48" radius="1.4199" width="0.254" layer="94"/>
<circle x="127" y="-25.4" radius="1.4199" width="0.254" layer="94"/>
<circle x="127" y="-15.24" radius="1.4199" width="0.254" layer="94"/>
<circle x="127" y="-20.32" radius="1.4199" width="0.254" layer="94"/>
<text x="127" y="-7.62" size="1.778" layer="95" align="bottom-center">JP4</text>
<text x="127" y="-35.56" size="1.27" layer="94" rot="R90" align="center">1</text>
<text x="127" y="-30.48" size="1.27" layer="94" rot="R90" align="center">2</text>
<text x="127" y="-25.4" size="1.27" layer="94" rot="R90" align="center">3</text>
<text x="127" y="-20.32" size="1.27" layer="94" rot="R90" align="center">4</text>
<text x="127" y="-15.24" size="1.27" layer="94" rot="R90" align="center">5</text>
<text x="127" y="-10.16" size="1.27" layer="94" rot="R90" align="center">6</text>
<pin name="JP4-1" x="121.92" y="-35.56" visible="off" length="short" direction="pas"/>
<pin name="JP4-2" x="121.92" y="-30.48" visible="off" length="short" direction="pas"/>
<pin name="JP4-3" x="121.92" y="-25.4" visible="off" length="short" direction="pas"/>
<pin name="JP4-4" x="121.92" y="-20.32" visible="off" length="short" direction="pas"/>
<pin name="JP4-5" x="121.92" y="-15.24" visible="off" length="short" direction="pas"/>
<pin name="JP4-6" x="121.92" y="-10.16" visible="off" length="short" direction="pas"/>
<wire x1="124.46" y1="-10.16" x2="125.095" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="124.46" y1="-15.24" x2="125.095" y2="-15.24" width="0.1524" layer="94"/>
<wire x1="124.46" y1="-20.32" x2="125.095" y2="-20.32" width="0.1524" layer="94"/>
<wire x1="124.46" y1="-25.4" x2="125.095" y2="-25.4" width="0.1524" layer="94"/>
<wire x1="124.46" y1="-30.48" x2="125.095" y2="-30.48" width="0.1524" layer="94"/>
<wire x1="124.46" y1="-35.56" x2="125.095" y2="-35.56" width="0.1524" layer="94"/>
<wire x1="150.495" y1="-37.465" x2="154.305" y2="-37.465" width="0.254" layer="94"/>
<wire x1="154.305" y1="-37.465" x2="154.305" y2="-8.255" width="0.254" layer="94"/>
<wire x1="154.305" y1="-8.255" x2="150.495" y2="-8.255" width="0.254" layer="94"/>
<wire x1="150.495" y1="-8.255" x2="150.495" y2="-10.16" width="0.254" layer="94"/>
<wire x1="150.495" y1="-10.16" x2="150.495" y2="-15.24" width="0.254" layer="94"/>
<wire x1="150.495" y1="-15.24" x2="150.495" y2="-20.32" width="0.254" layer="94"/>
<wire x1="150.495" y1="-20.32" x2="150.495" y2="-25.4" width="0.254" layer="94"/>
<wire x1="150.495" y1="-25.4" x2="150.495" y2="-30.48" width="0.254" layer="94"/>
<wire x1="150.495" y1="-30.48" x2="150.495" y2="-35.56" width="0.254" layer="94"/>
<wire x1="150.495" y1="-35.56" x2="150.495" y2="-37.465" width="0.254" layer="94"/>
<wire x1="152.4" y1="-36.83" x2="152.4" y2="-34.29" width="0.254" layer="94" curve="-180"/>
<wire x1="152.4" y1="-11.43" x2="152.4" y2="-8.89" width="0.254" layer="94" curve="-180"/>
<wire x1="152.4" y1="-36.83" x2="153.67" y2="-36.83" width="0.254" layer="94"/>
<wire x1="153.67" y1="-36.83" x2="153.67" y2="-34.29" width="0.254" layer="94"/>
<wire x1="153.67" y1="-34.29" x2="152.4" y2="-34.29" width="0.254" layer="94"/>
<wire x1="152.4" y1="-11.43" x2="153.67" y2="-11.43" width="0.254" layer="94"/>
<wire x1="153.67" y1="-11.43" x2="153.67" y2="-8.89" width="0.254" layer="94"/>
<wire x1="153.67" y1="-8.89" x2="152.4" y2="-8.89" width="0.254" layer="94"/>
<circle x="152.4" y="-30.48" radius="1.4199" width="0.254" layer="94"/>
<circle x="152.4" y="-25.4" radius="1.4199" width="0.254" layer="94"/>
<circle x="152.4" y="-15.24" radius="1.4199" width="0.254" layer="94"/>
<circle x="152.4" y="-20.32" radius="1.4199" width="0.254" layer="94"/>
<text x="152.4" y="-7.62" size="1.778" layer="95" align="bottom-center">JP5</text>
<text x="152.4" y="-35.56" size="1.27" layer="94" rot="R90" align="center">1</text>
<text x="152.4" y="-30.48" size="1.27" layer="94" rot="R90" align="center">2</text>
<text x="152.4" y="-25.4" size="1.27" layer="94" rot="R90" align="center">3</text>
<text x="152.4" y="-20.32" size="1.27" layer="94" rot="R90" align="center">4</text>
<text x="152.4" y="-15.24" size="1.27" layer="94" rot="R90" align="center">5</text>
<text x="152.4" y="-10.16" size="1.27" layer="94" rot="R90" align="center">6</text>
<pin name="JP5-1" x="147.32" y="-35.56" visible="off" length="short" direction="pas"/>
<pin name="JP5-2" x="147.32" y="-30.48" visible="off" length="short" direction="pas"/>
<pin name="JP5-3" x="147.32" y="-25.4" visible="off" length="short" direction="pas"/>
<pin name="JP5-4" x="147.32" y="-20.32" visible="off" length="short" direction="pas"/>
<pin name="JP5-5" x="147.32" y="-15.24" visible="off" length="short" direction="pas"/>
<pin name="JP5-6" x="147.32" y="-10.16" visible="off" length="short" direction="pas"/>
<wire x1="149.86" y1="-10.16" x2="150.495" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="149.86" y1="-15.24" x2="150.495" y2="-15.24" width="0.1524" layer="94"/>
<wire x1="149.86" y1="-20.32" x2="150.495" y2="-20.32" width="0.1524" layer="94"/>
<wire x1="149.86" y1="-25.4" x2="150.495" y2="-25.4" width="0.1524" layer="94"/>
<wire x1="149.86" y1="-30.48" x2="150.495" y2="-30.48" width="0.1524" layer="94"/>
<wire x1="149.86" y1="-35.56" x2="150.495" y2="-35.56" width="0.1524" layer="94"/>
<wire x1="175.895" y1="-37.465" x2="179.705" y2="-37.465" width="0.254" layer="94"/>
<wire x1="179.705" y1="-37.465" x2="179.705" y2="-8.255" width="0.254" layer="94"/>
<wire x1="179.705" y1="-8.255" x2="175.895" y2="-8.255" width="0.254" layer="94"/>
<wire x1="175.895" y1="-8.255" x2="175.895" y2="-10.16" width="0.254" layer="94"/>
<wire x1="175.895" y1="-10.16" x2="175.895" y2="-15.24" width="0.254" layer="94"/>
<wire x1="175.895" y1="-15.24" x2="175.895" y2="-20.32" width="0.254" layer="94"/>
<wire x1="175.895" y1="-20.32" x2="175.895" y2="-25.4" width="0.254" layer="94"/>
<wire x1="175.895" y1="-25.4" x2="175.895" y2="-30.48" width="0.254" layer="94"/>
<wire x1="175.895" y1="-30.48" x2="175.895" y2="-35.56" width="0.254" layer="94"/>
<wire x1="175.895" y1="-35.56" x2="175.895" y2="-37.465" width="0.254" layer="94"/>
<wire x1="177.8" y1="-36.83" x2="177.8" y2="-34.29" width="0.254" layer="94" curve="-180"/>
<wire x1="177.8" y1="-11.43" x2="177.8" y2="-8.89" width="0.254" layer="94" curve="-180"/>
<wire x1="177.8" y1="-36.83" x2="179.07" y2="-36.83" width="0.254" layer="94"/>
<wire x1="179.07" y1="-36.83" x2="179.07" y2="-34.29" width="0.254" layer="94"/>
<wire x1="179.07" y1="-34.29" x2="177.8" y2="-34.29" width="0.254" layer="94"/>
<wire x1="177.8" y1="-11.43" x2="179.07" y2="-11.43" width="0.254" layer="94"/>
<wire x1="179.07" y1="-11.43" x2="179.07" y2="-8.89" width="0.254" layer="94"/>
<wire x1="179.07" y1="-8.89" x2="177.8" y2="-8.89" width="0.254" layer="94"/>
<circle x="177.8" y="-30.48" radius="1.4199" width="0.254" layer="94"/>
<circle x="177.8" y="-25.4" radius="1.4199" width="0.254" layer="94"/>
<circle x="177.8" y="-15.24" radius="1.4199" width="0.254" layer="94"/>
<circle x="177.8" y="-20.32" radius="1.4199" width="0.254" layer="94"/>
<text x="177.8" y="-7.62" size="1.778" layer="95" align="bottom-center">JP6</text>
<text x="177.8" y="-35.56" size="1.27" layer="94" rot="R90" align="center">1</text>
<text x="177.8" y="-30.48" size="1.27" layer="94" rot="R90" align="center">2</text>
<text x="177.8" y="-25.4" size="1.27" layer="94" rot="R90" align="center">3</text>
<text x="177.8" y="-20.32" size="1.27" layer="94" rot="R90" align="center">4</text>
<text x="177.8" y="-15.24" size="1.27" layer="94" rot="R90" align="center">5</text>
<text x="177.8" y="-10.16" size="1.27" layer="94" rot="R90" align="center">6</text>
<pin name="JP6-1" x="172.72" y="-35.56" visible="off" length="short" direction="pas"/>
<pin name="JP6-2" x="172.72" y="-30.48" visible="off" length="short" direction="pas"/>
<pin name="JP6-3" x="172.72" y="-25.4" visible="off" length="short" direction="pas"/>
<pin name="JP6-4" x="172.72" y="-20.32" visible="off" length="short" direction="pas"/>
<pin name="JP6-5" x="172.72" y="-15.24" visible="off" length="short" direction="pas"/>
<pin name="JP6-6" x="172.72" y="-10.16" visible="off" length="short" direction="pas"/>
<wire x1="175.26" y1="-10.16" x2="175.895" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="175.26" y1="-15.24" x2="175.895" y2="-15.24" width="0.1524" layer="94"/>
<wire x1="175.26" y1="-20.32" x2="175.895" y2="-20.32" width="0.1524" layer="94"/>
<wire x1="175.26" y1="-25.4" x2="175.895" y2="-25.4" width="0.1524" layer="94"/>
<wire x1="175.26" y1="-30.48" x2="175.895" y2="-30.48" width="0.1524" layer="94"/>
<wire x1="175.26" y1="-35.56" x2="175.895" y2="-35.56" width="0.1524" layer="94"/>
<wire x1="201.295" y1="-37.465" x2="205.105" y2="-37.465" width="0.254" layer="94"/>
<wire x1="205.105" y1="-37.465" x2="205.105" y2="-8.255" width="0.254" layer="94"/>
<wire x1="205.105" y1="-8.255" x2="201.295" y2="-8.255" width="0.254" layer="94"/>
<wire x1="201.295" y1="-8.255" x2="201.295" y2="-10.16" width="0.254" layer="94"/>
<wire x1="201.295" y1="-10.16" x2="201.295" y2="-15.24" width="0.254" layer="94"/>
<wire x1="201.295" y1="-15.24" x2="201.295" y2="-20.32" width="0.254" layer="94"/>
<wire x1="201.295" y1="-20.32" x2="201.295" y2="-25.4" width="0.254" layer="94"/>
<wire x1="201.295" y1="-25.4" x2="201.295" y2="-30.48" width="0.254" layer="94"/>
<wire x1="201.295" y1="-30.48" x2="201.295" y2="-35.56" width="0.254" layer="94"/>
<wire x1="201.295" y1="-35.56" x2="201.295" y2="-37.465" width="0.254" layer="94"/>
<wire x1="203.2" y1="-36.83" x2="203.2" y2="-34.29" width="0.254" layer="94" curve="-180"/>
<wire x1="203.2" y1="-11.43" x2="203.2" y2="-8.89" width="0.254" layer="94" curve="-180"/>
<wire x1="203.2" y1="-36.83" x2="204.47" y2="-36.83" width="0.254" layer="94"/>
<wire x1="204.47" y1="-36.83" x2="204.47" y2="-34.29" width="0.254" layer="94"/>
<wire x1="204.47" y1="-34.29" x2="203.2" y2="-34.29" width="0.254" layer="94"/>
<wire x1="203.2" y1="-11.43" x2="204.47" y2="-11.43" width="0.254" layer="94"/>
<wire x1="204.47" y1="-11.43" x2="204.47" y2="-8.89" width="0.254" layer="94"/>
<wire x1="204.47" y1="-8.89" x2="203.2" y2="-8.89" width="0.254" layer="94"/>
<circle x="203.2" y="-30.48" radius="1.4199" width="0.254" layer="94"/>
<circle x="203.2" y="-25.4" radius="1.4199" width="0.254" layer="94"/>
<circle x="203.2" y="-15.24" radius="1.4199" width="0.254" layer="94"/>
<circle x="203.2" y="-20.32" radius="1.4199" width="0.254" layer="94"/>
<text x="203.2" y="-7.62" size="1.778" layer="95" align="bottom-center">JP7</text>
<text x="203.2" y="-35.56" size="1.27" layer="94" rot="R90" align="center">1</text>
<text x="203.2" y="-30.48" size="1.27" layer="94" rot="R90" align="center">2</text>
<text x="203.2" y="-25.4" size="1.27" layer="94" rot="R90" align="center">3</text>
<text x="203.2" y="-20.32" size="1.27" layer="94" rot="R90" align="center">4</text>
<text x="203.2" y="-15.24" size="1.27" layer="94" rot="R90" align="center">5</text>
<text x="203.2" y="-10.16" size="1.27" layer="94" rot="R90" align="center">6</text>
<pin name="JP7-1" x="198.12" y="-35.56" visible="off" length="short" direction="pas"/>
<pin name="JP7-2" x="198.12" y="-30.48" visible="off" length="short" direction="pas"/>
<pin name="JP7-3" x="198.12" y="-25.4" visible="off" length="short" direction="pas"/>
<pin name="JP7-4" x="198.12" y="-20.32" visible="off" length="short" direction="pas"/>
<pin name="JP7-5" x="198.12" y="-15.24" visible="off" length="short" direction="pas"/>
<pin name="JP7-6" x="198.12" y="-10.16" visible="off" length="short" direction="pas"/>
<wire x1="200.66" y1="-10.16" x2="201.295" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="200.66" y1="-15.24" x2="201.295" y2="-15.24" width="0.1524" layer="94"/>
<wire x1="200.66" y1="-20.32" x2="201.295" y2="-20.32" width="0.1524" layer="94"/>
<wire x1="200.66" y1="-25.4" x2="201.295" y2="-25.4" width="0.1524" layer="94"/>
<wire x1="200.66" y1="-30.48" x2="201.295" y2="-30.48" width="0.1524" layer="94"/>
<wire x1="200.66" y1="-35.56" x2="201.295" y2="-35.56" width="0.1524" layer="94"/>
<wire x1="226.695" y1="-37.465" x2="230.505" y2="-37.465" width="0.254" layer="94"/>
<wire x1="230.505" y1="-37.465" x2="230.505" y2="-8.255" width="0.254" layer="94"/>
<wire x1="230.505" y1="-8.255" x2="226.695" y2="-8.255" width="0.254" layer="94"/>
<wire x1="226.695" y1="-8.255" x2="226.695" y2="-10.16" width="0.254" layer="94"/>
<wire x1="226.695" y1="-10.16" x2="226.695" y2="-15.24" width="0.254" layer="94"/>
<wire x1="226.695" y1="-15.24" x2="226.695" y2="-20.32" width="0.254" layer="94"/>
<wire x1="226.695" y1="-20.32" x2="226.695" y2="-25.4" width="0.254" layer="94"/>
<wire x1="226.695" y1="-25.4" x2="226.695" y2="-30.48" width="0.254" layer="94"/>
<wire x1="226.695" y1="-30.48" x2="226.695" y2="-35.56" width="0.254" layer="94"/>
<wire x1="226.695" y1="-35.56" x2="226.695" y2="-37.465" width="0.254" layer="94"/>
<wire x1="228.6" y1="-36.83" x2="228.6" y2="-34.29" width="0.254" layer="94" curve="-180"/>
<wire x1="228.6" y1="-11.43" x2="228.6" y2="-8.89" width="0.254" layer="94" curve="-180"/>
<wire x1="228.6" y1="-36.83" x2="229.87" y2="-36.83" width="0.254" layer="94"/>
<wire x1="229.87" y1="-36.83" x2="229.87" y2="-34.29" width="0.254" layer="94"/>
<wire x1="229.87" y1="-34.29" x2="228.6" y2="-34.29" width="0.254" layer="94"/>
<wire x1="228.6" y1="-11.43" x2="229.87" y2="-11.43" width="0.254" layer="94"/>
<wire x1="229.87" y1="-11.43" x2="229.87" y2="-8.89" width="0.254" layer="94"/>
<wire x1="229.87" y1="-8.89" x2="228.6" y2="-8.89" width="0.254" layer="94"/>
<circle x="228.6" y="-30.48" radius="1.4199" width="0.254" layer="94"/>
<circle x="228.6" y="-25.4" radius="1.4199" width="0.254" layer="94"/>
<circle x="228.6" y="-15.24" radius="1.4199" width="0.254" layer="94"/>
<circle x="228.6" y="-20.32" radius="1.4199" width="0.254" layer="94"/>
<text x="228.6" y="-7.62" size="1.778" layer="95" align="bottom-center">JP8</text>
<text x="228.6" y="-35.56" size="1.27" layer="94" rot="R90" align="center">1</text>
<text x="228.6" y="-30.48" size="1.27" layer="94" rot="R90" align="center">2</text>
<text x="228.6" y="-25.4" size="1.27" layer="94" rot="R90" align="center">3</text>
<text x="228.6" y="-20.32" size="1.27" layer="94" rot="R90" align="center">4</text>
<text x="228.6" y="-15.24" size="1.27" layer="94" rot="R90" align="center">5</text>
<text x="228.6" y="-10.16" size="1.27" layer="94" rot="R90" align="center">6</text>
<pin name="JP8-1" x="223.52" y="-35.56" visible="off" length="short" direction="pas"/>
<pin name="JP8-2" x="223.52" y="-30.48" visible="off" length="short" direction="pas"/>
<pin name="JP8-3" x="223.52" y="-25.4" visible="off" length="short" direction="pas"/>
<pin name="JP8-4" x="223.52" y="-20.32" visible="off" length="short" direction="pas"/>
<pin name="JP8-5" x="223.52" y="-15.24" visible="off" length="short" direction="pas"/>
<pin name="JP8-6" x="223.52" y="-10.16" visible="off" length="short" direction="pas"/>
<wire x1="226.06" y1="-10.16" x2="226.695" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="226.06" y1="-15.24" x2="226.695" y2="-15.24" width="0.1524" layer="94"/>
<wire x1="226.06" y1="-20.32" x2="226.695" y2="-20.32" width="0.1524" layer="94"/>
<wire x1="226.06" y1="-25.4" x2="226.695" y2="-25.4" width="0.1524" layer="94"/>
<wire x1="226.06" y1="-30.48" x2="226.695" y2="-30.48" width="0.1524" layer="94"/>
<wire x1="226.06" y1="-35.56" x2="226.695" y2="-35.56" width="0.1524" layer="94"/>
<wire x1="252.095" y1="-37.465" x2="255.905" y2="-37.465" width="0.254" layer="94"/>
<wire x1="255.905" y1="-37.465" x2="255.905" y2="-8.255" width="0.254" layer="94"/>
<wire x1="255.905" y1="-8.255" x2="252.095" y2="-8.255" width="0.254" layer="94"/>
<wire x1="252.095" y1="-8.255" x2="252.095" y2="-10.16" width="0.254" layer="94"/>
<wire x1="252.095" y1="-10.16" x2="252.095" y2="-15.24" width="0.254" layer="94"/>
<wire x1="252.095" y1="-15.24" x2="252.095" y2="-20.32" width="0.254" layer="94"/>
<wire x1="252.095" y1="-20.32" x2="252.095" y2="-25.4" width="0.254" layer="94"/>
<wire x1="252.095" y1="-25.4" x2="252.095" y2="-30.48" width="0.254" layer="94"/>
<wire x1="252.095" y1="-30.48" x2="252.095" y2="-35.56" width="0.254" layer="94"/>
<wire x1="252.095" y1="-35.56" x2="252.095" y2="-37.465" width="0.254" layer="94"/>
<wire x1="254" y1="-36.83" x2="254" y2="-34.29" width="0.254" layer="94" curve="-180"/>
<wire x1="254" y1="-11.43" x2="254" y2="-8.89" width="0.254" layer="94" curve="-180"/>
<wire x1="254" y1="-36.83" x2="255.27" y2="-36.83" width="0.254" layer="94"/>
<wire x1="255.27" y1="-36.83" x2="255.27" y2="-34.29" width="0.254" layer="94"/>
<wire x1="255.27" y1="-34.29" x2="254" y2="-34.29" width="0.254" layer="94"/>
<wire x1="254" y1="-11.43" x2="255.27" y2="-11.43" width="0.254" layer="94"/>
<wire x1="255.27" y1="-11.43" x2="255.27" y2="-8.89" width="0.254" layer="94"/>
<wire x1="255.27" y1="-8.89" x2="254" y2="-8.89" width="0.254" layer="94"/>
<circle x="254" y="-30.48" radius="1.4199" width="0.254" layer="94"/>
<circle x="254" y="-25.4" radius="1.4199" width="0.254" layer="94"/>
<circle x="254" y="-15.24" radius="1.4199" width="0.254" layer="94"/>
<circle x="254" y="-20.32" radius="1.4199" width="0.254" layer="94"/>
<text x="254" y="-7.62" size="1.778" layer="95" align="bottom-center">JP9</text>
<text x="254" y="-35.56" size="1.27" layer="94" rot="R90" align="center">1</text>
<text x="254" y="-30.48" size="1.27" layer="94" rot="R90" align="center">2</text>
<text x="254" y="-25.4" size="1.27" layer="94" rot="R90" align="center">3</text>
<text x="254" y="-20.32" size="1.27" layer="94" rot="R90" align="center">4</text>
<text x="254" y="-15.24" size="1.27" layer="94" rot="R90" align="center">5</text>
<text x="254" y="-10.16" size="1.27" layer="94" rot="R90" align="center">6</text>
<pin name="JP9-1" x="248.92" y="-35.56" visible="off" length="short" direction="pas"/>
<pin name="JP9-2" x="248.92" y="-30.48" visible="off" length="short" direction="pas"/>
<pin name="JP9-3" x="248.92" y="-25.4" visible="off" length="short" direction="pas"/>
<pin name="JP9-4" x="248.92" y="-20.32" visible="off" length="short" direction="pas"/>
<pin name="JP9-5" x="248.92" y="-15.24" visible="off" length="short" direction="pas"/>
<pin name="JP9-6" x="248.92" y="-10.16" visible="off" length="short" direction="pas"/>
<wire x1="251.46" y1="-10.16" x2="252.095" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="251.46" y1="-15.24" x2="252.095" y2="-15.24" width="0.1524" layer="94"/>
<wire x1="251.46" y1="-20.32" x2="252.095" y2="-20.32" width="0.1524" layer="94"/>
<wire x1="251.46" y1="-25.4" x2="252.095" y2="-25.4" width="0.1524" layer="94"/>
<wire x1="251.46" y1="-30.48" x2="252.095" y2="-30.48" width="0.1524" layer="94"/>
<wire x1="251.46" y1="-35.56" x2="252.095" y2="-35.56" width="0.1524" layer="94"/>
<wire x1="277.495" y1="-27.305" x2="281.305" y2="-27.305" width="0.254" layer="94"/>
<wire x1="281.305" y1="-27.305" x2="281.305" y2="-8.255" width="0.254" layer="94"/>
<wire x1="281.305" y1="-8.255" x2="277.495" y2="-8.255" width="0.254" layer="94"/>
<wire x1="277.495" y1="-8.255" x2="277.495" y2="-10.16" width="0.254" layer="94"/>
<wire x1="277.495" y1="-10.16" x2="277.495" y2="-15.24" width="0.254" layer="94"/>
<wire x1="277.495" y1="-15.24" x2="277.495" y2="-20.32" width="0.254" layer="94"/>
<wire x1="277.495" y1="-20.32" x2="277.495" y2="-25.4" width="0.254" layer="94"/>
<wire x1="277.495" y1="-25.4" x2="277.495" y2="-27.305" width="0.254" layer="94"/>
<wire x1="279.4" y1="-26.67" x2="279.4" y2="-24.13" width="0.254" layer="94" curve="-180"/>
<wire x1="279.4" y1="-11.43" x2="279.4" y2="-8.89" width="0.254" layer="94" curve="-180"/>
<wire x1="279.4" y1="-26.67" x2="280.67" y2="-26.67" width="0.254" layer="94"/>
<wire x1="280.67" y1="-26.67" x2="280.67" y2="-24.13" width="0.254" layer="94"/>
<wire x1="280.67" y1="-24.13" x2="279.4" y2="-24.13" width="0.254" layer="94"/>
<wire x1="279.4" y1="-11.43" x2="280.67" y2="-11.43" width="0.254" layer="94"/>
<wire x1="280.67" y1="-11.43" x2="280.67" y2="-8.89" width="0.254" layer="94"/>
<wire x1="280.67" y1="-8.89" x2="279.4" y2="-8.89" width="0.254" layer="94"/>
<circle x="279.4" y="-20.32" radius="1.4199" width="0.254" layer="94"/>
<circle x="279.4" y="-15.24" radius="1.4199" width="0.254" layer="94"/>
<text x="279.4" y="-7.62" size="1.778" layer="95" align="bottom-center">JP10</text>
<text x="279.4" y="-25.4" size="1.27" layer="94" rot="R90" align="center">1</text>
<text x="279.4" y="-20.32" size="1.27" layer="94" rot="R90" align="center">2</text>
<text x="279.4" y="-15.24" size="1.27" layer="94" rot="R90" align="center">3</text>
<text x="279.4" y="-10.16" size="1.27" layer="94" rot="R90" align="center">4</text>
<pin name="JP10-1" x="274.32" y="-25.4" visible="off" length="short" direction="pas"/>
<pin name="JP10-2" x="274.32" y="-20.32" visible="off" length="short" direction="pas"/>
<pin name="JP10-3" x="274.32" y="-15.24" visible="off" length="short" direction="pas"/>
<pin name="JP10-4" x="274.32" y="-10.16" visible="off" length="short" direction="pas"/>
<wire x1="276.86" y1="-10.16" x2="277.495" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="276.86" y1="-15.24" x2="277.495" y2="-15.24" width="0.1524" layer="94"/>
<wire x1="276.86" y1="-20.32" x2="277.495" y2="-20.32" width="0.1524" layer="94"/>
<wire x1="276.86" y1="-25.4" x2="277.495" y2="-25.4" width="0.1524" layer="94"/>
<wire x1="150.495" y1="-100.965" x2="154.305" y2="-100.965" width="0.254" layer="94"/>
<wire x1="154.305" y1="-100.965" x2="154.305" y2="-81.915" width="0.254" layer="94"/>
<wire x1="154.305" y1="-81.915" x2="150.495" y2="-81.915" width="0.254" layer="94"/>
<wire x1="150.495" y1="-81.915" x2="150.495" y2="-83.82" width="0.254" layer="94"/>
<wire x1="150.495" y1="-83.82" x2="150.495" y2="-88.9" width="0.254" layer="94"/>
<wire x1="150.495" y1="-88.9" x2="150.495" y2="-93.98" width="0.254" layer="94"/>
<wire x1="150.495" y1="-93.98" x2="150.495" y2="-99.06" width="0.254" layer="94"/>
<wire x1="150.495" y1="-99.06" x2="150.495" y2="-100.965" width="0.254" layer="94"/>
<wire x1="152.4" y1="-100.33" x2="152.4" y2="-97.79" width="0.254" layer="94" curve="-180"/>
<wire x1="152.4" y1="-85.09" x2="152.4" y2="-82.55" width="0.254" layer="94" curve="-180"/>
<wire x1="152.4" y1="-100.33" x2="153.67" y2="-100.33" width="0.254" layer="94"/>
<wire x1="153.67" y1="-100.33" x2="153.67" y2="-97.79" width="0.254" layer="94"/>
<wire x1="153.67" y1="-97.79" x2="152.4" y2="-97.79" width="0.254" layer="94"/>
<wire x1="152.4" y1="-85.09" x2="153.67" y2="-85.09" width="0.254" layer="94"/>
<wire x1="153.67" y1="-85.09" x2="153.67" y2="-82.55" width="0.254" layer="94"/>
<wire x1="153.67" y1="-82.55" x2="152.4" y2="-82.55" width="0.254" layer="94"/>
<circle x="152.4" y="-93.98" radius="1.4199" width="0.254" layer="94"/>
<circle x="152.4" y="-88.9" radius="1.4199" width="0.254" layer="94"/>
<text x="152.4" y="-101.6" size="1.778" layer="96" align="top-center">MOTOR</text>
<text x="152.4" y="-81.28" size="1.778" layer="95" align="bottom-center">JP1A</text>
<text x="152.4" y="-99.06" size="1.27" layer="94" rot="R90" align="center">1</text>
<text x="152.4" y="-93.98" size="1.27" layer="94" rot="R90" align="center">2</text>
<text x="152.4" y="-88.9" size="1.27" layer="94" rot="R90" align="center">3</text>
<text x="152.4" y="-83.82" size="1.27" layer="94" rot="R90" align="center">4</text>
<pin name="JP1A-1" x="147.32" y="-99.06" visible="off" length="short" direction="pas"/>
<pin name="JP1A-2" x="147.32" y="-93.98" visible="off" length="short" direction="pas"/>
<pin name="JP1A-3" x="147.32" y="-88.9" visible="off" length="short" direction="pas"/>
<pin name="JP1A-4" x="147.32" y="-83.82" visible="off" length="short" direction="pas"/>
<wire x1="149.86" y1="-83.82" x2="150.495" y2="-83.82" width="0.1524" layer="94"/>
<wire x1="149.86" y1="-88.9" x2="150.495" y2="-88.9" width="0.1524" layer="94"/>
<wire x1="149.86" y1="-93.98" x2="150.495" y2="-93.98" width="0.1524" layer="94"/>
<wire x1="149.86" y1="-99.06" x2="150.495" y2="-99.06" width="0.1524" layer="94"/>
<wire x1="175.895" y1="-100.965" x2="179.705" y2="-100.965" width="0.254" layer="94"/>
<wire x1="179.705" y1="-100.965" x2="179.705" y2="-81.915" width="0.254" layer="94"/>
<wire x1="179.705" y1="-81.915" x2="175.895" y2="-81.915" width="0.254" layer="94"/>
<wire x1="175.895" y1="-81.915" x2="175.895" y2="-83.82" width="0.254" layer="94"/>
<wire x1="175.895" y1="-83.82" x2="175.895" y2="-88.9" width="0.254" layer="94"/>
<wire x1="175.895" y1="-88.9" x2="175.895" y2="-93.98" width="0.254" layer="94"/>
<wire x1="175.895" y1="-93.98" x2="175.895" y2="-99.06" width="0.254" layer="94"/>
<wire x1="175.895" y1="-99.06" x2="175.895" y2="-100.965" width="0.254" layer="94"/>
<wire x1="177.8" y1="-100.33" x2="177.8" y2="-97.79" width="0.254" layer="94" curve="-180"/>
<wire x1="177.8" y1="-85.09" x2="177.8" y2="-82.55" width="0.254" layer="94" curve="-180"/>
<wire x1="177.8" y1="-100.33" x2="179.07" y2="-100.33" width="0.254" layer="94"/>
<wire x1="179.07" y1="-100.33" x2="179.07" y2="-97.79" width="0.254" layer="94"/>
<wire x1="179.07" y1="-97.79" x2="177.8" y2="-97.79" width="0.254" layer="94"/>
<wire x1="177.8" y1="-85.09" x2="179.07" y2="-85.09" width="0.254" layer="94"/>
<wire x1="179.07" y1="-85.09" x2="179.07" y2="-82.55" width="0.254" layer="94"/>
<wire x1="179.07" y1="-82.55" x2="177.8" y2="-82.55" width="0.254" layer="94"/>
<circle x="177.8" y="-93.98" radius="1.4199" width="0.254" layer="94"/>
<circle x="177.8" y="-88.9" radius="1.4199" width="0.254" layer="94"/>
<text x="177.8" y="-101.6" size="1.778" layer="96" align="top-center">HEAT</text>
<text x="177.8" y="-81.28" size="1.778" layer="95" align="bottom-center">JP2A</text>
<text x="177.8" y="-99.06" size="1.27" layer="94" rot="R90" align="center">1</text>
<text x="177.8" y="-93.98" size="1.27" layer="94" rot="R90" align="center">2</text>
<text x="177.8" y="-88.9" size="1.27" layer="94" rot="R90" align="center">3</text>
<text x="177.8" y="-83.82" size="1.27" layer="94" rot="R90" align="center">4</text>
<pin name="JP2A-1" x="172.72" y="-99.06" visible="off" length="short" direction="pas"/>
<pin name="JP2A-2" x="172.72" y="-93.98" visible="off" length="short" direction="pas"/>
<pin name="JP2A-3" x="172.72" y="-88.9" visible="off" length="short" direction="pas"/>
<pin name="JP2A-4" x="172.72" y="-83.82" visible="off" length="short" direction="pas"/>
<wire x1="175.26" y1="-83.82" x2="175.895" y2="-83.82" width="0.1524" layer="94"/>
<wire x1="175.26" y1="-88.9" x2="175.895" y2="-88.9" width="0.1524" layer="94"/>
<wire x1="175.26" y1="-93.98" x2="175.895" y2="-93.98" width="0.1524" layer="94"/>
<wire x1="175.26" y1="-99.06" x2="175.895" y2="-99.06" width="0.1524" layer="94"/>
<wire x1="201.295" y1="-100.965" x2="205.105" y2="-100.965" width="0.254" layer="94"/>
<wire x1="205.105" y1="-100.965" x2="205.105" y2="-81.915" width="0.254" layer="94"/>
<wire x1="205.105" y1="-81.915" x2="201.295" y2="-81.915" width="0.254" layer="94"/>
<wire x1="201.295" y1="-81.915" x2="201.295" y2="-83.82" width="0.254" layer="94"/>
<wire x1="201.295" y1="-83.82" x2="201.295" y2="-88.9" width="0.254" layer="94"/>
<wire x1="201.295" y1="-88.9" x2="201.295" y2="-93.98" width="0.254" layer="94"/>
<wire x1="201.295" y1="-93.98" x2="201.295" y2="-99.06" width="0.254" layer="94"/>
<wire x1="201.295" y1="-99.06" x2="201.295" y2="-100.965" width="0.254" layer="94"/>
<wire x1="203.2" y1="-100.33" x2="203.2" y2="-97.79" width="0.254" layer="94" curve="-180"/>
<wire x1="203.2" y1="-85.09" x2="203.2" y2="-82.55" width="0.254" layer="94" curve="-180"/>
<wire x1="203.2" y1="-100.33" x2="204.47" y2="-100.33" width="0.254" layer="94"/>
<wire x1="204.47" y1="-100.33" x2="204.47" y2="-97.79" width="0.254" layer="94"/>
<wire x1="204.47" y1="-97.79" x2="203.2" y2="-97.79" width="0.254" layer="94"/>
<wire x1="203.2" y1="-85.09" x2="204.47" y2="-85.09" width="0.254" layer="94"/>
<wire x1="204.47" y1="-85.09" x2="204.47" y2="-82.55" width="0.254" layer="94"/>
<wire x1="204.47" y1="-82.55" x2="203.2" y2="-82.55" width="0.254" layer="94"/>
<circle x="203.2" y="-93.98" radius="1.4199" width="0.254" layer="94"/>
<circle x="203.2" y="-88.9" radius="1.4199" width="0.254" layer="94"/>
<text x="228.6" y="-101.6" size="1.778" layer="96" align="top-center">REC</text>
<text x="203.2" y="-81.28" size="1.778" layer="95" align="bottom-center">JP3A</text>
<text x="203.2" y="-99.06" size="1.27" layer="94" rot="R90" align="center">1</text>
<text x="203.2" y="-93.98" size="1.27" layer="94" rot="R90" align="center">2</text>
<text x="203.2" y="-88.9" size="1.27" layer="94" rot="R90" align="center">3</text>
<text x="203.2" y="-83.82" size="1.27" layer="94" rot="R90" align="center">4</text>
<pin name="JP3A-1" x="198.12" y="-99.06" visible="off" length="short" direction="pas"/>
<pin name="JP3A-2" x="198.12" y="-93.98" visible="off" length="short" direction="pas"/>
<pin name="JP3A-3" x="198.12" y="-88.9" visible="off" length="short" direction="pas"/>
<pin name="JP3A-4" x="198.12" y="-83.82" visible="off" length="short" direction="pas"/>
<wire x1="200.66" y1="-83.82" x2="201.295" y2="-83.82" width="0.1524" layer="94"/>
<wire x1="200.66" y1="-88.9" x2="201.295" y2="-88.9" width="0.1524" layer="94"/>
<wire x1="200.66" y1="-93.98" x2="201.295" y2="-93.98" width="0.1524" layer="94"/>
<wire x1="200.66" y1="-99.06" x2="201.295" y2="-99.06" width="0.1524" layer="94"/>
<wire x1="226.695" y1="-100.965" x2="230.505" y2="-100.965" width="0.254" layer="94"/>
<wire x1="230.505" y1="-100.965" x2="230.505" y2="-81.915" width="0.254" layer="94"/>
<wire x1="230.505" y1="-81.915" x2="226.695" y2="-81.915" width="0.254" layer="94"/>
<wire x1="226.695" y1="-81.915" x2="226.695" y2="-83.82" width="0.254" layer="94"/>
<wire x1="226.695" y1="-83.82" x2="226.695" y2="-88.9" width="0.254" layer="94"/>
<wire x1="226.695" y1="-88.9" x2="226.695" y2="-93.98" width="0.254" layer="94"/>
<wire x1="226.695" y1="-93.98" x2="226.695" y2="-99.06" width="0.254" layer="94"/>
<wire x1="226.695" y1="-99.06" x2="226.695" y2="-100.965" width="0.254" layer="94"/>
<wire x1="228.6" y1="-100.33" x2="228.6" y2="-97.79" width="0.254" layer="94" curve="-180"/>
<wire x1="228.6" y1="-85.09" x2="228.6" y2="-82.55" width="0.254" layer="94" curve="-180"/>
<wire x1="228.6" y1="-100.33" x2="229.87" y2="-100.33" width="0.254" layer="94"/>
<wire x1="229.87" y1="-100.33" x2="229.87" y2="-97.79" width="0.254" layer="94"/>
<wire x1="229.87" y1="-97.79" x2="228.6" y2="-97.79" width="0.254" layer="94"/>
<wire x1="228.6" y1="-85.09" x2="229.87" y2="-85.09" width="0.254" layer="94"/>
<wire x1="229.87" y1="-85.09" x2="229.87" y2="-82.55" width="0.254" layer="94"/>
<wire x1="229.87" y1="-82.55" x2="228.6" y2="-82.55" width="0.254" layer="94"/>
<circle x="228.6" y="-93.98" radius="1.4199" width="0.254" layer="94"/>
<circle x="228.6" y="-88.9" radius="1.4199" width="0.254" layer="94"/>
<text x="203.2" y="-101.6" size="1.778" layer="96" align="top-center">FLOOR</text>
<text x="228.6" y="-81.28" size="1.778" layer="95" align="bottom-center">JP4A</text>
<text x="228.6" y="-99.06" size="1.27" layer="94" rot="R90" align="center">1</text>
<text x="228.6" y="-93.98" size="1.27" layer="94" rot="R90" align="center">2</text>
<text x="228.6" y="-88.9" size="1.27" layer="94" rot="R90" align="center">3</text>
<text x="228.6" y="-83.82" size="1.27" layer="94" rot="R90" align="center">4</text>
<pin name="JP4A-1" x="223.52" y="-99.06" visible="off" length="short" direction="pas"/>
<pin name="JP4A-2" x="223.52" y="-93.98" visible="off" length="short" direction="pas"/>
<pin name="JP4A-3" x="223.52" y="-88.9" visible="off" length="short" direction="pas"/>
<pin name="JP4A-4" x="223.52" y="-83.82" visible="off" length="short" direction="pas"/>
<wire x1="226.06" y1="-83.82" x2="226.695" y2="-83.82" width="0.1524" layer="94"/>
<wire x1="226.06" y1="-88.9" x2="226.695" y2="-88.9" width="0.1524" layer="94"/>
<wire x1="226.06" y1="-93.98" x2="226.695" y2="-93.98" width="0.1524" layer="94"/>
<wire x1="226.06" y1="-99.06" x2="226.695" y2="-99.06" width="0.1524" layer="94"/>
<wire x1="252.095" y1="-100.965" x2="255.905" y2="-100.965" width="0.254" layer="94"/>
<wire x1="255.905" y1="-100.965" x2="255.905" y2="-81.915" width="0.254" layer="94"/>
<wire x1="255.905" y1="-81.915" x2="252.095" y2="-81.915" width="0.254" layer="94"/>
<wire x1="252.095" y1="-81.915" x2="252.095" y2="-83.82" width="0.254" layer="94"/>
<wire x1="252.095" y1="-83.82" x2="252.095" y2="-88.9" width="0.254" layer="94"/>
<wire x1="252.095" y1="-88.9" x2="252.095" y2="-93.98" width="0.254" layer="94"/>
<wire x1="252.095" y1="-93.98" x2="252.095" y2="-99.06" width="0.254" layer="94"/>
<wire x1="252.095" y1="-99.06" x2="252.095" y2="-100.965" width="0.254" layer="94"/>
<wire x1="254" y1="-100.33" x2="254" y2="-97.79" width="0.254" layer="94" curve="-180"/>
<wire x1="254" y1="-85.09" x2="254" y2="-82.55" width="0.254" layer="94" curve="-180"/>
<wire x1="254" y1="-100.33" x2="255.27" y2="-100.33" width="0.254" layer="94"/>
<wire x1="255.27" y1="-100.33" x2="255.27" y2="-97.79" width="0.254" layer="94"/>
<wire x1="255.27" y1="-97.79" x2="254" y2="-97.79" width="0.254" layer="94"/>
<wire x1="254" y1="-85.09" x2="255.27" y2="-85.09" width="0.254" layer="94"/>
<wire x1="255.27" y1="-85.09" x2="255.27" y2="-82.55" width="0.254" layer="94"/>
<wire x1="255.27" y1="-82.55" x2="254" y2="-82.55" width="0.254" layer="94"/>
<circle x="254" y="-93.98" radius="1.4199" width="0.254" layer="94"/>
<circle x="254" y="-88.9" radius="1.4199" width="0.254" layer="94"/>
<text x="254" y="-101.6" size="1.778" layer="96" align="top-center">INLET2</text>
<text x="254" y="-81.28" size="1.778" layer="95" align="bottom-center">JP5A</text>
<text x="254" y="-99.06" size="1.27" layer="94" rot="R90" align="center">1</text>
<text x="254" y="-93.98" size="1.27" layer="94" rot="R90" align="center">2</text>
<text x="254" y="-88.9" size="1.27" layer="94" rot="R90" align="center">3</text>
<text x="254" y="-83.82" size="1.27" layer="94" rot="R90" align="center">4</text>
<pin name="JP5A-1" x="248.92" y="-99.06" visible="off" length="short" direction="pas"/>
<pin name="JP5A-2" x="248.92" y="-93.98" visible="off" length="short" direction="pas"/>
<pin name="JP5A-3" x="248.92" y="-88.9" visible="off" length="short" direction="pas"/>
<pin name="JP5A-4" x="248.92" y="-83.82" visible="off" length="short" direction="pas"/>
<wire x1="251.46" y1="-83.82" x2="252.095" y2="-83.82" width="0.1524" layer="94"/>
<wire x1="251.46" y1="-88.9" x2="252.095" y2="-88.9" width="0.1524" layer="94"/>
<wire x1="251.46" y1="-93.98" x2="252.095" y2="-93.98" width="0.1524" layer="94"/>
<wire x1="251.46" y1="-99.06" x2="252.095" y2="-99.06" width="0.1524" layer="94"/>
<text x="290.195" y="-1.27" size="1.778" layer="95" align="top-right">&gt;NAME</text>
<wire x1="0" y1="-1.27" x2="1.27" y2="0" width="0.3048" layer="94"/>
<wire x1="0" y1="-0.9525" x2="0.9525" y2="0" width="0.3048" layer="94"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.3048" layer="94"/>
<wire x1="0" y1="-0.3175" x2="0.3175" y2="0" width="0.3048" layer="94"/>
</symbol>
<symbol name="OTOCNY_PREPINAC_2">
<text x="2.54" y="2.54" size="1.778" layer="94" rot="R180" align="bottom-center">&gt;GATE</text>
<wire x1="-1.905" y1="3.81" x2="-1.905" y2="2.54" width="0.254" layer="94"/>
<wire x1="-1.905" y1="3.81" x2="0" y2="3.81" width="0.254" layer="94"/>
<wire x1="1.905" y1="3.81" x2="1.905" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="3.81" x2="0" y2="1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="3.81" x2="1.905" y2="3.81" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="0" y2="0.762" width="0.1524" layer="94"/>
<wire x1="0" y1="-0.254" x2="0" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="-2.413" width="0.1524" layer="94"/>
<wire x1="0" y1="0.762" x2="-0.762" y2="0.254" width="0.1524" layer="94"/>
<wire x1="-0.762" y1="0.254" x2="0" y2="-0.254" width="0.1524" layer="94"/>
<wire x1="3.048" y1="-2.54" x2="5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="-2.54" x2="2.032" y2="-2.54" width="0.3048" layer="94"/>
<circle x="-2.54" y="-2.54" radius="0.508" width="0.3048" layer="94"/>
<circle x="2.54" y="-2.54" radius="0.508" width="0.3048" layer="94"/>
<text x="1.778" y="-2.032" size="1.27" layer="94" rot="MR0">I</text>
<text x="1.778" y="-4.572" size="1.27" layer="94" rot="MR0">II</text>
<wire x1="-3.048" y1="-2.54" x2="-7.62" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="3.048" y1="-5.08" x2="5.08" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="-3.048" y2="-5.08" width="0.1524" layer="94"/>
<circle x="-2.54" y="-5.08" radius="0.508" width="0.3048" layer="94"/>
<circle x="2.54" y="-5.08" radius="0.508" width="0.3048" layer="94"/>
<pin name="1" x="-10.16" y="-2.54" visible="pad" length="short" direction="pas"/>
<pin name="I" x="10.16" y="-2.54" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="II" x="10.16" y="-5.08" visible="pad" length="middle" direction="pas" rot="R180"/>
<wire x1="-6.35" y1="6.35" x2="-6.35" y2="-7.62" width="0.1524" layer="94" style="shortdash"/>
<wire x1="-6.35" y1="-7.62" x2="6.35" y2="-7.62" width="0.1524" layer="94" style="shortdash"/>
<wire x1="6.35" y1="-7.62" x2="6.35" y2="6.35" width="0.1524" layer="94" style="shortdash"/>
<wire x1="-6.35" y1="6.35" x2="6.35" y2="6.35" width="0.1524" layer="94" style="shortdash"/>
<pin name="2" x="-10.16" y="-5.08" visible="pad" length="short" direction="pas"/>
</symbol>
<symbol name="OTOCNY_PREPINAC_STITEK_2">
<wire x1="-0.635" y1="3.81" x2="0" y2="4.445" width="0.1524" layer="97"/>
<wire x1="0" y1="4.445" x2="0.635" y2="3.81" width="0.1524" layer="97"/>
<wire x1="1.5875" y1="-4.445" x2="-1.5875" y2="-4.445" width="0.1524" layer="97"/>
<wire x1="0.635" y1="3.81" x2="1.5875" y2="-4.445" width="0.1524" layer="97" curve="18"/>
<wire x1="-0.635" y1="3.81" x2="-1.5875" y2="-4.445" width="0.1524" layer="97" curve="-18"/>
<wire x1="0.635" y1="3.81" x2="1.27" y2="-3.175" width="0.1524" layer="97" curve="-139.915781"/>
<wire x1="-0.635" y1="3.81" x2="-1.27" y2="-3.175" width="0.1524" layer="97" curve="139.915781"/>
<text x="0" y="5.08" size="0.8128" layer="97" align="center">I</text>
<text x="3.4925" y="3.4925" size="0.8128" layer="97" align="center">II</text>
<wire x1="-6.35" y1="6.35" x2="-6.35" y2="-6.35" width="0.1524" layer="94" style="shortdash"/>
<wire x1="-6.35" y1="-6.35" x2="6.35" y2="-6.35" width="0.1524" layer="94" style="shortdash"/>
<wire x1="6.35" y1="-6.35" x2="6.35" y2="6.35" width="0.1524" layer="94" style="shortdash"/>
<wire x1="-6.35" y1="6.35" x2="6.35" y2="6.35" width="0.1524" layer="94" style="shortdash"/>
<text x="-33.02" y="-5.08" size="1.27" layer="97">Karta ABRA:</text>
<text x="-20.32" y="-5.08" size="1.27" layer="97">&gt;ABRA</text>
<text x="-33.02" y="0" size="1.27" layer="95">Přepínač 100% / 50%</text>
<text x="-33.02" y="-2.54" size="1.27" layer="97">TYP:</text>
<text x="-27.94" y="-2.54" size="1.27" layer="97">&gt;TYP</text>
</symbol>
<symbol name="OTOCNY_PREPINAC_3">
<text x="2.54" y="5.08" size="1.778" layer="94" rot="R180" align="bottom-center">&gt;GATE</text>
<wire x1="-1.905" y1="6.35" x2="-1.905" y2="5.08" width="0.254" layer="94"/>
<wire x1="-1.905" y1="6.35" x2="0" y2="6.35" width="0.254" layer="94"/>
<wire x1="1.905" y1="6.35" x2="1.905" y2="7.62" width="0.254" layer="94"/>
<wire x1="0" y1="6.35" x2="0" y2="4.445" width="0.1524" layer="94"/>
<wire x1="0" y1="6.35" x2="1.905" y2="6.35" width="0.254" layer="94"/>
<wire x1="0" y1="3.81" x2="0" y2="3.302" width="0.1524" layer="94"/>
<wire x1="0" y1="2.286" x2="0" y2="1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="1.27" x2="0" y2="0.127" width="0.1524" layer="94"/>
<wire x1="0" y1="3.302" x2="-0.762" y2="2.794" width="0.1524" layer="94"/>
<wire x1="-0.762" y1="2.794" x2="0" y2="2.286" width="0.1524" layer="94"/>
<wire x1="3.048" y1="0" x2="5.08" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="0" x2="2.032" y2="0" width="0.3048" layer="94"/>
<circle x="-2.54" y="0" radius="0.508" width="0.3048" layer="94"/>
<circle x="2.54" y="0" radius="0.508" width="0.3048" layer="94"/>
<text x="1.778" y="0.508" size="1.27" layer="94" rot="MR0">I</text>
<text x="1.778" y="-2.032" size="1.27" layer="94" rot="MR0">II</text>
<text x="1.778" y="-4.572" size="1.27" layer="94" rot="MR0">III</text>
<wire x1="-3.048" y1="0" x2="-7.62" y2="0" width="0.1524" layer="94"/>
<wire x1="3.048" y1="-2.54" x2="5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="3.048" y1="-5.08" x2="5.08" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="-2.54" x2="-3.048" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="-3.048" y2="-5.08" width="0.1524" layer="94"/>
<circle x="-2.54" y="-2.54" radius="0.508" width="0.3048" layer="94"/>
<circle x="-2.54" y="-5.08" radius="0.508" width="0.3048" layer="94"/>
<circle x="2.54" y="-2.54" radius="0.508" width="0.3048" layer="94"/>
<circle x="2.54" y="-5.08" radius="0.508" width="0.3048" layer="94"/>
<pin name="1" x="-10.16" y="0" visible="pad" length="short" direction="pas"/>
<pin name="I" x="10.16" y="0" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="II" x="10.16" y="-2.54" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="III" x="10.16" y="-5.08" visible="pad" length="middle" direction="pas" rot="R180"/>
<wire x1="-6.35" y1="8.89" x2="-6.35" y2="-7.62" width="0.1524" layer="94" style="shortdash"/>
<wire x1="-6.35" y1="-7.62" x2="6.35" y2="-7.62" width="0.1524" layer="94" style="shortdash"/>
<wire x1="6.35" y1="-7.62" x2="6.35" y2="8.89" width="0.1524" layer="94" style="shortdash"/>
<wire x1="-6.35" y1="8.89" x2="6.35" y2="8.89" width="0.1524" layer="94" style="shortdash"/>
<pin name="2" x="-10.16" y="-2.54" visible="pad" length="short" direction="pas"/>
<pin name="3" x="-10.16" y="-5.08" visible="pad" length="short" direction="pas"/>
</symbol>
<symbol name="OTOCNY_PREPINAC_STITEK_3">
<wire x1="-0.635" y1="3.81" x2="0" y2="4.445" width="0.1524" layer="97"/>
<wire x1="0" y1="4.445" x2="0.635" y2="3.81" width="0.1524" layer="97"/>
<wire x1="1.5875" y1="-4.445" x2="-1.5875" y2="-4.445" width="0.1524" layer="97"/>
<wire x1="0.635" y1="3.81" x2="1.5875" y2="-4.445" width="0.1524" layer="97" curve="18"/>
<wire x1="-0.635" y1="3.81" x2="-1.5875" y2="-4.445" width="0.1524" layer="97" curve="-18"/>
<wire x1="0.635" y1="3.81" x2="1.27" y2="-3.175" width="0.1524" layer="97" curve="-139.915781"/>
<wire x1="-0.635" y1="3.81" x2="-1.27" y2="-3.175" width="0.1524" layer="97" curve="139.915781"/>
<text x="0" y="5.08" size="0.8128" layer="97" align="center">I</text>
<text x="3.4925" y="3.4925" size="0.8128" layer="97" align="center">II</text>
<text x="5.08" y="0" size="0.8128" layer="97" align="center">III</text>
<wire x1="-6.35" y1="6.35" x2="-6.35" y2="-6.35" width="0.1524" layer="94" style="shortdash"/>
<wire x1="-6.35" y1="-6.35" x2="6.35" y2="-6.35" width="0.1524" layer="94" style="shortdash"/>
<wire x1="6.35" y1="-6.35" x2="6.35" y2="6.35" width="0.1524" layer="94" style="shortdash"/>
<wire x1="-6.35" y1="6.35" x2="6.35" y2="6.35" width="0.1524" layer="94" style="shortdash"/>
<text x="-33.02" y="-5.08" size="1.27" layer="97">Karta ABRA:</text>
<text x="-20.32" y="-5.08" size="1.27" layer="97">&gt;ABRA</text>
<text x="-33.02" y="0" size="1.27" layer="95">Přepínač AUTO / 100% / 50%</text>
<text x="-33.02" y="-2.54" size="1.27" layer="97">TYP:</text>
<text x="-27.94" y="-2.54" size="1.27" layer="97">&gt;TYP</text>
</symbol>
<symbol name="OTOCNY_PREPINAC_STITEK_H2">
<wire x1="-0.635" y1="3.81" x2="0" y2="4.445" width="0.1524" layer="97"/>
<wire x1="0" y1="4.445" x2="0.635" y2="3.81" width="0.1524" layer="97"/>
<wire x1="1.5875" y1="-4.445" x2="-1.5875" y2="-4.445" width="0.1524" layer="97"/>
<wire x1="0.635" y1="3.81" x2="1.5875" y2="-4.445" width="0.1524" layer="97" curve="18"/>
<wire x1="-0.635" y1="3.81" x2="-1.5875" y2="-4.445" width="0.1524" layer="97" curve="-18"/>
<wire x1="0.635" y1="3.81" x2="1.27" y2="-3.175" width="0.1524" layer="97" curve="-139.915781"/>
<wire x1="-0.635" y1="3.81" x2="-1.27" y2="-3.175" width="0.1524" layer="97" curve="139.915781"/>
<text x="0" y="5.08" size="0.8128" layer="97" align="center">I</text>
<text x="3.4925" y="3.4925" size="0.8128" layer="97" align="center">II</text>
<wire x1="-6.35" y1="6.35" x2="-6.35" y2="-6.35" width="0.1524" layer="94" style="shortdash"/>
<wire x1="-6.35" y1="-6.35" x2="6.35" y2="-6.35" width="0.1524" layer="94" style="shortdash"/>
<wire x1="6.35" y1="-6.35" x2="6.35" y2="6.35" width="0.1524" layer="94" style="shortdash"/>
<wire x1="-6.35" y1="6.35" x2="6.35" y2="6.35" width="0.1524" layer="94" style="shortdash"/>
<text x="-33.02" y="-5.08" size="1.27" layer="97">Karta ABRA:</text>
<text x="-20.32" y="-5.08" size="1.27" layer="97">&gt;ABRA</text>
<text x="-33.02" y="0" size="1.27" layer="95">Přepínač topení</text>
<text x="-33.02" y="-2.54" size="1.27" layer="97">TYP:</text>
<text x="-27.94" y="-2.54" size="1.27" layer="97">&gt;TYP</text>
</symbol>
<symbol name="OTOCNY_PREPINAC_H2">
<text x="2.54" y="5.08" size="1.778" layer="94" rot="R180" align="bottom-center">&gt;GATE</text>
<wire x1="-1.905" y1="6.35" x2="-1.905" y2="5.08" width="0.254" layer="94"/>
<wire x1="-1.905" y1="6.35" x2="0" y2="6.35" width="0.254" layer="94"/>
<wire x1="1.905" y1="6.35" x2="1.905" y2="7.62" width="0.254" layer="94"/>
<wire x1="0" y1="6.35" x2="0" y2="4.445" width="0.1524" layer="94"/>
<wire x1="0" y1="6.35" x2="1.905" y2="6.35" width="0.254" layer="94"/>
<wire x1="0" y1="3.81" x2="0" y2="3.302" width="0.1524" layer="94"/>
<wire x1="0" y1="2.286" x2="0" y2="1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="1.27" x2="0" y2="0.127" width="0.1524" layer="94"/>
<wire x1="0" y1="3.302" x2="-0.762" y2="2.794" width="0.1524" layer="94"/>
<wire x1="-0.762" y1="2.794" x2="0" y2="2.286" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="0" x2="-3.048" y2="0" width="0.1524" layer="94"/>
<wire x1="3.048" y1="0" x2="5.08" y2="0" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="-2.54" x2="-5.08" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="0" x2="2.032" y2="0" width="0.3048" layer="94"/>
<circle x="-2.54" y="0" radius="0.508" width="0.3048" layer="94"/>
<circle x="2.54" y="0" radius="0.508" width="0.3048" layer="94"/>
<text x="1.778" y="0.508" size="1.27" layer="94" rot="MR0">I</text>
<text x="1.778" y="-2.032" size="1.27" layer="94" rot="MR0">II</text>
<circle x="-5.08" y="0" radius="0.254" width="0.508" layer="94"/>
<wire x1="-5.08" y1="0" x2="-7.62" y2="0" width="0.1524" layer="94"/>
<wire x1="3.048" y1="-2.54" x2="5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="-2.54" x2="-3.048" y2="-2.54" width="0.1524" layer="94"/>
<circle x="-2.54" y="-2.54" radius="0.508" width="0.3048" layer="94"/>
<circle x="2.54" y="-2.54" radius="0.508" width="0.3048" layer="94"/>
<pin name="0" x="-10.16" y="0" visible="pad" length="short" direction="pas"/>
<pin name="I" x="10.16" y="0" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="II" x="10.16" y="-2.54" visible="pad" length="middle" direction="pas" rot="R180"/>
<wire x1="-6.35" y1="8.89" x2="-6.35" y2="-5.08" width="0.1524" layer="94" style="shortdash"/>
<wire x1="-6.35" y1="-5.08" x2="6.35" y2="-5.08" width="0.1524" layer="94" style="shortdash"/>
<wire x1="6.35" y1="-5.08" x2="6.35" y2="8.89" width="0.1524" layer="94" style="shortdash"/>
<wire x1="-6.35" y1="8.89" x2="6.35" y2="8.89" width="0.1524" layer="94" style="shortdash"/>
</symbol>
<symbol name="OTOCNY_PREPINAC_SEKCE_H2">
<text x="2.54" y="5.08" size="1.778" layer="94" rot="R180" align="bottom-center">&gt;GATE</text>
<wire x1="-4.445" y1="0" x2="-3.048" y2="0" width="0.1524" layer="94"/>
<wire x1="3.048" y1="0" x2="5.08" y2="0" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="-2.54" x2="-4.445" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.032" y1="0" x2="0" y2="0" width="0.3048" layer="94"/>
<circle x="-2.54" y="0" radius="0.508" width="0.3048" layer="94"/>
<circle x="2.54" y="0" radius="0.508" width="0.3048" layer="94"/>
<text x="1.778" y="0.508" size="1.27" layer="94" rot="MR0">I</text>
<text x="1.778" y="-2.032" size="1.27" layer="94" rot="MR0">II</text>
<circle x="-4.445" y="0" radius="0.254" width="0.508" layer="94"/>
<wire x1="0" y1="0" x2="2.032" y2="0" width="0.3048" layer="94"/>
<wire x1="-4.445" y1="0" x2="-7.62" y2="0" width="0.1524" layer="94"/>
<wire x1="3.048" y1="-2.54" x2="5.08" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-4.445" y1="-2.54" x2="-3.048" y2="-2.54" width="0.1524" layer="94"/>
<circle x="-2.54" y="-2.54" radius="0.508" width="0.3048" layer="94"/>
<circle x="2.54" y="-2.54" radius="0.508" width="0.3048" layer="94"/>
<pin name="0" x="-10.16" y="0" visible="pad" length="short" direction="pas"/>
<pin name="I" x="10.16" y="0" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="II" x="10.16" y="-2.54" visible="pad" length="middle" direction="pas" rot="R180"/>
<wire x1="-6.35" y1="6.35" x2="-6.35" y2="-5.08" width="0.1524" layer="94" style="shortdash"/>
<wire x1="-6.35" y1="-5.08" x2="6.35" y2="-5.08" width="0.1524" layer="94" style="shortdash"/>
<wire x1="6.35" y1="-5.08" x2="6.35" y2="6.35" width="0.1524" layer="94" style="shortdash"/>
<wire x1="0" y1="0" x2="0" y2="6.35" width="0.1524" layer="94" style="shortdash"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="AUTOSTART-KUBOTA" prefix="DPS">
<gates>
<gate name="G$1" symbol="AUTOSTART-KUBOTA-KONEKTORY" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name="">
<attribute name="_EXTERNAL_" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PRUCHODKA" prefix="PRUCHODKA">
<gates>
<gate name="G$1" symbol="PRUCHODKA" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SVORKA_PULENA" prefix="SVORKA" uservalue="yes">
<gates>
<gate name="G$1" symbol="SVORKA_PULENA_A" x="0" y="0" addlevel="always"/>
<gate name="G$2" symbol="SVORKA_PULENA_B" x="0" y="0" addlevel="always"/>
</gates>
<devices>
<device name="" package="1X02">
<connects>
<connect gate="G$1" pin="A" pad="L"/>
<connect gate="G$2" pin="B" pad="H"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="HORN_DUMMY">
<gates>
<gate name="G$1" symbol="HORN" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name="">
<attribute name="_EXTERNAL_" value=""/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="OTOCNY_PREPINAC" prefix="S">
<gates>
<gate name="A" symbol="OTOCNY_PREPINAC" x="0" y="0"/>
<gate name="G$1" symbol="OTOCNY_PREPINAC_STITEK" x="0" y="17.78"/>
<gate name="B" symbol="OTOCNY_PREPINAC_SEKCE" x="0" y="-17.78"/>
<gate name="C" symbol="OTOCNY_PREPINAC_SEKCE" x="0" y="-35.56"/>
<gate name="D" symbol="OTOCNY_PREPINAC_SEKCE" x="0" y="-53.34"/>
<gate name="E" symbol="OTOCNY_PREPINAC_SEKCE" x="0" y="-71.12"/>
</gates>
<devices>
<device name="" package="OTOCNY_PREPINAC">
<connects>
<connect gate="A" pin="0" pad="1"/>
<connect gate="A" pin="I" pad="2"/>
<connect gate="A" pin="II" pad="4"/>
<connect gate="A" pin="III" pad="6"/>
<connect gate="A" pin="IV" pad="8"/>
<connect gate="B" pin="0" pad="9"/>
<connect gate="B" pin="I" pad="10"/>
<connect gate="B" pin="II" pad="12"/>
<connect gate="B" pin="III" pad="14"/>
<connect gate="B" pin="IV" pad="16"/>
<connect gate="C" pin="0" pad="17"/>
<connect gate="C" pin="I" pad="18"/>
<connect gate="C" pin="II" pad="20"/>
<connect gate="C" pin="III" pad="22"/>
<connect gate="C" pin="IV" pad="24"/>
<connect gate="D" pin="0" pad="25"/>
<connect gate="D" pin="I" pad="26"/>
<connect gate="D" pin="II" pad="28"/>
<connect gate="D" pin="III" pad="30"/>
<connect gate="D" pin="IV" pad="32"/>
<connect gate="E" pin="0" pad="33"/>
<connect gate="E" pin="I" pad="34"/>
<connect gate="E" pin="II" pad="36"/>
<connect gate="E" pin="III" pad="38"/>
<connect gate="E" pin="IV" pad="40"/>
</connects>
<technologies>
<technology name="">
<attribute name="ABRA" value="MELE0738"/>
<attribute name="TYP" value="OBZOR VSR 10 2454 A8"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DESKA_RELE" prefix="DPS">
<gates>
<gate name="G$1" symbol="DESKA-RELE" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name="">
<attribute name="ABRA" value="" constant="no"/>
<attribute name="_EXTERNAL_" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="OTOCNY_PREPINAC_2" prefix="S">
<gates>
<gate name="G$1" symbol="OTOCNY_PREPINAC_STITEK_2" x="0" y="12.7"/>
<gate name="A" symbol="OTOCNY_PREPINAC_2" x="0" y="0"/>
</gates>
<devices>
<device name="" package="OTOCNY_PREPINAC">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="3"/>
<connect gate="A" pin="I" pad="2"/>
<connect gate="A" pin="II" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="ABRA" value="MELE0740"/>
<attribute name="TYP" value="OBZOR VSR 10 2251 A8"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="OTOCNY_PREPINAC_3" prefix="S">
<gates>
<gate name="A" symbol="OTOCNY_PREPINAC_3" x="0" y="0"/>
<gate name="G$2" symbol="OTOCNY_PREPINAC_STITEK_3" x="0" y="15.24"/>
</gates>
<devices>
<device name="" package="OTOCNY_PREPINAC">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="2" pad="3"/>
<connect gate="A" pin="3" pad="7"/>
<connect gate="A" pin="I" pad="2"/>
<connect gate="A" pin="II" pad="4"/>
<connect gate="A" pin="III" pad="8"/>
</connects>
<technologies>
<technology name="">
<attribute name="ABRA" value="MELE0739"/>
<attribute name="TYP" value="OBZOR VSR 10 2351 A8"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="OTOCNY_PREPINAC_H2" prefix="S">
<gates>
<gate name="G$1" symbol="OTOCNY_PREPINAC_STITEK_H2" x="0" y="12.7"/>
<gate name="A" symbol="OTOCNY_PREPINAC_H2" x="0" y="-2.54"/>
<gate name="B" symbol="OTOCNY_PREPINAC_SEKCE_H2" x="0" y="-15.24"/>
<gate name="C" symbol="OTOCNY_PREPINAC_SEKCE_H2" x="0" y="-27.94"/>
<gate name="D" symbol="OTOCNY_PREPINAC_SEKCE_H2" x="0" y="-40.64"/>
<gate name="E" symbol="OTOCNY_PREPINAC_SEKCE_H2" x="0" y="-53.34"/>
<gate name="F" symbol="OTOCNY_PREPINAC_SEKCE_H2" x="0" y="-66.04"/>
</gates>
<devices>
<device name="" package="OTOCNY_PREPINAC">
<connects>
<connect gate="A" pin="0" pad="1"/>
<connect gate="A" pin="I" pad="2"/>
<connect gate="A" pin="II" pad="4"/>
<connect gate="B" pin="0" pad="5"/>
<connect gate="B" pin="I" pad="6"/>
<connect gate="B" pin="II" pad="8"/>
<connect gate="C" pin="0" pad="9"/>
<connect gate="C" pin="I" pad="10"/>
<connect gate="C" pin="II" pad="12"/>
<connect gate="D" pin="0" pad="13"/>
<connect gate="D" pin="I" pad="14"/>
<connect gate="D" pin="II" pad="16"/>
<connect gate="E" pin="0" pad="17"/>
<connect gate="E" pin="I" pad="18"/>
<connect gate="E" pin="II" pad="20"/>
<connect gate="F" pin="0" pad="21"/>
<connect gate="F" pin="I" pad="22"/>
<connect gate="F" pin="II" pad="24"/>
</connects>
<technologies>
<technology name="">
<attribute name="ABRA" value="MELE1200"/>
<attribute name="TYP" value="OBZOR VSR 16 2256 A8"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="KUBOTA">
<packages>
<package name="EMPTY">
<pad name="1" x="0" y="0" drill="0.8" shape="square"/>
<pad name="2" x="0" y="-1.27" drill="0.8" shape="square"/>
<pad name="3" x="0" y="-2.54" drill="0.8" shape="square"/>
<pad name="4" x="0" y="-3.81" drill="0.8" shape="square"/>
<pad name="5" x="0" y="-5.08" drill="0.8" shape="square"/>
<pad name="6" x="0" y="-6.35" drill="0.8" shape="square"/>
<pad name="7" x="0" y="-7.62" drill="0.8" shape="square"/>
<pad name="8" x="0" y="-8.89" drill="0.8" shape="square"/>
<pad name="9" x="0" y="-10.16" drill="0.8" shape="square"/>
<pad name="10" x="0" y="-11.43" drill="0.8" shape="square"/>
<pad name="11" x="0" y="-12.7" drill="0.8" shape="square"/>
<pad name="12" x="0" y="-13.97" drill="0.8" shape="square"/>
<pad name="13" x="0" y="-15.24" drill="0.8" shape="square"/>
<pad name="14" x="0" y="-16.51" drill="0.8" shape="square"/>
<pad name="15" x="0" y="-17.78" drill="0.8" shape="square"/>
<pad name="16" x="0" y="-19.05" drill="0.8" shape="square"/>
<pad name="17" x="0" y="-20.32" drill="0.8" shape="square"/>
<pad name="18" x="0" y="-21.59" drill="0.8" shape="square"/>
<pad name="19" x="0" y="-22.86" drill="0.8" shape="square"/>
<pad name="20" x="0" y="-24.13" drill="0.8" shape="square"/>
<pad name="21" x="0" y="-25.4" drill="0.8" shape="square"/>
<pad name="22" x="0" y="-26.67" drill="0.8" shape="square"/>
<pad name="23" x="0" y="-27.94" drill="0.8" shape="square"/>
<pad name="24" x="0" y="-29.21" drill="0.8" shape="square"/>
<pad name="25" x="0" y="-30.48" drill="0.8" shape="square"/>
<pad name="26" x="0" y="-31.75" drill="0.8" shape="square"/>
<pad name="27" x="0" y="-33.02" drill="0.8" shape="square"/>
<pad name="28" x="0" y="-34.29" drill="0.8" shape="square"/>
<pad name="29" x="0" y="-35.56" drill="0.8" shape="square"/>
<pad name="30" x="0" y="-36.83" drill="0.8" shape="square"/>
<pad name="31" x="0" y="-38.1" drill="0.8" shape="square"/>
</package>
</packages>
<symbols>
<symbol name="MCFLEX">
<text x="-22.86" y="-23.495" size="1.27" layer="94" distance="100">    Outputs:
All outputs are short circuit-proof 
OUT(02): Gen. excitation D+, 0.5A, set as 
OUT(06): 7.5A (max. 1s) / 6.0A, set as
OUT(08): 40A (max. 1s) / 20A set as
OUT(10): 70A (max. 1s) / 35A, set as
OUT(11): 40A (max. 1s) / 20A, set as
OUT(12): 70A (max. 1s) / 35A, set as
OUT(15): 3.5A (max. 1s) / 3.0A, set as
OUT(18): 40A (max. 1s) / 20A, set as</text>
<text x="-22.86" y="4.445" size="1.27" layer="94" distance="100">    Inputs:
IN(01): Oil pressure, set as
IN(03): Temperature, set as
IN(04): Various, set as 
IN(07): Auto start, set as 
IN(09): External stop, set as 
IN(13): Speed monitoring:</text>
<pin name="18" x="43.18" y="-22.86" visible="pad" length="middle" direction="out" rot="R180"/>
<pin name="15" x="43.18" y="-20.32" visible="pad" length="middle" direction="out" rot="R180"/>
<pin name="12" x="43.18" y="-17.78" visible="pad" length="middle" direction="out" rot="R180"/>
<pin name="11" x="43.18" y="-15.24" visible="pad" length="middle" direction="out" rot="R180"/>
<pin name="10" x="43.18" y="-12.7" visible="pad" length="middle" direction="out" rot="R180"/>
<pin name="8" x="43.18" y="-10.16" visible="pad" length="middle" direction="out" rot="R180"/>
<pin name="6" x="43.18" y="-7.62" visible="pad" length="middle" direction="out" rot="R180"/>
<pin name="2" x="43.18" y="-5.08" visible="pad" length="middle" direction="out" rot="R180"/>
<pin name="SPEED" x="-43.18" y="5.08" length="middle" direction="in"/>
<pin name="STOP" x="-43.18" y="7.62" length="middle" direction="in"/>
<pin name="AUTOSTART" x="-43.18" y="10.16" length="middle" direction="in"/>
<pin name="DIVERSE" x="-43.18" y="12.7" length="middle" direction="in"/>
<pin name="TEMP" x="-43.18" y="15.24" length="middle" direction="in"/>
<pin name="OIL" x="-43.18" y="17.78" length="middle" direction="in"/>
<pin name="KL30@14" x="-43.18" y="25.4" length="middle" direction="in"/>
<pin name="KL30@16" x="-43.18" y="22.86" length="middle" direction="in"/>
<pin name="KL31" x="-43.18" y="-25.4" length="middle" direction="in"/>
<pin name="CANL" x="43.18" y="22.86" length="middle" direction="in" rot="R180"/>
<pin name="CANH" x="43.18" y="25.4" length="middle" direction="in" rot="R180"/>
<wire x1="-38.1" y1="27.94" x2="38.1" y2="27.94" width="0.4064" layer="94"/>
<wire x1="38.1" y1="27.94" x2="38.1" y2="-27.94" width="0.4064" layer="94"/>
<wire x1="38.1" y1="-27.94" x2="-38.1" y2="-27.94" width="0.4064" layer="94"/>
<wire x1="-38.1" y1="-27.94" x2="-38.1" y2="27.94" width="0.4064" layer="94"/>
<text x="0" y="25.4" size="2.54" layer="94" align="center">MCflex</text>
<text x="0" y="21.59" size="1.778" layer="94" align="center">ehb electronics</text>
<wire x1="35.67" y1="2.93" x2="35.67" y2="20.43" width="0.1" layer="94"/>
<wire x1="35.67" y1="20.43" x2="35.17" y2="20.93" width="0.1" layer="94" curve="90"/>
<wire x1="35.17" y1="20.93" x2="18.17" y2="20.93" width="0.1" layer="94"/>
<wire x1="18.17" y1="20.93" x2="17.67" y2="20.43" width="0.1" layer="94" curve="90"/>
<wire x1="17.67" y1="20.43" x2="17.67" y2="2.93" width="0.1" layer="94"/>
<wire x1="17.67" y1="2.93" x2="18.17" y2="2.43" width="0.1" layer="94" curve="90"/>
<wire x1="18.17" y1="2.43" x2="35.17" y2="2.43" width="0.1" layer="94"/>
<wire x1="35.17" y1="2.43" x2="35.67" y2="2.93" width="0.1" layer="94" curve="90"/>
<wire x1="19.17" y1="19.23" x2="19.37" y2="19.43" width="0.1" layer="94" curve="-90"/>
<wire x1="19.37" y1="19.43" x2="33.97" y2="19.43" width="0.1" layer="94"/>
<wire x1="33.97" y1="19.43" x2="34.17" y2="19.23" width="0.1" layer="94" curve="-90"/>
<wire x1="34.17" y1="19.23" x2="34.17" y2="16.93" width="0.1" layer="94"/>
<wire x1="34.17" y1="16.93" x2="34.17" y2="13.43" width="0.1" layer="94"/>
<wire x1="34.17" y1="13.43" x2="34.17" y2="4.13" width="0.1" layer="94"/>
<wire x1="34.17" y1="4.13" x2="33.97" y2="3.93" width="0.1" layer="94" curve="-90"/>
<wire x1="33.97" y1="3.93" x2="19.37" y2="3.93" width="0.1" layer="94"/>
<wire x1="19.37" y1="3.93" x2="19.17" y2="4.13" width="0.1" layer="94" curve="-90"/>
<wire x1="19.17" y1="4.13" x2="19.17" y2="13.43" width="0.1" layer="94"/>
<wire x1="19.17" y1="13.43" x2="19.17" y2="16.93" width="0.1" layer="94"/>
<wire x1="19.17" y1="16.93" x2="19.17" y2="19.23" width="0.1" layer="94"/>
<wire x1="19.17" y1="16.93" x2="20.67" y2="16.93" width="0.1" layer="94"/>
<circle x="30.17" y="8.43" radius="3.3" width="0.1" layer="94"/>
<wire x1="20.67" y1="16.93" x2="32.67" y2="16.93" width="0.1" layer="94"/>
<wire x1="32.67" y1="16.93" x2="34.17" y2="16.93" width="0.1" layer="94"/>
<wire x1="34.17" y1="13.43" x2="32.67" y2="13.43" width="0.1" layer="94"/>
<wire x1="32.67" y1="13.43" x2="20.67" y2="13.43" width="0.1" layer="94"/>
<wire x1="20.67" y1="13.43" x2="19.17" y2="13.43" width="0.1" layer="94"/>
<wire x1="20.67" y1="13.43" x2="20.67" y2="16.93" width="0.1" layer="94"/>
<wire x1="32.67" y1="13.43" x2="32.67" y2="16.93" width="0.1" layer="94"/>
<wire x1="20.87" y1="5.53" x2="20.87" y2="6.53" width="0.1" layer="94"/>
<wire x1="20.87" y1="6.53" x2="21.17" y2="6.83" width="0.1" layer="94"/>
<wire x1="21.17" y1="6.83" x2="21.17" y2="6.13" width="0.1" layer="94"/>
<wire x1="21.17" y1="6.13" x2="21.27" y2="6.13" width="0.1" layer="94"/>
<wire x1="21.27" y1="6.13" x2="21.97" y2="6.83" width="0.1" layer="94"/>
<wire x1="21.97" y1="6.83" x2="22.17" y2="6.63" width="0.1" layer="94"/>
<wire x1="22.17" y1="6.63" x2="21.47" y2="5.93" width="0.1" layer="94"/>
<wire x1="21.47" y1="5.93" x2="21.47" y2="5.83" width="0.1" layer="94"/>
<wire x1="21.47" y1="5.83" x2="22.17" y2="5.83" width="0.1" layer="94"/>
<wire x1="22.17" y1="5.83" x2="21.87" y2="5.53" width="0.1" layer="94"/>
<wire x1="21.87" y1="5.53" x2="20.87" y2="5.53" width="0.1" layer="94"/>
<wire x1="22.27" y1="11.33" x2="22.27" y2="10.33" width="0.1" layer="94"/>
<wire x1="22.27" y1="10.33" x2="21.97" y2="10.03" width="0.1" layer="94"/>
<wire x1="21.97" y1="10.03" x2="21.97" y2="10.73" width="0.1" layer="94"/>
<wire x1="21.97" y1="10.73" x2="21.87" y2="10.73" width="0.1" layer="94"/>
<wire x1="21.87" y1="10.73" x2="21.17" y2="10.03" width="0.1" layer="94"/>
<wire x1="21.17" y1="10.03" x2="20.97" y2="10.23" width="0.1" layer="94"/>
<wire x1="20.97" y1="10.23" x2="21.67" y2="10.93" width="0.1" layer="94"/>
<wire x1="21.67" y1="10.93" x2="21.67" y2="11.03" width="0.1" layer="94"/>
<wire x1="21.67" y1="11.03" x2="20.97" y2="11.03" width="0.1" layer="94"/>
<wire x1="20.97" y1="11.03" x2="21.27" y2="11.33" width="0.1" layer="94"/>
<wire x1="21.27" y1="11.33" x2="22.27" y2="11.33" width="0.1" layer="94"/>
<circle x="21.47" y="18.23" radius="0.4" width="0" layer="94"/>
<text x="23.87" y="5.63" size="1" layer="94">Set</text>
<wire x1="29.67" y1="10.43" x2="30.67" y2="10.43" width="0.1" layer="94"/>
<wire x1="30.67" y1="10.43" x2="30.67" y2="6.43" width="0.1" layer="94"/>
<wire x1="30.67" y1="6.43" x2="29.67" y2="6.43" width="0.1" layer="94"/>
<wire x1="29.67" y1="6.43" x2="29.67" y2="10.43" width="0.1" layer="94"/>
<text x="26.67" y="15.18" size="1.778" layer="94" align="center">MCflex</text>
<text x="-38.1" y="30.48" size="1.778" layer="95" align="top-left">&gt;NAME</text>
<text x="38.1" y="30.48" size="1.778" layer="96" align="top-right">&gt;VALUE</text>
<text x="17.526" y="-2.54" size="1.27" layer="97">Karta ABRA:</text>
<text x="35.814" y="-2.54" size="1.27" layer="97" align="bottom-right">&gt;ABRA</text>
<text x="17.526" y="0" size="1.27" layer="97">Kód výrobce:</text>
<text x="35.814" y="0" size="1.27" layer="97" align="bottom-right">&gt;MPN</text>
<wire x1="-29.21" y1="-26.67" x2="-27.94" y2="-26.67" width="0.254" layer="94"/>
<wire x1="-27.94" y1="-26.67" x2="-26.67" y2="-26.67" width="0.254" layer="94"/>
<wire x1="-27.94" y1="-26.67" x2="-27.94" y2="-24.13" width="0.254" layer="94"/>
<wire x1="-29.21" y1="24.13" x2="-26.67" y2="24.13" width="0.254" layer="94"/>
<wire x1="-27.94" y1="25.4" x2="-27.94" y2="22.86" width="0.254" layer="94"/>
<text x="7.62" y="17.145" size="1.27" layer="97" distance="100">&gt;IN_01</text>
<text x="7.62" y="17.145" size="1.27" layer="97" distance="100" align="bottom-right">............... </text>
<text x="-1.27" y="4.445" size="1.27" layer="97" distance="100">&gt;IN_13</text>
<text x="7.62" y="14.605" size="1.27" layer="97" distance="100">&gt;IN_03</text>
<text x="7.62" y="14.605" size="1.27" layer="97" distance="100" align="bottom-right">.............. </text>
<text x="7.62" y="12.065" size="1.27" layer="97" distance="100">&gt;IN_04</text>
<text x="7.62" y="12.065" size="1.27" layer="97" distance="100" align="bottom-right">..................... </text>
<text x="7.62" y="9.525" size="1.27" layer="97" distance="100">&gt;IN_07</text>
<text x="7.62" y="9.525" size="1.27" layer="97" distance="100" align="bottom-right">.................. </text>
<text x="7.62" y="6.985" size="1.27" layer="97" distance="100">&gt;IN_09</text>
<text x="7.62" y="6.985" size="1.27" layer="97" distance="100" align="bottom-right">............. </text>
<text x="25.4" y="-5.08" size="1.524" layer="97" distance="100" align="center-left">&gt;OUT_02</text>
<text x="25.4" y="-7.62" size="1.524" layer="97" distance="100" align="center-left">&gt;OUT_06</text>
<text x="25.4" y="-10.16" size="1.524" layer="97" distance="100" align="center-left">&gt;OUT_08</text>
<text x="25.4" y="-12.7" size="1.524" layer="97" distance="100" align="center-left">&gt;OUT_10</text>
<text x="25.4" y="-15.24" size="1.524" layer="97" distance="100" align="center-left">&gt;OUT_11</text>
<text x="25.4" y="-17.78" size="1.524" layer="97" distance="100" align="center-left">&gt;OUT_12</text>
<text x="25.4" y="-20.32" size="1.524" layer="97" distance="100" align="center-left">&gt;OUT_15</text>
<text x="25.4" y="-22.86" size="1.524" layer="97" distance="100" align="center-left">&gt;OUT_18</text>
<text x="25.4" y="-5.08" size="1.27" layer="97" distance="100" align="center-right">.......................... </text>
<text x="25.4" y="-7.62" size="1.27" layer="97" distance="100" align="center-right">............................... </text>
<text x="25.4" y="-10.16" size="1.27" layer="97" distance="100" align="center-right">.................................. </text>
<text x="25.4" y="-12.7" size="1.27" layer="97" distance="100" align="center-right">................................. </text>
<text x="25.4" y="-20.32" size="1.27" layer="97" distance="100" align="center-right">............................... </text>
<text x="25.4" y="-15.24" size="1.27" layer="97" distance="100" align="center-right">................................. </text>
<text x="25.4" y="-17.78" size="1.27" layer="97" distance="100" align="center-right">................................. </text>
<text x="25.4" y="-22.86" size="1.27" layer="97" distance="100" align="center-right">................................. </text>
</symbol>
<symbol name="KUBOTA-MOTOR">
<wire x1="0" y1="0" x2="0" y2="2.54" width="0.4064" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="101.6" width="0.4064" layer="94"/>
<wire x1="7.62" y1="44.45" x2="7.62" y2="12.7" width="0.1524" layer="94"/>
<wire x1="7.62" y1="12.7" x2="27.94" y2="12.7" width="0.1524" layer="94"/>
<wire x1="27.94" y1="12.7" x2="27.94" y2="44.45" width="0.1524" layer="94"/>
<wire x1="27.94" y1="44.45" x2="7.62" y2="44.45" width="0.1524" layer="94"/>
<wire x1="9.525" y1="36.195" x2="12.065" y2="36.195" width="0.1524" layer="94"/>
<wire x1="11.43" y1="35.56" x2="10.16" y2="35.56" width="0.1524" layer="94"/>
<wire x1="8.89" y1="36.83" x2="10.795" y2="36.83" width="0.1524" layer="94"/>
<wire x1="10.795" y1="36.83" x2="12.7" y2="36.83" width="0.1524" layer="94"/>
<wire x1="10.795" y1="36.83" x2="10.795" y2="38.735" width="0.1524" layer="94"/>
<wire x1="10.795" y1="38.735" x2="14.605" y2="38.735" width="0.1524" layer="94"/>
<wire x1="14.605" y1="38.735" x2="14.605" y2="41.275" width="0.1524" layer="94"/>
<wire x1="14.605" y1="41.275" x2="15.875" y2="40.005" width="0.1524" layer="94"/>
<wire x1="15.875" y1="40.005" x2="17.145" y2="41.275" width="0.1524" layer="94"/>
<wire x1="17.145" y1="41.275" x2="17.78" y2="41.275" width="0.1524" layer="94"/>
<wire x1="17.78" y1="41.275" x2="17.78" y2="36.83" width="0.1524" layer="94"/>
<wire x1="17.78" y1="36.83" x2="22.225" y2="36.83" width="0.1524" layer="94"/>
<wire x1="9.525" y1="30.48" x2="12.065" y2="30.48" width="0.1524" layer="94"/>
<wire x1="11.43" y1="29.845" x2="10.16" y2="29.845" width="0.1524" layer="94"/>
<wire x1="8.89" y1="31.115" x2="10.795" y2="31.115" width="0.1524" layer="94"/>
<wire x1="10.795" y1="31.115" x2="12.7" y2="31.115" width="0.1524" layer="94"/>
<wire x1="10.795" y1="31.115" x2="10.795" y2="33.02" width="0.1524" layer="94"/>
<wire x1="10.795" y1="33.02" x2="14.605" y2="33.02" width="0.1524" layer="94"/>
<wire x1="14.605" y1="33.02" x2="14.605" y2="35.56" width="0.1524" layer="94"/>
<wire x1="14.605" y1="35.56" x2="15.875" y2="34.29" width="0.1524" layer="94"/>
<wire x1="15.875" y1="34.29" x2="17.145" y2="35.56" width="0.1524" layer="94"/>
<wire x1="17.145" y1="35.56" x2="17.78" y2="35.56" width="0.1524" layer="94"/>
<wire x1="17.78" y1="35.56" x2="17.78" y2="31.115" width="0.1524" layer="94"/>
<wire x1="17.78" y1="31.115" x2="22.225" y2="31.115" width="0.1524" layer="94"/>
<wire x1="9.525" y1="24.765" x2="12.065" y2="24.765" width="0.1524" layer="94"/>
<wire x1="11.43" y1="24.13" x2="10.16" y2="24.13" width="0.1524" layer="94"/>
<wire x1="8.89" y1="25.4" x2="10.795" y2="25.4" width="0.1524" layer="94"/>
<wire x1="10.795" y1="25.4" x2="12.7" y2="25.4" width="0.1524" layer="94"/>
<wire x1="10.795" y1="25.4" x2="10.795" y2="27.305" width="0.1524" layer="94"/>
<wire x1="10.795" y1="27.305" x2="14.605" y2="27.305" width="0.1524" layer="94"/>
<wire x1="14.605" y1="27.305" x2="14.605" y2="29.845" width="0.1524" layer="94"/>
<wire x1="14.605" y1="29.845" x2="15.875" y2="28.575" width="0.1524" layer="94"/>
<wire x1="15.875" y1="28.575" x2="17.145" y2="29.845" width="0.1524" layer="94"/>
<wire x1="17.145" y1="29.845" x2="17.78" y2="29.845" width="0.1524" layer="94"/>
<wire x1="17.78" y1="29.845" x2="17.78" y2="25.4" width="0.1524" layer="94"/>
<wire x1="17.78" y1="25.4" x2="22.225" y2="25.4" width="0.1524" layer="94"/>
<wire x1="9.525" y1="19.05" x2="12.065" y2="19.05" width="0.1524" layer="94"/>
<wire x1="11.43" y1="18.415" x2="10.16" y2="18.415" width="0.1524" layer="94"/>
<wire x1="8.89" y1="19.685" x2="10.795" y2="19.685" width="0.1524" layer="94"/>
<wire x1="10.795" y1="19.685" x2="12.7" y2="19.685" width="0.1524" layer="94"/>
<wire x1="10.795" y1="19.685" x2="10.795" y2="21.59" width="0.1524" layer="94"/>
<wire x1="10.795" y1="21.59" x2="14.605" y2="21.59" width="0.1524" layer="94"/>
<wire x1="14.605" y1="21.59" x2="14.605" y2="24.13" width="0.1524" layer="94"/>
<wire x1="14.605" y1="24.13" x2="15.875" y2="22.86" width="0.1524" layer="94"/>
<wire x1="15.875" y1="22.86" x2="17.145" y2="24.13" width="0.1524" layer="94"/>
<wire x1="17.145" y1="24.13" x2="17.78" y2="24.13" width="0.1524" layer="94"/>
<wire x1="17.78" y1="24.13" x2="17.78" y2="19.685" width="0.1524" layer="94"/>
<wire x1="17.78" y1="19.685" x2="22.225" y2="19.685" width="0.1524" layer="94"/>
<wire x1="22.86" y1="36.195" x2="22.86" y2="31.75" width="0.1524" layer="94"/>
<wire x1="22.225" y1="36.195" x2="22.225" y2="19.05" width="0.1524" layer="94"/>
<wire x1="23.495" y1="36.195" x2="23.495" y2="19.05" width="0.1524" layer="94"/>
<wire x1="22.86" y1="30.48" x2="22.86" y2="26.035" width="0.1524" layer="94"/>
<wire x1="22.86" y1="24.765" x2="22.86" y2="20.32" width="0.1524" layer="94"/>
<wire x1="71.755" y1="72.39" x2="71.755" y2="70.485" width="0.1524" layer="94"/>
<wire x1="71.755" y1="70.485" x2="71.755" y2="52.705" width="0.1524" layer="94"/>
<wire x1="71.755" y1="52.705" x2="71.755" y2="50.8" width="0.1524" layer="94"/>
<wire x1="71.755" y1="50.8" x2="73.66" y2="50.8" width="0.1524" layer="94"/>
<wire x1="73.66" y1="50.8" x2="73.66" y2="52.07" width="0.1524" layer="94"/>
<wire x1="73.66" y1="52.07" x2="73.66" y2="71.12" width="0.1524" layer="94"/>
<wire x1="73.66" y1="71.12" x2="73.66" y2="72.39" width="0.1524" layer="94"/>
<wire x1="73.66" y1="72.39" x2="71.755" y2="72.39" width="0.1524" layer="94"/>
<wire x1="71.755" y1="70.485" x2="60.96" y2="68.58" width="0.1524" layer="94"/>
<wire x1="60.96" y1="55.88" x2="60.96" y2="68.58" width="0.1524" layer="94"/>
<wire x1="60.96" y1="55.88" x2="71.755" y2="52.705" width="0.1524" layer="94"/>
<wire x1="73.66" y1="71.12" x2="79.375" y2="71.12" width="0.1524" layer="94"/>
<wire x1="79.375" y1="71.12" x2="86.995" y2="71.12" width="0.1524" layer="94"/>
<wire x1="86.995" y1="71.12" x2="93.98" y2="71.12" width="0.1524" layer="94"/>
<wire x1="93.98" y1="71.12" x2="99.695" y2="71.12" width="0.1524" layer="94"/>
<wire x1="99.695" y1="71.12" x2="99.695" y2="52.07" width="0.1524" layer="94"/>
<wire x1="99.695" y1="52.07" x2="73.66" y2="52.07" width="0.1524" layer="94"/>
<wire x1="79.375" y1="71.12" x2="79.375" y2="76.835" width="0.1524" layer="94"/>
<wire x1="79.375" y1="76.835" x2="86.995" y2="76.835" width="0.1524" layer="94"/>
<wire x1="86.995" y1="76.835" x2="86.995" y2="71.12" width="0.1524" layer="94"/>
<wire x1="86.995" y1="76.835" x2="93.98" y2="76.835" width="0.1524" layer="94"/>
<wire x1="93.98" y1="76.835" x2="93.98" y2="71.12" width="0.1524" layer="94"/>
<wire x1="125.73" y1="53.34" x2="132.08" y2="53.34" width="0.1524" layer="94"/>
<wire x1="132.08" y1="53.34" x2="133.985" y2="68.58" width="0.1524" layer="94"/>
<wire x1="133.985" y1="68.58" x2="131.445" y2="68.58" width="0.1524" layer="94"/>
<wire x1="131.445" y1="68.58" x2="126.365" y2="68.58" width="0.1524" layer="94"/>
<wire x1="126.365" y1="68.58" x2="123.825" y2="68.58" width="0.1524" layer="94"/>
<wire x1="123.825" y1="68.58" x2="125.73" y2="53.34" width="0.1524" layer="94"/>
<wire x1="123.825" y1="68.58" x2="122.555" y2="68.58" width="0.1524" layer="94"/>
<wire x1="122.555" y1="68.58" x2="121.285" y2="68.58" width="0.1524" layer="94"/>
<wire x1="121.285" y1="68.58" x2="121.285" y2="71.12" width="0.1524" layer="94"/>
<wire x1="121.285" y1="71.12" x2="122.555" y2="71.12" width="0.1524" layer="94"/>
<wire x1="122.555" y1="71.12" x2="123.825" y2="71.12" width="0.1524" layer="94"/>
<wire x1="123.825" y1="71.12" x2="126.365" y2="71.12" width="0.1524" layer="94"/>
<wire x1="126.365" y1="71.12" x2="131.445" y2="71.12" width="0.1524" layer="94"/>
<wire x1="131.445" y1="71.12" x2="133.985" y2="71.12" width="0.1524" layer="94"/>
<wire x1="133.985" y1="71.12" x2="135.255" y2="71.12" width="0.1524" layer="94"/>
<wire x1="135.255" y1="71.12" x2="136.525" y2="71.12" width="0.1524" layer="94"/>
<wire x1="136.525" y1="71.12" x2="136.525" y2="68.58" width="0.1524" layer="94"/>
<wire x1="136.525" y1="68.58" x2="135.255" y2="68.58" width="0.1524" layer="94"/>
<wire x1="135.255" y1="68.58" x2="133.985" y2="68.58" width="0.1524" layer="94"/>
<wire x1="122.555" y1="71.12" x2="122.555" y2="68.58" width="0.1524" layer="94"/>
<wire x1="135.255" y1="68.58" x2="135.255" y2="71.12" width="0.1524" layer="94"/>
<wire x1="131.445" y1="71.12" x2="131.445" y2="68.58" width="0.1524" layer="94"/>
<wire x1="126.365" y1="71.12" x2="126.365" y2="68.58" width="0.1524" layer="94"/>
<wire x1="123.825" y1="71.12" x2="125.73" y2="73.025" width="0.1524" layer="94"/>
<wire x1="125.73" y1="73.025" x2="132.08" y2="73.025" width="0.1524" layer="94"/>
<wire x1="132.08" y1="73.025" x2="133.985" y2="71.12" width="0.1524" layer="94"/>
<wire x1="125.73" y1="73.025" x2="125.73" y2="77.47" width="0.1524" layer="94"/>
<wire x1="125.73" y1="77.47" x2="128.905" y2="77.47" width="0.1524" layer="94"/>
<wire x1="128.905" y1="77.47" x2="132.08" y2="77.47" width="0.1524" layer="94"/>
<wire x1="132.08" y1="77.47" x2="132.08" y2="73.025" width="0.1524" layer="94"/>
<wire x1="128.27" y1="65.405" x2="127" y2="59.055" width="0.1524" layer="94"/>
<wire x1="128.905" y1="58.42" x2="128.905" y2="50.8" width="0.1524" layer="94"/>
<wire x1="127" y1="50.8" x2="130.81" y2="50.8" width="0.1524" layer="94"/>
<wire x1="130.175" y1="50.165" x2="127.635" y2="50.165" width="0.1524" layer="94"/>
<wire x1="129.54" y1="49.53" x2="128.27" y2="49.53" width="0.1524" layer="94"/>
<wire x1="128.905" y1="66.04" x2="128.905" y2="77.47" width="0.1524" layer="94"/>
<wire x1="108.585" y1="53.34" x2="114.935" y2="53.34" width="0.1524" layer="94"/>
<wire x1="114.935" y1="53.34" x2="116.84" y2="68.58" width="0.1524" layer="94"/>
<wire x1="116.84" y1="68.58" x2="114.3" y2="68.58" width="0.1524" layer="94"/>
<wire x1="114.3" y1="68.58" x2="109.22" y2="68.58" width="0.1524" layer="94"/>
<wire x1="109.22" y1="68.58" x2="106.68" y2="68.58" width="0.1524" layer="94"/>
<wire x1="106.68" y1="68.58" x2="108.585" y2="53.34" width="0.1524" layer="94"/>
<wire x1="106.68" y1="68.58" x2="105.41" y2="68.58" width="0.1524" layer="94"/>
<wire x1="105.41" y1="68.58" x2="104.14" y2="68.58" width="0.1524" layer="94"/>
<wire x1="104.14" y1="68.58" x2="104.14" y2="71.12" width="0.1524" layer="94"/>
<wire x1="104.14" y1="71.12" x2="105.41" y2="71.12" width="0.1524" layer="94"/>
<wire x1="105.41" y1="71.12" x2="106.68" y2="71.12" width="0.1524" layer="94"/>
<wire x1="106.68" y1="71.12" x2="109.22" y2="71.12" width="0.1524" layer="94"/>
<wire x1="109.22" y1="71.12" x2="114.3" y2="71.12" width="0.1524" layer="94"/>
<wire x1="114.3" y1="71.12" x2="116.84" y2="71.12" width="0.1524" layer="94"/>
<wire x1="116.84" y1="71.12" x2="118.11" y2="71.12" width="0.1524" layer="94"/>
<wire x1="118.11" y1="71.12" x2="119.38" y2="71.12" width="0.1524" layer="94"/>
<wire x1="119.38" y1="71.12" x2="119.38" y2="68.58" width="0.1524" layer="94"/>
<wire x1="119.38" y1="68.58" x2="118.11" y2="68.58" width="0.1524" layer="94"/>
<wire x1="118.11" y1="68.58" x2="116.84" y2="68.58" width="0.1524" layer="94"/>
<wire x1="105.41" y1="71.12" x2="105.41" y2="68.58" width="0.1524" layer="94"/>
<wire x1="118.11" y1="68.58" x2="118.11" y2="71.12" width="0.1524" layer="94"/>
<wire x1="114.3" y1="71.12" x2="114.3" y2="68.58" width="0.1524" layer="94"/>
<wire x1="109.22" y1="71.12" x2="109.22" y2="68.58" width="0.1524" layer="94"/>
<wire x1="106.68" y1="71.12" x2="108.585" y2="73.025" width="0.1524" layer="94"/>
<wire x1="108.585" y1="73.025" x2="114.935" y2="73.025" width="0.1524" layer="94"/>
<wire x1="114.935" y1="73.025" x2="116.84" y2="71.12" width="0.1524" layer="94"/>
<wire x1="108.585" y1="73.025" x2="108.585" y2="77.47" width="0.1524" layer="94"/>
<wire x1="108.585" y1="77.47" x2="111.76" y2="77.47" width="0.1524" layer="94"/>
<wire x1="111.76" y1="77.47" x2="114.935" y2="77.47" width="0.1524" layer="94"/>
<wire x1="114.935" y1="77.47" x2="114.935" y2="73.025" width="0.1524" layer="94"/>
<wire x1="111.125" y1="65.405" x2="109.855" y2="59.055" width="0.1524" layer="94"/>
<wire x1="109.855" y1="46.355" x2="111.76" y2="46.355" width="0.1524" layer="94"/>
<wire x1="111.76" y1="46.355" x2="113.665" y2="46.355" width="0.1524" layer="94"/>
<wire x1="113.03" y1="45.72" x2="110.49" y2="45.72" width="0.1524" layer="94"/>
<wire x1="112.395" y1="45.085" x2="111.125" y2="45.085" width="0.1524" layer="94"/>
<wire x1="111.76" y1="66.04" x2="111.76" y2="77.47" width="0.1524" layer="94"/>
<wire x1="111.76" y1="46.355" x2="111.76" y2="48.26" width="0.1524" layer="94"/>
<wire x1="111.76" y1="48.26" x2="110.49" y2="48.895" width="0.1524" layer="94"/>
<wire x1="110.49" y1="48.895" x2="112.395" y2="49.53" width="0.1524" layer="94"/>
<wire x1="112.395" y1="49.53" x2="110.49" y2="50.165" width="0.1524" layer="94"/>
<wire x1="110.49" y1="50.165" x2="112.395" y2="50.8" width="0.1524" layer="94"/>
<wire x1="112.395" y1="50.8" x2="110.49" y2="51.435" width="0.1524" layer="94"/>
<wire x1="110.49" y1="51.435" x2="111.76" y2="52.07" width="0.1524" layer="94"/>
<wire x1="111.76" y1="52.07" x2="111.76" y2="58.42" width="0.1524" layer="94"/>
<wire x1="27.305" y1="87.63" x2="25.4" y2="87.63" width="0.1524" layer="94"/>
<wire x1="25.4" y1="87.63" x2="15.875" y2="87.63" width="0.1524" layer="94"/>
<wire x1="15.875" y1="87.63" x2="13.97" y2="87.63" width="0.1524" layer="94"/>
<wire x1="13.97" y1="87.63" x2="13.97" y2="89.535" width="0.1524" layer="94"/>
<wire x1="13.97" y1="89.535" x2="15.875" y2="89.535" width="0.1524" layer="94"/>
<wire x1="15.875" y1="89.535" x2="25.4" y2="89.535" width="0.1524" layer="94"/>
<wire x1="25.4" y1="89.535" x2="27.305" y2="89.535" width="0.1524" layer="94"/>
<wire x1="27.305" y1="89.535" x2="27.305" y2="87.63" width="0.1524" layer="94"/>
<wire x1="25.4" y1="87.63" x2="25.4" y2="77.47" width="0.1524" layer="94"/>
<wire x1="25.4" y1="77.47" x2="15.875" y2="77.47" width="0.1524" layer="94"/>
<wire x1="15.875" y1="77.47" x2="15.875" y2="87.63" width="0.1524" layer="94"/>
<wire x1="25.4" y1="77.47" x2="27.94" y2="77.47" width="0.1524" layer="94"/>
<wire x1="27.94" y1="77.47" x2="27.94" y2="70.485" width="0.1524" layer="94"/>
<wire x1="27.94" y1="70.485" x2="13.335" y2="70.485" width="0.1524" layer="94"/>
<wire x1="13.335" y1="70.485" x2="13.335" y2="77.47" width="0.1524" layer="94"/>
<wire x1="13.335" y1="77.47" x2="15.875" y2="77.47" width="0.1524" layer="94"/>
<wire x1="25.4" y1="89.535" x2="22.86" y2="92.075" width="0.1524" layer="94"/>
<wire x1="22.86" y1="92.075" x2="21.59" y2="92.075" width="0.1524" layer="94"/>
<wire x1="21.59" y1="92.075" x2="19.685" y2="92.075" width="0.1524" layer="94"/>
<wire x1="19.685" y1="92.075" x2="18.415" y2="92.075" width="0.1524" layer="94"/>
<wire x1="18.415" y1="92.075" x2="15.875" y2="89.535" width="0.1524" layer="94"/>
<wire x1="21.59" y1="92.075" x2="21.59" y2="95.885" width="0.1524" layer="94"/>
<wire x1="21.59" y1="95.885" x2="19.685" y2="95.885" width="0.1524" layer="94"/>
<wire x1="19.685" y1="95.885" x2="19.685" y2="92.075" width="0.1524" layer="94"/>
<wire x1="27.94" y1="70.485" x2="27.94" y2="69.85" width="0.1524" layer="94"/>
<wire x1="27.94" y1="69.85" x2="24.765" y2="66.675" width="0.1524" layer="94"/>
<wire x1="24.765" y1="66.675" x2="17.145" y2="66.675" width="0.1524" layer="94"/>
<wire x1="17.145" y1="66.675" x2="13.335" y2="70.485" width="0.1524" layer="94"/>
<wire x1="23.495" y1="76.2" x2="23.495" y2="71.755" width="0.1524" layer="94" curve="-180" cap="flat"/>
<wire x1="17.78" y1="76.2" x2="17.78" y2="71.755" width="0.1524" layer="94" curve="180" cap="flat"/>
<wire x1="23.495" y1="71.755" x2="17.78" y2="71.755" width="0.1524" layer="94"/>
<wire x1="17.78" y1="76.2" x2="23.495" y2="76.2" width="0.1524" layer="94"/>
<wire x1="24.13" y1="74.295" x2="22.225" y2="74.295" width="0.1524" layer="94"/>
<wire x1="19.05" y1="74.295" x2="17.145" y2="74.295" width="0.1524" layer="94"/>
<wire x1="169.545" y1="52.705" x2="175.895" y2="52.705" width="0.1524" layer="94"/>
<wire x1="175.895" y1="52.705" x2="177.8" y2="67.945" width="0.1524" layer="94"/>
<wire x1="177.8" y1="67.945" x2="175.26" y2="67.945" width="0.1524" layer="94"/>
<wire x1="175.26" y1="67.945" x2="170.18" y2="67.945" width="0.1524" layer="94"/>
<wire x1="170.18" y1="67.945" x2="167.64" y2="67.945" width="0.1524" layer="94"/>
<wire x1="167.64" y1="67.945" x2="169.545" y2="52.705" width="0.1524" layer="94"/>
<wire x1="167.64" y1="67.945" x2="166.37" y2="67.945" width="0.1524" layer="94"/>
<wire x1="166.37" y1="67.945" x2="165.1" y2="67.945" width="0.1524" layer="94"/>
<wire x1="165.1" y1="67.945" x2="165.1" y2="70.485" width="0.1524" layer="94"/>
<wire x1="165.1" y1="70.485" x2="166.37" y2="70.485" width="0.1524" layer="94"/>
<wire x1="166.37" y1="70.485" x2="167.64" y2="70.485" width="0.1524" layer="94"/>
<wire x1="167.64" y1="70.485" x2="170.18" y2="70.485" width="0.1524" layer="94"/>
<wire x1="170.18" y1="70.485" x2="175.26" y2="70.485" width="0.1524" layer="94"/>
<wire x1="175.26" y1="70.485" x2="177.8" y2="70.485" width="0.1524" layer="94"/>
<wire x1="177.8" y1="70.485" x2="179.07" y2="70.485" width="0.1524" layer="94"/>
<wire x1="179.07" y1="70.485" x2="180.34" y2="70.485" width="0.1524" layer="94"/>
<wire x1="180.34" y1="70.485" x2="180.34" y2="67.945" width="0.1524" layer="94"/>
<wire x1="180.34" y1="67.945" x2="179.07" y2="67.945" width="0.1524" layer="94"/>
<wire x1="179.07" y1="67.945" x2="177.8" y2="67.945" width="0.1524" layer="94"/>
<wire x1="166.37" y1="70.485" x2="166.37" y2="67.945" width="0.1524" layer="94"/>
<wire x1="179.07" y1="67.945" x2="179.07" y2="70.485" width="0.1524" layer="94"/>
<wire x1="175.26" y1="70.485" x2="175.26" y2="67.945" width="0.1524" layer="94"/>
<wire x1="170.18" y1="70.485" x2="170.18" y2="67.945" width="0.1524" layer="94"/>
<wire x1="167.64" y1="70.485" x2="169.545" y2="72.39" width="0.1524" layer="94"/>
<wire x1="169.545" y1="72.39" x2="175.895" y2="72.39" width="0.1524" layer="94"/>
<wire x1="175.895" y1="72.39" x2="177.8" y2="70.485" width="0.1524" layer="94"/>
<wire x1="169.545" y1="72.39" x2="169.545" y2="76.835" width="0.1524" layer="94"/>
<wire x1="169.545" y1="76.835" x2="172.72" y2="76.835" width="0.1524" layer="94"/>
<wire x1="172.72" y1="76.835" x2="175.895" y2="76.835" width="0.1524" layer="94"/>
<wire x1="175.895" y1="76.835" x2="175.895" y2="72.39" width="0.1524" layer="94"/>
<wire x1="170.815" y1="45.72" x2="172.72" y2="45.72" width="0.1524" layer="94"/>
<wire x1="172.72" y1="45.72" x2="174.625" y2="45.72" width="0.1524" layer="94"/>
<wire x1="173.99" y1="45.085" x2="171.45" y2="45.085" width="0.1524" layer="94"/>
<wire x1="173.355" y1="44.45" x2="172.085" y2="44.45" width="0.1524" layer="94"/>
<wire x1="172.72" y1="65.405" x2="172.72" y2="76.835" width="0.1524" layer="94"/>
<wire x1="172.72" y1="45.72" x2="172.72" y2="47.625" width="0.1524" layer="94"/>
<wire x1="172.72" y1="47.625" x2="171.45" y2="48.26" width="0.1524" layer="94"/>
<wire x1="171.45" y1="48.26" x2="173.355" y2="48.895" width="0.1524" layer="94"/>
<wire x1="173.355" y1="48.895" x2="171.45" y2="49.53" width="0.1524" layer="94"/>
<wire x1="171.45" y1="49.53" x2="173.355" y2="50.165" width="0.1524" layer="94"/>
<wire x1="173.355" y1="50.165" x2="171.45" y2="50.8" width="0.1524" layer="94"/>
<wire x1="171.45" y1="50.8" x2="172.72" y2="51.435" width="0.1524" layer="94"/>
<wire x1="172.72" y1="55.245" x2="172.72" y2="51.435" width="0.1524" layer="94"/>
<wire x1="172.72" y1="65.405" x2="171.45" y2="65.405" width="0.1524" layer="94"/>
<wire x1="171.45" y1="65.405" x2="171.45" y2="61.595" width="0.1524" layer="94"/>
<wire x1="171.45" y1="61.595" x2="171.45" y2="56.515" width="0.1524" layer="94"/>
<wire x1="171.45" y1="56.515" x2="173.99" y2="56.515" width="0.1524" layer="94"/>
<wire x1="173.99" y1="56.515" x2="173.99" y2="65.405" width="0.1524" layer="94"/>
<wire x1="173.99" y1="65.405" x2="172.72" y2="65.405" width="0.1524" layer="94"/>
<wire x1="172.72" y1="55.245" x2="170.18" y2="55.245" width="0.1524" layer="94"/>
<wire x1="170.18" y1="55.245" x2="170.18" y2="61.595" width="0.1524" layer="94"/>
<wire x1="170.18" y1="61.595" x2="171.45" y2="61.595" width="0.1524" layer="94"/>
<wire x1="171.45" y1="61.595" x2="170.815" y2="62.23" width="0.1524" layer="94"/>
<wire x1="171.45" y1="61.595" x2="170.815" y2="60.96" width="0.1524" layer="94"/>
<wire x1="96.52" y1="17.78" x2="96.52" y2="21.59" width="0.1524" layer="94"/>
<wire x1="95.25" y1="17.78" x2="97.79" y2="17.78" width="0.1524" layer="94" curve="180" cap="flat"/>
<wire x1="91.44" y1="17.78" x2="91.44" y2="21.59" width="0.1524" layer="94"/>
<wire x1="90.17" y1="17.78" x2="92.71" y2="17.78" width="0.1524" layer="94" curve="180" cap="flat"/>
<wire x1="86.36" y1="17.78" x2="86.36" y2="21.59" width="0.1524" layer="94"/>
<wire x1="85.09" y1="17.78" x2="87.63" y2="17.78" width="0.1524" layer="94" curve="180" cap="flat"/>
<wire x1="81.28" y1="17.78" x2="81.28" y2="21.59" width="0.1524" layer="94"/>
<wire x1="80.01" y1="17.78" x2="82.55" y2="17.78" width="0.1524" layer="94" curve="180" cap="flat"/>
<wire x1="76.2" y1="17.78" x2="76.2" y2="21.59" width="0.1524" layer="94"/>
<wire x1="74.93" y1="17.78" x2="77.47" y2="17.78" width="0.1524" layer="94" curve="180" cap="flat"/>
<wire x1="101.6" y1="17.78" x2="101.6" y2="21.59" width="0.1524" layer="94"/>
<wire x1="100.33" y1="17.78" x2="102.87" y2="17.78" width="0.1524" layer="94" curve="180" cap="flat"/>
<wire x1="72.39" y1="12.7" x2="72.39" y2="25.4" width="0.1524" layer="94"/>
<wire x1="72.39" y1="25.4" x2="120.65" y2="25.4" width="0.1524" layer="94"/>
<wire x1="120.65" y1="25.4" x2="120.65" y2="12.7" width="0.1524" layer="94"/>
<wire x1="106.68" y1="17.78" x2="106.68" y2="21.59" width="0.1524" layer="94"/>
<wire x1="105.41" y1="17.78" x2="107.95" y2="17.78" width="0.1524" layer="94" curve="180" cap="flat"/>
<wire x1="111.76" y1="17.78" x2="111.76" y2="21.59" width="0.1524" layer="94"/>
<wire x1="110.49" y1="17.78" x2="113.03" y2="17.78" width="0.1524" layer="94" curve="180" cap="flat"/>
<wire x1="72.39" y1="12.7" x2="120.65" y2="12.7" width="0.1524" layer="94"/>
<wire x1="144.145" y1="64.135" x2="150.495" y2="64.135" width="0.1524" layer="94"/>
<wire x1="150.495" y1="64.135" x2="152.4" y2="68.58" width="0.1524" layer="94"/>
<wire x1="152.4" y1="68.58" x2="149.86" y2="68.58" width="0.1524" layer="94"/>
<wire x1="149.86" y1="68.58" x2="144.78" y2="68.58" width="0.1524" layer="94"/>
<wire x1="144.78" y1="68.58" x2="142.24" y2="68.58" width="0.1524" layer="94"/>
<wire x1="142.24" y1="68.58" x2="144.145" y2="64.135" width="0.1524" layer="94"/>
<wire x1="142.24" y1="68.58" x2="140.97" y2="68.58" width="0.1524" layer="94"/>
<wire x1="140.97" y1="68.58" x2="139.7" y2="68.58" width="0.1524" layer="94"/>
<wire x1="139.7" y1="68.58" x2="139.7" y2="71.12" width="0.1524" layer="94"/>
<wire x1="139.7" y1="71.12" x2="140.97" y2="71.12" width="0.1524" layer="94"/>
<wire x1="140.97" y1="71.12" x2="142.24" y2="71.12" width="0.1524" layer="94"/>
<wire x1="142.24" y1="71.12" x2="144.78" y2="71.12" width="0.1524" layer="94"/>
<wire x1="144.78" y1="71.12" x2="149.86" y2="71.12" width="0.1524" layer="94"/>
<wire x1="149.86" y1="71.12" x2="152.4" y2="71.12" width="0.1524" layer="94"/>
<wire x1="152.4" y1="71.12" x2="153.67" y2="71.12" width="0.1524" layer="94"/>
<wire x1="153.67" y1="71.12" x2="154.94" y2="71.12" width="0.1524" layer="94"/>
<wire x1="154.94" y1="71.12" x2="154.94" y2="68.58" width="0.1524" layer="94"/>
<wire x1="154.94" y1="68.58" x2="153.67" y2="68.58" width="0.1524" layer="94"/>
<wire x1="153.67" y1="68.58" x2="152.4" y2="68.58" width="0.1524" layer="94"/>
<wire x1="140.97" y1="71.12" x2="140.97" y2="68.58" width="0.1524" layer="94"/>
<wire x1="153.67" y1="68.58" x2="153.67" y2="71.12" width="0.1524" layer="94"/>
<wire x1="149.86" y1="71.12" x2="149.86" y2="68.58" width="0.1524" layer="94"/>
<wire x1="144.78" y1="71.12" x2="144.78" y2="68.58" width="0.1524" layer="94"/>
<wire x1="142.24" y1="71.12" x2="144.145" y2="73.025" width="0.1524" layer="94"/>
<wire x1="144.145" y1="73.025" x2="150.495" y2="73.025" width="0.1524" layer="94"/>
<wire x1="150.495" y1="73.025" x2="152.4" y2="71.12" width="0.1524" layer="94"/>
<wire x1="144.145" y1="73.025" x2="144.145" y2="77.47" width="0.1524" layer="94"/>
<wire x1="144.145" y1="77.47" x2="147.32" y2="77.47" width="0.1524" layer="94"/>
<wire x1="147.32" y1="77.47" x2="150.495" y2="77.47" width="0.1524" layer="94"/>
<wire x1="150.495" y1="77.47" x2="150.495" y2="73.025" width="0.1524" layer="94"/>
<wire x1="146.685" y1="67.31" x2="145.415" y2="65.405" width="0.1524" layer="94"/>
<wire x1="147.32" y1="64.77" x2="147.32" y2="61.595" width="0.1524" layer="94"/>
<wire x1="145.415" y1="61.595" x2="149.225" y2="61.595" width="0.1524" layer="94"/>
<wire x1="148.59" y1="60.96" x2="146.05" y2="60.96" width="0.1524" layer="94"/>
<wire x1="147.955" y1="60.325" x2="146.685" y2="60.325" width="0.1524" layer="94"/>
<wire x1="147.32" y1="67.945" x2="147.32" y2="77.47" width="0.1524" layer="94"/>
<wire x1="116.84" y1="17.78" x2="116.84" y2="21.59" width="0.1524" layer="94"/>
<wire x1="115.57" y1="17.78" x2="118.11" y2="17.78" width="0.1524" layer="94" curve="180" cap="flat"/>
<wire x1="58.42" y1="17.78" x2="58.42" y2="21.59" width="0.1524" layer="94"/>
<wire x1="57.15" y1="17.78" x2="59.69" y2="17.78" width="0.1524" layer="94" curve="180" cap="flat"/>
<wire x1="53.34" y1="17.78" x2="53.34" y2="21.59" width="0.1524" layer="94"/>
<wire x1="52.07" y1="17.78" x2="54.61" y2="17.78" width="0.1524" layer="94" curve="180" cap="flat"/>
<wire x1="49.53" y1="25.4" x2="49.53" y2="12.7" width="0.1524" layer="94"/>
<wire x1="49.53" y1="12.7" x2="62.23" y2="12.7" width="0.1524" layer="94"/>
<wire x1="62.23" y1="12.7" x2="62.23" y2="25.4" width="0.1524" layer="94"/>
<wire x1="62.23" y1="25.4" x2="49.53" y2="25.4" width="0.1524" layer="94"/>
<wire x1="33.655" y1="40.005" x2="31.75" y2="36.83" width="0.1524" layer="94"/>
<wire x1="33.655" y1="40.005" x2="35.56" y2="36.83" width="0.1524" layer="94"/>
<wire x1="33.655" y1="67.945" x2="35.56" y2="71.12" width="0.1524" layer="94"/>
<wire x1="33.655" y1="67.945" x2="31.75" y2="71.12" width="0.1524" layer="94"/>
<wire x1="31.75" y1="36.83" x2="31.75" y2="71.12" width="0.1524" layer="94"/>
<wire x1="35.56" y1="36.83" x2="35.56" y2="48.895" width="0.1524" layer="94"/>
<wire x1="35.56" y1="48.895" x2="35.56" y2="58.42" width="0.1524" layer="94"/>
<wire x1="35.56" y1="58.42" x2="35.56" y2="71.12" width="0.1524" layer="94"/>
<wire x1="35.56" y1="58.42" x2="36.83" y2="58.42" width="0.1524" layer="94"/>
<wire x1="36.83" y1="58.42" x2="36.83" y2="48.895" width="0.1524" layer="94"/>
<wire x1="36.83" y1="48.895" x2="35.56" y2="48.895" width="0.1524" layer="94"/>
<wire x1="36.83" y1="58.42" x2="36.83" y2="71.12" width="0.1524" layer="94"/>
<wire x1="36.83" y1="71.12" x2="38.735" y2="73.025" width="0.1524" layer="94"/>
<wire x1="38.735" y1="73.025" x2="39.37" y2="73.025" width="0.1524" layer="94"/>
<wire x1="39.37" y1="73.025" x2="42.545" y2="73.025" width="0.1524" layer="94"/>
<wire x1="42.545" y1="73.025" x2="47.625" y2="73.025" width="0.1524" layer="94"/>
<wire x1="47.625" y1="73.025" x2="50.8" y2="73.025" width="0.1524" layer="94"/>
<wire x1="50.8" y1="73.025" x2="52.07" y2="73.025" width="0.1524" layer="94"/>
<wire x1="52.07" y1="73.025" x2="53.975" y2="71.12" width="0.1524" layer="94"/>
<wire x1="53.975" y1="71.12" x2="53.975" y2="43.18" width="0.1524" layer="94"/>
<wire x1="36.83" y1="48.895" x2="36.83" y2="36.195" width="0.1524" layer="94"/>
<wire x1="36.83" y1="36.195" x2="38.735" y2="34.29" width="0.1524" layer="94"/>
<wire x1="38.735" y1="34.29" x2="52.07" y2="34.29" width="0.1524" layer="94"/>
<wire x1="52.07" y1="34.29" x2="53.975" y2="36.195" width="0.1524" layer="94"/>
<wire x1="53.975" y1="36.195" x2="53.975" y2="64.135" width="0.1524" layer="94"/>
<wire x1="39.37" y1="57.15" x2="39.37" y2="37.465" width="0.1524" layer="94"/>
<wire x1="39.37" y1="37.465" x2="52.07" y2="37.465" width="0.1524" layer="94"/>
<wire x1="52.07" y1="37.465" x2="52.07" y2="57.15" width="0.1524" layer="94"/>
<wire x1="52.07" y1="57.15" x2="39.37" y2="57.15" width="0.1524" layer="94"/>
<wire x1="39.37" y1="59.69" x2="39.37" y2="66.675" width="0.1524" layer="94"/>
<wire x1="39.37" y1="66.675" x2="43.18" y2="66.675" width="0.1524" layer="94"/>
<wire x1="43.18" y1="66.675" x2="43.18" y2="67.945" width="0.1524" layer="94"/>
<wire x1="43.18" y1="67.945" x2="46.99" y2="67.945" width="0.1524" layer="94"/>
<wire x1="46.99" y1="67.945" x2="46.99" y2="66.675" width="0.1524" layer="94"/>
<wire x1="46.99" y1="66.675" x2="50.8" y2="66.675" width="0.1524" layer="94"/>
<wire x1="50.8" y1="66.675" x2="50.8" y2="59.69" width="0.1524" layer="94"/>
<wire x1="50.8" y1="59.69" x2="39.37" y2="59.69" width="0.1524" layer="94"/>
<wire x1="40.64" y1="62.865" x2="49.53" y2="62.865" width="0.1524" layer="94"/>
<wire x1="41.275" y1="52.705" x2="50.165" y2="52.705" width="0.1524" layer="94"/>
<wire x1="45.72" y1="39.37" x2="45.72" y2="46.355" width="0.1524" layer="94"/>
<wire x1="42.545" y1="73.025" x2="42.545" y2="74.93" width="0.1524" layer="94"/>
<wire x1="42.545" y1="74.93" x2="43.815" y2="74.93" width="0.1524" layer="94"/>
<wire x1="43.815" y1="74.93" x2="46.355" y2="74.93" width="0.1524" layer="94"/>
<wire x1="46.355" y1="74.93" x2="47.625" y2="74.93" width="0.1524" layer="94"/>
<wire x1="47.625" y1="74.93" x2="47.625" y2="73.025" width="0.1524" layer="94"/>
<wire x1="47.625" y1="74.93" x2="48.26" y2="74.93" width="0.1524" layer="94"/>
<wire x1="48.26" y1="74.93" x2="50.165" y2="74.93" width="0.1524" layer="94"/>
<wire x1="50.165" y1="74.93" x2="50.8" y2="74.295" width="0.1524" layer="94"/>
<wire x1="50.8" y1="74.295" x2="50.8" y2="73.025" width="0.1524" layer="94"/>
<wire x1="42.545" y1="74.93" x2="41.91" y2="74.93" width="0.1524" layer="94"/>
<wire x1="41.91" y1="74.93" x2="40.005" y2="74.93" width="0.1524" layer="94"/>
<wire x1="40.005" y1="74.93" x2="39.37" y2="74.295" width="0.1524" layer="94"/>
<wire x1="39.37" y1="74.295" x2="39.37" y2="73.025" width="0.1524" layer="94"/>
<wire x1="43.815" y1="74.93" x2="43.815" y2="75.565" width="0.1524" layer="94"/>
<wire x1="43.815" y1="75.565" x2="46.355" y2="75.565" width="0.1524" layer="94"/>
<wire x1="46.355" y1="75.565" x2="46.355" y2="74.93" width="0.1524" layer="94"/>
<wire x1="46.355" y1="75.565" x2="47.625" y2="75.565" width="0.1524" layer="94"/>
<wire x1="47.625" y1="75.565" x2="48.26" y2="74.93" width="0.1524" layer="94"/>
<wire x1="43.815" y1="75.565" x2="42.545" y2="75.565" width="0.1524" layer="94"/>
<wire x1="42.545" y1="75.565" x2="41.91" y2="74.93" width="0.1524" layer="94"/>
<circle x="22.86" y="36.83" radius="0.898" width="0.1524" layer="94"/>
<circle x="22.86" y="31.115" radius="0.898" width="0.1524" layer="94"/>
<circle x="22.86" y="25.4" radius="0.898" width="0.1524" layer="94"/>
<circle x="22.86" y="19.685" radius="0.898" width="0.1524" layer="94"/>
<circle x="128.905" y="65.405" radius="0.635" width="0.1524" layer="94"/>
<circle x="128.905" y="59.055" radius="0.635" width="0.1524" layer="94"/>
<circle x="111.76" y="65.405" radius="0.635" width="0.1524" layer="94"/>
<circle x="111.76" y="59.055" radius="0.635" width="0.1524" layer="94"/>
<circle x="147.32" y="67.31" radius="0.635" width="0.1524" layer="94"/>
<circle x="147.32" y="65.405" radius="0.635" width="0.1524" layer="94"/>
<text x="17.78" y="13.97" size="1.778" layer="94" align="bottom-center">GLOW PLUGS</text>
<text x="81.915" y="73.025" size="2.54" layer="94">B</text>
<text x="88.265" y="73.025" size="2.54" layer="94">ST</text>
<text x="76.2" y="73.66" size="1.4224" layer="95">30</text>
<text x="94.615" y="73.66" size="1.4224" layer="95">50</text>
<text x="78.105" y="64.77" size="2.54" layer="94">STARTER</text>
<text x="75.565" y="60.96" size="2.54" layer="94">24V/3,2kW</text>
<text x="111.76" y="83.82" size="1.778" layer="94" align="bottom-center">Thermo-switch</text>
<text x="129.54" y="83.82" size="1.778" layer="94" align="bottom-center">Oil-switch</text>
<text x="22.86" y="82.55" size="1.778" layer="94" rot="R270" align="top-center">STOP</text>
<text x="20.32" y="82.55" size="1.4224" layer="94" rot="R270" align="top-center">SOLENOID</text>
<text x="172.72" y="83.82" size="1.778" layer="94" align="bottom-center">Thermo sensor</text>
<text x="75.565" y="19.05" size="1.778" layer="94" rot="R90">1</text>
<text x="80.645" y="19.05" size="1.778" layer="94" rot="R90">2</text>
<text x="85.725" y="19.05" size="1.778" layer="94" rot="R90">3</text>
<text x="90.805" y="19.05" size="1.778" layer="94" rot="R90">4</text>
<text x="95.885" y="19.05" size="1.778" layer="94" rot="R90">5</text>
<text x="100.965" y="19.05" size="1.778" layer="94" rot="R90">6</text>
<text x="106.045" y="19.05" size="1.778" layer="94" rot="R90">7</text>
<text x="111.125" y="19.05" size="1.778" layer="94" rot="R90">8</text>
<text x="147.32" y="83.82" size="1.778" layer="94" align="bottom-center">Air-switch</text>
<text x="116.205" y="19.05" size="1.778" layer="94" rot="R90">9</text>
<text x="71.12" y="17.78" size="1.778" layer="95" rot="R90">K1-B</text>
<text x="52.705" y="19.05" size="1.778" layer="94" rot="R90">1</text>
<text x="57.785" y="19.05" size="1.778" layer="94" rot="R90">2</text>
<text x="48.26" y="17.78" size="1.778" layer="95" rot="R90">K2-B</text>
<text x="53.34" y="15.24" size="1.27" layer="95" align="center">4</text>
<text x="58.42" y="15.24" size="1.27" layer="95" align="center">2,5</text>
<text x="76.2" y="15.24" size="1.27" layer="95" align="center">0,5</text>
<text x="17.526" y="74.422" size="1.4224" layer="94">1</text>
<text x="22.606" y="74.422" size="1.4224" layer="94">2</text>
<text x="44.45" y="53.34" size="1.778" layer="94">IG</text>
<text x="46.355" y="43.815" size="1.778" layer="94">L</text>
<text x="44.45" y="64.135" size="1.778" layer="94">P</text>
<text x="44.45" y="70.485" size="1.778" layer="94">B</text>
<text x="34.925" y="45.72" size="1.778" layer="94" rot="R90">ALT. 24V/30A</text>
<wire x1="106.68" y1="21.59" x2="106.68" y2="26.67" width="0.1524" layer="91"/>
<wire x1="147.32" y1="77.47" x2="147.32" y2="81.28" width="0.1524" layer="91"/>
<wire x1="138.43" y1="81.28" x2="147.32" y2="81.28" width="0.1524" layer="91"/>
<wire x1="96.52" y1="21.59" x2="96.52" y2="36.83" width="0.1524" layer="91"/>
<wire x1="96.52" y1="36.83" x2="138.43" y2="36.83" width="0.1524" layer="91"/>
<wire x1="138.43" y1="36.83" x2="138.43" y2="81.28" width="0.1524" layer="91"/>
<wire x1="53.34" y1="27.94" x2="30.48" y2="27.94" width="0.1524" layer="91"/>
<wire x1="30.48" y1="27.94" x2="30.48" y2="17.78" width="0.1524" layer="91"/>
<wire x1="30.48" y1="17.78" x2="22.86" y2="17.78" width="0.1524" layer="91"/>
<wire x1="53.34" y1="27.94" x2="53.34" y2="21.59" width="0.1524" layer="91"/>
<wire x1="101.6" y1="49.53" x2="101.6" y2="81.28" width="0.1524" layer="91"/>
<wire x1="86.36" y1="49.53" x2="101.6" y2="49.53" width="0.1524" layer="91"/>
<wire x1="86.36" y1="49.53" x2="86.36" y2="21.59" width="0.1524" layer="91"/>
<wire x1="101.6" y1="81.28" x2="111.76" y2="81.28" width="0.1524" layer="91"/>
<wire x1="111.76" y1="81.28" x2="111.76" y2="77.47" width="0.1524" layer="91"/>
<wire x1="157.48" y1="99.06" x2="10.16" y2="99.06" width="0.1524" layer="91"/>
<wire x1="101.6" y1="21.59" x2="101.6" y2="33.02" width="0.1524" layer="91"/>
<wire x1="101.6" y1="33.02" x2="157.48" y2="33.02" width="0.1524" layer="91"/>
<wire x1="157.48" y1="33.02" x2="157.48" y2="99.06" width="0.1524" layer="91"/>
<wire x1="10.16" y1="88.9" x2="10.16" y2="99.06" width="0.1524" layer="91"/>
<wire x1="91.44" y1="21.59" x2="91.44" y2="40.64" width="0.1524" layer="91"/>
<wire x1="91.44" y1="40.64" x2="120.65" y2="40.64" width="0.1524" layer="91"/>
<wire x1="120.65" y1="40.64" x2="120.65" y2="81.28" width="0.1524" layer="91"/>
<wire x1="120.65" y1="81.28" x2="128.905" y2="81.28" width="0.1524" layer="91"/>
<wire x1="128.905" y1="81.28" x2="128.905" y2="77.47" width="0.1524" layer="91"/>
<wire x1="111.76" y1="21.59" x2="111.76" y2="29.21" width="0.1524" layer="91"/>
<wire x1="111.76" y1="29.21" x2="162.56" y2="29.21" width="0.1524" layer="91"/>
<wire x1="162.56" y1="29.21" x2="162.56" y2="81.28" width="0.1524" layer="91"/>
<wire x1="162.56" y1="81.28" x2="172.72" y2="81.28" width="0.1524" layer="91"/>
<wire x1="172.72" y1="81.28" x2="172.72" y2="76.835" width="0.1524" layer="91"/>
<wire x1="58.42" y1="86.36" x2="90.805" y2="86.36" width="0.1524" layer="91"/>
<wire x1="90.805" y1="86.36" x2="90.805" y2="76.835" width="0.1524" layer="91"/>
<wire x1="58.42" y1="21.59" x2="58.42" y2="86.36" width="0.1524" layer="91"/>
<wire x1="22.86" y1="35.56" x2="22.86" y2="45.72" width="0.1524" layer="91"/>
<wire x1="83.185" y1="76.835" x2="83.185" y2="81.28" width="0.1524" layer="91"/>
<wire x1="83.185" y1="81.28" x2="45.085" y2="81.28" width="0.1524" layer="91"/>
<wire x1="45.085" y1="75.565" x2="45.085" y2="81.28" width="0.1524" layer="91"/>
<wire x1="76.2" y1="21.59" x2="76.2" y2="43.18" width="0.1524" layer="91"/>
<wire x1="81.28" y1="21.59" x2="81.28" y2="49.53" width="0.1524" layer="91"/>
<wire x1="81.28" y1="49.53" x2="45.72" y2="49.53" width="0.1524" layer="91"/>
<wire x1="45.72" y1="49.53" x2="45.72" y2="52.705" width="0.1524" layer="91"/>
<wire x1="10.16" y1="78.74" x2="10.16" y2="68.58" width="0.1524" layer="91"/>
<wire x1="22.86" y1="55.88" x2="22.86" y2="74.295" width="0.1524" layer="91"/>
<wire x1="182.88" y1="0" x2="182.88" y2="101.6" width="0.4064" layer="94"/>
<wire x1="0" y1="101.6" x2="182.88" y2="101.6" width="0.4064" layer="94"/>
<wire x1="21.59" y1="54.61" x2="22.86" y2="54.61" width="0.1524" layer="94"/>
<wire x1="22.86" y1="54.61" x2="24.13" y2="54.61" width="0.1524" layer="94"/>
<wire x1="24.13" y1="54.61" x2="24.13" y2="46.99" width="0.1524" layer="94"/>
<wire x1="24.13" y1="46.99" x2="22.86" y2="46.99" width="0.1524" layer="94"/>
<wire x1="22.86" y1="46.99" x2="21.59" y2="46.99" width="0.1524" layer="94"/>
<wire x1="21.59" y1="46.99" x2="21.59" y2="54.61" width="0.1524" layer="94"/>
<wire x1="22.86" y1="54.61" x2="22.86" y2="55.88" width="0.1524" layer="94"/>
<wire x1="22.86" y1="46.99" x2="22.86" y2="45.72" width="0.1524" layer="94"/>
<wire x1="10.16" y1="68.58" x2="17.78" y2="68.58" width="0.1524" layer="91"/>
<wire x1="17.78" y1="68.58" x2="17.78" y2="74.168" width="0.1524" layer="91"/>
<text x="20.32" y="52.07" size="1.778" layer="96" align="center-right">1R0</text>
<wire x1="8.89" y1="87.63" x2="10.16" y2="87.63" width="0.1524" layer="94"/>
<wire x1="10.16" y1="87.63" x2="11.43" y2="87.63" width="0.1524" layer="94"/>
<wire x1="11.43" y1="87.63" x2="11.43" y2="80.01" width="0.1524" layer="94"/>
<wire x1="11.43" y1="80.01" x2="10.16" y2="80.01" width="0.1524" layer="94"/>
<wire x1="10.16" y1="80.01" x2="8.89" y2="80.01" width="0.1524" layer="94"/>
<wire x1="8.89" y1="80.01" x2="8.89" y2="87.63" width="0.1524" layer="94"/>
<wire x1="10.16" y1="87.63" x2="10.16" y2="88.9" width="0.1524" layer="94"/>
<wire x1="10.16" y1="80.01" x2="10.16" y2="78.74" width="0.1524" layer="94"/>
<text x="7.62" y="82.55" size="1.778" layer="96" align="center-right">20W</text>
<wire x1="45.085" y1="81.28" x2="45.085" y2="93.98" width="0.1524" layer="91"/>
<text x="45.085" y="94.615" size="1.778" layer="94" align="bottom-center">BAT</text>
<text x="121.92" y="15.24" size="1.27" layer="95" align="center-left">mm2</text>
<text x="63.5" y="15.24" size="1.27" layer="95" align="center-left">mm2</text>
<text x="20.32" y="49.53" size="1.778" layer="96" align="center-right">20W</text>
<text x="7.62" y="85.09" size="1.778" layer="96" align="center-right">15R</text>
<wire x1="22.86" y1="17.78" x2="22.86" y2="20.32" width="0.1524" layer="91"/>
<wire x1="43.815" y1="32.385" x2="45.72" y2="32.385" width="0.1524" layer="94"/>
<wire x1="45.72" y1="32.385" x2="47.625" y2="32.385" width="0.1524" layer="94"/>
<wire x1="46.99" y1="31.75" x2="44.45" y2="31.75" width="0.1524" layer="94"/>
<wire x1="46.355" y1="31.115" x2="45.085" y2="31.115" width="0.1524" layer="94"/>
<wire x1="45.72" y1="32.385" x2="45.72" y2="34.29" width="0.1524" layer="94"/>
<wire x1="18.415" y1="64.77" x2="20.32" y2="64.77" width="0.1524" layer="94"/>
<wire x1="20.32" y1="64.77" x2="22.225" y2="64.77" width="0.1524" layer="94"/>
<wire x1="21.59" y1="64.135" x2="19.05" y2="64.135" width="0.1524" layer="94"/>
<wire x1="20.955" y1="63.5" x2="19.685" y2="63.5" width="0.1524" layer="94"/>
<wire x1="20.32" y1="64.77" x2="20.32" y2="66.675" width="0.1524" layer="94"/>
<wire x1="108.585" y1="28.575" x2="106.68" y2="28.575" width="0.1524" layer="94"/>
<wire x1="106.68" y1="28.575" x2="104.775" y2="28.575" width="0.1524" layer="94"/>
<wire x1="105.41" y1="29.21" x2="107.95" y2="29.21" width="0.1524" layer="94"/>
<wire x1="106.045" y1="29.845" x2="107.315" y2="29.845" width="0.1524" layer="94"/>
<wire x1="106.68" y1="28.575" x2="106.68" y2="26.67" width="0.1524" layer="94"/>
<text x="101.6" y="96.52" size="5.08" layer="94" align="top-center">Dieselový motor KUBOTA 2,4L</text>
<wire x1="0" y1="0" x2="2.54" y2="0" width="0.4064" layer="94"/>
<text x="81.28" y="15.24" size="1.27" layer="95" align="center">0,5</text>
<text x="86.36" y="15.24" size="1.27" layer="95" align="center">0,5</text>
<text x="91.44" y="15.24" size="1.27" layer="95" align="center">0,5</text>
<text x="96.52" y="15.24" size="1.27" layer="95" align="center">0,5</text>
<text x="101.6" y="15.24" size="1.27" layer="95" align="center">0,5</text>
<text x="106.68" y="15.24" size="1.27" layer="95" align="center">0,5</text>
<text x="111.76" y="15.24" size="1.27" layer="95" align="center">0,5</text>
<text x="116.84" y="15.24" size="1.27" layer="95" align="center">0,5</text>
<wire x1="2.54" y1="0" x2="53.34" y2="0" width="0.4064" layer="94"/>
<wire x1="53.34" y1="0" x2="58.42" y2="0" width="0.4064" layer="94"/>
<wire x1="58.42" y1="0" x2="76.2" y2="0" width="0.4064" layer="94"/>
<wire x1="76.2" y1="0" x2="81.28" y2="0" width="0.4064" layer="94"/>
<wire x1="81.28" y1="0" x2="86.36" y2="0" width="0.4064" layer="94"/>
<wire x1="86.36" y1="0" x2="91.44" y2="0" width="0.4064" layer="94"/>
<wire x1="91.44" y1="0" x2="96.52" y2="0" width="0.4064" layer="94"/>
<wire x1="96.52" y1="0" x2="101.6" y2="0" width="0.4064" layer="94"/>
<wire x1="101.6" y1="0" x2="106.68" y2="0" width="0.4064" layer="94"/>
<wire x1="106.68" y1="0" x2="111.76" y2="0" width="0.4064" layer="94"/>
<wire x1="111.76" y1="0" x2="182.88" y2="0" width="0.4064" layer="94"/>
<wire x1="53.34" y1="13.97" x2="53.34" y2="0" width="0.1524" layer="94"/>
<wire x1="58.42" y1="13.97" x2="58.42" y2="0" width="0.1524" layer="94"/>
<wire x1="76.2" y1="13.97" x2="76.2" y2="0" width="0.1524" layer="94"/>
<wire x1="81.28" y1="13.97" x2="81.28" y2="0" width="0.1524" layer="94"/>
<wire x1="86.36" y1="13.97" x2="86.36" y2="0" width="0.1524" layer="94"/>
<wire x1="91.44" y1="13.97" x2="91.44" y2="0" width="0.1524" layer="94"/>
<wire x1="96.52" y1="13.97" x2="96.52" y2="0" width="0.1524" layer="94"/>
<wire x1="101.6" y1="13.97" x2="101.6" y2="0" width="0.1524" layer="94"/>
<wire x1="106.68" y1="13.97" x2="106.68" y2="0" width="0.1524" layer="94"/>
<wire x1="111.76" y1="13.97" x2="111.76" y2="0" width="0.1524" layer="94"/>
<text x="52.705" y="1.27" size="1.27" layer="94" rot="R90">GLOW</text>
<text x="57.785" y="1.27" size="1.27" layer="94" rot="R90">START</text>
<text x="75.565" y="1.27" size="1.27" layer="94" rot="R90">EXTITATION</text>
<text x="80.645" y="1.27" size="1.27" layer="94" rot="R90">MOTOR</text>
<text x="85.725" y="1.27" size="1.27" layer="94" rot="R90">TEMP</text>
<text x="90.805" y="1.27" size="1.27" layer="94" rot="R90">OIL</text>
<text x="95.885" y="1.27" size="1.27" layer="94" rot="R90">AIR</text>
<text x="100.965" y="1.27" size="1.27" layer="94" rot="R90">STOP_SEL</text>
<text x="106.045" y="1.27" size="1.27" layer="94" rot="R90">GND</text>
<text x="111.125" y="1.27" size="1.27" layer="94" rot="R90">MTEMP</text>
<wire x1="45.72" y1="43.18" x2="76.2" y2="43.18" width="0.1524" layer="91"/>
<pin name="K2.1" x="53.34" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="K2.2" x="58.42" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="K1.1" x="76.2" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="K1.2" x="81.28" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="K1.3" x="86.36" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="K1.4" x="91.44" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="K1.5" x="96.52" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="K1.6" x="101.6" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="K1.7" x="106.68" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="K1.8" x="111.76" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
<wire x1="0" y1="2.54" x2="2.54" y2="0" width="0.4064" layer="94"/>
<text x="180.34" y="99.06" size="1.9304" layer="95" rot="R180">&gt;NAME</text>
<wire x1="13.335" y1="-3.175" x2="15.24" y2="-3.175" width="0.1524" layer="94"/>
<wire x1="15.24" y1="-3.175" x2="17.145" y2="-3.175" width="0.1524" layer="94"/>
<wire x1="16.51" y1="-3.81" x2="13.97" y2="-3.81" width="0.1524" layer="94"/>
<wire x1="15.875" y1="-4.445" x2="14.605" y2="-4.445" width="0.1524" layer="94"/>
<wire x1="18.415" y1="4.445" x2="20.32" y2="4.445" width="0.1524" layer="94"/>
<wire x1="20.32" y1="4.445" x2="22.225" y2="4.445" width="0.1524" layer="94"/>
<wire x1="21.59" y1="3.81" x2="19.05" y2="3.81" width="0.1524" layer="94"/>
<wire x1="20.955" y1="3.175" x2="19.685" y2="3.175" width="0.1524" layer="94"/>
<wire x1="20.32" y1="4.445" x2="20.32" y2="7.62" width="0.1524" layer="94"/>
<wire x1="15.24" y1="-3.175" x2="15.24" y2="7.62" width="0.1524" layer="94"/>
<wire x1="15.24" y1="7.62" x2="20.32" y2="7.62" width="0.1524" layer="94"/>
<wire x1="135.89" y1="14.605" x2="139.7" y2="14.605" width="0.1524" layer="94" curve="-180" cap="flat"/>
<wire x1="135.89" y1="14.605" x2="135.89" y2="12.7" width="0.1524" layer="94"/>
<wire x1="135.89" y1="12.7" x2="139.7" y2="12.7" width="0.1524" layer="94"/>
<wire x1="139.7" y1="12.7" x2="139.7" y2="14.605" width="0.1524" layer="94"/>
<wire x1="140.335" y1="23.495" x2="144.145" y2="23.495" width="0.1524" layer="94" curve="180" cap="flat"/>
<wire x1="144.145" y1="23.495" x2="144.145" y2="25.4" width="0.1524" layer="94"/>
<wire x1="144.145" y1="25.4" x2="140.335" y2="25.4" width="0.1524" layer="94"/>
<wire x1="140.335" y1="25.4" x2="140.335" y2="23.495" width="0.1524" layer="94"/>
<wire x1="131.445" y1="23.495" x2="135.255" y2="23.495" width="0.1524" layer="94" curve="180" cap="flat"/>
<wire x1="135.255" y1="23.495" x2="135.255" y2="25.4" width="0.1524" layer="94"/>
<wire x1="131.445" y1="23.495" x2="131.445" y2="25.4" width="0.1524" layer="94"/>
<wire x1="131.445" y1="25.4" x2="135.255" y2="25.4" width="0.1524" layer="94"/>
<wire x1="130.81" y1="26.035" x2="130.81" y2="12.065" width="0.1524" layer="94"/>
<wire x1="130.81" y1="12.065" x2="136.525" y2="12.065" width="0.1524" layer="94"/>
<wire x1="136.525" y1="12.065" x2="139.065" y2="12.065" width="0.1524" layer="94"/>
<wire x1="139.065" y1="12.065" x2="144.78" y2="12.065" width="0.1524" layer="94"/>
<wire x1="144.78" y1="12.065" x2="144.78" y2="26.035" width="0.1524" layer="94"/>
<wire x1="144.78" y1="26.035" x2="139.065" y2="26.035" width="0.1524" layer="94"/>
<wire x1="139.065" y1="26.035" x2="136.525" y2="26.035" width="0.1524" layer="94"/>
<wire x1="136.525" y1="26.035" x2="130.81" y2="26.035" width="0.1524" layer="94"/>
<wire x1="130.81" y1="26.035" x2="128.905" y2="26.035" width="0.1524" layer="94"/>
<wire x1="130.81" y1="12.065" x2="128.905" y2="12.065" width="0.1524" layer="94"/>
<wire x1="144.78" y1="12.065" x2="146.685" y2="12.065" width="0.1524" layer="94"/>
<wire x1="144.78" y1="26.035" x2="146.685" y2="26.035" width="0.1524" layer="94"/>
<wire x1="136.525" y1="26.035" x2="136.525" y2="27.305" width="0.1524" layer="94"/>
<wire x1="136.525" y1="27.305" x2="139.065" y2="27.305" width="0.1524" layer="94"/>
<wire x1="139.065" y1="27.305" x2="139.065" y2="26.035" width="0.1524" layer="94"/>
<wire x1="136.525" y1="12.065" x2="136.525" y2="10.795" width="0.1524" layer="94"/>
<wire x1="136.525" y1="10.795" x2="139.065" y2="10.795" width="0.1524" layer="94"/>
<wire x1="139.065" y1="10.795" x2="139.065" y2="12.065" width="0.1524" layer="94"/>
<circle x="142.24" y="14.605" radius="1.796" width="0.1524" layer="94"/>
<circle x="133.35" y="14.605" radius="1.796" width="0.1524" layer="94"/>
<circle x="142.24" y="19.05" radius="1.796" width="0.1524" layer="94"/>
<circle x="137.795" y="19.05" radius="1.796" width="0.1524" layer="94"/>
<circle x="133.35" y="19.05" radius="1.796" width="0.1524" layer="94"/>
<circle x="137.795" y="23.495" radius="1.796" width="0.1524" layer="94"/>
<circle x="142.24" y="14.605" radius="0.635" width="1.6764" layer="94"/>
<circle x="137.795" y="14.605" radius="0.635" width="1.6764" layer="94"/>
<circle x="133.35" y="14.605" radius="0.635" width="1.6764" layer="94"/>
<circle x="142.24" y="19.05" radius="0.635" width="1.6764" layer="94"/>
<circle x="137.795" y="19.05" radius="0.635" width="1.6764" layer="94"/>
<circle x="133.35" y="19.05" radius="0.635" width="1.6764" layer="94"/>
<circle x="142.24" y="23.495" radius="0.635" width="1.6764" layer="94"/>
<circle x="137.795" y="23.495" radius="0.635" width="1.6764" layer="94"/>
<circle x="133.35" y="23.495" radius="0.635" width="1.6764" layer="94"/>
<text x="142.24" y="26.67" size="1.016" layer="94" align="bottom-center">1</text>
<text x="146.05" y="11.43" size="1.778" layer="94" align="top-right">K1-B</text>
<text x="147.32" y="15.24" size="1.778" layer="95" rot="R90">SAMEC</text>
<wire x1="33.655" y1="26.035" x2="33.655" y2="20.32" width="0.254" layer="94"/>
<wire x1="33.655" y1="20.32" x2="36.195" y2="20.32" width="0.254" layer="94"/>
<wire x1="36.195" y1="20.32" x2="36.195" y2="12.065" width="0.254" layer="94"/>
<wire x1="36.195" y1="12.065" x2="41.91" y2="12.065" width="0.254" layer="94"/>
<wire x1="41.91" y1="12.065" x2="41.91" y2="20.32" width="0.254" layer="94"/>
<wire x1="41.91" y1="20.32" x2="44.45" y2="20.32" width="0.254" layer="94"/>
<wire x1="44.45" y1="20.32" x2="44.45" y2="26.035" width="0.254" layer="94"/>
<wire x1="44.45" y1="26.035" x2="33.655" y2="26.035" width="0.254" layer="94"/>
<text x="42.545" y="22.225" size="1.9304" layer="94">1</text>
<text x="41.91" y="11.43" size="1.9304" layer="94" rot="R180">K2-B</text>
<text x="44.45" y="11.43" size="1.778" layer="95" rot="R90">SAMEC</text>
<rectangle x1="36.195" y1="22.225" x2="41.91" y2="24.13" layer="94"/>
<rectangle x1="38.1" y1="13.97" x2="40.005" y2="19.05" layer="94"/>
<wire x1="31.115" y1="92.71" x2="31.115" y2="87.63" width="0.1524" layer="94"/>
<wire x1="31.75" y1="93.345" x2="32.385" y2="93.345" width="0.1524" layer="94"/>
<wire x1="32.385" y1="93.345" x2="33.401" y2="93.345" width="0.1524" layer="94"/>
<wire x1="38.354" y1="93.345" x2="39.37" y2="93.345" width="0.1524" layer="94"/>
<wire x1="39.37" y1="93.345" x2="40.005" y2="93.345" width="0.1524" layer="94"/>
<wire x1="40.64" y1="92.71" x2="40.64" y2="87.63" width="0.1524" layer="94"/>
<wire x1="31.115" y1="92.71" x2="31.75" y2="93.345" width="0.1524" layer="94" curve="-53.130102" cap="flat"/>
<wire x1="40.005" y1="93.345" x2="40.64" y2="92.71" width="0.1524" layer="94" curve="-90" cap="flat"/>
<wire x1="31.115" y1="87.63" x2="31.75" y2="86.995" width="0.1524" layer="94" curve="53.130102" cap="flat"/>
<wire x1="40.005" y1="86.995" x2="40.64" y2="87.63" width="0.1524" layer="94" curve="36.869898" cap="flat"/>
<wire x1="31.75" y1="86.995" x2="31.75" y2="86.487" width="0.1524" layer="94"/>
<wire x1="31.75" y1="86.487" x2="31.75" y2="85.09" width="0.1524" layer="94"/>
<wire x1="40.005" y1="86.995" x2="40.005" y2="85.09" width="0.1524" layer="94"/>
<wire x1="31.75" y1="86.487" x2="33.655" y2="86.487" width="0.1524" layer="94"/>
<wire x1="31.75" y1="85.09" x2="32.385" y2="84.455" width="0.1524" layer="94" curve="90" cap="flat"/>
<wire x1="39.37" y1="84.455" x2="40.005" y2="85.09" width="0.1524" layer="94" curve="90" cap="flat"/>
<wire x1="39.37" y1="84.455" x2="32.385" y2="84.455" width="0.1524" layer="94"/>
<wire x1="32.385" y1="93.345" x2="32.385" y2="94.742" width="0.1524" layer="94"/>
<wire x1="39.37" y1="93.345" x2="39.37" y2="94.742" width="0.1524" layer="94"/>
<wire x1="39.37" y1="94.742" x2="38.735" y2="94.742" width="0.1524" layer="94"/>
<wire x1="32.385" y1="92.075" x2="39.37" y2="92.075" width="0.1524" layer="94"/>
<wire x1="40.005" y1="91.44" x2="40.005" y2="89.535" width="0.1524" layer="94"/>
<wire x1="31.75" y1="91.44" x2="31.75" y2="89.535" width="0.1524" layer="94"/>
<wire x1="32.385" y1="88.9" x2="39.37" y2="88.9" width="0.1524" layer="94"/>
<wire x1="31.75" y1="91.44" x2="32.385" y2="92.075" width="0.1524" layer="94" curve="-90" cap="flat"/>
<wire x1="31.75" y1="89.535" x2="32.385" y2="88.9" width="0.1524" layer="94" curve="90" cap="flat"/>
<wire x1="39.37" y1="88.9" x2="40.005" y2="89.535" width="0.1524" layer="94" curve="90" cap="flat"/>
<wire x1="39.37" y1="92.075" x2="40.005" y2="91.44" width="0.1524" layer="94" curve="-90" cap="flat"/>
<wire x1="32.258" y1="91.313" x2="32.258" y2="89.535" width="0.1524" layer="94"/>
<wire x1="32.258" y1="89.535" x2="35.306" y2="89.535" width="0.1524" layer="94"/>
<wire x1="35.306" y1="89.535" x2="35.306" y2="91.313" width="0.1524" layer="94"/>
<wire x1="35.306" y1="91.313" x2="32.258" y2="91.313" width="0.1524" layer="94"/>
<wire x1="39.37" y1="91.313" x2="39.37" y2="89.535" width="0.1524" layer="94"/>
<wire x1="39.37" y1="89.535" x2="36.322" y2="89.535" width="0.1524" layer="94"/>
<wire x1="36.322" y1="89.535" x2="36.322" y2="91.313" width="0.1524" layer="94"/>
<wire x1="36.322" y1="91.313" x2="39.37" y2="91.313" width="0.1524" layer="94"/>
<wire x1="38.1" y1="86.233" x2="39.878" y2="86.233" width="0.1524" layer="94"/>
<wire x1="32.385" y1="94.742" x2="33.401" y2="94.742" width="0.1524" layer="94"/>
<wire x1="33.401" y1="94.742" x2="33.401" y2="93.345" width="0.1524" layer="94"/>
<wire x1="33.401" y1="93.345" x2="38.354" y2="93.345" width="0.1524" layer="94"/>
<wire x1="38.354" y1="93.345" x2="38.354" y2="94.742" width="0.1524" layer="94"/>
<wire x1="38.354" y1="94.742" x2="38.608" y2="94.742" width="0.1524" layer="94"/>
<wire x1="32.004" y1="92.075" x2="32.258" y2="92.329" width="0.1524" layer="94"/>
<wire x1="32.258" y1="92.329" x2="39.37" y2="92.329" width="0.1524" layer="94"/>
<wire x1="39.37" y1="92.329" x2="39.624" y2="92.075" width="0.1524" layer="94"/>
<text x="37.338" y="89.789" size="1.4224" layer="94">1</text>
<text x="33.401" y="89.789" size="1.4224" layer="94">2</text>
<text x="43.815" y="83.82" size="1.016" layer="94" align="top-right">STOP SELENOID konektor
pohled zepředu na konektor
na kabelu</text>
<circle x="45.085" y="81.28" radius="0.381" width="0" layer="91"/>
<wire x1="45.085" y1="93.98" x2="43.815" y2="92.71" width="0.1524" layer="94"/>
<wire x1="45.085" y1="93.98" x2="46.355" y2="92.71" width="0.1524" layer="94"/>
<circle x="20.32" y="66.675" radius="0.381" width="0" layer="94"/>
<wire x1="81.915" y1="50.165" x2="83.82" y2="50.165" width="0.1524" layer="94"/>
<wire x1="83.82" y1="50.165" x2="85.725" y2="50.165" width="0.1524" layer="94"/>
<wire x1="85.09" y1="49.53" x2="82.55" y2="49.53" width="0.1524" layer="94"/>
<wire x1="84.455" y1="48.895" x2="83.185" y2="48.895" width="0.1524" layer="94"/>
<wire x1="83.82" y1="50.165" x2="83.82" y2="52.07" width="0.1524" layer="94"/>
<circle x="83.82" y="52.07" radius="0.381" width="0" layer="94"/>
<circle x="45.72" y="34.29" radius="0.381" width="0" layer="94"/>
</symbol>
<symbol name="KUBOTA-FUELPUMP">
<wire x1="6.6675" y1="-3.175" x2="-6.6675" y2="-3.175" width="0.1524" layer="94"/>
<wire x1="-6.6675" y1="-3.175" x2="-6.6675" y2="3.175" width="0.1524" layer="94"/>
<wire x1="-6.6675" y1="3.175" x2="-4.7625" y2="3.175" width="0.1524" layer="94"/>
<wire x1="-4.7625" y1="3.175" x2="4.7625" y2="3.175" width="0.1524" layer="94"/>
<wire x1="4.7625" y1="3.175" x2="6.6675" y2="3.175" width="0.1524" layer="94"/>
<wire x1="6.6675" y1="3.175" x2="6.6675" y2="-3.175" width="0.1524" layer="94"/>
<wire x1="7.3025" y1="-3.175" x2="6.6675" y2="-3.175" width="0.1524" layer="94"/>
<wire x1="-6.6675" y1="-3.175" x2="-7.3025" y2="-3.175" width="0.1524" layer="94"/>
<wire x1="7.3025" y1="-3.175" x2="7.3025" y2="-4.445" width="0.1524" layer="94"/>
<wire x1="7.3025" y1="-4.445" x2="-7.3025" y2="-4.445" width="0.1524" layer="94"/>
<wire x1="-7.3025" y1="-4.445" x2="-7.3025" y2="-3.175" width="0.1524" layer="94"/>
<wire x1="4.7625" y1="3.175" x2="2.2225" y2="5.715" width="0.1524" layer="94"/>
<wire x1="2.2225" y1="5.715" x2="0.9525" y2="5.715" width="0.1524" layer="94"/>
<wire x1="0.9525" y1="5.715" x2="-0.9525" y2="5.715" width="0.1524" layer="94"/>
<wire x1="-0.9525" y1="5.715" x2="-2.2225" y2="5.715" width="0.1524" layer="94"/>
<wire x1="-2.2225" y1="5.715" x2="-4.7625" y2="3.175" width="0.1524" layer="94"/>
<wire x1="0.9525" y1="5.715" x2="0.9525" y2="9.525" width="0.1524" layer="94"/>
<wire x1="0.9525" y1="9.525" x2="-0.9525" y2="9.525" width="0.1524" layer="94"/>
<wire x1="-0.9525" y1="9.525" x2="-0.9525" y2="5.715" width="0.1524" layer="94"/>
<wire x1="-4.7625" y1="-4.445" x2="-2.2225" y2="-6.985" width="0.1524" layer="94"/>
<wire x1="-2.2225" y1="-6.985" x2="-0.9525" y2="-6.985" width="0.1524" layer="94"/>
<wire x1="-0.9525" y1="-6.985" x2="0.9525" y2="-6.985" width="0.1524" layer="94"/>
<wire x1="0.9525" y1="-6.985" x2="2.2225" y2="-6.985" width="0.1524" layer="94"/>
<wire x1="2.2225" y1="-6.985" x2="4.7625" y2="-4.445" width="0.1524" layer="94"/>
<wire x1="-0.9525" y1="-6.985" x2="-0.9525" y2="-10.795" width="0.1524" layer="94"/>
<wire x1="-0.9525" y1="-10.795" x2="0.9525" y2="-10.795" width="0.1524" layer="94"/>
<wire x1="0.9525" y1="-10.795" x2="0.9525" y2="-6.985" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="15.875" x2="-5.08" y2="12.065" width="0.1524" layer="94"/>
<wire x1="-3.81" y1="15.875" x2="-6.35" y2="15.875" width="0.1524" layer="94" curve="180" cap="flat"/>
<wire x1="-8.255" y1="10.795" x2="-2.54" y2="10.795" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="10.795" x2="-2.54" y2="17.78" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="17.78" x2="-5.08" y2="17.78" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="17.78" x2="-8.255" y2="17.78" width="0.1524" layer="94"/>
<wire x1="-8.255" y1="17.78" x2="-8.255" y2="10.795" width="0.1524" layer="94"/>
<text x="0" y="0.635" size="1.778" layer="94" align="bottom-center">FUEL</text>
<text x="0" y="-2.54" size="1.778" layer="94" align="bottom-center">PUMP</text>
<text x="-7.62" y="15.24" size="1.778" layer="94" rot="R270">1</text>
<text x="-0.635" y="12.7" size="1.778" layer="95">K4B</text>
<wire x1="-6.0325" y1="3.175" x2="-6.0325" y2="5.08" width="0.1524" layer="91"/>
<wire x1="-6.0325" y1="5.08" x2="-10.16" y2="5.08" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="12.065" x2="-5.08" y2="3.175" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="17.78" x2="-5.08" y2="20.32" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="20.32" x2="-10.16" y2="20.32" width="0.1524" layer="94"/>
<pin name="+" x="-12.7" y="20.32" visible="off" length="short" direction="pas"/>
<pin name="-" x="-12.7" y="5.08" visible="off" length="short" direction="pas"/>
<wire x1="-10.16" y1="22.86" x2="-10.16" y2="-12.7" width="0.4064" layer="94"/>
<wire x1="-10.16" y1="-12.7" x2="10.16" y2="-12.7" width="0.4064" layer="94"/>
<wire x1="10.16" y1="-12.7" x2="10.16" y2="22.86" width="0.4064" layer="94"/>
<wire x1="10.16" y1="22.86" x2="-10.16" y2="22.86" width="0.4064" layer="94"/>
</symbol>
<symbol name="KUBOTA-SPEED_SENSOR">
<wire x1="-10.16" y1="7.62" x2="-10.16" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="-10.16" y1="-5.08" x2="10.16" y2="-5.08" width="0.4064" layer="94"/>
<wire x1="10.16" y1="-5.08" x2="10.16" y2="7.62" width="0.4064" layer="94"/>
<wire x1="10.16" y1="7.62" x2="-10.16" y2="7.62" width="0.4064" layer="94"/>
<text x="0" y="5.08" size="1.778" layer="94" align="bottom-center">Speed sensor</text>
<circle x="5.08" y="0" radius="1.27" width="0.1524" layer="94"/>
<circle x="0" y="0" radius="1.27" width="0.1524" layer="94"/>
<circle x="-5.08" y="0" radius="1.27" width="0.1524" layer="94"/>
<text x="-5.08" y="0" size="1.27" layer="94" align="center">1</text>
<text x="0" y="0" size="1.27" layer="94" align="center">2</text>
<text x="5.08" y="0" size="1.27" layer="94" align="center">3</text>
<wire x1="-5.08" y1="-1.27" x2="-5.08" y2="-12.7" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="-12.7" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="-12.7" width="0.1524" layer="94"/>
<text x="-11.43" y="0" size="1.778" layer="95" rot="R90">K3</text>
<text x="-5.715" y="-11.43" size="1.27" layer="94" rot="R90">GND</text>
<text x="-0.635" y="-11.43" size="1.27" layer="94" rot="R90">Signal</text>
<text x="4.445" y="-11.43" size="1.27" layer="94" rot="R90">+</text>
<pin name="K3.1" x="-5.08" y="-15.24" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="K3.2" x="0" y="-15.24" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="K3.3" x="5.08" y="-15.24" visible="off" length="short" direction="pas" rot="R90"/>
<text x="0" y="2.54" size="1.4224" layer="94" align="bottom-center">ABRA: MOST0605</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="MCFLEX" prefix="MCFLEX">
<description>TODO: udělat volitelný symbol uspořádání konektoru&lt;P&gt;
&lt;B&gt;MCflex&lt;/B&gt; control panel</description>
<gates>
<gate name="G$1" symbol="MCFLEX" x="0" y="0"/>
</gates>
<devices>
<device name="" package="EMPTY">
<connects>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="15" pad="15"/>
<connect gate="G$1" pin="18" pad="18"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="AUTOSTART" pad="7"/>
<connect gate="G$1" pin="CANH" pad="19"/>
<connect gate="G$1" pin="CANL" pad="17"/>
<connect gate="G$1" pin="DIVERSE" pad="4"/>
<connect gate="G$1" pin="KL30@14" pad="14"/>
<connect gate="G$1" pin="KL30@16" pad="16"/>
<connect gate="G$1" pin="KL31" pad="5"/>
<connect gate="G$1" pin="OIL" pad="1"/>
<connect gate="G$1" pin="SPEED" pad="13"/>
<connect gate="G$1" pin="STOP" pad="9"/>
<connect gate="G$1" pin="TEMP" pad="3"/>
</connects>
<technologies>
<technology name="">
<attribute name="ABRA" value="MELE1658"/>
<attribute name="IN_01" value="NC" constant="no"/>
<attribute name="IN_03" value="NO" constant="no"/>
<attribute name="IN_04" value="NO" constant="no"/>
<attribute name="IN_07" value="NO" constant="no"/>
<attribute name="IN_09" value="NO" constant="no"/>
<attribute name="IN_13" value="NPN, 34.5 pulses/rev" constant="no"/>
<attribute name="MPN" value="ehb5200"/>
<attribute name="OUT_02" value="with" constant="no"/>
<attribute name="OUT_06" value="fuelpump" constant="no"/>
<attribute name="OUT_08" value="KL.50F*" constant="no"/>
<attribute name="OUT_10" value="glow" constant="no"/>
<attribute name="OUT_11" value="alarm" constant="no"/>
<attribute name="OUT_12" value="glow" constant="no"/>
<attribute name="OUT_15" value="startalarm" constant="no"/>
<attribute name="OUT_18" value="fuelpump" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="KUBOTA" prefix="DIESEL">
<description>TODO:&lt;BR&gt;
- nazančit číslování 9pin konektoru&lt;BR&gt;
- speed sensor lépe polohu pinů&lt;BR&gt;
- stop selenoid dát z druhé strany</description>
<gates>
<gate name="G$1" symbol="KUBOTA-MOTOR" x="0" y="0" addlevel="must"/>
<gate name="G$2" symbol="KUBOTA-FUELPUMP" x="10.16" y="-30.48"/>
<gate name="G$3" symbol="KUBOTA-SPEED_SENSOR" x="38.1" y="-15.24"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name="">
<attribute name="_EXTERNAL_" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="CMI">
<description>Project library CMI</description>
<packages>
<package name="1X02">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-1.905" y1="1.27" x2="-0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="0" y2="0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="0" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="0.635" x2="-2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="1.27" x2="-2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-0.635" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-1.27" x2="-1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0" y1="0.635" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.905" y2="1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="1.27" x2="2.54" y2="0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="0.635" x2="2.54" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.635" x2="1.905" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-1.27" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="0" y2="-0.635" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="1.1" diameter="1.8" shape="octagon" rot="R90"/>
<pad name="2" x="1.27" y="0" drill="1.1" diameter="1.8" rot="R90"/>
<text x="-2.6162" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.524" y1="-0.254" x2="-1.016" y2="0.254" layer="51"/>
<rectangle x1="1.016" y1="-0.254" x2="1.524" y2="0.254" layer="51"/>
</package>
<package name="EMPTY">
<pad name="1" x="0" y="0" drill="0.8" shape="square"/>
<pad name="2" x="0" y="-1.27" drill="0.8" shape="square"/>
<pad name="3" x="0" y="-2.54" drill="0.8" shape="square"/>
<pad name="4" x="0" y="-3.81" drill="0.8" shape="square"/>
<pad name="5" x="0" y="-5.08" drill="0.8" shape="square"/>
<pad name="6" x="0" y="-6.35" drill="0.8" shape="square"/>
<pad name="7" x="0" y="-7.62" drill="0.8" shape="square"/>
<pad name="8" x="0" y="-8.89" drill="0.8" shape="square"/>
<pad name="9" x="0" y="-10.16" drill="0.8" shape="square"/>
<pad name="10" x="0" y="-11.43" drill="0.8" shape="square"/>
<pad name="11" x="0" y="-12.7" drill="0.8" shape="square"/>
<pad name="12" x="0" y="-13.97" drill="0.8" shape="square"/>
<pad name="13" x="0" y="-15.24" drill="0.8" shape="square"/>
<pad name="14" x="0" y="-16.51" drill="0.8" shape="square"/>
<pad name="15" x="0" y="-17.78" drill="0.8" shape="square"/>
<pad name="16" x="0" y="-19.05" drill="0.8" shape="square"/>
<pad name="17" x="0" y="-20.32" drill="0.8" shape="square"/>
<pad name="18" x="0" y="-21.59" drill="0.8" shape="square"/>
<pad name="19" x="0" y="-22.86" drill="0.8" shape="square"/>
<pad name="20" x="0" y="-24.13" drill="0.8" shape="square"/>
<pad name="21" x="0" y="-25.4" drill="0.8" shape="square"/>
<pad name="22" x="0" y="-26.67" drill="0.8" shape="square"/>
<pad name="23" x="0" y="-27.94" drill="0.8" shape="square"/>
<pad name="24" x="0" y="-29.21" drill="0.8" shape="square"/>
<pad name="25" x="0" y="-30.48" drill="0.8" shape="square"/>
<pad name="26" x="0" y="-31.75" drill="0.8" shape="square"/>
<pad name="27" x="0" y="-33.02" drill="0.8" shape="square"/>
<pad name="28" x="0" y="-34.29" drill="0.8" shape="square"/>
<pad name="29" x="0" y="-35.56" drill="0.8" shape="square"/>
<pad name="30" x="0" y="-36.83" drill="0.8" shape="square"/>
<pad name="31" x="0" y="-38.1" drill="0.8" shape="square"/>
</package>
</packages>
<symbols>
<symbol name="JISTIC_DUMMY">
<wire x1="-2.54" y1="0" x2="3.175" y2="1.016" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-5.08" y2="0" width="0.1524" layer="94"/>
<circle x="-2.54" y="0" radius="0.254" width="0.508" layer="94"/>
<wire x1="5.08" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="0" y="2.54" size="1.778" layer="94" align="bottom-center">&gt;VALUE</text>
<wire x1="1.016" y1="0.635" x2="0.889" y2="1.397" width="0.254" layer="94"/>
<wire x1="2.413" y1="0.889" x2="2.286" y2="1.651" width="0.254" layer="94"/>
<wire x1="0.889" y1="1.397" x2="2.286" y2="1.651" width="0.254" layer="94"/>
</symbol>
<symbol name="UZEL_DUMMY">
<circle x="0" y="0" radius="0.254" width="0.508" layer="94"/>
</symbol>
<symbol name="D_DUMMY">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
</symbol>
<symbol name="SPINAC">
<wire x1="-2.54" y1="0" x2="3.175" y2="1.016" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-3.81" y2="0" width="0.1524" layer="94"/>
<circle x="-2.54" y="0" radius="0.254" width="0.508" layer="94"/>
<pin name="NO" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="CO" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
</symbol>
<symbol name="ZAROVKA_SE_SPINACEM">
<pin name="Z" x="0" y="-10.16" visible="off" length="short" direction="pas" rot="R90"/>
<circle x="0" y="-3.81" radius="2.54" width="0.254" layer="94"/>
<wire x1="-1.778" y1="-5.588" x2="1.778" y2="-2.032" width="0.254" layer="94"/>
<wire x1="1.778" y1="-5.588" x2="-1.778" y2="-2.032" width="0.254" layer="94"/>
<pin name="S" x="0" y="10.16" visible="off" length="middle" direction="pas" rot="R270"/>
<wire x1="-5.08" y1="7.62" x2="5.08" y2="7.62" width="0.254" layer="94" style="shortdash"/>
<wire x1="5.08" y1="7.62" x2="5.08" y2="-7.62" width="0.254" layer="94" style="shortdash"/>
<wire x1="5.08" y1="-7.62" x2="0" y2="-7.62" width="0.254" layer="94" style="shortdash"/>
<wire x1="0" y1="-7.62" x2="-5.08" y2="-7.62" width="0.254" layer="94" style="shortdash"/>
<wire x1="-5.08" y1="-7.62" x2="-5.08" y2="7.62" width="0.254" layer="94" style="shortdash"/>
<wire x1="0" y1="0" x2="-1.016" y2="5.715" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-1.27" width="0.1524" layer="94"/>
<circle x="0" y="0" radius="0.254" width="0.508" layer="94"/>
<wire x1="0" y1="7.62" x2="0" y2="5.08" width="0.1524" layer="94"/>
<wire x1="0" y1="-7.62" x2="0" y2="-6.35" width="0.1524" layer="94"/>
</symbol>
<symbol name="SPINAC_S_ZAROVKOU">
<pin name="F" x="-10.16" y="-2.54" visible="off" length="middle" direction="pas"/>
<circle x="0" y="-2.54" radius="2.54" width="0.254" layer="94"/>
<wire x1="-1.778" y1="-4.318" x2="1.778" y2="-0.762" width="0.254" layer="94"/>
<wire x1="1.778" y1="-4.318" x2="-1.778" y2="-0.762" width="0.254" layer="94"/>
<wire x1="-7.62" y1="7.62" x2="7.62" y2="7.62" width="0.254" layer="94" style="shortdash"/>
<wire x1="7.62" y1="7.62" x2="7.62" y2="-7.62" width="0.254" layer="94" style="shortdash"/>
<wire x1="7.62" y1="-7.62" x2="-7.62" y2="-7.62" width="0.254" layer="94" style="shortdash"/>
<wire x1="-7.62" y1="-7.62" x2="-7.62" y2="7.62" width="0.254" layer="94" style="shortdash"/>
<wire x1="-2.54" y1="2.54" x2="3.175" y2="3.556" width="0.254" layer="94"/>
<wire x1="-2.54" y1="2.54" x2="-5.08" y2="2.54" width="0.1524" layer="94"/>
<circle x="-2.54" y="2.54" radius="0.254" width="0.508" layer="94"/>
<wire x1="5.08" y1="2.54" x2="2.54" y2="2.54" width="0.1524" layer="94"/>
<pin name="A" x="-10.16" y="2.54" visible="off" length="middle" direction="pas"/>
<pin name="B" x="10.16" y="2.54" visible="off" length="middle" direction="pas" rot="R180"/>
<pin name="E" x="10.16" y="-2.54" visible="off" length="middle" direction="pas" rot="R180"/>
<wire x1="-5.08" y1="-2.54" x2="-2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<text x="-6.985" y="2.8575" size="1.778" layer="94">A</text>
<text x="-6.985" y="-2.2225" size="1.778" layer="94">F</text>
<text x="6.985" y="-2.2225" size="1.778" layer="94" align="bottom-right">E</text>
<text x="6.985" y="2.8575" size="1.778" layer="94" align="bottom-right">B</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="JISTIC_DUMMY" uservalue="yes">
<gates>
<gate name="G$1" symbol="JISTIC_DUMMY" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="UZEL_DUMMY">
<gates>
<gate name="G$1" symbol="UZEL_DUMMY" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="D_DUMMY">
<gates>
<gate name="G$1" symbol="D_DUMMY" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SPINAC">
<gates>
<gate name="G$1" symbol="SPINAC" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X02">
<connects>
<connect gate="G$1" pin="CO" pad="1"/>
<connect gate="G$1" pin="NO" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ZAROVKA_SE_SPINACEM">
<gates>
<gate name="G$1" symbol="ZAROVKA_SE_SPINACEM" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X02">
<connects>
<connect gate="G$1" pin="S" pad="1"/>
<connect gate="G$1" pin="Z" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SPINAC_S_ZAROVKOU" prefix="S">
<gates>
<gate name="G$1" symbol="SPINAC_S_ZAROVKOU" x="0" y="0"/>
</gates>
<devices>
<device name="" package="EMPTY">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="B" pad="2"/>
<connect gate="G$1" pin="E" pad="5"/>
<connect gate="G$1" pin="F" pad="6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SUPPLY2">
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.27" y1="2.54" x2="1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="0" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<text x="-1.905" y="-0.635" size="1.524" layer="94">GND</text>
<pin name="GND" x="0" y="5.08" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="+24V">
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="0.635" x2="0" y2="1.905" width="0.1524" layer="94"/>
<circle x="0" y="1.27" radius="1.27" width="0.254" layer="94"/>
<text x="-2.54" y="3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="+24V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="AKU">
<wire x1="-0.635" y1="3.81" x2="0.635" y2="3.81" width="0.1524" layer="94"/>
<wire x1="0" y1="3.175" x2="0" y2="4.445" width="0.1524" layer="94"/>
<circle x="0" y="3.81" radius="1.27" width="0.254" layer="94"/>
<text x="-1.778" y="5.715" size="1.524" layer="94">AKU</text>
<pin name="AKU" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="V--&gt;">
<wire x1="15.24" y1="0" x2="13.97" y2="1.27" width="0.1524" layer="94"/>
<wire x1="13.97" y1="1.27" x2="0" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="1.27" x2="0" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.27" x2="13.97" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="15.24" y1="0" x2="13.97" y2="-1.27" width="0.1524" layer="94"/>
<text x="1.27" y="-0.762" size="1.524" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="+24V@2">
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="0.635" x2="0" y2="1.905" width="0.1524" layer="94"/>
<circle x="0" y="1.27" radius="1.27" width="0.254" layer="94"/>
<text x="-2.54" y="3.175" size="1.524" layer="94">+24V</text>
<pin name="+24V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="+10V">
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="0.635" x2="0" y2="1.905" width="0.1524" layer="94"/>
<circle x="0" y="1.27" radius="1.27" width="0.254" layer="94"/>
<text x="-2.54" y="3.175" size="1.524" layer="94">+10V</text>
<pin name="+10V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND">
<gates>
<gate name="GND" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+24V" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="+24V" symbol="+24V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="AKU">
<gates>
<gate name="G$1" symbol="AKU" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="V--&gt;" prefix="V" uservalue="yes">
<gates>
<gate name="G$1" symbol="V--&gt;" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+24V@2">
<gates>
<gate name="+24V" symbol="+24V@2" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+10V">
<gates>
<gate name="G$1" symbol="+10V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Svorkovnice-3patra">
<packages>
</packages>
<symbols>
<symbol name="3SVORKA-00">
<circle x="2.54" y="-7.62" radius="0.762" width="0.1524" layer="94"/>
<circle x="0" y="-5.08" radius="0.762" width="0.1524" layer="94"/>
<circle x="-2.54" y="-2.54" radius="0.762" width="0.1524" layer="94"/>
<circle x="2.54" y="7.62" radius="0.762" width="0.1524" layer="94"/>
<circle x="0" y="5.08" radius="0.762" width="0.1524" layer="94"/>
<circle x="-2.54" y="2.54" radius="0.762" width="0.1524" layer="94"/>
<pin name="1" x="2.54" y="10.16" visible="off" length="point" direction="pas" rot="R270"/>
<pin name="2" x="0" y="7.62" visible="off" length="point" direction="pas" rot="R270"/>
<pin name="3" x="-2.54" y="5.08" visible="off" length="point" direction="pas" rot="R270"/>
<pin name="4" x="-2.54" y="-5.08" visible="off" length="point" direction="pas" rot="R90"/>
<pin name="5" x="0" y="-7.62" visible="off" length="point" direction="pas" rot="R90"/>
<pin name="6" x="2.54" y="-10.16" visible="off" length="point" direction="pas" rot="R90"/>
<wire x1="2.54" y1="10.16" x2="2.54" y2="8.382" width="0.1524" layer="94"/>
<wire x1="0" y1="7.62" x2="0" y2="5.842" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="3.302" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-10.16" x2="2.54" y2="-8.382" width="0.1524" layer="94"/>
<wire x1="0" y1="-7.62" x2="0" y2="-5.842" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-2.54" y2="-3.302" width="0.1524" layer="94"/>
<text x="2.54" y="7.62" size="1.016" layer="94" rot="MR270" align="center">1</text>
<text x="0" y="5.08" size="1.016" layer="94" rot="MR270" align="center">2</text>
<text x="-2.54" y="2.54" size="1.016" layer="94" rot="MR270" align="center">3</text>
<text x="-2.54" y="-2.54" size="1.016" layer="94" rot="MR270" align="center">4</text>
<text x="0" y="-5.08" size="1.016" layer="94" rot="MR270" align="center">5</text>
<text x="2.54" y="-7.62" size="1.016" layer="94" rot="MR270" align="center">6</text>
<wire x1="-3.81" y1="3.81" x2="-1.27" y2="4.445" width="0.3048" layer="94"/>
<wire x1="-1.27" y1="4.445" x2="-1.27" y2="6.35" width="0.3048" layer="94"/>
<wire x1="-1.27" y1="6.35" x2="1.27" y2="6.985" width="0.3048" layer="94"/>
<wire x1="1.27" y1="6.985" x2="1.27" y2="8.89" width="0.3048" layer="94"/>
<wire x1="1.27" y1="8.89" x2="3.81" y2="9.525" width="0.3048" layer="94"/>
<wire x1="3.81" y1="9.525" x2="3.81" y2="-9.525" width="0.3048" layer="94"/>
<wire x1="3.81" y1="-9.525" x2="1.27" y2="-8.89" width="0.3048" layer="94"/>
<wire x1="1.27" y1="-8.89" x2="1.27" y2="-6.985" width="0.3048" layer="94"/>
<wire x1="1.27" y1="-6.985" x2="-1.27" y2="-6.35" width="0.3048" layer="94"/>
<wire x1="-1.27" y1="-6.35" x2="-1.27" y2="-4.445" width="0.3048" layer="94"/>
<wire x1="-1.27" y1="-4.445" x2="-3.81" y2="-3.81" width="0.3048" layer="94"/>
<wire x1="-3.81" y1="-3.81" x2="-3.81" y2="3.81" width="0.3048" layer="94"/>
<text x="2.286" y="0" size="1.778" layer="96" rot="R90" align="center">&gt;VALUE</text>
<wire x1="-2.54" y1="-1.778" x2="-2.54" y2="1.778" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-4.318" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="4.318" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="1.27" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-6.858" x2="2.54" y2="-3.175" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-3.175" x2="0" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="0" y1="-0.635" x2="0" y2="0.635" width="0.1524" layer="94"/>
<wire x1="0" y1="0.635" x2="2.54" y2="3.175" width="0.1524" layer="94"/>
<wire x1="2.54" y1="3.175" x2="2.54" y2="6.858" width="0.1524" layer="94"/>
</symbol>
<symbol name="3SVORKA-01">
<circle x="2.54" y="-7.62" radius="0.762" width="0.1524" layer="94"/>
<circle x="0" y="-5.08" radius="0.762" width="0.1524" layer="94"/>
<circle x="-2.54" y="-2.54" radius="0.762" width="0.1524" layer="94"/>
<circle x="2.54" y="7.62" radius="0.762" width="0.1524" layer="94"/>
<circle x="0" y="5.08" radius="0.762" width="0.1524" layer="94"/>
<circle x="-2.54" y="2.54" radius="0.762" width="0.1524" layer="94"/>
<pin name="1" x="2.54" y="10.16" visible="off" length="point" direction="pas" rot="R270"/>
<pin name="2" x="0" y="7.62" visible="off" length="point" direction="pas" rot="R270"/>
<pin name="3" x="-2.54" y="5.08" visible="off" length="point" direction="pas" rot="R270"/>
<pin name="4" x="-2.54" y="-5.08" visible="off" length="point" direction="pas" rot="R90"/>
<pin name="5" x="0" y="-7.62" visible="off" length="point" direction="pas" rot="R90"/>
<pin name="6" x="2.54" y="-10.16" visible="off" length="point" direction="pas" rot="R90"/>
<wire x1="2.54" y1="10.16" x2="2.54" y2="8.382" width="0.1524" layer="94"/>
<wire x1="0" y1="7.62" x2="0" y2="5.842" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="3.302" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-10.16" x2="2.54" y2="-8.382" width="0.1524" layer="94"/>
<wire x1="0" y1="-7.62" x2="0" y2="-5.842" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-2.54" y2="-3.302" width="0.1524" layer="94"/>
<text x="2.54" y="7.62" size="1.016" layer="94" rot="MR270" align="center">1</text>
<text x="0" y="5.08" size="1.016" layer="94" rot="MR270" align="center">2</text>
<text x="-2.54" y="2.54" size="1.016" layer="94" rot="MR270" align="center">3</text>
<text x="-2.54" y="-2.54" size="1.016" layer="94" rot="MR270" align="center">4</text>
<text x="0" y="-5.08" size="1.016" layer="94" rot="MR270" align="center">5</text>
<text x="2.54" y="-7.62" size="1.016" layer="94" rot="MR270" align="center">6</text>
<wire x1="-3.81" y1="3.81" x2="-1.27" y2="4.445" width="0.3048" layer="94"/>
<wire x1="-1.27" y1="4.445" x2="-1.27" y2="6.35" width="0.3048" layer="94"/>
<wire x1="-1.27" y1="6.35" x2="1.27" y2="6.985" width="0.3048" layer="94"/>
<wire x1="1.27" y1="6.985" x2="1.27" y2="8.89" width="0.3048" layer="94"/>
<wire x1="1.27" y1="8.89" x2="3.81" y2="9.525" width="0.3048" layer="94"/>
<wire x1="3.81" y1="9.525" x2="3.81" y2="-9.525" width="0.3048" layer="94"/>
<wire x1="3.81" y1="-9.525" x2="1.27" y2="-8.89" width="0.3048" layer="94"/>
<wire x1="1.27" y1="-8.89" x2="1.27" y2="-6.985" width="0.3048" layer="94"/>
<wire x1="1.27" y1="-6.985" x2="-1.27" y2="-6.35" width="0.3048" layer="94"/>
<wire x1="-1.27" y1="-6.35" x2="-1.27" y2="-4.445" width="0.3048" layer="94"/>
<wire x1="-1.27" y1="-4.445" x2="-3.81" y2="-3.81" width="0.3048" layer="94"/>
<wire x1="-3.81" y1="-3.81" x2="-3.81" y2="3.81" width="0.3048" layer="94"/>
<text x="2.286" y="0" size="1.778" layer="96" rot="R90" align="center">&gt;VALUE</text>
<wire x1="-2.54" y1="-1.778" x2="-2.54" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-0.635" x2="-2.54" y2="1.778" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-4.318" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="4.318" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="1.27" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="-0.635" x2="-1.27" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-6.858" x2="2.54" y2="-3.175" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-3.175" x2="0" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="0" y1="-0.635" x2="0" y2="0.635" width="0.1524" layer="94"/>
<wire x1="0" y1="0.635" x2="2.54" y2="3.175" width="0.1524" layer="94"/>
<wire x1="2.54" y1="3.175" x2="2.54" y2="6.858" width="0.1524" layer="94"/>
<wire x1="0" y1="0.635" x2="-1.27" y2="0.635" width="0.3048" layer="94"/>
<circle x="-1.27" y="0.635" radius="0.4064" width="0" layer="94"/>
<circle x="0" y="0.635" radius="0.4064" width="0" layer="94"/>
</symbol>
<symbol name="3SVORKA-11">
<circle x="2.54" y="-7.62" radius="0.762" width="0.1524" layer="94"/>
<circle x="0" y="-5.08" radius="0.762" width="0.1524" layer="94"/>
<circle x="-2.54" y="-2.54" radius="0.762" width="0.1524" layer="94"/>
<circle x="2.54" y="7.62" radius="0.762" width="0.1524" layer="94"/>
<circle x="0" y="5.08" radius="0.762" width="0.1524" layer="94"/>
<circle x="-2.54" y="2.54" radius="0.762" width="0.1524" layer="94"/>
<pin name="1" x="2.54" y="10.16" visible="off" length="point" direction="pas" rot="R270"/>
<pin name="2" x="0" y="7.62" visible="off" length="point" direction="pas" rot="R270"/>
<pin name="3" x="-2.54" y="5.08" visible="off" length="point" direction="pas" rot="R270"/>
<pin name="4" x="-2.54" y="-5.08" visible="off" length="point" direction="pas" rot="R90"/>
<pin name="5" x="0" y="-7.62" visible="off" length="point" direction="pas" rot="R90"/>
<pin name="6" x="2.54" y="-10.16" visible="off" length="point" direction="pas" rot="R90"/>
<wire x1="2.54" y1="10.16" x2="2.54" y2="8.382" width="0.1524" layer="94"/>
<wire x1="0" y1="7.62" x2="0" y2="5.842" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="3.302" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-10.16" x2="2.54" y2="-8.382" width="0.1524" layer="94"/>
<wire x1="0" y1="-7.62" x2="0" y2="-5.842" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-2.54" y2="-3.302" width="0.1524" layer="94"/>
<text x="2.54" y="7.62" size="1.016" layer="94" rot="MR270" align="center">1</text>
<text x="0" y="5.08" size="1.016" layer="94" rot="MR270" align="center">2</text>
<text x="-2.54" y="2.54" size="1.016" layer="94" rot="MR270" align="center">3</text>
<text x="-2.54" y="-2.54" size="1.016" layer="94" rot="MR270" align="center">4</text>
<text x="0" y="-5.08" size="1.016" layer="94" rot="MR270" align="center">5</text>
<text x="2.54" y="-7.62" size="1.016" layer="94" rot="MR270" align="center">6</text>
<wire x1="-3.81" y1="3.81" x2="-1.27" y2="4.445" width="0.3048" layer="94"/>
<wire x1="-1.27" y1="4.445" x2="-1.27" y2="6.35" width="0.3048" layer="94"/>
<wire x1="-1.27" y1="6.35" x2="1.27" y2="6.985" width="0.3048" layer="94"/>
<wire x1="1.27" y1="6.985" x2="1.27" y2="8.89" width="0.3048" layer="94"/>
<wire x1="1.27" y1="8.89" x2="3.81" y2="9.525" width="0.3048" layer="94"/>
<wire x1="3.81" y1="9.525" x2="3.81" y2="-9.525" width="0.3048" layer="94"/>
<wire x1="3.81" y1="-9.525" x2="1.27" y2="-8.89" width="0.3048" layer="94"/>
<wire x1="1.27" y1="-8.89" x2="1.27" y2="-6.985" width="0.3048" layer="94"/>
<wire x1="1.27" y1="-6.985" x2="-1.27" y2="-6.35" width="0.3048" layer="94"/>
<wire x1="-1.27" y1="-6.35" x2="-1.27" y2="-4.445" width="0.3048" layer="94"/>
<wire x1="-1.27" y1="-4.445" x2="-3.81" y2="-3.81" width="0.3048" layer="94"/>
<wire x1="-3.81" y1="-3.81" x2="-3.81" y2="3.81" width="0.3048" layer="94"/>
<text x="2.286" y="0" size="1.778" layer="96" rot="R90" align="center">&gt;VALUE</text>
<wire x1="-2.54" y1="-1.778" x2="-2.54" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-0.635" x2="-2.54" y2="1.778" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-4.318" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="4.318" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="1.27" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="-0.635" x2="-1.27" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-6.858" x2="2.54" y2="-3.175" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-3.175" x2="0" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="0" y1="-0.635" x2="0" y2="0.635" width="0.1524" layer="94"/>
<wire x1="0" y1="0.635" x2="2.54" y2="3.175" width="0.1524" layer="94"/>
<wire x1="2.54" y1="3.175" x2="2.54" y2="6.858" width="0.1524" layer="94"/>
<wire x1="0" y1="0.635" x2="-1.27" y2="0.635" width="0.3048" layer="94"/>
<wire x1="-1.27" y1="-0.635" x2="-2.54" y2="-0.635" width="0.3048" layer="94"/>
<circle x="-1.27" y="-0.635" radius="0.4064" width="0" layer="94"/>
<circle x="-2.54" y="-0.635" radius="0.4064" width="0" layer="94"/>
<circle x="-1.27" y="0.635" radius="0.4064" width="0" layer="94"/>
<circle x="0" y="0.635" radius="0.4064" width="0" layer="94"/>
</symbol>
<symbol name="3SVORKA-10">
<circle x="2.54" y="-7.62" radius="0.762" width="0.1524" layer="94"/>
<circle x="0" y="-5.08" radius="0.762" width="0.1524" layer="94"/>
<circle x="-2.54" y="-2.54" radius="0.762" width="0.1524" layer="94"/>
<circle x="2.54" y="7.62" radius="0.762" width="0.1524" layer="94"/>
<circle x="0" y="5.08" radius="0.762" width="0.1524" layer="94"/>
<circle x="-2.54" y="2.54" radius="0.762" width="0.1524" layer="94"/>
<pin name="1" x="2.54" y="10.16" visible="off" length="point" direction="pas" rot="R270"/>
<pin name="2" x="0" y="7.62" visible="off" length="point" direction="pas" rot="R270"/>
<pin name="3" x="-2.54" y="5.08" visible="off" length="point" direction="pas" rot="R270"/>
<pin name="4" x="-2.54" y="-5.08" visible="off" length="point" direction="pas" rot="R90"/>
<pin name="5" x="0" y="-7.62" visible="off" length="point" direction="pas" rot="R90"/>
<pin name="6" x="2.54" y="-10.16" visible="off" length="point" direction="pas" rot="R90"/>
<wire x1="2.54" y1="10.16" x2="2.54" y2="8.382" width="0.1524" layer="94"/>
<wire x1="0" y1="7.62" x2="0" y2="5.842" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="3.302" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-10.16" x2="2.54" y2="-8.382" width="0.1524" layer="94"/>
<wire x1="0" y1="-7.62" x2="0" y2="-5.842" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-2.54" y2="-3.302" width="0.1524" layer="94"/>
<text x="2.54" y="7.62" size="1.016" layer="94" rot="MR270" align="center">1</text>
<text x="0" y="5.08" size="1.016" layer="94" rot="MR270" align="center">2</text>
<text x="-2.54" y="2.54" size="1.016" layer="94" rot="MR270" align="center">3</text>
<text x="-2.54" y="-2.54" size="1.016" layer="94" rot="MR270" align="center">4</text>
<text x="0" y="-5.08" size="1.016" layer="94" rot="MR270" align="center">5</text>
<text x="2.54" y="-7.62" size="1.016" layer="94" rot="MR270" align="center">6</text>
<wire x1="-3.81" y1="3.81" x2="-1.27" y2="4.445" width="0.3048" layer="94"/>
<wire x1="-1.27" y1="4.445" x2="-1.27" y2="6.35" width="0.3048" layer="94"/>
<wire x1="-1.27" y1="6.35" x2="1.27" y2="6.985" width="0.3048" layer="94"/>
<wire x1="1.27" y1="6.985" x2="1.27" y2="8.89" width="0.3048" layer="94"/>
<wire x1="1.27" y1="8.89" x2="3.81" y2="9.525" width="0.3048" layer="94"/>
<wire x1="3.81" y1="9.525" x2="3.81" y2="-9.525" width="0.3048" layer="94"/>
<wire x1="3.81" y1="-9.525" x2="1.27" y2="-8.89" width="0.3048" layer="94"/>
<wire x1="1.27" y1="-8.89" x2="1.27" y2="-6.985" width="0.3048" layer="94"/>
<wire x1="1.27" y1="-6.985" x2="-1.27" y2="-6.35" width="0.3048" layer="94"/>
<wire x1="-1.27" y1="-6.35" x2="-1.27" y2="-4.445" width="0.3048" layer="94"/>
<wire x1="-1.27" y1="-4.445" x2="-3.81" y2="-3.81" width="0.3048" layer="94"/>
<wire x1="-3.81" y1="-3.81" x2="-3.81" y2="3.81" width="0.3048" layer="94"/>
<text x="2.286" y="0" size="1.778" layer="96" rot="R90" align="center">&gt;VALUE</text>
<wire x1="-2.54" y1="-1.778" x2="-2.54" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-0.635" x2="-2.54" y2="1.778" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-4.318" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="4.318" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="1.27" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="-0.635" x2="-1.27" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-6.858" x2="2.54" y2="-3.175" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-3.175" x2="0" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="0" y1="-0.635" x2="0" y2="0.635" width="0.1524" layer="94"/>
<wire x1="0" y1="0.635" x2="2.54" y2="3.175" width="0.1524" layer="94"/>
<wire x1="2.54" y1="3.175" x2="2.54" y2="6.858" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="-0.635" x2="-2.54" y2="-0.635" width="0.3048" layer="94"/>
<circle x="-1.27" y="-0.635" radius="0.4064" width="0" layer="94"/>
<circle x="-2.54" y="-0.635" radius="0.4064" width="0" layer="94"/>
</symbol>
<symbol name="3SVORKA-11-BLUE">
<circle x="2.54" y="-7.62" radius="0.762" width="0.1524" layer="94"/>
<circle x="0" y="-5.08" radius="0.762" width="0.1524" layer="94"/>
<circle x="-2.54" y="-2.54" radius="0.762" width="0.1524" layer="94"/>
<circle x="2.54" y="7.62" radius="0.762" width="0.1524" layer="94"/>
<circle x="0" y="5.08" radius="0.762" width="0.1524" layer="94"/>
<circle x="-2.54" y="2.54" radius="0.762" width="0.1524" layer="94"/>
<pin name="1" x="2.54" y="10.16" visible="off" length="point" direction="pas" rot="R270"/>
<pin name="2" x="0" y="7.62" visible="off" length="point" direction="pas" rot="R270"/>
<pin name="3" x="-2.54" y="5.08" visible="off" length="point" direction="pas" rot="R270"/>
<pin name="4" x="-2.54" y="-5.08" visible="off" length="point" direction="pas" rot="R90"/>
<pin name="5" x="0" y="-7.62" visible="off" length="point" direction="pas" rot="R90"/>
<pin name="6" x="2.54" y="-10.16" visible="off" length="point" direction="pas" rot="R90"/>
<wire x1="2.54" y1="10.16" x2="2.54" y2="8.382" width="0.1524" layer="94"/>
<wire x1="0" y1="7.62" x2="0" y2="5.842" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="3.302" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-10.16" x2="2.54" y2="-8.382" width="0.1524" layer="94"/>
<wire x1="0" y1="-7.62" x2="0" y2="-5.842" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-2.54" y2="-3.302" width="0.1524" layer="94"/>
<text x="2.54" y="7.62" size="1.016" layer="94" rot="MR270" align="center">1</text>
<text x="0" y="5.08" size="1.016" layer="94" rot="MR270" align="center">2</text>
<text x="-2.54" y="2.54" size="1.016" layer="94" rot="MR270" align="center">3</text>
<text x="-2.54" y="-2.54" size="1.016" layer="94" rot="MR270" align="center">4</text>
<text x="0" y="-5.08" size="1.016" layer="94" rot="MR270" align="center">5</text>
<text x="2.54" y="-7.62" size="1.016" layer="94" rot="MR270" align="center">6</text>
<wire x1="-3.81" y1="3.81" x2="-1.27" y2="4.445" width="0.3048" layer="94"/>
<wire x1="-1.27" y1="4.445" x2="-1.27" y2="6.35" width="0.3048" layer="94"/>
<wire x1="-1.27" y1="6.35" x2="1.27" y2="6.985" width="0.3048" layer="94"/>
<wire x1="1.27" y1="6.985" x2="1.27" y2="8.89" width="0.3048" layer="94"/>
<wire x1="1.27" y1="8.89" x2="3.81" y2="9.525" width="0.3048" layer="94"/>
<wire x1="3.81" y1="9.525" x2="3.81" y2="-9.525" width="0.3048" layer="94"/>
<wire x1="3.81" y1="-9.525" x2="1.27" y2="-8.89" width="0.3048" layer="94"/>
<wire x1="1.27" y1="-8.89" x2="1.27" y2="-6.985" width="0.3048" layer="94"/>
<wire x1="1.27" y1="-6.985" x2="-1.27" y2="-6.35" width="0.3048" layer="94"/>
<wire x1="-1.27" y1="-6.35" x2="-1.27" y2="-4.445" width="0.3048" layer="94"/>
<wire x1="-1.27" y1="-4.445" x2="-3.81" y2="-3.81" width="0.3048" layer="94"/>
<wire x1="-3.81" y1="-3.81" x2="-3.81" y2="3.81" width="0.3048" layer="94"/>
<text x="2.286" y="0" size="1.778" layer="96" rot="R90" align="center">&gt;VALUE</text>
<wire x1="-2.54" y1="-1.778" x2="-2.54" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-0.635" x2="-2.54" y2="1.778" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-4.318" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="4.318" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="1.27" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="-0.635" x2="-1.27" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-6.858" x2="2.54" y2="-3.175" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-3.175" x2="0" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="0" y1="-0.635" x2="0" y2="0.635" width="0.1524" layer="94"/>
<wire x1="0" y1="0.635" x2="2.54" y2="3.175" width="0.1524" layer="94"/>
<wire x1="2.54" y1="3.175" x2="2.54" y2="6.858" width="0.1524" layer="94"/>
<wire x1="0" y1="0.635" x2="-1.27" y2="0.635" width="0.3048" layer="94"/>
<wire x1="-1.27" y1="-0.635" x2="-2.54" y2="-0.635" width="0.3048" layer="94"/>
<circle x="-1.27" y="-0.635" radius="0.4064" width="0" layer="94"/>
<circle x="-2.54" y="-0.635" radius="0.4064" width="0" layer="94"/>
<circle x="-1.27" y="0.635" radius="0.4064" width="0" layer="94"/>
<circle x="0" y="0.635" radius="0.4064" width="0" layer="94"/>
<text x="2.2225" y="-3.175" size="1.016" layer="94" rot="R90" align="bottom-right">BLUE</text>
</symbol>
<symbol name="3SVORKA-00-TOP">
<circle x="-2.54" y="-2.54" radius="0.762" width="0.1524" layer="94"/>
<circle x="-2.54" y="2.54" radius="0.762" width="0.1524" layer="94"/>
<pin name="3" x="-2.54" y="5.08" visible="off" length="point" direction="pas" rot="R270"/>
<pin name="4" x="-2.54" y="-5.08" visible="off" length="point" direction="pas" rot="R90"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="3.302" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-2.54" y2="-3.302" width="0.1524" layer="94"/>
<text x="-2.54" y="2.54" size="1.016" layer="94" rot="MR270" align="center">3</text>
<text x="-2.54" y="-2.54" size="1.016" layer="94" rot="MR270" align="center">4</text>
<wire x1="-3.81" y1="3.81" x2="-1.27" y2="4.445" width="0.3048" layer="94"/>
<wire x1="-1.27" y1="-6.35" x2="-1.27" y2="-4.445" width="0.3048" layer="94"/>
<wire x1="-1.27" y1="-4.445" x2="-3.81" y2="-3.81" width="0.3048" layer="94"/>
<wire x1="-3.81" y1="-3.81" x2="-3.81" y2="3.81" width="0.3048" layer="94"/>
<text x="2.286" y="0" size="1.778" layer="96" rot="R90" align="center">&gt;VALUE</text>
<wire x1="-2.54" y1="-1.778" x2="-2.54" y2="1.778" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="6.35" x2="-1.27" y2="4.445" width="0.3048" layer="94"/>
</symbol>
<symbol name="3SVORKA-00-MID">
<circle x="0" y="-5.08" radius="0.762" width="0.1524" layer="94"/>
<circle x="0" y="5.08" radius="0.762" width="0.1524" layer="94"/>
<pin name="2" x="0" y="7.62" visible="off" length="point" direction="pas" rot="R270"/>
<pin name="5" x="0" y="-7.62" visible="off" length="point" direction="pas" rot="R90"/>
<wire x1="0" y1="7.62" x2="0" y2="5.842" width="0.1524" layer="94"/>
<wire x1="0" y1="-7.62" x2="0" y2="-5.842" width="0.1524" layer="94"/>
<text x="0" y="5.08" size="1.016" layer="94" rot="MR270" align="center">2</text>
<text x="0" y="-5.08" size="1.016" layer="94" rot="MR270" align="center">5</text>
<wire x1="-1.27" y1="4.445" x2="-1.27" y2="6.35" width="0.3048" layer="94"/>
<wire x1="-1.27" y1="6.35" x2="1.27" y2="6.985" width="0.3048" layer="94"/>
<wire x1="1.27" y1="6.985" x2="1.27" y2="8.89" width="0.3048" layer="94"/>
<wire x1="1.27" y1="-8.89" x2="1.27" y2="-6.985" width="0.3048" layer="94"/>
<wire x1="1.27" y1="-6.985" x2="-1.27" y2="-6.35" width="0.3048" layer="94"/>
<wire x1="-1.27" y1="-6.35" x2="-1.27" y2="-4.445" width="0.3048" layer="94"/>
<text x="2.286" y="0" size="1.778" layer="96" rot="R90" align="center">&gt;VALUE</text>
<wire x1="0" y1="-2.54" x2="0" y2="-4.318" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="4.318" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="1.27" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.1524" layer="94"/>
</symbol>
<symbol name="3SVORKA-00-BOT">
<circle x="2.54" y="-7.62" radius="0.762" width="0.1524" layer="94"/>
<circle x="2.54" y="7.62" radius="0.762" width="0.1524" layer="94"/>
<pin name="1" x="2.54" y="10.16" visible="off" length="point" direction="pas" rot="R270"/>
<pin name="6" x="2.54" y="-10.16" visible="off" length="point" direction="pas" rot="R90"/>
<wire x1="2.54" y1="10.16" x2="2.54" y2="8.382" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-10.16" x2="2.54" y2="-8.382" width="0.1524" layer="94"/>
<text x="2.54" y="7.62" size="1.016" layer="94" rot="MR270" align="center">1</text>
<text x="2.54" y="-7.62" size="1.016" layer="94" rot="MR270" align="center">6</text>
<wire x1="1.27" y1="6.985" x2="1.27" y2="8.89" width="0.3048" layer="94"/>
<wire x1="1.27" y1="8.89" x2="3.81" y2="9.525" width="0.3048" layer="94"/>
<wire x1="3.81" y1="9.525" x2="3.81" y2="-9.525" width="0.3048" layer="94"/>
<wire x1="3.81" y1="-9.525" x2="1.27" y2="-8.89" width="0.3048" layer="94"/>
<wire x1="1.27" y1="-8.89" x2="1.27" y2="-6.985" width="0.3048" layer="94"/>
<text x="2.286" y="0" size="1.778" layer="96" rot="R90" align="center">&gt;VALUE</text>
<wire x1="2.54" y1="-6.858" x2="2.54" y2="-3.175" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-3.175" x2="0" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="0" y1="-0.635" x2="0" y2="0.635" width="0.1524" layer="94"/>
<wire x1="0" y1="0.635" x2="2.54" y2="3.175" width="0.1524" layer="94"/>
<wire x1="2.54" y1="3.175" x2="2.54" y2="6.858" width="0.1524" layer="94"/>
</symbol>
<symbol name="3SVORKA-00-SHADOW">
<circle x="2.54" y="-7.62" radius="0.762" width="0.1524" layer="97"/>
<circle x="0" y="-5.08" radius="0.762" width="0.1524" layer="97"/>
<circle x="-2.54" y="-2.54" radius="0.762" width="0.1524" layer="97"/>
<circle x="2.54" y="7.62" radius="0.762" width="0.1524" layer="97"/>
<circle x="0" y="5.08" radius="0.762" width="0.1524" layer="97"/>
<circle x="-2.54" y="2.54" radius="0.762" width="0.1524" layer="97"/>
<text x="2.54" y="7.62" size="1.016" layer="97" rot="MR270" align="center">1</text>
<text x="0" y="5.08" size="1.016" layer="97" rot="MR270" align="center">2</text>
<text x="-2.54" y="2.54" size="1.016" layer="97" rot="MR270" align="center">3</text>
<text x="-2.54" y="-2.54" size="1.016" layer="97" rot="MR270" align="center">4</text>
<text x="0" y="-5.08" size="1.016" layer="97" rot="MR270" align="center">5</text>
<text x="2.54" y="-7.62" size="1.016" layer="97" rot="MR270" align="center">6</text>
<wire x1="-3.81" y1="3.81" x2="-1.27" y2="4.445" width="0.3048" layer="97" style="shortdash"/>
<wire x1="-1.27" y1="4.445" x2="-1.27" y2="6.35" width="0.3048" layer="97" style="shortdash"/>
<wire x1="-1.27" y1="6.35" x2="1.27" y2="6.985" width="0.3048" layer="97" style="shortdash"/>
<wire x1="1.27" y1="6.985" x2="1.27" y2="8.89" width="0.3048" layer="97" style="shortdash"/>
<wire x1="1.27" y1="8.89" x2="3.81" y2="9.525" width="0.3048" layer="97" style="shortdash"/>
<wire x1="3.81" y1="9.525" x2="3.81" y2="-9.525" width="0.3048" layer="97" style="shortdash"/>
<wire x1="3.81" y1="-9.525" x2="1.27" y2="-8.89" width="0.3048" layer="97" style="shortdash"/>
<wire x1="1.27" y1="-8.89" x2="1.27" y2="-6.985" width="0.3048" layer="97" style="shortdash"/>
<wire x1="1.27" y1="-6.985" x2="-1.27" y2="-6.35" width="0.3048" layer="97" style="shortdash"/>
<wire x1="-1.27" y1="-6.35" x2="-1.27" y2="-4.445" width="0.3048" layer="97" style="shortdash"/>
<wire x1="-1.27" y1="-4.445" x2="-3.81" y2="-3.81" width="0.3048" layer="97" style="shortdash"/>
<wire x1="-3.81" y1="-3.81" x2="-3.81" y2="3.81" width="0.3048" layer="97" style="shortdash"/>
<text x="2.286" y="0" size="1.778" layer="96" rot="R90" align="center">&gt;VALUE</text>
</symbol>
<symbol name="3SVORKA-00-SHADOW-XREF">
<circle x="2.54" y="-7.62" radius="0.762" width="0.1524" layer="97"/>
<circle x="0" y="-5.08" radius="0.762" width="0.1524" layer="97"/>
<circle x="-2.54" y="-2.54" radius="0.762" width="0.1524" layer="97"/>
<circle x="2.54" y="7.62" radius="0.762" width="0.1524" layer="97"/>
<circle x="0" y="5.08" radius="0.762" width="0.1524" layer="97"/>
<circle x="-2.54" y="2.54" radius="0.762" width="0.1524" layer="97"/>
<text x="2.54" y="7.62" size="1.016" layer="97" rot="MR270" align="center">1</text>
<text x="0" y="5.08" size="1.016" layer="97" rot="MR270" align="center">2</text>
<text x="-2.54" y="2.54" size="1.016" layer="97" rot="MR270" align="center">3</text>
<text x="-2.54" y="-2.54" size="1.016" layer="97" rot="MR270" align="center">4</text>
<text x="0" y="-5.08" size="1.016" layer="97" rot="MR270" align="center">5</text>
<text x="2.54" y="-7.62" size="1.016" layer="97" rot="MR270" align="center">6</text>
<wire x1="-3.81" y1="3.81" x2="-1.27" y2="4.445" width="0.3048" layer="97" style="shortdash"/>
<wire x1="-1.27" y1="4.445" x2="-1.27" y2="6.35" width="0.3048" layer="97" style="shortdash"/>
<wire x1="-1.27" y1="6.35" x2="1.27" y2="6.985" width="0.3048" layer="97" style="shortdash"/>
<wire x1="1.27" y1="6.985" x2="1.27" y2="8.89" width="0.3048" layer="97" style="shortdash"/>
<wire x1="1.27" y1="8.89" x2="3.81" y2="9.525" width="0.3048" layer="97" style="shortdash"/>
<wire x1="3.81" y1="9.525" x2="3.81" y2="-9.525" width="0.3048" layer="97" style="shortdash"/>
<wire x1="3.81" y1="-9.525" x2="1.27" y2="-8.89" width="0.3048" layer="97" style="shortdash"/>
<wire x1="1.27" y1="-8.89" x2="1.27" y2="-6.985" width="0.3048" layer="97" style="shortdash"/>
<wire x1="1.27" y1="-6.985" x2="-1.27" y2="-6.35" width="0.3048" layer="97" style="shortdash"/>
<wire x1="-1.27" y1="-6.35" x2="-1.27" y2="-4.445" width="0.3048" layer="97" style="shortdash"/>
<wire x1="-1.27" y1="-4.445" x2="-3.81" y2="-3.81" width="0.3048" layer="97" style="shortdash"/>
<wire x1="-3.81" y1="-3.81" x2="-3.81" y2="3.81" width="0.3048" layer="97" style="shortdash"/>
<text x="2.286" y="0" size="1.778" layer="96" rot="R90" align="center">&gt;VALUE</text>
<text x="0" y="26.67" size="1.778" layer="94" rot="R90">&gt;XREF</text>
<text x="0" y="25.4" size="1.778" layer="94" rot="R90" align="bottom-right">Použito na listu:</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="3SVORKA-00" prefix="SVORKA" uservalue="yes">
<description>3patrová svorkovnice - bez propojů&lt;P&gt;

Svorka řadová &lt;B&gt;ZDLD 2.5-2VN šedá&lt;/B&gt;</description>
<gates>
<gate name="G$1" symbol="3SVORKA-00" x="0" y="0" addlevel="must"/>
<gate name="G$2" symbol="3SVORKA-00-SHADOW-XREF" x="17.78" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name="">
<attribute name="ABRA" value="MELE1616"/>
<attribute name="MPN" value="1782320000"/>
<attribute name="_EXTERNAL_" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="3SVORKA-01" prefix="SVORKA" uservalue="yes">
<description>3patrová svorkovnice - střední propojena se spodní&lt;P&gt;

Svorka řadová &lt;B&gt;ZDLD 2.5-2VN šedá&lt;/B&gt;</description>
<gates>
<gate name="G$1" symbol="3SVORKA-01" x="0" y="0" addlevel="must"/>
<gate name="G$2" symbol="3SVORKA-00-SHADOW-XREF" x="17.78" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name="">
<attribute name="ABRA" value="MELE1616"/>
<attribute name="MPN" value="1782320000"/>
<attribute name="_EXTERNAL_" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="3SVORKA-11" prefix="SVORKA" uservalue="yes">
<description>3patrová svorkovnice - vše propojeno&lt;P&gt;

Svorka řadová &lt;B&gt;ZDLD 2.5-2VN šedá&lt;/B&gt;</description>
<gates>
<gate name="G$1" symbol="3SVORKA-11" x="0" y="0" addlevel="must"/>
<gate name="G$2" symbol="3SVORKA-00-SHADOW-XREF" x="17.78" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name="">
<attribute name="ABRA" value="MELE1616"/>
<attribute name="MPN" value="1782320000"/>
<attribute name="_EXTERNAL_" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="3SVORKA-10" prefix="SVORKA" uservalue="yes">
<description>3patrová svorkovnice - střední propojena s vrchní&lt;P&gt;

Svorka řadová &lt;B&gt;ZDLD 2.5-2VN šedá&lt;/B&gt;</description>
<gates>
<gate name="G$1" symbol="3SVORKA-10" x="0" y="0" addlevel="must"/>
<gate name="G$2" symbol="3SVORKA-00-SHADOW-XREF" x="17.78" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name="">
<attribute name="ABRA" value="MELE1616"/>
<attribute name="MPN" value="1782320000"/>
<attribute name="_EXTERNAL_" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="3SVORKA-11-BLUE" prefix="SVORKA" uservalue="yes">
<description>3patrová svorkovnice - vše propojeno&lt;P&gt;

Svorka řadová &lt;B&gt;ZDLD 2.5-2VN modrá&lt;/B&gt;</description>
<gates>
<gate name="G$1" symbol="3SVORKA-11-BLUE" x="0" y="0" addlevel="must"/>
<gate name="G$2" symbol="3SVORKA-00-SHADOW-XREF" x="17.78" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name="">
<attribute name="ABRA" value="MELE1618"/>
<attribute name="MPN" value="1782330000"/>
<attribute name="_EXTERNAL_" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="3SVORKA-00-SHADOW" prefix="SVORKA" uservalue="yes">
<description>3patrová svorkovnice - bez propojů&lt;P&gt;

Svorka řadová &lt;B&gt;ZDLD 2.5-2VN šedá&lt;/B&gt;</description>
<gates>
<gate name="G$1" symbol="3SVORKA-00-TOP" x="0" y="0"/>
<gate name="G$2" symbol="3SVORKA-00-MID" x="0" y="0"/>
<gate name="G$3" symbol="3SVORKA-00-BOT" x="0" y="0"/>
<gate name="G$4" symbol="3SVORKA-00-SHADOW" x="0" y="0"/>
<gate name="G$5" symbol="3SVORKA-00-SHADOW" x="0" y="0"/>
<gate name="G$6" symbol="3SVORKA-00-SHADOW" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name="">
<attribute name="ABRA" value="MELE1616"/>
<attribute name="MPN" value="1782320000"/>
<attribute name="_EXTERNAL_" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="3SVORKA-00-SEPARATED" prefix="SVORKA" uservalue="yes">
<description>3patrová svorkovnice - bez propojů&lt;P&gt;

Svorka řadová &lt;B&gt;ZDLD 2.5-2VN šedá&lt;/B&gt;</description>
<gates>
<gate name="G$1" symbol="3SVORKA-00-TOP" x="0" y="0" addlevel="must"/>
<gate name="G$2" symbol="3SVORKA-00-MID" x="0" y="0" addlevel="must"/>
<gate name="G$3" symbol="3SVORKA-00-BOT" x="0" y="0" addlevel="must"/>
<gate name="G$4" symbol="3SVORKA-00-SHADOW" x="0" y="0"/>
<gate name="G$5" symbol="3SVORKA-00-SHADOW" x="0" y="0"/>
<gate name="G$6" symbol="3SVORKA-00-SHADOW" x="0" y="0"/>
<gate name="G$7" symbol="3SVORKA-00-SHADOW-XREF" x="17.78" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name="">
<attribute name="ABRA" value="MELE1616"/>
<attribute name="MPN" value="1782320000"/>
<attribute name="_EXTERNAL_" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="TMC">
<packages>
</packages>
<symbols>
<symbol name="TMC-PIN1">
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="NAZEV_PINU_1" x="33.02" y="0" visible="pin" length="point" direction="pas" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN2">
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="NAZEV_PINU_2" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN3">
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="NAZEV_PINU_3" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN4">
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="NAZEV_PINU_4" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN5">
<description>&lt;B&gt;DV8&lt;/B&gt; plamen topení 2; x,24V; open - tma, 24V - plamen - od verze 4 kabelu TMC není na tomto pinu vodič (přesunut na AV10)</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="FLAME2" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN6">
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="NAZEV_PINU_6" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN7">
<description>&lt;B&gt;DV7&lt;/B&gt; chod motoru vozidla; x,24V; open - stop, 24V - motor ON</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="D+" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN8">
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="NAZEV_PINU_8" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN9">
<description>&lt;B&gt;DV6&lt;/B&gt; porucha ventilátorů; x,24V; open - OK, GND - chyba vent.</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="FAILVENT" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN10">
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="NAZEV_PINU_10" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN11">
<description>&lt;B&gt;DV5&lt;/B&gt; plamen topení 1; x,24V; open - tma, 24V - plamen</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="FLAME1" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN12">
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="NAZEV_PINU_12" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN13">
<description>&lt;B&gt;DV4&lt;/B&gt; porucha topení a chlazení; x,24V; open - chyba, 24V - OK</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="FAILH/C" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN14">
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="NAZEV_PINU_14" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN15">
<description>&lt;B&gt;DV3&lt;/B&gt; přep. Jednotka/rozv.;  X,GND; open - nouzový přepínač, GND - ovládání skrz TMC</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="EMERGENCY" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN16">
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="NAZEV_PINU_16" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN17">
<description>&lt;B&gt;DV2&lt;/B&gt; diesel zapnuté zapalování; x,24V; open - Kubota OFF, 24V - Kubota ON</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="MOTOR" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN18">
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="NAZEV_PINU_18" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN19">
<description>&lt;B&gt;DV1&lt;/B&gt; 1.alternátor běží; x,24V; open - chyba, 24V - OK</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="ALT" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN20">
<description>&lt;B&gt;AVY5&lt;/B&gt; Ovládání serv přední; 2-10V</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="CONFLOOR" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN21">
<description>&lt;B&gt;AV9&lt;/B&gt; tlak filtry; 1-10V</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="PRESSFILTER" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN22">
<description>&lt;B&gt;AVY3+4&lt;/B&gt; Ovládání serva topení; 2-10V</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="CONHEAT" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN23">
<description>&lt;B&gt;AV10&lt;/B&gt; dezinfekce připravena; 1-10V; pokud U je pod 2V=dezinfekce není nainstalována (není v menu),
pokud dezinfekce je tak U je nad 6V= LOGO ready, následně U&lt;6V=konec cyklu.</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="DIS_READY" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN24">
<description>&lt;B&gt;AVY1+2&lt;/B&gt; Ovládání serv nápor,rec.; 2-10V</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="CONI_R" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN25">
<description>&lt;B&gt;AV12&lt;/B&gt; Výstup serva topení; 2-10V</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="CHKHEAT" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN26">
<description>&lt;B&gt;DVY1&lt;/B&gt; Napájení dezinfekce</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="LOGO_ON" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN27">
<description>&lt;B&gt;AV13&lt;/B&gt; Výstup serva nápor 1; 2-10V</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="CHKI1" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN28">
<description>&lt;B&gt;DVY2&lt;/B&gt; body disinfection</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="BODYDIS" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN29">
<description>&lt;B&gt;AV14&lt;/B&gt; Výstup serva nápor 2; 2-10V</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="CHKI2" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN30">
<description>&lt;B&gt;DVY3&lt;/B&gt; wheel disinfection</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="WHEELDIS" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN31">
<description>&lt;B&gt;AV15&lt;/B&gt; Výstup serva recirkulace; 2-10V</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="CHKREC" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN32">
<description>&lt;B&gt;DVY4&lt;/B&gt; Houkačka/ALERT, ovládání hlasité sirény GND - aktivní</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="ALERT" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN33">
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="NAZEV_PINU_33" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN34">
<description>&lt;B&gt;DVY5&lt;/B&gt; Zapínání chlazení</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="COOL" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN35">
<description>&lt;B&gt;AV1&lt;/B&gt; tlak sání chlazení; 0-10V; 20°C=4,7BAR=8,5V</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="LP" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN36">
<description>&lt;B&gt;DVY6&lt;/B&gt; Zapínání topení</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="HEAT" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN37">
<description>&lt;B&gt;AV2&lt;/B&gt; kontrola serva podlaha; 2-10V</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="CHKFLOOR" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN38">
<description>&lt;B&gt;DVY7&lt;/B&gt; Zapínání ventilace</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="VENTILATION" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN39">
<description>&lt;B&gt;AV3&lt;/B&gt; tlak výtlak chlazení; 0-10V; 20°C=4,7BAR=3,5V</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="HP" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN40">
<description>&lt;B&gt;DVY8&lt;/B&gt; ovládní čerpadla topení</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="PUMP" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN41">
<description>&lt;B&gt;AV4&lt;/B&gt; teplota vody v topení; 0-10V</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="HEATTEMP" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN42">
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="NAZEV_PINU_42" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN43">
<description>&lt;B&gt;AV5&lt;/B&gt; čidlo CO2; 0-10V</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="CO2" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN44">
<description>Čidlo teploty T6</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="T6" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN45">
<description>&lt;B&gt;AV6&lt;/B&gt; čidlo vlhkosti; 0-10V</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="HUM" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN46">
<description>Čidlo teploty T5</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="T5" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN47">
<description>&lt;B&gt;AV7&lt;/B&gt; Teplota dieselu; 1-10V; bargraf teploty motoru (není zobrazen pokud MOTOR DV2 v "0"), &gt;9,5V= plný bliká, 9,5 - 4V prázdný (60oC), 3V - dva dílky z 10, 2V - 5 z 10, 1V - 8 z 10, 0,5V plný, &lt;0,4V plný bliká, 0V - nezobrazuje se.</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="MTEMP" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN48">
<description>Čidlo teploty T4</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="T4" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN49">
<description>&lt;B&gt;AV8&lt;/B&gt; Výkon ventilace; 0-24V</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="FANCONTROL" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN50">
<description>Čidlo teploty T3</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="T3" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN51">
<description>Rx</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="RX" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN52">
<description>Čidlo teploty T2</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="T2" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN53">
<description>Tx</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="TX" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN54">
<description>Čidlo teploty T1</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="T1" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN55">
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="NAZEV_PINU_55" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN56">
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="NAZEV_PINU_56" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN57">
<description>GND @ 57</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="GND" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN58">
<description>GND @ 58</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="GND" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN59">
<description>POWERTMC @ 59</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="POWERTMC" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN60">
<description>POWERTMC @ 60</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="POWERTMC" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="TMC" prefix="TMC">
<description>&lt;B&gt; TMC &lt;/B&gt; piny</description>
<gates>
<gate name="1" symbol="TMC-PIN1" x="0" y="0" addlevel="can"/>
<gate name="2" symbol="TMC-PIN2" x="0" y="-2.54" addlevel="can"/>
<gate name="3" symbol="TMC-PIN3" x="0" y="-5.08" addlevel="can"/>
<gate name="4" symbol="TMC-PIN4" x="0" y="-7.62" addlevel="can"/>
<gate name="5" symbol="TMC-PIN5" x="0" y="-10.16" addlevel="can"/>
<gate name="6" symbol="TMC-PIN6" x="0" y="-12.7" addlevel="can"/>
<gate name="7" symbol="TMC-PIN7" x="0" y="-15.24" addlevel="can"/>
<gate name="8" symbol="TMC-PIN8" x="0" y="-17.78" addlevel="can"/>
<gate name="9" symbol="TMC-PIN9" x="0" y="-20.32" addlevel="can"/>
<gate name="10" symbol="TMC-PIN10" x="0" y="-22.86" addlevel="can"/>
<gate name="11" symbol="TMC-PIN11" x="0" y="-25.4" addlevel="can"/>
<gate name="12" symbol="TMC-PIN12" x="0" y="-27.94" addlevel="can"/>
<gate name="13" symbol="TMC-PIN13" x="0" y="-30.48" addlevel="can"/>
<gate name="14" symbol="TMC-PIN14" x="0" y="-33.02" addlevel="can"/>
<gate name="15" symbol="TMC-PIN15" x="0" y="-35.56" addlevel="can"/>
<gate name="16" symbol="TMC-PIN16" x="0" y="-38.1" addlevel="can"/>
<gate name="17" symbol="TMC-PIN17" x="0" y="-40.64" addlevel="can"/>
<gate name="18" symbol="TMC-PIN18" x="0" y="-43.18" addlevel="can"/>
<gate name="19" symbol="TMC-PIN19" x="0" y="-45.72" addlevel="can"/>
<gate name="20" symbol="TMC-PIN20" x="0" y="-48.26" addlevel="can"/>
<gate name="21" symbol="TMC-PIN21" x="0" y="-50.8" addlevel="can"/>
<gate name="22" symbol="TMC-PIN22" x="0" y="-53.34" addlevel="can"/>
<gate name="23" symbol="TMC-PIN23" x="0" y="-55.88" addlevel="can"/>
<gate name="24" symbol="TMC-PIN24" x="0" y="-58.42" addlevel="can"/>
<gate name="25" symbol="TMC-PIN25" x="0" y="-60.96" addlevel="can"/>
<gate name="26" symbol="TMC-PIN26" x="0" y="-63.5" addlevel="can"/>
<gate name="27" symbol="TMC-PIN27" x="0" y="-66.04" addlevel="can"/>
<gate name="28" symbol="TMC-PIN28" x="0" y="-68.58" addlevel="can"/>
<gate name="29" symbol="TMC-PIN29" x="0" y="-71.12" addlevel="can"/>
<gate name="30" symbol="TMC-PIN30" x="0" y="-73.66" addlevel="can"/>
<gate name="31" symbol="TMC-PIN31" x="0" y="-76.2" addlevel="can"/>
<gate name="32" symbol="TMC-PIN32" x="0" y="-78.74" addlevel="can"/>
<gate name="33" symbol="TMC-PIN33" x="0" y="-81.28" addlevel="can"/>
<gate name="34" symbol="TMC-PIN34" x="0" y="-83.82" addlevel="can"/>
<gate name="35" symbol="TMC-PIN35" x="0" y="-86.36" addlevel="can"/>
<gate name="36" symbol="TMC-PIN36" x="0" y="-88.9" addlevel="can"/>
<gate name="37" symbol="TMC-PIN37" x="0" y="-91.44" addlevel="can"/>
<gate name="38" symbol="TMC-PIN38" x="0" y="-93.98" addlevel="can"/>
<gate name="39" symbol="TMC-PIN39" x="0" y="-96.52" addlevel="can"/>
<gate name="40" symbol="TMC-PIN40" x="0" y="-99.06" addlevel="can"/>
<gate name="41" symbol="TMC-PIN41" x="0" y="-101.6" addlevel="can"/>
<gate name="42" symbol="TMC-PIN42" x="0" y="-104.14" addlevel="can"/>
<gate name="43" symbol="TMC-PIN43" x="0" y="-106.68" addlevel="can"/>
<gate name="44" symbol="TMC-PIN44" x="0" y="-109.22" addlevel="can"/>
<gate name="45" symbol="TMC-PIN45" x="0" y="-111.76" addlevel="can"/>
<gate name="46" symbol="TMC-PIN46" x="0" y="-114.3" addlevel="can"/>
<gate name="47" symbol="TMC-PIN47" x="0" y="-116.84" addlevel="can"/>
<gate name="48" symbol="TMC-PIN48" x="0" y="-119.38" addlevel="can"/>
<gate name="49" symbol="TMC-PIN49" x="0" y="-121.92" addlevel="can"/>
<gate name="50" symbol="TMC-PIN50" x="0" y="-124.46" addlevel="can"/>
<gate name="51" symbol="TMC-PIN51" x="0" y="-127" addlevel="can"/>
<gate name="52" symbol="TMC-PIN52" x="0" y="-129.54" addlevel="can"/>
<gate name="53" symbol="TMC-PIN53" x="0" y="-132.08" addlevel="can"/>
<gate name="54" symbol="TMC-PIN54" x="0" y="-134.62" addlevel="can"/>
<gate name="55" symbol="TMC-PIN55" x="0" y="-137.16" addlevel="can"/>
<gate name="56" symbol="TMC-PIN56" x="0" y="-139.7" addlevel="can"/>
<gate name="57" symbol="TMC-PIN57" x="0" y="-142.24" addlevel="can"/>
<gate name="58" symbol="TMC-PIN58" x="0" y="-144.78" addlevel="can"/>
<gate name="59" symbol="TMC-PIN59" x="0" y="-147.32" addlevel="can"/>
<gate name="60" symbol="TMC-PIN60" x="0" y="-149.86" addlevel="can"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SUPPLY2@2">
<packages>
</packages>
<symbols>
<symbol name="V&lt;--">
<wire x1="-15.24" y1="0" x2="-13.97" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="-13.97" y1="-1.27" x2="0" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="1.27" x2="-13.97" y2="1.27" width="0.1524" layer="94"/>
<wire x1="-15.24" y1="0" x2="-13.97" y2="1.27" width="0.1524" layer="94"/>
<text x="-13.335" y="-0.762" size="1.524" layer="96">&gt;VALUE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="V&lt;--" prefix="V" uservalue="yes">
<gates>
<gate name="G$1" symbol="V&lt;--" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Senzory">
<packages>
</packages>
<symbols>
<symbol name="HUM">
<text x="0" y="1.27" size="2.54" layer="95" align="bottom-center">HUM</text>
<text x="0" y="-2.54" size="2.54" layer="95" align="bottom-center">SENSOR</text>
<text x="0" y="-6.35" size="1.4224" layer="96" align="bottom-center">----</text>
<wire x1="-10.16" y1="7.62" x2="10.16" y2="7.62" width="0.4064" layer="94"/>
<wire x1="10.16" y1="7.62" x2="10.16" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="10.16" y1="-7.62" x2="-10.16" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="-10.16" y1="-7.62" x2="-10.16" y2="7.62" width="0.4064" layer="94"/>
</symbol>
<symbol name="FP">
<text x="0" y="1.27" size="2.54" layer="95" align="bottom-center">FILTER</text>
<text x="0" y="-2.54" size="2.54" layer="95" align="bottom-center">PRESSURE</text>
<text x="0" y="-6.35" size="1.4224" layer="96" align="bottom-center">HUBA CONTROL 699</text>
<wire x1="-10.16" y1="7.62" x2="10.16" y2="7.62" width="0.4064" layer="94"/>
<wire x1="10.16" y1="7.62" x2="10.16" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="10.16" y1="-7.62" x2="-10.16" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="-10.16" y1="-7.62" x2="-10.16" y2="7.62" width="0.4064" layer="94"/>
</symbol>
<symbol name="CO2">
<text x="0" y="1.27" size="2.54" layer="95" align="bottom-center">CO2</text>
<text x="0" y="-2.54" size="2.54" layer="95" align="bottom-center">SENSOR</text>
<text x="0" y="-6.35" size="1.4224" layer="96" align="bottom-center">EE16-F3A23</text>
<wire x1="-10.16" y1="7.62" x2="10.16" y2="7.62" width="0.4064" layer="94"/>
<wire x1="10.16" y1="7.62" x2="10.16" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="10.16" y1="-7.62" x2="-10.16" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="-10.16" y1="-7.62" x2="-10.16" y2="7.62" width="0.4064" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="HUM" prefix="SENSOR">
<gates>
<gate name="G$1" symbol="HUM" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="FP" prefix="SENSOR">
<gates>
<gate name="G$1" symbol="FP" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CO2" prefix="SENSOR">
<gates>
<gate name="G$1" symbol="CO2" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="7P">
<packages>
<package name="7P">
<pad name="P1" x="-7.62" y="7.62" drill="0.8" shape="square"/>
<pad name="L1" x="-7.62" y="-8.89" drill="0.8" shape="square"/>
<pad name="P2" x="-3.81" y="7.62" drill="0.8" shape="square"/>
<pad name="L2" x="-3.81" y="-8.89" drill="0.8" shape="square"/>
<pad name="P3" x="0" y="7.62" drill="0.8" shape="square"/>
<pad name="L3" x="0" y="-8.89" drill="0.8" shape="square"/>
<pad name="P4" x="3.81" y="7.62" drill="0.8" shape="square"/>
<pad name="L4" x="3.81" y="-8.89" drill="0.8" shape="square"/>
<pad name="P5" x="7.62" y="7.62" drill="0.8" shape="square"/>
<pad name="L5" x="7.62" y="-8.89" drill="0.8" shape="square"/>
<pad name="P6" x="11.43" y="7.62" drill="0.8" shape="square"/>
<pad name="L6" x="11.43" y="-8.89" drill="0.8" shape="square"/>
<pad name="P7" x="15.24" y="7.62" drill="0.8" shape="square"/>
<pad name="L7" x="15.24" y="-8.89" drill="0.8" shape="square"/>
<pad name="PGND" x="24.13" y="0" drill="0.8" shape="square"/>
<pad name="9" x="-21.59" y="-10.16" drill="0.8" shape="square"/>
<pad name="1" x="-21.59" y="10.16" drill="0.8" shape="square"/>
<text x="-25.4" y="10.16" size="1.27" layer="21">1</text>
<pad name="2" x="-21.59" y="7.62" drill="0.8" shape="square"/>
<pad name="3" x="-21.59" y="5.08" drill="0.8" shape="square"/>
<pad name="4" x="-21.59" y="2.54" drill="0.8" shape="square"/>
<pad name="5" x="-21.59" y="0" drill="0.8" shape="square"/>
<pad name="6" x="-21.59" y="-2.54" drill="0.8" shape="square"/>
<pad name="7" x="-21.59" y="-5.08" drill="0.8" shape="square"/>
<pad name="8" x="-21.59" y="-7.62" drill="0.8" shape="square"/>
<pad name="10" x="-21.59" y="-12.7" drill="0.8" shape="square"/>
<text x="-25.4" y="-12.7" size="1.27" layer="21">10</text>
<text x="-7.62" y="10.16" size="1.27" layer="21" align="bottom-center">P1</text>
<text x="-7.62" y="-12.7" size="1.27" layer="21" align="bottom-center">L1</text>
<text x="15.24" y="10.16" size="1.27" layer="21" align="bottom-center">P7</text>
<text x="15.24" y="-12.7" size="1.27" layer="21" align="bottom-center">L7</text>
<text x="24.13" y="2.54" size="1.27" layer="21" align="bottom-center">PGND</text>
</package>
</packages>
<symbols>
<symbol name="7P-NAPAJENI">
<wire x1="15.24" y1="-7.62" x2="15.24" y2="-5.08" width="0.1524" layer="94"/>
<circle x="15.24" y="-4.445" radius="0.635" width="0.1524" layer="94"/>
<wire x1="14.605" y1="-2.54" x2="15.875" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="15.24" y1="-1.905" x2="15.24" y2="-3.175" width="0.1524" layer="94"/>
<pin name="SUPPLY" x="-5.08" y="-7.62" visible="pad" length="middle" direction="pas"/>
<pin name="GND" x="-5.08" y="-25.4" visible="pad" length="middle" direction="pas"/>
<pin name="PGND@3" x="48.26" y="-12.7" visible="pad" length="middle" direction="pas" rot="R180"/>
<wire x1="15.24" y1="-7.62" x2="0" y2="-7.62" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-0.3175" width="0.3048" layer="94"/>
<wire x1="0" y1="-0.3175" x2="0" y2="-0.635" width="0.3048" layer="94"/>
<wire x1="0" y1="-0.635" x2="0" y2="-0.9525" width="0.3048" layer="94"/>
<wire x1="0" y1="-0.9525" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="-7.62" width="0.3048" layer="94"/>
<wire x1="0" y1="-7.62" x2="0" y2="-25.4" width="0.3048" layer="94"/>
<wire x1="0" y1="-25.4" x2="0" y2="-32.385" width="0.3048" layer="94"/>
<wire x1="0" y1="-32.385" x2="0.635" y2="-33.02" width="0.3048" layer="94"/>
<wire x1="0.635" y1="-33.02" x2="43.18" y2="-33.02" width="0.3048" layer="94"/>
<wire x1="43.18" y1="-33.02" x2="43.18" y2="-12.7" width="0.3048" layer="94"/>
<wire x1="43.18" y1="-12.7" x2="43.18" y2="-0.635" width="0.3048" layer="94"/>
<wire x1="43.18" y1="-0.635" x2="42.545" y2="0" width="0.3048" layer="94"/>
<wire x1="42.545" y1="0" x2="1.27" y2="0" width="0.3048" layer="94"/>
<wire x1="1.27" y1="0" x2="0.9525" y2="0" width="0.3048" layer="94"/>
<wire x1="0.9525" y1="0" x2="0.635" y2="0" width="0.3048" layer="94"/>
<wire x1="0.635" y1="0" x2="0.3175" y2="0" width="0.3048" layer="94"/>
<wire x1="0.3175" y1="0" x2="0" y2="0" width="0.3048" layer="94"/>
<wire x1="15.24" y1="-25.4" x2="15.24" y2="-30.48" width="0.1524" layer="94"/>
<wire x1="13.97" y1="-30.48" x2="15.24" y2="-30.48" width="0.4064" layer="94"/>
<wire x1="15.24" y1="-30.48" x2="16.51" y2="-30.48" width="0.4064" layer="94"/>
<wire x1="0" y1="-25.4" x2="15.24" y2="-25.4" width="0.1524" layer="94"/>
<wire x1="26.3525" y1="-15.875" x2="27.94" y2="-15.875" width="0.254" layer="94"/>
<wire x1="27.94" y1="-15.875" x2="29.5275" y2="-15.875" width="0.254" layer="94"/>
<wire x1="26.9875" y1="-16.51" x2="28.8925" y2="-16.51" width="0.254" layer="94"/>
<wire x1="27.6225" y1="-17.145" x2="28.2575" y2="-17.145" width="0.254" layer="94"/>
<wire x1="27.94" y1="-12.7" x2="27.94" y2="-15.875" width="0.1524" layer="94"/>
<text x="0" y="2.54" size="1.778" layer="94" rot="MR180">Deska 7P - napájení + zem</text>
<text x="2.54" y="-5.08" size="1.778" layer="94" rot="MR180">Napájení</text>
<text x="2.54" y="-22.86" size="1.778" layer="94" rot="MR180">Zem</text>
<text x="25.4" y="-12.7" size="1.778" layer="94" align="top-right">Silová zem</text>
<wire x1="27.94" y1="-12.7" x2="43.18" y2="-12.7" width="0.1524" layer="94"/>
<wire x1="0" y1="-32.385" x2="42.545" y2="-32.385" width="0.3048" layer="94"/>
<wire x1="42.545" y1="-32.385" x2="42.545" y2="0" width="0.3048" layer="94"/>
<wire x1="42.545" y1="-32.385" x2="43.18" y2="-33.02" width="0.3048" layer="94"/>
<text x="44.45" y="-33.02" size="1.27" layer="94">Část desky:</text>
<text x="58.42" y="-33.02" size="1.27" layer="94">&gt;GATE</text>
<text x="44.45" y="-30.48" size="1.27" layer="94">Karta ABRA:</text>
<text x="58.42" y="-30.48" size="1.27" layer="94">&gt;ABRA</text>
<text x="44.45" y="-27.94" size="1.27" layer="94">Označení desky:</text>
<text x="58.42" y="-27.94" size="1.27" layer="94">&gt;VALUE</text>
<text x="41.275" y="-1.27" size="1.27" layer="95" align="top-right">&gt;PART</text>
<wire x1="33.02" y1="-16.51" x2="33.02" y2="-19.05" width="0.254" layer="94"/>
<wire x1="33.02" y1="-19.05" x2="33.02" y2="-21.59" width="0.254" layer="94"/>
<wire x1="33.02" y1="-21.59" x2="33.02" y2="-21.844" width="0.254" layer="94"/>
<wire x1="33.02" y1="-21.844" x2="33.02" y2="-22.098" width="0.254" layer="94"/>
<wire x1="33.02" y1="-22.098" x2="33.02" y2="-22.225" width="0.254" layer="94"/>
<wire x1="33.02" y1="-22.225" x2="33.02" y2="-23.495" width="0.254" layer="94"/>
<wire x1="33.02" y1="-23.495" x2="33.02" y2="-23.622" width="0.254" layer="94"/>
<wire x1="33.02" y1="-23.876" x2="33.02" y2="-24.13" width="0.254" layer="94"/>
<wire x1="33.02" y1="-24.13" x2="33.02" y2="-24.638" width="0.254" layer="94"/>
<wire x1="33.02" y1="-24.638" x2="33.02" y2="-24.765" width="0.254" layer="94"/>
<wire x1="33.02" y1="-24.765" x2="33.02" y2="-26.035" width="0.254" layer="94"/>
<wire x1="33.02" y1="-26.035" x2="33.02" y2="-26.162" width="0.254" layer="94"/>
<wire x1="33.02" y1="-26.162" x2="33.02" y2="-26.416" width="0.254" layer="94"/>
<wire x1="33.02" y1="-26.416" x2="33.02" y2="-26.67" width="0.254" layer="94"/>
<wire x1="33.02" y1="-26.67" x2="33.02" y2="-29.21" width="0.254" layer="94"/>
<wire x1="33.02" y1="-29.21" x2="35.56" y2="-29.21" width="0.254" layer="94"/>
<wire x1="35.56" y1="-29.21" x2="35.814" y2="-29.21" width="0.254" layer="94"/>
<wire x1="36.068" y1="-29.21" x2="36.195" y2="-29.21" width="0.254" layer="94"/>
<wire x1="36.195" y1="-29.21" x2="38.1" y2="-29.21" width="0.254" layer="94"/>
<wire x1="38.1" y1="-29.21" x2="38.1" y2="-26.67" width="0.254" layer="94"/>
<wire x1="38.1" y1="-26.67" x2="38.1" y2="-24.13" width="0.254" layer="94"/>
<wire x1="38.1" y1="-24.13" x2="38.1" y2="-21.59" width="0.254" layer="94"/>
<wire x1="38.1" y1="-21.59" x2="38.1" y2="-19.05" width="0.254" layer="94"/>
<wire x1="38.1" y1="-19.05" x2="38.1" y2="-16.51" width="0.254" layer="94"/>
<wire x1="38.1" y1="-16.51" x2="36.195" y2="-16.51" width="0.254" layer="94"/>
<wire x1="36.195" y1="-16.51" x2="36.068" y2="-16.51" width="0.254" layer="94"/>
<wire x1="35.56" y1="-16.51" x2="33.02" y2="-16.51" width="0.254" layer="94"/>
<wire x1="35.56" y1="-16.51" x2="35.56" y2="-17.018" width="0.254" layer="94"/>
<wire x1="35.56" y1="-17.018" x2="35.56" y2="-17.145" width="0.254" layer="94"/>
<wire x1="35.56" y1="-17.145" x2="35.56" y2="-18.415" width="0.254" layer="94"/>
<wire x1="35.56" y1="-18.415" x2="35.56" y2="-18.542" width="0.254" layer="94"/>
<wire x1="35.56" y1="-18.796" x2="35.56" y2="-19.304" width="0.254" layer="94"/>
<wire x1="35.56" y1="-19.304" x2="35.56" y2="-19.558" width="0.254" layer="94"/>
<wire x1="35.56" y1="-19.558" x2="35.56" y2="-19.685" width="0.254" layer="94"/>
<wire x1="35.56" y1="-19.685" x2="35.56" y2="-20.955" width="0.254" layer="94"/>
<wire x1="35.56" y1="-20.955" x2="35.56" y2="-21.082" width="0.254" layer="94"/>
<wire x1="35.56" y1="-21.082" x2="35.56" y2="-21.336" width="0.254" layer="94"/>
<wire x1="35.56" y1="-21.336" x2="35.56" y2="-26.924" width="0.254" layer="94"/>
<wire x1="35.56" y1="-26.924" x2="35.56" y2="-27.178" width="0.254" layer="94"/>
<wire x1="35.56" y1="-27.178" x2="35.56" y2="-27.305" width="0.254" layer="94"/>
<wire x1="35.56" y1="-27.305" x2="35.56" y2="-28.575" width="0.254" layer="94"/>
<wire x1="35.56" y1="-28.575" x2="35.56" y2="-28.702" width="0.254" layer="94"/>
<wire x1="35.56" y1="-28.702" x2="35.56" y2="-28.956" width="0.254" layer="94"/>
<wire x1="35.56" y1="-28.956" x2="35.56" y2="-29.21" width="0.254" layer="94"/>
<wire x1="33.02" y1="-19.05" x2="35.814" y2="-19.05" width="0.254" layer="94"/>
<wire x1="35.814" y1="-19.05" x2="36.068" y2="-19.05" width="0.254" layer="94"/>
<wire x1="36.068" y1="-19.05" x2="36.195" y2="-19.05" width="0.254" layer="94"/>
<wire x1="36.195" y1="-19.05" x2="38.1" y2="-19.05" width="0.254" layer="94"/>
<wire x1="38.1" y1="-21.59" x2="36.195" y2="-21.59" width="0.254" layer="94"/>
<wire x1="36.195" y1="-21.59" x2="36.068" y2="-21.59" width="0.254" layer="94"/>
<wire x1="35.814" y1="-21.59" x2="33.655" y2="-21.59" width="0.254" layer="94"/>
<wire x1="33.655" y1="-21.59" x2="33.528" y2="-21.59" width="0.254" layer="94"/>
<wire x1="33.274" y1="-21.59" x2="33.02" y2="-21.59" width="0.254" layer="94"/>
<wire x1="33.02" y1="-24.13" x2="33.274" y2="-24.13" width="0.254" layer="94"/>
<wire x1="33.274" y1="-24.13" x2="33.528" y2="-24.13" width="0.254" layer="94"/>
<wire x1="33.528" y1="-24.13" x2="33.655" y2="-24.13" width="0.254" layer="94"/>
<wire x1="33.655" y1="-24.13" x2="38.1" y2="-24.13" width="0.254" layer="94"/>
<wire x1="38.1" y1="-26.67" x2="36.195" y2="-26.67" width="0.254" layer="94"/>
<wire x1="36.195" y1="-26.67" x2="36.068" y2="-26.67" width="0.254" layer="94"/>
<wire x1="35.814" y1="-26.67" x2="33.655" y2="-26.67" width="0.254" layer="94"/>
<wire x1="33.655" y1="-26.67" x2="33.528" y2="-26.67" width="0.254" layer="94"/>
<wire x1="33.274" y1="-26.67" x2="33.02" y2="-26.67" width="0.254" layer="94"/>
<wire x1="33.655" y1="-21.59" x2="33.02" y2="-22.225" width="0.254" layer="94"/>
<wire x1="33.655" y1="-24.13" x2="33.02" y2="-24.765" width="0.254" layer="94"/>
<wire x1="33.02" y1="-26.035" x2="33.655" y2="-26.67" width="0.254" layer="94"/>
<wire x1="33.02" y1="-23.495" x2="33.655" y2="-24.13" width="0.254" layer="94"/>
<wire x1="36.195" y1="-26.67" x2="35.56" y2="-27.305" width="0.254" layer="94"/>
<wire x1="35.56" y1="-28.575" x2="36.195" y2="-29.21" width="0.254" layer="94"/>
<wire x1="35.56" y1="-20.955" x2="36.195" y2="-21.59" width="0.254" layer="94"/>
<wire x1="35.56" y1="-19.685" x2="36.195" y2="-19.05" width="0.254" layer="94"/>
<wire x1="36.195" y1="-19.05" x2="35.56" y2="-18.415" width="0.254" layer="94"/>
<wire x1="35.56" y1="-17.145" x2="36.195" y2="-16.51" width="0.254" layer="94"/>
<wire x1="35.56" y1="-17.018" x2="35.814" y2="-16.764" width="0.254" layer="94"/>
<wire x1="35.814" y1="-16.764" x2="36.068" y2="-16.51" width="0.254" layer="94"/>
<wire x1="36.068" y1="-16.51" x2="35.56" y2="-16.51" width="0.254" layer="94"/>
<wire x1="35.56" y1="-16.51" x2="35.814" y2="-16.764" width="0.254" layer="94"/>
<wire x1="35.56" y1="-19.558" x2="36.068" y2="-19.05" width="0.254" layer="94"/>
<wire x1="36.068" y1="-19.05" x2="35.56" y2="-18.542" width="0.254" layer="94"/>
<wire x1="35.56" y1="-18.542" x2="35.56" y2="-18.796" width="0.254" layer="94"/>
<wire x1="35.56" y1="-18.796" x2="35.814" y2="-19.05" width="0.254" layer="94"/>
<wire x1="35.814" y1="-19.05" x2="35.56" y2="-19.304" width="0.254" layer="94"/>
<wire x1="35.56" y1="-21.082" x2="36.068" y2="-21.59" width="0.254" layer="94"/>
<wire x1="36.068" y1="-21.59" x2="35.814" y2="-21.59" width="0.254" layer="94"/>
<wire x1="35.814" y1="-21.59" x2="35.56" y2="-21.336" width="0.254" layer="94"/>
<wire x1="33.02" y1="-22.098" x2="33.528" y2="-21.59" width="0.254" layer="94"/>
<wire x1="33.528" y1="-21.59" x2="33.274" y2="-21.59" width="0.254" layer="94"/>
<wire x1="33.274" y1="-21.59" x2="33.02" y2="-21.844" width="0.254" layer="94"/>
<wire x1="33.02" y1="-24.638" x2="33.274" y2="-24.384" width="0.254" layer="94"/>
<wire x1="33.274" y1="-24.384" x2="33.528" y2="-24.13" width="0.254" layer="94"/>
<wire x1="33.528" y1="-24.13" x2="33.02" y2="-23.622" width="0.254" layer="94"/>
<wire x1="33.02" y1="-23.622" x2="33.02" y2="-23.876" width="0.254" layer="94"/>
<wire x1="33.02" y1="-23.876" x2="33.274" y2="-24.13" width="0.254" layer="94"/>
<wire x1="33.274" y1="-24.13" x2="33.274" y2="-24.384" width="0.254" layer="94"/>
<wire x1="33.528" y1="-26.67" x2="33.274" y2="-26.67" width="0.254" layer="94"/>
<wire x1="33.274" y1="-26.67" x2="33.02" y2="-26.416" width="0.254" layer="94"/>
<wire x1="33.528" y1="-26.67" x2="33.02" y2="-26.162" width="0.254" layer="94"/>
<wire x1="35.56" y1="-28.702" x2="36.068" y2="-29.21" width="0.254" layer="94"/>
<wire x1="36.068" y1="-29.21" x2="35.814" y2="-29.21" width="0.254" layer="94"/>
<wire x1="35.814" y1="-29.21" x2="35.56" y2="-28.956" width="0.254" layer="94"/>
<wire x1="35.56" y1="-27.178" x2="36.068" y2="-26.67" width="0.254" layer="94"/>
<wire x1="36.068" y1="-26.67" x2="35.814" y2="-26.67" width="0.254" layer="94"/>
<wire x1="35.814" y1="-26.67" x2="35.56" y2="-26.924" width="0.254" layer="94"/>
<text x="33.782" y="-28.702" size="1.4224" layer="94">1</text>
<text x="33.782" y="-26.162" size="1.4224" layer="94">2</text>
<text x="33.782" y="-23.622" size="1.4224" layer="94">3</text>
<text x="33.782" y="-21.082" size="1.4224" layer="94">4</text>
<text x="33.782" y="-18.542" size="1.4224" layer="94">5</text>
<text x="36.322" y="-28.702" size="1.4224" layer="94">6</text>
<text x="36.322" y="-26.162" size="1.4224" layer="94">7</text>
<text x="36.322" y="-23.622" size="1.4224" layer="94">8</text>
<text x="36.322" y="-21.082" size="1.4224" layer="94">9</text>
<text x="35.56" y="-18.542" size="1.4224" layer="94">10</text>
<rectangle x1="38.1" y1="-23.876" x2="38.862" y2="-21.844" layer="94"/>
<text x="30.48" y="-22.86" size="1.27" layer="94" align="center-right">Čislování konektoru
na desce 7P
(horní pohled)</text>
<pin name="PGND" x="35.56" y="-7.62" visible="pin" direction="pas" rot="R180"/>
<wire x1="27.94" y1="-12.7" x2="27.94" y2="-7.62" width="0.1524" layer="94"/>
<circle x="27.94" y="-12.7" radius="0.4064" width="0" layer="94"/>
<wire x1="34.6075" y1="-5.08" x2="34.6075" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="34.6075" y1="-10.16" x2="36.5125" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="36.5125" y1="-10.16" x2="36.5125" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="36.5125" y1="-5.08" x2="34.6075" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="34.6075" y1="-5.08" x2="33.9725" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="33.9725" y1="-5.08" x2="33.9725" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="33.9725" y1="-10.16" x2="34.6075" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="36.5125" y1="-10.16" x2="37.1475" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="37.1475" y1="-10.16" x2="37.1475" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="37.1475" y1="-5.08" x2="36.5125" y2="-5.08" width="0.1524" layer="94"/>
<circle x="35.56" y="-7.62" radius="0.635" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.27" x2="1.27" y2="0" width="0.3048" layer="94"/>
<wire x1="0" y1="-0.9525" x2="0.9525" y2="0" width="0.3048" layer="94"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.3048" layer="94"/>
<wire x1="0" y1="-0.3175" x2="0.3175" y2="0" width="0.3048" layer="94"/>
</symbol>
<symbol name="7P-POJISTKY">
<wire x1="26.67" y1="-15.24" x2="27.6225" y2="-15.24" width="0.3048" layer="94"/>
<wire x1="27.6225" y1="-15.24" x2="33.3375" y2="-15.24" width="0.3048" layer="94"/>
<wire x1="33.3375" y1="-15.24" x2="34.29" y2="-15.24" width="0.3048" layer="94"/>
<wire x1="34.29" y1="-15.24" x2="34.29" y2="-16.51" width="0.3048" layer="94"/>
<wire x1="34.29" y1="-16.51" x2="34.29" y2="-16.8275" width="0.3048" layer="94"/>
<wire x1="34.29" y1="-16.8275" x2="34.29" y2="-23.8125" width="0.3048" layer="94"/>
<wire x1="34.29" y1="-23.8125" x2="34.29" y2="-24.13" width="0.3048" layer="94"/>
<wire x1="34.29" y1="-24.13" x2="34.29" y2="-25.4" width="0.3048" layer="94"/>
<wire x1="34.29" y1="-25.4" x2="27.6225" y2="-25.4" width="0.3048" layer="94"/>
<wire x1="27.6225" y1="-25.4" x2="26.67" y2="-25.4" width="0.3048" layer="94"/>
<wire x1="26.67" y1="-25.4" x2="26.67" y2="-24.13" width="0.3048" layer="94"/>
<wire x1="26.67" y1="-24.13" x2="26.67" y2="-23.8125" width="0.3048" layer="94"/>
<wire x1="26.67" y1="-23.8125" x2="26.67" y2="-16.8275" width="0.3048" layer="94"/>
<wire x1="26.67" y1="-16.8275" x2="26.67" y2="-16.51" width="0.3048" layer="94"/>
<wire x1="26.67" y1="-16.51" x2="26.67" y2="-15.24" width="0.3048" layer="94"/>
<wire x1="26.67" y1="-16.51" x2="34.29" y2="-16.51" width="0.1524" layer="94"/>
<wire x1="26.67" y1="-24.13" x2="34.29" y2="-24.13" width="0.1524" layer="94"/>
<wire x1="26.67" y1="-16.8275" x2="34.29" y2="-16.8275" width="0.1524" layer="94"/>
<wire x1="26.67" y1="-23.8125" x2="34.29" y2="-23.8125" width="0.1524" layer="94"/>
<wire x1="27.6225" y1="-15.24" x2="27.6225" y2="-9.525" width="0.3048" layer="94"/>
<wire x1="27.6225" y1="-9.525" x2="28.8925" y2="-8.255" width="0.3048" layer="94"/>
<wire x1="28.8925" y1="-8.255" x2="30.48" y2="-8.255" width="0.3048" layer="94"/>
<wire x1="30.48" y1="-8.255" x2="32.0675" y2="-8.255" width="0.3048" layer="94"/>
<wire x1="32.0675" y1="-8.255" x2="33.3375" y2="-9.525" width="0.3048" layer="94"/>
<wire x1="33.3375" y1="-9.525" x2="33.3375" y2="-15.24" width="0.3048" layer="94"/>
<wire x1="33.3375" y1="-25.4" x2="33.3375" y2="-31.115" width="0.3048" layer="94"/>
<wire x1="33.3375" y1="-31.115" x2="32.0675" y2="-32.385" width="0.3048" layer="94"/>
<wire x1="32.0675" y1="-32.385" x2="30.48" y2="-32.385" width="0.3048" layer="94"/>
<wire x1="30.48" y1="-32.385" x2="28.8925" y2="-32.385" width="0.3048" layer="94"/>
<wire x1="28.8925" y1="-32.385" x2="27.6225" y2="-31.115" width="0.3048" layer="94"/>
<wire x1="27.6225" y1="-31.115" x2="27.6225" y2="-25.4" width="0.3048" layer="94"/>
<circle x="30.48" y="-11.1125" radius="1.7097" width="0.3048" layer="94"/>
<circle x="30.48" y="-29.5275" radius="1.7097" width="0.3048" layer="94"/>
<wire x1="30.48" y1="-18.7325" x2="30.48" y2="-21.59" width="0.1524" layer="94"/>
<wire x1="30.1625" y1="-19.3675" x2="30.1625" y2="-20.955" width="0.1524" layer="94"/>
<wire x1="30.1625" y1="-20.955" x2="30.7975" y2="-20.955" width="0.1524" layer="94"/>
<wire x1="30.7975" y1="-20.955" x2="30.7975" y2="-19.3675" width="0.1524" layer="94"/>
<wire x1="30.7975" y1="-19.3675" x2="30.1625" y2="-19.3675" width="0.1524" layer="94"/>
<wire x1="30.48" y1="-8.255" x2="30.48" y2="-7.62" width="0.1524" layer="94"/>
<pin name="P1" x="30.48" y="-5.08" visible="pin" length="short" direction="pas" rot="R270"/>
<pin name="L1" x="30.48" y="-35.56" visible="pin" length="short" direction="pas" rot="R90"/>
<wire x1="36.83" y1="-15.24" x2="37.7825" y2="-15.24" width="0.3048" layer="94"/>
<wire x1="37.7825" y1="-15.24" x2="43.4975" y2="-15.24" width="0.3048" layer="94"/>
<wire x1="43.4975" y1="-15.24" x2="44.45" y2="-15.24" width="0.3048" layer="94"/>
<wire x1="44.45" y1="-15.24" x2="44.45" y2="-16.51" width="0.3048" layer="94"/>
<wire x1="44.45" y1="-16.51" x2="44.45" y2="-16.8275" width="0.3048" layer="94"/>
<wire x1="44.45" y1="-16.8275" x2="44.45" y2="-23.8125" width="0.3048" layer="94"/>
<wire x1="44.45" y1="-23.8125" x2="44.45" y2="-24.13" width="0.3048" layer="94"/>
<wire x1="44.45" y1="-24.13" x2="44.45" y2="-25.4" width="0.3048" layer="94"/>
<wire x1="44.45" y1="-25.4" x2="37.7825" y2="-25.4" width="0.3048" layer="94"/>
<wire x1="37.7825" y1="-25.4" x2="36.83" y2="-25.4" width="0.3048" layer="94"/>
<wire x1="36.83" y1="-25.4" x2="36.83" y2="-24.13" width="0.3048" layer="94"/>
<wire x1="36.83" y1="-24.13" x2="36.83" y2="-23.8125" width="0.3048" layer="94"/>
<wire x1="36.83" y1="-23.8125" x2="36.83" y2="-16.8275" width="0.3048" layer="94"/>
<wire x1="36.83" y1="-16.8275" x2="36.83" y2="-16.51" width="0.3048" layer="94"/>
<wire x1="36.83" y1="-16.51" x2="36.83" y2="-15.24" width="0.3048" layer="94"/>
<wire x1="36.83" y1="-16.51" x2="44.45" y2="-16.51" width="0.1524" layer="94"/>
<wire x1="36.83" y1="-24.13" x2="44.45" y2="-24.13" width="0.1524" layer="94"/>
<wire x1="36.83" y1="-16.8275" x2="44.45" y2="-16.8275" width="0.1524" layer="94"/>
<wire x1="36.83" y1="-23.8125" x2="44.45" y2="-23.8125" width="0.1524" layer="94"/>
<wire x1="37.7825" y1="-15.24" x2="37.7825" y2="-9.525" width="0.3048" layer="94"/>
<wire x1="37.7825" y1="-9.525" x2="39.0525" y2="-8.255" width="0.3048" layer="94"/>
<wire x1="39.0525" y1="-8.255" x2="40.64" y2="-8.255" width="0.3048" layer="94"/>
<wire x1="40.64" y1="-8.255" x2="42.2275" y2="-8.255" width="0.3048" layer="94"/>
<wire x1="42.2275" y1="-8.255" x2="43.4975" y2="-9.525" width="0.3048" layer="94"/>
<wire x1="43.4975" y1="-9.525" x2="43.4975" y2="-15.24" width="0.3048" layer="94"/>
<wire x1="43.4975" y1="-25.4" x2="43.4975" y2="-31.115" width="0.3048" layer="94"/>
<wire x1="43.4975" y1="-31.115" x2="42.2275" y2="-32.385" width="0.3048" layer="94"/>
<wire x1="42.2275" y1="-32.385" x2="40.64" y2="-32.385" width="0.3048" layer="94"/>
<wire x1="40.64" y1="-32.385" x2="39.0525" y2="-32.385" width="0.3048" layer="94"/>
<wire x1="39.0525" y1="-32.385" x2="37.7825" y2="-31.115" width="0.3048" layer="94"/>
<wire x1="37.7825" y1="-31.115" x2="37.7825" y2="-25.4" width="0.3048" layer="94"/>
<circle x="40.64" y="-11.1125" radius="1.7097" width="0.3048" layer="94"/>
<circle x="40.64" y="-29.5275" radius="1.7097" width="0.3048" layer="94"/>
<wire x1="40.64" y1="-8.255" x2="40.64" y2="-7.62" width="0.1524" layer="94"/>
<pin name="P2" x="40.64" y="-5.08" visible="pin" length="short" direction="pas" rot="R270"/>
<pin name="L2" x="40.64" y="-35.56" visible="pin" length="short" direction="pas" rot="R90"/>
<wire x1="46.99" y1="-15.24" x2="47.9425" y2="-15.24" width="0.3048" layer="94"/>
<wire x1="47.9425" y1="-15.24" x2="53.6575" y2="-15.24" width="0.3048" layer="94"/>
<wire x1="53.6575" y1="-15.24" x2="54.61" y2="-15.24" width="0.3048" layer="94"/>
<wire x1="54.61" y1="-15.24" x2="54.61" y2="-16.51" width="0.3048" layer="94"/>
<wire x1="54.61" y1="-16.51" x2="54.61" y2="-16.8275" width="0.3048" layer="94"/>
<wire x1="54.61" y1="-16.8275" x2="54.61" y2="-23.8125" width="0.3048" layer="94"/>
<wire x1="54.61" y1="-23.8125" x2="54.61" y2="-24.13" width="0.3048" layer="94"/>
<wire x1="54.61" y1="-24.13" x2="54.61" y2="-25.4" width="0.3048" layer="94"/>
<wire x1="54.61" y1="-25.4" x2="47.9425" y2="-25.4" width="0.3048" layer="94"/>
<wire x1="47.9425" y1="-25.4" x2="46.99" y2="-25.4" width="0.3048" layer="94"/>
<wire x1="46.99" y1="-25.4" x2="46.99" y2="-24.13" width="0.3048" layer="94"/>
<wire x1="46.99" y1="-24.13" x2="46.99" y2="-23.8125" width="0.3048" layer="94"/>
<wire x1="46.99" y1="-23.8125" x2="46.99" y2="-16.8275" width="0.3048" layer="94"/>
<wire x1="46.99" y1="-16.8275" x2="46.99" y2="-16.51" width="0.3048" layer="94"/>
<wire x1="46.99" y1="-16.51" x2="46.99" y2="-15.24" width="0.3048" layer="94"/>
<wire x1="46.99" y1="-16.51" x2="54.61" y2="-16.51" width="0.1524" layer="94"/>
<wire x1="46.99" y1="-24.13" x2="54.61" y2="-24.13" width="0.1524" layer="94"/>
<wire x1="46.99" y1="-16.8275" x2="54.61" y2="-16.8275" width="0.1524" layer="94"/>
<wire x1="46.99" y1="-23.8125" x2="54.61" y2="-23.8125" width="0.1524" layer="94"/>
<wire x1="47.9425" y1="-15.24" x2="47.9425" y2="-9.525" width="0.3048" layer="94"/>
<wire x1="47.9425" y1="-9.525" x2="49.2125" y2="-8.255" width="0.3048" layer="94"/>
<wire x1="49.2125" y1="-8.255" x2="50.8" y2="-8.255" width="0.3048" layer="94"/>
<wire x1="50.8" y1="-8.255" x2="52.3875" y2="-8.255" width="0.3048" layer="94"/>
<wire x1="52.3875" y1="-8.255" x2="53.6575" y2="-9.525" width="0.3048" layer="94"/>
<wire x1="53.6575" y1="-9.525" x2="53.6575" y2="-15.24" width="0.3048" layer="94"/>
<wire x1="53.6575" y1="-25.4" x2="53.6575" y2="-31.115" width="0.3048" layer="94"/>
<wire x1="53.6575" y1="-31.115" x2="52.3875" y2="-32.385" width="0.3048" layer="94"/>
<wire x1="52.3875" y1="-32.385" x2="50.8" y2="-32.385" width="0.3048" layer="94"/>
<wire x1="50.8" y1="-32.385" x2="49.2125" y2="-32.385" width="0.3048" layer="94"/>
<wire x1="49.2125" y1="-32.385" x2="47.9425" y2="-31.115" width="0.3048" layer="94"/>
<wire x1="47.9425" y1="-31.115" x2="47.9425" y2="-25.4" width="0.3048" layer="94"/>
<circle x="50.8" y="-11.1125" radius="1.7097" width="0.3048" layer="94"/>
<circle x="50.8" y="-29.5275" radius="1.7097" width="0.3048" layer="94"/>
<wire x1="50.8" y1="-8.255" x2="50.8" y2="-7.62" width="0.1524" layer="94"/>
<pin name="P3" x="50.8" y="-5.08" visible="pin" length="short" direction="pas" rot="R270"/>
<pin name="L3" x="50.8" y="-35.56" visible="pin" length="short" direction="pas" rot="R90"/>
<wire x1="57.15" y1="-15.24" x2="58.1025" y2="-15.24" width="0.3048" layer="94"/>
<wire x1="58.1025" y1="-15.24" x2="63.8175" y2="-15.24" width="0.3048" layer="94"/>
<wire x1="63.8175" y1="-15.24" x2="64.77" y2="-15.24" width="0.3048" layer="94"/>
<wire x1="64.77" y1="-15.24" x2="64.77" y2="-16.51" width="0.3048" layer="94"/>
<wire x1="64.77" y1="-16.51" x2="64.77" y2="-16.8275" width="0.3048" layer="94"/>
<wire x1="64.77" y1="-16.8275" x2="64.77" y2="-23.8125" width="0.3048" layer="94"/>
<wire x1="64.77" y1="-23.8125" x2="64.77" y2="-24.13" width="0.3048" layer="94"/>
<wire x1="64.77" y1="-24.13" x2="64.77" y2="-25.4" width="0.3048" layer="94"/>
<wire x1="64.77" y1="-25.4" x2="58.1025" y2="-25.4" width="0.3048" layer="94"/>
<wire x1="58.1025" y1="-25.4" x2="57.15" y2="-25.4" width="0.3048" layer="94"/>
<wire x1="57.15" y1="-25.4" x2="57.15" y2="-24.13" width="0.3048" layer="94"/>
<wire x1="57.15" y1="-24.13" x2="57.15" y2="-23.8125" width="0.3048" layer="94"/>
<wire x1="57.15" y1="-23.8125" x2="57.15" y2="-16.8275" width="0.3048" layer="94"/>
<wire x1="57.15" y1="-16.8275" x2="57.15" y2="-16.51" width="0.3048" layer="94"/>
<wire x1="57.15" y1="-16.51" x2="57.15" y2="-15.24" width="0.3048" layer="94"/>
<wire x1="57.15" y1="-16.51" x2="64.77" y2="-16.51" width="0.1524" layer="94"/>
<wire x1="57.15" y1="-24.13" x2="64.77" y2="-24.13" width="0.1524" layer="94"/>
<wire x1="57.15" y1="-16.8275" x2="64.77" y2="-16.8275" width="0.1524" layer="94"/>
<wire x1="57.15" y1="-23.8125" x2="64.77" y2="-23.8125" width="0.1524" layer="94"/>
<wire x1="58.1025" y1="-15.24" x2="58.1025" y2="-9.525" width="0.3048" layer="94"/>
<wire x1="58.1025" y1="-9.525" x2="59.3725" y2="-8.255" width="0.3048" layer="94"/>
<wire x1="59.3725" y1="-8.255" x2="60.96" y2="-8.255" width="0.3048" layer="94"/>
<wire x1="60.96" y1="-8.255" x2="62.5475" y2="-8.255" width="0.3048" layer="94"/>
<wire x1="62.5475" y1="-8.255" x2="63.8175" y2="-9.525" width="0.3048" layer="94"/>
<wire x1="63.8175" y1="-9.525" x2="63.8175" y2="-15.24" width="0.3048" layer="94"/>
<wire x1="63.8175" y1="-25.4" x2="63.8175" y2="-31.115" width="0.3048" layer="94"/>
<wire x1="63.8175" y1="-31.115" x2="62.5475" y2="-32.385" width="0.3048" layer="94"/>
<wire x1="62.5475" y1="-32.385" x2="60.96" y2="-32.385" width="0.3048" layer="94"/>
<wire x1="60.96" y1="-32.385" x2="59.3725" y2="-32.385" width="0.3048" layer="94"/>
<wire x1="59.3725" y1="-32.385" x2="58.1025" y2="-31.115" width="0.3048" layer="94"/>
<wire x1="58.1025" y1="-31.115" x2="58.1025" y2="-25.4" width="0.3048" layer="94"/>
<circle x="60.96" y="-11.1125" radius="1.7097" width="0.3048" layer="94"/>
<circle x="60.96" y="-29.5275" radius="1.7097" width="0.3048" layer="94"/>
<wire x1="60.96" y1="-8.255" x2="60.96" y2="-7.62" width="0.1524" layer="94"/>
<pin name="P4" x="60.96" y="-5.08" visible="pin" length="short" direction="pas" rot="R270"/>
<pin name="L4" x="60.96" y="-35.56" visible="pin" length="short" direction="pas" rot="R90"/>
<wire x1="67.31" y1="-15.24" x2="68.2625" y2="-15.24" width="0.3048" layer="94"/>
<wire x1="68.2625" y1="-15.24" x2="73.9775" y2="-15.24" width="0.3048" layer="94"/>
<wire x1="73.9775" y1="-15.24" x2="74.93" y2="-15.24" width="0.3048" layer="94"/>
<wire x1="74.93" y1="-15.24" x2="74.93" y2="-16.51" width="0.3048" layer="94"/>
<wire x1="74.93" y1="-16.51" x2="74.93" y2="-16.8275" width="0.3048" layer="94"/>
<wire x1="74.93" y1="-16.8275" x2="74.93" y2="-23.8125" width="0.3048" layer="94"/>
<wire x1="74.93" y1="-23.8125" x2="74.93" y2="-24.13" width="0.3048" layer="94"/>
<wire x1="74.93" y1="-24.13" x2="74.93" y2="-25.4" width="0.3048" layer="94"/>
<wire x1="74.93" y1="-25.4" x2="68.2625" y2="-25.4" width="0.3048" layer="94"/>
<wire x1="68.2625" y1="-25.4" x2="67.31" y2="-25.4" width="0.3048" layer="94"/>
<wire x1="67.31" y1="-25.4" x2="67.31" y2="-24.13" width="0.3048" layer="94"/>
<wire x1="67.31" y1="-24.13" x2="67.31" y2="-23.8125" width="0.3048" layer="94"/>
<wire x1="67.31" y1="-23.8125" x2="67.31" y2="-16.8275" width="0.3048" layer="94"/>
<wire x1="67.31" y1="-16.8275" x2="67.31" y2="-16.51" width="0.3048" layer="94"/>
<wire x1="67.31" y1="-16.51" x2="67.31" y2="-15.24" width="0.3048" layer="94"/>
<wire x1="67.31" y1="-16.51" x2="74.93" y2="-16.51" width="0.1524" layer="94"/>
<wire x1="67.31" y1="-24.13" x2="74.93" y2="-24.13" width="0.1524" layer="94"/>
<wire x1="67.31" y1="-16.8275" x2="74.93" y2="-16.8275" width="0.1524" layer="94"/>
<wire x1="67.31" y1="-23.8125" x2="74.93" y2="-23.8125" width="0.1524" layer="94"/>
<wire x1="68.2625" y1="-15.24" x2="68.2625" y2="-9.525" width="0.3048" layer="94"/>
<wire x1="68.2625" y1="-9.525" x2="69.5325" y2="-8.255" width="0.3048" layer="94"/>
<wire x1="69.5325" y1="-8.255" x2="71.12" y2="-8.255" width="0.3048" layer="94"/>
<wire x1="71.12" y1="-8.255" x2="72.7075" y2="-8.255" width="0.3048" layer="94"/>
<wire x1="72.7075" y1="-8.255" x2="73.9775" y2="-9.525" width="0.3048" layer="94"/>
<wire x1="73.9775" y1="-9.525" x2="73.9775" y2="-15.24" width="0.3048" layer="94"/>
<wire x1="73.9775" y1="-25.4" x2="73.9775" y2="-31.115" width="0.3048" layer="94"/>
<wire x1="73.9775" y1="-31.115" x2="72.7075" y2="-32.385" width="0.3048" layer="94"/>
<wire x1="72.7075" y1="-32.385" x2="71.12" y2="-32.385" width="0.3048" layer="94"/>
<wire x1="71.12" y1="-32.385" x2="69.5325" y2="-32.385" width="0.3048" layer="94"/>
<wire x1="69.5325" y1="-32.385" x2="68.2625" y2="-31.115" width="0.3048" layer="94"/>
<wire x1="68.2625" y1="-31.115" x2="68.2625" y2="-25.4" width="0.3048" layer="94"/>
<circle x="71.12" y="-11.1125" radius="1.7097" width="0.3048" layer="94"/>
<circle x="71.12" y="-29.5275" radius="1.7097" width="0.3048" layer="94"/>
<wire x1="71.12" y1="-8.255" x2="71.12" y2="-7.62" width="0.1524" layer="94"/>
<pin name="P5" x="71.12" y="-5.08" visible="pin" length="short" direction="pas" rot="R270"/>
<pin name="L5" x="71.12" y="-35.56" visible="pin" length="short" direction="pas" rot="R90"/>
<wire x1="77.47" y1="-15.24" x2="78.4225" y2="-15.24" width="0.3048" layer="94"/>
<wire x1="78.4225" y1="-15.24" x2="84.1375" y2="-15.24" width="0.3048" layer="94"/>
<wire x1="84.1375" y1="-15.24" x2="85.09" y2="-15.24" width="0.3048" layer="94"/>
<wire x1="85.09" y1="-15.24" x2="85.09" y2="-16.51" width="0.3048" layer="94"/>
<wire x1="85.09" y1="-16.51" x2="85.09" y2="-16.8275" width="0.3048" layer="94"/>
<wire x1="85.09" y1="-16.8275" x2="85.09" y2="-23.8125" width="0.3048" layer="94"/>
<wire x1="85.09" y1="-23.8125" x2="85.09" y2="-24.13" width="0.3048" layer="94"/>
<wire x1="85.09" y1="-24.13" x2="85.09" y2="-25.4" width="0.3048" layer="94"/>
<wire x1="85.09" y1="-25.4" x2="78.4225" y2="-25.4" width="0.3048" layer="94"/>
<wire x1="78.4225" y1="-25.4" x2="77.47" y2="-25.4" width="0.3048" layer="94"/>
<wire x1="77.47" y1="-25.4" x2="77.47" y2="-24.13" width="0.3048" layer="94"/>
<wire x1="77.47" y1="-24.13" x2="77.47" y2="-23.8125" width="0.3048" layer="94"/>
<wire x1="77.47" y1="-23.8125" x2="77.47" y2="-16.8275" width="0.3048" layer="94"/>
<wire x1="77.47" y1="-16.8275" x2="77.47" y2="-16.51" width="0.3048" layer="94"/>
<wire x1="77.47" y1="-16.51" x2="77.47" y2="-15.24" width="0.3048" layer="94"/>
<wire x1="77.47" y1="-16.51" x2="85.09" y2="-16.51" width="0.1524" layer="94"/>
<wire x1="77.47" y1="-24.13" x2="85.09" y2="-24.13" width="0.1524" layer="94"/>
<wire x1="77.47" y1="-16.8275" x2="85.09" y2="-16.8275" width="0.1524" layer="94"/>
<wire x1="77.47" y1="-23.8125" x2="85.09" y2="-23.8125" width="0.1524" layer="94"/>
<wire x1="78.4225" y1="-15.24" x2="78.4225" y2="-9.525" width="0.3048" layer="94"/>
<wire x1="78.4225" y1="-9.525" x2="79.6925" y2="-8.255" width="0.3048" layer="94"/>
<wire x1="79.6925" y1="-8.255" x2="81.28" y2="-8.255" width="0.3048" layer="94"/>
<wire x1="81.28" y1="-8.255" x2="82.8675" y2="-8.255" width="0.3048" layer="94"/>
<wire x1="82.8675" y1="-8.255" x2="84.1375" y2="-9.525" width="0.3048" layer="94"/>
<wire x1="84.1375" y1="-9.525" x2="84.1375" y2="-15.24" width="0.3048" layer="94"/>
<wire x1="84.1375" y1="-25.4" x2="84.1375" y2="-31.115" width="0.3048" layer="94"/>
<wire x1="84.1375" y1="-31.115" x2="82.8675" y2="-32.385" width="0.3048" layer="94"/>
<wire x1="82.8675" y1="-32.385" x2="81.28" y2="-32.385" width="0.3048" layer="94"/>
<wire x1="81.28" y1="-32.385" x2="79.6925" y2="-32.385" width="0.3048" layer="94"/>
<wire x1="79.6925" y1="-32.385" x2="78.4225" y2="-31.115" width="0.3048" layer="94"/>
<wire x1="78.4225" y1="-31.115" x2="78.4225" y2="-25.4" width="0.3048" layer="94"/>
<circle x="81.28" y="-11.1125" radius="1.7097" width="0.3048" layer="94"/>
<circle x="81.28" y="-29.5275" radius="1.7097" width="0.3048" layer="94"/>
<wire x1="81.28" y1="-8.255" x2="81.28" y2="-7.62" width="0.1524" layer="94"/>
<pin name="P6" x="81.28" y="-5.08" visible="pin" length="short" direction="pas" rot="R270"/>
<pin name="L6" x="81.28" y="-35.56" visible="pin" length="short" direction="pas" rot="R90"/>
<wire x1="87.63" y1="-15.24" x2="88.5825" y2="-15.24" width="0.3048" layer="94"/>
<wire x1="88.5825" y1="-15.24" x2="94.2975" y2="-15.24" width="0.3048" layer="94"/>
<wire x1="94.2975" y1="-15.24" x2="95.25" y2="-15.24" width="0.3048" layer="94"/>
<wire x1="95.25" y1="-15.24" x2="95.25" y2="-16.51" width="0.3048" layer="94"/>
<wire x1="95.25" y1="-16.51" x2="95.25" y2="-16.8275" width="0.3048" layer="94"/>
<wire x1="95.25" y1="-16.8275" x2="95.25" y2="-23.8125" width="0.3048" layer="94"/>
<wire x1="95.25" y1="-23.8125" x2="95.25" y2="-24.13" width="0.3048" layer="94"/>
<wire x1="95.25" y1="-24.13" x2="95.25" y2="-25.4" width="0.3048" layer="94"/>
<wire x1="95.25" y1="-25.4" x2="88.5825" y2="-25.4" width="0.3048" layer="94"/>
<wire x1="88.5825" y1="-25.4" x2="87.63" y2="-25.4" width="0.3048" layer="94"/>
<wire x1="87.63" y1="-25.4" x2="87.63" y2="-24.13" width="0.3048" layer="94"/>
<wire x1="87.63" y1="-24.13" x2="87.63" y2="-23.8125" width="0.3048" layer="94"/>
<wire x1="87.63" y1="-23.8125" x2="87.63" y2="-16.8275" width="0.3048" layer="94"/>
<wire x1="87.63" y1="-16.8275" x2="87.63" y2="-16.51" width="0.3048" layer="94"/>
<wire x1="87.63" y1="-16.51" x2="87.63" y2="-15.24" width="0.3048" layer="94"/>
<wire x1="87.63" y1="-16.51" x2="95.25" y2="-16.51" width="0.1524" layer="94"/>
<wire x1="87.63" y1="-24.13" x2="95.25" y2="-24.13" width="0.1524" layer="94"/>
<wire x1="87.63" y1="-16.8275" x2="95.25" y2="-16.8275" width="0.1524" layer="94"/>
<wire x1="87.63" y1="-23.8125" x2="95.25" y2="-23.8125" width="0.1524" layer="94"/>
<wire x1="88.5825" y1="-15.24" x2="88.5825" y2="-9.525" width="0.3048" layer="94"/>
<wire x1="88.5825" y1="-9.525" x2="89.8525" y2="-8.255" width="0.3048" layer="94"/>
<wire x1="89.8525" y1="-8.255" x2="91.44" y2="-8.255" width="0.3048" layer="94"/>
<wire x1="91.44" y1="-8.255" x2="93.0275" y2="-8.255" width="0.3048" layer="94"/>
<wire x1="93.0275" y1="-8.255" x2="94.2975" y2="-9.525" width="0.3048" layer="94"/>
<wire x1="94.2975" y1="-9.525" x2="94.2975" y2="-15.24" width="0.3048" layer="94"/>
<wire x1="94.2975" y1="-25.4" x2="94.2975" y2="-31.115" width="0.3048" layer="94"/>
<wire x1="94.2975" y1="-31.115" x2="93.0275" y2="-32.385" width="0.3048" layer="94"/>
<wire x1="93.0275" y1="-32.385" x2="91.44" y2="-32.385" width="0.3048" layer="94"/>
<wire x1="91.44" y1="-32.385" x2="89.8525" y2="-32.385" width="0.3048" layer="94"/>
<wire x1="89.8525" y1="-32.385" x2="88.5825" y2="-31.115" width="0.3048" layer="94"/>
<wire x1="88.5825" y1="-31.115" x2="88.5825" y2="-25.4" width="0.3048" layer="94"/>
<circle x="91.44" y="-11.1125" radius="1.7097" width="0.3048" layer="94"/>
<circle x="91.44" y="-29.5275" radius="1.7097" width="0.3048" layer="94"/>
<wire x1="91.44" y1="-8.255" x2="91.44" y2="-7.62" width="0.1524" layer="94"/>
<pin name="P7" x="91.44" y="-5.08" visible="pin" length="short" direction="pas" rot="R270"/>
<pin name="L7" x="91.44" y="-35.56" visible="pin" length="short" direction="pas" rot="R90"/>
<text x="26.9875" y="-17.30375" size="1.778" layer="94" align="top-left">F1</text>
<wire x1="106.68" y1="-18.415" x2="108.2675" y2="-18.415" width="0.254" layer="94"/>
<wire x1="108.2675" y1="-22.225" x2="106.68" y2="-22.225" width="0.254" layer="94"/>
<wire x1="104.775" y1="-17.78" x2="104.775" y2="-18.415" width="0.254" layer="94"/>
<wire x1="104.775" y1="-18.415" x2="104.775" y2="-19.05" width="0.254" layer="94"/>
<wire x1="104.775" y1="-19.685" x2="104.775" y2="-20.32" width="0.254" layer="94"/>
<wire x1="104.775" y1="-20.32" x2="104.775" y2="-20.955" width="0.254" layer="94"/>
<wire x1="104.775" y1="-22.86" x2="104.775" y2="-22.225" width="0.254" layer="94"/>
<wire x1="104.775" y1="-22.225" x2="104.775" y2="-21.59" width="0.254" layer="94"/>
<wire x1="104.14" y1="-17.78" x2="104.14" y2="-20.32" width="0.254" layer="94"/>
<wire x1="104.14" y1="-20.32" x2="104.14" y2="-22.86" width="0.254" layer="94"/>
<wire x1="106.68" y1="-20.32" x2="106.68" y2="-22.225" width="0.254" layer="94"/>
<wire x1="104.775" y1="-18.415" x2="106.68" y2="-18.415" width="0.254" layer="94"/>
<wire x1="104.775" y1="-22.225" x2="106.68" y2="-22.225" width="0.254" layer="94"/>
<wire x1="108.2675" y1="-22.225" x2="108.2675" y2="-20.32" width="0.254" layer="94"/>
<wire x1="108.2675" y1="-20.32" x2="108.2675" y2="-19.8755" width="0.254" layer="94"/>
<wire x1="108.2675" y1="-19.8755" x2="108.2675" y2="-18.415" width="0.254" layer="94"/>
<wire x1="108.9025" y1="-20.7645" x2="108.585" y2="-20.7645" width="0.254" layer="94"/>
<wire x1="108.585" y1="-20.7645" x2="107.95" y2="-20.7645" width="0.254" layer="94"/>
<wire x1="107.95" y1="-20.7645" x2="107.6325" y2="-20.7645" width="0.254" layer="94"/>
<wire x1="107.6325" y1="-20.7645" x2="108.2675" y2="-19.8755" width="0.254" layer="94"/>
<wire x1="108.2675" y1="-19.8755" x2="108.9025" y2="-20.7645" width="0.254" layer="94"/>
<wire x1="108.9025" y1="-19.8755" x2="108.2675" y2="-19.8755" width="0.254" layer="94"/>
<wire x1="108.2675" y1="-19.8755" x2="107.6325" y2="-19.8755" width="0.254" layer="94"/>
<wire x1="108.585" y1="-20.7645" x2="108.2675" y2="-20.32" width="0.254" layer="94"/>
<wire x1="108.2675" y1="-20.32" x2="107.95" y2="-20.7645" width="0.254" layer="94"/>
<wire x1="108.585" y1="-20.6375" x2="108.2675" y2="-20.32" width="0.254" layer="94"/>
<wire x1="108.2675" y1="-20.32" x2="107.95" y2="-20.6375" width="0.254" layer="94"/>
<wire x1="106.68" y1="-20.32" x2="105.0925" y2="-20.32" width="0.254" layer="94"/>
<wire x1="104.775" y1="-20.32" x2="105.7275" y2="-20.6375" width="0.254" layer="94"/>
<wire x1="104.775" y1="-20.32" x2="105.7275" y2="-20.0025" width="0.254" layer="94"/>
<wire x1="105.7275" y1="-20.6375" x2="105.7275" y2="-20.0025" width="0.254" layer="94"/>
<wire x1="105.7275" y1="-20.0025" x2="105.0925" y2="-20.32" width="0.254" layer="94"/>
<wire x1="105.0925" y1="-20.32" x2="105.7275" y2="-20.6375" width="0.254" layer="94"/>
<wire x1="105.7275" y1="-20.6375" x2="105.283" y2="-20.193" width="0.254" layer="94"/>
<circle x="106.68" y="-18.415" radius="0.254" width="0.254" layer="94"/>
<circle x="106.68" y="-22.225" radius="0.254" width="0.254" layer="94"/>
<pin name="!VENTILATION_ON" x="-5.08" y="-20.32" visible="pad" length="middle" direction="in"/>
<wire x1="104.14" y1="-20.32" x2="96.52" y2="-20.32" width="0.1524" layer="94"/>
<wire x1="106.68" y1="-18.415" x2="106.68" y2="-12.7" width="0.1524" layer="94"/>
<pin name="!FAILURE" x="127" y="-12.7" visible="pad" length="middle" direction="oc" rot="R180"/>
<wire x1="0" y1="-20.32" x2="25.4" y2="-20.32" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-0.3175" width="0.3048" layer="94"/>
<wire x1="0" y1="-0.3175" x2="0" y2="-0.635" width="0.3048" layer="94"/>
<wire x1="0" y1="-0.635" x2="0" y2="-0.9525" width="0.3048" layer="94"/>
<wire x1="0" y1="-0.9525" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="-20.32" width="0.3048" layer="94"/>
<wire x1="0" y1="-20.32" x2="0" y2="-40.005" width="0.3048" layer="94"/>
<wire x1="0" y1="-40.005" x2="0.635" y2="-40.64" width="0.3048" layer="94"/>
<wire x1="0.635" y1="-40.64" x2="121.92" y2="-40.64" width="0.3048" layer="94"/>
<wire x1="121.92" y1="-40.64" x2="121.92" y2="-12.7" width="0.3048" layer="94"/>
<wire x1="121.92" y1="-12.7" x2="121.92" y2="-0.635" width="0.3048" layer="94"/>
<wire x1="121.92" y1="-0.635" x2="121.285" y2="0" width="0.3048" layer="94"/>
<text x="21.59" y="-17.78" size="1.778" layer="94" rot="R180">!VENTILATION_ON</text>
<text x="109.22" y="-10.16" size="1.778" layer="94" rot="MR180">!FAILURE</text>
<wire x1="121.285" y1="0" x2="1.27" y2="0" width="0.3048" layer="94"/>
<wire x1="1.27" y1="0" x2="0.9525" y2="0" width="0.3048" layer="94"/>
<wire x1="0.9525" y1="0" x2="0.635" y2="0" width="0.3048" layer="94"/>
<wire x1="0.635" y1="0" x2="0.3175" y2="0" width="0.3048" layer="94"/>
<wire x1="0.3175" y1="0" x2="0" y2="0" width="0.3048" layer="94"/>
<wire x1="121.92" y1="-12.7" x2="106.68" y2="-12.7" width="0.1524" layer="94"/>
<wire x1="106.68" y1="-22.225" x2="106.68" y2="-27.94" width="0.1524" layer="94"/>
<wire x1="105.41" y1="-27.94" x2="106.68" y2="-27.94" width="0.4064" layer="94"/>
<wire x1="106.68" y1="-27.94" x2="107.95" y2="-27.94" width="0.4064" layer="94"/>
<text x="119.38" y="-31.75" size="1.27" layer="97" font="fixed" align="top-right">!FAILURE!:
L - pokud je podproud
nebo přepálená pojistka
(signalizace chyby)</text>
<text x="1.27" y="-31.75" size="1.27" layer="97" font="fixed" align="top-left">!VENTILATION_ON!:
L - odblokuje hlídání
podproudu přes pojistky
(ventilace v chodu)</text>
<text x="0" y="2.54" size="1.778" layer="94" rot="MR180">- ventilátorové pojistky</text>
<wire x1="0" y1="-40.005" x2="121.285" y2="-40.005" width="0.3048" layer="94"/>
<wire x1="121.285" y1="-40.005" x2="121.285" y2="0" width="0.3048" layer="94"/>
<wire x1="121.285" y1="-40.005" x2="121.92" y2="-40.64" width="0.3048" layer="94"/>
<text x="123.19" y="-40.64" size="1.27" layer="94">Část desky:</text>
<text x="137.16" y="-40.64" size="1.27" layer="94">&gt;GATE</text>
<text x="123.19" y="-38.1" size="1.27" layer="94">Karta ABRA:</text>
<text x="137.16" y="-38.1" size="1.27" layer="94">&gt;ABRA</text>
<text x="123.19" y="-35.56" size="1.27" layer="94">Označení desky:</text>
<text x="137.16" y="-35.56" size="1.27" layer="94">&gt;VALUE</text>
<text x="120.015" y="-1.27" size="1.27" layer="95" align="top-right">&gt;PART</text>
<polygon width="0.1524" layer="94">
<vertex x="98.425" y="-19.685"/>
<vertex x="98.425" y="-20.955"/>
<vertex x="100.33" y="-20.32"/>
</polygon>
<polygon width="0.1524" layer="94">
<vertex x="23.495" y="-19.685"/>
<vertex x="23.495" y="-20.955"/>
<vertex x="25.4" y="-20.32"/>
</polygon>
<wire x1="30.48" y1="-32.385" x2="30.48" y2="-33.02" width="0.1524" layer="94"/>
<wire x1="40.64" y1="-32.385" x2="40.64" y2="-33.02" width="0.1524" layer="94"/>
<wire x1="50.8" y1="-32.385" x2="50.8" y2="-33.02" width="0.1524" layer="94"/>
<wire x1="60.96" y1="-32.385" x2="60.96" y2="-33.02" width="0.1524" layer="94"/>
<wire x1="71.12" y1="-32.385" x2="71.12" y2="-33.02" width="0.1524" layer="94"/>
<wire x1="81.28" y1="-32.385" x2="81.28" y2="-33.02" width="0.1524" layer="94"/>
<wire x1="91.44" y1="-32.385" x2="91.44" y2="-33.02" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.27" x2="1.27" y2="0" width="0.3048" layer="94"/>
<wire x1="0" y1="-0.9525" x2="0.9525" y2="0" width="0.3048" layer="94"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.3048" layer="94"/>
<wire x1="0" y1="-0.3175" x2="0.3175" y2="0" width="0.3048" layer="94"/>
<text x="33.81375" y="-23.495" size="1.27" layer="94" align="bottom-right">&gt;PROUD</text>
<wire x1="40.64" y1="-18.7325" x2="40.64" y2="-21.59" width="0.1524" layer="94"/>
<wire x1="40.3225" y1="-19.3675" x2="40.3225" y2="-20.955" width="0.1524" layer="94"/>
<wire x1="40.3225" y1="-20.955" x2="40.9575" y2="-20.955" width="0.1524" layer="94"/>
<wire x1="40.9575" y1="-20.955" x2="40.9575" y2="-19.3675" width="0.1524" layer="94"/>
<wire x1="40.9575" y1="-19.3675" x2="40.3225" y2="-19.3675" width="0.1524" layer="94"/>
<text x="37.1475" y="-17.30375" size="1.778" layer="94" align="top-left">F2</text>
<text x="43.97375" y="-23.495" size="1.27" layer="94" align="bottom-right">&gt;PROUD</text>
<wire x1="50.8" y1="-18.7325" x2="50.8" y2="-21.59" width="0.1524" layer="94"/>
<wire x1="50.4825" y1="-19.3675" x2="50.4825" y2="-20.955" width="0.1524" layer="94"/>
<wire x1="50.4825" y1="-20.955" x2="51.1175" y2="-20.955" width="0.1524" layer="94"/>
<wire x1="51.1175" y1="-20.955" x2="51.1175" y2="-19.3675" width="0.1524" layer="94"/>
<wire x1="51.1175" y1="-19.3675" x2="50.4825" y2="-19.3675" width="0.1524" layer="94"/>
<text x="47.3075" y="-17.30375" size="1.778" layer="94" align="top-left">F3</text>
<text x="54.13375" y="-23.495" size="1.27" layer="94" align="bottom-right">&gt;PROUD</text>
<wire x1="60.96" y1="-18.7325" x2="60.96" y2="-21.59" width="0.1524" layer="94"/>
<wire x1="60.6425" y1="-19.3675" x2="60.6425" y2="-20.955" width="0.1524" layer="94"/>
<wire x1="60.6425" y1="-20.955" x2="61.2775" y2="-20.955" width="0.1524" layer="94"/>
<wire x1="61.2775" y1="-20.955" x2="61.2775" y2="-19.3675" width="0.1524" layer="94"/>
<wire x1="61.2775" y1="-19.3675" x2="60.6425" y2="-19.3675" width="0.1524" layer="94"/>
<text x="57.4675" y="-17.30375" size="1.778" layer="94" align="top-left">F4</text>
<text x="64.29375" y="-23.495" size="1.27" layer="94" align="bottom-right">&gt;PROUD</text>
<wire x1="71.12" y1="-18.7325" x2="71.12" y2="-21.59" width="0.1524" layer="94"/>
<wire x1="70.8025" y1="-19.3675" x2="70.8025" y2="-20.955" width="0.1524" layer="94"/>
<wire x1="70.8025" y1="-20.955" x2="71.4375" y2="-20.955" width="0.1524" layer="94"/>
<wire x1="71.4375" y1="-20.955" x2="71.4375" y2="-19.3675" width="0.1524" layer="94"/>
<wire x1="71.4375" y1="-19.3675" x2="70.8025" y2="-19.3675" width="0.1524" layer="94"/>
<text x="67.6275" y="-17.30375" size="1.778" layer="94" align="top-left">F5</text>
<text x="74.45375" y="-23.495" size="1.27" layer="94" align="bottom-right">&gt;PROUD</text>
<wire x1="81.28" y1="-18.7325" x2="81.28" y2="-21.59" width="0.1524" layer="94"/>
<wire x1="80.9625" y1="-19.3675" x2="80.9625" y2="-20.955" width="0.1524" layer="94"/>
<wire x1="80.9625" y1="-20.955" x2="81.5975" y2="-20.955" width="0.1524" layer="94"/>
<wire x1="81.5975" y1="-20.955" x2="81.5975" y2="-19.3675" width="0.1524" layer="94"/>
<wire x1="81.5975" y1="-19.3675" x2="80.9625" y2="-19.3675" width="0.1524" layer="94"/>
<text x="77.7875" y="-17.30375" size="1.778" layer="94" align="top-left">F6</text>
<text x="84.61375" y="-23.495" size="1.27" layer="94" align="bottom-right">&gt;PROUD</text>
<wire x1="91.44" y1="-18.7325" x2="91.44" y2="-21.59" width="0.1524" layer="94"/>
<wire x1="91.1225" y1="-19.3675" x2="91.1225" y2="-20.955" width="0.1524" layer="94"/>
<wire x1="91.1225" y1="-20.955" x2="91.7575" y2="-20.955" width="0.1524" layer="94"/>
<wire x1="91.7575" y1="-20.955" x2="91.7575" y2="-19.3675" width="0.1524" layer="94"/>
<wire x1="91.7575" y1="-19.3675" x2="91.1225" y2="-19.3675" width="0.1524" layer="94"/>
<text x="87.9475" y="-17.30375" size="1.778" layer="94" align="top-left">F7</text>
<text x="94.77375" y="-23.495" size="1.27" layer="94" align="bottom-right">&gt;PROUD</text>
<text x="0" y="5.08" size="1.778" layer="94" rot="MR180">Deska 7P -</text>
</symbol>
<symbol name="7P-OTACKY">
<wire x1="20.32" y1="-12.7" x2="20.32" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="20.32" y1="-10.16" x2="20.32" y2="-7.62" width="0.1524" layer="94"/>
<wire x1="20.32" y1="-10.16" x2="22.86" y2="-12.7" width="0.1524" layer="94"/>
<wire x1="20.32" y1="-10.16" x2="22.86" y2="-7.62" width="0.1524" layer="94"/>
<wire x1="22.86" y1="-12.7" x2="22.225" y2="-11.43" width="0.1524" layer="94"/>
<wire x1="22.86" y1="-12.7" x2="21.59" y2="-12.065" width="0.1524" layer="94"/>
<wire x1="22.86" y1="-22.225" x2="22.86" y2="-25.4" width="0.1524" layer="94"/>
<wire x1="21.59" y1="-25.4" x2="22.86" y2="-25.4" width="0.4064" layer="94"/>
<wire x1="22.86" y1="-25.4" x2="24.13" y2="-25.4" width="0.4064" layer="94"/>
<wire x1="22.86" y1="-12.7" x2="22.86" y2="-15.24" width="0.1524" layer="94"/>
<wire x1="22.86" y1="-15.24" x2="22.86" y2="-18.415" width="0.1524" layer="94"/>
<wire x1="22.86" y1="-15.24" x2="45.72" y2="-15.24" width="0.1524" layer="94"/>
<circle x="22.86" y="-15.24" radius="0.4064" width="0" layer="94"/>
<wire x1="22.225" y1="-18.415" x2="22.225" y2="-22.225" width="0.1524" layer="94"/>
<wire x1="22.225" y1="-22.225" x2="23.495" y2="-22.225" width="0.1524" layer="94"/>
<wire x1="23.495" y1="-22.225" x2="23.495" y2="-18.415" width="0.1524" layer="94"/>
<wire x1="23.495" y1="-18.415" x2="22.86" y2="-18.415" width="0.1524" layer="94"/>
<wire x1="22.86" y1="-18.415" x2="22.225" y2="-18.415" width="0.1524" layer="94"/>
<pin name="EBM_REG_OUT" x="50.8" y="-15.24" visible="pad" length="middle" direction="out" rot="R180"/>
<text x="43.18" y="-12.7" size="1.778" layer="94" align="top-right">EBM_REG_OUT</text>
<pin name="EBM_REG_IN" x="-5.08" y="-10.16" visible="pad" length="middle" direction="in"/>
<wire x1="20.32" y1="-10.16" x2="0" y2="-10.16" width="0.1524" layer="94"/>
<text x="2.54" y="-7.62" size="1.778" layer="94" rot="MR180">EBM_REG_IN</text>
<wire x1="22.86" y1="-7.62" x2="22.86" y2="-5.08" width="0.1524" layer="94"/>
<circle x="22.86" y="-4.445" radius="0.635" width="0.1524" layer="94"/>
<wire x1="22.225" y1="-2.54" x2="23.495" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="22.86" y1="-1.905" x2="22.86" y2="-3.175" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-0.3175" width="0.3048" layer="94"/>
<wire x1="0" y1="-0.3175" x2="0" y2="-0.635" width="0.3048" layer="94"/>
<wire x1="0" y1="-0.635" x2="0" y2="-0.9525" width="0.3048" layer="94"/>
<wire x1="0" y1="-0.9525" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="-10.16" width="0.3048" layer="94"/>
<wire x1="0" y1="-10.16" x2="0" y2="-34.925" width="0.3048" layer="94"/>
<wire x1="0" y1="-34.925" x2="0.635" y2="-35.56" width="0.3048" layer="94"/>
<wire x1="0.635" y1="-35.56" x2="45.72" y2="-35.56" width="0.3048" layer="94"/>
<wire x1="45.72" y1="-35.56" x2="45.72" y2="-15.24" width="0.3048" layer="94"/>
<wire x1="45.72" y1="-15.24" x2="45.72" y2="-0.635" width="0.3048" layer="94"/>
<wire x1="45.72" y1="-0.635" x2="45.085" y2="0" width="0.3048" layer="94"/>
<text x="2.54" y="-25.4" size="1.27" layer="97" font="fixed" align="top-left">!VENTILATION_ON!:
- při L se kopíruje napětí ze vstupu
  na výstup (ventilace v chodu)
- při H je výstup uzemněn
  (ventilátory zastaveny)</text>
<text x="0" y="2.54" size="1.778" layer="94" rot="MR180">Deska 7P - řízení otáček ventilátorů</text>
<wire x1="45.085" y1="0" x2="1.27" y2="0" width="0.3048" layer="94"/>
<wire x1="1.27" y1="0" x2="0.9525" y2="0" width="0.3048" layer="94"/>
<wire x1="0.9525" y1="0" x2="0.635" y2="0" width="0.3048" layer="94"/>
<wire x1="0.635" y1="0" x2="0.3175" y2="0" width="0.3048" layer="94"/>
<wire x1="0.3175" y1="0" x2="0" y2="0" width="0.3048" layer="94"/>
<wire x1="45.085" y1="0" x2="45.085" y2="-34.925" width="0.3048" layer="94"/>
<wire x1="45.085" y1="-34.925" x2="0" y2="-34.925" width="0.3048" layer="94"/>
<wire x1="45.085" y1="-34.925" x2="45.72" y2="-35.56" width="0.3048" layer="94"/>
<text x="46.99" y="-35.56" size="1.27" layer="94">Část desky:</text>
<text x="60.96" y="-35.56" size="1.27" layer="94">&gt;GATE</text>
<text x="46.99" y="-33.02" size="1.27" layer="94">Karta ABRA:</text>
<text x="60.96" y="-33.02" size="1.27" layer="94">&gt;ABRA</text>
<text x="46.99" y="-30.48" size="1.27" layer="94">Označení desky:</text>
<text x="60.96" y="-30.48" size="1.27" layer="94">&gt;VALUE</text>
<text x="43.815" y="-1.27" size="1.27" layer="95" align="top-right">&gt;PART</text>
<wire x1="0" y1="-1.27" x2="1.27" y2="0" width="0.3048" layer="94"/>
<wire x1="0" y1="-0.9525" x2="0.9525" y2="0" width="0.3048" layer="94"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.3048" layer="94"/>
<wire x1="0" y1="-0.3175" x2="0.3175" y2="0" width="0.3048" layer="94"/>
</symbol>
<symbol name="7P-HOUKACKA">
<text x="0" y="2.54" size="1.778" layer="94" rot="MR180">Deska 7P - spínání houkačky</text>
<wire x1="22.86" y1="-14.605" x2="24.4475" y2="-14.605" width="0.254" layer="94"/>
<wire x1="24.4475" y1="-14.605" x2="24.4475" y2="-12.7" width="0.254" layer="94"/>
<wire x1="24.4475" y1="-12.7" x2="24.4475" y2="-12.2555" width="0.254" layer="94"/>
<wire x1="24.4475" y1="-12.2555" x2="24.4475" y2="-10.795" width="0.254" layer="94"/>
<wire x1="24.4475" y1="-10.795" x2="22.86" y2="-10.795" width="0.254" layer="94"/>
<wire x1="23.8125" y1="-13.1445" x2="24.13" y2="-13.1445" width="0.254" layer="94"/>
<wire x1="24.13" y1="-13.1445" x2="24.765" y2="-13.1445" width="0.254" layer="94"/>
<wire x1="24.765" y1="-13.1445" x2="25.0825" y2="-13.1445" width="0.254" layer="94"/>
<wire x1="25.0825" y1="-13.1445" x2="24.4475" y2="-12.2555" width="0.254" layer="94"/>
<wire x1="24.4475" y1="-12.2555" x2="23.8125" y2="-13.1445" width="0.254" layer="94"/>
<wire x1="23.8125" y1="-12.2555" x2="24.4475" y2="-12.2555" width="0.254" layer="94"/>
<wire x1="24.4475" y1="-12.2555" x2="25.0825" y2="-12.2555" width="0.254" layer="94"/>
<wire x1="20.955" y1="-15.24" x2="20.955" y2="-14.605" width="0.254" layer="94"/>
<wire x1="20.955" y1="-14.605" x2="20.955" y2="-13.97" width="0.254" layer="94"/>
<wire x1="20.955" y1="-13.335" x2="20.955" y2="-12.7" width="0.254" layer="94"/>
<wire x1="20.955" y1="-12.7" x2="20.955" y2="-12.065" width="0.254" layer="94"/>
<wire x1="20.955" y1="-10.16" x2="20.955" y2="-10.795" width="0.254" layer="94"/>
<wire x1="20.955" y1="-10.795" x2="20.955" y2="-11.43" width="0.254" layer="94"/>
<wire x1="20.32" y1="-15.24" x2="20.32" y2="-12.7" width="0.254" layer="94"/>
<wire x1="20.32" y1="-12.7" x2="20.32" y2="-10.16" width="0.254" layer="94"/>
<wire x1="20.955" y1="-12.7" x2="22.5425" y2="-12.7" width="0.254" layer="94"/>
<wire x1="22.5425" y1="-12.7" x2="22.86" y2="-12.7" width="0.1524" layer="94"/>
<wire x1="22.86" y1="-12.7" x2="22.86" y2="-10.795" width="0.254" layer="94"/>
<wire x1="20.955" y1="-14.605" x2="22.86" y2="-14.605" width="0.254" layer="94"/>
<wire x1="20.955" y1="-10.795" x2="22.86" y2="-10.795" width="0.254" layer="94"/>
<wire x1="22.86" y1="-12.7" x2="21.9075" y2="-13.0175" width="0.254" layer="94"/>
<wire x1="22.86" y1="-12.7" x2="21.9075" y2="-12.3825" width="0.254" layer="94"/>
<wire x1="21.9075" y1="-13.0175" x2="21.9075" y2="-12.3825" width="0.254" layer="94"/>
<wire x1="21.9075" y1="-12.3825" x2="22.5425" y2="-12.7" width="0.254" layer="94"/>
<wire x1="22.5425" y1="-12.7" x2="21.9075" y2="-13.0175" width="0.254" layer="94"/>
<wire x1="21.9075" y1="-13.0175" x2="22.352" y2="-12.573" width="0.254" layer="94"/>
<wire x1="24.13" y1="-13.1445" x2="24.4475" y2="-12.7" width="0.254" layer="94"/>
<wire x1="24.4475" y1="-12.7" x2="24.765" y2="-13.1445" width="0.254" layer="94"/>
<wire x1="24.13" y1="-13.0175" x2="24.4475" y2="-12.7" width="0.254" layer="94"/>
<wire x1="24.4475" y1="-12.7" x2="24.765" y2="-13.0175" width="0.254" layer="94"/>
<circle x="22.86" y="-14.605" radius="0.254" width="0.254" layer="94"/>
<circle x="22.86" y="-10.795" radius="0.254" width="0.254" layer="94"/>
<wire x1="22.86" y1="-10.795" x2="22.86" y2="-7.62" width="0.1524" layer="94"/>
<circle x="22.86" y="-6.985" radius="0.635" width="0.1524" layer="94"/>
<wire x1="22.225" y1="-5.08" x2="23.495" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="22.86" y1="-4.445" x2="22.86" y2="-5.715" width="0.1524" layer="94"/>
<wire x1="22.86" y1="-14.605" x2="22.86" y2="-17.78" width="0.1524" layer="94"/>
<wire x1="22.86" y1="-17.78" x2="30.48" y2="-17.78" width="0.1524" layer="94"/>
<wire x1="21.2725" y1="-31.115" x2="22.86" y2="-31.115" width="0.254" layer="94"/>
<wire x1="22.86" y1="-31.115" x2="24.4475" y2="-31.115" width="0.254" layer="94"/>
<wire x1="21.9075" y1="-31.75" x2="23.8125" y2="-31.75" width="0.254" layer="94"/>
<wire x1="22.5425" y1="-32.385" x2="23.1775" y2="-32.385" width="0.254" layer="94"/>
<wire x1="22.86" y1="-27.94" x2="22.86" y2="-31.115" width="0.1524" layer="94"/>
<wire x1="22.86" y1="-27.94" x2="30.48" y2="-27.94" width="0.1524" layer="94"/>
<pin name="!HORN_ON" x="-5.08" y="-12.7" length="middle" direction="in"/>
<pin name="HORN" x="35.56" y="-17.78" visible="pad" length="middle" direction="out" rot="R180"/>
<pin name="HORN_RETURN" x="35.56" y="-27.94" visible="pad" length="middle" direction="pas" rot="R180"/>
<wire x1="0" y1="0" x2="0" y2="-0.3175" width="0.3048" layer="94"/>
<wire x1="0" y1="-0.3175" x2="0" y2="-0.635" width="0.3048" layer="94"/>
<wire x1="0" y1="-0.635" x2="0" y2="-0.9525" width="0.3048" layer="94"/>
<wire x1="0" y1="-0.9525" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="-37.465" width="0.3048" layer="94"/>
<wire x1="0" y1="-37.465" x2="0.635" y2="-38.1" width="0.3048" layer="94"/>
<wire x1="0.635" y1="-38.1" x2="30.48" y2="-38.1" width="0.3048" layer="94"/>
<wire x1="30.48" y1="-38.1" x2="30.48" y2="-27.94" width="0.3048" layer="94"/>
<wire x1="30.48" y1="-27.94" x2="30.48" y2="-17.78" width="0.3048" layer="94"/>
<wire x1="30.48" y1="-17.78" x2="30.48" y2="-0.635" width="0.3048" layer="94"/>
<wire x1="30.48" y1="-0.635" x2="29.845" y2="0" width="0.3048" layer="94"/>
<wire x1="29.845" y1="0" x2="1.27" y2="0" width="0.3048" layer="94"/>
<wire x1="1.27" y1="0" x2="0.9525" y2="0" width="0.3048" layer="94"/>
<wire x1="0.9525" y1="0" x2="0.635" y2="0" width="0.3048" layer="94"/>
<wire x1="0.635" y1="0" x2="0.3175" y2="0" width="0.3048" layer="94"/>
<wire x1="0.3175" y1="0" x2="0" y2="0" width="0.3048" layer="94"/>
<wire x1="22.225" y1="-23.495" x2="22.86" y2="-23.495" width="0.254" layer="94"/>
<wire x1="22.86" y1="-23.495" x2="23.495" y2="-23.495" width="0.254" layer="94"/>
<wire x1="23.495" y1="-23.495" x2="22.86" y2="-22.225" width="0.254" layer="94"/>
<wire x1="22.86" y1="-22.225" x2="22.225" y2="-23.495" width="0.254" layer="94"/>
<wire x1="22.225" y1="-22.225" x2="22.86" y2="-22.225" width="0.254" layer="94"/>
<wire x1="22.86" y1="-22.225" x2="23.495" y2="-22.225" width="0.254" layer="94"/>
<wire x1="22.86" y1="-23.495" x2="22.86" y2="-22.225" width="0.1524" layer="94"/>
<wire x1="22.86" y1="-22.225" x2="22.86" y2="-17.78" width="0.1524" layer="94"/>
<wire x1="22.86" y1="-23.495" x2="22.86" y2="-27.94" width="0.1524" layer="94"/>
<circle x="22.86" y="-27.94" radius="0.4064" width="0" layer="94"/>
<circle x="22.86" y="-17.78" radius="0.4064" width="0" layer="94"/>
<wire x1="20.32" y1="-12.7" x2="12.7" y2="-12.7" width="0.1524" layer="94"/>
<text x="2.54" y="-31.75" size="1.27" layer="97" font="fixed" align="top-left">!HORN_ON:
L - aktivuje houkačku</text>
<wire x1="0" y1="-37.465" x2="29.845" y2="-37.465" width="0.3048" layer="94"/>
<wire x1="29.845" y1="-37.465" x2="29.845" y2="0" width="0.3048" layer="94"/>
<wire x1="29.845" y1="-37.465" x2="30.48" y2="-38.1" width="0.3048" layer="94"/>
<text x="31.75" y="-38.1" size="1.27" layer="94">Část desky:</text>
<text x="45.72" y="-38.1" size="1.27" layer="94">&gt;GATE</text>
<text x="31.75" y="-35.56" size="1.27" layer="94">Karta ABRA:</text>
<text x="45.72" y="-35.56" size="1.27" layer="94">&gt;ABRA</text>
<text x="31.75" y="-33.02" size="1.27" layer="94">Označení desky:</text>
<text x="45.72" y="-33.02" size="1.27" layer="94">&gt;VALUE</text>
<text x="28.575" y="-1.27" size="1.27" layer="95" align="top-right">&gt;PART</text>
<polygon width="0.1524" layer="94">
<vertex x="16.51" y="-12.7"/>
<vertex x="14.605" y="-12.065"/>
<vertex x="14.605" y="-13.335"/>
</polygon>
<wire x1="0" y1="-1.27" x2="1.27" y2="0" width="0.3048" layer="94"/>
<wire x1="0" y1="-0.9525" x2="0.9525" y2="0" width="0.3048" layer="94"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.3048" layer="94"/>
<wire x1="0" y1="-0.3175" x2="0.3175" y2="0" width="0.3048" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="7P" prefix="DPS">
<gates>
<gate name="A..." symbol="7P-NAPAJENI" x="0" y="0" addlevel="must"/>
<gate name=".B.." symbol="7P-POJISTKY" x="0" y="-43.18" addlevel="always"/>
<gate name="..C." symbol="7P-OTACKY" x="0" y="-91.44" addlevel="always"/>
<gate name="...D" symbol="7P-HOUKACKA" x="0" y="-134.62" addlevel="always"/>
</gates>
<devices>
<device name="_V0.2" package="7P">
<connects>
<connect gate="...D" pin="!HORN_ON" pad="2"/>
<connect gate="...D" pin="HORN" pad="5"/>
<connect gate="...D" pin="HORN_RETURN" pad="10"/>
<connect gate="..C." pin="EBM_REG_IN" pad="7"/>
<connect gate="..C." pin="EBM_REG_OUT" pad="6"/>
<connect gate=".B.." pin="!FAILURE" pad="9"/>
<connect gate=".B.." pin="!VENTILATION_ON" pad="1"/>
<connect gate=".B.." pin="L1" pad="L1"/>
<connect gate=".B.." pin="L2" pad="L2"/>
<connect gate=".B.." pin="L3" pad="L3"/>
<connect gate=".B.." pin="L4" pad="L4"/>
<connect gate=".B.." pin="L5" pad="L5"/>
<connect gate=".B.." pin="L6" pad="L6"/>
<connect gate=".B.." pin="L7" pad="L7"/>
<connect gate=".B.." pin="P1" pad="P1"/>
<connect gate=".B.." pin="P2" pad="P2"/>
<connect gate=".B.." pin="P3" pad="P3"/>
<connect gate=".B.." pin="P4" pad="P4"/>
<connect gate=".B.." pin="P5" pad="P5"/>
<connect gate=".B.." pin="P6" pad="P6"/>
<connect gate=".B.." pin="P7" pad="P7"/>
<connect gate="A..." pin="GND" pad="8"/>
<connect gate="A..." pin="PGND" pad="PGND"/>
<connect gate="A..." pin="PGND@3" pad="3"/>
<connect gate="A..." pin="SUPPLY" pad="4"/>
</connects>
<technologies>
<technology name="">
<attribute name="ABRA" value="VAUV0120"/>
<attribute name="PROUD" value="30A"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Servomotory">
<packages>
</packages>
<symbols>
<symbol name="SERVO">
<text x="0" y="1.27" size="2.54" layer="95" align="bottom-center">SERVO</text>
<text x="0" y="-2.54" size="2.54" layer="95" align="bottom-center">MOTOR</text>
<text x="0" y="-6.35" size="1.4224" layer="96" align="bottom-center">-----</text>
<wire x1="-10.16" y1="7.62" x2="10.16" y2="7.62" width="0.4064" layer="94"/>
<wire x1="10.16" y1="7.62" x2="10.16" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="10.16" y1="-7.62" x2="-10.16" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="-10.16" y1="-7.62" x2="-10.16" y2="7.62" width="0.4064" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SERVO" prefix="SERVO">
<gates>
<gate name="G$1" symbol="SERVO" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="R">
<packages>
<package name="0207/10">
<wire x1="5.08" y1="0" x2="4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-5.08" y1="0" x2="-4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.127" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.127" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.127" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.127" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.127" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.127" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.127" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.127" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.127" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.127" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.127" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.127" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.127" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.127" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.127" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.127" layer="21"/>
<wire x1="-2.667" y1="0.889" x2="-2.667" y2="-0.889" width="0.0508" layer="21"/>
<wire x1="-2.921" y1="0.889" x2="-2.921" y2="-0.889" width="0.0508" layer="21"/>
<wire x1="2.921" y1="0.889" x2="2.921" y2="-0.889" width="0.0508" layer="21"/>
<wire x1="2.667" y1="0.889" x2="2.667" y2="-0.889" width="0.0508" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" diameter="1.6002" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" diameter="1.6002" shape="octagon"/>
<text x="-3.048" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.2606" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="R">
<wire x1="-3.81" y1="-0.889" x2="3.81" y2="-0.889" width="0.254" layer="94"/>
<wire x1="3.81" y1="-0.889" x2="3.81" y2="0" width="0.254" layer="94"/>
<wire x1="3.81" y1="0.889" x2="-3.81" y2="0.889" width="0.254" layer="94"/>
<wire x1="-3.81" y1="-0.889" x2="-3.81" y2="0" width="0.254" layer="94"/>
<wire x1="3.81" y1="0" x2="5.08" y2="0" width="0.1524" layer="94"/>
<wire x1="3.81" y1="0" x2="3.81" y2="0.889" width="0.254" layer="94"/>
<wire x1="-5.08" y1="0" x2="-3.81" y2="0" width="0.1524" layer="94"/>
<wire x1="-3.81" y1="0" x2="-3.81" y2="0.889" width="0.254" layer="94"/>
<text x="-3.81" y="1.3716" size="1.524" layer="95">&gt;NAME</text>
<text x="-3.81" y="-2.921" size="1.524" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="point" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="point" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="0207/10" prefix="R" uservalue="yes">
<gates>
<gate name="1" symbol="R" x="0" y="0"/>
</gates>
<devices>
<device name="" package="0207/10">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="TRIMPOT">
<packages>
<package name="RS3">
<wire x1="4.826" y1="-4.826" x2="4.826" y2="4.8006" width="0.127" layer="21"/>
<wire x1="4.826" y1="4.8006" x2="-4.826" y2="4.8006" width="0.127" layer="21"/>
<wire x1="-4.826" y1="4.8006" x2="-4.826" y2="-4.826" width="0.127" layer="21"/>
<wire x1="-0.3302" y1="-1.5748" x2="0.3048" y2="-1.5748" width="0.254" layer="21"/>
<wire x1="0.3048" y1="-1.5748" x2="0.3048" y2="0.635" width="0.254" layer="21"/>
<wire x1="0.3048" y1="0.635" x2="0.381" y2="0.635" width="0.254" layer="21"/>
<wire x1="0.635" y1="0.635" x2="0" y2="1.905" width="0.254" layer="21"/>
<wire x1="0" y1="1.905" x2="-0.635" y2="0.635" width="0.254" layer="21"/>
<wire x1="-0.3302" y1="0.635" x2="-0.3302" y2="-1.5748" width="0.254" layer="21"/>
<wire x1="0" y1="-1.524" x2="-0.127" y2="-1.524" width="0.254" layer="21"/>
<wire x1="-0.127" y1="1.524" x2="-0.127" y2="-1.524" width="0.254" layer="21"/>
<wire x1="-0.127" y1="1.524" x2="0.127" y2="1.524" width="0.254" layer="21"/>
<wire x1="0.127" y1="1.524" x2="0.127" y2="-1.524" width="0.254" layer="21"/>
<wire x1="0.254" y1="0.635" x2="0.3048" y2="0.635" width="0.254" layer="21"/>
<wire x1="0.381" y1="1.016" x2="0.381" y2="0.635" width="0.254" layer="21"/>
<wire x1="0.381" y1="0.635" x2="0.635" y2="0.635" width="0.254" layer="21"/>
<wire x1="-0.635" y1="0.635" x2="-0.381" y2="0.635" width="0.254" layer="21"/>
<wire x1="-0.3302" y1="0.635" x2="-0.254" y2="0.635" width="0.254" layer="21"/>
<wire x1="-0.381" y1="1.016" x2="-0.381" y2="0.635" width="0.254" layer="21"/>
<wire x1="-0.381" y1="0.635" x2="-0.3302" y2="0.635" width="0.254" layer="21"/>
<wire x1="-4.826" y1="-4.826" x2="-3.81" y2="-4.826" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-3.937" x2="-3.81" y2="-4.826" width="0.127" layer="21"/>
<wire x1="4.826" y1="-4.826" x2="3.81" y2="-4.826" width="0.127" layer="21"/>
<wire x1="3.81" y1="-3.937" x2="3.81" y2="-4.826" width="0.127" layer="21"/>
<wire x1="3.81" y1="-3.937" x2="-3.81" y2="-3.937" width="0.127" layer="21"/>
<wire x1="-0.7581" y1="-2.0215" x2="0" y2="2.159" width="0.254" layer="21" curve="-159.443138" cap="flat"/>
<wire x1="0" y1="2.159" x2="0.7581" y2="-2.0215" width="0.254" layer="21" curve="-159.443138" cap="flat"/>
<wire x1="0" y1="3.556" x2="1.8543" y2="3.0343" width="0.127" layer="21" curve="-31.429813" cap="flat"/>
<wire x1="-1.8295" y1="3.0492" x2="0" y2="3.556" width="0.127" layer="21" curve="-30.963201" cap="flat"/>
<wire x1="0" y1="-3.556" x2="3.0493" y2="1.8295" width="0.127" layer="21" curve="120.96244" cap="flat"/>
<wire x1="-3.0875" y1="1.7643" x2="0" y2="-3.5559" width="0.127" layer="21" curve="119.743785" cap="flat"/>
<wire x1="-0.9047" y1="-1.9603" x2="0" y2="-2.159" width="0.254" layer="51" curve="24.773812" cap="flat"/>
<wire x1="0" y1="-2.159" x2="0.9655" y2="-1.9311" width="0.254" layer="51" curve="26.563946" cap="flat"/>
<wire x1="-3.556" y1="0" x2="-1.6937" y2="3.1268" width="0.127" layer="51" curve="-61.557324" cap="flat"/>
<wire x1="1.5903" y1="3.1806" x2="3.556" y2="0" width="0.127" layer="51" curve="-63.435137" cap="flat"/>
<pad name="1" x="-2.54" y="2.54" drill="0.8128" diameter="1.6002" shape="octagon"/>
<pad name="2" x="0" y="-2.54" drill="0.8128" diameter="1.6002" shape="octagon"/>
<pad name="3" x="2.54" y="2.54" drill="0.8128" diameter="1.6002" shape="octagon"/>
<text x="-4.826" y="5.207" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.2766" y="-5.6642" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-4.191" y="-3.556" size="1.27" layer="21" ratio="10">1</text>
<text x="3.302" y="-3.556" size="1.27" layer="21" ratio="10">3</text>
</package>
</packages>
<symbols>
<symbol name="TPOT">
<wire x1="-0.762" y1="-3.81" x2="0" y2="-3.81" width="0.254" layer="94"/>
<wire x1="0.762" y1="3.81" x2="0" y2="3.81" width="0.254" layer="94"/>
<wire x1="-0.762" y1="3.81" x2="-0.762" y2="-3.81" width="0.254" layer="94"/>
<wire x1="0.762" y1="-3.81" x2="0.762" y2="3.81" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="1.651" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="3.81" x2="0" y2="5.08" width="0.1524" layer="94"/>
<wire x1="0" y1="3.81" x2="-0.762" y2="3.81" width="0.254" layer="94"/>
<wire x1="0" y1="-5.08" x2="0" y2="-3.81" width="0.1524" layer="94"/>
<wire x1="0" y1="-3.81" x2="0.762" y2="-3.81" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-3.683" x2="-2.54" y2="-1.143" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-1.143" x2="-3.175" y2="-2.413" width="0.1524" layer="94"/>
<wire x1="-3.175" y1="-2.413" x2="-1.905" y2="-2.413" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-2.413" x2="-2.54" y2="-1.143" width="0.1524" layer="94"/>
<wire x1="1.651" y1="0" x2="-2.286" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-2.286" y1="2.54" x2="-1.905" y2="3.175" width="0.1524" layer="94"/>
<wire x1="-2.286" y1="2.54" x2="-2.667" y2="1.905" width="0.1524" layer="94"/>
<text x="-5.715" y="-3.81" size="1.524" layer="95" rot="R90">&gt;NAME</text>
<text x="-3.7592" y="-3.81" size="1.524" layer="96" rot="R90">&gt;VALUE</text>
<text x="-1.905" y="-4.953" size="1.27" layer="94" rot="R90">1</text>
<text x="-1.905" y="-0.508" size="1.27" layer="94" rot="R90">3</text>
<pin name="1" x="0" y="-7.62" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="3" x="0" y="7.62" visible="pad" length="short" direction="pas" rot="R270"/>
<pin name="2" x="5.08" y="0" visible="pad" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="RS3" prefix="P" uservalue="yes">
<gates>
<gate name="1" symbol="TPOT" x="0" y="0"/>
</gates>
<devices>
<device name="" package="RS3">
<connects>
<connect gate="1" pin="1" pad="1"/>
<connect gate="1" pin="2" pad="2"/>
<connect gate="1" pin="3" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SMDA">
<packages>
<package name="1206">
<wire x1="-0.8636" y1="0.635" x2="0.8636" y2="0.635" width="0.127" layer="21"/>
<wire x1="-0.8636" y1="-0.635" x2="0.8636" y2="-0.635" width="0.127" layer="21"/>
<wire x1="0.8636" y1="0.635" x2="0.8636" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-0.8636" y1="0.635" x2="-0.8636" y2="-0.635" width="0.127" layer="21"/>
<smd name="2" x="1.4732" y="0" dx="1.143" dy="1.7018" layer="1"/>
<smd name="1" x="-1.4732" y="0" dx="1.143" dy="1.7018" layer="1"/>
<text x="-1.397" y="1.27" size="1.27" layer="27">&gt;VALUE</text>
<text x="-1.397" y="3.175" size="1.27" layer="25">&gt;NAME</text>
<rectangle x1="-1.4224" y1="-0.7874" x2="-0.9144" y2="0.7874" layer="27"/>
<rectangle x1="0.9144" y1="-0.7874" x2="1.4224" y2="0.7874" layer="27"/>
</package>
</packages>
<symbols>
<symbol name="REZ">
<wire x1="-2.54" y1="-2.54" x2="2.54" y2="-2.54" width="0.254" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="-2.54" y2="0" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-2.54" y2="-2.54" width="0.254" layer="94"/>
<text x="-3.81" y="1.27" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-5.715" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-5.08" y="-1.27" visible="off" length="short" direction="pas" swaplevel="1"/>
<pin name="2" x="5.08" y="-1.27" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="REZ" prefix="R" uservalue="yes">
<gates>
<gate name="G$1" symbol="REZ" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="rele">
<packages>
<package name="DIL-08">
<wire x1="4.572" y1="3.048" x2="-4.572" y2="3.048" width="0.127" layer="21"/>
<wire x1="-4.572" y1="-3.048" x2="4.572" y2="-3.048" width="0.127" layer="21"/>
<wire x1="4.572" y1="3.048" x2="4.572" y2="-3.048" width="0.127" layer="21"/>
<wire x1="-4.572" y1="3.048" x2="-4.572" y2="1.016" width="0.127" layer="21"/>
<wire x1="-4.572" y1="-3.048" x2="-4.572" y2="-1.016" width="0.127" layer="21"/>
<wire x1="-4.572" y1="1.016" x2="-4.572" y2="-1.016" width="0.127" layer="21" curve="-180"/>
<pad name="1" x="-3.81" y="-3.81" drill="0.8128" diameter="1.397" shape="octagon"/>
<pad name="2" x="-1.27" y="-3.81" drill="0.8128" diameter="1.397" shape="octagon"/>
<pad name="3" x="1.27" y="-3.81" drill="0.8128" diameter="1.397" shape="octagon"/>
<pad name="4" x="3.81" y="-3.81" drill="0.8128" diameter="1.397" shape="octagon"/>
<pad name="5" x="3.81" y="3.81" drill="0.8128" diameter="1.397" shape="octagon"/>
<pad name="6" x="1.27" y="3.81" drill="0.8128" diameter="1.397" shape="octagon"/>
<pad name="7" x="-1.27" y="3.81" drill="0.8128" diameter="1.397" shape="octagon"/>
<pad name="8" x="-3.81" y="3.81" drill="0.8128" diameter="1.397" shape="octagon"/>
<text x="-2.5654" y="0.5588" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="POWERRELAY">
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="0" width="0.254" layer="94"/>
<wire x1="-5.08" y1="0" x2="-3.81" y2="0" width="0.254" layer="94"/>
<wire x1="-3.81" y1="-1.27" x2="-5.08" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-1.27" x2="-5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-2.54" x2="-3.81" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-3.175" y1="0.635" x2="-3.175" y2="-3.175" width="0.254" layer="94"/>
<wire x1="-3.175" y1="-3.175" x2="-2.1685" y2="-3.175" width="0.254" layer="94"/>
<wire x1="-3.81" y1="0.3175" x2="-3.81" y2="-0.3175" width="0.254" layer="94"/>
<wire x1="-3.81" y1="-0.9525" x2="-3.81" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-3.81" y1="-1.27" x2="-3.81" y2="-1.5875" width="0.254" layer="94"/>
<wire x1="-3.81" y1="-2.2225" x2="-3.81" y2="-2.8575" width="0.254" layer="94"/>
<wire x1="-3.81" y1="-1.27" x2="-4.1275" y2="-0.9525" width="0.254" layer="94"/>
<wire x1="-3.81" y1="-1.27" x2="-4.1275" y2="-1.5875" width="0.254" layer="94"/>
<wire x1="5.08" y1="-8.89" x2="5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="5.08" y2="0" width="0.254" layer="94"/>
<wire x1="5.08" y1="0" x2="5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="5.08" y1="5.08" x2="-7.1275" y2="5.08" width="0.254" layer="94"/>
<wire x1="-7.1275" y1="5.08" x2="-7.1275" y2="-8.89" width="0.254" layer="94"/>
<wire x1="-7.1275" y1="-8.89" x2="5.08" y2="-8.89" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="1.905" y2="2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="1.905" y2="-2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="-5.08" x2="1.905" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-7.62" x2="-5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="0" y1="-7.62" x2="-0.0085" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-0.0085" y1="-7.62" x2="-0.0085" y2="-5.5725" width="0.254" layer="94"/>
<wire x1="1.849" y1="-5.5725" x2="-2.2375" y2="-5.5725" width="0.254" layer="94"/>
<wire x1="-2.2375" y1="-5.5725" x2="-2.2375" y2="3.715" width="0.254" layer="94"/>
<wire x1="-2.2375" y1="3.715" x2="1.849" y2="3.715" width="0.254" layer="94"/>
<wire x1="1.849" y1="3.715" x2="1.849" y2="-5.5725" width="0.254" layer="94"/>
<text x="2.2205" y="2.972" size="1.016" layer="94">IN+</text>
<text x="2.9635" y="0.3715" size="1.016" layer="94">SF</text>
<text x="2.9635" y="-2.229" size="1.016" layer="94">AS</text>
<text x="2.592" y="-4.8295" size="0.8128" layer="94">U(I)</text>
<text x="0.242" y="-8.484" size="0.8128" layer="94">GND</text>
<text x="-4.4665" y="3.715" size="0.6096" layer="94">LINE</text>
<text x="-4.4665" y="-8.5445" size="0.6096" layer="94">LOAD</text>
<text x="-2.54" y="9.525" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.54" y="6.35" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-5.08" y="10.16" visible="off" length="middle" direction="nc" rot="R270"/>
<pin name="3" x="0" y="-12.7" visible="off" length="middle" direction="nc" rot="R90"/>
<pin name="2" x="-5.08" y="-12.7" visible="off" length="middle" direction="nc" rot="R90"/>
<pin name="IN" x="10.16" y="2.54" visible="off" length="middle" direction="nc" rot="R180"/>
<pin name="SF" x="10.16" y="0" visible="off" length="middle" direction="nc" rot="R180"/>
<pin name="AS" x="10.16" y="-2.54" visible="off" length="middle" direction="nc" rot="R180"/>
<pin name="U" x="10.16" y="-5.08" visible="off" length="middle" direction="nc" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SCRELE" prefix="R">
<gates>
<gate name="G$1" symbol="POWERRELAY" x="-15.24" y="2.54"/>
</gates>
<devices>
<device name="" package="DIL-08">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="AS" pad="4"/>
<connect gate="G$1" pin="IN" pad="5"/>
<connect gate="G$1" pin="SF" pad="6"/>
<connect gate="G$1" pin="U" pad="7"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SKLADKA2@2">
<packages>
<package name="NIC1">
<pad name="1" x="0" y="0" drill="0.8128" diameter="1.397" shape="square"/>
<pad name="2" x="2.54" y="0" drill="0.8128" diameter="1.397" shape="square"/>
<pad name="3" x="3.81" y="0" drill="0.8128" diameter="1.397" shape="square"/>
<pad name="4" x="6.35" y="0" drill="0.8128" diameter="1.397" shape="square"/>
<pad name="5" x="8.89" y="0" drill="0.8128" diameter="1.397" shape="square"/>
<pad name="6" x="11.43" y="0" drill="0.8128" diameter="1.397" shape="square"/>
<pad name="7" x="13.97" y="0" drill="0.8128" diameter="1.397" shape="square"/>
</package>
</packages>
<symbols>
<symbol name="SPINAC1">
<wire x1="0.762" y1="0.508" x2="2.54" y2="5.08" width="0.4064" layer="94"/>
<circle x="0" y="0" radius="0.762" width="0.254" layer="94"/>
<text x="5.08" y="3.048" size="2.032" layer="95">&gt;NAME</text>
<text x="5.08" y="0.508" size="2.032" layer="96">&gt;VALUE</text>
<pin name="1" x="0" y="-5.08" visible="off" length="middle" rot="R90"/>
<pin name="2" x="0" y="10.16" visible="off" length="middle" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SPINAC1" uservalue="yes">
<gates>
<gate name="G$1" symbol="SPINAC1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="NIC1">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Dezinfekce">
<packages>
</packages>
<symbols>
<symbol name="DEZINFEKCE">
<text x="0" y="1.27" size="2.54" layer="95" align="bottom-center">Dezinfekce</text>
<text x="0" y="-2.54" size="2.54" layer="95" align="bottom-center">...</text>
<text x="0" y="-6.35" size="1.4224" layer="96" align="bottom-center">-----</text>
<wire x1="-10.16" y1="7.62" x2="10.16" y2="7.62" width="0.4064" layer="94"/>
<wire x1="10.16" y1="7.62" x2="10.16" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="10.16" y1="-7.62" x2="-10.16" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="-10.16" y1="-7.62" x2="-10.16" y2="7.62" width="0.4064" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="DEZINFEKCE">
<gates>
<gate name="G$1" symbol="DEZINFEKCE" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Ventilatory">
<packages>
<package name="EC_FAN">
<pad name="1" x="0" y="0" drill="0.8" shape="square"/>
<pad name="2" x="0" y="-2.54" drill="0.8" shape="square"/>
<pad name="3" x="0" y="-5.08" drill="0.8" shape="square"/>
<pad name="4" x="0" y="-7.62" drill="0.8" shape="square"/>
<pad name="5" x="0" y="-10.16" drill="0.8" shape="square"/>
<pad name="6" x="0" y="-12.7" drill="0.8" shape="square"/>
<text x="-2.54" y="0" size="1.27" layer="21" align="center">1</text>
<text x="-2.54" y="-12.7" size="1.27" layer="21" align="center">6</text>
</package>
</packages>
<symbols>
<symbol name="EC_FAN">
<pin name="UB" x="-2.54" y="-2.54" length="short" direction="pwr"/>
<pin name="DU" x="-2.54" y="-5.08" length="short" direction="oc"/>
<pin name="INVLIN" x="-2.54" y="-7.62" length="short" direction="in"/>
<pin name="PWMLIN" x="-2.54" y="-10.16" length="short"/>
<pin name="GND" x="-2.54" y="-15.24" length="short" direction="pwr"/>
<pin name="LOWER" x="-2.54" y="-12.7" length="short" direction="in"/>
<wire x1="0" y1="0" x2="0" y2="-0.3175" width="0.3048" layer="94"/>
<text x="0" y="2.54" size="1.778" layer="94" align="top-left">EC Ventilátor</text>
<circle x="20.32" y="-7.62" radius="7.62" width="0.1524" layer="94"/>
<wire x1="0" y1="-0.3175" x2="0" y2="-0.635" width="0.3048" layer="94"/>
<wire x1="0" y1="-0.635" x2="0" y2="-0.9525" width="0.3048" layer="94"/>
<wire x1="0" y1="-0.9525" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="-17.78" width="0.3048" layer="94"/>
<wire x1="0" y1="-17.78" x2="12.7" y2="-17.78" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-17.78" x2="15.24" y2="-15.24" width="0.3048" layer="94"/>
<wire x1="0" y1="0" x2="0.3175" y2="0" width="0.3048" layer="94"/>
<circle x="20.32" y="-7.62" radius="9.1581" width="0.3048" layer="94"/>
<wire x1="0.3175" y1="0" x2="0.635" y2="0" width="0.3048" layer="94"/>
<wire x1="0.635" y1="0" x2="0.9525" y2="0" width="0.3048" layer="94"/>
<wire x1="0.9525" y1="0" x2="1.27" y2="0" width="0.3048" layer="94"/>
<wire x1="1.27" y1="0" x2="15.24" y2="0" width="0.3048" layer="94"/>
<wire x1="20.32" y1="-0.635" x2="22.86" y2="-3.175" width="0.1524" layer="94" curve="-90"/>
<wire x1="22.86" y1="-3.175" x2="22.86" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="27.305" y1="-7.62" x2="24.765" y2="-10.16" width="0.1524" layer="94" curve="-90"/>
<wire x1="24.765" y1="-10.16" x2="22.86" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="20.32" y1="-14.605" x2="17.78" y2="-12.065" width="0.1524" layer="94" curve="-90"/>
<wire x1="17.78" y1="-12.065" x2="17.78" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="13.335" y1="-7.62" x2="15.875" y2="-5.08" width="0.1524" layer="94" curve="-90"/>
<wire x1="15.875" y1="-5.08" x2="17.78" y2="-5.08" width="0.1524" layer="94"/>
<circle x="20.32" y="-7.62" radius="3.5921" width="0.1524" layer="94"/>
<wire x1="13.335" y1="-7.62" x2="13.335" y2="-8.255" width="0.1524" layer="94"/>
<wire x1="13.335" y1="-8.255" x2="13.97" y2="-8.255" width="0.1524" layer="94"/>
<wire x1="13.97" y1="-8.255" x2="16.8275" y2="-8.5725" width="0.1524" layer="94" curve="-12.680383"/>
<wire x1="20.32" y1="-0.635" x2="19.685" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="19.685" y1="-0.635" x2="19.685" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="19.685" y1="-1.27" x2="19.3675" y2="-4.1275" width="0.1524" layer="94" curve="-12.680383"/>
<wire x1="27.305" y1="-7.62" x2="27.305" y2="-6.985" width="0.1524" layer="94"/>
<wire x1="27.305" y1="-6.985" x2="26.67" y2="-6.985" width="0.1524" layer="94"/>
<wire x1="26.67" y1="-6.985" x2="23.8125" y2="-6.6675" width="0.1524" layer="94" curve="-12.680383"/>
<wire x1="20.32" y1="-14.605" x2="20.955" y2="-14.605" width="0.1524" layer="94"/>
<wire x1="20.955" y1="-14.605" x2="20.955" y2="-13.97" width="0.1524" layer="94"/>
<wire x1="20.955" y1="-13.97" x2="21.2725" y2="-11.1125" width="0.1524" layer="94" curve="-12.680383"/>
<text x="20.32" y="-7.62" size="2.54" layer="94" align="center">M</text>
<text x="34.29" y="-17.78" size="1.27" layer="94" align="bottom-right">Karta ABRA:</text>
<text x="35.56" y="-17.78" size="1.27" layer="94">&gt;ABRA</text>
<text x="34.29" y="-15.24" size="1.27" layer="94" align="bottom-right">TYP:</text>
<text x="35.56" y="-15.24" size="1.27" layer="94">&gt;VALUE</text>
<text x="30.48" y="-7.62" size="1.27" layer="95" align="center-left">&gt;NAME</text>
<text x="30.48" y="-11.43" size="1.27" layer="94">&gt;POWER_INPUT</text>
<wire x1="0" y1="-1.27" x2="1.27" y2="0" width="0.3048" layer="94"/>
<wire x1="0" y1="-0.9525" x2="0.9525" y2="0" width="0.3048" layer="94"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.3048" layer="94"/>
<wire x1="0" y1="-0.3175" x2="0.3175" y2="0" width="0.3048" layer="94"/>
</symbol>
<symbol name="EC_FAN-SCHEMA">
<wire x1="0" y1="-2.54" x2="10.16" y2="-2.54" width="0.1524" layer="97"/>
<wire x1="10.16" y1="-2.54" x2="22.86" y2="-2.54" width="0.1524" layer="97"/>
<wire x1="10.614" y1="-5.942" x2="11.122" y2="-5.026" width="0.1524" layer="97"/>
<wire x1="11.122" y1="-5.026" x2="12.184" y2="-6.142" width="0.1524" layer="97"/>
<wire x1="12.184" y1="-6.142" x2="10.614" y2="-5.942" width="0.1524" layer="97"/>
<wire x1="10.16" y1="-5.08" x2="10.892" y2="-5.496" width="0.1524" layer="97"/>
<wire x1="10.16" y1="-10.16" x2="12.192" y2="-9.144" width="0.1524" layer="97"/>
<wire x1="10.795" y1="-5.842" x2="11.176" y2="-5.207" width="0.254" layer="97"/>
<wire x1="11.176" y1="-5.207" x2="11.938" y2="-5.969" width="0.254" layer="97"/>
<wire x1="11.938" y1="-5.969" x2="10.922" y2="-5.842" width="0.254" layer="97"/>
<wire x1="10.922" y1="-5.842" x2="11.176" y2="-5.461" width="0.254" layer="97"/>
<wire x1="11.176" y1="-5.461" x2="11.557" y2="-5.715" width="0.254" layer="97"/>
<wire x1="11.557" y1="-5.715" x2="11.176" y2="-5.715" width="0.254" layer="97"/>
<rectangle x1="12.192" y1="-10.16" x2="12.954" y2="-5.08" layer="97" rot="R180"/>
<wire x1="2.54" y1="-11.43" x2="2.54" y2="-12.7" width="0.1524" layer="97"/>
<wire x1="2.54" y1="-12.7" x2="2.54" y2="-13.97" width="0.1524" layer="97"/>
<wire x1="2.54" y1="-13.97" x2="7.62" y2="-13.97" width="0.1524" layer="97"/>
<wire x1="7.62" y1="-13.97" x2="7.62" y2="-12.7" width="0.1524" layer="97"/>
<wire x1="7.62" y1="-12.7" x2="7.62" y2="-11.43" width="0.1524" layer="97"/>
<wire x1="7.62" y1="-11.43" x2="2.54" y2="-11.43" width="0.1524" layer="97"/>
<wire x1="10.16" y1="-10.16" x2="10.16" y2="-12.7" width="0.1524" layer="97"/>
<wire x1="10.16" y1="-12.7" x2="7.62" y2="-12.7" width="0.1524" layer="97"/>
<wire x1="10.16" y1="-5.08" x2="10.16" y2="-2.54" width="0.1524" layer="97"/>
<wire x1="12.7" y1="-7.62" x2="17.78" y2="-7.62" width="0.1524" layer="97"/>
<text x="16.51" y="-6.35" size="1.27" layer="97" align="center">DU</text>
<text x="24.13" y="-2.54" size="1.27" layer="97" align="center-left">  UB
+26 VDC</text>
<text x="5.08" y="-10.795" size="1.27" layer="97" align="bottom-center">50</text>
<wire x1="12.7" y1="-30.48" x2="10.668" y2="-31.496" width="0.1524" layer="97"/>
<wire x1="11.7" y1="-35.0599" x2="10.4681" y2="-34.4439" width="0.1524" layer="97"/>
<wire x1="11.938" y1="-34.544" x2="12.7" y2="-35.56" width="0.1524" layer="97"/>
<wire x1="12.7" y1="-35.56" x2="11.43" y2="-35.56" width="0.1524" layer="97"/>
<wire x1="11.43" y1="-35.56" x2="11.938" y2="-34.544" width="0.1524" layer="97"/>
<wire x1="11.684" y1="-35.433" x2="12.446" y2="-35.433" width="0.254" layer="97"/>
<wire x1="12.446" y1="-35.433" x2="11.938" y2="-34.798" width="0.254" layer="97"/>
<wire x1="11.938" y1="-34.798" x2="11.684" y2="-35.306" width="0.254" layer="97"/>
<wire x1="11.684" y1="-35.306" x2="12.065" y2="-35.306" width="0.254" layer="97"/>
<wire x1="12.065" y1="-35.306" x2="11.938" y2="-35.052" width="0.254" layer="97"/>
<rectangle x1="9.906" y1="-35.56" x2="10.668" y2="-30.48" layer="97"/>
<wire x1="2.54" y1="-31.75" x2="2.54" y2="-33.02" width="0.1524" layer="97"/>
<wire x1="2.54" y1="-33.02" x2="2.54" y2="-34.29" width="0.1524" layer="97"/>
<wire x1="2.54" y1="-34.29" x2="7.62" y2="-34.29" width="0.1524" layer="97"/>
<wire x1="7.62" y1="-34.29" x2="7.62" y2="-33.02" width="0.1524" layer="97"/>
<wire x1="7.62" y1="-33.02" x2="7.62" y2="-31.75" width="0.1524" layer="97"/>
<wire x1="7.62" y1="-31.75" x2="2.54" y2="-31.75" width="0.1524" layer="97"/>
<wire x1="10.16" y1="-33.02" x2="7.62" y2="-33.02" width="0.1524" layer="97"/>
<text x="5.08" y="-31.115" size="1.27" layer="97" align="bottom-center">47k</text>
<wire x1="22.86" y1="-38.1" x2="12.7" y2="-38.1" width="0.1524" layer="97"/>
<wire x1="12.7" y1="-38.1" x2="6.985" y2="-38.1" width="0.1524" layer="97"/>
<wire x1="6.985" y1="-38.1" x2="4.445" y2="-38.1" width="0.1524" layer="97"/>
<wire x1="4.445" y1="-38.1" x2="3.175" y2="-38.1" width="0.1524" layer="97"/>
<wire x1="3.175" y1="-38.1" x2="0" y2="-38.1" width="0.1524" layer="97"/>
<wire x1="12.7" y1="-35.56" x2="12.7" y2="-38.1" width="0.1524" layer="97"/>
<wire x1="12.7" y1="-30.48" x2="12.7" y2="-27.94" width="0.1524" layer="97"/>
<wire x1="12.7" y1="-27.94" x2="17.78" y2="-27.94" width="0.1524" layer="97"/>
<text x="15.24" y="-26.67" size="1.27" layer="97" align="center">LOWER</text>
<text x="24.13" y="-38.1" size="1.27" layer="97" align="center-left">GND</text>
<wire x1="19.05" y1="-12.7" x2="20.32" y2="-12.7" width="0.1524" layer="97"/>
<wire x1="20.32" y1="-12.7" x2="21.59" y2="-12.7" width="0.1524" layer="97"/>
<wire x1="21.59" y1="-12.7" x2="21.59" y2="-7.62" width="0.1524" layer="97"/>
<wire x1="21.59" y1="-7.62" x2="20.32" y2="-7.62" width="0.1524" layer="97"/>
<wire x1="20.32" y1="-7.62" x2="19.05" y2="-7.62" width="0.1524" layer="97"/>
<wire x1="19.05" y1="-7.62" x2="19.05" y2="-12.7" width="0.1524" layer="97"/>
<wire x1="20.32" y1="-2.54" x2="20.32" y2="-7.62" width="0.1524" layer="97"/>
<wire x1="0" y1="-17.78" x2="20.32" y2="-17.78" width="0.1524" layer="97"/>
<wire x1="20.32" y1="-17.78" x2="22.86" y2="-17.78" width="0.1524" layer="97"/>
<wire x1="20.32" y1="-12.7" x2="20.32" y2="-17.78" width="0.1524" layer="97"/>
<text x="24.13" y="-17.78" size="1.27" layer="97" align="center-left">INVLIN</text>
<wire x1="2.54" y1="-12.7" x2="0" y2="-12.7" width="0.1524" layer="97"/>
<wire x1="2.54" y1="-33.02" x2="0" y2="-33.02" width="0.1524" layer="97"/>
<text x="24.13" y="-22.86" size="1.27" layer="97" align="center-left">PWMLIN</text>
<wire x1="2.54" y1="-21.59" x2="2.54" y2="-22.86" width="0.1524" layer="97"/>
<wire x1="2.54" y1="-22.86" x2="2.54" y2="-24.13" width="0.1524" layer="97"/>
<wire x1="2.54" y1="-24.13" x2="7.62" y2="-24.13" width="0.1524" layer="97"/>
<wire x1="7.62" y1="-24.13" x2="7.62" y2="-22.86" width="0.1524" layer="97"/>
<wire x1="7.62" y1="-22.86" x2="7.62" y2="-21.59" width="0.1524" layer="97"/>
<wire x1="7.62" y1="-21.59" x2="2.54" y2="-21.59" width="0.1524" layer="97"/>
<text x="5.08" y="-20.955" size="1.27" layer="97" align="bottom-center">75k</text>
<wire x1="2.54" y1="-22.86" x2="0" y2="-22.86" width="0.1524" layer="97"/>
<wire x1="7.62" y1="-22.86" x2="22.86" y2="-22.86" width="0.1524" layer="97"/>
<circle x="20.32" y="-2.54" radius="0.254" width="0.508" layer="97"/>
<circle x="10.16" y="-2.54" radius="0.254" width="0.508" layer="97"/>
<circle x="20.32" y="-17.78" radius="0.254" width="0.508" layer="97"/>
<circle x="12.7" y="-38.1" radius="0.254" width="0.508" layer="97"/>
<rectangle x1="2.54" y1="-3.81" x2="7.62" y2="-1.27" layer="97"/>
<wire x1="0" y1="0" x2="0" y2="-0.3175" width="0.3048" layer="97"/>
<wire x1="0" y1="-0.3175" x2="0" y2="-0.635" width="0.3048" layer="97"/>
<wire x1="0" y1="-0.635" x2="0" y2="-0.9525" width="0.3048" layer="97"/>
<wire x1="0" y1="-0.9525" x2="0" y2="-1.27" width="0.3048" layer="97"/>
<wire x1="0" y1="-1.27" x2="0" y2="-2.54" width="0.3048" layer="97"/>
<wire x1="0" y1="-2.54" x2="0" y2="-12.7" width="0.3048" layer="97"/>
<wire x1="0" y1="-12.7" x2="0" y2="-17.78" width="0.3048" layer="97"/>
<wire x1="0" y1="-17.78" x2="0" y2="-22.86" width="0.3048" layer="97"/>
<wire x1="0" y1="-22.86" x2="0" y2="-33.02" width="0.3048" layer="97"/>
<wire x1="0" y1="-33.02" x2="0" y2="-38.1" width="0.3048" layer="97"/>
<wire x1="0" y1="-38.1" x2="0" y2="-40.64" width="0.3048" layer="97"/>
<wire x1="0" y1="-40.64" x2="33.02" y2="-40.64" width="0.3048" layer="97"/>
<wire x1="33.02" y1="-40.64" x2="33.02" y2="0" width="0.3048" layer="97"/>
<wire x1="33.02" y1="0" x2="1.27" y2="0" width="0.3048" layer="97"/>
<text x="22.86" y="-10.16" size="1.27" layer="97" align="center-left">100k</text>
<wire x1="1.27" y1="0" x2="0.9525" y2="0" width="0.3048" layer="97"/>
<wire x1="0.9525" y1="0" x2="0.635" y2="0" width="0.3048" layer="97"/>
<wire x1="0.635" y1="0" x2="0.3175" y2="0" width="0.3048" layer="97"/>
<wire x1="0.3175" y1="0" x2="0" y2="0" width="0.3048" layer="97"/>
<wire x1="4.445" y1="-38.735" x2="4.445" y2="-38.1" width="0.1524" layer="97"/>
<wire x1="4.445" y1="-38.1" x2="4.445" y2="-37.465" width="0.1524" layer="97"/>
<wire x1="4.445" y1="-38.1" x2="5.715" y2="-37.465" width="0.1524" layer="97"/>
<wire x1="5.715" y1="-37.465" x2="5.715" y2="-38.735" width="0.1524" layer="97"/>
<wire x1="5.715" y1="-38.735" x2="4.445" y2="-38.1" width="0.1524" layer="97"/>
<wire x1="3.175" y1="-38.1" x2="3.175" y2="-36.83" width="0.1524" layer="97"/>
<wire x1="3.175" y1="-36.83" x2="4.445" y2="-36.83" width="0.1524" layer="97"/>
<wire x1="4.445" y1="-36.83" x2="5.715" y2="-36.195" width="0.1524" layer="97"/>
<wire x1="5.715" y1="-36.83" x2="6.985" y2="-36.83" width="0.1524" layer="97"/>
<wire x1="6.985" y1="-36.83" x2="6.985" y2="-38.1" width="0.1524" layer="97"/>
<circle x="3.175" y="-38.1" radius="0.1524" width="0.3048" layer="97"/>
<circle x="6.985" y="-38.1" radius="0.1524" width="0.3048" layer="97"/>
<text x="0" y="2.54" size="1.778" layer="97" align="top-left">EC FAN - vnitřní zapojení</text>
<text x="-0.635" y="-1.905" size="1.27" layer="97" align="bottom-right">1</text>
<text x="-0.635" y="-12.065" size="1.27" layer="97" align="bottom-right">6</text>
<text x="-0.635" y="-17.145" size="1.27" layer="97" align="bottom-right">4</text>
<text x="-0.635" y="-22.225" size="1.27" layer="97" align="bottom-right">3</text>
<text x="-0.635" y="-32.385" size="1.27" layer="97" align="bottom-right">5</text>
<text x="-0.635" y="-37.465" size="1.27" layer="97" align="bottom-right">2</text>
<wire x1="0" y1="-2.54" x2="-2.54" y2="-2.54" width="0.1524" layer="97"/>
<wire x1="0" y1="-12.7" x2="-2.54" y2="-12.7" width="0.1524" layer="97"/>
<wire x1="0" y1="-17.78" x2="-2.54" y2="-17.78" width="0.1524" layer="97"/>
<wire x1="0" y1="-22.86" x2="-2.54" y2="-22.86" width="0.1524" layer="97"/>
<wire x1="0" y1="-33.02" x2="-2.54" y2="-33.02" width="0.1524" layer="97"/>
<wire x1="0" y1="-38.1" x2="-2.54" y2="-38.1" width="0.1524" layer="97"/>
<wire x1="0" y1="-1.27" x2="1.27" y2="0" width="0.3048" layer="97"/>
<wire x1="0" y1="-0.9525" x2="0.9525" y2="0" width="0.3048" layer="97"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.3048" layer="97"/>
<wire x1="0" y1="-0.3175" x2="0.3175" y2="0" width="0.3048" layer="97"/>
<text x="31.75" y="-33.02" size="1.9304" layer="97" align="center-right">For Info Only!</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="W3G300" prefix="FAN">
<gates>
<gate name="G$1" symbol="EC_FAN" x="0" y="0" addlevel="always"/>
<gate name="G$2" symbol="EC_FAN-SCHEMA" x="0" y="-25.4" addlevel="request"/>
</gates>
<devices>
<device name="" package="EC_FAN">
<connects>
<connect gate="G$1" pin="DU" pad="6"/>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="INVLIN" pad="4"/>
<connect gate="G$1" pin="LOWER" pad="5"/>
<connect gate="G$1" pin="PWMLIN" pad="3"/>
<connect gate="G$1" pin="UB" pad="1"/>
</connects>
<technologies>
<technology name="-BV24-01">
<attribute name="ABRA" value="MOST0118"/>
<attribute name="POWER_INPUT" value="205 W"/>
</technology>
<technology name="-BV25-21">
<attribute name="ABRA" value="MOST0119"/>
<attribute name="POWER_INPUT" value="380 W" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="FUSE">
<packages>
<package name="AUTOPOJ">
<wire x1="-3.81" y1="-1.905" x2="-3.81" y2="4.445" width="0.127" layer="22"/>
<wire x1="-3.81" y1="4.445" x2="16.51" y2="4.445" width="0.127" layer="22"/>
<wire x1="16.51" y1="4.445" x2="16.51" y2="-1.905" width="0.127" layer="22"/>
<wire x1="16.51" y1="-1.905" x2="-3.81" y2="-1.905" width="0.127" layer="22"/>
<wire x1="-2.286" y1="4.318" x2="-2.286" y2="-1.778" width="0.127" layer="22"/>
<wire x1="14.732" y1="4.318" x2="14.732" y2="-1.778" width="0.127" layer="22"/>
<wire x1="-1.778" y1="4.064" x2="-1.778" y2="-1.524" width="0.127" layer="22"/>
<wire x1="-1.778" y1="-1.524" x2="4.572" y2="-1.524" width="0.127" layer="22"/>
<wire x1="4.572" y1="-1.524" x2="4.572" y2="4.064" width="0.127" layer="22"/>
<wire x1="4.572" y1="4.064" x2="-1.778" y2="4.064" width="0.127" layer="22"/>
<wire x1="8.128" y1="4.064" x2="8.128" y2="-1.524" width="0.127" layer="22"/>
<wire x1="8.128" y1="-1.524" x2="14.478" y2="-1.524" width="0.127" layer="22"/>
<wire x1="14.478" y1="-1.524" x2="14.478" y2="4.064" width="0.127" layer="22"/>
<wire x1="14.478" y1="4.064" x2="8.128" y2="4.064" width="0.127" layer="22"/>
<circle x="6.35" y="1.27" radius="0.4016" width="0.127" layer="18"/>
<pad name="2C" x="12.7" y="2.54" drill="1.4224" diameter="3.81" shape="square"/>
<pad name="2D" x="12.7" y="0" drill="1.4224" diameter="3.81" shape="square"/>
<pad name="1A" x="0" y="2.54" drill="1.4224" diameter="3.81" shape="square"/>
<pad name="1B" x="0" y="0" drill="1.4224" diameter="3.81" shape="square"/>
<pad name="1C" x="3.81" y="2.54" drill="1.4224" diameter="1.905" shape="long" rot="R90"/>
<pad name="1D" x="3.81" y="0" drill="1.4224" diameter="1.905" shape="long" rot="R90"/>
<pad name="2A" x="8.89" y="2.54" drill="1.4224" diameter="1.905" shape="long" rot="R90"/>
<pad name="2B" x="8.89" y="0" drill="1.4224" diameter="1.905" shape="long" rot="R90"/>
<text x="10.795" y="6.477" size="1.778" layer="26" ratio="10" rot="R180">&gt;NAME</text>
<text x="11.811" y="-2.413" size="1.778" layer="28" ratio="10" rot="R180">&gt;VALUE</text>
<hole x="6.35" y="1.27" drill="2.54"/>
<polygon width="0.127" layer="17">
<vertex x="1.778" y="4.318"/>
<vertex x="1.778" y="-1.778"/>
<vertex x="3.81" y="-1.778"/>
<vertex x="3.048" y="-1.016"/>
<vertex x="3.048" y="3.81"/>
<vertex x="3.556" y="4.318"/>
</polygon>
<polygon width="0.127" layer="17">
<vertex x="10.922" y="4.318"/>
<vertex x="9.144" y="4.318"/>
<vertex x="9.144" y="4.064"/>
<vertex x="9.652" y="3.556"/>
<vertex x="9.652" y="-1.27"/>
<vertex x="9.144" y="-1.778"/>
<vertex x="10.922" y="-1.778"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="FUSE4">
<wire x1="-3.81" y1="-0.762" x2="3.81" y2="-0.762" width="0.254" layer="94"/>
<wire x1="3.81" y1="0.762" x2="-3.81" y2="0.762" width="0.254" layer="94"/>
<wire x1="3.81" y1="-0.762" x2="3.81" y2="0" width="0.254" layer="94"/>
<wire x1="5.08" y1="0" x2="3.81" y2="0" width="0.1524" layer="94"/>
<wire x1="3.81" y1="0" x2="3.81" y2="0.762" width="0.254" layer="94"/>
<wire x1="-5.08" y1="0" x2="-3.81" y2="0" width="0.1524" layer="94"/>
<wire x1="-3.81" y1="0" x2="-3.81" y2="-0.762" width="0.254" layer="94"/>
<wire x1="-3.81" y1="0" x2="3.81" y2="0" width="0.1524" layer="94"/>
<wire x1="-3.81" y1="0.762" x2="-3.81" y2="0" width="0.254" layer="94"/>
<text x="-3.81" y="1.397" size="1.524" layer="95">&gt;NAME</text>
<text x="-3.81" y="-2.921" size="1.524" layer="96">&gt;VALUE</text>
<pin name="1A" x="-7.62" y="0" visible="off" length="short"/>
<pin name="1B" x="-7.62" y="0" visible="off" length="short"/>
<pin name="1C" x="-7.62" y="0" visible="off" length="short"/>
<pin name="1D" x="-7.62" y="0" visible="off" length="short"/>
<pin name="2A" x="7.62" y="0" visible="off" length="short" rot="R180"/>
<pin name="2B" x="7.62" y="0" visible="off" length="short" rot="R180"/>
<pin name="2C" x="7.62" y="0" visible="off" length="short" rot="R180"/>
<pin name="2D" x="7.62" y="0" visible="off" length="short" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="AUTOPOJ" prefix="POJ" uservalue="yes">
<gates>
<gate name="G$1" symbol="FUSE4" x="-2.54" y="-2.54"/>
</gates>
<devices>
<device name="" package="AUTOPOJ">
<connects>
<connect gate="G$1" pin="1A" pad="1A"/>
<connect gate="G$1" pin="1B" pad="1B"/>
<connect gate="G$1" pin="1C" pad="1C"/>
<connect gate="G$1" pin="1D" pad="1D"/>
<connect gate="G$1" pin="2A" pad="2A"/>
<connect gate="G$1" pin="2B" pad="2B"/>
<connect gate="G$1" pin="2C" pad="2C"/>
<connect gate="G$1" pin="2D" pad="2D"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SKLADKA2@5">
<packages>
<package name="NIC1">
<pad name="1" x="0" y="0" drill="0.8128" diameter="1.397" shape="square"/>
<pad name="2" x="2.54" y="0" drill="0.8128" diameter="1.397" shape="square"/>
<pad name="3" x="3.81" y="0" drill="0.8128" diameter="1.397" shape="square"/>
<pad name="4" x="6.35" y="0" drill="0.8128" diameter="1.397" shape="square"/>
<pad name="5" x="8.89" y="0" drill="0.8128" diameter="1.397" shape="square"/>
<pad name="6" x="11.43" y="0" drill="0.8128" diameter="1.397" shape="square"/>
<pad name="7" x="13.97" y="0" drill="0.8128" diameter="1.397" shape="square"/>
</package>
</packages>
<symbols>
<symbol name="RELECIVK">
<wire x1="-5.08" y1="2.54" x2="5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="2.54" x2="5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="5.08" y1="-2.54" x2="-5.08" y2="-2.54" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-2.54" x2="-5.08" y2="2.54" width="0.254" layer="94"/>
<text x="-4.318" y="-1.016" size="2.032" layer="95">&gt;NAME</text>
<text x="-11.938" y="3.048" size="2.032" layer="96">&gt;VALUE</text>
<pin name="1" x="0" y="-7.62" visible="off" length="middle" rot="R90"/>
<pin name="2" x="0" y="7.62" visible="off" length="middle" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="RELECIVK" uservalue="yes">
<gates>
<gate name="G$1" symbol="RELECIVK" x="0" y="0"/>
</gates>
<devices>
<device name="" package="NIC1">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="RELAIS@2">
<packages>
<package name="G2RA">
<wire x1="-5.969" y1="-6.604" x2="23.114" y2="-6.604" width="0.127" layer="21"/>
<wire x1="23.114" y1="6.477" x2="23.114" y2="-6.604" width="0.127" layer="21"/>
<wire x1="23.114" y1="6.477" x2="-5.969" y2="6.477" width="0.127" layer="21"/>
<wire x1="-5.969" y1="-6.604" x2="-5.969" y2="6.477" width="0.127" layer="21"/>
<wire x1="-3.81" y1="2.0574" x2="-3.81" y2="1.1938" width="0.127" layer="21"/>
<wire x1="-1.9304" y1="1.1938" x2="-1.9304" y2="-1.3208" width="0.254" layer="21"/>
<wire x1="-5.6896" y1="-1.3208" x2="-5.6896" y2="1.1938" width="0.254" layer="21"/>
<wire x1="-5.6896" y1="1.1938" x2="-3.81" y2="1.1938" width="0.254" layer="21"/>
<wire x1="-3.81" y1="1.1938" x2="-2.5654" y2="1.1938" width="0.254" layer="21"/>
<wire x1="-5.0546" y1="-1.3208" x2="-5.6896" y2="-1.3208" width="0.254" layer="21"/>
<wire x1="-2.5654" y1="1.1938" x2="-5.0546" y2="-1.3208" width="0.127" layer="21"/>
<wire x1="-2.5654" y1="1.1938" x2="-1.9304" y2="1.1938" width="0.254" layer="21"/>
<wire x1="-1.9304" y1="-1.3208" x2="-3.81" y2="-1.3208" width="0.254" layer="21"/>
<wire x1="-3.81" y1="-1.3208" x2="-3.81" y2="-1.9304" width="0.127" layer="21"/>
<wire x1="-3.81" y1="-1.3208" x2="-5.0546" y2="-1.3208" width="0.254" layer="21"/>
<wire x1="15.621" y1="-0.508" x2="16.256" y2="0.762" width="0.254" layer="21"/>
<wire x1="17.2212" y1="-0.1778" x2="19.685" y2="-0.1778" width="0.127" layer="21"/>
<wire x1="19.685" y1="-0.1778" x2="19.685" y2="-1.8288" width="0.127" layer="21"/>
<wire x1="16.256" y1="1.905" x2="16.256" y2="0.762" width="0.127" layer="21"/>
<pad name="2" x="-3.81" y="3.81" drill="1.3208" diameter="3.1496" shape="octagon"/>
<pad name="1" x="-3.81" y="-3.683" drill="1.3208" diameter="3.1496" shape="octagon"/>
<pad name="P" x="16.1798" y="3.81" drill="1.3208" diameter="3.1496" shape="octagon"/>
<pad name="S" x="19.685" y="-3.683" drill="1.3208" diameter="3.1496" shape="octagon"/>
<text x="25.3746" y="-6.2992" size="1.778" layer="25" ratio="10" rot="R90">&gt;NAME</text>
<text x="3.81" y="-3.81" size="1.778" layer="27" ratio="10" rot="R90">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="K">
<wire x1="-3.81" y1="-1.905" x2="-1.905" y2="-1.905" width="0.254" layer="94"/>
<wire x1="3.81" y1="-1.905" x2="3.81" y2="1.905" width="0.254" layer="94"/>
<wire x1="3.81" y1="1.905" x2="1.905" y2="1.905" width="0.254" layer="94"/>
<wire x1="-3.81" y1="1.905" x2="-3.81" y2="-1.905" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.905" x2="3.81" y2="-1.905" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="1.905" x2="-3.81" y2="1.905" width="0.254" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="1.905" y2="1.905" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="0" y2="-1.905" width="0.254" layer="94"/>
<wire x1="1.905" y1="1.905" x2="0" y2="1.905" width="0.254" layer="94"/>
<text x="1.27" y="2.921" size="1.524" layer="96">&gt;VALUE</text>
<text x="1.27" y="5.08" size="1.524" layer="95">&gt;PART</text>
<text x="0.635" y="3.175" size="0.8636" layer="93">1</text>
<text x="0.635" y="-3.81" size="0.8636" layer="93">2</text>
<pin name="2" x="0" y="-5.08" visible="pad" length="short" direction="pas" rot="R90"/>
<pin name="1" x="0" y="5.08" visible="pad" length="short" direction="pas" rot="R270"/>
</symbol>
<symbol name="S">
<wire x1="0" y1="3.175" x2="0" y2="1.905" width="0.254" layer="94"/>
<wire x1="0" y1="-3.175" x2="0" y2="-1.905" width="0.254" layer="94"/>
<wire x1="0" y1="-1.905" x2="-1.27" y2="1.905" width="0.254" layer="94"/>
<circle x="0" y="-1.905" radius="0.127" width="0.4064" layer="94"/>
<text x="-2.54" y="-2.54" size="1.524" layer="95" rot="R90">&gt;PART</text>
<text x="0.635" y="-3.81" size="0.8636" layer="93">P</text>
<text x="0.635" y="2.54" size="0.8636" layer="93">S</text>
<pin name="P" x="0" y="-5.08" visible="pad" length="short" direction="pas" swaplevel="1" rot="R90"/>
<pin name="S" x="0" y="5.08" visible="pad" length="short" direction="pas" swaplevel="1" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="G2RA" prefix="K" uservalue="yes">
<gates>
<gate name="K" symbol="K" x="0" y="0"/>
<gate name="A" symbol="S" x="15.24" y="2.54"/>
</gates>
<devices>
<device name="" package="G2RA">
<connects>
<connect gate="A" pin="P" pad="P"/>
<connect gate="A" pin="S" pad="S"/>
<connect gate="K" pin="1" pad="1"/>
<connect gate="K" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="RELAIS">
<packages>
<package name="RELEAUTO">
<wire x1="-5.08" y1="4.445" x2="-10.795" y2="4.445" width="0.127" layer="22"/>
<wire x1="-10.795" y1="-4.445" x2="-5.08" y2="-4.445" width="0.127" layer="22"/>
<wire x1="-11.43" y1="3.81" x2="-10.795" y2="4.445" width="0.127" layer="22" curve="-53.130102"/>
<wire x1="-11.43" y1="-3.81" x2="-10.795" y2="-4.445" width="0.127" layer="22" curve="53.130102"/>
<wire x1="-5.08" y1="4.445" x2="-5.08" y2="12.7" width="0.127" layer="22"/>
<wire x1="-5.08" y1="-4.445" x2="-5.08" y2="-12.065" width="0.127" layer="22"/>
<wire x1="3.81" y1="-12.065" x2="3.81" y2="-3.81" width="0.127" layer="22"/>
<wire x1="3.81" y1="-3.81" x2="13.97" y2="-3.81" width="0.127" layer="22"/>
<wire x1="13.97" y1="-3.81" x2="14.605" y2="-3.175" width="0.127" layer="22" curve="53.130102"/>
<wire x1="-11.43" y1="3.81" x2="-11.43" y2="-3.81" width="0.127" layer="22"/>
<wire x1="13.97" y1="3.81" x2="14.605" y2="3.175" width="0.127" layer="22" curve="-53.130102"/>
<wire x1="14.605" y1="3.175" x2="14.605" y2="-3.175" width="0.127" layer="22"/>
<wire x1="3.81" y1="3.81" x2="13.97" y2="3.81" width="0.127" layer="22"/>
<wire x1="3.81" y1="3.81" x2="3.81" y2="12.7" width="0.127" layer="22"/>
<wire x1="-5.08" y1="-12.065" x2="-4.445" y2="-12.7" width="0.127" layer="22" curve="53.130102"/>
<wire x1="3.175" y1="-12.7" x2="3.81" y2="-12.065" width="0.127" layer="22" curve="90"/>
<wire x1="-5.08" y1="12.7" x2="-4.445" y2="13.335" width="0.127" layer="22" curve="-90"/>
<wire x1="3.175" y1="13.335" x2="3.81" y2="12.7" width="0.127" layer="22" curve="-53.130102"/>
<wire x1="3.175" y1="13.335" x2="-4.445" y2="13.335" width="0.127" layer="22"/>
<wire x1="-4.445" y1="-12.7" x2="3.175" y2="-12.7" width="0.127" layer="22"/>
<wire x1="-7.62" y1="3.81" x2="-7.62" y2="-3.81" width="0.127" layer="22"/>
<wire x1="-7.62" y1="-3.81" x2="-10.16" y2="-3.81" width="0.127" layer="22"/>
<wire x1="-10.16" y1="-3.81" x2="-10.16" y2="3.81" width="0.127" layer="22"/>
<wire x1="-10.16" y1="3.81" x2="-7.62" y2="3.81" width="0.127" layer="22"/>
<wire x1="0" y1="3.81" x2="0" y2="-3.81" width="0.127" layer="22"/>
<wire x1="0" y1="-3.81" x2="-2.54" y2="-3.81" width="0.127" layer="22"/>
<wire x1="-2.54" y1="-3.81" x2="-2.54" y2="3.81" width="0.127" layer="22"/>
<wire x1="-2.54" y1="3.81" x2="0" y2="3.81" width="0.127" layer="22"/>
<wire x1="3.175" y1="7.62" x2="-4.445" y2="7.62" width="0.127" layer="22"/>
<wire x1="-4.445" y1="7.62" x2="-4.445" y2="10.16" width="0.127" layer="22"/>
<wire x1="-4.445" y1="10.16" x2="3.175" y2="10.16" width="0.127" layer="22"/>
<wire x1="3.175" y1="10.16" x2="3.175" y2="7.62" width="0.127" layer="22"/>
<wire x1="3.175" y1="-10.16" x2="-4.445" y2="-10.16" width="0.127" layer="22"/>
<wire x1="-4.445" y1="-10.16" x2="-4.445" y2="-7.62" width="0.127" layer="22"/>
<wire x1="-4.445" y1="-7.62" x2="3.175" y2="-7.62" width="0.127" layer="22"/>
<wire x1="3.175" y1="-7.62" x2="3.175" y2="-10.16" width="0.127" layer="22"/>
<wire x1="13.335" y1="-1.27" x2="5.715" y2="-1.27" width="0.127" layer="51"/>
<wire x1="5.715" y1="-1.27" x2="5.715" y2="1.27" width="0.127" layer="51"/>
<wire x1="5.715" y1="1.27" x2="13.335" y2="1.27" width="0.127" layer="51"/>
<wire x1="13.335" y1="1.27" x2="13.335" y2="-1.27" width="0.127" layer="51"/>
<wire x1="3.81" y1="-12.065" x2="13.97" y2="-3.81" width="0.127" layer="22"/>
<wire x1="-10.795" y1="4.445" x2="-5.08" y2="12.7" width="0.127" layer="22"/>
<wire x1="-12.065" y1="12.7" x2="-10.795" y2="13.97" width="0.127" layer="22" curve="-90"/>
<wire x1="-12.065" y1="12.7" x2="-12.065" y2="-12.065" width="0.127" layer="22"/>
<wire x1="-12.065" y1="-12.065" x2="-10.795" y2="-13.335" width="0.127" layer="22" curve="67.380135"/>
<wire x1="-10.795" y1="-13.335" x2="13.97" y2="-13.335" width="0.127" layer="22"/>
<wire x1="13.97" y1="-13.335" x2="15.24" y2="-12.065" width="0.127" layer="22" curve="90"/>
<wire x1="15.24" y1="-12.065" x2="15.24" y2="12.7" width="0.127" layer="22"/>
<wire x1="13.97" y1="13.97" x2="-10.795" y2="13.97" width="0.127" layer="22"/>
<wire x1="13.97" y1="13.97" x2="15.24" y2="12.7" width="0.127" layer="22" curve="-90"/>
<pad name="87" x="-8.89" y="0" drill="2.54" diameter="5.08"/>
<pad name="30" x="9.017" y="0" drill="2.54" diameter="5.08"/>
<pad name="87A" x="-0.889" y="0" drill="2.54" diameter="5.08"/>
<pad name="86" x="-0.762" y="8.636" drill="2.54" diameter="5.08"/>
<pad name="85" x="-0.508" y="-8.509" drill="2.54" diameter="5.08"/>
<pad name="P$6" x="-6.985" y="6.477" drill="2.1844" diameter="2.54"/>
<pad name="P$7" x="6.985" y="-6.477" drill="2.1844" diameter="2.54"/>
<text x="-9.525" y="17.145" size="1.778" layer="26" ratio="10">&gt;NAME</text>
<text x="-9.652" y="14.605" size="1.778" layer="28" ratio="10">&gt;VALUE</text>
<rectangle x1="-12.7" y1="-9.525" x2="-11.43" y2="10.795" layer="22"/>
</package>
</packages>
<symbols>
<symbol name="K">
<wire x1="3.81" y1="-1.905" x2="3.81" y2="1.905" width="0.254" layer="94"/>
<wire x1="-3.81" y1="1.905" x2="-3.81" y2="-1.905" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.905" x2="3.81" y2="-1.905" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="1.905" x2="-3.81" y2="1.905" width="0.254" layer="94"/>
<wire x1="-3.81" y1="-1.905" x2="0" y2="-1.905" width="0.254" layer="94"/>
<wire x1="3.81" y1="1.905" x2="0" y2="1.905" width="0.254" layer="94"/>
<text x="1.27" y="2.921" size="1.524" layer="96">&gt;VALUE</text>
<text x="0.635" y="3.175" size="0.8636" layer="93">1</text>
<text x="0.635" y="-3.81" size="0.8636" layer="93">2</text>
<text x="-2.54" y="-0.635" size="1.27" layer="95">&gt;PART</text>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
</symbol>
<symbol name="U">
<wire x1="3.175" y1="5.08" x2="1.905" y2="5.08" width="0.254" layer="94"/>
<wire x1="-3.175" y1="5.08" x2="-1.905" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="2.54" y2="5.715" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="0" y2="0" width="0.254" layer="94"/>
<circle x="0" y="1.27" radius="0.127" width="0.4064" layer="94"/>
<text x="0.635" y="0.635" size="0.8636" layer="93">P</text>
<text x="-2.54" y="3.81" size="0.8636" layer="93">S</text>
<text x="2.54" y="3.81" size="0.8636" layer="93">O</text>
<pin name="O" x="5.08" y="5.08" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="S" x="-5.08" y="5.08" visible="off" length="short" direction="pas"/>
<pin name="P" x="0" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="AUTORELE" prefix="RE" uservalue="yes">
<gates>
<gate name="A" symbol="K" x="0" y="-2.54"/>
<gate name="B" symbol="U" x="17.78" y="-5.08"/>
</gates>
<devices>
<device name="" package="RELEAUTO">
<connects>
<connect gate="A" pin="1" pad="86"/>
<connect gate="A" pin="2" pad="85"/>
<connect gate="B" pin="O" pad="87A"/>
<connect gate="B" pin="P" pad="30"/>
<connect gate="B" pin="S" pad="87"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="DIODE">
<packages>
<package name="DO41-10">
<wire x1="2.032" y1="-1.27" x2="-2.032" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.032" y1="-1.27" x2="2.032" y2="1.27" width="0.127" layer="21"/>
<wire x1="-2.032" y1="1.27" x2="2.032" y2="1.27" width="0.127" layer="21"/>
<wire x1="-2.032" y1="1.27" x2="-2.032" y2="-1.27" width="0.127" layer="21"/>
<wire x1="5.08" y1="0" x2="4.064" y2="0" width="0.762" layer="51"/>
<wire x1="-5.08" y1="0" x2="-4.064" y2="0" width="0.762" layer="51"/>
<wire x1="-0.635" y1="0" x2="0" y2="0" width="0.127" layer="21"/>
<wire x1="1.016" y1="0.635" x2="1.016" y2="-0.635" width="0.127" layer="21"/>
<wire x1="1.016" y1="-0.635" x2="0" y2="0" width="0.127" layer="21"/>
<wire x1="0" y1="0" x2="1.524" y2="0" width="0.127" layer="21"/>
<wire x1="0" y1="0" x2="1.016" y2="0.635" width="0.127" layer="21"/>
<wire x1="0" y1="0.635" x2="0" y2="0" width="0.127" layer="21"/>
<wire x1="0" y1="0" x2="0" y2="-0.635" width="0.127" layer="21"/>
<pad name="A" x="5.08" y="0" drill="1.1176" diameter="2.159" shape="octagon"/>
<pad name="K" x="-5.08" y="0" drill="1.1176" diameter="2.159"/>
<text x="6.35" y="-0.127" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="6.35" y="-1.778" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.27" x2="-1.143" y2="1.27" layer="21"/>
<rectangle x1="2.032" y1="-0.381" x2="3.937" y2="0.381" layer="21"/>
<rectangle x1="-3.937" y1="-0.381" x2="-2.032" y2="0.381" layer="21"/>
</package>
</packages>
<symbols>
<symbol name="D">
<wire x1="-1.27" y1="-1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-1.27" y2="1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.27" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.27" y2="-1.27" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="1.27" y2="-1.27" width="0.254" layer="94"/>
<text x="2.54" y="0.4826" size="1.524" layer="95">&gt;NAME</text>
<text x="2.54" y="-2.0574" size="1.524" layer="96">&gt;VALUE</text>
<pin name="A" x="-2.54" y="0" visible="off" length="point" direction="pas"/>
<pin name="K" x="2.54" y="0" visible="off" length="point" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="1N4004" prefix="D" uservalue="yes">
<gates>
<gate name="1" symbol="D" x="0" y="0"/>
</gates>
<devices>
<device name="" package="DO41-10">
<connects>
<connect gate="1" pin="A" pad="A"/>
<connect gate="1" pin="K" pad="K"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SKLADKA2@4">
<packages>
<package name="BOD">
<pad name="P$1" x="0" y="0" drill="1.27" diameter="2.54"/>
<text x="-3.937" y="2.54" size="1.27" layer="27">&gt;VALUE</text>
<text x="-3.937" y="4.445" size="1.27" layer="25">&gt;NAME</text>
</package>
</packages>
<symbols>
<symbol name="PRBOD">
<wire x1="-1.27" y1="-1.016" x2="1.27" y2="1.016" width="0.254" layer="94"/>
<circle x="0" y="0" radius="0.9158" width="0.254" layer="94"/>
<text x="-5.08" y="2.54" size="2.032" layer="95">&gt;NAME</text>
<pin name="P$1" x="0" y="0" visible="off" length="point" direction="pas"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PRBOD" prefix="A" uservalue="yes">
<gates>
<gate name="G$1" symbol="PRBOD" x="0" y="0"/>
</gates>
<devices>
<device name="" package="BOD">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
<attribute name="ABRA_GLOBAL" value="VAUT0248"/>
<attribute name="VEHICLE_NAME" value="ISR-Yeffe-Hod VEIT26"/>
<attribute name="VEHICLE_NUMBER" value="40318"/>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="FRAME1" library="VEIT_logotyp" deviceset="DINA3_L-AUTO" device="" value="Konfigurační list"/>
<part name="FRAME2" library="VEIT_logotyp" deviceset="DINA3_L-AUTO" device="" value="Autostart KUBOTA"/>
<part name="FRAME3" library="VEIT_logotyp" deviceset="DINA3_L-AUTO" device="" value="Pokyny"/>
<part name="U$1" library="CMI" deviceset="JISTIC_DUMMY" device="" value="50A"/>
<part name="U$94" library="SUPPLY2" deviceset="+24V" device=""/>
<part name="U$10" library="SUPPLY2" deviceset="GND" device=""/>
<part name="U$36" library="SUPPLY2" deviceset="AKU" device=""/>
<part name="U$4" library="SUPPLY2" deviceset="+24V" device=""/>
<part name="PRUCHODKA2" library="AUTO-schema" deviceset="PRUCHODKA" device=""/>
<part name="PRUCHODKA3" library="AUTO-schema" deviceset="PRUCHODKA" device=""/>
<part name="DIESEL1" library="KUBOTA" deviceset="KUBOTA" device=""/>
<part name="DPS1" library="AUTO-schema" deviceset="AUTOSTART-KUBOTA" device=""/>
<part name="MCFLEX1" library="KUBOTA" deviceset="MCFLEX" device=""/>
<part name="U$9" library="SUPPLY2" deviceset="GND" device=""/>
<part name="U$11" library="Svorkovnice-3patra" deviceset="3SVORKA-00" device="" value="47?"/>
<part name="U$12" library="Svorkovnice-3patra" deviceset="3SVORKA-00" device="" value="46?"/>
<part name="U$3" library="SUPPLY2" deviceset="GND" device=""/>
<part name="SVORKA23" library="Svorkovnice-3patra" deviceset="3SVORKA-01" device="" value="8"/>
<part name="TMC1" library="TMC" deviceset="TMC" device=""/>
<part name="V16" library="SUPPLY2@2" deviceset="V&lt;--" device="" value="SENSOR T1"/>
<part name="V17" library="SUPPLY2@2" deviceset="V&lt;--" device="" value="SENSOR T2"/>
<part name="V18" library="SUPPLY2@2" deviceset="V&lt;--" device="" value="SENSOR T3"/>
<part name="V19" library="SUPPLY2@2" deviceset="V&lt;--" device="" value="SENSOR T4"/>
<part name="V20" library="SUPPLY2@2" deviceset="V&lt;--" device="" value="SENSOR T5"/>
<part name="V1" library="SUPPLY2@2" deviceset="V&lt;--" device="" value="SENSOR T6"/>
<part name="SVORKA25" library="Svorkovnice-3patra" deviceset="3SVORKA-00" device="" value="1"/>
<part name="SVORKA26" library="Svorkovnice-3patra" deviceset="3SVORKA-00" device="" value="2"/>
<part name="U$6" library="SUPPLY2" deviceset="GND" device=""/>
<part name="FRAME6" library="VEIT_logotyp" deviceset="DINA3_L-AUTO" device="" value="Senzory (měření)"/>
<part name="PRUCHODKA4" library="AUTO-schema" deviceset="PRUCHODKA" device=""/>
<part name="PRUCHODKA16" library="AUTO-schema" deviceset="PRUCHODKA" device=""/>
<part name="PRUCHODKA17" library="AUTO-schema" deviceset="PRUCHODKA" device=""/>
<part name="PRUCHODKA18" library="AUTO-schema" deviceset="PRUCHODKA" device=""/>
<part name="PRUCHODKA19" library="AUTO-schema" deviceset="PRUCHODKA" device=""/>
<part name="SVORKA28" library="Svorkovnice-3patra" deviceset="3SVORKA-11" device="" value="21"/>
<part name="SENSOR1" library="Senzory" deviceset="HUM" device=""/>
<part name="SENSOR2" library="Senzory" deviceset="FP" device=""/>
<part name="SENSOR3" library="Senzory" deviceset="CO2" device=""/>
<part name="SVORKA29" library="Svorkovnice-3patra" deviceset="3SVORKA-00" device="" value="19"/>
<part name="SVORKA30" library="Svorkovnice-3patra" deviceset="3SVORKA-01" device="" value="18"/>
<part name="U$7" library="SUPPLY2" deviceset="GND" device=""/>
<part name="PRUCHODKA20" library="AUTO-schema" deviceset="PRUCHODKA" device=""/>
<part name="PRUCHODKA21" library="AUTO-schema" deviceset="PRUCHODKA" device=""/>
<part name="PRUCHODKA22" library="AUTO-schema" deviceset="PRUCHODKA" device=""/>
<part name="SVORKA31" library="Svorkovnice-3patra" deviceset="3SVORKA-10" device="" value="22"/>
<part name="SVORKA32" library="Svorkovnice-3patra" deviceset="3SVORKA-00" device="" value="23"/>
<part name="SVORKA33" library="Svorkovnice-3patra" deviceset="3SVORKA-00" device="" value="24"/>
<part name="V48" library="SUPPLY2" deviceset="V--&gt;" device="" value="GPS"/>
<part name="U$13" library="SUPPLY2" deviceset="GND" device=""/>
<part name="PRUCHODKA23" library="AUTO-schema" deviceset="PRUCHODKA" device=""/>
<part name="V2" library="SUPPLY2@2" deviceset="V&lt;--" device="" value="HERMES"/>
<part name="PRUCHODKA24" library="AUTO-schema" deviceset="PRUCHODKA" device=""/>
<part name="FRAME7" library="VEIT_logotyp" deviceset="DINA3_L-AUTO" device="" value="Houkačka"/>
<part name="SVORKA35" library="Svorkovnice-3patra" deviceset="3SVORKA-00" device="" value="27"/>
<part name="U$15" library="AUTO-schema" deviceset="HORN_DUMMY" device=""/>
<part name="PRUCHODKA25" library="AUTO-schema" deviceset="PRUCHODKA" device=""/>
<part name="U$5" library="Svorkovnice-3patra" deviceset="3SVORKA-00" device="" value="48?"/>
<part name="U$8" library="Svorkovnice-3patra" deviceset="3SVORKA-00" device="" value="43?"/>
<part name="SVORKA37" library="Svorkovnice-3patra" deviceset="3SVORKA-11" device="" value="30?"/>
<part name="U$22" library="CMI" deviceset="UZEL_DUMMY" device=""/>
<part name="SVORKA38" library="AUTO-schema" deviceset="SVORKA_PULENA" device=""/>
<part name="SVORKA39" library="AUTO-schema" deviceset="SVORKA_PULENA" device=""/>
<part name="U$17" library="CMI" deviceset="UZEL_DUMMY" device=""/>
<part name="U$23" library="SUPPLY2" deviceset="GND" device=""/>
<part name="SERVO1" library="Servomotory" deviceset="SERVO" device=""/>
<part name="PRUCHODKA1" library="AUTO-schema" deviceset="PRUCHODKA" device=""/>
<part name="U$24" library="SUPPLY2" deviceset="GND" device=""/>
<part name="PRUCHODKA26" library="AUTO-schema" deviceset="PRUCHODKA" device=""/>
<part name="U$16" library="SUPPLY2" deviceset="GND" device=""/>
<part name="SVORKA42" library="Svorkovnice-3patra" deviceset="3SVORKA-01" device="" value="44?"/>
<part name="PRUCHODKA27" library="AUTO-schema" deviceset="PRUCHODKA" device=""/>
<part name="PRUCHODKA28" library="AUTO-schema" deviceset="PRUCHODKA" device=""/>
<part name="FRAME4" library="VEIT_logotyp" deviceset="DINA3_L-AUTO" device="" value="Hlavní přepínač funkce"/>
<part name="SVORKA1" library="Svorkovnice-3patra" deviceset="3SVORKA-00" device="" value="26"/>
<part name="SVORKA2" library="Svorkovnice-3patra" deviceset="3SVORKA-00" device="" value="28"/>
<part name="SVORKA3" library="Svorkovnice-3patra" deviceset="3SVORKA-00" device="" value="10"/>
<part name="SVORKA8" library="Svorkovnice-3patra" deviceset="3SVORKA-11-BLUE" device="" value="45?"/>
<part name="SVORKA9" library="Svorkovnice-3patra" deviceset="3SVORKA-11-BLUE" device="" value="20"/>
<part name="SVORKA10" library="Svorkovnice-3patra" deviceset="3SVORKA-11-BLUE" device="" value="3"/>
<part name="SVORKA11" library="Svorkovnice-3patra" deviceset="3SVORKA-11-BLUE" device="" value="25"/>
<part name="SVORKA12" library="Svorkovnice-3patra" deviceset="3SVORKA-11-BLUE" device="" value="32"/>
<part name="SVORKA13" library="Svorkovnice-3patra" deviceset="3SVORKA-11-BLUE" device="" value="36"/>
<part name="SVORKA14" library="Svorkovnice-3patra" deviceset="3SVORKA-11-BLUE" device="" value="38"/>
<part name="SVORKA15" library="Svorkovnice-3patra" deviceset="3SVORKA-11-BLUE" device="" value="50"/>
<part name="S1" library="AUTO-schema" deviceset="OTOCNY_PREPINAC" device=""/>
<part name="U$14" library="SUPPLY2" deviceset="GND" device=""/>
<part name="U$19" library="CMI" deviceset="D_DUMMY" device=""/>
<part name="U$18" library="CMI" deviceset="D_DUMMY" device=""/>
<part name="FRAME5" library="VEIT_logotyp" deviceset="DINA3_L-AUTO" device="" value="Deska relé"/>
<part name="DPS2" library="AUTO-schema" deviceset="DESKA_RELE" device=""/>
<part name="SERVO6" library="Servomotory" deviceset="SERVO" device=""/>
<part name="PRUCHODKA5" library="AUTO-schema" deviceset="PRUCHODKA" device=""/>
<part name="SERVO7" library="Servomotory" deviceset="SERVO" device=""/>
<part name="PRUCHODKA6" library="AUTO-schema" deviceset="PRUCHODKA" device=""/>
<part name="SERVO8" library="Servomotory" deviceset="SERVO" device=""/>
<part name="PRUCHODKA7" library="AUTO-schema" deviceset="PRUCHODKA" device=""/>
<part name="SERVO9" library="Servomotory" deviceset="SERVO" device=""/>
<part name="PRUCHODKA8" library="AUTO-schema" deviceset="PRUCHODKA" device=""/>
<part name="SERVO10" library="Servomotory" deviceset="SERVO" device=""/>
<part name="PRUCHODKA9" library="AUTO-schema" deviceset="PRUCHODKA" device=""/>
<part name="SERVO11" library="Servomotory" deviceset="SERVO" device=""/>
<part name="PRUCHODKA10" library="AUTO-schema" deviceset="PRUCHODKA" device=""/>
<part name="U$2" library="SUPPLY2" deviceset="AKU" device=""/>
<part name="U$20" library="SUPPLY2" deviceset="+24V@2" device="" value="+24V"/>
<part name="SVORKA16" library="Svorkovnice-3patra" deviceset="3SVORKA-00" device="" value="4"/>
<part name="SVORKA17" library="Svorkovnice-3patra" deviceset="3SVORKA-00" device="" value="5"/>
<part name="SVORKA18" library="Svorkovnice-3patra" deviceset="3SVORKA-00" device="" value="6"/>
<part name="U$21" library="SUPPLY2" deviceset="GND" device=""/>
<part name="U$25" library="SUPPLY2" deviceset="GND" device=""/>
<part name="P1" library="TRIMPOT" deviceset="RS3" device="" value="RS3"/>
<part name="R23" library="R" deviceset="0207/10" device="" value="680R"/>
<part name="U$57" library="SUPPLY2" deviceset="GND" device=""/>
<part name="U$65" library="SUPPLY2" deviceset="+10V" device=""/>
<part name="U$26" library="SUPPLY2" deviceset="GND" device=""/>
<part name="R19" library="SMDA" deviceset="REZ" device="" value="2K2"/>
<part name="R21" library="SMDA" deviceset="REZ" device="" value="6K8"/>
<part name="U$27" library="SUPPLY2" deviceset="+10V" device=""/>
<part name="U$33" library="SUPPLY2" deviceset="+10V" device=""/>
<part name="RE21" library="rele" deviceset="SCRELE" device=""/>
<part name="U$45" library="SUPPLY2" deviceset="GND" device=""/>
<part name="U$37" library="SUPPLY2" deviceset="+24V" device=""/>
<part name="FRAME8" library="VEIT_logotyp" deviceset="DINA3_L-AUTO" device="" value="Naftové topení"/>
<part name="U$38" library="CMI" deviceset="JISTIC_DUMMY" device="" value="30A"/>
<part name="U$39" library="SUPPLY2" deviceset="+24V" device=""/>
<part name="U$40" library="CMI" deviceset="UZEL_DUMMY" device=""/>
<part name="SVORKA24" library="Svorkovnice-3patra" deviceset="3SVORKA-00" device="" value="15"/>
<part name="PRUCHODKA12" library="AUTO-schema" deviceset="PRUCHODKA" device=""/>
<part name="U$41" library="SUPPLY2" deviceset="GND" device=""/>
<part name="SVORKA27" library="Svorkovnice-3patra" deviceset="3SVORKA-01" device="" value="13"/>
<part name="SVORKA21" library="Svorkovnice-3patra" deviceset="3SVORKA-01" device="" value="14"/>
<part name="SVORKA22" library="Svorkovnice-3patra" deviceset="3SVORKA-00" device="" value="9"/>
<part name="V31" library="SUPPLY2" deviceset="V--&gt;" device="" value="SENSOR TR"/>
<part name="V58" library="SUPPLY2" deviceset="V--&gt;" device="" value="TR"/>
<part name="PRUCHODKA11" library="AUTO-schema" deviceset="PRUCHODKA" device=""/>
<part name="V52" library="SUPPLY2" deviceset="V--&gt;" device="" value="LOWPRESS"/>
<part name="V59" library="SUPPLY2" deviceset="V--&gt;" device="" value="HIGHPRESS"/>
<part name="PRUCHODKA13" library="AUTO-schema" deviceset="PRUCHODKA" device=""/>
<part name="PRUCHODKA14" library="AUTO-schema" deviceset="PRUCHODKA" device=""/>
<part name="SVORKA20" library="Svorkovnice-3patra" deviceset="3SVORKA-00" device="" value="16"/>
<part name="SVORKA34" library="Svorkovnice-3patra" deviceset="3SVORKA-01" device="" value="17"/>
<part name="FRAME9" library="VEIT_logotyp" deviceset="DINA3_L-AUTO" device="" value="Desinfekce"/>
<part name="SVORKA36" library="Svorkovnice-3patra" deviceset="3SVORKA-11" device="" value="35"/>
<part name="SVORKA40" library="Svorkovnice-3patra" deviceset="3SVORKA-00" device="" value="37"/>
<part name="SVORKA41" library="Svorkovnice-3patra" deviceset="3SVORKA-00" device="" value="39"/>
<part name="SVORKA43" library="Svorkovnice-3patra" deviceset="3SVORKA-00" device="" value="40"/>
<part name="V64" library="SUPPLY2" deviceset="V--&gt;" device="" value="LIGHT"/>
<part name="V3" library="SUPPLY2" deviceset="V--&gt;" device="" value="LIGHTrozvad"/>
<part name="PRUCHODKA15" library="AUTO-schema" deviceset="PRUCHODKA" device=""/>
<part name="U$29" library="SUPPLY2" deviceset="GND" device=""/>
<part name="U$30" library="SUPPLY2" deviceset="GND" device=""/>
<part name="U$31" library="Dezinfekce" deviceset="DEZINFEKCE" device=""/>
<part name="SVORKA45" library="Svorkovnice-3patra" deviceset="3SVORKA-00" device="" value="12"/>
<part name="SVORKA47" library="Svorkovnice-3patra" deviceset="3SVORKA-00-SHADOW" device="" value="100"/>
<part name="SVORKA4" library="Svorkovnice-3patra" deviceset="3SVORKA-00-SEPARATED" device="" value="11"/>
<part name="PRUCHODKA29" library="AUTO-schema" deviceset="PRUCHODKA" device=""/>
<part name="DPS3" library="7P" deviceset="7P" device="_V0.2"/>
<part name="FRAME10" library="VEIT_logotyp" deviceset="DINA3_L-AUTO" device="" value="Ventilace"/>
<part name="FAN1" library="Ventilatory" deviceset="W3G300" device="" technology="-BV25-21"/>
<part name="FAN2" library="Ventilatory" deviceset="W3G300" device="" technology="-BV25-21"/>
<part name="FAN3" library="Ventilatory" deviceset="W3G300" device="" technology="-BV25-21"/>
<part name="FAN4" library="Ventilatory" deviceset="W3G300" device="" technology="-BV25-21"/>
<part name="FAN5" library="Ventilatory" deviceset="W3G300" device="" technology="-BV25-21"/>
<part name="FAN6" library="Ventilatory" deviceset="W3G300" device="" technology="-BV25-21"/>
<part name="FAN7" library="Ventilatory" deviceset="W3G300" device="" technology="-BV25-21"/>
<part name="U$85" library="SUPPLY2" deviceset="+24V@2" device="" value="+24V"/>
<part name="U$32" library="SUPPLY2" deviceset="+24V@2" device="" value="+24V"/>
<part name="U$34" library="SUPPLY2" deviceset="+24V@2" device="" value="+24V"/>
<part name="U$35" library="SUPPLY2" deviceset="+24V@2" device="" value="+24V"/>
<part name="U$42" library="SUPPLY2" deviceset="+24V@2" device="" value="+24V"/>
<part name="U$43" library="SUPPLY2" deviceset="+24V@2" device="" value="+24V"/>
<part name="U$44" library="SUPPLY2" deviceset="+24V@2" device="" value="+24V"/>
<part name="U$46" library="SUPPLY2" deviceset="GND" device=""/>
<part name="U$47" library="SUPPLY2" deviceset="GND" device=""/>
<part name="U$48" library="SUPPLY2" deviceset="GND" device=""/>
<part name="U$49" library="SUPPLY2" deviceset="GND" device=""/>
<part name="U$50" library="SUPPLY2" deviceset="GND" device=""/>
<part name="U$51" library="SUPPLY2" deviceset="GND" device=""/>
<part name="U$52" library="SUPPLY2" deviceset="GND" device=""/>
<part name="U$53" library="SUPPLY2" deviceset="GND" device=""/>
<part name="U$54" library="SUPPLY2" deviceset="GND" device=""/>
<part name="U$55" library="SUPPLY2" deviceset="GND" device=""/>
<part name="SVORKA5" library="Svorkovnice-3patra" deviceset="3SVORKA-01" device="" value="7"/>
<part name="U$56" library="SUPPLY2" deviceset="+24V@2" device="" value="+24V"/>
<part name="FA_7P" library="FUSE" deviceset="AUTOPOJ" device="" value="3A"/>
<part name="U$58" library="SUPPLY2" deviceset="+24V@2" device="" value="+24V"/>
<part name="FA_EM1" library="FUSE" deviceset="AUTOPOJ" device="" value="1A"/>
<part name="V24" library="SUPPLY2" deviceset="V--&gt;" device="" value="COOLFAN1+"/>
<part name="RE5" library="rele" deviceset="SCRELE" device=""/>
<part name="RE6" library="rele" deviceset="SCRELE" device=""/>
<part name="RE7" library="rele" deviceset="SCRELE" device=""/>
<part name="RE8" library="rele" deviceset="SCRELE" device=""/>
<part name="U$59" library="SUPPLY2" deviceset="GND" device=""/>
<part name="U$79" library="SUPPLY2" deviceset="GND" device=""/>
<part name="U$81" library="SUPPLY2" deviceset="GND" device=""/>
<part name="U$82" library="SUPPLY2" deviceset="GND" device=""/>
<part name="V65" library="SUPPLY2" deviceset="V--&gt;" device="" value="COOLFAN2+"/>
<part name="V84" library="SUPPLY2" deviceset="V--&gt;" device="" value="COOLFAN3+"/>
<part name="V85" library="SUPPLY2" deviceset="V--&gt;" device="" value="COOLFAN4+"/>
<part name="V86" library="SUPPLY2" deviceset="V--&gt;" device="" value="COOLFAN7+"/>
<part name="RE11" library="rele" deviceset="SCRELE" device=""/>
<part name="RE12" library="rele" deviceset="SCRELE" device=""/>
<part name="RE13" library="rele" deviceset="SCRELE" device=""/>
<part name="RE14" library="rele" deviceset="SCRELE" device=""/>
<part name="U$60" library="SUPPLY2" deviceset="GND" device=""/>
<part name="U$61" library="SUPPLY2" deviceset="GND" device=""/>
<part name="U$62" library="SUPPLY2" deviceset="GND" device=""/>
<part name="U$63" library="SUPPLY2" deviceset="GND" device=""/>
<part name="V87" library="SUPPLY2" deviceset="V--&gt;" device="" value="COOLFAN8+"/>
<part name="V88" library="SUPPLY2" deviceset="V--&gt;" device="" value="COOLFAN9+"/>
<part name="V89" library="SUPPLY2" deviceset="V--&gt;" device="" value="COOLFAN10+"/>
<part name="RE9" library="rele" deviceset="SCRELE" device=""/>
<part name="RE10" library="rele" deviceset="SCRELE" device=""/>
<part name="U$67" library="SUPPLY2" deviceset="GND" device=""/>
<part name="U$68" library="SUPPLY2" deviceset="GND" device=""/>
<part name="V92" library="SUPPLY2" deviceset="V--&gt;" device="" value="COOLFAN5+"/>
<part name="V93" library="SUPPLY2" deviceset="V--&gt;" device="" value="COOLFAN6+"/>
<part name="RE15" library="rele" deviceset="SCRELE" device=""/>
<part name="U$69" library="SUPPLY2" deviceset="GND" device=""/>
<part name="V94" library="SUPPLY2" deviceset="V--&gt;" device="" value="COOLFAN11+"/>
<part name="RE4" library="SKLADKA2@5" deviceset="RELECIVK" device="" value="EV200AAANA"/>
<part name="RE4A" library="SKLADKA2@2" deviceset="SPINAC1" device="" value=" "/>
<part name="V34" library="SUPPLY2" deviceset="V--&gt;" device="" value="AKUAUTO"/>
<part name="S2" library="RELAIS@2" deviceset="G2RA" device="" value="ODPOJ"/>
<part name="U$70" library="SUPPLY2" deviceset="+24V@2" device="" value="+24V"/>
<part name="U$71" library="SUPPLY2" deviceset="AKU" device=""/>
<part name="S1A" library="RELAIS@2" deviceset="G2RA" device="" value="ODPOJ"/>
<part name="S3" library="RELAIS@2" deviceset="G2RA" device="" value="ODPOJ"/>
<part name="FRAME11" library="VEIT_logotyp" deviceset="DINA3_L-AUTO" device="" value="ETA relé (pro SPAL ventilátory)"/>
<part name="RE_D2" library="RELAIS" deviceset="AUTORELE" device="" value="3TX7005"/>
<part name="FA_D1" library="FUSE" deviceset="AUTOPOJ" device="" value="3A"/>
<part name="RE_D3" library="RELAIS" deviceset="AUTORELE" device="" value="3TX7005"/>
<part name="R303" library="SMDA" deviceset="REZ" device="" value="8K2"/>
<part name="R302" library="SMDA" deviceset="REZ" device="" value="8K2"/>
<part name="R301" library="SMDA" deviceset="REZ" device="" value="1K8"/>
<part name="GND1" library="SUPPLY2" deviceset="GND" device=""/>
<part name="SVORKA19" library="Svorkovnice-3patra" deviceset="3SVORKA-10" device="" value="34"/>
<part name="SVORKA44" library="Svorkovnice-3patra" deviceset="3SVORKA-00" device="" value="33"/>
<part name="V53" library="SUPPLY2" deviceset="V--&gt;" device="" value="VENTCOOL1"/>
<part name="V54" library="SUPPLY2" deviceset="V--&gt;" device="" value="VENTCOOL2"/>
<part name="PRUCHODKA30" library="AUTO-schema" deviceset="PRUCHODKA" device=""/>
<part name="PRUCHODKA31" library="AUTO-schema" deviceset="PRUCHODKA" device=""/>
<part name="U$96" library="SUPPLY2" deviceset="GND" device=""/>
<part name="SVORKA46" library="Svorkovnice-3patra" deviceset="3SVORKA-01" device="" value="31"/>
<part name="V56" library="SUPPLY2" deviceset="V--&gt;" device="" value="SOLENOID"/>
<part name="PRUCHODKA32" library="AUTO-schema" deviceset="PRUCHODKA" device=""/>
<part name="RE_D4" library="SKLADKA2@5" deviceset="RELECIVK" device="" value="AUTORELE"/>
<part name="U$97" library="SUPPLY2" deviceset="+24V@2" device="" value="+24V"/>
<part name="U$100" library="CMI" deviceset="SPINAC" device=""/>
<part name="RE_D1" library="RELAIS" deviceset="AUTORELE" device="" value="3TX7005"/>
<part name="V5" library="SUPPLY2" deviceset="V--&gt;" device="" value="LOADLIGHT"/>
<part name="V6" library="SUPPLY2" deviceset="V--&gt;" device="" value="BLIGHT"/>
<part name="V4" library="SUPPLY2" deviceset="V--&gt;" device="" value="REV_LI"/>
<part name="PRUCHODKA33" library="AUTO-schema" deviceset="PRUCHODKA" device=""/>
<part name="PRUCHODKA34" library="AUTO-schema" deviceset="PRUCHODKA" device=""/>
<part name="PRUCHODKA35" library="AUTO-schema" deviceset="PRUCHODKA" device=""/>
<part name="V7" library="SUPPLY2" deviceset="V--&gt;" device="" value="CLUTCH"/>
<part name="PRUCHODKA36" library="AUTO-schema" deviceset="PRUCHODKA" device=""/>
<part name="RE22" library="SKLADKA2@5" deviceset="RELECIVK" device="" value="EV200AAANA"/>
<part name="RE4A1" library="SKLADKA2@2" deviceset="SPINAC1" device="" value=" "/>
<part name="V8" library="SUPPLY2" deviceset="V--&gt;" device="" value="STARTACCU"/>
<part name="S4" library="RELAIS@2" deviceset="G2RA" device="" value="ODPOJ"/>
<part name="U$87" library="SUPPLY2" deviceset="+24V@2" device="" value="+24V"/>
<part name="U$90" library="SUPPLY2" deviceset="AKU" device=""/>
<part name="V9" library="SUPPLY2" deviceset="V--&gt;" device="" value="RELIEF"/>
<part name="PRUCHODKA37" library="AUTO-schema" deviceset="PRUCHODKA" device=""/>
<part name="PRUCHODKA38" library="AUTO-schema" deviceset="PRUCHODKA" device=""/>
<part name="V10" library="SUPPLY2" deviceset="V--&gt;" device="" value="HP4"/>
<part name="V23" library="SUPPLY2" deviceset="V--&gt;" device="" value="BOX400VAC"/>
<part name="PRUCHODKA39" library="AUTO-schema" deviceset="PRUCHODKA" device=""/>
<part name="V47" library="SUPPLY2" deviceset="V--&gt;" device="" value="H/LSWITCH"/>
<part name="PRUCHODKA40" library="AUTO-schema" deviceset="PRUCHODKA" device=""/>
<part name="V55" library="SUPPLY2" deviceset="V--&gt;" device="" value="TH"/>
<part name="PRUCHODKA41" library="AUTO-schema" deviceset="PRUCHODKA" device=""/>
<part name="SVORKA6" library="Svorkovnice-3patra" deviceset="3SVORKA-00" device="" value="42"/>
<part name="SVORKA7" library="Svorkovnice-3patra" deviceset="3SVORKA-00" device="" value="43"/>
<part name="SVORKA48" library="Svorkovnice-3patra" deviceset="3SVORKA-10" device="" value="41"/>
<part name="V73" library="SUPPLY2" deviceset="V--&gt;" device="" value="WATER IN FUEL"/>
<part name="V95" library="SUPPLY2" deviceset="V--&gt;" device="" value="AIR FILTER"/>
<part name="V96" library="SUPPLY2" deviceset="V--&gt;" device="" value="COOLANT LEVEL"/>
<part name="V42" library="SUPPLY2" deviceset="V--&gt;" device="" value="FILTERTERM1"/>
<part name="V45" library="SUPPLY2" deviceset="V--&gt;" device="" value="FAILHEAT 2"/>
<part name="V67" library="SUPPLY2" deviceset="V--&gt;" device="" value="HEATSTART 2"/>
<part name="V68" library="SUPPLY2@2" deviceset="V&lt;--" device="" value="FLAME2"/>
<part name="V69" library="SUPPLY2" deviceset="V--&gt;" device="" value="PUMPSTART 2"/>
<part name="V46" library="SUPPLY2" deviceset="V--&gt;" device="" value="HEATSTART 1"/>
<part name="V71" library="SUPPLY2" deviceset="V--&gt;" device="" value="PUMPSTART 1"/>
<part name="V72" library="SUPPLY2" deviceset="V--&gt;" device="" value="FAILHEAT 1"/>
<part name="V49" library="SUPPLY2@2" deviceset="V&lt;--" device="" value="FLAME1"/>
<part name="V63" library="SUPPLY2" deviceset="V--&gt;" device="" value="FILTERTERM1"/>
<part name="V77" library="SUPPLY2@2" deviceset="V&lt;--" device="" value="FUELHEAT2"/>
<part name="V78" library="SUPPLY2@2" deviceset="V&lt;--" device="" value="FUELHEAT1"/>
<part name="V79" library="SUPPLY2" deviceset="V--&gt;" device="" value="HEATPUMP 2"/>
<part name="V80" library="SUPPLY2" deviceset="V--&gt;" device="" value="HEATPUMP 1"/>
<part name="V81" library="SUPPLY2@2" deviceset="V&lt;--" device="" value="HEATPUMP"/>
<part name="2" library="SKLADKA2@4" deviceset="PRBOD" device="" value=" "/>
<part name="4" library="SKLADKA2@4" deviceset="PRBOD" device="" value=" "/>
<part name="6" library="SKLADKA2@4" deviceset="PRBOD" device="" value=" "/>
<part name="1" library="SKLADKA2@4" deviceset="PRBOD" device="" value=" "/>
<part name="8" library="SKLADKA2@4" deviceset="PRBOD" device="" value=" "/>
<part name="10" library="SKLADKA2@4" deviceset="PRBOD" device="" value=" "/>
<part name="12" library="SKLADKA2@4" deviceset="PRBOD" device="" value=" "/>
<part name="14" library="SKLADKA2@4" deviceset="PRBOD" device="" value=" "/>
<part name="9" library="SKLADKA2@4" deviceset="PRBOD" device="" value=" "/>
<part name="16" library="SKLADKA2@4" deviceset="PRBOD" device="" value=" "/>
<part name="18" library="SKLADKA2@4" deviceset="PRBOD" device="" value=" "/>
<part name="20" library="SKLADKA2@4" deviceset="PRBOD" device="" value=" "/>
<part name="22" library="SKLADKA2@4" deviceset="PRBOD" device="" value=" "/>
<part name="17" library="SKLADKA2@4" deviceset="PRBOD" device="" value=" "/>
<part name="24" library="SKLADKA2@4" deviceset="PRBOD" device="" value=" "/>
<part name="U$101" library="SUPPLY2" deviceset="GND" device=""/>
<part name="D28" library="DIODE" deviceset="1N4004" device="" value="1N4004"/>
<part name="D29" library="DIODE" deviceset="1N4004" device="" value="1N4004"/>
<part name="P2" library="TRIMPOT" deviceset="RS3" device="" value="RS3"/>
<part name="R1" library="R" deviceset="0207/10" device="" value="680R"/>
<part name="U$102" library="SUPPLY2" deviceset="GND" device=""/>
<part name="U$103" library="SUPPLY2" deviceset="+10V" device=""/>
<part name="26" library="SKLADKA2@4" deviceset="PRBOD" device="" value=" "/>
<part name="28" library="SKLADKA2@4" deviceset="PRBOD" device="" value=" "/>
<part name="30" library="SKLADKA2@4" deviceset="PRBOD" device="" value=" "/>
<part name="25" library="SKLADKA2@4" deviceset="PRBOD" device="" value=" "/>
<part name="32" library="SKLADKA2@4" deviceset="PRBOD" device="" value=" "/>
<part name="U$105" library="SUPPLY2" deviceset="GND" device=""/>
<part name="U$106" library="SUPPLY2" deviceset="GND" device=""/>
<part name="U$107" library="SUPPLY2" deviceset="+10V" device=""/>
<part name="U$109" library="SUPPLY2" deviceset="GND" device=""/>
<part name="R2" library="SMDA" deviceset="REZ" device="" value="2K2"/>
<part name="R3" library="SMDA" deviceset="REZ" device="" value="6K8"/>
<part name="34" library="SKLADKA2@4" deviceset="PRBOD" device="" value=" "/>
<part name="36" library="SKLADKA2@4" deviceset="PRBOD" device="" value=" "/>
<part name="38" library="SKLADKA2@4" deviceset="PRBOD" device="" value=" "/>
<part name="33" library="SKLADKA2@4" deviceset="PRBOD" device="" value=" "/>
<part name="40" library="SKLADKA2@4" deviceset="PRBOD" device="" value=" "/>
<part name="U$110" library="CMI" deviceset="ZAROVKA_SE_SPINACEM" device=""/>
<part name="S9" library="CMI" deviceset="SPINAC_S_ZAROVKOU" device=""/>
<part name="U$111" library="SUPPLY2" deviceset="GND" device=""/>
<part name="S10" library="CMI" deviceset="SPINAC_S_ZAROVKOU" device=""/>
<part name="FRAME12" library="VEIT_logotyp" deviceset="DINA3_L-AUTO" device="" value="Pokyny"/>
<part name="S5" library="AUTO-schema" deviceset="OTOCNY_PREPINAC_2" device=""/>
<part name="S6" library="AUTO-schema" deviceset="OTOCNY_PREPINAC_3" device=""/>
<part name="S7" library="AUTO-schema" deviceset="OTOCNY_PREPINAC" device=""/>
<part name="S8" library="AUTO-schema" deviceset="OTOCNY_PREPINAC_H2" device=""/>
</parts>
<sheets>
<sheet>
<description>Konfigurační list</description>
<plain>
</plain>
<instances>
<instance part="FRAME1" gate="G$1" x="0" y="0"/>
<instance part="FRAME1" gate="G$2" x="287.02" y="0"/>
</instances>
<busses>
</busses>
<nets>
</nets>
</sheet>
<sheet>
<description>Hlavní přepínač</description>
<plain>
<text x="111.76" y="152.4" size="1.778" layer="94" align="bottom-center">Diody umístit na přepínač</text>
<text x="101.6" y="134.62" size="1.778" layer="94" align="center-right">2x 1N4007</text>
<wire x1="134.62" y1="78.74" x2="170.18" y2="78.74" width="0.1524" layer="94" style="shortdash"/>
<wire x1="187.96" y1="78.74" x2="223.52" y2="78.74" width="0.1524" layer="94" style="shortdash"/>
<wire x1="241.3" y1="78.74" x2="276.86" y2="78.74" width="0.1524" layer="94" style="shortdash"/>
<wire x1="294.64" y1="78.74" x2="330.2" y2="78.74" width="0.1524" layer="94" style="shortdash"/>
</plain>
<instances>
<instance part="FRAME4" gate="G$1" x="0" y="0"/>
<instance part="FRAME4" gate="G$2" x="287.02" y="0"/>
<instance part="SVORKA3" gate="G$1" x="66.04" y="119.38" rot="R270"/>
<instance part="TMC1" gate="15" x="7.62" y="114.3"/>
<instance part="TMC1" gate="34" x="7.62" y="121.92"/>
<instance part="TMC1" gate="36" x="7.62" y="119.38"/>
<instance part="TMC1" gate="38" x="7.62" y="116.84"/>
<instance part="S1" gate="A" x="127" y="78.74" rot="R90"/>
<instance part="S1" gate="G$1" x="109.22" y="78.74"/>
<instance part="S1" gate="B" x="180.34" y="78.74" rot="R90"/>
<instance part="S1" gate="C" x="233.68" y="78.74" rot="R90"/>
<instance part="S1" gate="D" x="287.02" y="78.74" rot="R90"/>
<instance part="U$14" gate="GND" x="124.46" y="60.96"/>
<instance part="U$19" gate="G$1" x="106.68" y="134.62" rot="R270"/>
<instance part="U$18" gate="G$1" x="116.84" y="134.62" rot="R270"/>
<instance part="SVORKA16" gate="G$1" x="66.04" y="218.44" rot="R270"/>
<instance part="TMC1" gate="20" x="7.62" y="218.44"/>
<instance part="TMC1" gate="22" x="7.62" y="220.98"/>
<instance part="TMC1" gate="24" x="7.62" y="215.9"/>
<instance part="U$21" gate="GND" x="198.12" y="83.82"/>
<instance part="U$25" gate="GND" x="304.8" y="83.82"/>
<instance part="P1" gate="1" x="213.36" y="109.22" smashed="yes" rot="MR0">
<attribute name="NAME" x="205.74" y="110.49" size="1.524" layer="95" rot="MR90"/>
<attribute name="VALUE" x="208.2292" y="104.775" size="1.524" layer="96" rot="MR90"/>
</instance>
<instance part="R23" gate="1" x="213.36" y="96.52" rot="MR270"/>
<instance part="U$57" gate="GND" x="213.36" y="83.82" rot="MR0"/>
<instance part="U$65" gate="G$1" x="213.36" y="119.38" rot="MR0"/>
<instance part="U$26" gate="GND" x="261.62" y="86.36"/>
<instance part="R19" gate="G$1" x="260.35" y="96.52" smashed="yes" rot="R90">
<attribute name="NAME" x="258.1275" y="97.4725" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="258.445" y="94.615" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R21" gate="G$1" x="260.35" y="106.68" smashed="yes" rot="R90">
<attribute name="NAME" x="258.445" y="110.1725" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="258.1275" y="107.6325" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="U$27" gate="G$1" x="261.62" y="119.38" rot="MR0"/>
<instance part="SVORKA4" gate="G$3" x="66.04" y="152.4" rot="R270"/>
<instance part="SVORKA4" gate="G$5" x="66.04" y="152.4" rot="R270"/>
<instance part="S1" gate="E" x="340.36" y="78.74" rot="R90"/>
</instances>
<busses>
</busses>
<nets>
<net name="EMERGENCY" class="0">
<segment>
<pinref part="TMC1" gate="15" pin="EMERGENCY"/>
<pinref part="SVORKA3" gate="G$1" pin="6"/>
<wire x1="40.64" y1="114.3" x2="53.34" y2="114.3" width="0.1524" layer="91"/>
<wire x1="53.34" y1="114.3" x2="53.34" y2="116.84" width="0.1524" layer="91"/>
<wire x1="53.34" y1="116.84" x2="55.88" y2="116.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="HEAT" class="0">
<segment>
<pinref part="SVORKA3" gate="G$1" pin="5"/>
<pinref part="TMC1" gate="36" pin="HEAT"/>
<wire x1="58.42" y1="119.38" x2="40.64" y2="119.38" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SVORKA3" gate="G$1" pin="2"/>
<pinref part="S1" gate="A" pin="III"/>
<wire x1="73.66" y1="119.38" x2="106.68" y2="119.38" width="0.1524" layer="91"/>
<wire x1="106.68" y1="119.38" x2="129.54" y2="119.38" width="0.1524" layer="91"/>
<wire x1="129.54" y1="119.38" x2="129.54" y2="88.9" width="0.1524" layer="91"/>
<wire x1="106.68" y1="132.08" x2="106.68" y2="119.38" width="0.1524" layer="91"/>
<junction x="106.68" y="119.38"/>
<wire x1="129.54" y1="119.38" x2="137.16" y2="119.38" width="0.1524" layer="91"/>
<junction x="129.54" y="119.38"/>
<label x="137.16" y="119.38" size="1.27" layer="95" xref="yes"/>
<label x="78.74" y="119.38" size="1.778" layer="95"/>
</segment>
</net>
<net name="COOL" class="0">
<segment>
<pinref part="TMC1" gate="34" pin="COOL"/>
<pinref part="SVORKA3" gate="G$1" pin="4"/>
<wire x1="40.64" y1="121.92" x2="60.96" y2="121.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SVORKA3" gate="G$1" pin="3"/>
<pinref part="S1" gate="A" pin="IV"/>
<wire x1="71.12" y1="121.92" x2="116.84" y2="121.92" width="0.1524" layer="91"/>
<wire x1="116.84" y1="121.92" x2="132.08" y2="121.92" width="0.1524" layer="91"/>
<wire x1="132.08" y1="121.92" x2="132.08" y2="88.9" width="0.1524" layer="91"/>
<wire x1="116.84" y1="132.08" x2="116.84" y2="121.92" width="0.1524" layer="91"/>
<junction x="116.84" y="121.92"/>
<wire x1="132.08" y1="121.92" x2="137.16" y2="121.92" width="0.1524" layer="91"/>
<junction x="132.08" y="121.92"/>
<label x="137.16" y="121.92" size="1.27" layer="95" xref="yes"/>
<label x="78.74" y="121.92" size="1.778" layer="95"/>
</segment>
</net>
<net name="VENTILATION" class="0">
<segment>
<pinref part="TMC1" gate="38" pin="VENTILATION"/>
<wire x1="40.64" y1="116.84" x2="48.26" y2="116.84" width="0.1524" layer="91"/>
<wire x1="48.26" y1="116.84" x2="48.26" y2="149.86" width="0.1524" layer="91"/>
<wire x1="48.26" y1="149.86" x2="55.88" y2="149.86" width="0.1524" layer="91"/>
<pinref part="SVORKA4" gate="G$3" pin="6"/>
</segment>
</net>
<net name="!TMC_EMERGENCY" class="0">
<segment>
<label x="137.16" y="116.84" size="1.27" layer="95" xref="yes"/>
<pinref part="S1" gate="A" pin="I"/>
<wire x1="124.46" y1="88.9" x2="124.46" y2="116.84" width="0.1524" layer="91"/>
<wire x1="124.46" y1="116.84" x2="76.2" y2="116.84" width="0.1524" layer="91"/>
<pinref part="SVORKA3" gate="G$1" pin="1"/>
<wire x1="137.16" y1="116.84" x2="124.46" y2="116.84" width="0.1524" layer="91"/>
<junction x="124.46" y="116.84"/>
<label x="78.74" y="116.84" size="1.778" layer="95"/>
</segment>
</net>
<net name="!VENTILATION_ON" class="0">
<segment>
<label x="137.16" y="149.86" size="1.27" layer="95" xref="yes"/>
<wire x1="76.2" y1="149.86" x2="106.68" y2="149.86" width="0.1524" layer="91"/>
<pinref part="S1" gate="A" pin="II"/>
<wire x1="106.68" y1="149.86" x2="116.84" y2="149.86" width="0.1524" layer="91"/>
<wire x1="116.84" y1="149.86" x2="127" y2="149.86" width="0.1524" layer="91"/>
<wire x1="127" y1="149.86" x2="127" y2="88.9" width="0.1524" layer="91"/>
<wire x1="106.68" y1="137.16" x2="106.68" y2="149.86" width="0.1524" layer="91"/>
<junction x="106.68" y="149.86"/>
<wire x1="116.84" y1="137.16" x2="116.84" y2="149.86" width="0.1524" layer="91"/>
<junction x="116.84" y="149.86"/>
<wire x1="137.16" y1="149.86" x2="127" y2="149.86" width="0.1524" layer="91"/>
<junction x="127" y="149.86"/>
<label x="78.74" y="149.86" size="1.778" layer="95"/>
<pinref part="SVORKA4" gate="G$3" pin="1"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="S1" gate="A" pin="0"/>
<pinref part="U$14" gate="GND" pin="GND"/>
<wire x1="124.46" y1="66.04" x2="124.46" y2="68.58" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="S1" gate="B" pin="II"/>
<wire x1="180.34" y1="88.9" x2="180.34" y2="91.44" width="0.1524" layer="91"/>
<wire x1="180.34" y1="91.44" x2="185.42" y2="91.44" width="0.1524" layer="91"/>
<pinref part="S1" gate="B" pin="IV"/>
<wire x1="185.42" y1="91.44" x2="185.42" y2="88.9" width="0.1524" layer="91"/>
<wire x1="185.42" y1="91.44" x2="198.12" y2="91.44" width="0.1524" layer="91"/>
<junction x="185.42" y="91.44"/>
<wire x1="198.12" y1="91.44" x2="198.12" y2="88.9" width="0.1524" layer="91"/>
<pinref part="U$21" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="S1" gate="D" pin="III"/>
<wire x1="289.56" y1="88.9" x2="289.56" y2="91.44" width="0.1524" layer="91"/>
<wire x1="289.56" y1="91.44" x2="304.8" y2="91.44" width="0.1524" layer="91"/>
<pinref part="U$25" gate="GND" pin="GND"/>
<wire x1="304.8" y1="91.44" x2="304.8" y2="88.9" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="213.36" y1="88.9" x2="213.36" y2="91.44" width="0.1524" layer="91"/>
<pinref part="R23" gate="1" pin="2"/>
<pinref part="U$57" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U$26" gate="GND" pin="GND"/>
<pinref part="R19" gate="G$1" pin="1"/>
</segment>
</net>
<net name="CONHEAT" class="0">
<segment>
<pinref part="TMC1" gate="22" pin="CONHEAT"/>
<pinref part="SVORKA16" gate="G$1" pin="4"/>
<wire x1="40.64" y1="220.98" x2="60.96" y2="220.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CONFLOOR" class="0">
<segment>
<pinref part="SVORKA16" gate="G$1" pin="5"/>
<pinref part="TMC1" gate="20" pin="CONFLOOR"/>
<wire x1="58.42" y1="218.44" x2="40.64" y2="218.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CONI_R" class="0">
<segment>
<pinref part="TMC1" gate="24" pin="CONI_R"/>
<pinref part="SVORKA16" gate="G$1" pin="6"/>
<wire x1="40.64" y1="215.9" x2="55.88" y2="215.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CON3" class="0">
<segment>
<pinref part="SVORKA16" gate="G$1" pin="3"/>
<label x="78.74" y="220.98" size="1.778" layer="95"/>
<pinref part="S1" gate="B" pin="I"/>
<wire x1="71.12" y1="220.98" x2="177.8" y2="220.98" width="0.1524" layer="91"/>
<wire x1="177.8" y1="220.98" x2="177.8" y2="88.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CON2" class="0">
<segment>
<pinref part="SVORKA16" gate="G$1" pin="2"/>
<label x="78.74" y="218.44" size="1.778" layer="95"/>
<pinref part="S1" gate="D" pin="I"/>
<wire x1="73.66" y1="218.44" x2="284.48" y2="218.44" width="0.1524" layer="91"/>
<wire x1="284.48" y1="218.44" x2="284.48" y2="88.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CON1" class="0">
<segment>
<pinref part="SVORKA16" gate="G$1" pin="1"/>
<label x="78.74" y="215.9" size="1.778" layer="95"/>
<pinref part="S1" gate="C" pin="I"/>
<wire x1="76.2" y1="215.9" x2="231.14" y2="215.9" width="0.1524" layer="91"/>
<wire x1="231.14" y1="215.9" x2="231.14" y2="88.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CON6" class="0">
<segment>
<pinref part="S1" gate="B" pin="0"/>
<wire x1="177.8" y1="68.58" x2="177.8" y2="53.34" width="0.1524" layer="91"/>
<wire x1="177.8" y1="53.34" x2="195.58" y2="53.34" width="0.1524" layer="91"/>
<label x="195.58" y="53.34" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="CON4" class="0">
<segment>
<pinref part="S1" gate="C" pin="0"/>
<wire x1="231.14" y1="68.58" x2="231.14" y2="53.34" width="0.1524" layer="91"/>
<wire x1="231.14" y1="53.34" x2="248.92" y2="53.34" width="0.1524" layer="91"/>
<label x="248.92" y="53.34" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="CON5" class="0">
<segment>
<pinref part="S1" gate="D" pin="0"/>
<wire x1="284.48" y1="68.58" x2="284.48" y2="53.34" width="0.1524" layer="91"/>
<wire x1="284.48" y1="53.34" x2="302.26" y2="53.34" width="0.1524" layer="91"/>
<label x="302.26" y="53.34" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="337.82" y1="68.58" x2="337.82" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$58" class="0">
<segment>
<pinref part="R23" gate="1" pin="1"/>
<pinref part="P1" gate="1" pin="1"/>
</segment>
</net>
<net name="+10V" class="0">
<segment>
<pinref part="U$65" gate="G$1" pin="+10V"/>
<pinref part="P1" gate="1" pin="3"/>
</segment>
<segment>
<pinref part="S1" gate="D" pin="IV"/>
<wire x1="292.1" y1="88.9" x2="292.1" y2="96.52" width="0.1524" layer="91"/>
<wire x1="292.1" y1="96.52" x2="289.56" y2="96.52" width="0.1524" layer="91"/>
<pinref part="S1" gate="D" pin="II"/>
<wire x1="289.56" y1="96.52" x2="287.02" y2="96.52" width="0.1524" layer="91"/>
<wire x1="287.02" y1="96.52" x2="287.02" y2="88.9" width="0.1524" layer="91"/>
<pinref part="R21" gate="G$1" pin="2"/>
<wire x1="261.62" y1="111.76" x2="261.62" y2="114.3" width="0.1524" layer="91"/>
<wire x1="261.62" y1="114.3" x2="289.56" y2="114.3" width="0.1524" layer="91"/>
<wire x1="289.56" y1="114.3" x2="289.56" y2="96.52" width="0.1524" layer="91"/>
<junction x="289.56" y="96.52"/>
<pinref part="S1" gate="C" pin="II"/>
<wire x1="233.68" y1="88.9" x2="233.68" y2="96.52" width="0.1524" layer="91"/>
<wire x1="233.68" y1="96.52" x2="236.22" y2="96.52" width="0.1524" layer="91"/>
<pinref part="S1" gate="C" pin="IV"/>
<wire x1="236.22" y1="96.52" x2="238.76" y2="96.52" width="0.1524" layer="91"/>
<wire x1="238.76" y1="96.52" x2="238.76" y2="88.9" width="0.1524" layer="91"/>
<wire x1="261.62" y1="114.3" x2="236.22" y2="114.3" width="0.1524" layer="91"/>
<wire x1="236.22" y1="114.3" x2="236.22" y2="96.52" width="0.1524" layer="91"/>
<junction x="261.62" y="114.3"/>
<junction x="236.22" y="96.52"/>
<pinref part="U$27" gate="G$1" pin="+10V"/>
<wire x1="261.62" y1="116.84" x2="261.62" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$31" class="0">
<segment>
<wire x1="182.88" y1="109.22" x2="208.28" y2="109.22" width="0.1524" layer="91"/>
<pinref part="P1" gate="1" pin="2"/>
<pinref part="S1" gate="B" pin="III"/>
<wire x1="182.88" y1="88.9" x2="182.88" y2="109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="R19" gate="G$1" pin="2"/>
<pinref part="R21" gate="G$1" pin="1"/>
<wire x1="261.62" y1="101.6" x2="251.46" y2="101.6" width="0.1524" layer="91"/>
<wire x1="251.46" y1="101.6" x2="251.46" y2="91.44" width="0.1524" layer="91"/>
<junction x="261.62" y="101.6"/>
<wire x1="251.46" y1="91.44" x2="236.22" y2="91.44" width="0.1524" layer="91"/>
<pinref part="S1" gate="C" pin="III"/>
<wire x1="236.22" y1="91.44" x2="236.22" y2="88.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="INVLIN" class="0">
<segment>
<wire x1="337.82" y1="53.34" x2="355.6" y2="53.34" width="0.1524" layer="91"/>
<label x="355.6" y="53.34" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<description>Deska relé</description>
<plain>
<wire x1="109.22" y1="43.18" x2="350.52" y2="43.18" width="0.1524" layer="94" style="longdash"/>
<wire x1="350.52" y1="43.18" x2="350.52" y2="132.08" width="0.1524" layer="94" style="longdash"/>
<wire x1="132.08" y1="33.02" x2="157.48" y2="2.54" width="0.1524" layer="94"/>
<wire x1="157.48" y1="33.02" x2="132.08" y2="2.54" width="0.1524" layer="94"/>
<text x="170.18" y="231.14" size="1.27" layer="94">Výstup z desky 7P</text>
<text x="154.94" y="210.82" size="5.08" layer="92">????</text>
<text x="365.76" y="114.3" size="1.6764" layer="95">LOWPRESSURE</text>
<text x="365.76" y="109.22" size="1.6764" layer="95">PT5-07M</text>
<text x="381" y="99.06" size="1.6764" layer="95" rot="R180">HIGHPRESSURE</text>
<text x="381" y="93.98" size="1.6764" layer="95" rot="R180">PT5-30</text>
<text x="365.76" y="111.76" size="1.6764" layer="95">SENSOR</text>
<text x="381" y="96.52" size="1.6764" layer="95" rot="R180">SENSOR</text>
<text x="132.08" y="17.78" size="1.778" layer="92" rot="R180">Ovládá se z desky autostartu</text>
<text x="132.08" y="45.72" size="1.778" layer="94" rot="R180">Rozvaděč</text>
<text x="132.08" y="40.64" size="1.778" layer="94" rot="R180" align="top-left">Strojovna</text>
</plain>
<instances>
<instance part="FRAME5" gate="G$1" x="0" y="0"/>
<instance part="FRAME5" gate="G$2" x="287.02" y="0"/>
<instance part="DPS2" gate="G$1" x="5.08" y="172.72"/>
<instance part="SERVO6" gate="G$1" x="271.78" y="17.78"/>
<instance part="PRUCHODKA5" gate="G$1" x="271.78" y="43.18" rot="R270"/>
<instance part="SERVO7" gate="G$1" x="246.38" y="17.78"/>
<instance part="PRUCHODKA6" gate="G$1" x="246.38" y="43.18" rot="R270"/>
<instance part="SERVO8" gate="G$1" x="220.98" y="17.78"/>
<instance part="PRUCHODKA7" gate="G$1" x="220.98" y="43.18" rot="R270"/>
<instance part="SERVO9" gate="G$1" x="195.58" y="17.78"/>
<instance part="PRUCHODKA8" gate="G$1" x="195.58" y="43.18" rot="R270"/>
<instance part="SERVO10" gate="G$1" x="170.18" y="17.78"/>
<instance part="PRUCHODKA9" gate="G$1" x="170.18" y="43.18" rot="R270"/>
<instance part="SERVO11" gate="G$1" x="144.78" y="17.78"/>
<instance part="PRUCHODKA10" gate="G$1" x="144.78" y="43.18" rot="R270"/>
<instance part="U$2" gate="G$1" x="73.66" y="152.4" rot="R90"/>
<instance part="U$20" gate="+24V" x="71.12" y="160.02" rot="R90"/>
<instance part="SVORKA17" gate="G$1" x="124.46" y="248.92" rot="R270"/>
<instance part="SVORKA18" gate="G$1" x="124.46" y="228.6" rot="R270"/>
<instance part="TMC1" gate="25" x="66.04" y="228.6"/>
<instance part="TMC1" gate="27" x="66.04" y="246.38"/>
<instance part="TMC1" gate="29" x="66.04" y="248.92"/>
<instance part="TMC1" gate="31" x="66.04" y="251.46"/>
<instance part="TMC1" gate="37" x="66.04" y="226.06"/>
<instance part="TMC1" gate="9" x="66.04" y="231.14"/>
<instance part="U$33" gate="G$1" x="40.64" y="137.16" rot="R90"/>
<instance part="V52" gate="G$1" x="378.46" y="106.68" smashed="yes" rot="MR0">
<attribute name="VALUE" x="377.19" y="107.442" size="1.524" layer="96" rot="R180"/>
</instance>
<instance part="V59" gate="G$1" x="378.46" y="101.6" smashed="yes" rot="MR0">
<attribute name="VALUE" x="377.19" y="102.362" size="1.524" layer="96" rot="R180"/>
</instance>
<instance part="PRUCHODKA13" gate="G$1" x="350.52" y="101.6"/>
<instance part="PRUCHODKA14" gate="G$1" x="350.52" y="106.68"/>
<instance part="SVORKA34" gate="G$1" x="124.46" y="205.74" rot="R270"/>
<instance part="TMC1" gate="7" x="66.04" y="203.2"/>
</instances>
<busses>
<bus name="1,2,3,5">
<segment>
<wire x1="271.78" y1="86.36" x2="271.78" y2="25.4" width="0.762" layer="92"/>
</segment>
<segment>
<wire x1="246.38" y1="86.36" x2="246.38" y2="25.4" width="0.762" layer="92"/>
</segment>
<segment>
<wire x1="220.98" y1="86.36" x2="220.98" y2="25.4" width="0.762" layer="92"/>
</segment>
<segment>
<wire x1="195.58" y1="86.36" x2="195.58" y2="25.4" width="0.762" layer="92"/>
</segment>
<segment>
<wire x1="170.18" y1="86.36" x2="170.18" y2="25.4" width="0.762" layer="92"/>
</segment>
<segment>
<wire x1="144.78" y1="86.36" x2="144.78" y2="25.4" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="HP+,HP-">
<segment>
<wire x1="363.22" y1="101.6" x2="220.98" y2="101.6" width="0.762" layer="92"/>
<wire x1="220.98" y1="101.6" x2="215.9" y2="106.68" width="0.762" layer="92"/>
<wire x1="215.9" y1="106.68" x2="215.9" y2="149.86" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="LP+,LP-">
<segment>
<wire x1="363.22" y1="106.68" x2="223.52" y2="106.68" width="0.762" layer="92"/>
<wire x1="223.52" y1="106.68" x2="220.98" y2="109.22" width="0.762" layer="92"/>
<wire x1="220.98" y1="109.22" x2="220.98" y2="139.7" width="0.762" layer="92"/>
</segment>
</bus>
</busses>
<nets>
<net name="5" class="0">
<segment>
<wire x1="271.78" y1="86.36" x2="274.32" y2="88.9" width="0.1524" layer="91"/>
<pinref part="DPS2" gate="G$1" pin="JP6A-4"/>
<wire x1="274.32" y1="88.9" x2="279.4" y2="88.9" width="0.1524" layer="91"/>
<label x="276.86" y="88.9" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="246.38" y1="86.36" x2="248.92" y2="88.9" width="0.1524" layer="91"/>
<wire x1="248.92" y1="88.9" x2="254" y2="88.9" width="0.1524" layer="91"/>
<label x="251.46" y="88.9" size="1.778" layer="95"/>
<pinref part="DPS2" gate="G$1" pin="JP5A-4"/>
</segment>
<segment>
<wire x1="220.98" y1="86.36" x2="223.52" y2="88.9" width="0.1524" layer="91"/>
<wire x1="223.52" y1="88.9" x2="228.6" y2="88.9" width="0.1524" layer="91"/>
<label x="226.06" y="88.9" size="1.778" layer="95"/>
<pinref part="DPS2" gate="G$1" pin="JP4A-4"/>
</segment>
<segment>
<wire x1="195.58" y1="86.36" x2="198.12" y2="88.9" width="0.1524" layer="91"/>
<wire x1="198.12" y1="88.9" x2="203.2" y2="88.9" width="0.1524" layer="91"/>
<label x="200.66" y="88.9" size="1.778" layer="95"/>
<pinref part="DPS2" gate="G$1" pin="JP3A-4"/>
</segment>
<segment>
<wire x1="170.18" y1="86.36" x2="172.72" y2="88.9" width="0.1524" layer="91"/>
<wire x1="172.72" y1="88.9" x2="177.8" y2="88.9" width="0.1524" layer="91"/>
<label x="175.26" y="88.9" size="1.778" layer="95"/>
<pinref part="DPS2" gate="G$1" pin="JP2A-4"/>
</segment>
<segment>
<wire x1="144.78" y1="86.36" x2="147.32" y2="88.9" width="0.1524" layer="91"/>
<wire x1="147.32" y1="88.9" x2="152.4" y2="88.9" width="0.1524" layer="91"/>
<label x="149.86" y="88.9" size="1.778" layer="95"/>
<pinref part="DPS2" gate="G$1" pin="JP1A-4"/>
</segment>
</net>
<net name="3" class="0">
<segment>
<wire x1="271.78" y1="81.28" x2="274.32" y2="83.82" width="0.1524" layer="91"/>
<pinref part="DPS2" gate="G$1" pin="JP6A-3"/>
<wire x1="274.32" y1="83.82" x2="279.4" y2="83.82" width="0.1524" layer="91"/>
<label x="276.86" y="83.82" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="246.38" y1="81.28" x2="248.92" y2="83.82" width="0.1524" layer="91"/>
<wire x1="248.92" y1="83.82" x2="254" y2="83.82" width="0.1524" layer="91"/>
<label x="251.46" y="83.82" size="1.778" layer="95"/>
<pinref part="DPS2" gate="G$1" pin="JP5A-3"/>
</segment>
<segment>
<wire x1="220.98" y1="81.28" x2="223.52" y2="83.82" width="0.1524" layer="91"/>
<wire x1="223.52" y1="83.82" x2="228.6" y2="83.82" width="0.1524" layer="91"/>
<label x="226.06" y="83.82" size="1.778" layer="95"/>
<pinref part="DPS2" gate="G$1" pin="JP4A-3"/>
</segment>
<segment>
<wire x1="195.58" y1="81.28" x2="198.12" y2="83.82" width="0.1524" layer="91"/>
<wire x1="198.12" y1="83.82" x2="203.2" y2="83.82" width="0.1524" layer="91"/>
<label x="200.66" y="83.82" size="1.778" layer="95"/>
<pinref part="DPS2" gate="G$1" pin="JP3A-3"/>
</segment>
<segment>
<wire x1="170.18" y1="81.28" x2="172.72" y2="83.82" width="0.1524" layer="91"/>
<wire x1="172.72" y1="83.82" x2="177.8" y2="83.82" width="0.1524" layer="91"/>
<label x="175.26" y="83.82" size="1.778" layer="95"/>
<pinref part="DPS2" gate="G$1" pin="JP2A-3"/>
</segment>
<segment>
<wire x1="144.78" y1="81.28" x2="147.32" y2="83.82" width="0.1524" layer="91"/>
<wire x1="147.32" y1="83.82" x2="152.4" y2="83.82" width="0.1524" layer="91"/>
<label x="149.86" y="83.82" size="1.778" layer="95"/>
<pinref part="DPS2" gate="G$1" pin="JP1A-3"/>
</segment>
</net>
<net name="1" class="0">
<segment>
<wire x1="271.78" y1="71.12" x2="274.32" y2="73.66" width="0.1524" layer="91"/>
<pinref part="DPS2" gate="G$1" pin="JP6A-1"/>
<wire x1="274.32" y1="73.66" x2="279.4" y2="73.66" width="0.1524" layer="91"/>
<label x="276.86" y="73.66" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="246.38" y1="71.12" x2="248.92" y2="73.66" width="0.1524" layer="91"/>
<wire x1="248.92" y1="73.66" x2="254" y2="73.66" width="0.1524" layer="91"/>
<label x="251.46" y="73.66" size="1.778" layer="95"/>
<pinref part="DPS2" gate="G$1" pin="JP5A-1"/>
</segment>
<segment>
<wire x1="220.98" y1="71.12" x2="223.52" y2="73.66" width="0.1524" layer="91"/>
<wire x1="223.52" y1="73.66" x2="228.6" y2="73.66" width="0.1524" layer="91"/>
<label x="226.06" y="73.66" size="1.778" layer="95"/>
<pinref part="DPS2" gate="G$1" pin="JP4A-1"/>
</segment>
<segment>
<wire x1="195.58" y1="71.12" x2="198.12" y2="73.66" width="0.1524" layer="91"/>
<wire x1="198.12" y1="73.66" x2="203.2" y2="73.66" width="0.1524" layer="91"/>
<label x="200.66" y="73.66" size="1.778" layer="95"/>
<pinref part="DPS2" gate="G$1" pin="JP3A-1"/>
</segment>
<segment>
<wire x1="170.18" y1="71.12" x2="172.72" y2="73.66" width="0.1524" layer="91"/>
<wire x1="172.72" y1="73.66" x2="177.8" y2="73.66" width="0.1524" layer="91"/>
<label x="175.26" y="73.66" size="1.778" layer="95"/>
<pinref part="DPS2" gate="G$1" pin="JP2A-1"/>
</segment>
<segment>
<wire x1="144.78" y1="71.12" x2="147.32" y2="73.66" width="0.1524" layer="91"/>
<wire x1="147.32" y1="73.66" x2="152.4" y2="73.66" width="0.1524" layer="91"/>
<label x="149.86" y="73.66" size="1.778" layer="95"/>
<pinref part="DPS2" gate="G$1" pin="JP1A-1"/>
</segment>
</net>
<net name="2" class="0">
<segment>
<wire x1="271.78" y1="76.2" x2="274.32" y2="78.74" width="0.1524" layer="91"/>
<pinref part="DPS2" gate="G$1" pin="JP6A-2"/>
<wire x1="274.32" y1="78.74" x2="279.4" y2="78.74" width="0.1524" layer="91"/>
<label x="276.86" y="78.74" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="246.38" y1="76.2" x2="248.92" y2="78.74" width="0.1524" layer="91"/>
<wire x1="248.92" y1="78.74" x2="254" y2="78.74" width="0.1524" layer="91"/>
<label x="251.46" y="78.74" size="1.778" layer="95"/>
<pinref part="DPS2" gate="G$1" pin="JP5A-2"/>
</segment>
<segment>
<wire x1="220.98" y1="76.2" x2="223.52" y2="78.74" width="0.1524" layer="91"/>
<wire x1="223.52" y1="78.74" x2="228.6" y2="78.74" width="0.1524" layer="91"/>
<label x="226.06" y="78.74" size="1.778" layer="95"/>
<pinref part="DPS2" gate="G$1" pin="JP4A-2"/>
</segment>
<segment>
<wire x1="195.58" y1="76.2" x2="198.12" y2="78.74" width="0.1524" layer="91"/>
<wire x1="198.12" y1="78.74" x2="203.2" y2="78.74" width="0.1524" layer="91"/>
<label x="200.66" y="78.74" size="1.778" layer="95"/>
<pinref part="DPS2" gate="G$1" pin="JP3A-2"/>
</segment>
<segment>
<wire x1="170.18" y1="76.2" x2="172.72" y2="78.74" width="0.1524" layer="91"/>
<wire x1="172.72" y1="78.74" x2="177.8" y2="78.74" width="0.1524" layer="91"/>
<label x="175.26" y="78.74" size="1.778" layer="95"/>
<pinref part="DPS2" gate="G$1" pin="JP2A-2"/>
</segment>
<segment>
<wire x1="144.78" y1="76.2" x2="147.32" y2="78.74" width="0.1524" layer="91"/>
<wire x1="147.32" y1="78.74" x2="152.4" y2="78.74" width="0.1524" layer="91"/>
<label x="149.86" y="78.74" size="1.778" layer="95"/>
<pinref part="DPS2" gate="G$1" pin="JP1A-2"/>
</segment>
</net>
<net name="HEAT" class="0">
<segment>
<wire x1="25.4" y1="162.56" x2="22.86" y2="162.56" width="0.1524" layer="91"/>
<label x="22.86" y="162.56" size="1.27" layer="95" rot="MR0" xref="yes"/>
<pinref part="DPS2" gate="G$1" pin="JP1-6"/>
</segment>
</net>
<net name="COOL" class="0">
<segment>
<wire x1="76.2" y1="147.32" x2="73.66" y2="147.32" width="0.1524" layer="91"/>
<label x="73.66" y="147.32" size="1.27" layer="95" rot="MR0" xref="yes"/>
<pinref part="DPS2" gate="G$1" pin="JP3-3"/>
</segment>
</net>
<net name="!VENTILATION_ON" class="0">
<segment>
<label x="60.96" y="109.22" size="1.27" layer="95" rot="MR0" xref="yes"/>
<wire x1="60.96" y1="109.22" x2="66.04" y2="109.22" width="0.1524" layer="91"/>
<wire x1="66.04" y1="109.22" x2="66.04" y2="137.16" width="0.1524" layer="91"/>
<pinref part="DPS2" gate="G$1" pin="JP3-1"/>
<wire x1="66.04" y1="137.16" x2="76.2" y2="137.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="AKU" class="0">
<segment>
<pinref part="U$2" gate="G$1" pin="AKU"/>
<pinref part="DPS2" gate="G$1" pin="JP3-4"/>
<wire x1="73.66" y1="152.4" x2="76.2" y2="152.4" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+24V" class="0">
<segment>
<pinref part="DPS2" gate="G$1" pin="JP3-5"/>
<wire x1="76.2" y1="157.48" x2="73.66" y2="157.48" width="0.1524" layer="91"/>
<pinref part="U$20" gate="+24V" pin="+24V"/>
<wire x1="73.66" y1="157.48" x2="73.66" y2="160.02" width="0.1524" layer="91"/>
<pinref part="DPS2" gate="G$1" pin="JP3-6"/>
<wire x1="76.2" y1="162.56" x2="73.66" y2="162.56" width="0.1524" layer="91"/>
<wire x1="73.66" y1="162.56" x2="73.66" y2="160.02" width="0.1524" layer="91"/>
<junction x="73.66" y="160.02"/>
</segment>
</net>
<net name="MTEMP" class="0">
<segment>
<pinref part="DPS2" gate="G$1" pin="JP2-2"/>
<wire x1="50.8" y1="142.24" x2="48.26" y2="142.24" width="0.1524" layer="91"/>
<label x="48.26" y="142.24" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="CHKREC" class="0">
<segment>
<pinref part="SVORKA17" gate="G$1" pin="4"/>
<pinref part="TMC1" gate="31" pin="CHKREC"/>
<wire x1="119.38" y1="251.46" x2="99.06" y2="251.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CHKI2" class="0">
<segment>
<pinref part="TMC1" gate="29" pin="CHKI2"/>
<pinref part="SVORKA17" gate="G$1" pin="5"/>
<wire x1="99.06" y1="248.92" x2="116.84" y2="248.92" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CHKI1" class="0">
<segment>
<pinref part="SVORKA17" gate="G$1" pin="6"/>
<pinref part="TMC1" gate="27" pin="CHKI1"/>
<wire x1="114.3" y1="246.38" x2="99.06" y2="246.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="FAILVENT" class="0">
<segment>
<pinref part="TMC1" gate="9" pin="FAILVENT"/>
<pinref part="SVORKA18" gate="G$1" pin="4"/>
<wire x1="99.06" y1="231.14" x2="119.38" y2="231.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CHKHEAT" class="0">
<segment>
<pinref part="SVORKA18" gate="G$1" pin="5"/>
<pinref part="TMC1" gate="25" pin="CHKHEAT"/>
<wire x1="116.84" y1="228.6" x2="99.06" y2="228.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CHKFLOOR" class="0">
<segment>
<pinref part="TMC1" gate="37" pin="CHKFLOOR"/>
<pinref part="SVORKA18" gate="G$1" pin="6"/>
<wire x1="99.06" y1="226.06" x2="114.3" y2="226.06" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CHK4" class="0">
<segment>
<pinref part="DPS2" gate="G$1" pin="JP9-1"/>
<wire x1="254" y1="137.16" x2="238.76" y2="137.16" width="0.1524" layer="91"/>
<pinref part="SVORKA18" gate="G$1" pin="1"/>
<wire x1="238.76" y1="137.16" x2="238.76" y2="226.06" width="0.1524" layer="91"/>
<wire x1="238.76" y1="226.06" x2="134.62" y2="226.06" width="0.1524" layer="91"/>
<label x="137.16" y="226.06" size="1.778" layer="95"/>
<label x="251.46" y="137.16" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="CHK5" class="0">
<segment>
<pinref part="DPS2" gate="G$1" pin="JP2-3"/>
<wire x1="50.8" y1="147.32" x2="35.56" y2="147.32" width="0.1524" layer="91"/>
<wire x1="35.56" y1="147.32" x2="35.56" y2="220.98" width="0.1524" layer="91"/>
<wire x1="35.56" y1="220.98" x2="149.86" y2="220.98" width="0.1524" layer="91"/>
<wire x1="149.86" y1="220.98" x2="149.86" y2="228.6" width="0.1524" layer="91"/>
<pinref part="SVORKA18" gate="G$1" pin="2"/>
<wire x1="149.86" y1="228.6" x2="132.08" y2="228.6" width="0.1524" layer="91"/>
<label x="137.16" y="228.6" size="1.778" layer="95"/>
<label x="48.26" y="147.32" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="CHK3" class="0">
<segment>
<pinref part="DPS2" gate="G$1" pin="JP9-3"/>
<wire x1="254" y1="147.32" x2="241.3" y2="147.32" width="0.1524" layer="91"/>
<pinref part="SVORKA17" gate="G$1" pin="3"/>
<wire x1="241.3" y1="147.32" x2="241.3" y2="251.46" width="0.1524" layer="91"/>
<wire x1="241.3" y1="251.46" x2="129.54" y2="251.46" width="0.1524" layer="91"/>
<label x="251.46" y="147.32" size="1.778" layer="95" rot="MR0"/>
<label x="137.16" y="251.46" size="1.778" layer="95"/>
</segment>
</net>
<net name="CHK2" class="0">
<segment>
<pinref part="DPS2" gate="G$1" pin="JP9-4"/>
<wire x1="254" y1="152.4" x2="243.84" y2="152.4" width="0.1524" layer="91"/>
<pinref part="SVORKA17" gate="G$1" pin="2"/>
<wire x1="243.84" y1="152.4" x2="243.84" y2="248.92" width="0.1524" layer="91"/>
<wire x1="243.84" y1="248.92" x2="132.08" y2="248.92" width="0.1524" layer="91"/>
<label x="251.46" y="152.4" size="1.778" layer="95" rot="MR0"/>
<label x="137.16" y="248.92" size="1.778" layer="95"/>
</segment>
</net>
<net name="CHK1" class="0">
<segment>
<pinref part="DPS2" gate="G$1" pin="JP9-5"/>
<wire x1="254" y1="157.48" x2="246.38" y2="157.48" width="0.1524" layer="91"/>
<pinref part="SVORKA17" gate="G$1" pin="1"/>
<wire x1="246.38" y1="157.48" x2="246.38" y2="246.38" width="0.1524" layer="91"/>
<wire x1="246.38" y1="246.38" x2="134.62" y2="246.38" width="0.1524" layer="91"/>
<label x="251.46" y="157.48" size="1.778" layer="95" rot="MR0"/>
<label x="137.16" y="246.38" size="1.778" layer="95"/>
</segment>
</net>
<net name="FAIL1" class="0">
<segment>
<pinref part="SVORKA18" gate="G$1" pin="3"/>
<wire x1="129.54" y1="231.14" x2="149.86" y2="231.14" width="0.1524" layer="91"/>
<label x="137.16" y="231.14" size="1.778" layer="95"/>
<label x="157.48" y="231.14" size="1.27" layer="95" xref="yes"/>
<label x="157.48" y="236.22" size="1.27" layer="95" xref="yes"/>
<wire x1="149.86" y1="231.14" x2="157.48" y2="231.14" width="0.1524" layer="91"/>
<wire x1="149.86" y1="231.14" x2="149.86" y2="236.22" width="0.1524" layer="91"/>
<junction x="149.86" y="231.14"/>
<wire x1="149.86" y1="236.22" x2="157.48" y2="236.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CON6" class="0">
<segment>
<pinref part="DPS2" gate="G$1" pin="JP2-4"/>
<wire x1="50.8" y1="152.4" x2="48.26" y2="152.4" width="0.1524" layer="91"/>
<label x="48.26" y="152.4" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="CON4" class="0">
<segment>
<pinref part="DPS2" gate="G$1" pin="JP9-6"/>
<wire x1="254" y1="162.56" x2="251.46" y2="162.56" width="0.1524" layer="91"/>
<wire x1="251.46" y1="162.56" x2="251.46" y2="180.34" width="0.1524" layer="91"/>
<wire x1="251.46" y1="180.34" x2="256.54" y2="180.34" width="0.1524" layer="91"/>
<label x="256.54" y="180.34" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="CON5" class="0">
<segment>
<pinref part="DPS2" gate="G$1" pin="JP7-6"/>
<wire x1="203.2" y1="162.56" x2="195.58" y2="162.56" width="0.1524" layer="91"/>
<label x="195.58" y="162.56" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="FAIL3" class="0">
<segment>
<pinref part="DPS2" gate="G$1" pin="JP1-2"/>
<wire x1="22.86" y1="142.24" x2="25.4" y2="142.24" width="0.1524" layer="91"/>
<label x="22.86" y="142.24" size="1.27" layer="95" rot="MR0" xref="yes"/>
</segment>
</net>
<net name="HEATPWR" class="0">
<segment>
<pinref part="DPS2" gate="G$1" pin="JP1-3"/>
<wire x1="25.4" y1="147.32" x2="22.86" y2="147.32" width="0.1524" layer="91"/>
<label x="22.86" y="147.32" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="+10V" class="0">
<segment>
<pinref part="DPS2" gate="G$1" pin="JP2-1"/>
<pinref part="U$33" gate="G$1" pin="+10V"/>
<wire x1="50.8" y1="137.16" x2="43.18" y2="137.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PUMPSTART" class="0">
<segment>
<pinref part="DPS2" gate="G$1" pin="JP2-5"/>
<wire x1="50.8" y1="157.48" x2="48.26" y2="157.48" width="0.1524" layer="91"/>
<label x="48.26" y="157.48" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="HEATSTART" class="0">
<segment>
<pinref part="DPS2" gate="G$1" pin="JP1-4"/>
<wire x1="25.4" y1="152.4" x2="22.86" y2="152.4" width="0.1524" layer="91"/>
<label x="22.86" y="152.4" size="1.27" layer="95" rot="R180" xref="yes"/>
<label x="22.86" y="157.48" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
<segment>
<wire x1="25.4" y1="157.48" x2="22.86" y2="157.48" width="0.1524" layer="91"/>
<pinref part="DPS2" gate="G$1" pin="JP1-5"/>
</segment>
</net>
<net name="FAILHEAT" class="0">
<segment>
<pinref part="DPS2" gate="G$1" pin="JP1-1"/>
<wire x1="25.4" y1="137.16" x2="22.86" y2="137.16" width="0.1524" layer="91"/>
<label x="22.86" y="137.16" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="HEATTEMP" class="0">
<segment>
<pinref part="DPS2" gate="G$1" pin="JP8-6"/>
<wire x1="228.6" y1="162.56" x2="226.06" y2="162.56" width="0.1524" layer="91"/>
<wire x1="226.06" y1="162.56" x2="226.06" y2="203.2" width="0.1524" layer="91"/>
<wire x1="226.06" y1="203.2" x2="190.5" y2="203.2" width="0.1524" layer="91"/>
<label x="190.5" y="203.2" size="1.27" layer="95" rot="MR0" xref="yes"/>
</segment>
</net>
<net name="HEATTEMP_GND" class="0">
<segment>
<pinref part="DPS2" gate="G$1" pin="JP8-5"/>
<wire x1="228.6" y1="157.48" x2="223.52" y2="157.48" width="0.1524" layer="91"/>
<wire x1="223.52" y1="157.48" x2="223.52" y2="198.12" width="0.1524" layer="91"/>
<wire x1="223.52" y1="198.12" x2="190.5" y2="198.12" width="0.1524" layer="91"/>
<label x="190.5" y="198.12" size="1.27" layer="95" rot="MR0" xref="yes"/>
</segment>
</net>
<net name="HEATTEM" class="0">
<segment>
<pinref part="DPS2" gate="G$1" pin="JP7-3"/>
<wire x1="203.2" y1="147.32" x2="198.12" y2="147.32" width="0.1524" layer="91"/>
<label x="198.12" y="147.32" size="1.27" layer="95" rot="MR0" xref="yes"/>
</segment>
</net>
<net name="PUMP" class="0">
<segment>
<pinref part="DPS2" gate="G$1" pin="JP2-6"/>
<wire x1="50.8" y1="162.56" x2="48.26" y2="162.56" width="0.1524" layer="91"/>
<label x="48.26" y="162.56" size="1.27" layer="95" rot="MR0" xref="yes"/>
</segment>
</net>
<net name="LP-" class="0">
<segment>
<wire x1="220.98" y1="134.62" x2="223.52" y2="137.16" width="0.1524" layer="91"/>
<pinref part="DPS2" gate="G$1" pin="JP8-1"/>
<wire x1="223.52" y1="137.16" x2="228.6" y2="137.16" width="0.1524" layer="91"/>
<label x="226.06" y="137.16" size="1.778" layer="95"/>
</segment>
</net>
<net name="LP+" class="0">
<segment>
<wire x1="220.98" y1="139.7" x2="223.52" y2="142.24" width="0.1524" layer="91"/>
<pinref part="DPS2" gate="G$1" pin="JP8-2"/>
<wire x1="223.52" y1="142.24" x2="228.6" y2="142.24" width="0.1524" layer="91"/>
<label x="226.06" y="142.24" size="1.778" layer="95"/>
</segment>
</net>
<net name="HP-" class="0">
<segment>
<wire x1="215.9" y1="144.78" x2="218.44" y2="147.32" width="0.1524" layer="91"/>
<pinref part="DPS2" gate="G$1" pin="JP8-3"/>
<wire x1="218.44" y1="147.32" x2="228.6" y2="147.32" width="0.1524" layer="91"/>
<label x="226.06" y="147.32" size="1.778" layer="95"/>
</segment>
</net>
<net name="HP+" class="0">
<segment>
<pinref part="DPS2" gate="G$1" pin="JP8-4"/>
<wire x1="215.9" y1="149.86" x2="218.44" y2="152.4" width="0.1524" layer="91"/>
<wire x1="218.44" y1="152.4" x2="228.6" y2="152.4" width="0.1524" layer="91"/>
<label x="226.06" y="152.4" size="1.778" layer="95"/>
</segment>
</net>
<net name="HP" class="0">
<segment>
<pinref part="DPS2" gate="G$1" pin="JP7-2"/>
<wire x1="203.2" y1="142.24" x2="193.04" y2="142.24" width="0.1524" layer="91"/>
<label x="200.66" y="142.24" size="1.778" layer="95"/>
<label x="193.04" y="142.24" size="1.27" layer="95" rot="MR0" xref="yes"/>
</segment>
</net>
<net name="LP" class="0">
<segment>
<pinref part="DPS2" gate="G$1" pin="JP7-1"/>
<wire x1="203.2" y1="137.16" x2="193.04" y2="137.16" width="0.1524" layer="91"/>
<label x="200.66" y="137.16" size="1.778" layer="95"/>
<label x="193.04" y="137.16" size="1.27" layer="95" rot="MR0" xref="yes"/>
</segment>
</net>
<net name="ALT" class="0">
<segment>
<pinref part="DPS2" gate="G$1" pin="JP6-4"/>
<wire x1="177.8" y1="152.4" x2="172.72" y2="152.4" width="0.1524" layer="91"/>
<label x="172.72" y="152.4" size="1.27" layer="95" rot="MR0" xref="yes"/>
</segment>
</net>
<net name="D+" class="0">
<segment>
<pinref part="TMC1" gate="7" pin="D+"/>
<pinref part="SVORKA34" gate="G$1" pin="6"/>
<wire x1="99.06" y1="203.2" x2="114.3" y2="203.2" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="DPS2" gate="G$1" pin="JP5-3"/>
<wire x1="152.4" y1="147.32" x2="144.78" y2="147.32" width="0.1524" layer="91"/>
<wire x1="144.78" y1="147.32" x2="144.78" y2="203.2" width="0.1524" layer="91"/>
<pinref part="SVORKA34" gate="G$1" pin="1"/>
<wire x1="144.78" y1="203.2" x2="134.62" y2="203.2" width="0.1524" layer="91"/>
<label x="137.16" y="203.2" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="SVORKA34" gate="G$1" pin="2"/>
<wire x1="132.08" y1="205.74" x2="165.1" y2="205.74" width="0.1524" layer="91"/>
<label x="137.16" y="205.74" size="1.778" layer="95"/>
</segment>
</net>
<net name="AKUAUTO" class="0">
<segment>
<pinref part="SVORKA34" gate="G$1" pin="3"/>
<wire x1="129.54" y1="208.28" x2="165.1" y2="208.28" width="0.1524" layer="91"/>
<label x="137.16" y="208.28" size="1.778" layer="95"/>
</segment>
</net>
<net name="PWR4" class="0">
<segment>
<pinref part="DPS2" gate="G$1" pin="JP4-4"/>
<wire x1="127" y1="152.4" x2="121.92" y2="152.4" width="0.1524" layer="91"/>
<label x="121.92" y="152.4" size="1.27" layer="95" rot="MR0" xref="yes"/>
</segment>
</net>
<net name="VENTSTART" class="0">
<segment>
<pinref part="DPS2" gate="G$1" pin="JP3-2"/>
<wire x1="76.2" y1="142.24" x2="63.5" y2="142.24" width="0.1524" layer="91"/>
<wire x1="63.5" y1="142.24" x2="63.5" y2="114.3" width="0.1524" layer="91"/>
<wire x1="63.5" y1="114.3" x2="60.96" y2="114.3" width="0.1524" layer="91"/>
<label x="60.96" y="114.3" size="1.27" layer="95" rot="MR0" xref="yes"/>
</segment>
</net>
<net name="POWERTMC" class="0">
<segment>
<pinref part="DPS2" gate="G$1" pin="JP4-6"/>
<wire x1="127" y1="162.56" x2="121.92" y2="162.56" width="0.1524" layer="91"/>
<label x="121.92" y="162.56" size="1.27" layer="95" rot="MR0" xref="yes"/>
</segment>
</net>
<net name="VENTCOOL0" class="0">
<segment>
<pinref part="DPS2" gate="G$1" pin="JP4-5"/>
<wire x1="127" y1="157.48" x2="121.92" y2="157.48" width="0.1524" layer="91"/>
<label x="121.92" y="157.48" size="1.27" layer="95" rot="MR0" xref="yes"/>
</segment>
</net>
<net name="COMPRESSOR" class="0">
<segment>
<pinref part="DPS2" gate="G$1" pin="JP4-2"/>
<wire x1="127" y1="142.24" x2="121.92" y2="142.24" width="0.1524" layer="91"/>
<label x="121.92" y="142.24" size="1.27" layer="95" rot="MR0" xref="yes"/>
</segment>
</net>
<net name="COOLSTART" class="0">
<segment>
<pinref part="DPS2" gate="G$1" pin="JP4-3"/>
<wire x1="127" y1="147.32" x2="121.92" y2="147.32" width="0.1524" layer="91"/>
<label x="121.92" y="147.32" size="1.27" layer="95" rot="MR0" xref="yes"/>
</segment>
</net>
<net name="HL/SWITCH2" class="0">
<segment>
<pinref part="DPS2" gate="G$1" pin="JP4-1"/>
<wire x1="127" y1="137.16" x2="121.92" y2="137.16" width="0.1524" layer="91"/>
<label x="121.92" y="137.16" size="1.27" layer="95" rot="MR0" xref="yes"/>
</segment>
</net>
<net name="FAN_50%" class="0">
<segment>
<pinref part="DPS2" gate="G$1" pin="JP10-3"/>
<wire x1="279.4" y1="157.48" x2="276.86" y2="157.48" width="0.1524" layer="91"/>
<label x="276.86" y="157.48" size="1.27" layer="95" rot="MR0" xref="yes"/>
</segment>
</net>
<net name="FAN_100%" class="0">
<segment>
<pinref part="DPS2" gate="G$1" pin="JP10-2"/>
<wire x1="279.4" y1="152.4" x2="276.86" y2="152.4" width="0.1524" layer="91"/>
<label x="276.86" y="152.4" size="1.27" layer="95" rot="MR0" xref="yes"/>
</segment>
</net>
<net name="CLAMP+" class="0">
<segment>
<pinref part="DPS2" gate="G$1" pin="JP5-5"/>
<wire x1="152.4" y1="157.48" x2="147.32" y2="157.48" width="0.1524" layer="91"/>
<wire x1="147.32" y1="157.48" x2="147.32" y2="182.88" width="0.1524" layer="91"/>
<wire x1="147.32" y1="182.88" x2="154.94" y2="182.88" width="0.1524" layer="91"/>
<label x="154.94" y="182.88" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PSOK" class="0">
<segment>
<pinref part="DPS2" gate="G$1" pin="JP7-4"/>
<wire x1="203.2" y1="152.4" x2="195.58" y2="152.4" width="0.1524" layer="91"/>
<label x="195.58" y="152.4" size="1.27" layer="95" rot="MR0" xref="yes"/>
</segment>
</net>
<net name="CLAMP-" class="0">
<segment>
<pinref part="DPS2" gate="G$1" pin="JP5-6"/>
<wire x1="152.4" y1="162.56" x2="149.86" y2="162.56" width="0.1524" layer="91"/>
<wire x1="149.86" y1="162.56" x2="149.86" y2="177.8" width="0.1524" layer="91"/>
<wire x1="149.86" y1="177.8" x2="154.94" y2="177.8" width="0.1524" layer="91"/>
<label x="154.94" y="177.8" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<description>Naftové topení</description>
<plain>
<text x="182.88" y="200.66" size="1.778" layer="94" rot="MR180">Jistič topení</text>
<text x="139.7" y="33.02" size="6.4516" layer="94" align="center">TOPENÍ</text>
<wire x1="177.8" y1="193.04" x2="180.34" y2="195.58" width="0.1524" layer="94"/>
<wire x1="180.34" y1="195.58" x2="180.34" y2="193.04" width="0.1524" layer="94"/>
<wire x1="116.84" y1="43.18" x2="116.84" y2="25.4" width="0.4064" layer="94"/>
<wire x1="116.84" y1="25.4" x2="162.56" y2="25.4" width="0.4064" layer="94"/>
<wire x1="162.56" y1="25.4" x2="162.56" y2="43.18" width="0.4064" layer="94"/>
<wire x1="162.56" y1="43.18" x2="116.84" y2="43.18" width="0.4064" layer="94"/>
<text x="256.54" y="96.52" size="1.778" layer="96">20A</text>
<text x="116.84" y="215.9" size="1.778" layer="92">HEATPUMP?</text>
<text x="116.84" y="213.36" size="1.778" layer="92">HEATFUEL?</text>
<wire x1="93.98" y1="58.42" x2="289.56" y2="58.42" width="0.1524" layer="94" style="longdash"/>
<wire x1="289.56" y1="58.42" x2="289.56" y2="190.5" width="0.1524" layer="94" style="longdash"/>
<text x="259.08" y="60.96" size="1.778" layer="94" rot="R180">Rozvaděč</text>
<text x="259.08" y="55.88" size="1.778" layer="94" rot="R180" align="top-left">Strojovna</text>
</plain>
<instances>
<instance part="FRAME8" gate="G$1" x="0" y="0"/>
<instance part="FRAME8" gate="G$2" x="287.02" y="0"/>
<instance part="U$38" gate="G$1" x="180.34" y="200.66" smashed="yes" rot="R90">
<attribute name="VALUE" x="175.26" y="200.66" size="1.778" layer="94" rot="R180" align="bottom-center"/>
</instance>
<instance part="U$39" gate="+24V" x="180.34" y="210.82"/>
<instance part="U$40" gate="G$1" x="180.34" y="195.58"/>
<instance part="SVORKA24" gate="G$1" x="180.34" y="91.44" rot="R270"/>
<instance part="PRUCHODKA12" gate="G$1" x="139.7" y="58.42" rot="R270"/>
<instance part="U$41" gate="GND" x="147.32" y="68.58"/>
<instance part="SVORKA27" gate="G$1" x="180.34" y="152.4" rot="R270"/>
<instance part="RE21" gate="G$1" x="254" y="109.22"/>
<instance part="U$45" gate="GND" x="254" y="88.9"/>
<instance part="U$37" gate="+24V" x="248.92" y="124.46"/>
<instance part="SVORKA21" gate="G$1" x="180.34" y="121.92" rot="R270"/>
<instance part="SVORKA22" gate="G$1" x="180.34" y="231.14" rot="R270"/>
<instance part="TMC1" gate="5" x="106.68" y="223.52"/>
<instance part="TMC1" gate="11" x="106.68" y="228.6"/>
<instance part="TMC1" gate="41" x="106.68" y="233.68"/>
<instance part="TMC1" gate="40" x="106.68" y="231.14"/>
<instance part="V31" gate="G$1" x="314.96" y="139.7" rot="R180"/>
<instance part="V58" gate="G$1" x="142.24" y="139.7"/>
<instance part="PRUCHODKA11" gate="G$1" x="289.56" y="139.7"/>
</instances>
<busses>
<bus name="HEATPWR,GND,HEATSTART,FAILHEAT,PUMPSTART,FLAME1,FUELHEAT,FILTERTERM1,FILTERTERM2,HEATTEMP,HEATTEMP_GND">
<segment>
<wire x1="139.7" y1="190.5" x2="139.7" y2="152.4" width="0.762" layer="92"/>
<wire x1="139.7" y1="152.4" x2="139.7" y2="43.18" width="0.762" layer="92"/>
<wire x1="139.7" y1="152.4" x2="137.16" y2="154.94" width="0.762" layer="92"/>
<wire x1="137.16" y1="154.94" x2="127" y2="154.94" width="0.762" layer="92"/>
<wire x1="127" y1="154.94" x2="124.46" y2="157.48" width="0.762" layer="92"/>
<wire x1="124.46" y1="157.48" x2="124.46" y2="170.18" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="TR+,TR-">
<segment>
<wire x1="299.72" y1="139.7" x2="203.2" y2="139.7" width="0.762" layer="92"/>
</segment>
<segment>
<wire x1="165.1" y1="139.7" x2="157.48" y2="139.7" width="0.762" layer="92"/>
</segment>
</bus>
</busses>
<nets>
<net name="+24V" class="0">
<segment>
<pinref part="U$39" gate="+24V" pin="+24V"/>
<wire x1="180.34" y1="205.74" x2="180.34" y2="208.28" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="248.92" y1="119.38" x2="248.92" y2="121.92" width="0.1524" layer="91"/>
<pinref part="RE21" gate="G$1" pin="1"/>
<pinref part="U$37" gate="+24V" pin="+24V"/>
</segment>
</net>
<net name="HEATPWR" class="0">
<segment>
<label x="187.96" y="187.96" size="1.27" layer="95" xref="yes"/>
<wire x1="180.34" y1="193.04" x2="180.34" y2="187.96" width="0.1524" layer="91"/>
<wire x1="180.34" y1="187.96" x2="187.96" y2="187.96" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="139.7" y1="190.5" x2="142.24" y2="193.04" width="0.1524" layer="91"/>
<wire x1="142.24" y1="193.04" x2="177.8" y2="193.04" width="0.1524" layer="91"/>
<label x="144.78" y="193.04" size="1.778" layer="95"/>
</segment>
</net>
<net name="PUMPSTART" class="0">
<segment>
<wire x1="139.7" y1="88.9" x2="142.24" y2="91.44" width="0.1524" layer="91"/>
<pinref part="SVORKA24" gate="G$1" pin="5"/>
<wire x1="172.72" y1="91.44" x2="142.24" y2="91.44" width="0.1524" layer="91"/>
<label x="170.18" y="91.44" size="1.778" layer="95" rot="MR0"/>
</segment>
<segment>
<pinref part="SVORKA24" gate="G$1" pin="2"/>
<wire x1="187.96" y1="91.44" x2="210.82" y2="91.44" width="0.1524" layer="91"/>
<label x="210.82" y="91.44" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="HEATSTART" class="0">
<segment>
<wire x1="190.5" y1="149.86" x2="210.82" y2="149.86" width="0.1524" layer="91"/>
<label x="210.82" y="149.86" size="1.27" layer="95" xref="yes"/>
<pinref part="SVORKA27" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="187.96" y1="152.4" x2="210.82" y2="152.4" width="0.1524" layer="91"/>
<pinref part="SVORKA27" gate="G$1" pin="2"/>
<label x="210.82" y="152.4" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="139.7" y1="149.86" x2="142.24" y2="152.4" width="0.1524" layer="91"/>
<pinref part="SVORKA27" gate="G$1" pin="5"/>
<wire x1="142.24" y1="152.4" x2="172.72" y2="152.4" width="0.1524" layer="91"/>
<label x="165.1" y="152.4" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="FAILHEAT" class="0">
<segment>
<label x="210.82" y="88.9" size="1.27" layer="95" xref="yes"/>
<pinref part="SVORKA24" gate="G$1" pin="1"/>
<wire x1="210.82" y1="88.9" x2="190.5" y2="88.9" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="139.7" y1="86.36" x2="142.24" y2="88.9" width="0.1524" layer="91"/>
<pinref part="SVORKA24" gate="G$1" pin="6"/>
<wire x1="170.18" y1="88.9" x2="142.24" y2="88.9" width="0.1524" layer="91"/>
<label x="170.18" y="88.9" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="FILTERTERM1" class="0">
<segment>
<wire x1="139.7" y1="147.32" x2="142.24" y2="149.86" width="0.1524" layer="91"/>
<pinref part="SVORKA27" gate="G$1" pin="6"/>
<label x="165.1" y="149.86" size="1.778" layer="95" rot="MR0"/>
<wire x1="142.24" y1="149.86" x2="170.18" y2="149.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<wire x1="139.7" y1="73.66" x2="142.24" y2="76.2" width="0.1524" layer="91"/>
<wire x1="142.24" y1="76.2" x2="147.32" y2="76.2" width="0.1524" layer="91"/>
<pinref part="U$41" gate="GND" pin="GND"/>
<wire x1="147.32" y1="76.2" x2="147.32" y2="73.66" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="254" y1="93.98" x2="254" y2="96.52" width="0.1524" layer="91"/>
<pinref part="U$45" gate="GND" pin="GND"/>
<pinref part="RE21" gate="G$1" pin="3"/>
</segment>
</net>
<net name="FTERM2" class="0">
<segment>
<pinref part="RE21" gate="G$1" pin="IN"/>
<wire x1="264.16" y1="111.76" x2="279.4" y2="111.76" width="0.1524" layer="91"/>
<wire x1="279.4" y1="111.76" x2="279.4" y2="134.62" width="0.1524" layer="91"/>
<pinref part="SVORKA21" gate="G$1" pin="6"/>
<wire x1="279.4" y1="134.62" x2="152.4" y2="134.62" width="0.1524" layer="91"/>
<wire x1="152.4" y1="134.62" x2="152.4" y2="119.38" width="0.1524" layer="91"/>
<wire x1="152.4" y1="119.38" x2="170.18" y2="119.38" width="0.1524" layer="91"/>
<label x="167.64" y="119.38" size="1.778" layer="95" rot="MR0"/>
<label x="266.7" y="111.76" size="1.778" layer="95"/>
</segment>
</net>
<net name="FUELHEAT" class="0">
<segment>
<pinref part="RE21" gate="G$1" pin="2"/>
<wire x1="248.92" y1="93.98" x2="248.92" y2="96.52" width="0.1524" layer="91"/>
<label x="246.38" y="93.98" size="1.778" layer="95" rot="MR0"/>
<pinref part="SVORKA24" gate="G$1" pin="3"/>
<wire x1="248.92" y1="93.98" x2="185.42" y2="93.98" width="0.1524" layer="91"/>
<label x="190.5" y="93.98" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="SVORKA24" gate="G$1" pin="4"/>
<wire x1="175.26" y1="93.98" x2="142.24" y2="93.98" width="0.1524" layer="91"/>
<label x="170.18" y="93.98" size="1.778" layer="95" rot="MR0"/>
<wire x1="142.24" y1="93.98" x2="139.7" y2="91.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="FILTERTERM2" class="0">
<segment>
<pinref part="SVORKA21" gate="G$1" pin="1"/>
<wire x1="190.5" y1="119.38" x2="210.82" y2="119.38" width="0.1524" layer="91"/>
<wire x1="210.82" y1="119.38" x2="210.82" y2="109.22" width="0.1524" layer="91"/>
<wire x1="210.82" y1="109.22" x2="142.24" y2="109.22" width="0.1524" layer="91"/>
<label x="193.04" y="119.38" size="1.778" layer="95"/>
<wire x1="139.7" y1="106.68" x2="142.24" y2="109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="HEATTEMP" class="0">
<segment>
<wire x1="121.92" y1="172.72" x2="109.22" y2="172.72" width="0.1524" layer="91"/>
<label x="109.22" y="172.72" size="1.27" layer="95" rot="MR0" xref="yes"/>
<wire x1="121.92" y1="172.72" x2="124.46" y2="170.18" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="TMC1" gate="41" pin="HEATTEMP"/>
<pinref part="SVORKA22" gate="G$1" pin="4"/>
<wire x1="139.7" y1="233.68" x2="175.26" y2="233.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="HEATTEMP_GND" class="0">
<segment>
<wire x1="121.92" y1="170.18" x2="109.22" y2="170.18" width="0.1524" layer="91"/>
<label x="109.22" y="170.18" size="1.27" layer="95" rot="MR0" xref="yes"/>
<wire x1="121.92" y1="170.18" x2="124.46" y2="167.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="HEATTEM" class="0">
<segment>
<wire x1="185.42" y1="233.68" x2="210.82" y2="233.68" width="0.1524" layer="91"/>
<label x="210.82" y="233.68" size="1.27" layer="95" rot="MR180" xref="yes"/>
<pinref part="SVORKA22" gate="G$1" pin="3"/>
</segment>
</net>
<net name="PUMP" class="0">
<segment>
<pinref part="SVORKA22" gate="G$1" pin="5"/>
<pinref part="TMC1" gate="40" pin="PUMP"/>
<wire x1="172.72" y1="231.14" x2="139.7" y2="231.14" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SVORKA22" gate="G$1" pin="2"/>
<wire x1="187.96" y1="231.14" x2="210.82" y2="231.14" width="0.1524" layer="91"/>
<label x="210.82" y="231.14" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="FLAME1" class="0">
<segment>
<pinref part="TMC1" gate="11" pin="FLAME1"/>
<pinref part="SVORKA22" gate="G$1" pin="6"/>
<wire x1="139.7" y1="228.6" x2="170.18" y2="228.6" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="139.7" y1="170.18" x2="142.24" y2="172.72" width="0.1524" layer="91"/>
<wire x1="142.24" y1="172.72" x2="210.82" y2="172.72" width="0.1524" layer="91"/>
<wire x1="210.82" y1="172.72" x2="210.82" y2="228.6" width="0.1524" layer="91"/>
<pinref part="SVORKA22" gate="G$1" pin="1"/>
<wire x1="210.82" y1="228.6" x2="190.5" y2="228.6" width="0.1524" layer="91"/>
<label x="144.78" y="172.72" size="1.778" layer="95"/>
<label x="193.04" y="228.6" size="1.778" layer="95"/>
</segment>
</net>
<net name="TR-" class="0">
<segment>
<pinref part="SVORKA21" gate="G$1" pin="3"/>
<wire x1="185.42" y1="124.46" x2="200.66" y2="124.46" width="0.1524" layer="91"/>
<wire x1="200.66" y1="124.46" x2="200.66" y2="137.16" width="0.1524" layer="91"/>
<label x="193.04" y="124.46" size="1.778" layer="95"/>
<wire x1="200.66" y1="137.16" x2="203.2" y2="139.7" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SVORKA21" gate="G$1" pin="4"/>
<wire x1="175.26" y1="124.46" x2="167.64" y2="124.46" width="0.1524" layer="91"/>
<wire x1="167.64" y1="124.46" x2="167.64" y2="137.16" width="0.1524" layer="91"/>
<label x="170.18" y="124.46" size="1.778" layer="95"/>
<wire x1="167.64" y1="137.16" x2="165.1" y2="139.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="TR+" class="0">
<segment>
<pinref part="SVORKA27" gate="G$1" pin="4"/>
<wire x1="175.26" y1="154.94" x2="167.64" y2="154.94" width="0.1524" layer="91"/>
<wire x1="167.64" y1="154.94" x2="167.64" y2="142.24" width="0.1524" layer="91"/>
<label x="170.18" y="154.94" size="1.778" layer="95"/>
<wire x1="167.64" y1="142.24" x2="165.1" y2="139.7" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SVORKA27" gate="G$1" pin="3"/>
<wire x1="185.42" y1="154.94" x2="200.66" y2="154.94" width="0.1524" layer="91"/>
<wire x1="200.66" y1="154.94" x2="200.66" y2="142.24" width="0.1524" layer="91"/>
<label x="193.04" y="154.94" size="1.778" layer="95"/>
<wire x1="200.66" y1="142.24" x2="203.2" y2="139.7" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<description>Senzory</description>
<plain>
<text x="170.18" y="215.9" size="1.778" layer="91" rot="MR0">dát sem i druhou zem TMC? ---&gt;</text>
<text x="190.5" y="101.6" size="1.778" layer="94">rezerva GPS</text>
<text x="190.5" y="91.44" size="1.778" layer="94">rezerva GPS</text>
<wire x1="236.22" y1="264.16" x2="236.22" y2="0" width="0.1524" layer="94" style="longdash"/>
<wire x1="81.28" y1="246.38" x2="124.46" y2="246.38" width="0.1524" layer="94" style="shortdash"/>
<wire x1="124.46" y1="246.38" x2="124.46" y2="99.06" width="0.1524" layer="94" style="shortdash"/>
<wire x1="124.46" y1="99.06" x2="81.28" y2="99.06" width="0.1524" layer="94" style="shortdash"/>
<wire x1="81.28" y1="99.06" x2="81.28" y2="246.38" width="0.1524" layer="94" style="shortdash"/>
<text x="104.14" y="193.04" size="5.08" layer="94" align="center">TMC</text>
<text x="233.68" y="27.94" size="1.778" layer="94" rot="R270">Rozvaděč</text>
<text x="238.76" y="27.94" size="1.778" layer="94" rot="R270" align="top-left">Strojovna</text>
</plain>
<instances>
<instance part="TMC1" gate="44" x="86.36" y="226.06"/>
<instance part="TMC1" gate="46" x="86.36" y="223.52"/>
<instance part="TMC1" gate="48" x="86.36" y="220.98"/>
<instance part="TMC1" gate="50" x="86.36" y="236.22"/>
<instance part="TMC1" gate="52" x="86.36" y="233.68"/>
<instance part="TMC1" gate="54" x="86.36" y="231.14"/>
<instance part="TMC1" gate="57" x="86.36" y="215.9"/>
<instance part="V16" gate="G$1" x="271.78" y="243.84" rot="MR180"/>
<instance part="V17" gate="G$1" x="271.78" y="248.92" rot="MR180"/>
<instance part="V18" gate="G$1" x="271.78" y="254" rot="MR180"/>
<instance part="V19" gate="G$1" x="271.78" y="226.06" rot="MR180"/>
<instance part="V20" gate="G$1" x="271.78" y="231.14" rot="MR180"/>
<instance part="V1" gate="G$1" x="271.78" y="236.22" rot="MR180"/>
<instance part="SVORKA25" gate="G$1" x="177.8" y="233.68" rot="R270"/>
<instance part="SVORKA26" gate="G$1" x="177.8" y="223.52" rot="R270"/>
<instance part="U$6" gate="GND" x="162.56" y="205.74"/>
<instance part="FRAME6" gate="G$1" x="0" y="0"/>
<instance part="FRAME6" gate="G$2" x="287.02" y="0"/>
<instance part="PRUCHODKA2" gate="G$1" x="236.22" y="254"/>
<instance part="PRUCHODKA4" gate="G$1" x="236.22" y="248.92"/>
<instance part="PRUCHODKA16" gate="G$1" x="236.22" y="243.84"/>
<instance part="PRUCHODKA17" gate="G$1" x="236.22" y="236.22"/>
<instance part="PRUCHODKA18" gate="G$1" x="236.22" y="231.14"/>
<instance part="PRUCHODKA19" gate="G$1" x="236.22" y="226.06"/>
<instance part="SVORKA28" gate="G$1" x="177.8" y="121.92" rot="R270"/>
<instance part="TMC1" gate="58" x="86.36" y="213.36"/>
<instance part="TMC1" gate="59" x="86.36" y="127"/>
<instance part="TMC1" gate="60" x="86.36" y="124.46"/>
<instance part="SENSOR1" gate="G$1" x="264.16" y="175.26"/>
<instance part="SENSOR2" gate="G$1" x="309.88" y="165.1"/>
<instance part="SENSOR3" gate="G$1" x="287.02" y="170.18"/>
<instance part="SVORKA29" gate="G$1" x="177.8" y="165.1" rot="R270"/>
<instance part="SVORKA30" gate="G$1" x="177.8" y="175.26" rot="R270"/>
<instance part="TMC1" gate="21" x="86.36" y="162.56"/>
<instance part="TMC1" gate="43" x="86.36" y="165.1"/>
<instance part="TMC1" gate="45" x="86.36" y="167.64"/>
<instance part="U$7" gate="GND" x="162.56" y="147.32"/>
<instance part="PRUCHODKA20" gate="G$1" x="236.22" y="170.18"/>
<instance part="PRUCHODKA21" gate="G$1" x="236.22" y="165.1"/>
<instance part="PRUCHODKA22" gate="G$1" x="236.22" y="160.02"/>
<instance part="SVORKA31" gate="G$1" x="177.8" y="111.76" rot="R270"/>
<instance part="SVORKA32" gate="G$1" x="177.8" y="101.6" rot="R270"/>
<instance part="SVORKA33" gate="G$1" x="177.8" y="91.44" rot="R270"/>
<instance part="V48" gate="G$1" x="271.78" y="119.38" smashed="yes" rot="R180">
<attribute name="VALUE" x="262.89" y="120.142" size="1.4224" layer="96" rot="MR180"/>
</instance>
<instance part="TMC1" gate="51" x="86.36" y="109.22"/>
<instance part="TMC1" gate="53" x="86.36" y="111.76"/>
<instance part="U$13" gate="GND" x="162.56" y="73.66"/>
<instance part="PRUCHODKA23" gate="G$1" x="236.22" y="119.38"/>
<instance part="V2" gate="G$1" x="271.78" y="76.2" rot="MR180"/>
<instance part="PRUCHODKA24" gate="G$1" x="236.22" y="76.2"/>
<instance part="SVORKA1" gate="G$1" x="177.8" y="66.04" rot="R270"/>
<instance part="SVORKA9" gate="G$1" x="177.8" y="154.94" rot="R270"/>
<instance part="SVORKA10" gate="G$1" x="177.8" y="213.36" rot="R270"/>
<instance part="SVORKA11" gate="G$1" x="177.8" y="81.28" rot="R270"/>
<instance part="V55" gate="G$1" x="271.78" y="55.88" rot="R180"/>
<instance part="PRUCHODKA41" gate="G$1" x="236.22" y="55.88"/>
</instances>
<busses>
<bus name="T3,GNDT36">
<segment>
<wire x1="256.54" y1="254" x2="218.44" y2="254" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="T2,GNDT25">
<segment>
<wire x1="256.54" y1="248.92" x2="220.98" y2="248.92" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="T1,GNDT14">
<segment>
<wire x1="256.54" y1="243.84" x2="223.52" y2="243.84" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="T6,GNDT36">
<segment>
<wire x1="256.54" y1="236.22" x2="226.06" y2="236.22" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="T5,GNDT25">
<segment>
<wire x1="256.54" y1="231.14" x2="228.6" y2="231.14" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="T4,GNDT14">
<segment>
<wire x1="256.54" y1="226.06" x2="231.14" y2="226.06" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="HUM_VS,HUM,GNDHUM">
<segment>
<wire x1="254" y1="170.18" x2="215.9" y2="170.18" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="CO2_VS,CO2,GNDCO2">
<segment>
<wire x1="276.86" y1="165.1" x2="223.52" y2="165.1" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="PF_VS,PF,GNDPF">
<segment>
<wire x1="299.72" y1="160.02" x2="231.14" y2="160.02" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="GNDGPS,PWRGPS,TXGPS,RX">
<segment>
<wire x1="256.54" y1="119.38" x2="231.14" y2="119.38" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="GNDHER,PWRHER,TH+HER,TH-HER">
<segment>
<wire x1="256.54" y1="76.2" x2="231.14" y2="76.2" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="TH+,TH-">
<segment>
<wire x1="256.54" y1="55.88" x2="147.32" y2="55.88" width="0.762" layer="92"/>
<wire x1="147.32" y1="55.88" x2="144.78" y2="58.42" width="0.762" layer="92"/>
<wire x1="144.78" y1="58.42" x2="144.78" y2="66.04" width="0.762" layer="92"/>
</segment>
</bus>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<pinref part="U$6" gate="GND" pin="GND"/>
<wire x1="162.56" y1="210.82" x2="167.64" y2="210.82" width="0.1524" layer="91"/>
<pinref part="SVORKA10" gate="G$1" pin="6"/>
</segment>
<segment>
<pinref part="TMC1" gate="58" pin="GND"/>
<wire x1="170.18" y1="213.36" x2="119.38" y2="213.36" width="0.1524" layer="91"/>
<pinref part="SVORKA10" gate="G$1" pin="5"/>
</segment>
<segment>
<wire x1="162.56" y1="152.4" x2="167.64" y2="152.4" width="0.1524" layer="91"/>
<pinref part="U$7" gate="GND" pin="GND"/>
<pinref part="SVORKA9" gate="G$1" pin="6"/>
</segment>
<segment>
<pinref part="U$13" gate="GND" pin="GND"/>
<wire x1="167.64" y1="78.74" x2="162.56" y2="78.74" width="0.1524" layer="91"/>
<pinref part="SVORKA11" gate="G$1" pin="6"/>
</segment>
</net>
<net name="T3" class="0">
<segment>
<pinref part="SVORKA25" gate="G$1" pin="3"/>
<wire x1="182.88" y1="236.22" x2="195.58" y2="236.22" width="0.1524" layer="91"/>
<label x="190.5" y="236.22" size="1.778" layer="95"/>
<wire x1="195.58" y1="236.22" x2="195.58" y2="254" width="0.1524" layer="91"/>
<wire x1="195.58" y1="254" x2="218.44" y2="254" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SVORKA25" gate="G$1" pin="4"/>
<pinref part="TMC1" gate="50" pin="T3"/>
<wire x1="172.72" y1="236.22" x2="119.38" y2="236.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="T2" class="0">
<segment>
<pinref part="SVORKA25" gate="G$1" pin="2"/>
<wire x1="185.42" y1="233.68" x2="198.12" y2="233.68" width="0.1524" layer="91"/>
<label x="190.5" y="233.68" size="1.778" layer="95"/>
<wire x1="198.12" y1="233.68" x2="198.12" y2="248.92" width="0.1524" layer="91"/>
<wire x1="198.12" y1="248.92" x2="220.98" y2="248.92" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SVORKA25" gate="G$1" pin="5"/>
<pinref part="TMC1" gate="52" pin="T2"/>
<wire x1="170.18" y1="233.68" x2="119.38" y2="233.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="T1" class="0">
<segment>
<pinref part="SVORKA25" gate="G$1" pin="1"/>
<wire x1="187.96" y1="231.14" x2="200.66" y2="231.14" width="0.1524" layer="91"/>
<label x="190.5" y="231.14" size="1.778" layer="95"/>
<wire x1="200.66" y1="231.14" x2="200.66" y2="243.84" width="0.1524" layer="91"/>
<wire x1="200.66" y1="243.84" x2="223.52" y2="243.84" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SVORKA25" gate="G$1" pin="6"/>
<pinref part="TMC1" gate="54" pin="T1"/>
<wire x1="167.64" y1="231.14" x2="119.38" y2="231.14" width="0.1524" layer="91"/>
</segment>
</net>
<net name="T6" class="0">
<segment>
<pinref part="SVORKA26" gate="G$1" pin="3"/>
<wire x1="182.88" y1="226.06" x2="205.74" y2="226.06" width="0.1524" layer="91"/>
<label x="190.5" y="226.06" size="1.778" layer="95"/>
<wire x1="205.74" y1="226.06" x2="205.74" y2="236.22" width="0.1524" layer="91"/>
<wire x1="205.74" y1="236.22" x2="226.06" y2="236.22" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SVORKA26" gate="G$1" pin="4"/>
<pinref part="TMC1" gate="44" pin="T6"/>
<wire x1="172.72" y1="226.06" x2="119.38" y2="226.06" width="0.1524" layer="91"/>
</segment>
</net>
<net name="T4" class="0">
<segment>
<pinref part="SVORKA26" gate="G$1" pin="1"/>
<wire x1="187.96" y1="220.98" x2="210.82" y2="220.98" width="0.1524" layer="91"/>
<wire x1="210.82" y1="220.98" x2="210.82" y2="226.06" width="0.1524" layer="91"/>
<wire x1="210.82" y1="226.06" x2="231.14" y2="226.06" width="0.1524" layer="91"/>
<label x="190.5" y="220.98" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="TMC1" gate="48" pin="T4"/>
<pinref part="SVORKA26" gate="G$1" pin="6"/>
<wire x1="119.38" y1="220.98" x2="167.64" y2="220.98" width="0.1524" layer="91"/>
</segment>
</net>
<net name="T5" class="0">
<segment>
<pinref part="SVORKA26" gate="G$1" pin="2"/>
<wire x1="185.42" y1="223.52" x2="208.28" y2="223.52" width="0.1524" layer="91"/>
<label x="190.5" y="223.52" size="1.778" layer="95"/>
<wire x1="208.28" y1="223.52" x2="208.28" y2="231.14" width="0.1524" layer="91"/>
<wire x1="228.6" y1="231.14" x2="208.28" y2="231.14" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SVORKA26" gate="G$1" pin="5"/>
<pinref part="TMC1" gate="46" pin="T5"/>
<wire x1="170.18" y1="223.52" x2="119.38" y2="223.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GNDT14" class="0">
<segment>
<wire x1="231.14" y1="226.06" x2="228.6" y2="223.52" width="0.1524" layer="91"/>
<wire x1="223.52" y1="243.84" x2="220.98" y2="241.3" width="0.1524" layer="91"/>
<wire x1="187.96" y1="210.82" x2="220.98" y2="210.82" width="0.1524" layer="91"/>
<label x="190.5" y="210.82" size="1.778" layer="95"/>
<wire x1="220.98" y1="241.3" x2="220.98" y2="223.52" width="0.1524" layer="91"/>
<wire x1="220.98" y1="223.52" x2="220.98" y2="210.82" width="0.1524" layer="91"/>
<wire x1="228.6" y1="223.52" x2="220.98" y2="223.52" width="0.1524" layer="91"/>
<junction x="220.98" y="223.52"/>
<pinref part="SVORKA10" gate="G$1" pin="1"/>
</segment>
</net>
<net name="GNDT25" class="0">
<segment>
<wire x1="220.98" y1="248.92" x2="218.44" y2="246.38" width="0.1524" layer="91"/>
<wire x1="185.42" y1="213.36" x2="218.44" y2="213.36" width="0.1524" layer="91"/>
<label x="190.5" y="213.36" size="1.778" layer="95"/>
<wire x1="226.06" y1="228.6" x2="228.6" y2="231.14" width="0.1524" layer="91"/>
<wire x1="218.44" y1="246.38" x2="218.44" y2="228.6" width="0.1524" layer="91"/>
<wire x1="218.44" y1="228.6" x2="218.44" y2="213.36" width="0.1524" layer="91"/>
<wire x1="218.44" y1="228.6" x2="226.06" y2="228.6" width="0.1524" layer="91"/>
<junction x="218.44" y="228.6"/>
<pinref part="SVORKA10" gate="G$1" pin="2"/>
</segment>
</net>
<net name="GNDT36" class="0">
<segment>
<wire x1="218.44" y1="254" x2="215.9" y2="251.46" width="0.1524" layer="91"/>
<wire x1="182.88" y1="215.9" x2="215.9" y2="215.9" width="0.1524" layer="91"/>
<label x="190.5" y="215.9" size="1.778" layer="95"/>
<wire x1="223.52" y1="233.68" x2="226.06" y2="236.22" width="0.1524" layer="91"/>
<wire x1="215.9" y1="251.46" x2="215.9" y2="233.68" width="0.1524" layer="91"/>
<wire x1="215.9" y1="233.68" x2="215.9" y2="215.9" width="0.1524" layer="91"/>
<wire x1="215.9" y1="233.68" x2="223.52" y2="233.68" width="0.1524" layer="91"/>
<junction x="215.9" y="233.68"/>
<pinref part="SVORKA10" gate="G$1" pin="3"/>
</segment>
</net>
<net name="CO2_VS" class="0">
<segment>
<wire x1="223.52" y1="165.1" x2="220.98" y2="167.64" width="0.1524" layer="91"/>
<wire x1="220.98" y1="167.64" x2="220.98" y2="177.8" width="0.1524" layer="91"/>
<pinref part="SVORKA30" gate="G$1" pin="2"/>
<wire x1="185.42" y1="175.26" x2="203.2" y2="175.26" width="0.1524" layer="91"/>
<wire x1="203.2" y1="175.26" x2="203.2" y2="177.8" width="0.1524" layer="91"/>
<wire x1="203.2" y1="177.8" x2="220.98" y2="177.8" width="0.1524" layer="91"/>
<label x="190.5" y="175.26" size="1.778" layer="95"/>
</segment>
</net>
<net name="PF_VS" class="0">
<segment>
<wire x1="231.14" y1="160.02" x2="228.6" y2="162.56" width="0.1524" layer="91"/>
<wire x1="228.6" y1="162.56" x2="228.6" y2="175.26" width="0.1524" layer="91"/>
<pinref part="SVORKA30" gate="G$1" pin="1"/>
<label x="190.5" y="172.72" size="1.778" layer="95"/>
<wire x1="228.6" y1="175.26" x2="205.74" y2="175.26" width="0.1524" layer="91"/>
<wire x1="205.74" y1="175.26" x2="205.74" y2="172.72" width="0.1524" layer="91"/>
<wire x1="205.74" y1="172.72" x2="187.96" y2="172.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="HUM_VS" class="0">
<segment>
<wire x1="215.9" y1="170.18" x2="213.36" y2="172.72" width="0.1524" layer="91"/>
<wire x1="213.36" y1="172.72" x2="213.36" y2="180.34" width="0.1524" layer="91"/>
<pinref part="SVORKA30" gate="G$1" pin="5"/>
<wire x1="170.18" y1="175.26" x2="152.4" y2="175.26" width="0.1524" layer="91"/>
<label x="165.1" y="175.26" size="1.778" layer="95" rot="MR0"/>
<wire x1="213.36" y1="180.34" x2="152.4" y2="180.34" width="0.1524" layer="91"/>
<wire x1="152.4" y1="180.34" x2="152.4" y2="175.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VENTSTART" class="0">
<segment>
<wire x1="167.64" y1="172.72" x2="142.24" y2="172.72" width="0.1524" layer="91"/>
<label x="165.1" y="172.72" size="1.778" layer="95" rot="MR0"/>
<label x="142.24" y="172.72" size="1.27" layer="95" rot="MR0" xref="yes"/>
<pinref part="SVORKA30" gate="G$1" pin="6"/>
</segment>
</net>
<net name="HUM" class="0">
<segment>
<pinref part="SVORKA29" gate="G$1" pin="4"/>
<pinref part="TMC1" gate="45" pin="HUM"/>
<wire x1="172.72" y1="167.64" x2="119.38" y2="167.64" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="215.9" y1="170.18" x2="208.28" y2="170.18" width="0.1524" layer="91"/>
<wire x1="208.28" y1="170.18" x2="208.28" y2="167.64" width="0.1524" layer="91"/>
<pinref part="SVORKA29" gate="G$1" pin="3"/>
<wire x1="208.28" y1="167.64" x2="182.88" y2="167.64" width="0.1524" layer="91"/>
<label x="190.5" y="167.64" size="1.778" layer="95"/>
</segment>
</net>
<net name="CO2" class="0">
<segment>
<pinref part="SVORKA29" gate="G$1" pin="5"/>
<pinref part="TMC1" gate="43" pin="CO2"/>
<wire x1="170.18" y1="165.1" x2="119.38" y2="165.1" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SVORKA29" gate="G$1" pin="2"/>
<wire x1="223.52" y1="165.1" x2="185.42" y2="165.1" width="0.1524" layer="91"/>
<label x="190.5" y="165.1" size="1.778" layer="95"/>
</segment>
</net>
<net name="PRESSFILTER" class="0">
<segment>
<pinref part="TMC1" gate="21" pin="PRESSFILTER"/>
<pinref part="SVORKA29" gate="G$1" pin="6"/>
<wire x1="119.38" y1="162.56" x2="167.64" y2="162.56" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PF" class="0">
<segment>
<wire x1="231.14" y1="160.02" x2="208.28" y2="160.02" width="0.1524" layer="91"/>
<wire x1="208.28" y1="160.02" x2="208.28" y2="162.56" width="0.1524" layer="91"/>
<pinref part="SVORKA29" gate="G$1" pin="1"/>
<wire x1="208.28" y1="162.56" x2="187.96" y2="162.56" width="0.1524" layer="91"/>
<label x="190.5" y="162.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="GNDPF" class="0">
<segment>
<wire x1="231.14" y1="160.02" x2="228.6" y2="157.48" width="0.1524" layer="91"/>
<wire x1="228.6" y1="157.48" x2="228.6" y2="152.4" width="0.1524" layer="91"/>
<wire x1="228.6" y1="152.4" x2="187.96" y2="152.4" width="0.1524" layer="91"/>
<label x="190.5" y="152.4" size="1.778" layer="95"/>
<pinref part="SVORKA9" gate="G$1" pin="1"/>
</segment>
</net>
<net name="GNDCO2" class="0">
<segment>
<wire x1="223.52" y1="165.1" x2="220.98" y2="162.56" width="0.1524" layer="91"/>
<wire x1="220.98" y1="162.56" x2="220.98" y2="154.94" width="0.1524" layer="91"/>
<wire x1="220.98" y1="154.94" x2="185.42" y2="154.94" width="0.1524" layer="91"/>
<label x="190.5" y="154.94" size="1.778" layer="95"/>
<pinref part="SVORKA9" gate="G$1" pin="2"/>
</segment>
</net>
<net name="GNDHUM" class="0">
<segment>
<wire x1="215.9" y1="170.18" x2="213.36" y2="167.64" width="0.1524" layer="91"/>
<wire x1="213.36" y1="167.64" x2="213.36" y2="157.48" width="0.1524" layer="91"/>
<wire x1="213.36" y1="157.48" x2="182.88" y2="157.48" width="0.1524" layer="91"/>
<label x="190.5" y="157.48" size="1.778" layer="95"/>
<pinref part="SVORKA9" gate="G$1" pin="3"/>
</segment>
</net>
<net name="POWERTMC" class="0">
<segment>
<pinref part="TMC1" gate="60" pin="POWERTMC"/>
<pinref part="SVORKA28" gate="G$1" pin="4"/>
<wire x1="119.38" y1="124.46" x2="172.72" y2="124.46" width="0.1524" layer="91"/>
<wire x1="172.72" y1="124.46" x2="170.18" y2="127" width="0.1524" layer="91"/>
<junction x="172.72" y="124.46"/>
<pinref part="TMC1" gate="59" pin="POWERTMC"/>
<wire x1="170.18" y1="127" x2="119.38" y2="127" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SVORKA28" gate="G$1" pin="5"/>
<wire x1="170.18" y1="121.92" x2="147.32" y2="121.92" width="0.1524" layer="91"/>
<wire x1="147.32" y1="121.92" x2="147.32" y2="119.38" width="0.1524" layer="91"/>
<label x="165.1" y="121.92" size="1.778" layer="95" rot="MR0"/>
<wire x1="147.32" y1="119.38" x2="142.24" y2="119.38" width="0.1524" layer="91"/>
<label x="142.24" y="119.38" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="PWRD+" class="0">
<segment>
<pinref part="SVORKA28" gate="G$1" pin="2"/>
<wire x1="185.42" y1="121.92" x2="210.82" y2="121.92" width="0.1524" layer="91"/>
<label x="190.5" y="121.92" size="1.778" layer="95"/>
</segment>
</net>
<net name="PWRHER" class="0">
<segment>
<wire x1="231.14" y1="76.2" x2="231.14" y2="83.82" width="0.1524" layer="91"/>
<wire x1="231.14" y1="83.82" x2="220.98" y2="83.82" width="0.1524" layer="91"/>
<wire x1="220.98" y1="83.82" x2="220.98" y2="124.46" width="0.1524" layer="91"/>
<pinref part="SVORKA28" gate="G$1" pin="3"/>
<wire x1="220.98" y1="124.46" x2="182.88" y2="124.46" width="0.1524" layer="91"/>
<label x="190.5" y="124.46" size="1.778" layer="95"/>
</segment>
</net>
<net name="TXD+" class="0">
<segment>
<pinref part="SVORKA31" gate="G$1" pin="3"/>
<wire x1="182.88" y1="114.3" x2="210.82" y2="114.3" width="0.1524" layer="91"/>
<label x="190.5" y="114.3" size="1.778" layer="95"/>
</segment>
</net>
<net name="TXGPS" class="0">
<segment>
<wire x1="231.14" y1="119.38" x2="228.6" y2="116.84" width="0.1524" layer="91"/>
<wire x1="228.6" y1="116.84" x2="228.6" y2="111.76" width="0.1524" layer="91"/>
<pinref part="SVORKA31" gate="G$1" pin="2"/>
<wire x1="185.42" y1="111.76" x2="228.6" y2="111.76" width="0.1524" layer="91"/>
<label x="190.5" y="111.76" size="1.778" layer="95"/>
</segment>
</net>
<net name="RX" class="0">
<segment>
<pinref part="SVORKA31" gate="G$1" pin="6"/>
<pinref part="TMC1" gate="51" pin="RX"/>
<wire x1="167.64" y1="109.22" x2="119.38" y2="109.22" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="231.14" y1="119.38" x2="231.14" y2="109.22" width="0.1524" layer="91"/>
<pinref part="SVORKA31" gate="G$1" pin="1"/>
<label x="190.5" y="109.22" size="1.778" layer="95"/>
<wire x1="187.96" y1="109.22" x2="231.14" y2="109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PWRGPS" class="0">
<segment>
<pinref part="SVORKA28" gate="G$1" pin="1"/>
<label x="190.5" y="119.38" size="1.778" layer="95"/>
<wire x1="187.96" y1="119.38" x2="231.14" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="TX" class="0">
<segment>
<pinref part="TMC1" gate="53" pin="TX"/>
<pinref part="SVORKA31" gate="G$1" pin="5"/>
<wire x1="119.38" y1="111.76" x2="170.18" y2="111.76" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GNDGPS" class="0">
<segment>
<wire x1="231.14" y1="119.38" x2="228.6" y2="121.92" width="0.1524" layer="91"/>
<wire x1="172.72" y1="157.48" x2="149.86" y2="157.48" width="0.1524" layer="91"/>
<wire x1="149.86" y1="157.48" x2="149.86" y2="134.62" width="0.1524" layer="91"/>
<wire x1="149.86" y1="134.62" x2="228.6" y2="134.62" width="0.1524" layer="91"/>
<label x="165.1" y="157.48" size="1.778" layer="95" rot="MR0"/>
<wire x1="228.6" y1="121.92" x2="228.6" y2="134.62" width="0.1524" layer="91"/>
<pinref part="SVORKA9" gate="G$1" pin="4"/>
</segment>
</net>
<net name="GNDD+" class="0">
<segment>
<wire x1="170.18" y1="154.94" x2="152.4" y2="154.94" width="0.1524" layer="91"/>
<label x="165.1" y="154.94" size="1.778" layer="95" rot="MR0"/>
<pinref part="SVORKA9" gate="G$1" pin="5"/>
</segment>
</net>
<net name="GNDHER" class="0">
<segment>
<wire x1="187.96" y1="78.74" x2="228.6" y2="78.74" width="0.1524" layer="91"/>
<wire x1="228.6" y1="78.74" x2="231.14" y2="76.2" width="0.1524" layer="91"/>
<label x="190.5" y="78.74" size="1.778" layer="95"/>
<pinref part="SVORKA11" gate="G$1" pin="1"/>
</segment>
</net>
<net name="PWRTMC" class="0">
<segment>
<pinref part="SVORKA28" gate="G$1" pin="6"/>
<wire x1="167.64" y1="119.38" x2="165.1" y2="119.38" width="0.1524" layer="91"/>
<label x="165.1" y="119.38" size="1.27" layer="95" rot="MR0" xref="yes"/>
</segment>
</net>
<net name="TH+HER" class="0">
<segment>
<pinref part="SVORKA1" gate="G$1" pin="2"/>
<wire x1="231.14" y1="76.2" x2="231.14" y2="66.04" width="0.1524" layer="91"/>
<wire x1="231.14" y1="66.04" x2="185.42" y2="66.04" width="0.1524" layer="91"/>
<label x="190.5" y="66.04" size="1.778" layer="95"/>
</segment>
</net>
<net name="TH-HER" class="0">
<segment>
<wire x1="231.14" y1="76.2" x2="228.6" y2="73.66" width="0.1524" layer="91"/>
<wire x1="228.6" y1="73.66" x2="228.6" y2="68.58" width="0.1524" layer="91"/>
<pinref part="SVORKA1" gate="G$1" pin="3"/>
<wire x1="228.6" y1="68.58" x2="182.88" y2="68.58" width="0.1524" layer="91"/>
<label x="190.5" y="68.58" size="1.778" layer="95"/>
</segment>
</net>
<net name="TH-" class="0">
<segment>
<pinref part="SVORKA1" gate="G$1" pin="4"/>
<wire x1="144.78" y1="66.04" x2="147.32" y2="68.58" width="0.1524" layer="91"/>
<wire x1="147.32" y1="68.58" x2="172.72" y2="68.58" width="0.1524" layer="91"/>
<label x="165.1" y="68.58" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="TH+" class="0">
<segment>
<wire x1="144.78" y1="63.5" x2="147.32" y2="66.04" width="0.1524" layer="91"/>
<pinref part="SVORKA1" gate="G$1" pin="5"/>
<wire x1="147.32" y1="66.04" x2="170.18" y2="66.04" width="0.1524" layer="91"/>
<label x="165.1" y="66.04" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<description>Ventilace</description>
<plain>
<text x="177.8" y="10.16" size="1.778" layer="92">&lt;&lt;&lt;?</text>
<wire x1="257.175" y1="253.365" x2="277.495" y2="253.365" width="0.1524" layer="94"/>
<wire x1="277.495" y1="253.365" x2="277.495" y2="240.665" width="0.1524" layer="94"/>
<wire x1="277.495" y1="240.665" x2="257.175" y2="240.665" width="0.1524" layer="94"/>
<wire x1="257.175" y1="240.665" x2="257.175" y2="253.365" width="0.1524" layer="94"/>
</plain>
<instances>
<instance part="FRAME10" gate="G$1" x="0" y="0"/>
<instance part="FRAME10" gate="G$3" x="287.02" y="0"/>
<instance part="DPS3" gate="A..." x="309.88" y="243.84"/>
<instance part="DPS3" gate=".B.." x="60.96" y="251.46"/>
<instance part="DPS3" gate="..C." x="38.1" y="60.96"/>
<instance part="FAN1" gate="G$1" x="231.14" y="55.88"/>
<instance part="FAN2" gate="G$1" x="231.14" y="81.28"/>
<instance part="FAN3" gate="G$1" x="231.14" y="106.68"/>
<instance part="FAN4" gate="G$1" x="231.14" y="132.08"/>
<instance part="FAN5" gate="G$1" x="231.14" y="157.48"/>
<instance part="FAN6" gate="G$1" x="231.14" y="182.88"/>
<instance part="FAN7" gate="G$1" x="231.14" y="208.28"/>
<instance part="FAN1" gate="G$2" x="314.96" y="99.06"/>
<instance part="U$85" gate="+24V" x="91.44" y="256.54"/>
<instance part="U$32" gate="+24V" x="101.6" y="256.54"/>
<instance part="U$34" gate="+24V" x="111.76" y="256.54"/>
<instance part="U$35" gate="+24V" x="121.92" y="256.54"/>
<instance part="U$42" gate="+24V" x="132.08" y="256.54"/>
<instance part="U$43" gate="+24V" x="142.24" y="256.54"/>
<instance part="U$44" gate="+24V" x="152.4" y="256.54"/>
<instance part="U$46" gate="GND" x="182.88" y="185.42"/>
<instance part="U$47" gate="GND" x="182.88" y="160.02"/>
<instance part="U$48" gate="GND" x="182.88" y="134.62"/>
<instance part="U$49" gate="GND" x="182.88" y="109.22"/>
<instance part="U$50" gate="GND" x="182.88" y="83.82"/>
<instance part="U$51" gate="GND" x="182.88" y="58.42"/>
<instance part="U$52" gate="GND" x="182.88" y="33.02"/>
<instance part="U$53" gate="GND" x="302.26" y="210.82"/>
<instance part="U$54" gate="GND" x="360.68" y="223.52"/>
<instance part="U$55" gate="GND" x="370.84" y="223.52"/>
<instance part="TMC1" gate="49" x="76.2" y="17.78"/>
<instance part="TMC1" gate="35" x="76.2" y="5.08"/>
<instance part="TMC1" gate="39" x="76.2" y="7.62"/>
<instance part="SVORKA20" gate="G$1" x="142.24" y="7.62" rot="R270"/>
<instance part="SVORKA5" gate="G$1" x="142.24" y="20.32" rot="R270"/>
<instance part="TMC1" gate="13" x="76.2" y="20.32"/>
<instance part="U$56" gate="+24V" x="260.985" y="257.175" rot="MR0"/>
<instance part="FA_7P" gate="G$1" x="260.985" y="246.38" rot="MR90"/>
<instance part="U$58" gate="+24V" x="273.05" y="257.175" rot="MR0"/>
<instance part="FA_EM1" gate="G$1" x="273.05" y="246.38" rot="MR90"/>
</instances>
<busses>
</busses>
<nets>
<net name="+24V" class="0">
<segment>
<pinref part="U$85" gate="+24V" pin="+24V"/>
<pinref part="DPS3" gate=".B.." pin="P1"/>
<wire x1="91.44" y1="254" x2="91.44" y2="246.38" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$32" gate="+24V" pin="+24V"/>
<pinref part="DPS3" gate=".B.." pin="P2"/>
<wire x1="101.6" y1="254" x2="101.6" y2="246.38" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$34" gate="+24V" pin="+24V"/>
<pinref part="DPS3" gate=".B.." pin="P3"/>
<wire x1="111.76" y1="254" x2="111.76" y2="246.38" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$35" gate="+24V" pin="+24V"/>
<pinref part="DPS3" gate=".B.." pin="P4"/>
<wire x1="121.92" y1="254" x2="121.92" y2="246.38" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$42" gate="+24V" pin="+24V"/>
<pinref part="DPS3" gate=".B.." pin="P5"/>
<wire x1="132.08" y1="254" x2="132.08" y2="246.38" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$43" gate="+24V" pin="+24V"/>
<pinref part="DPS3" gate=".B.." pin="P6"/>
<wire x1="142.24" y1="254" x2="142.24" y2="246.38" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$44" gate="+24V" pin="+24V"/>
<pinref part="DPS3" gate=".B.." pin="P7"/>
<wire x1="152.4" y1="254" x2="152.4" y2="246.38" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="260.985" y1="254.635" x2="260.985" y2="254" width="0.1524" layer="91"/>
<pinref part="FA_7P" gate="G$1" pin="2A"/>
<pinref part="FA_7P" gate="G$1" pin="2B"/>
<pinref part="FA_7P" gate="G$1" pin="2C"/>
<pinref part="FA_7P" gate="G$1" pin="2D"/>
<pinref part="U$56" gate="+24V" pin="+24V"/>
</segment>
<segment>
<wire x1="273.05" y1="254.635" x2="273.05" y2="254" width="0.1524" layer="91"/>
<pinref part="FA_EM1" gate="G$1" pin="2A"/>
<pinref part="FA_EM1" gate="G$1" pin="2B"/>
<pinref part="FA_EM1" gate="G$1" pin="2C"/>
<pinref part="FA_EM1" gate="G$1" pin="2D"/>
<pinref part="U$58" gate="+24V" pin="+24V"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="U$46" gate="GND" pin="GND"/>
<wire x1="182.88" y1="190.5" x2="182.88" y2="193.04" width="0.1524" layer="91"/>
<pinref part="FAN7" gate="G$1" pin="GND"/>
<wire x1="182.88" y1="193.04" x2="226.06" y2="193.04" width="0.1524" layer="91"/>
<pinref part="FAN7" gate="G$1" pin="LOWER"/>
<wire x1="226.06" y1="193.04" x2="228.6" y2="193.04" width="0.1524" layer="91"/>
<wire x1="228.6" y1="195.58" x2="226.06" y2="195.58" width="0.1524" layer="91"/>
<wire x1="226.06" y1="195.58" x2="226.06" y2="193.04" width="0.1524" layer="91"/>
<junction x="226.06" y="193.04"/>
</segment>
<segment>
<pinref part="U$47" gate="GND" pin="GND"/>
<wire x1="182.88" y1="165.1" x2="182.88" y2="167.64" width="0.1524" layer="91"/>
<wire x1="182.88" y1="167.64" x2="226.06" y2="167.64" width="0.1524" layer="91"/>
<wire x1="226.06" y1="167.64" x2="228.6" y2="167.64" width="0.1524" layer="91"/>
<wire x1="228.6" y1="170.18" x2="226.06" y2="170.18" width="0.1524" layer="91"/>
<wire x1="226.06" y1="170.18" x2="226.06" y2="167.64" width="0.1524" layer="91"/>
<junction x="226.06" y="167.64"/>
<pinref part="FAN6" gate="G$1" pin="GND"/>
<pinref part="FAN6" gate="G$1" pin="LOWER"/>
</segment>
<segment>
<pinref part="U$48" gate="GND" pin="GND"/>
<wire x1="182.88" y1="139.7" x2="182.88" y2="142.24" width="0.1524" layer="91"/>
<wire x1="182.88" y1="142.24" x2="226.06" y2="142.24" width="0.1524" layer="91"/>
<wire x1="226.06" y1="142.24" x2="228.6" y2="142.24" width="0.1524" layer="91"/>
<wire x1="228.6" y1="144.78" x2="226.06" y2="144.78" width="0.1524" layer="91"/>
<wire x1="226.06" y1="144.78" x2="226.06" y2="142.24" width="0.1524" layer="91"/>
<junction x="226.06" y="142.24"/>
<pinref part="FAN5" gate="G$1" pin="GND"/>
<pinref part="FAN5" gate="G$1" pin="LOWER"/>
</segment>
<segment>
<pinref part="U$49" gate="GND" pin="GND"/>
<wire x1="182.88" y1="114.3" x2="182.88" y2="116.84" width="0.1524" layer="91"/>
<wire x1="182.88" y1="116.84" x2="226.06" y2="116.84" width="0.1524" layer="91"/>
<wire x1="226.06" y1="116.84" x2="228.6" y2="116.84" width="0.1524" layer="91"/>
<wire x1="228.6" y1="119.38" x2="226.06" y2="119.38" width="0.1524" layer="91"/>
<wire x1="226.06" y1="119.38" x2="226.06" y2="116.84" width="0.1524" layer="91"/>
<junction x="226.06" y="116.84"/>
<pinref part="FAN4" gate="G$1" pin="GND"/>
<pinref part="FAN4" gate="G$1" pin="LOWER"/>
</segment>
<segment>
<pinref part="U$50" gate="GND" pin="GND"/>
<wire x1="182.88" y1="88.9" x2="182.88" y2="91.44" width="0.1524" layer="91"/>
<wire x1="182.88" y1="91.44" x2="226.06" y2="91.44" width="0.1524" layer="91"/>
<wire x1="226.06" y1="91.44" x2="228.6" y2="91.44" width="0.1524" layer="91"/>
<wire x1="228.6" y1="93.98" x2="226.06" y2="93.98" width="0.1524" layer="91"/>
<wire x1="226.06" y1="93.98" x2="226.06" y2="91.44" width="0.1524" layer="91"/>
<junction x="226.06" y="91.44"/>
<pinref part="FAN3" gate="G$1" pin="GND"/>
<pinref part="FAN3" gate="G$1" pin="LOWER"/>
</segment>
<segment>
<pinref part="U$51" gate="GND" pin="GND"/>
<wire x1="182.88" y1="63.5" x2="182.88" y2="66.04" width="0.1524" layer="91"/>
<wire x1="182.88" y1="66.04" x2="226.06" y2="66.04" width="0.1524" layer="91"/>
<wire x1="226.06" y1="66.04" x2="228.6" y2="66.04" width="0.1524" layer="91"/>
<wire x1="228.6" y1="68.58" x2="226.06" y2="68.58" width="0.1524" layer="91"/>
<wire x1="226.06" y1="68.58" x2="226.06" y2="66.04" width="0.1524" layer="91"/>
<junction x="226.06" y="66.04"/>
<pinref part="FAN2" gate="G$1" pin="GND"/>
<pinref part="FAN2" gate="G$1" pin="LOWER"/>
</segment>
<segment>
<pinref part="U$52" gate="GND" pin="GND"/>
<wire x1="182.88" y1="38.1" x2="182.88" y2="40.64" width="0.1524" layer="91"/>
<wire x1="182.88" y1="40.64" x2="226.06" y2="40.64" width="0.1524" layer="91"/>
<wire x1="226.06" y1="40.64" x2="228.6" y2="40.64" width="0.1524" layer="91"/>
<wire x1="228.6" y1="43.18" x2="226.06" y2="43.18" width="0.1524" layer="91"/>
<wire x1="226.06" y1="43.18" x2="226.06" y2="40.64" width="0.1524" layer="91"/>
<junction x="226.06" y="40.64"/>
<pinref part="FAN1" gate="G$1" pin="GND"/>
<pinref part="FAN1" gate="G$1" pin="LOWER"/>
</segment>
<segment>
<pinref part="U$53" gate="GND" pin="GND"/>
<wire x1="302.26" y1="215.9" x2="302.26" y2="218.44" width="0.1524" layer="91"/>
<pinref part="DPS3" gate="A..." pin="GND"/>
<wire x1="302.26" y1="218.44" x2="304.8" y2="218.44" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$54" gate="GND" pin="GND"/>
<wire x1="360.68" y1="228.6" x2="360.68" y2="231.14" width="0.1524" layer="91"/>
<pinref part="DPS3" gate="A..." pin="PGND@3"/>
<wire x1="360.68" y1="231.14" x2="358.14" y2="231.14" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="DPS3" gate="A..." pin="PGND"/>
<pinref part="U$55" gate="GND" pin="GND"/>
<wire x1="345.44" y1="236.22" x2="370.84" y2="236.22" width="0.1524" layer="91"/>
<wire x1="370.84" y1="236.22" x2="370.84" y2="228.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$3" class="0">
<segment>
<pinref part="FAN7" gate="G$1" pin="UB"/>
<pinref part="DPS3" gate=".B.." pin="L7"/>
<wire x1="228.6" y1="205.74" x2="152.4" y2="205.74" width="0.1524" layer="91"/>
<wire x1="152.4" y1="205.74" x2="152.4" y2="215.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="FAN6" gate="G$1" pin="UB"/>
<pinref part="DPS3" gate=".B.." pin="L6"/>
<wire x1="228.6" y1="180.34" x2="142.24" y2="180.34" width="0.1524" layer="91"/>
<wire x1="142.24" y1="180.34" x2="142.24" y2="215.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<pinref part="FAN5" gate="G$1" pin="UB"/>
<pinref part="DPS3" gate=".B.." pin="L5"/>
<wire x1="228.6" y1="154.94" x2="132.08" y2="154.94" width="0.1524" layer="91"/>
<wire x1="132.08" y1="154.94" x2="132.08" y2="215.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="DPS3" gate=".B.." pin="L4"/>
<pinref part="FAN4" gate="G$1" pin="UB"/>
<wire x1="121.92" y1="129.54" x2="121.92" y2="215.9" width="0.1524" layer="91"/>
<wire x1="228.6" y1="129.54" x2="121.92" y2="129.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$9" class="0">
<segment>
<pinref part="FAN3" gate="G$1" pin="UB"/>
<wire x1="228.6" y1="104.14" x2="111.76" y2="104.14" width="0.1524" layer="91"/>
<pinref part="DPS3" gate=".B.." pin="L3"/>
<wire x1="111.76" y1="104.14" x2="111.76" y2="215.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<pinref part="FAN2" gate="G$1" pin="UB"/>
<wire x1="228.6" y1="78.74" x2="101.6" y2="78.74" width="0.1524" layer="91"/>
<pinref part="DPS3" gate=".B.." pin="L2"/>
<wire x1="101.6" y1="78.74" x2="101.6" y2="215.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$7" class="0">
<segment>
<pinref part="FAN1" gate="G$1" pin="UB"/>
<wire x1="228.6" y1="53.34" x2="91.44" y2="53.34" width="0.1524" layer="91"/>
<pinref part="DPS3" gate=".B.." pin="L1"/>
<wire x1="91.44" y1="53.34" x2="91.44" y2="215.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="FANCTRL" class="0">
<segment>
<pinref part="FAN1" gate="G$1" pin="PWMLIN"/>
<pinref part="FAN7" gate="G$1" pin="PWMLIN"/>
<wire x1="170.18" y1="58.42" x2="170.18" y2="45.72" width="0.1524" layer="91"/>
<wire x1="170.18" y1="45.72" x2="228.6" y2="45.72" width="0.1524" layer="91"/>
<wire x1="170.18" y1="58.42" x2="170.18" y2="71.12" width="0.1524" layer="91"/>
<wire x1="170.18" y1="71.12" x2="170.18" y2="96.52" width="0.1524" layer="91"/>
<wire x1="170.18" y1="96.52" x2="170.18" y2="121.92" width="0.1524" layer="91"/>
<wire x1="170.18" y1="121.92" x2="170.18" y2="147.32" width="0.1524" layer="91"/>
<wire x1="170.18" y1="147.32" x2="170.18" y2="172.72" width="0.1524" layer="91"/>
<wire x1="170.18" y1="172.72" x2="170.18" y2="198.12" width="0.1524" layer="91"/>
<wire x1="170.18" y1="198.12" x2="228.6" y2="198.12" width="0.1524" layer="91"/>
<pinref part="FAN2" gate="G$1" pin="PWMLIN"/>
<wire x1="228.6" y1="71.12" x2="170.18" y2="71.12" width="0.1524" layer="91"/>
<junction x="170.18" y="71.12"/>
<pinref part="FAN3" gate="G$1" pin="PWMLIN"/>
<wire x1="228.6" y1="96.52" x2="170.18" y2="96.52" width="0.1524" layer="91"/>
<junction x="170.18" y="96.52"/>
<pinref part="FAN4" gate="G$1" pin="PWMLIN"/>
<wire x1="228.6" y1="121.92" x2="170.18" y2="121.92" width="0.1524" layer="91"/>
<junction x="170.18" y="121.92"/>
<pinref part="FAN5" gate="G$1" pin="PWMLIN"/>
<wire x1="228.6" y1="147.32" x2="170.18" y2="147.32" width="0.1524" layer="91"/>
<junction x="170.18" y="147.32"/>
<pinref part="FAN6" gate="G$1" pin="PWMLIN"/>
<wire x1="228.6" y1="172.72" x2="170.18" y2="172.72" width="0.1524" layer="91"/>
<junction x="170.18" y="172.72"/>
<junction x="170.18" y="58.42"/>
<wire x1="170.18" y1="58.42" x2="116.84" y2="58.42" width="0.1524" layer="91"/>
<wire x1="116.84" y1="58.42" x2="116.84" y2="20.32" width="0.1524" layer="91"/>
<pinref part="SVORKA5" gate="G$1" pin="5"/>
<wire x1="116.84" y1="20.32" x2="134.62" y2="20.32" width="0.1524" layer="91"/>
<label x="119.38" y="20.32" size="1.778" layer="95"/>
</segment>
</net>
<net name="LP" class="0">
<segment>
<pinref part="TMC1" gate="35" pin="LP"/>
<pinref part="SVORKA20" gate="G$1" pin="6"/>
<wire x1="109.22" y1="5.08" x2="132.08" y2="5.08" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SVORKA20" gate="G$1" pin="1"/>
<wire x1="152.4" y1="5.08" x2="167.64" y2="5.08" width="0.1524" layer="91"/>
<label x="167.64" y="5.08" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="HP" class="0">
<segment>
<pinref part="TMC1" gate="39" pin="HP"/>
<pinref part="SVORKA20" gate="G$1" pin="5"/>
<wire x1="109.22" y1="7.62" x2="134.62" y2="7.62" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SVORKA20" gate="G$1" pin="2"/>
<wire x1="149.86" y1="7.62" x2="167.64" y2="7.62" width="0.1524" layer="91"/>
<label x="167.64" y="7.62" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="INVLIN" class="0">
<segment>
<pinref part="SVORKA20" gate="G$1" pin="4"/>
<wire x1="137.16" y1="10.16" x2="121.92" y2="10.16" width="0.1524" layer="91"/>
<label x="132.08" y="10.16" size="1.778" layer="95" rot="MR0"/>
<label x="121.92" y="10.16" size="1.27" layer="95" rot="MR0" xref="yes"/>
</segment>
<segment>
<pinref part="FAN7" gate="G$1" pin="INVLIN"/>
<wire x1="228.6" y1="200.66" x2="172.72" y2="200.66" width="0.1524" layer="91"/>
<wire x1="172.72" y1="200.66" x2="172.72" y2="175.26" width="0.1524" layer="91"/>
<pinref part="FAN6" gate="G$1" pin="INVLIN"/>
<wire x1="172.72" y1="175.26" x2="172.72" y2="149.86" width="0.1524" layer="91"/>
<wire x1="172.72" y1="149.86" x2="172.72" y2="124.46" width="0.1524" layer="91"/>
<wire x1="172.72" y1="124.46" x2="172.72" y2="99.06" width="0.1524" layer="91"/>
<wire x1="172.72" y1="99.06" x2="172.72" y2="73.66" width="0.1524" layer="91"/>
<wire x1="172.72" y1="73.66" x2="172.72" y2="48.26" width="0.1524" layer="91"/>
<wire x1="228.6" y1="175.26" x2="172.72" y2="175.26" width="0.1524" layer="91"/>
<junction x="172.72" y="175.26"/>
<pinref part="FAN5" gate="G$1" pin="INVLIN"/>
<wire x1="228.6" y1="149.86" x2="172.72" y2="149.86" width="0.1524" layer="91"/>
<junction x="172.72" y="149.86"/>
<pinref part="FAN4" gate="G$1" pin="INVLIN"/>
<wire x1="228.6" y1="124.46" x2="172.72" y2="124.46" width="0.1524" layer="91"/>
<junction x="172.72" y="124.46"/>
<pinref part="FAN3" gate="G$1" pin="INVLIN"/>
<wire x1="228.6" y1="99.06" x2="172.72" y2="99.06" width="0.1524" layer="91"/>
<junction x="172.72" y="99.06"/>
<pinref part="FAN2" gate="G$1" pin="INVLIN"/>
<wire x1="228.6" y1="73.66" x2="172.72" y2="73.66" width="0.1524" layer="91"/>
<junction x="172.72" y="73.66"/>
<pinref part="FAN1" gate="G$1" pin="INVLIN"/>
<wire x1="228.6" y1="48.26" x2="172.72" y2="48.26" width="0.1524" layer="91"/>
<junction x="172.72" y="48.26"/>
<pinref part="SVORKA20" gate="G$1" pin="3"/>
<wire x1="147.32" y1="10.16" x2="172.72" y2="10.16" width="0.1524" layer="91"/>
<wire x1="172.72" y1="10.16" x2="172.72" y2="48.26" width="0.1524" layer="91"/>
<label x="154.94" y="10.16" size="1.778" layer="95"/>
</segment>
</net>
<net name="!VENTILATION_ON" class="0">
<segment>
<label x="35.56" y="231.14" size="1.27" layer="95" rot="MR0" xref="yes"/>
<wire x1="35.56" y1="231.14" x2="55.88" y2="231.14" width="0.1524" layer="91"/>
<pinref part="DPS3" gate=".B.." pin="!VENTILATION_ON"/>
</segment>
</net>
<net name="FANCONTROL" class="0">
<segment>
<pinref part="TMC1" gate="49" pin="FANCONTROL"/>
<pinref part="SVORKA5" gate="G$1" pin="6"/>
<wire x1="109.22" y1="17.78" x2="132.08" y2="17.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="FAIL2" class="0">
<segment>
<label x="154.94" y="20.32" size="1.778" layer="95"/>
<pinref part="DPS3" gate="..C." pin="EBM_REG_OUT"/>
<wire x1="88.9" y1="45.72" x2="165.1" y2="45.72" width="0.1524" layer="91"/>
<wire x1="165.1" y1="45.72" x2="165.1" y2="20.32" width="0.1524" layer="91"/>
<label x="91.44" y="45.72" size="1.778" layer="95"/>
<pinref part="SVORKA5" gate="G$1" pin="2"/>
<wire x1="165.1" y1="20.32" x2="149.86" y2="20.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SVORKA5" gate="G$1" pin="1"/>
<wire x1="152.4" y1="17.78" x2="187.96" y2="17.78" width="0.1524" layer="91"/>
<label x="187.96" y="17.78" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="FAIL3" class="0">
<segment>
<pinref part="SVORKA5" gate="G$1" pin="3"/>
<wire x1="147.32" y1="22.86" x2="162.56" y2="22.86" width="0.1524" layer="91"/>
<label x="154.94" y="22.86" size="1.778" layer="95"/>
</segment>
</net>
<net name="FAILH/C" class="0">
<segment>
<pinref part="TMC1" gate="13" pin="FAILH/C"/>
<pinref part="SVORKA5" gate="G$1" pin="4"/>
<wire x1="137.16" y1="22.86" x2="111.76" y2="22.86" width="0.1524" layer="91"/>
<wire x1="111.76" y1="22.86" x2="111.76" y2="20.32" width="0.1524" layer="91"/>
<wire x1="111.76" y1="20.32" x2="109.22" y2="20.32" width="0.1524" layer="91"/>
</segment>
</net>
<net name="FAIL1" class="0">
<segment>
<pinref part="DPS3" gate=".B.." pin="!FAILURE"/>
<wire x1="187.96" y1="238.76" x2="210.82" y2="238.76" width="0.1524" layer="91"/>
<label x="210.82" y="238.76" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="K1-7" class="0">
<segment>
<pinref part="DPS3" gate="..C." pin="EBM_REG_IN"/>
<wire x1="33.02" y1="50.8" x2="15.24" y2="50.8" width="0.1524" layer="91"/>
<label x="15.24" y="50.8" size="1.27" layer="95" rot="MR0" xref="yes"/>
</segment>
</net>
<net name="EM_PWR" class="0">
<segment>
<pinref part="FA_EM1" gate="G$1" pin="1A"/>
<pinref part="FA_EM1" gate="G$1" pin="1B"/>
<pinref part="FA_EM1" gate="G$1" pin="1C"/>
<pinref part="FA_EM1" gate="G$1" pin="1D"/>
<wire x1="273.05" y1="219.075" x2="273.05" y2="238.76" width="0.1524" layer="91"/>
<label x="272.415" y="220.98" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="PWR_7P" class="0">
<segment>
<pinref part="FA_7P" gate="G$1" pin="1A"/>
<pinref part="FA_7P" gate="G$1" pin="1B"/>
<pinref part="FA_7P" gate="G$1" pin="1C"/>
<pinref part="FA_7P" gate="G$1" pin="1D"/>
<wire x1="261.62" y1="218.44" x2="260.985" y2="238.76" width="0.1524" layer="91"/>
<label x="260.35" y="220.98" size="1.778" layer="95" rot="R90"/>
<wire x1="261.62" y1="218.44" x2="261.62" y2="215.9" width="0.1524" layer="91"/>
<wire x1="261.62" y1="215.9" x2="287.02" y2="215.9" width="0.1524" layer="91"/>
<pinref part="DPS3" gate="A..." pin="SUPPLY"/>
<wire x1="287.02" y1="215.9" x2="287.02" y2="236.22" width="0.1524" layer="91"/>
<wire x1="287.02" y1="236.22" x2="304.8" y2="236.22" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<description>Houkačka</description>
<plain>
<wire x1="284.48" y1="116.84" x2="284.48" y2="218.44" width="0.1524" layer="94" style="longdash"/>
<text x="281.94" y="129.54" size="1.778" layer="94" rot="R270">Rozvaděč</text>
<text x="287.02" y="129.54" size="1.778" layer="94" rot="R270" align="top-left">Strojovna</text>
</plain>
<instances>
<instance part="FRAME7" gate="G$2" x="287.02" y="0"/>
<instance part="FRAME7" gate="G$1" x="0" y="0"/>
<instance part="SVORKA35" gate="G$1" x="190.5" y="210.82" rot="R270"/>
<instance part="U$15" gate="G$1" x="314.96" y="170.18"/>
<instance part="PRUCHODKA25" gate="G$1" x="284.48" y="170.18"/>
<instance part="TMC1" gate="32" x="124.46" y="218.44"/>
<instance part="DPS3" gate="...D" x="175.26" y="190.5"/>
</instances>
<busses>
<bus name="HORN+,HORN-">
<segment>
<wire x1="256.54" y1="170.18" x2="297.18" y2="170.18" width="0.762" layer="92"/>
<wire x1="256.54" y1="170.18" x2="254" y2="172.72" width="0.762" layer="92"/>
<wire x1="254" y1="172.72" x2="254" y2="208.28" width="0.762" layer="92"/>
</segment>
<segment>
<wire x1="228.6" y1="165.1" x2="228.6" y2="198.12" width="0.762" layer="92"/>
<wire x1="228.6" y1="198.12" x2="226.06" y2="200.66" width="0.762" layer="92"/>
<wire x1="226.06" y1="200.66" x2="154.94" y2="200.66" width="0.762" layer="92"/>
<wire x1="154.94" y1="200.66" x2="152.4" y2="203.2" width="0.762" layer="92"/>
<wire x1="152.4" y1="203.2" x2="152.4" y2="208.28" width="0.762" layer="92"/>
</segment>
</bus>
</busses>
<nets>
<net name="HORN+" class="0">
<segment>
<wire x1="297.18" y1="170.18" x2="302.26" y2="175.26" width="0.1524" layer="91"/>
<pinref part="U$15" gate="G$1" pin="P$1"/>
<wire x1="302.26" y1="175.26" x2="314.96" y2="175.26" width="0.1524" layer="91"/>
<label x="304.8" y="177.8" size="1.778" layer="95" rot="MR180"/>
</segment>
<segment>
<wire x1="254" y1="205.74" x2="251.46" y2="208.28" width="0.1524" layer="91"/>
<pinref part="SVORKA35" gate="G$1" pin="1"/>
<wire x1="251.46" y1="208.28" x2="200.66" y2="208.28" width="0.1524" layer="91"/>
<label x="203.2" y="208.28" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="SVORKA35" gate="G$1" pin="6"/>
<wire x1="152.4" y1="205.74" x2="154.94" y2="208.28" width="0.1524" layer="91"/>
<wire x1="154.94" y1="208.28" x2="180.34" y2="208.28" width="0.1524" layer="91"/>
<label x="177.8" y="208.28" size="1.778" layer="95" rot="MR0"/>
</segment>
<segment>
<wire x1="228.6" y1="175.26" x2="226.06" y2="172.72" width="0.1524" layer="91"/>
<wire x1="226.06" y1="172.72" x2="210.82" y2="172.72" width="0.1524" layer="91"/>
<label x="213.36" y="172.72" size="1.778" layer="95"/>
<pinref part="DPS3" gate="...D" pin="HORN"/>
</segment>
</net>
<net name="HORN-" class="0">
<segment>
<wire x1="297.18" y1="170.18" x2="302.26" y2="165.1" width="0.1524" layer="91"/>
<pinref part="U$15" gate="G$1" pin="P$2"/>
<wire x1="302.26" y1="165.1" x2="314.96" y2="165.1" width="0.1524" layer="91"/>
<label x="304.8" y="167.64" size="1.778" layer="95" rot="MR180"/>
</segment>
<segment>
<wire x1="254" y1="208.28" x2="251.46" y2="210.82" width="0.1524" layer="91"/>
<pinref part="SVORKA35" gate="G$1" pin="2"/>
<wire x1="251.46" y1="210.82" x2="198.12" y2="210.82" width="0.1524" layer="91"/>
<label x="203.2" y="210.82" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="SVORKA35" gate="G$1" pin="5"/>
<wire x1="152.4" y1="208.28" x2="154.94" y2="210.82" width="0.1524" layer="91"/>
<wire x1="154.94" y1="210.82" x2="182.88" y2="210.82" width="0.1524" layer="91"/>
<label x="177.8" y="210.82" size="1.778" layer="95" rot="MR0"/>
</segment>
<segment>
<wire x1="228.6" y1="165.1" x2="226.06" y2="162.56" width="0.1524" layer="91"/>
<wire x1="226.06" y1="162.56" x2="210.82" y2="162.56" width="0.1524" layer="91"/>
<label x="213.36" y="162.56" size="1.778" layer="95"/>
<pinref part="DPS3" gate="...D" pin="HORN_RETURN"/>
</segment>
</net>
<net name="!HORN_ON" class="0">
<segment>
<wire x1="170.18" y1="177.8" x2="119.38" y2="177.8" width="0.1524" layer="91"/>
<wire x1="119.38" y1="177.8" x2="119.38" y2="226.06" width="0.1524" layer="91"/>
<wire x1="119.38" y1="226.06" x2="228.6" y2="226.06" width="0.1524" layer="91"/>
<wire x1="228.6" y1="226.06" x2="228.6" y2="213.36" width="0.1524" layer="91"/>
<pinref part="SVORKA35" gate="G$1" pin="3"/>
<wire x1="228.6" y1="213.36" x2="195.58" y2="213.36" width="0.1524" layer="91"/>
<label x="167.64" y="177.8" size="1.778" layer="95" rot="MR0"/>
<label x="203.2" y="213.36" size="1.778" layer="95"/>
<label x="241.3" y="226.06" size="1.27" layer="95" xref="yes"/>
<wire x1="241.3" y1="226.06" x2="228.6" y2="226.06" width="0.1524" layer="91"/>
<junction x="228.6" y="226.06"/>
<pinref part="DPS3" gate="...D" pin="!HORN_ON"/>
</segment>
</net>
<net name="ALERT" class="0">
<segment>
<pinref part="SVORKA35" gate="G$1" pin="4"/>
<pinref part="TMC1" gate="32" pin="ALERT"/>
<wire x1="185.42" y1="213.36" x2="160.02" y2="213.36" width="0.1524" layer="91"/>
<wire x1="160.02" y1="213.36" x2="160.02" y2="218.44" width="0.1524" layer="91"/>
<wire x1="160.02" y1="218.44" x2="157.48" y2="218.44" width="0.1524" layer="91"/>
<label x="177.8" y="213.36" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<description>ETA relé</description>
<plain>
<wire x1="239.4585" y1="148.971" x2="240.0935" y2="148.971" width="0.1524" layer="94"/>
<wire x1="240.7285" y1="148.971" x2="240.0935" y2="148.971" width="0.1524" layer="94"/>
<wire x1="240.0935" y1="148.971" x2="240.0935" y2="148.336" width="0.1524" layer="94"/>
<wire x1="240.0935" y1="148.971" x2="240.0935" y2="149.606" width="0.1524" layer="94"/>
<wire x1="240.0935" y1="147.701" x2="240.0935" y2="145.161" width="0.1524" layer="94"/>
<circle x="240.0935" y="148.971" radius="1.4199" width="0.1524" layer="94"/>
<circle x="240.0935" y="148.971" radius="1.1447" width="0.3048" layer="94"/>
<text x="236.601" y="150.876" size="1.4224" layer="94">+24 COOL</text>
<text x="275.6535" y="114.3635" size="1.778" layer="96">15A</text>
<text x="254.0635" y="114.3635" size="1.778" layer="96">15A</text>
<text x="295.9735" y="114.3635" size="1.778" layer="96">15A</text>
<text x="316.9285" y="114.3635" size="1.778" layer="96">15A</text>
<text x="275.6535" y="61.6585" size="1.778" layer="96">15A</text>
<text x="254.0635" y="61.6585" size="1.778" layer="96">15A</text>
<text x="295.9735" y="61.6585" size="1.778" layer="96">15A</text>
<text x="316.9285" y="61.6585" size="1.778" layer="96">15A</text>
<text x="338.5185" y="114.3635" size="1.778" layer="96">15A</text>
<text x="359.4735" y="114.3635" size="1.778" layer="96">15A</text>
<text x="338.5185" y="61.6585" size="1.778" layer="96">15A</text>
<wire x1="238.8235" y1="96.901" x2="239.4585" y2="96.901" width="0.1524" layer="94"/>
<wire x1="240.0935" y1="96.901" x2="239.4585" y2="96.901" width="0.1524" layer="94"/>
<wire x1="239.4585" y1="96.901" x2="239.4585" y2="96.266" width="0.1524" layer="94"/>
<wire x1="239.4585" y1="96.901" x2="239.4585" y2="97.536" width="0.1524" layer="94"/>
<wire x1="239.4585" y1="95.631" x2="239.4585" y2="93.091" width="0.1524" layer="94"/>
<circle x="239.4585" y="96.901" radius="1.4199" width="0.1524" layer="94"/>
<circle x="239.4585" y="96.901" radius="1.1447" width="0.3048" layer="94"/>
<text x="235.966" y="98.806" size="1.4224" layer="94">+24 COOL</text>
<wire x1="63.5" y1="35.56" x2="62.23" y2="35.56" width="0.1524" layer="94"/>
<wire x1="60.96" y1="35.56" x2="59.055" y2="35.56" width="0.1524" layer="94"/>
<wire x1="26.67" y1="31.4325" x2="27.305" y2="31.4325" width="0.1524" layer="94"/>
<wire x1="27.94" y1="31.4325" x2="27.305" y2="31.4325" width="0.1524" layer="94"/>
<wire x1="27.305" y1="31.4325" x2="27.305" y2="30.7975" width="0.1524" layer="94"/>
<wire x1="27.305" y1="31.4325" x2="27.305" y2="32.0675" width="0.1524" layer="94"/>
<wire x1="27.305" y1="30.1625" x2="27.305" y2="27.6225" width="0.1524" layer="94"/>
<circle x="27.305" y="31.4325" radius="1.4199" width="0.1524" layer="94"/>
<circle x="27.305" y="31.4325" radius="1.1447" width="0.3048" layer="94"/>
<text x="23.8125" y="33.3375" size="1.4224" layer="94">+24 COOL</text>
<wire x1="58.42" y1="127" x2="58.42" y2="243.84" width="0.1524" layer="94" style="longdash"/>
<text x="27.94" y="160.02" size="1.778" layer="94">HP2</text>
<text x="27.94" y="139.7" size="1.778" layer="94">HP3</text>
<wire x1="154.813" y1="35.433" x2="153.543" y2="35.433" width="0.1524" layer="94"/>
<wire x1="152.273" y1="35.433" x2="150.368" y2="35.433" width="0.1524" layer="94"/>
<text x="150.368" y="39.878" size="2.1844" layer="95" rot="R90">A2</text>
<text x="149.733" y="26.543" size="2.1844" layer="95" rot="R90">A1</text>
<text x="27.94" y="220.98" size="1.778" layer="94">HP4</text>
<text x="27.94" y="193.04" size="1.778" layer="94">VALVE</text>
<wire x1="223.52" y1="160.02" x2="223.52" y2="223.52" width="0.1524" layer="94" style="longdash"/>
</plain>
<instances>
<instance part="V24" gate="G$1" x="247.7135" y="98.4885" rot="R90"/>
<instance part="RE5" gate="G$1" x="252.7935" y="126.4285"/>
<instance part="RE6" gate="G$1" x="273.7485" y="126.4285"/>
<instance part="RE7" gate="G$1" x="294.0685" y="126.4285"/>
<instance part="RE8" gate="G$1" x="315.0235" y="126.4285"/>
<instance part="U$59" gate="GND" x="252.7935" y="107.3785"/>
<instance part="U$79" gate="GND" x="273.7485" y="107.3785"/>
<instance part="U$81" gate="GND" x="294.0685" y="107.3785"/>
<instance part="U$82" gate="GND" x="315.0235" y="107.3785"/>
<instance part="V65" gate="G$1" x="268.6685" y="98.4885" rot="R90"/>
<instance part="V84" gate="G$1" x="288.9885" y="98.4885" rot="R90"/>
<instance part="V85" gate="G$1" x="309.9435" y="98.4885" rot="R90"/>
<instance part="V86" gate="G$1" x="247.7135" y="45.7835" rot="R90"/>
<instance part="RE11" gate="G$1" x="252.7935" y="73.7235"/>
<instance part="RE12" gate="G$1" x="273.7485" y="73.7235"/>
<instance part="RE13" gate="G$1" x="294.0685" y="73.7235"/>
<instance part="RE14" gate="G$1" x="315.0235" y="73.7235"/>
<instance part="U$60" gate="GND" x="252.7935" y="54.6735"/>
<instance part="U$61" gate="GND" x="273.7485" y="54.6735"/>
<instance part="U$62" gate="GND" x="294.0685" y="54.6735"/>
<instance part="U$63" gate="GND" x="315.0235" y="54.6735"/>
<instance part="V87" gate="G$1" x="268.6685" y="45.7835" rot="R90"/>
<instance part="V88" gate="G$1" x="288.9885" y="45.7835" rot="R90"/>
<instance part="V89" gate="G$1" x="309.9435" y="45.7835" rot="R90"/>
<instance part="RE9" gate="G$1" x="336.6135" y="126.4285"/>
<instance part="RE10" gate="G$1" x="357.5685" y="126.4285"/>
<instance part="U$67" gate="GND" x="336.6135" y="107.3785"/>
<instance part="U$68" gate="GND" x="357.5685" y="107.3785"/>
<instance part="V92" gate="G$1" x="331.5335" y="98.4885" rot="R90"/>
<instance part="V93" gate="G$1" x="352.4885" y="98.4885" rot="R90"/>
<instance part="RE15" gate="G$1" x="336.6135" y="73.7235"/>
<instance part="U$69" gate="GND" x="336.6135" y="54.6735"/>
<instance part="V94" gate="G$1" x="331.5335" y="45.7835" rot="R90"/>
<instance part="RE4" gate="G$1" x="68.58" y="35.56" smashed="yes">
<attribute name="NAME" x="68.707" y="38.989" size="2.032" layer="95"/>
<attribute name="VALUE" x="78.867" y="27.432" size="2.032" layer="96" rot="R90"/>
</instance>
<instance part="RE4A" gate="G$1" x="59.69" y="38.1" smashed="yes" rot="R180"/>
<instance part="V34" gate="G$1" x="52.07" y="65.405" rot="R270"/>
<instance part="S2" gate="A" x="38.1" y="38.1" rot="MR270"/>
<instance part="U$70" gate="+24V" x="30.48" y="43.18" rot="MR0"/>
<instance part="U$71" gate="G$1" x="43.815" y="44.45"/>
<instance part="S1A" gate="A" x="34.925" y="25.4" rot="MR270"/>
<instance part="S3" gate="A" x="52.07" y="34.925" rot="MR180"/>
<instance part="FRAME11" gate="G$1" x="0" y="0"/>
<instance part="FRAME11" gate="G$3" x="287.02" y="0"/>
<instance part="SVORKA19" gate="G$1" x="157.48" y="134.62" rot="R270"/>
<instance part="SVORKA44" gate="G$1" x="157.48" y="160.02" rot="R270"/>
<instance part="V53" gate="G$1" x="22.86" y="157.48" rot="MR180"/>
<instance part="V54" gate="G$1" x="22.86" y="137.16"/>
<instance part="PRUCHODKA30" gate="G$1" x="58.42" y="137.16" rot="R180"/>
<instance part="PRUCHODKA31" gate="G$1" x="58.42" y="157.48" rot="R180"/>
<instance part="SVORKA12" gate="G$1" x="157.48" y="180.34" rot="R270"/>
<instance part="U$96" gate="GND" x="144.78" y="170.18"/>
<instance part="SVORKA46" gate="G$1" x="157.48" y="203.2" rot="R270"/>
<instance part="V56" gate="G$1" x="259.08" y="193.04" rot="R180"/>
<instance part="PRUCHODKA32" gate="G$1" x="223.52" y="193.04"/>
<instance part="V7" gate="G$1" x="259.08" y="187.96" rot="R180"/>
<instance part="PRUCHODKA36" gate="G$1" x="223.52" y="187.96"/>
<instance part="RE22" gate="G$1" x="160.02" y="35.56" smashed="yes">
<attribute name="NAME" x="156.337" y="34.544" size="2.032" layer="95"/>
<attribute name="VALUE" x="168.402" y="28.067" size="2.032" layer="96" rot="R90"/>
</instance>
<instance part="RE4A1" gate="G$1" x="151.003" y="37.973" smashed="yes" rot="R180"/>
<instance part="V8" gate="G$1" x="134.493" y="44.958"/>
<instance part="S4" gate="A" x="134.493" y="49.403" rot="R270"/>
<instance part="U$87" gate="+24V" x="142.748" y="54.483"/>
<instance part="U$90" gate="G$1" x="127.508" y="53.213"/>
<instance part="V9" gate="G$1" x="22.86" y="190.5" rot="MR180"/>
<instance part="PRUCHODKA37" gate="G$1" x="58.42" y="190.5" rot="R180"/>
<instance part="PRUCHODKA38" gate="G$1" x="58.42" y="218.44" rot="R180"/>
<instance part="V10" gate="G$1" x="22.86" y="218.44" rot="MR180"/>
<instance part="SVORKA2" gate="G$1" x="157.48" y="233.68" rot="R270"/>
<instance part="V23" gate="G$1" x="22.86" y="233.68" smashed="yes">
<attribute name="VALUE" x="34.29" y="232.918" size="1.4224" layer="96" rot="MR0"/>
</instance>
<instance part="PRUCHODKA39" gate="G$1" x="58.42" y="233.68" rot="R180"/>
<instance part="V47" gate="G$1" x="22.86" y="175.26" smashed="yes" rot="MR180">
<attribute name="VALUE" x="24.765" y="176.022" size="1.4224" layer="96" rot="MR180"/>
</instance>
<instance part="PRUCHODKA40" gate="G$1" x="58.42" y="175.26" rot="R180"/>
</instances>
<busses>
<bus name="VC0,VC1">
<segment>
<wire x1="38.1" y1="157.48" x2="93.98" y2="157.48" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="VC0,VC2">
<segment>
<wire x1="38.1" y1="137.16" x2="93.98" y2="137.16" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="SOLE,GNDSOLE">
<segment>
<wire x1="243.84" y1="193.04" x2="190.5" y2="193.04" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="CLUTCH,GNDCLUTCH">
<segment>
<wire x1="243.84" y1="187.96" x2="195.58" y2="187.96" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="RELIEF,GNDRELIEF">
<segment>
<wire x1="127" y1="190.5" x2="38.1" y2="190.5" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="HP4+,HP4-">
<segment>
<wire x1="38.1" y1="218.44" x2="127" y2="218.44" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="PSOK,ELMOT,HP4">
<segment>
<wire x1="127" y1="233.68" x2="38.1" y2="233.68" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="HL/SWITCH1,HL/SWITCH2">
<segment>
<wire x1="38.1" y1="175.26" x2="106.68" y2="175.26" width="0.762" layer="92"/>
</segment>
</bus>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<wire x1="252.7935" y1="112.4585" x2="252.7935" y2="113.7285" width="0.1524" layer="91"/>
<pinref part="U$59" gate="GND" pin="GND"/>
<pinref part="RE5" gate="G$1" pin="3"/>
</segment>
<segment>
<wire x1="273.7485" y1="112.4585" x2="273.7485" y2="113.7285" width="0.1524" layer="91"/>
<pinref part="U$79" gate="GND" pin="GND"/>
<pinref part="RE6" gate="G$1" pin="3"/>
</segment>
<segment>
<wire x1="294.0685" y1="112.4585" x2="294.0685" y2="113.7285" width="0.1524" layer="91"/>
<pinref part="U$81" gate="GND" pin="GND"/>
<pinref part="RE7" gate="G$1" pin="3"/>
</segment>
<segment>
<wire x1="315.0235" y1="112.4585" x2="315.0235" y2="113.7285" width="0.1524" layer="91"/>
<pinref part="U$82" gate="GND" pin="GND"/>
<pinref part="RE8" gate="G$1" pin="3"/>
</segment>
<segment>
<wire x1="252.7935" y1="59.7535" x2="252.7935" y2="61.0235" width="0.1524" layer="91"/>
<pinref part="U$60" gate="GND" pin="GND"/>
<pinref part="RE11" gate="G$1" pin="3"/>
</segment>
<segment>
<wire x1="273.7485" y1="59.7535" x2="273.7485" y2="61.0235" width="0.1524" layer="91"/>
<pinref part="U$61" gate="GND" pin="GND"/>
<pinref part="RE12" gate="G$1" pin="3"/>
</segment>
<segment>
<wire x1="294.0685" y1="59.7535" x2="294.0685" y2="61.0235" width="0.1524" layer="91"/>
<pinref part="U$62" gate="GND" pin="GND"/>
<pinref part="RE13" gate="G$1" pin="3"/>
</segment>
<segment>
<wire x1="315.0235" y1="59.7535" x2="315.0235" y2="61.0235" width="0.1524" layer="91"/>
<pinref part="U$63" gate="GND" pin="GND"/>
<pinref part="RE14" gate="G$1" pin="3"/>
</segment>
<segment>
<wire x1="336.6135" y1="112.4585" x2="336.6135" y2="113.7285" width="0.1524" layer="91"/>
<pinref part="U$67" gate="GND" pin="GND"/>
<pinref part="RE9" gate="G$1" pin="3"/>
</segment>
<segment>
<wire x1="357.5685" y1="112.4585" x2="357.5685" y2="113.7285" width="0.1524" layer="91"/>
<pinref part="U$68" gate="GND" pin="GND"/>
<pinref part="RE10" gate="G$1" pin="3"/>
</segment>
<segment>
<wire x1="336.6135" y1="59.7535" x2="336.6135" y2="61.0235" width="0.1524" layer="91"/>
<pinref part="U$69" gate="GND" pin="GND"/>
<pinref part="RE15" gate="G$1" pin="3"/>
</segment>
<segment>
<pinref part="U$96" gate="GND" pin="GND"/>
<wire x1="144.78" y1="175.26" x2="144.78" y2="177.8" width="0.1524" layer="91" style="longdash"/>
<pinref part="SVORKA12" gate="G$1" pin="6"/>
<wire x1="144.78" y1="177.8" x2="147.32" y2="177.8" width="0.1524" layer="91" style="longdash"/>
</segment>
</net>
<net name="+24V" class="0">
<segment>
<wire x1="33.02" y1="38.1" x2="30.48" y2="38.1" width="0.1524" layer="91"/>
<wire x1="30.48" y1="38.1" x2="30.48" y2="40.64" width="0.1524" layer="91"/>
<pinref part="S2" gate="A" pin="S"/>
<pinref part="U$70" gate="+24V" pin="+24V"/>
</segment>
<segment>
<wire x1="139.573" y1="49.403" x2="142.748" y2="49.403" width="0.1524" layer="91"/>
<wire x1="142.748" y1="49.403" x2="142.748" y2="51.943" width="0.1524" layer="91"/>
<pinref part="S4" gate="A" pin="S"/>
<pinref part="U$87" gate="+24V" pin="+24V"/>
</segment>
</net>
<net name="FAIL1" class="0">
<segment>
<wire x1="367.7285" y1="126.4285" x2="371.5385" y2="126.4285" width="0.1524" layer="91"/>
<wire x1="371.5385" y1="126.4285" x2="371.5385" y2="97.2185" width="0.1524" layer="91"/>
<wire x1="346.7735" y1="126.4285" x2="348.6785" y2="126.4285" width="0.1524" layer="91"/>
<wire x1="348.6785" y1="126.4285" x2="348.6785" y2="97.2185" width="0.1524" layer="91"/>
<wire x1="327.7235" y1="97.2185" x2="348.6785" y2="97.2185" width="0.1524" layer="91"/>
<wire x1="348.6785" y1="97.2185" x2="371.5385" y2="97.2185" width="0.1524" layer="91"/>
<junction x="348.6785" y="97.2185"/>
<pinref part="RE10" gate="G$1" pin="SF"/>
<pinref part="RE9" gate="G$1" pin="SF"/>
<pinref part="RE7" gate="G$1" pin="SF"/>
<wire x1="304.2285" y1="126.4285" x2="306.1335" y2="126.4285" width="0.1524" layer="91"/>
<wire x1="306.1335" y1="126.4285" x2="306.1335" y2="97.2185" width="0.1524" layer="91"/>
<pinref part="RE6" gate="G$1" pin="SF"/>
<wire x1="283.9085" y1="126.4285" x2="285.8135" y2="126.4285" width="0.1524" layer="91"/>
<wire x1="285.8135" y1="126.4285" x2="285.8135" y2="97.2185" width="0.1524" layer="91"/>
<pinref part="RE5" gate="G$1" pin="SF"/>
<wire x1="262.9535" y1="126.4285" x2="264.8585" y2="126.4285" width="0.1524" layer="91"/>
<wire x1="264.8585" y1="126.4285" x2="264.8585" y2="97.2185" width="0.1524" layer="91"/>
<wire x1="264.8585" y1="97.2185" x2="285.8135" y2="97.2185" width="0.1524" layer="91"/>
<junction x="285.8135" y="97.2185"/>
<wire x1="285.8135" y1="97.2185" x2="306.1335" y2="97.2185" width="0.1524" layer="91"/>
<junction x="306.1335" y="97.2185"/>
<wire x1="306.1335" y1="97.2185" x2="327.7235" y2="97.2185" width="0.1524" layer="91"/>
<wire x1="327.7235" y1="126.4285" x2="327.7235" y2="97.2185" width="0.1524" layer="91"/>
<wire x1="325.1835" y1="126.4285" x2="327.7235" y2="126.4285" width="0.1524" layer="91"/>
<pinref part="RE8" gate="G$1" pin="SF"/>
<label x="263.5885" y="99.1235" size="1.4224" layer="95" rot="R90"/>
<junction x="327.7235" y="97.2185"/>
</segment>
<segment>
<wire x1="348.6785" y1="96.901" x2="348.6785" y2="73.7235" width="0.1524" layer="91"/>
<pinref part="RE14" gate="G$1" pin="SF"/>
<wire x1="348.6785" y1="73.7235" x2="348.6785" y2="44.5135" width="0.1524" layer="91"/>
<wire x1="325.1835" y1="73.7235" x2="327.7235" y2="73.7235" width="0.1524" layer="91"/>
<wire x1="327.7235" y1="73.7235" x2="327.7235" y2="44.5135" width="0.1524" layer="91"/>
<wire x1="327.7235" y1="44.5135" x2="348.6785" y2="44.5135" width="0.1524" layer="91"/>
<junction x="327.7235" y="44.5135"/>
<pinref part="RE13" gate="G$1" pin="SF"/>
<wire x1="304.2285" y1="73.7235" x2="306.1335" y2="73.7235" width="0.1524" layer="91"/>
<wire x1="306.1335" y1="73.7235" x2="306.1335" y2="44.5135" width="0.1524" layer="91"/>
<wire x1="306.1335" y1="44.5135" x2="327.7235" y2="44.5135" width="0.1524" layer="91"/>
<junction x="306.1335" y="44.5135"/>
<wire x1="285.8135" y1="44.5135" x2="306.1335" y2="44.5135" width="0.1524" layer="91"/>
<pinref part="RE12" gate="G$1" pin="SF"/>
<wire x1="283.9085" y1="73.7235" x2="285.8135" y2="73.7235" width="0.1524" layer="91"/>
<wire x1="285.8135" y1="73.7235" x2="285.8135" y2="44.5135" width="0.1524" layer="91"/>
<junction x="285.8135" y="44.5135"/>
<wire x1="264.8585" y1="44.5135" x2="285.8135" y2="44.5135" width="0.1524" layer="91"/>
<pinref part="RE11" gate="G$1" pin="SF"/>
<wire x1="262.9535" y1="73.7235" x2="264.8585" y2="73.7235" width="0.1524" layer="91"/>
<wire x1="264.8585" y1="73.7235" x2="264.8585" y2="44.5135" width="0.1524" layer="91"/>
<junction x="264.8585" y="44.5135"/>
<label x="254" y="40.64" size="1.9304" layer="95"/>
<pinref part="RE15" gate="G$1" pin="SF"/>
<wire x1="346.7735" y1="73.7235" x2="348.6785" y2="73.7235" width="0.1524" layer="91"/>
<junction x="348.6785" y="73.7235"/>
<wire x1="264.8585" y1="44.5135" x2="264.8585" y2="40.64" width="0.1524" layer="91"/>
<wire x1="264.8585" y1="40.64" x2="228.6" y2="40.64" width="0.1524" layer="91"/>
<label x="228.6" y="40.64" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="VENTCOOL2" class="0">
<segment>
<wire x1="325.8185" y1="76.2635" x2="325.1835" y2="76.2635" width="0.1524" layer="91"/>
<wire x1="325.8185" y1="86.4235" x2="325.8185" y2="76.2635" width="0.1524" layer="91"/>
<wire x1="304.8635" y1="86.4235" x2="325.8185" y2="86.4235" width="0.1524" layer="91"/>
<wire x1="304.2285" y1="76.2635" x2="304.8635" y2="76.2635" width="0.1524" layer="91"/>
<wire x1="304.8635" y1="76.2635" x2="304.8635" y2="86.4235" width="0.1524" layer="91"/>
<wire x1="262.9535" y1="76.2635" x2="264.8585" y2="76.2635" width="0.1524" layer="91"/>
<wire x1="264.8585" y1="76.2635" x2="264.8585" y2="86.4235" width="0.1524" layer="91"/>
<wire x1="264.8585" y1="86.4235" x2="284.5435" y2="86.4235" width="0.1524" layer="91"/>
<wire x1="284.5435" y1="86.4235" x2="304.8635" y2="86.4235" width="0.1524" layer="91"/>
<wire x1="284.5435" y1="76.2635" x2="284.5435" y2="86.4235" width="0.1524" layer="91"/>
<wire x1="283.9085" y1="76.2635" x2="284.5435" y2="76.2635" width="0.1524" layer="91"/>
<wire x1="236.22" y1="86.36" x2="264.8585" y2="86.4235" width="0.1524" layer="91"/>
<junction x="304.8635" y="86.4235"/>
<junction x="264.8585" y="86.4235"/>
<junction x="284.5435" y="86.4235"/>
<pinref part="RE14" gate="G$1" pin="IN"/>
<pinref part="RE13" gate="G$1" pin="IN"/>
<pinref part="RE11" gate="G$1" pin="IN"/>
<pinref part="RE12" gate="G$1" pin="IN"/>
<wire x1="346.7735" y1="76.2635" x2="347.4085" y2="76.2635" width="0.1524" layer="91"/>
<wire x1="347.4085" y1="76.2635" x2="347.4085" y2="86.4235" width="0.1524" layer="91"/>
<wire x1="325.8185" y1="86.4235" x2="347.4085" y2="86.4235" width="0.1524" layer="91"/>
<pinref part="RE15" gate="G$1" pin="IN"/>
<junction x="325.8185" y="86.4235"/>
<wire x1="236.22" y1="86.36" x2="200.66" y2="86.36" width="0.1524" layer="91"/>
<pinref part="SVORKA19" gate="G$1" pin="1"/>
<wire x1="167.64" y1="132.08" x2="200.66" y2="132.08" width="0.1524" layer="91"/>
<wire x1="200.66" y1="86.36" x2="200.66" y2="132.08" width="0.1524" layer="91"/>
<label x="170.18" y="132.08" size="1.778" layer="95"/>
<label x="226.06" y="86.36" size="1.778" layer="95"/>
</segment>
</net>
<net name="VENTCOOL1" class="0">
<segment>
<wire x1="325.8185" y1="128.9685" x2="325.1835" y2="128.9685" width="0.1524" layer="91"/>
<wire x1="325.8185" y1="139.1285" x2="325.8185" y2="128.9685" width="0.1524" layer="91"/>
<wire x1="304.8635" y1="139.1285" x2="325.8185" y2="139.1285" width="0.1524" layer="91"/>
<wire x1="304.2285" y1="128.9685" x2="304.8635" y2="128.9685" width="0.1524" layer="91"/>
<wire x1="304.8635" y1="128.9685" x2="304.8635" y2="139.1285" width="0.1524" layer="91"/>
<wire x1="284.5435" y1="139.1285" x2="304.8635" y2="139.1285" width="0.1524" layer="91"/>
<wire x1="283.9085" y1="128.9685" x2="284.5435" y2="128.9685" width="0.1524" layer="91"/>
<wire x1="284.5435" y1="128.9685" x2="284.5435" y2="139.1285" width="0.1524" layer="91"/>
<wire x1="262.9535" y1="128.9685" x2="264.8585" y2="128.9685" width="0.1524" layer="91"/>
<wire x1="264.8585" y1="128.9685" x2="264.8585" y2="139.1285" width="0.1524" layer="91"/>
<wire x1="264.8585" y1="139.1285" x2="284.5435" y2="139.1285" width="0.1524" layer="91"/>
<junction x="304.8635" y="139.1285"/>
<junction x="284.5435" y="139.1285"/>
<junction x="264.8585" y="139.1285"/>
<pinref part="RE8" gate="G$1" pin="IN"/>
<pinref part="RE7" gate="G$1" pin="IN"/>
<pinref part="RE6" gate="G$1" pin="IN"/>
<pinref part="RE5" gate="G$1" pin="IN"/>
<wire x1="368.9985" y1="128.9685" x2="367.7285" y2="128.9685" width="0.1524" layer="91"/>
<wire x1="368.9985" y1="139.1285" x2="368.9985" y2="128.9685" width="0.1524" layer="91"/>
<wire x1="347.4085" y1="139.1285" x2="368.9985" y2="139.1285" width="0.1524" layer="91"/>
<wire x1="346.7735" y1="128.9685" x2="347.4085" y2="128.9685" width="0.1524" layer="91"/>
<wire x1="347.4085" y1="128.9685" x2="347.4085" y2="139.1285" width="0.1524" layer="91"/>
<wire x1="325.8185" y1="139.1285" x2="347.4085" y2="139.1285" width="0.1524" layer="91"/>
<junction x="347.4085" y="139.1285"/>
<pinref part="RE10" gate="G$1" pin="IN"/>
<pinref part="RE9" gate="G$1" pin="IN"/>
<junction x="325.8185" y="139.1285"/>
<wire x1="264.8585" y1="139.1285" x2="213.36" y2="139.1285" width="0.1524" layer="91"/>
<pinref part="SVORKA44" gate="G$1" pin="3"/>
<wire x1="162.56" y1="162.56" x2="213.36" y2="162.56" width="0.1524" layer="91"/>
<wire x1="213.36" y1="162.56" x2="213.36" y2="139.1285" width="0.1524" layer="91"/>
<label x="170.18" y="162.56" size="1.778" layer="95"/>
<label x="226.06" y="139.7" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<wire x1="247.7135" y1="136.5885" x2="247.7135" y2="143.5735" width="0.1524" layer="91"/>
<wire x1="268.6685" y1="136.5885" x2="268.6685" y2="143.5735" width="0.1524" layer="91"/>
<wire x1="288.9885" y1="136.5885" x2="288.9885" y2="143.5735" width="0.1524" layer="91"/>
<wire x1="288.9885" y1="143.5735" x2="309.9435" y2="143.5735" width="0.1524" layer="91"/>
<wire x1="309.9435" y1="136.5885" x2="309.9435" y2="143.5735" width="0.1524" layer="91"/>
<wire x1="247.7135" y1="143.5735" x2="268.6685" y2="143.5735" width="0.1524" layer="91"/>
<junction x="268.6685" y="143.5735"/>
<wire x1="268.6685" y1="143.5735" x2="288.9885" y2="143.5735" width="0.1524" layer="91"/>
<junction x="288.9885" y="143.5735"/>
<junction x="247.7135" y="143.5735"/>
<pinref part="RE5" gate="G$1" pin="1"/>
<pinref part="RE6" gate="G$1" pin="1"/>
<pinref part="RE7" gate="G$1" pin="1"/>
<pinref part="RE8" gate="G$1" pin="1"/>
<wire x1="331.5335" y1="136.5885" x2="331.5335" y2="143.5735" width="0.1524" layer="91"/>
<wire x1="331.5335" y1="143.5735" x2="352.4885" y2="143.5735" width="0.1524" layer="91"/>
<wire x1="352.4885" y1="136.5885" x2="352.4885" y2="143.5735" width="0.1524" layer="91"/>
<wire x1="309.9435" y1="143.5735" x2="331.5335" y2="143.5735" width="0.1524" layer="91"/>
<junction x="331.5335" y="143.5735"/>
<pinref part="RE9" gate="G$1" pin="1"/>
<pinref part="RE10" gate="G$1" pin="1"/>
<junction x="309.9435" y="143.5735"/>
<wire x1="247.7135" y1="143.5735" x2="240.0935" y2="143.5735" width="0.1524" layer="91"/>
<wire x1="240.0935" y1="143.5735" x2="240.0935" y2="145.161" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$28" class="0">
<segment>
<wire x1="247.7135" y1="83.8835" x2="247.7135" y2="90.8685" width="0.1524" layer="91"/>
<wire x1="268.6685" y1="83.8835" x2="268.6685" y2="90.8685" width="0.1524" layer="91"/>
<wire x1="288.9885" y1="83.8835" x2="288.9885" y2="90.8685" width="0.1524" layer="91"/>
<wire x1="288.9885" y1="90.8685" x2="309.9435" y2="90.8685" width="0.1524" layer="91"/>
<wire x1="309.9435" y1="83.8835" x2="309.9435" y2="90.8685" width="0.1524" layer="91"/>
<wire x1="239.4585" y1="90.8685" x2="247.7135" y2="90.8685" width="0.1524" layer="91"/>
<wire x1="247.7135" y1="90.8685" x2="268.6685" y2="90.8685" width="0.1524" layer="91"/>
<junction x="268.6685" y="90.8685"/>
<wire x1="268.6685" y1="90.8685" x2="288.9885" y2="90.8685" width="0.1524" layer="91"/>
<junction x="288.9885" y="90.8685"/>
<junction x="247.7135" y="90.8685"/>
<pinref part="RE11" gate="G$1" pin="1"/>
<pinref part="RE12" gate="G$1" pin="1"/>
<pinref part="RE13" gate="G$1" pin="1"/>
<pinref part="RE14" gate="G$1" pin="1"/>
<wire x1="331.5335" y1="83.8835" x2="331.5335" y2="90.8685" width="0.1524" layer="91"/>
<wire x1="309.9435" y1="90.8685" x2="331.5335" y2="90.8685" width="0.1524" layer="91"/>
<pinref part="RE15" gate="G$1" pin="1"/>
<junction x="309.9435" y="90.8685"/>
<wire x1="239.4585" y1="93.091" x2="239.4585" y2="90.8685" width="0.1524" layer="91"/>
</segment>
</net>
<net name="AKU" class="0">
<segment>
<wire x1="59.69" y1="27.94" x2="59.69" y2="25.4" width="0.1524" layer="91"/>
<wire x1="43.815" y1="44.45" x2="43.815" y2="38.1" width="0.1524" layer="91"/>
<wire x1="43.18" y1="38.1" x2="43.815" y2="38.1" width="0.1524" layer="91"/>
<wire x1="43.815" y1="38.1" x2="43.815" y2="25.4" width="0.1524" layer="91"/>
<wire x1="43.815" y1="25.4" x2="52.07" y2="25.4" width="0.1524" layer="91"/>
<wire x1="52.07" y1="25.4" x2="59.69" y2="25.4" width="0.1524" layer="91"/>
<wire x1="40.005" y1="25.4" x2="43.815" y2="25.4" width="0.1524" layer="91"/>
<wire x1="52.07" y1="29.845" x2="52.07" y2="25.4" width="0.1524" layer="91"/>
<junction x="43.815" y="38.1"/>
<junction x="43.815" y="25.4"/>
<junction x="52.07" y="25.4"/>
<pinref part="RE4A" gate="G$1" pin="2"/>
<pinref part="U$71" gate="G$1" pin="AKU"/>
<pinref part="S2" gate="A" pin="P"/>
<pinref part="S1A" gate="A" pin="P"/>
<pinref part="S3" gate="A" pin="S"/>
</segment>
<segment>
<wire x1="151.003" y1="27.813" x2="151.003" y2="25.273" width="0.1524" layer="91"/>
<wire x1="127.508" y1="53.213" x2="127.508" y2="49.403" width="0.1524" layer="91"/>
<wire x1="129.413" y1="49.403" x2="127.508" y2="49.403" width="0.1524" layer="91"/>
<wire x1="127.508" y1="49.403" x2="127.508" y2="25.273" width="0.1524" layer="91"/>
<wire x1="127.508" y1="25.273" x2="151.003" y2="25.273" width="0.1524" layer="91"/>
<junction x="127.508" y="49.403"/>
<pinref part="RE4A1" gate="G$1" pin="2"/>
<pinref part="U$90" gate="G$1" pin="AKU"/>
<pinref part="S4" gate="A" pin="P"/>
</segment>
</net>
<net name="CLAMP+" class="0">
<segment>
<wire x1="68.58" y1="43.18" x2="68.58" y2="55.88" width="0.1524" layer="91"/>
<pinref part="RE4" gate="G$1" pin="2"/>
<wire x1="68.58" y1="55.88" x2="83.82" y2="55.88" width="0.1524" layer="91"/>
<label x="83.82" y="55.88" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="160.02" y1="43.18" x2="160.02" y2="55.88" width="0.1524" layer="91"/>
<pinref part="RE22" gate="G$1" pin="2"/>
<wire x1="160.02" y1="55.88" x2="177.8" y2="55.88" width="0.1524" layer="91"/>
<label x="177.8" y="55.88" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="CLAMP-" class="0">
<segment>
<wire x1="76.2" y1="25.4" x2="68.58" y2="25.4" width="0.1524" layer="91"/>
<wire x1="68.58" y1="25.4" x2="68.58" y2="27.94" width="0.1524" layer="91"/>
<wire x1="76.2" y1="48.26" x2="76.2" y2="25.4" width="0.1524" layer="91"/>
<pinref part="RE4" gate="G$1" pin="1"/>
<wire x1="76.2" y1="48.26" x2="83.82" y2="48.26" width="0.1524" layer="91"/>
<label x="83.82" y="48.26" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="170.18" y1="25.4" x2="160.02" y2="25.4" width="0.1524" layer="91"/>
<wire x1="160.02" y1="25.4" x2="160.02" y2="27.94" width="0.1524" layer="91"/>
<wire x1="170.18" y1="48.26" x2="170.18" y2="25.4" width="0.1524" layer="91"/>
<pinref part="RE22" gate="G$1" pin="1"/>
<wire x1="170.18" y1="48.26" x2="177.8" y2="48.26" width="0.1524" layer="91"/>
<label x="177.8" y="48.26" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<wire x1="27.305" y1="27.94" x2="27.305" y2="25.4" width="0.1524" layer="91"/>
<wire x1="27.305" y1="25.4" x2="29.845" y2="25.4" width="0.1524" layer="91"/>
<pinref part="S1A" gate="A" pin="S"/>
</segment>
</net>
<net name="AKUAUTO" class="0">
<segment>
<wire x1="59.69" y1="68.58" x2="63.5" y2="71.12" width="0.1524" layer="91"/>
<wire x1="59.69" y1="68.58" x2="59.69" y2="43.815" width="0.1524" layer="91"/>
<wire x1="59.69" y1="43.815" x2="59.69" y2="43.18" width="0.1524" layer="91"/>
<wire x1="52.07" y1="43.815" x2="59.69" y2="43.815" width="0.1524" layer="91"/>
<wire x1="52.07" y1="40.005" x2="52.07" y2="43.815" width="0.1524" layer="91"/>
<wire x1="52.07" y1="43.815" x2="52.07" y2="50.165" width="0.1524" layer="91"/>
<junction x="59.69" y="43.815"/>
<junction x="52.07" y="43.815"/>
<pinref part="RE4A" gate="G$1" pin="1"/>
<pinref part="S3" gate="A" pin="P"/>
<label x="68.58" y="71.12" size="1.27" layer="95" xref="yes"/>
<wire x1="68.58" y1="71.12" x2="63.5" y2="71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VENTCOOL0" class="0">
<segment>
<pinref part="SVORKA19" gate="G$1" pin="5"/>
<wire x1="149.86" y1="134.62" x2="129.54" y2="134.62" width="0.1524" layer="91"/>
<label x="129.54" y="134.62" size="1.27" layer="95" rot="MR0" xref="yes"/>
</segment>
</net>
<net name="VC2" class="0">
<segment>
<wire x1="93.98" y1="137.16" x2="96.52" y2="134.62" width="0.1524" layer="91"/>
<wire x1="96.52" y1="134.62" x2="96.52" y2="132.08" width="0.1524" layer="91"/>
<pinref part="SVORKA19" gate="G$1" pin="6"/>
<wire x1="147.32" y1="132.08" x2="96.52" y2="132.08" width="0.1524" layer="91"/>
<label x="144.78" y="132.08" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="VC0" class="0">
<segment>
<wire x1="93.98" y1="137.16" x2="96.52" y2="139.7" width="0.1524" layer="91" style="longdash"/>
<wire x1="96.52" y1="139.7" x2="96.52" y2="142.24" width="0.1524" layer="91" style="longdash"/>
<pinref part="SVORKA19" gate="G$1" pin="3"/>
<wire x1="162.56" y1="137.16" x2="187.96" y2="137.16" width="0.1524" layer="91"/>
<wire x1="187.96" y1="137.16" x2="187.96" y2="142.24" width="0.1524" layer="91"/>
<wire x1="187.96" y1="142.24" x2="99.06" y2="142.24" width="0.1524" layer="91"/>
<label x="170.18" y="137.16" size="1.778" layer="95"/>
<wire x1="96.52" y1="142.24" x2="99.06" y2="142.24" width="0.1524" layer="91" style="longdash"/>
<label x="144.78" y="142.24" size="1.778" layer="95" rot="MR0"/>
</segment>
<segment>
<wire x1="93.98" y1="157.48" x2="96.52" y2="154.94" width="0.1524" layer="91" style="longdash"/>
<wire x1="96.52" y1="154.94" x2="96.52" y2="152.4" width="0.1524" layer="91" style="longdash"/>
<pinref part="SVORKA19" gate="G$1" pin="2"/>
<wire x1="165.1" y1="134.62" x2="190.5" y2="134.62" width="0.1524" layer="91"/>
<wire x1="190.5" y1="134.62" x2="190.5" y2="152.4" width="0.1524" layer="91"/>
<wire x1="190.5" y1="152.4" x2="99.06" y2="152.4" width="0.1524" layer="91"/>
<label x="170.18" y="134.62" size="1.778" layer="95"/>
<wire x1="96.52" y1="152.4" x2="99.06" y2="152.4" width="0.1524" layer="91" style="longdash"/>
<label x="144.78" y="152.4" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="VC1" class="0">
<segment>
<wire x1="93.98" y1="157.48" x2="96.52" y2="160.02" width="0.1524" layer="91"/>
<wire x1="96.52" y1="160.02" x2="96.52" y2="162.56" width="0.1524" layer="91"/>
<pinref part="SVORKA44" gate="G$1" pin="4"/>
<wire x1="152.4" y1="162.56" x2="96.52" y2="162.56" width="0.1524" layer="91"/>
<label x="144.78" y="162.56" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="COMPRESSOR" class="0">
<segment>
<pinref part="SVORKA46" gate="G$1" pin="6"/>
<wire x1="147.32" y1="200.66" x2="124.46" y2="200.66" width="0.1524" layer="91"/>
<label x="124.46" y="200.66" size="1.27" layer="95" rot="MR0" xref="yes"/>
</segment>
</net>
<net name="COOLSTART" class="0">
<segment>
<pinref part="SVORKA44" gate="G$1" pin="1"/>
<wire x1="167.64" y1="157.48" x2="170.18" y2="157.48" width="0.1524" layer="91"/>
<label x="170.18" y="157.48" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="HL/SWITCH2" class="0">
<segment>
<pinref part="SVORKA44" gate="G$1" pin="2"/>
<wire x1="165.1" y1="160.02" x2="170.18" y2="160.02" width="0.1524" layer="91"/>
<label x="170.18" y="160.02" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="106.68" y1="175.26" x2="109.22" y2="177.8" width="0.1524" layer="91"/>
<wire x1="109.22" y1="177.8" x2="121.92" y2="177.8" width="0.1524" layer="91"/>
<wire x1="121.92" y1="177.8" x2="121.92" y2="160.02" width="0.1524" layer="91"/>
<pinref part="SVORKA44" gate="G$1" pin="5"/>
<wire x1="121.92" y1="160.02" x2="149.86" y2="160.02" width="0.1524" layer="91"/>
<label x="144.78" y="160.02" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="GNDSOLE" class="0">
<segment>
<pinref part="SVORKA12" gate="G$1" pin="2"/>
<wire x1="165.1" y1="180.34" x2="187.96" y2="180.34" width="0.1524" layer="91"/>
<wire x1="187.96" y1="180.34" x2="187.96" y2="190.5" width="0.1524" layer="91"/>
<wire x1="190.5" y1="193.04" x2="187.96" y2="190.5" width="0.1524" layer="91"/>
<label x="170.18" y="180.34" size="1.778" layer="95"/>
</segment>
</net>
<net name="SOLE" class="0">
<segment>
<wire x1="190.5" y1="193.04" x2="187.96" y2="195.58" width="0.1524" layer="91"/>
<wire x1="187.96" y1="195.58" x2="187.96" y2="200.66" width="0.1524" layer="91"/>
<pinref part="SVORKA46" gate="G$1" pin="1"/>
<wire x1="187.96" y1="200.66" x2="167.64" y2="200.66" width="0.1524" layer="91"/>
<label x="170.18" y="200.66" size="1.778" layer="95"/>
</segment>
</net>
<net name="GNDCLUTCH" class="0">
<segment>
<wire x1="195.58" y1="187.96" x2="193.04" y2="185.42" width="0.1524" layer="91"/>
<wire x1="193.04" y1="185.42" x2="193.04" y2="177.8" width="0.1524" layer="91"/>
<pinref part="SVORKA12" gate="G$1" pin="1"/>
<wire x1="193.04" y1="177.8" x2="167.64" y2="177.8" width="0.1524" layer="91"/>
<label x="170.18" y="177.8" size="1.778" layer="95"/>
</segment>
</net>
<net name="CLUTCH" class="0">
<segment>
<wire x1="195.58" y1="187.96" x2="193.04" y2="190.5" width="0.1524" layer="91"/>
<pinref part="SVORKA46" gate="G$1" pin="2"/>
<wire x1="193.04" y1="190.5" x2="193.04" y2="203.2" width="0.1524" layer="91"/>
<wire x1="193.04" y1="203.2" x2="165.1" y2="203.2" width="0.1524" layer="91"/>
<label x="170.18" y="203.2" size="1.778" layer="95"/>
</segment>
</net>
<net name="GNDRELIEF" class="0">
<segment>
<wire x1="127" y1="190.5" x2="129.54" y2="187.96" width="0.1524" layer="91"/>
<wire x1="129.54" y1="187.96" x2="129.54" y2="182.88" width="0.1524" layer="91"/>
<pinref part="SVORKA12" gate="G$1" pin="4"/>
<wire x1="129.54" y1="182.88" x2="152.4" y2="182.88" width="0.1524" layer="91"/>
<label x="144.78" y="182.88" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="RELIEF" class="0">
<segment>
<wire x1="127" y1="190.5" x2="129.54" y2="193.04" width="0.1524" layer="91"/>
<wire x1="129.54" y1="193.04" x2="129.54" y2="205.74" width="0.1524" layer="91"/>
<pinref part="SVORKA46" gate="G$1" pin="4"/>
<wire x1="129.54" y1="205.74" x2="152.4" y2="205.74" width="0.1524" layer="91"/>
<label x="144.78" y="205.74" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="STARTACCU" class="0">
<segment>
<wire x1="152.4" y1="71.12" x2="151.003" y2="67.818" width="0.1524" layer="91"/>
<wire x1="151.003" y1="43.053" x2="151.003" y2="44.958" width="0.1524" layer="91"/>
<wire x1="151.003" y1="67.818" x2="151.003" y2="44.958" width="0.1524" layer="91"/>
<wire x1="151.003" y1="44.958" x2="149.733" y2="44.958" width="0.1524" layer="91"/>
<junction x="151.003" y="44.958"/>
<pinref part="RE4A1" gate="G$1" pin="1"/>
<label x="160.02" y="71.12" size="1.27" layer="95" xref="yes"/>
<wire x1="160.02" y1="71.12" x2="152.4" y2="71.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="!ELMOT" class="0">
<segment>
<pinref part="SVORKA2" gate="G$1" pin="2"/>
<wire x1="165.1" y1="233.68" x2="195.58" y2="233.68" width="0.1524" layer="91"/>
<label x="195.58" y="233.68" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="PSOK" class="0">
<segment>
<pinref part="SVORKA2" gate="G$1" pin="1"/>
<wire x1="167.64" y1="231.14" x2="195.58" y2="231.14" width="0.1524" layer="91"/>
<label x="195.58" y="231.14" size="1.27" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="127" y1="233.68" x2="129.54" y2="231.14" width="0.1524" layer="91"/>
<pinref part="SVORKA2" gate="G$1" pin="6"/>
<wire x1="129.54" y1="231.14" x2="147.32" y2="231.14" width="0.1524" layer="91"/>
<label x="144.78" y="231.14" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="HP4-" class="0">
<segment>
<wire x1="127" y1="218.44" x2="129.54" y2="215.9" width="0.1524" layer="91"/>
<wire x1="129.54" y1="215.9" x2="182.88" y2="215.9" width="0.1524" layer="91"/>
<wire x1="182.88" y1="215.9" x2="182.88" y2="205.74" width="0.1524" layer="91"/>
<pinref part="SVORKA46" gate="G$1" pin="3"/>
<wire x1="182.88" y1="205.74" x2="162.56" y2="205.74" width="0.1524" layer="91"/>
<label x="170.18" y="205.74" size="1.778" layer="95"/>
<label x="144.78" y="215.9" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="HP4+" class="0">
<segment>
<wire x1="127" y1="218.44" x2="129.54" y2="220.98" width="0.1524" layer="91"/>
<wire x1="129.54" y1="220.98" x2="182.88" y2="220.98" width="0.1524" layer="91"/>
<wire x1="182.88" y1="220.98" x2="182.88" y2="236.22" width="0.1524" layer="91"/>
<pinref part="SVORKA2" gate="G$1" pin="3"/>
<wire x1="182.88" y1="236.22" x2="162.56" y2="236.22" width="0.1524" layer="91"/>
<label x="170.18" y="236.22" size="1.778" layer="95"/>
<label x="144.78" y="220.98" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="HP4" class="0">
<segment>
<wire x1="127" y1="233.68" x2="129.54" y2="236.22" width="0.1524" layer="91"/>
<pinref part="SVORKA2" gate="G$1" pin="4"/>
<wire x1="129.54" y1="236.22" x2="152.4" y2="236.22" width="0.1524" layer="91"/>
<label x="144.78" y="236.22" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="ELMOT" class="0">
<segment>
<pinref part="SVORKA2" gate="G$1" pin="5"/>
<wire x1="127" y1="233.68" x2="149.86" y2="233.68" width="0.1524" layer="91"/>
<label x="144.78" y="233.68" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="HL/SWITCH1" class="0">
<segment>
<wire x1="106.68" y1="175.26" x2="109.22" y2="172.72" width="0.1524" layer="91"/>
<wire x1="109.22" y1="172.72" x2="116.84" y2="172.72" width="0.1524" layer="91"/>
<wire x1="116.84" y1="172.72" x2="116.84" y2="157.48" width="0.1524" layer="91"/>
<wire x1="116.84" y1="157.48" x2="147.32" y2="157.48" width="0.1524" layer="91"/>
<pinref part="SVORKA44" gate="G$1" pin="6"/>
<label x="144.78" y="157.48" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<description>Autostart KUBOTA</description>
<plain>
<text x="274.32" y="167.64" size="1.778" layer="94" rot="R270">Twisted pair</text>
<text x="360.68" y="190.5" size="1.778" layer="91">GND TMC</text>
<text x="129.54" y="76.2" size="1.778" layer="94" align="bottom-right">Twisted pair</text>
<text x="78.74" y="88.9" size="1.778" layer="94" align="top-center">Dořešit dimenzování svorek</text>
<text x="15.24" y="20.32" size="1.778" layer="94">MCF-5</text>
<text x="15.24" y="99.06" size="1.778" layer="94">Jistič dieselu</text>
<wire x1="12.7" y1="86.36" x2="12.7" y2="91.44" width="0.1524" layer="94"/>
<wire x1="12.7" y1="91.44" x2="15.24" y2="88.9" width="0.1524" layer="94"/>
<wire x1="10.16" y1="86.36" x2="10.16" y2="88.9" width="0.1524" layer="94"/>
<wire x1="10.16" y1="88.9" x2="12.7" y2="91.44" width="0.1524" layer="94"/>
<wire x1="15.24" y1="88.9" x2="17.78" y2="88.9" width="0.1524" layer="94"/>
<wire x1="83.82" y1="93.98" x2="86.36" y2="93.98" width="0.1524" layer="94"/>
<wire x1="83.82" y1="93.98" x2="86.36" y2="91.44" width="0.1524" layer="94"/>
<wire x1="50.8" y1="99.06" x2="50.8" y2="86.36" width="0.1524" layer="94" style="shortdash"/>
<wire x1="50.8" y1="86.36" x2="99.06" y2="86.36" width="0.1524" layer="94" style="shortdash"/>
<wire x1="99.06" y1="86.36" x2="99.06" y2="99.06" width="0.1524" layer="94" style="shortdash"/>
<wire x1="99.06" y1="99.06" x2="50.8" y2="99.06" width="0.1524" layer="94" style="shortdash"/>
<wire x1="12.7" y1="93.98" x2="12.7" y2="91.44" width="0.1524" layer="94"/>
<text x="266.7" y="259.08" size="1.778" layer="94" align="center">Ovládání otáček dieselu</text>
<wire x1="63.5" y1="124.46" x2="0" y2="124.46" width="0.1524" layer="94" style="longdash"/>
<wire x1="68.58" y1="124.46" x2="71.12" y2="124.46" width="0.1524" layer="94" style="longdash"/>
<wire x1="76.2" y1="124.46" x2="124.46" y2="124.46" width="0.1524" layer="94" style="longdash"/>
<wire x1="124.46" y1="124.46" x2="124.46" y2="152.4" width="0.1524" layer="94" style="longdash"/>
<wire x1="124.46" y1="152.4" x2="193.04" y2="152.4" width="0.1524" layer="94" style="longdash"/>
<wire x1="193.04" y1="152.4" x2="193.04" y2="180.34" width="0.1524" layer="94" style="longdash"/>
<wire x1="193.04" y1="180.34" x2="198.12" y2="180.34" width="0.1524" layer="94" style="longdash"/>
<wire x1="203.2" y1="180.34" x2="210.82" y2="180.34" width="0.1524" layer="94" style="longdash"/>
<wire x1="215.9" y1="180.34" x2="218.44" y2="180.34" width="0.1524" layer="94" style="longdash"/>
<wire x1="218.44" y1="180.34" x2="269.24" y2="231.14" width="0.1524" layer="94" style="longdash"/>
<wire x1="269.24" y1="231.14" x2="294.64" y2="231.14" width="0.1524" layer="94" style="longdash"/>
<wire x1="294.64" y1="231.14" x2="294.64" y2="246.38" width="0.1524" layer="94" style="longdash"/>
<wire x1="294.64" y1="251.46" x2="294.64" y2="264.16" width="0.1524" layer="94" style="longdash"/>
<text x="276.86" y="228.6" size="1.778" layer="94">Rozvaděč</text>
<text x="276.86" y="233.68" size="1.778" layer="94" align="top-left">Strojovna</text>
<text x="25.4" y="121.92" size="1.778" layer="94">Rozvaděč</text>
<text x="25.4" y="127" size="1.778" layer="94" align="top-left">Strojovna</text>
<text x="175.26" y="30.48" size="1.778" layer="94" align="center">TMC</text>
<wire x1="152.4" y1="48.26" x2="180.34" y2="48.26" width="0.1524" layer="94" style="shortdash"/>
<wire x1="180.34" y1="48.26" x2="180.34" y2="12.7" width="0.1524" layer="94" style="shortdash"/>
<wire x1="180.34" y1="12.7" x2="152.4" y2="12.7" width="0.1524" layer="94" style="shortdash"/>
<wire x1="152.4" y1="12.7" x2="152.4" y2="48.26" width="0.1524" layer="94" style="shortdash"/>
<wire x1="274.32" y1="167.64" x2="271.78" y2="170.18" width="0.1524" layer="94"/>
<wire x1="265.43" y1="171.45" x2="265.43" y2="168.91" width="0.1524" layer="94"/>
<wire x1="265.43" y1="168.91" x2="270.51" y2="168.91" width="0.1524" layer="94"/>
<wire x1="270.51" y1="168.91" x2="270.51" y2="170.18" width="0.1524" layer="94"/>
<wire x1="270.51" y1="170.18" x2="270.51" y2="171.45" width="0.1524" layer="94"/>
<wire x1="270.51" y1="171.45" x2="265.43" y2="171.45" width="0.1524" layer="94"/>
<wire x1="271.78" y1="170.18" x2="270.51" y2="170.18" width="0.1524" layer="94"/>
<wire x1="129.54" y1="76.2" x2="132.08" y2="73.66" width="0.1524" layer="94"/>
<wire x1="132.08" y1="73.66" x2="132.08" y2="72.39" width="0.1524" layer="94"/>
<wire x1="132.08" y1="72.39" x2="130.81" y2="72.39" width="0.1524" layer="94"/>
<wire x1="130.81" y1="72.39" x2="130.81" y2="67.31" width="0.1524" layer="94"/>
<wire x1="130.81" y1="67.31" x2="133.35" y2="67.31" width="0.1524" layer="94"/>
<wire x1="133.35" y1="67.31" x2="133.35" y2="72.39" width="0.1524" layer="94"/>
<wire x1="133.35" y1="72.39" x2="132.08" y2="72.39" width="0.1524" layer="94"/>
<text x="109.22" y="66.04" size="1.778" layer="94" align="center">NC</text>
<text x="299.72" y="73.66" size="1.778" layer="94">Vysvětlení prefixů:
MCF - vodiče jdoucí na MCflex
KUB - vodiče jdoucí na motor KUBOTA
AS - vodiče jdoucí na desku Autostartu

Přerušené popsané vodiče jsou propojeny v rámci listu
Vodiče s vlaječkou pokračují na jiných listech</text>
<text x="139.7" y="58.42" size="1.778" layer="94" rot="R90" align="bottom-center">CAN-bus cable</text>
<text x="256.54" y="27.94" size="1.778" layer="94" rot="R90" align="top-center">CAN-bus cable</text>
<text x="254" y="129.54" size="1.778" layer="94" rot="R90" align="bottom-center">CAN-bus cable</text>
<text x="236.22" y="259.08" size="1.778" layer="94" align="center">Měření otáček dieselu</text>
<text x="205.74" y="259.08" size="1.778" layer="94" align="center">Palivové čerpadlo dieselu</text>
<wire x1="-5.08" y1="266.7" x2="391.16" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="396.24" y1="266.7" x2="-7.62" y2="0" width="0.1524" layer="94"/>
<text x="114.3" y="271.78" size="6.4516" layer="94">Použít aktualizované schéma z BLOKU-...</text>
</plain>
<instances>
<instance part="FRAME2" gate="G$1" x="0" y="0"/>
<instance part="FRAME2" gate="G$2" x="287.02" y="0"/>
<instance part="U$1" gate="G$1" x="12.7" y="99.06" smashed="yes" rot="R90">
<attribute name="VALUE" x="7.62" y="99.06" size="1.778" layer="94" align="bottom-center"/>
</instance>
<instance part="U$94" gate="+24V" x="370.84" y="43.18"/>
<instance part="U$10" gate="GND" x="378.46" y="40.64"/>
<instance part="U$36" gate="G$1" x="363.22" y="40.64" rot="MR0"/>
<instance part="U$4" gate="+24V" x="12.7" y="109.22"/>
<instance part="PRUCHODKA3" gate="G$1" x="66.04" y="124.46" rot="R90"/>
<instance part="DIESEL1" gate="G$1" x="2.54" y="160.02"/>
<instance part="DIESEL1" gate="G$2" x="205.74" y="233.68"/>
<instance part="DIESEL1" gate="G$3" x="236.22" y="248.92"/>
<instance part="DPS1" gate="G$1" x="320.04" y="165.1"/>
<instance part="MCFLEX1" gate="G$1" x="68.58" y="45.72"/>
<instance part="U$9" gate="GND" x="22.86" y="15.24"/>
<instance part="U$11" gate="G$1" x="236.22" y="83.82"/>
<instance part="U$12" gate="G$1" x="223.52" y="83.82"/>
<instance part="U$3" gate="GND" x="365.76" y="185.42" rot="R90"/>
<instance part="SVORKA23" gate="G$1" x="160.02" y="83.82"/>
<instance part="TMC1" gate="17" x="170.18" y="15.24" rot="R90"/>
<instance part="TMC1" gate="19" x="157.48" y="15.24" rot="R90"/>
<instance part="TMC1" gate="47" x="160.02" y="15.24" rot="R90"/>
<instance part="U$5" gate="G$1" x="248.92" y="83.82"/>
<instance part="U$8" gate="G$1" x="185.42" y="83.82"/>
<instance part="SVORKA37" gate="G$1" x="172.72" y="83.82"/>
<instance part="U$22" gate="G$1" x="12.7" y="91.44"/>
<instance part="SVORKA38" gate="G$1" x="76.2" y="96.52" rot="R180"/>
<instance part="SVORKA38" gate="G$2" x="76.2" y="96.52" rot="R180"/>
<instance part="SVORKA39" gate="G$1" x="76.2" y="93.98" rot="R180"/>
<instance part="SVORKA39" gate="G$2" x="76.2" y="93.98" rot="R180"/>
<instance part="U$17" gate="G$1" x="83.82" y="93.98"/>
<instance part="U$23" gate="GND" x="365.76" y="154.94" rot="R90"/>
<instance part="SERVO1" gate="G$1" x="266.7" y="248.92"/>
<instance part="PRUCHODKA1" gate="G$1" x="294.64" y="248.92"/>
<instance part="U$24" gate="GND" x="276.86" y="137.16"/>
<instance part="PRUCHODKA26" gate="G$1" x="73.66" y="124.46" rot="R90"/>
<instance part="U$16" gate="GND" x="213.36" y="66.04"/>
<instance part="SVORKA42" gate="G$1" x="198.12" y="83.82"/>
<instance part="PRUCHODKA27" gate="G$1" x="200.66" y="180.34" rot="R90"/>
<instance part="PRUCHODKA28" gate="G$1" x="213.36" y="180.34" rot="R90"/>
<instance part="SVORKA8" gate="G$1" x="210.82" y="83.82"/>
</instances>
<busses>
<bus name="SPEEDS-SUP,SPEEDS-SIG,SPEEDS-GND">
<segment>
<wire x1="213.36" y1="210.82" x2="213.36" y2="152.4" width="0.762" layer="92"/>
<wire x1="213.36" y1="152.4" x2="215.9" y2="149.86" width="0.762" layer="92"/>
<wire x1="215.9" y1="149.86" x2="223.52" y2="149.86" width="0.762" layer="92"/>
<wire x1="213.36" y1="210.82" x2="215.9" y2="213.36" width="0.762" layer="92"/>
<wire x1="215.9" y1="213.36" x2="238.76" y2="213.36" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="MCF-[1..19],MCF-SHIELD">
<segment>
<wire x1="10.16" y1="60.96" x2="10.16" y2="12.7" width="0.762" layer="92"/>
<wire x1="10.16" y1="12.7" x2="12.7" y2="10.16" width="0.762" layer="92"/>
<wire x1="12.7" y1="10.16" x2="129.54" y2="10.16" width="0.762" layer="92"/>
<wire x1="129.54" y1="10.16" x2="147.32" y2="10.16" width="0.762" layer="92"/>
<wire x1="147.32" y1="10.16" x2="256.54" y2="10.16" width="0.762" layer="92"/>
<wire x1="129.54" y1="10.16" x2="127" y2="12.7" width="0.762" layer="92"/>
<wire x1="127" y1="12.7" x2="127" y2="45.72" width="0.762" layer="92"/>
<wire x1="147.32" y1="10.16" x2="149.86" y2="12.7" width="0.762" layer="92"/>
<wire x1="149.86" y1="12.7" x2="149.86" y2="78.74" width="0.762" layer="92"/>
<wire x1="149.86" y1="78.74" x2="147.32" y2="81.28" width="0.762" layer="92"/>
<wire x1="147.32" y1="81.28" x2="104.14" y2="81.28" width="0.762" layer="92"/>
<wire x1="104.14" y1="81.28" x2="101.6" y2="83.82" width="0.762" layer="92"/>
<wire x1="101.6" y1="83.82" x2="101.6" y2="93.98" width="0.762" layer="92"/>
<wire x1="256.54" y1="10.16" x2="259.08" y2="12.7" width="0.762" layer="92"/>
<wire x1="259.08" y1="12.7" x2="259.08" y2="40.64" width="0.762" layer="92"/>
<wire x1="259.08" y1="40.64" x2="256.54" y2="43.18" width="0.762" layer="92"/>
<wire x1="256.54" y1="43.18" x2="248.92" y2="43.18" width="0.762" layer="92"/>
<wire x1="127" y1="45.72" x2="129.54" y2="48.26" width="0.762" layer="92"/>
<wire x1="129.54" y1="48.26" x2="134.62" y2="48.26" width="0.762" layer="92"/>
<wire x1="134.62" y1="48.26" x2="137.16" y2="50.8" width="0.762" layer="92"/>
<wire x1="137.16" y1="50.8" x2="137.16" y2="68.58" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="KUB-GLOW,KUB-START">
<segment>
<wire x1="58.42" y1="137.16" x2="63.5" y2="137.16" width="0.762" layer="92"/>
<wire x1="63.5" y1="137.16" x2="66.04" y2="134.62" width="0.762" layer="92"/>
<wire x1="66.04" y1="134.62" x2="66.04" y2="114.3" width="0.762" layer="92"/>
<wire x1="66.04" y1="114.3" x2="63.5" y2="111.76" width="0.762" layer="92"/>
<wire x1="63.5" y1="111.76" x2="50.8" y2="111.76" width="0.762" layer="92"/>
<wire x1="50.8" y1="111.76" x2="48.26" y2="109.22" width="0.762" layer="92"/>
<wire x1="48.26" y1="109.22" x2="48.26" y2="96.52" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="KUB-LAMP,KUB-IG,KUB-TEMP,KUB-OIL,KUB-AIR,KUB-STOPS,KUB-AIRGND,KUB-MTEMP">
<segment>
<wire x1="76.2" y1="137.16" x2="111.76" y2="137.16" width="0.762" layer="92"/>
<wire x1="76.2" y1="137.16" x2="73.66" y2="134.62" width="0.762" layer="92"/>
<wire x1="73.66" y1="134.62" x2="73.66" y2="114.3" width="0.762" layer="92"/>
<wire x1="73.66" y1="114.3" x2="76.2" y2="111.76" width="0.762" layer="92"/>
<wire x1="76.2" y1="111.76" x2="132.08" y2="111.76" width="0.762" layer="92"/>
<wire x1="132.08" y1="111.76" x2="134.62" y2="114.3" width="0.762" layer="92"/>
<wire x1="134.62" y1="114.3" x2="134.62" y2="142.24" width="0.762" layer="92"/>
<wire x1="134.62" y1="142.24" x2="137.16" y2="144.78" width="0.762" layer="92"/>
<wire x1="137.16" y1="144.78" x2="195.58" y2="144.78" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="SERVO-[1..5]">
<segment>
<wire x1="383.54" y1="137.16" x2="383.54" y2="246.38" width="0.762" layer="92"/>
<wire x1="383.54" y1="246.38" x2="381" y2="248.92" width="0.762" layer="92"/>
<wire x1="381" y1="248.92" x2="276.86" y2="248.92" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="FUELP+,FUELP-">
<segment>
<wire x1="200.66" y1="149.86" x2="200.66" y2="210.82" width="0.762" layer="92"/>
<wire x1="200.66" y1="210.82" x2="198.12" y2="213.36" width="0.762" layer="92"/>
<wire x1="198.12" y1="213.36" x2="193.04" y2="213.36" width="0.762" layer="92"/>
<wire x1="200.66" y1="149.86" x2="203.2" y2="147.32" width="0.762" layer="92"/>
<wire x1="203.2" y1="147.32" x2="208.28" y2="147.32" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="AS-MTEMP,AS-STOP,AS-ALARM">
<segment>
<wire x1="254" y1="152.4" x2="254" y2="187.96" width="0.762" layer="92"/>
<wire x1="254" y1="152.4" x2="251.46" y2="149.86" width="0.762" layer="92"/>
<wire x1="251.46" y1="149.86" x2="243.84" y2="149.86" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="AS-CANH,AS-CANL,AS-SHIELD">
<segment>
<wire x1="261.62" y1="116.84" x2="254" y2="116.84" width="0.762" layer="92"/>
<wire x1="254" y1="116.84" x2="251.46" y2="119.38" width="0.762" layer="92"/>
<wire x1="251.46" y1="119.38" x2="251.46" y2="142.24" width="0.762" layer="92"/>
<wire x1="251.46" y1="142.24" x2="259.08" y2="149.86" width="0.762" layer="92"/>
<wire x1="259.08" y1="149.86" x2="259.08" y2="162.56" width="0.762" layer="92"/>
<wire x1="259.08" y1="162.56" x2="261.62" y2="165.1" width="0.762" layer="92"/>
<wire x1="261.62" y1="165.1" x2="266.7" y2="165.1" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="AS-MOTOR,AS-AUTOSTART,AS-FPUMP,AS-EXCITATION,AS-ENGINE_RL">
<segment>
<wire x1="312.42" y1="104.14" x2="287.02" y2="104.14" width="0.762" layer="92"/>
<wire x1="287.02" y1="104.14" x2="284.48" y2="101.6" width="0.762" layer="92"/>
<wire x1="284.48" y1="101.6" x2="284.48" y2="53.34" width="0.762" layer="92"/>
</segment>
</bus>
</busses>
<nets>
<net name="KUBOTA_SUPPLY" class="0">
<segment>
<wire x1="38.1" y1="88.9" x2="17.78" y2="88.9" width="0.1524" layer="91"/>
<label x="17.78" y="91.44" size="1.778" layer="95" rot="MR180"/>
<wire x1="17.78" y1="88.9" x2="17.78" y2="86.36" width="0.1524" layer="91"/>
<wire x1="17.78" y1="86.36" x2="38.1" y2="86.36" width="0.1524" layer="91"/>
<junction x="17.78" y="88.9"/>
</segment>
<segment>
<pinref part="DPS1" gate="G$1" pin="K2.13"/>
<wire x1="292.1" y1="198.12" x2="292.1" y2="213.36" width="0.1524" layer="91"/>
<wire x1="292.1" y1="213.36" x2="320.04" y2="213.36" width="0.1524" layer="91"/>
<label x="317.5" y="213.36" size="1.778" layer="95" rot="MR0"/>
</segment>
<segment>
<label x="360.68" y="160.02" size="1.778" layer="95"/>
<pinref part="DPS1" gate="G$1" pin="JP4.6"/>
<wire x1="381" y1="160.02" x2="347.98" y2="160.02" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="MCFLEX1" gate="G$1" pin="KL31"/>
<wire x1="25.4" y1="20.32" x2="22.86" y2="20.32" width="0.1524" layer="91"/>
<pinref part="U$9" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="DPS1" gate="G$1" pin="JP2.1"/>
<wire x1="350.52" y1="187.96" x2="350.52" y2="185.42" width="0.1524" layer="91"/>
<wire x1="350.52" y1="185.42" x2="360.68" y2="185.42" width="0.1524" layer="91"/>
<pinref part="U$3" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="DPS1" gate="G$1" pin="JP4.5"/>
<wire x1="347.98" y1="154.94" x2="360.68" y2="154.94" width="0.1524" layer="91"/>
<pinref part="U$23" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="DPS1" gate="G$1" pin="K1.1"/>
<wire x1="289.56" y1="142.24" x2="289.56" y2="144.78" width="0.1524" layer="91"/>
<wire x1="289.56" y1="144.78" x2="276.86" y2="144.78" width="0.1524" layer="91"/>
<pinref part="U$24" gate="GND" pin="GND"/>
<wire x1="276.86" y1="144.78" x2="276.86" y2="142.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$16" gate="GND" pin="GND"/>
<wire x1="213.36" y1="71.12" x2="213.36" y2="73.66" width="0.1524" layer="91"/>
<pinref part="SVORKA8" gate="G$1" pin="6"/>
</segment>
</net>
<net name="+24V" class="0">
<segment>
<wire x1="12.7" y1="104.14" x2="12.7" y2="106.68" width="0.1524" layer="91"/>
<pinref part="U$4" gate="+24V" pin="+24V"/>
</segment>
</net>
<net name="KUB-START" class="0">
<segment>
<wire x1="50.8" y1="96.52" x2="68.58" y2="96.52" width="0.1524" layer="91"/>
<label x="66.04" y="96.52" size="1.778" layer="95" rot="MR0"/>
<pinref part="SVORKA38" gate="G$2" pin="B"/>
<wire x1="48.26" y1="99.06" x2="50.8" y2="96.52" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="DIESEL1" gate="G$1" pin="K2.2"/>
<wire x1="63.5" y1="137.16" x2="60.96" y2="139.7" width="0.1524" layer="91"/>
<wire x1="60.96" y1="139.7" x2="60.96" y2="157.48" width="0.1524" layer="91"/>
<label x="60.96" y="142.24" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="AS-EXCITATION" class="0">
<segment>
<pinref part="U$11" gate="G$1" pin="5"/>
<wire x1="236.22" y1="55.88" x2="236.22" y2="76.2" width="0.1524" layer="91"/>
<wire x1="284.48" y1="58.42" x2="281.94" y2="55.88" width="0.1524" layer="91"/>
<wire x1="281.94" y1="55.88" x2="236.22" y2="55.88" width="0.1524" layer="91"/>
<label x="236.22" y="73.66" size="1.778" layer="95" rot="MR270"/>
</segment>
<segment>
<pinref part="DPS1" gate="G$1" pin="K1.4"/>
<wire x1="297.18" y1="142.24" x2="297.18" y2="149.86" width="0.1524" layer="91"/>
<wire x1="297.18" y1="149.86" x2="312.42" y2="149.86" width="0.1524" layer="91"/>
<wire x1="312.42" y1="149.86" x2="312.42" y2="106.68" width="0.1524" layer="91"/>
<label x="312.42" y="109.22" size="1.778" layer="95" rot="R90"/>
<wire x1="309.88" y1="104.14" x2="312.42" y2="106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="AS-MOTOR" class="0">
<segment>
<pinref part="DPS1" gate="G$1" pin="K1.11"/>
<wire x1="297.18" y1="132.08" x2="297.18" y2="106.68" width="0.1524" layer="91"/>
<wire x1="294.64" y1="104.14" x2="297.18" y2="106.68" width="0.1524" layer="91"/>
<label x="297.18" y="109.22" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="SVORKA37" gate="G$1" pin="5"/>
<wire x1="281.94" y1="50.8" x2="172.72" y2="50.8" width="0.1524" layer="91"/>
<wire x1="172.72" y1="50.8" x2="172.72" y2="76.2" width="0.1524" layer="91"/>
<label x="172.72" y="73.66" size="1.778" layer="95" rot="MR270"/>
<wire x1="284.48" y1="53.34" x2="281.94" y2="50.8" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SVORKA37" gate="G$1" pin="4"/>
<pinref part="TMC1" gate="17" pin="MOTOR"/>
<wire x1="170.18" y1="78.74" x2="170.18" y2="48.26" width="0.1524" layer="91"/>
</segment>
</net>
<net name="MTEMP" class="0">
<segment>
<pinref part="TMC1" gate="47" pin="MTEMP"/>
<pinref part="SVORKA23" gate="G$1" pin="5"/>
<wire x1="160.02" y1="48.26" x2="160.02" y2="76.2" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SVORKA23" gate="G$1" pin="6"/>
<wire x1="162.56" y1="60.96" x2="162.56" y2="73.66" width="0.1524" layer="91"/>
<label x="162.56" y="60.96" size="1.27" layer="95" rot="R270" xref="yes"/>
</segment>
</net>
<net name="ALT" class="0">
<segment>
<wire x1="157.48" y1="88.9" x2="157.48" y2="114.3" width="0.1524" layer="91"/>
<label x="157.48" y="96.52" size="1.778" layer="95" rot="R90"/>
<pinref part="SVORKA23" gate="G$1" pin="3"/>
<label x="157.48" y="114.3" size="1.27" layer="95" rot="R90" xref="yes"/>
</segment>
<segment>
<pinref part="TMC1" gate="19" pin="ALT"/>
<pinref part="SVORKA23" gate="G$1" pin="4"/>
<wire x1="157.48" y1="48.26" x2="157.48" y2="78.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="MCF-13" class="0">
<segment>
<label x="226.06" y="73.66" size="1.778" layer="95" rot="MR270"/>
<wire x1="223.52" y1="10.16" x2="226.06" y2="12.7" width="0.1524" layer="91"/>
<pinref part="U$12" gate="G$1" pin="6"/>
<wire x1="226.06" y1="12.7" x2="226.06" y2="73.66" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="MCFLEX1" gate="G$1" pin="SPEED"/>
<wire x1="10.16" y1="48.26" x2="12.7" y2="50.8" width="0.1524" layer="91"/>
<wire x1="12.7" y1="50.8" x2="25.4" y2="50.8" width="0.1524" layer="91"/>
<label x="15.24" y="50.8" size="1.778" layer="95"/>
</segment>
</net>
<net name="MCF-9" class="0">
<segment>
<pinref part="MCFLEX1" gate="G$1" pin="STOP"/>
<wire x1="10.16" y1="50.8" x2="12.7" y2="53.34" width="0.1524" layer="91"/>
<wire x1="12.7" y1="53.34" x2="25.4" y2="53.34" width="0.1524" layer="91"/>
<label x="15.24" y="53.34" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="U$12" gate="G$1" pin="4"/>
<wire x1="218.44" y1="10.16" x2="220.98" y2="12.7" width="0.1524" layer="91"/>
<wire x1="220.98" y1="12.7" x2="220.98" y2="78.74" width="0.1524" layer="91"/>
<label x="220.98" y="73.66" size="1.778" layer="95" rot="MR270"/>
</segment>
</net>
<net name="MCF-7" class="0">
<segment>
<pinref part="U$11" gate="G$1" pin="6"/>
<wire x1="238.76" y1="12.7" x2="238.76" y2="73.66" width="0.1524" layer="91"/>
<label x="238.76" y="73.66" size="1.778" layer="95" rot="MR270"/>
<wire x1="236.22" y1="10.16" x2="238.76" y2="12.7" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="MCFLEX1" gate="G$1" pin="AUTOSTART"/>
<wire x1="10.16" y1="53.34" x2="12.7" y2="55.88" width="0.1524" layer="91"/>
<wire x1="12.7" y1="55.88" x2="25.4" y2="55.88" width="0.1524" layer="91"/>
<label x="15.24" y="55.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="MCF-4" class="0">
<segment>
<pinref part="MCFLEX1" gate="G$1" pin="DIVERSE"/>
<wire x1="10.16" y1="55.88" x2="12.7" y2="58.42" width="0.1524" layer="91"/>
<wire x1="12.7" y1="58.42" x2="25.4" y2="58.42" width="0.1524" layer="91"/>
<label x="15.24" y="58.42" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="SVORKA42" gate="G$1" pin="4"/>
<wire x1="193.04" y1="10.16" x2="195.58" y2="12.7" width="0.1524" layer="91"/>
<wire x1="195.58" y1="12.7" x2="195.58" y2="78.74" width="0.1524" layer="91"/>
<label x="195.58" y="73.66" size="1.778" layer="95" rot="MR270"/>
</segment>
</net>
<net name="MCF-3" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="5"/>
<wire x1="185.42" y1="76.2" x2="185.42" y2="12.7" width="0.1524" layer="91"/>
<label x="185.42" y="73.66" size="1.778" layer="95" rot="MR270"/>
<wire x1="182.88" y1="10.16" x2="185.42" y2="12.7" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="MCFLEX1" gate="G$1" pin="TEMP"/>
<wire x1="10.16" y1="58.42" x2="12.7" y2="60.96" width="0.1524" layer="91"/>
<wire x1="12.7" y1="60.96" x2="25.4" y2="60.96" width="0.1524" layer="91"/>
<label x="15.24" y="60.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="MCF-1" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="6"/>
<wire x1="187.96" y1="12.7" x2="187.96" y2="73.66" width="0.1524" layer="91"/>
<label x="187.96" y="73.66" size="1.778" layer="95" rot="MR270"/>
<wire x1="185.42" y1="10.16" x2="187.96" y2="12.7" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="MCFLEX1" gate="G$1" pin="OIL"/>
<wire x1="10.16" y1="60.96" x2="12.7" y2="63.5" width="0.1524" layer="91"/>
<wire x1="12.7" y1="63.5" x2="25.4" y2="63.5" width="0.1524" layer="91"/>
<label x="15.24" y="63.5" size="1.778" layer="95"/>
</segment>
</net>
<net name="MCF-18" class="0">
<segment>
<wire x1="198.12" y1="12.7" x2="198.12" y2="76.2" width="0.1524" layer="91"/>
<label x="198.12" y="73.66" size="1.778" layer="95" rot="MR270"/>
<wire x1="195.58" y1="10.16" x2="198.12" y2="12.7" width="0.1524" layer="91"/>
<pinref part="SVORKA42" gate="G$1" pin="5"/>
</segment>
<segment>
<pinref part="MCFLEX1" gate="G$1" pin="18"/>
<wire x1="127" y1="20.32" x2="124.46" y2="22.86" width="0.1524" layer="91"/>
<wire x1="124.46" y1="22.86" x2="111.76" y2="22.86" width="0.1524" layer="91"/>
<label x="121.92" y="22.86" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="MCF-15" class="0">
<segment>
<pinref part="U$11" gate="G$1" pin="4"/>
<wire x1="233.68" y1="12.7" x2="233.68" y2="78.74" width="0.1524" layer="91"/>
<label x="233.68" y="73.66" size="1.778" layer="95" rot="MR270"/>
<wire x1="231.14" y1="10.16" x2="233.68" y2="12.7" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="MCFLEX1" gate="G$1" pin="15"/>
<wire x1="127" y1="22.86" x2="124.46" y2="25.4" width="0.1524" layer="91"/>
<wire x1="124.46" y1="25.4" x2="111.76" y2="25.4" width="0.1524" layer="91"/>
<label x="121.92" y="25.4" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="MCF-12" class="0">
<segment>
<wire x1="101.6" y1="88.9" x2="99.06" y2="91.44" width="0.1524" layer="91"/>
<wire x1="99.06" y1="91.44" x2="86.36" y2="91.44" width="0.1524" layer="91"/>
<label x="88.9" y="91.44" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MCFLEX1" gate="G$1" pin="12"/>
<wire x1="127" y1="25.4" x2="124.46" y2="27.94" width="0.1524" layer="91"/>
<wire x1="124.46" y1="27.94" x2="111.76" y2="27.94" width="0.1524" layer="91"/>
<label x="121.92" y="27.94" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="MCF-11" class="0">
<segment>
<pinref part="MCFLEX1" gate="G$1" pin="11"/>
<wire x1="127" y1="27.94" x2="124.46" y2="30.48" width="0.1524" layer="91"/>
<wire x1="124.46" y1="30.48" x2="111.76" y2="30.48" width="0.1524" layer="91"/>
<label x="121.92" y="30.48" size="1.778" layer="95" rot="MR0"/>
</segment>
<segment>
<pinref part="U$12" gate="G$1" pin="5"/>
<wire x1="220.98" y1="10.16" x2="223.52" y2="12.7" width="0.1524" layer="91"/>
<wire x1="223.52" y1="12.7" x2="223.52" y2="76.2" width="0.1524" layer="91"/>
<label x="223.52" y="73.66" size="1.778" layer="95" rot="MR270"/>
</segment>
</net>
<net name="MCF-10" class="0">
<segment>
<wire x1="101.6" y1="91.44" x2="99.06" y2="93.98" width="0.1524" layer="91"/>
<wire x1="99.06" y1="93.98" x2="86.36" y2="93.98" width="0.1524" layer="91"/>
<label x="88.9" y="93.98" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MCFLEX1" gate="G$1" pin="10"/>
<wire x1="127" y1="30.48" x2="124.46" y2="33.02" width="0.1524" layer="91"/>
<wire x1="124.46" y1="33.02" x2="111.76" y2="33.02" width="0.1524" layer="91"/>
<label x="121.92" y="33.02" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="MCF-8" class="0">
<segment>
<wire x1="101.6" y1="93.98" x2="99.06" y2="96.52" width="0.1524" layer="91"/>
<pinref part="SVORKA38" gate="G$1" pin="A"/>
<wire x1="99.06" y1="96.52" x2="83.82" y2="96.52" width="0.1524" layer="91"/>
<label x="88.9" y="96.52" size="1.778" layer="95"/>
</segment>
<segment>
<pinref part="MCFLEX1" gate="G$1" pin="8"/>
<wire x1="127" y1="33.02" x2="124.46" y2="35.56" width="0.1524" layer="91"/>
<wire x1="124.46" y1="35.56" x2="111.76" y2="35.56" width="0.1524" layer="91"/>
<label x="121.92" y="35.56" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="MCF-6" class="0">
<segment>
<wire x1="182.88" y1="12.7" x2="182.88" y2="78.74" width="0.1524" layer="91"/>
<pinref part="U$8" gate="G$1" pin="4"/>
<label x="182.88" y="73.66" size="1.778" layer="95" rot="MR270"/>
<wire x1="180.34" y1="10.16" x2="182.88" y2="12.7" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="MCFLEX1" gate="G$1" pin="6"/>
<wire x1="127" y1="35.56" x2="124.46" y2="38.1" width="0.1524" layer="91"/>
<wire x1="124.46" y1="38.1" x2="111.76" y2="38.1" width="0.1524" layer="91"/>
<label x="121.92" y="38.1" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="MCF-17" class="0">
<segment>
<pinref part="U$5" gate="G$1" pin="5"/>
<wire x1="251.46" y1="43.18" x2="248.92" y2="45.72" width="0.1524" layer="91"/>
<wire x1="248.92" y1="45.72" x2="248.92" y2="76.2" width="0.1524" layer="91"/>
<label x="248.92" y="73.66" size="1.778" layer="95" rot="MR270"/>
</segment>
</net>
<net name="MCF-19" class="0">
<segment>
<pinref part="MCFLEX1" gate="G$1" pin="CANH"/>
<wire x1="111.76" y1="71.12" x2="134.62" y2="71.12" width="0.1524" layer="91"/>
<label x="114.3" y="71.12" size="1.778" layer="95"/>
<wire x1="137.16" y1="68.58" x2="134.62" y2="71.12" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$5" gate="G$1" pin="4"/>
<wire x1="248.92" y1="43.18" x2="246.38" y2="45.72" width="0.1524" layer="91"/>
<wire x1="246.38" y1="45.72" x2="246.38" y2="78.74" width="0.1524" layer="91"/>
<label x="246.38" y="73.66" size="1.778" layer="95" rot="MR270"/>
</segment>
<segment>
<pinref part="MCFLEX1" gate="G$1" pin="CANL"/>
<wire x1="111.76" y1="68.58" x2="134.62" y2="68.58" width="0.1524" layer="91"/>
<label x="114.3" y="68.58" size="1.778" layer="95"/>
<wire x1="137.16" y1="66.04" x2="134.62" y2="68.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="D+" class="0">
<segment>
<pinref part="DPS1" gate="G$1" pin="K1.8"/>
<label x="289.56" y="121.92" size="1.27" layer="95" rot="R270" xref="yes"/>
<wire x1="289.56" y1="121.92" x2="289.56" y2="132.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="AS-AUTOSTART" class="0">
<segment>
<pinref part="DPS1" gate="G$1" pin="K1.14"/>
<wire x1="304.8" y1="132.08" x2="304.8" y2="106.68" width="0.1524" layer="91"/>
<wire x1="304.8" y1="106.68" x2="302.26" y2="104.14" width="0.1524" layer="91"/>
<label x="304.8" y="109.22" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="284.48" y1="96.52" x2="281.94" y2="93.98" width="0.1524" layer="91"/>
<wire x1="281.94" y1="93.98" x2="271.78" y2="93.98" width="0.1524" layer="91"/>
<pinref part="U$11" gate="G$1" pin="1"/>
<wire x1="271.78" y1="109.22" x2="238.76" y2="109.22" width="0.1524" layer="91"/>
<wire x1="238.76" y1="93.98" x2="238.76" y2="109.22" width="0.1524" layer="91"/>
<label x="238.76" y="93.98" size="1.778" layer="95" rot="R90"/>
<wire x1="271.78" y1="93.98" x2="271.78" y2="109.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="AS-ENGINE_RL" class="0">
<segment>
<wire x1="284.48" y1="99.06" x2="281.94" y2="96.52" width="0.1524" layer="91"/>
<wire x1="281.94" y1="96.52" x2="274.32" y2="96.52" width="0.1524" layer="91"/>
<wire x1="274.32" y1="96.52" x2="274.32" y2="111.76" width="0.1524" layer="91"/>
<wire x1="274.32" y1="111.76" x2="233.68" y2="111.76" width="0.1524" layer="91"/>
<pinref part="U$11" gate="G$1" pin="3"/>
<wire x1="233.68" y1="88.9" x2="233.68" y2="111.76" width="0.1524" layer="91"/>
<label x="233.68" y="93.98" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="312.42" y1="104.14" x2="314.96" y2="106.68" width="0.1524" layer="91"/>
<pinref part="DPS1" gate="G$1" pin="K1.2"/>
<wire x1="292.1" y1="142.24" x2="292.1" y2="152.4" width="0.1524" layer="91"/>
<wire x1="292.1" y1="152.4" x2="314.96" y2="152.4" width="0.1524" layer="91"/>
<wire x1="314.96" y1="152.4" x2="314.96" y2="106.68" width="0.1524" layer="91"/>
<label x="314.96" y="109.22" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="AS-FPUMP" class="0">
<segment>
<pinref part="DPS1" gate="G$1" pin="K1.6"/>
<wire x1="302.26" y1="142.24" x2="302.26" y2="147.32" width="0.1524" layer="91"/>
<wire x1="302.26" y1="147.32" x2="309.88" y2="147.32" width="0.1524" layer="91"/>
<wire x1="309.88" y1="147.32" x2="309.88" y2="106.68" width="0.1524" layer="91"/>
<wire x1="307.34" y1="104.14" x2="309.88" y2="106.68" width="0.1524" layer="91"/>
<label x="309.88" y="109.22" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="SVORKA42" gate="G$1" pin="6"/>
<wire x1="281.94" y1="53.34" x2="200.66" y2="53.34" width="0.1524" layer="91"/>
<wire x1="200.66" y1="73.66" x2="200.66" y2="53.34" width="0.1524" layer="91"/>
<label x="200.66" y="73.66" size="1.778" layer="95" rot="MR270"/>
<wire x1="284.48" y1="55.88" x2="281.94" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="AS-CANL" class="0">
<segment>
<pinref part="DPS1" gate="G$1" pin="K2.7"/>
<wire x1="289.56" y1="187.96" x2="289.56" y2="185.42" width="0.1524" layer="91"/>
<wire x1="289.56" y1="185.42" x2="266.7" y2="185.42" width="0.1524" layer="91"/>
<wire x1="266.7" y1="167.64" x2="266.7" y2="185.42" width="0.1524" layer="91"/>
<wire x1="264.16" y1="165.1" x2="266.7" y2="167.64" width="0.1524" layer="91"/>
<label x="279.4" y="185.42" size="1.778" layer="95" rot="MR0"/>
</segment>
<segment>
<wire x1="259.08" y1="116.84" x2="261.62" y2="114.3" width="0.1524" layer="91"/>
<wire x1="261.62" y1="114.3" x2="261.62" y2="101.6" width="0.1524" layer="91"/>
<pinref part="U$5" gate="G$1" pin="2"/>
<label x="248.92" y="93.98" size="1.778" layer="95" rot="R90"/>
<wire x1="261.62" y1="101.6" x2="248.92" y2="101.6" width="0.1524" layer="91"/>
<wire x1="248.92" y1="101.6" x2="248.92" y2="91.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="AS-CANH" class="0">
<segment>
<wire x1="266.7" y1="165.1" x2="269.24" y2="167.64" width="0.1524" layer="91"/>
<wire x1="269.24" y1="167.64" x2="269.24" y2="182.88" width="0.1524" layer="91"/>
<wire x1="292.1" y1="182.88" x2="269.24" y2="182.88" width="0.1524" layer="91"/>
<pinref part="DPS1" gate="G$1" pin="K2.6"/>
<wire x1="292.1" y1="187.96" x2="292.1" y2="182.88" width="0.1524" layer="91"/>
<label x="279.4" y="182.88" size="1.778" layer="95" rot="MR0"/>
</segment>
<segment>
<wire x1="256.54" y1="116.84" x2="259.08" y2="114.3" width="0.1524" layer="91"/>
<wire x1="259.08" y1="114.3" x2="259.08" y2="104.14" width="0.1524" layer="91"/>
<wire x1="259.08" y1="104.14" x2="246.38" y2="104.14" width="0.1524" layer="91"/>
<pinref part="U$5" gate="G$1" pin="3"/>
<label x="246.38" y="93.98" size="1.778" layer="95" rot="R90"/>
<wire x1="246.38" y1="104.14" x2="246.38" y2="88.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="AS-STOP" class="0">
<segment>
<wire x1="256.54" y1="177.8" x2="254" y2="175.26" width="0.1524" layer="91"/>
<pinref part="DPS1" gate="G$1" pin="K2.2"/>
<wire x1="302.26" y1="187.96" x2="302.26" y2="177.8" width="0.1524" layer="91"/>
<wire x1="302.26" y1="177.8" x2="256.54" y2="177.8" width="0.1524" layer="91"/>
<label x="279.4" y="177.8" size="1.778" layer="95" rot="MR0"/>
</segment>
<segment>
<wire x1="246.38" y1="149.86" x2="243.84" y2="147.32" width="0.1524" layer="91"/>
<pinref part="U$12" gate="G$1" pin="3"/>
<wire x1="243.84" y1="119.38" x2="220.98" y2="119.38" width="0.1524" layer="91"/>
<wire x1="220.98" y1="119.38" x2="220.98" y2="88.9" width="0.1524" layer="91"/>
<label x="220.98" y="93.98" size="1.778" layer="95" rot="R90"/>
<wire x1="243.84" y1="147.32" x2="243.84" y2="119.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="AS-ALARM" class="0">
<segment>
<wire x1="256.54" y1="175.26" x2="254" y2="172.72" width="0.1524" layer="91"/>
<pinref part="DPS1" gate="G$1" pin="K2.1"/>
<wire x1="304.8" y1="187.96" x2="304.8" y2="175.26" width="0.1524" layer="91"/>
<wire x1="304.8" y1="175.26" x2="256.54" y2="175.26" width="0.1524" layer="91"/>
<label x="279.4" y="175.26" size="1.778" layer="95" rot="MR0"/>
</segment>
<segment>
<wire x1="248.92" y1="149.86" x2="246.38" y2="147.32" width="0.1524" layer="91"/>
<pinref part="U$12" gate="G$1" pin="2"/>
<wire x1="223.52" y1="91.44" x2="223.52" y2="116.84" width="0.1524" layer="91"/>
<wire x1="223.52" y1="116.84" x2="246.38" y2="116.84" width="0.1524" layer="91"/>
<label x="223.52" y="93.98" size="1.778" layer="95" rot="R90"/>
<wire x1="246.38" y1="147.32" x2="246.38" y2="116.84" width="0.1524" layer="91"/>
</segment>
</net>
<net name="!HORN_ON" class="0">
<segment>
<pinref part="DPS1" gate="G$1" pin="K2.14"/>
<wire x1="289.56" y1="198.12" x2="289.56" y2="215.9" width="0.1524" layer="91"/>
<label x="320.04" y="215.9" size="1.27" layer="95" xref="yes"/>
<wire x1="289.56" y1="215.9" x2="320.04" y2="215.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="!ELMOT" class="0">
<segment>
<pinref part="DPS1" gate="G$1" pin="K2.9"/>
<wire x1="302.26" y1="198.12" x2="302.26" y2="205.74" width="0.1524" layer="91"/>
<wire x1="320.04" y1="205.74" x2="302.26" y2="205.74" width="0.1524" layer="91"/>
<label x="320.04" y="205.74" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="SPEEDS-GND" class="0">
<segment>
<wire x1="220.98" y1="149.86" x2="223.52" y2="147.32" width="0.1524" layer="91"/>
<wire x1="223.52" y1="147.32" x2="223.52" y2="127" width="0.1524" layer="91"/>
<wire x1="223.52" y1="127" x2="213.36" y2="127" width="0.1524" layer="91"/>
<wire x1="213.36" y1="127" x2="213.36" y2="93.98" width="0.1524" layer="91"/>
<label x="213.36" y="93.98" size="1.778" layer="95" rot="R90"/>
<pinref part="SVORKA8" gate="G$1" pin="1"/>
</segment>
<segment>
<pinref part="DIESEL1" gate="G$3" pin="K3.1"/>
<wire x1="231.14" y1="215.9" x2="231.14" y2="233.68" width="0.1524" layer="91"/>
<label x="231.14" y="218.44" size="1.778" layer="95" rot="R90"/>
<wire x1="231.14" y1="215.9" x2="228.6" y2="213.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SPEEDS-SIG" class="0">
<segment>
<pinref part="U$12" gate="G$1" pin="1"/>
<wire x1="226.06" y1="147.32" x2="226.06" y2="93.98" width="0.1524" layer="91"/>
<label x="226.06" y="93.98" size="1.778" layer="95" rot="R90"/>
<wire x1="226.06" y1="147.32" x2="223.52" y2="149.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="DIESEL1" gate="G$3" pin="K3.2"/>
<wire x1="236.22" y1="215.9" x2="236.22" y2="233.68" width="0.1524" layer="91"/>
<label x="236.22" y="218.44" size="1.778" layer="95" rot="R90"/>
<wire x1="236.22" y1="215.9" x2="233.68" y2="213.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SPEEDS-SUP" class="0">
<segment>
<wire x1="220.98" y1="147.32" x2="218.44" y2="149.86" width="0.1524" layer="91"/>
<wire x1="220.98" y1="147.32" x2="220.98" y2="132.08" width="0.1524" layer="91"/>
<wire x1="220.98" y1="132.08" x2="200.66" y2="132.08" width="0.1524" layer="91"/>
<pinref part="SVORKA42" gate="G$1" pin="1"/>
<wire x1="200.66" y1="132.08" x2="200.66" y2="93.98" width="0.1524" layer="91"/>
<label x="200.66" y="93.98" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="DIESEL1" gate="G$3" pin="K3.3"/>
<wire x1="238.76" y1="213.36" x2="241.3" y2="215.9" width="0.1524" layer="91"/>
<wire x1="241.3" y1="215.9" x2="241.3" y2="233.68" width="0.1524" layer="91"/>
<label x="241.3" y="218.44" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="!VENTILATION_ON" class="0">
<segment>
<label x="360.68" y="177.8" size="1.27" layer="95" xref="yes"/>
<pinref part="DPS1" gate="G$1" pin="JP2.6"/>
<wire x1="360.68" y1="177.8" x2="337.82" y2="177.8" width="0.1524" layer="91"/>
<wire x1="337.82" y1="177.8" x2="337.82" y2="187.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="!TMC_EMERGENCY" class="0">
<segment>
<label x="360.68" y="180.34" size="1.27" layer="95" xref="yes"/>
<pinref part="DPS1" gate="G$1" pin="JP2.5"/>
<wire x1="360.68" y1="180.34" x2="340.36" y2="180.34" width="0.1524" layer="91"/>
<wire x1="340.36" y1="180.34" x2="340.36" y2="187.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="!TMC_COOL" class="0">
<segment>
<label x="345.44" y="210.82" size="1.27" layer="95" xref="yes"/>
<pinref part="DPS1" gate="G$1" pin="JP2.12"/>
<wire x1="345.44" y1="210.82" x2="337.82" y2="210.82" width="0.1524" layer="91"/>
<wire x1="337.82" y1="210.82" x2="337.82" y2="198.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="COOL" class="0">
<segment>
<label x="345.44" y="208.28" size="1.27" layer="95" xref="yes"/>
<pinref part="DPS1" gate="G$1" pin="JP2.11"/>
<wire x1="345.44" y1="208.28" x2="340.36" y2="208.28" width="0.1524" layer="91"/>
<wire x1="340.36" y1="208.28" x2="340.36" y2="198.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="MANUAL_SWITCH_8_PIN" class="0">
<segment>
<label x="345.44" y="205.74" size="1.27" layer="95" xref="yes"/>
<pinref part="DPS1" gate="G$1" pin="JP2.10"/>
<wire x1="345.44" y1="205.74" x2="342.9" y2="205.74" width="0.1524" layer="91"/>
<wire x1="342.9" y1="205.74" x2="342.9" y2="198.12" width="0.1524" layer="91"/>
</segment>
</net>
<net name="MCF-14" class="0">
<segment>
<pinref part="MCFLEX1" gate="G$1" pin="KL30@14"/>
<wire x1="25.4" y1="71.12" x2="12.7" y2="71.12" width="0.1524" layer="91"/>
<label x="15.24" y="71.12" size="1.778" layer="95"/>
<wire x1="12.7" y1="71.12" x2="12.7" y2="86.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="MCF-16" class="0">
<segment>
<pinref part="MCFLEX1" gate="G$1" pin="KL30@16"/>
<wire x1="25.4" y1="68.58" x2="10.16" y2="68.58" width="0.1524" layer="91"/>
<label x="15.24" y="68.58" size="1.778" layer="95"/>
<wire x1="10.16" y1="68.58" x2="10.16" y2="86.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="FUELP-" class="0">
<segment>
<wire x1="210.82" y1="91.44" x2="210.82" y2="144.78" width="0.1524" layer="91"/>
<label x="210.82" y="93.98" size="1.778" layer="95" rot="R90"/>
<wire x1="210.82" y1="144.78" x2="208.28" y2="147.32" width="0.1524" layer="91"/>
<pinref part="SVORKA8" gate="G$1" pin="2"/>
</segment>
<segment>
<pinref part="DIESEL1" gate="G$2" pin="-"/>
<wire x1="193.04" y1="215.9" x2="193.04" y2="238.76" width="0.1524" layer="91"/>
<label x="193.04" y="236.22" size="1.778" layer="95" rot="MR270"/>
<wire x1="195.58" y1="213.36" x2="193.04" y2="215.9" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SERVO-3" class="0">
<segment>
<pinref part="DPS1" gate="G$1" pin="JP4.3"/>
<wire x1="347.98" y1="144.78" x2="381" y2="144.78" width="0.1524" layer="91"/>
<label x="375.92" y="144.78" size="1.778" layer="95" rot="MR0"/>
<wire x1="383.54" y1="147.32" x2="381" y2="144.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SERVO-2" class="0">
<segment>
<pinref part="DPS1" gate="G$1" pin="JP4.2"/>
<wire x1="347.98" y1="139.7" x2="381" y2="139.7" width="0.1524" layer="91"/>
<label x="375.92" y="139.7" size="1.778" layer="95" rot="MR0"/>
<wire x1="383.54" y1="142.24" x2="381" y2="139.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SERVO-1" class="0">
<segment>
<pinref part="DPS1" gate="G$1" pin="JP4.1"/>
<wire x1="347.98" y1="134.62" x2="381" y2="134.62" width="0.1524" layer="91"/>
<label x="375.92" y="134.62" size="1.778" layer="95" rot="MR0"/>
<wire x1="383.54" y1="137.16" x2="381" y2="134.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="SERVO-5" class="0">
<segment>
<pinref part="DPS1" gate="G$1" pin="JP4.4"/>
<wire x1="381" y1="149.86" x2="347.98" y2="149.86" width="0.1524" layer="91"/>
<label x="375.92" y="149.86" size="1.778" layer="95" rot="MR0"/>
<wire x1="383.54" y1="152.4" x2="381" y2="149.86" width="0.1524" layer="91"/>
</segment>
</net>
<net name="MCF-2" class="0">
<segment>
<pinref part="MCFLEX1" gate="G$1" pin="2"/>
<wire x1="127" y1="38.1" x2="124.46" y2="40.64" width="0.1524" layer="91"/>
<wire x1="124.46" y1="40.64" x2="111.76" y2="40.64" width="0.1524" layer="91"/>
<label x="121.92" y="40.64" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="KUB-GLOW" class="0">
<segment>
<wire x1="50.8" y1="93.98" x2="68.58" y2="93.98" width="0.1524" layer="91"/>
<label x="66.04" y="93.98" size="1.778" layer="95" rot="MR0"/>
<pinref part="SVORKA39" gate="G$2" pin="B"/>
<wire x1="48.26" y1="96.52" x2="50.8" y2="93.98" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="DIESEL1" gate="G$1" pin="K2.1"/>
<wire x1="58.42" y1="137.16" x2="55.88" y2="139.7" width="0.1524" layer="91"/>
<wire x1="55.88" y1="139.7" x2="55.88" y2="157.48" width="0.1524" layer="91"/>
<label x="55.88" y="142.24" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="KUB-MTEMP" class="0">
<segment>
<wire x1="157.48" y1="144.78" x2="160.02" y2="142.24" width="0.1524" layer="91"/>
<wire x1="160.02" y1="91.44" x2="160.02" y2="142.24" width="0.1524" layer="91"/>
<pinref part="SVORKA23" gate="G$1" pin="2"/>
<label x="160.02" y="96.52" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<label x="114.3" y="142.24" size="1.778" layer="95" rot="R90"/>
<pinref part="DIESEL1" gate="G$1" pin="K1.8"/>
<wire x1="111.76" y1="137.16" x2="114.3" y2="139.7" width="0.1524" layer="91"/>
<wire x1="114.3" y1="139.7" x2="114.3" y2="157.48" width="0.1524" layer="91"/>
</segment>
</net>
<net name="KUB-AIRGND" class="0">
<segment>
<label x="109.22" y="142.24" size="1.778" layer="95" rot="R90"/>
<pinref part="DIESEL1" gate="G$1" pin="K1.7"/>
<wire x1="106.68" y1="137.16" x2="109.22" y2="139.7" width="0.1524" layer="91"/>
<wire x1="109.22" y1="139.7" x2="109.22" y2="157.48" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="190.5" y1="144.78" x2="193.04" y2="142.24" width="0.1524" layer="91"/>
<wire x1="193.04" y1="142.24" x2="193.04" y2="129.54" width="0.1524" layer="91"/>
<label x="208.28" y="93.98" size="1.778" layer="95" rot="R90"/>
<wire x1="208.28" y1="129.54" x2="208.28" y2="88.9" width="0.1524" layer="91"/>
<wire x1="193.04" y1="129.54" x2="208.28" y2="129.54" width="0.1524" layer="91"/>
<pinref part="SVORKA8" gate="G$1" pin="3"/>
</segment>
</net>
<net name="KUB-AIR" class="0">
<segment>
<wire x1="99.06" y1="139.7" x2="99.06" y2="157.48" width="0.1524" layer="91"/>
<label x="99.06" y="142.24" size="1.778" layer="95" rot="R90"/>
<pinref part="DIESEL1" gate="G$1" pin="K1.5"/>
<wire x1="96.52" y1="137.16" x2="99.06" y2="139.7" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SVORKA42" gate="G$1" pin="3"/>
<wire x1="187.96" y1="144.78" x2="190.5" y2="142.24" width="0.1524" layer="91"/>
<wire x1="190.5" y1="142.24" x2="190.5" y2="127" width="0.1524" layer="91"/>
<wire x1="190.5" y1="127" x2="195.58" y2="127" width="0.1524" layer="91"/>
<wire x1="195.58" y1="127" x2="195.58" y2="88.9" width="0.1524" layer="91"/>
<label x="195.58" y="93.98" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="KUB-OIL" class="0">
<segment>
<wire x1="185.42" y1="144.78" x2="187.96" y2="142.24" width="0.1524" layer="91"/>
<wire x1="187.96" y1="142.24" x2="187.96" y2="93.98" width="0.1524" layer="91"/>
<pinref part="U$8" gate="G$1" pin="1"/>
<label x="187.96" y="93.98" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="93.98" y1="139.7" x2="93.98" y2="157.48" width="0.1524" layer="91"/>
<label x="93.98" y="142.24" size="1.778" layer="95" rot="R90"/>
<pinref part="DIESEL1" gate="G$1" pin="K1.4"/>
<wire x1="91.44" y1="137.16" x2="93.98" y2="139.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="KUB-TEMP" class="0">
<segment>
<wire x1="182.88" y1="144.78" x2="185.42" y2="142.24" width="0.1524" layer="91"/>
<wire x1="185.42" y1="142.24" x2="185.42" y2="91.44" width="0.1524" layer="91"/>
<pinref part="U$8" gate="G$1" pin="2"/>
<label x="185.42" y="93.98" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="88.9" y1="139.7" x2="88.9" y2="157.48" width="0.1524" layer="91"/>
<label x="88.9" y="142.24" size="1.778" layer="95" rot="R90"/>
<pinref part="DIESEL1" gate="G$1" pin="K1.3"/>
<wire x1="86.36" y1="137.16" x2="88.9" y2="139.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="KUB-STOPS" class="0">
<segment>
<pinref part="U$8" gate="G$1" pin="3"/>
<wire x1="180.34" y1="144.78" x2="182.88" y2="142.24" width="0.1524" layer="91"/>
<wire x1="182.88" y1="142.24" x2="182.88" y2="88.9" width="0.1524" layer="91"/>
<label x="182.88" y="93.98" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="104.14" y1="139.7" x2="104.14" y2="157.48" width="0.1524" layer="91"/>
<label x="104.14" y="142.24" size="1.778" layer="95" rot="R90"/>
<pinref part="DIESEL1" gate="G$1" pin="K1.6"/>
<wire x1="101.6" y1="137.16" x2="104.14" y2="139.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="KUB-IG" class="0">
<segment>
<pinref part="SVORKA37" gate="G$1" pin="3"/>
<wire x1="167.64" y1="144.78" x2="170.18" y2="142.24" width="0.1524" layer="91"/>
<wire x1="170.18" y1="142.24" x2="170.18" y2="88.9" width="0.1524" layer="91"/>
<label x="170.18" y="93.98" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<label x="83.82" y="142.24" size="1.778" layer="95" rot="R90"/>
<pinref part="DIESEL1" gate="G$1" pin="K1.2"/>
<wire x1="83.82" y1="157.48" x2="83.82" y2="139.7" width="0.1524" layer="91"/>
<wire x1="81.28" y1="137.16" x2="83.82" y2="139.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="KUB-LAMP" class="0">
<segment>
<label x="78.74" y="142.24" size="1.778" layer="95" rot="R90"/>
<pinref part="DIESEL1" gate="G$1" pin="K1.1"/>
<wire x1="78.74" y1="157.48" x2="78.74" y2="139.7" width="0.1524" layer="91"/>
<wire x1="76.2" y1="137.16" x2="78.74" y2="139.7" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="198.12" y1="142.24" x2="236.22" y2="142.24" width="0.1524" layer="91"/>
<pinref part="U$11" gate="G$1" pin="2"/>
<wire x1="236.22" y1="142.24" x2="236.22" y2="91.44" width="0.1524" layer="91"/>
<label x="236.22" y="93.98" size="1.778" layer="95" rot="R90"/>
<wire x1="195.58" y1="144.78" x2="198.12" y2="142.24" width="0.1524" layer="91"/>
</segment>
</net>
<net name="AS-MTEMP" class="0">
<segment>
<wire x1="243.84" y1="149.86" x2="241.3" y2="147.32" width="0.1524" layer="91"/>
<label x="162.56" y="96.52" size="1.778" layer="95" rot="R90"/>
<wire x1="241.3" y1="121.92" x2="162.56" y2="121.92" width="0.1524" layer="91"/>
<wire x1="162.56" y1="121.92" x2="162.56" y2="93.98" width="0.1524" layer="91"/>
<pinref part="SVORKA23" gate="G$1" pin="1"/>
<wire x1="241.3" y1="147.32" x2="241.3" y2="121.92" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="254" y1="187.96" x2="256.54" y2="190.5" width="0.1524" layer="91"/>
<wire x1="294.64" y1="210.82" x2="256.54" y2="210.82" width="0.1524" layer="91"/>
<pinref part="DPS1" gate="G$1" pin="K2.12"/>
<wire x1="294.64" y1="198.12" x2="294.64" y2="210.82" width="0.1524" layer="91"/>
<label x="279.4" y="210.82" size="1.778" layer="95" rot="MR0"/>
<wire x1="256.54" y1="190.5" x2="256.54" y2="210.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="FUELP+" class="0">
<segment>
<wire x1="190.5" y1="215.9" x2="190.5" y2="254" width="0.1524" layer="91"/>
<pinref part="DIESEL1" gate="G$2" pin="+"/>
<wire x1="190.5" y1="254" x2="193.04" y2="254" width="0.1524" layer="91"/>
<label x="190.5" y="251.46" size="1.778" layer="95" rot="MR270"/>
<wire x1="193.04" y1="213.36" x2="190.5" y2="215.9" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="205.74" y1="147.32" x2="208.28" y2="144.78" width="0.1524" layer="91"/>
<wire x1="198.12" y1="91.44" x2="198.12" y2="137.16" width="0.1524" layer="91"/>
<pinref part="SVORKA42" gate="G$1" pin="2"/>
<label x="198.12" y="93.98" size="1.778" layer="95" rot="R90"/>
<wire x1="198.12" y1="137.16" x2="208.28" y2="137.16" width="0.1524" layer="91"/>
<wire x1="208.28" y1="144.78" x2="208.28" y2="137.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="AS-SHIELD" class="0">
<segment>
<pinref part="DPS1" gate="G$1" pin="K2.11"/>
<wire x1="297.18" y1="198.12" x2="297.18" y2="205.74" width="0.1524" layer="91"/>
<wire x1="297.18" y1="205.74" x2="264.16" y2="205.74" width="0.1524" layer="91"/>
<label x="279.4" y="205.74" size="1.778" layer="95" rot="MR0"/>
<wire x1="264.16" y1="205.74" x2="264.16" y2="167.64" width="0.1524" layer="91"/>
<wire x1="264.16" y1="167.64" x2="261.62" y2="165.1" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="261.62" y1="116.84" x2="264.16" y2="114.3" width="0.1524" layer="91"/>
<wire x1="264.16" y1="114.3" x2="264.16" y2="99.06" width="0.1524" layer="91"/>
<wire x1="264.16" y1="99.06" x2="251.46" y2="99.06" width="0.1524" layer="91"/>
<pinref part="U$5" gate="G$1" pin="1"/>
<wire x1="251.46" y1="99.06" x2="251.46" y2="93.98" width="0.1524" layer="91"/>
<label x="251.46" y="99.06" size="1.778" layer="95"/>
</segment>
</net>
<net name="MCF-SHIELD" class="0">
<segment>
<pinref part="U$5" gate="G$1" pin="6"/>
<wire x1="254" y1="43.18" x2="251.46" y2="45.72" width="0.1524" layer="91"/>
<wire x1="251.46" y1="45.72" x2="251.46" y2="73.66" width="0.1524" layer="91"/>
<label x="251.46" y="73.66" size="1.778" layer="95" rot="MR270"/>
</segment>
<segment>
<wire x1="137.16" y1="63.5" x2="134.62" y2="66.04" width="0.1524" layer="91"/>
<wire x1="134.62" y1="66.04" x2="111.76" y2="66.04" width="0.1524" layer="91"/>
<label x="114.3" y="66.04" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<description>Dezinfekce</description>
<plain>
<wire x1="309.245" y1="220.98" x2="311.15" y2="220.98" width="0.1524" layer="94"/>
<wire x1="312.42" y1="220.98" x2="314.325" y2="220.98" width="0.1524" layer="94"/>
<wire x1="315.595" y1="220.98" x2="317.5" y2="220.98" width="0.1524" layer="94"/>
<text x="224.79" y="214.63" size="2.1844" layer="95" rot="R90">87</text>
<text x="224.79" y="226.695" size="2.1844" layer="95" rot="R90">30</text>
<text x="193.04" y="213.995" size="2.1844" layer="95" rot="R90">86</text>
<text x="193.04" y="226.06" size="2.1844" layer="95" rot="R90">85</text>
<wire x1="200.66" y1="220.98" x2="219.71" y2="220.98" width="0.1524" layer="94" style="shortdash"/>
<wire x1="337.185" y1="220.98" x2="339.09" y2="220.98" width="0.1524" layer="94"/>
<wire x1="340.36" y1="220.98" x2="342.265" y2="220.98" width="0.1524" layer="94"/>
<wire x1="343.535" y1="220.98" x2="345.44" y2="220.98" width="0.1524" layer="94"/>
<text x="308.61" y="224.155" size="2.1844" layer="95" rot="R90">A1</text>
<text x="303.53" y="213.995" size="2.1844" layer="95" rot="R90">A2</text>
<text x="336.55" y="224.155" size="2.1844" layer="95" rot="R90">A1</text>
<text x="336.55" y="213.995" size="2.1844" layer="95" rot="R90">A2</text>
<text x="319.405" y="227.33" size="2.1844" layer="95" rot="R180">11</text>
<text x="316.865" y="217.17" size="2.1844" layer="95" rot="R180">14</text>
<text x="324.485" y="217.17" size="2.1844" layer="95" rot="R180">12</text>
<text x="354.965" y="217.17" size="2.1844" layer="95" rot="R180">12</text>
<text x="344.805" y="217.17" size="2.1844" layer="95" rot="R180">14</text>
<text x="347.345" y="227.33" size="2.1844" layer="95" rot="R180">11</text>
<text x="114.3" y="149.86" size="5.08" layer="92">?</text>
<text x="114.3" y="114.3" size="5.08" layer="92">?</text>
<text x="114.3" y="99.06" size="5.08" layer="92">?</text>
<wire x1="71.12" y1="43.18" x2="124.46" y2="43.18" width="0.1524" layer="94" style="longdash"/>
<wire x1="215.9" y1="175.26" x2="215.9" y2="96.52" width="0.1524" layer="94" style="longdash"/>
<text x="147.32" y="66.04" size="5.08" layer="92">?</text>
<text x="33.02" y="137.16" size="1.778" layer="97" align="bottom-center">BODY LIGHT</text>
<text x="66.04" y="137.16" size="1.778" layer="97" align="bottom-center">BODY LIGHT</text>
<text x="33.02" y="157.48" size="1.778" layer="97" align="top-center">S5</text>
<text x="66.04" y="157.48" size="1.778" layer="97" align="top-center">S12</text>
</plain>
<instances>
<instance part="FRAME9" gate="G$1" x="0" y="0"/>
<instance part="FRAME9" gate="G$2" x="287.02" y="0"/>
<instance part="SVORKA13" gate="G$1" x="134.62" y="142.24" rot="R270"/>
<instance part="SVORKA14" gate="G$1" x="134.62" y="106.68" rot="R270"/>
<instance part="SVORKA36" gate="G$1" x="134.62" y="160.02" rot="R270"/>
<instance part="SVORKA40" gate="G$1" x="134.62" y="124.46" rot="R270"/>
<instance part="SVORKA41" gate="G$1" x="134.62" y="86.36" rot="R270"/>
<instance part="SVORKA43" gate="G$1" x="134.62" y="66.04" rot="R270"/>
<instance part="V64" gate="G$1" x="243.84" y="170.18" rot="MR0"/>
<instance part="V3" gate="G$1" x="210.82" y="154.94" rot="MR0"/>
<instance part="PRUCHODKA15" gate="G$1" x="215.9" y="170.18"/>
<instance part="U$29" gate="GND" x="119.38" y="132.08"/>
<instance part="U$30" gate="GND" x="119.38" y="96.52"/>
<instance part="U$31" gate="G$1" x="96.52" y="25.4"/>
<instance part="SVORKA45" gate="G$1" x="134.62" y="180.34" rot="R270"/>
<instance part="TMC1" gate="23" x="76.2" y="203.2"/>
<instance part="TMC1" gate="28" x="76.2" y="177.8"/>
<instance part="TMC1" gate="30" x="76.2" y="180.34"/>
<instance part="TMC1" gate="26" x="76.2" y="205.74"/>
<instance part="SVORKA47" gate="G$1" x="370.84" y="106.68" rot="R270"/>
<instance part="SVORKA47" gate="G$2" x="370.84" y="96.52" rot="R270"/>
<instance part="SVORKA47" gate="G$3" x="370.84" y="86.36" rot="R270"/>
<instance part="SVORKA47" gate="G$4" x="370.84" y="106.68" rot="R270"/>
<instance part="SVORKA47" gate="G$5" x="370.84" y="96.52" rot="R270"/>
<instance part="SVORKA47" gate="G$6" x="370.84" y="86.36" rot="R270"/>
<instance part="SVORKA4" gate="G$1" x="134.62" y="203.2" rot="R270"/>
<instance part="SVORKA4" gate="G$2" x="134.62" y="203.2" rot="R270"/>
<instance part="SVORKA4" gate="G$4" x="134.62" y="203.2" rot="R270"/>
<instance part="PRUCHODKA29" gate="G$1" x="96.52" y="43.18" rot="R270"/>
<instance part="FRAME9" gate="G$3" x="287.02" y="40.64"/>
<instance part="RE_D2" gate="A" x="304.8" y="220.98" smashed="yes">
<attribute name="VALUE" x="296.545" y="223.901" size="1.524" layer="96"/>
<attribute name="PART" x="296.545" y="226.695" size="1.27" layer="95"/>
</instance>
<instance part="RE_D2" gate="B" x="320.04" y="223.52" rot="MR180"/>
<instance part="FA_D1" gate="G$1" x="195.58" y="241.3" rot="MR90"/>
<instance part="RE_D3" gate="A" x="332.74" y="220.98" smashed="yes">
<attribute name="VALUE" x="324.485" y="223.901" size="1.524" layer="96"/>
<attribute name="PART" x="324.485" y="226.695" size="1.27" layer="95"/>
</instance>
<instance part="R303" gate="G$1" x="275.59" y="215.9" smashed="yes" rot="R90">
<attribute name="NAME" x="271.4625" y="213.36" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="274.0025" y="213.36" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="R302" gate="G$1" x="288.29" y="195.58" smashed="yes" rot="MR90">
<attribute name="NAME" x="294.64" y="198.12" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="294.9575" y="195.58" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="R301" gate="G$1" x="278.13" y="195.58" smashed="yes" rot="MR90">
<attribute name="NAME" x="274.32" y="196.5325" size="1.778" layer="95" rot="MR0"/>
<attribute name="VALUE" x="274.32" y="193.9925" size="1.778" layer="96" rot="MR0"/>
</instance>
<instance part="GND1" gate="GND" x="276.86" y="182.88"/>
<instance part="RE_D4" gate="G$1" x="195.58" y="220.98" smashed="yes">
<attribute name="NAME" x="195.707" y="224.409" size="2.032" layer="95"/>
<attribute name="VALUE" x="196.342" y="215.138" size="2.032" layer="96"/>
</instance>
<instance part="U$97" gate="+24V" x="195.58" y="254" rot="MR0"/>
<instance part="U$100" gate="G$1" x="220.98" y="220.98" rot="R90"/>
<instance part="RE_D1" gate="B" x="347.98" y="223.52" rot="MR180"/>
<instance part="V5" gate="G$1" x="243.84" y="121.92" rot="MR0"/>
<instance part="V6" gate="G$1" x="243.84" y="111.76" rot="MR0"/>
<instance part="V4" gate="G$1" x="243.84" y="134.62" rot="MR0"/>
<instance part="PRUCHODKA33" gate="G$1" x="215.9" y="134.62"/>
<instance part="PRUCHODKA34" gate="G$1" x="215.9" y="121.92"/>
<instance part="PRUCHODKA35" gate="G$1" x="215.9" y="111.76"/>
<instance part="U$110" gate="G$1" x="96.52" y="147.32"/>
<instance part="S9" gate="G$1" x="33.02" y="147.32"/>
<instance part="U$111" gate="GND" x="96.52" y="132.08"/>
<instance part="S10" gate="G$1" x="66.04" y="147.32"/>
<instance part="SVORKA15" gate="G$1" x="370.84" y="116.84" rot="R270"/>
<instance part="S5" gate="A" x="363.22" y="162.56" rot="R90"/>
<instance part="S5" gate="G$1" x="350.52" y="162.56"/>
</instances>
<busses>
<bus name="LIGHTROZ,GNDLROZ">
<segment>
<wire x1="195.58" y1="154.94" x2="182.88" y2="154.94" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="LIGHT,GNDLIGHT">
<segment>
<wire x1="228.6" y1="170.18" x2="175.26" y2="170.18" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="PWR_D,BODY_D,D_READY,VENT_D,WHEEL_D,GNDDESI">
<segment>
<wire x1="96.52" y1="91.44" x2="96.52" y2="33.02" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="GND_REV,REV_LI">
<segment>
<wire x1="228.6" y1="134.62" x2="182.88" y2="134.62" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="BLIGHT,GNDBLIG">
<segment>
<wire x1="228.6" y1="111.76" x2="182.88" y2="111.76" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="LOADLIGHT,GNDLOLI">
<segment>
<wire x1="228.6" y1="121.92" x2="190.5" y2="121.92" width="0.762" layer="92"/>
</segment>
</bus>
</busses>
<nets>
<net name="PWR4" class="0">
<segment>
<pinref part="SVORKA36" gate="G$1" pin="1"/>
<wire x1="144.78" y1="157.48" x2="152.4" y2="157.48" width="0.1524" layer="91"/>
<label x="152.4" y="157.48" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="LIGHTROZ" class="0">
<segment>
<wire x1="182.88" y1="154.94" x2="180.34" y2="157.48" width="0.1524" layer="91"/>
<pinref part="SVORKA36" gate="G$1" pin="3"/>
<wire x1="180.34" y1="157.48" x2="180.34" y2="162.56" width="0.1524" layer="91"/>
<wire x1="180.34" y1="162.56" x2="139.7" y2="162.56" width="0.1524" layer="91"/>
<label x="147.32" y="162.56" size="1.778" layer="95"/>
</segment>
</net>
<net name="GNDLROZ" class="0">
<segment>
<wire x1="182.88" y1="154.94" x2="180.34" y2="152.4" width="0.1524" layer="91"/>
<wire x1="180.34" y1="152.4" x2="180.34" y2="144.78" width="0.1524" layer="91"/>
<pinref part="SVORKA13" gate="G$1" pin="3"/>
<wire x1="180.34" y1="144.78" x2="139.7" y2="144.78" width="0.1524" layer="91"/>
<label x="147.32" y="144.78" size="1.778" layer="95"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="U$29" gate="GND" pin="GND"/>
<wire x1="119.38" y1="137.16" x2="119.38" y2="139.7" width="0.1524" layer="91"/>
<pinref part="SVORKA13" gate="G$1" pin="6"/>
<wire x1="124.46" y1="139.7" x2="119.38" y2="139.7" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SVORKA14" gate="G$1" pin="6"/>
<wire x1="124.46" y1="104.14" x2="119.38" y2="104.14" width="0.1524" layer="91"/>
<pinref part="U$30" gate="GND" pin="GND"/>
<wire x1="119.38" y1="104.14" x2="119.38" y2="101.6" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="R301" gate="G$1" pin="1"/>
<pinref part="GND1" gate="GND" pin="GND"/>
<wire x1="276.86" y1="190.5" x2="276.86" y2="187.96" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$110" gate="G$1" pin="Z"/>
<pinref part="U$111" gate="GND" pin="GND"/>
</segment>
</net>
<net name="LIGHT" class="0">
<segment>
<wire x1="175.26" y1="170.18" x2="172.72" y2="172.72" width="0.1524" layer="91"/>
<wire x1="172.72" y1="172.72" x2="167.64" y2="172.72" width="0.1524" layer="91"/>
<wire x1="167.64" y1="172.72" x2="167.64" y2="160.02" width="0.1524" layer="91"/>
<pinref part="SVORKA36" gate="G$1" pin="2"/>
<wire x1="167.64" y1="160.02" x2="142.24" y2="160.02" width="0.1524" layer="91"/>
<label x="147.32" y="160.02" size="1.778" layer="95"/>
</segment>
</net>
<net name="GNDLIGHT" class="0">
<segment>
<wire x1="175.26" y1="170.18" x2="172.72" y2="167.64" width="0.1524" layer="91"/>
<wire x1="172.72" y1="167.64" x2="172.72" y2="142.24" width="0.1524" layer="91"/>
<pinref part="SVORKA13" gate="G$1" pin="2"/>
<wire x1="172.72" y1="142.24" x2="142.24" y2="142.24" width="0.1524" layer="91"/>
<label x="147.32" y="142.24" size="1.778" layer="95"/>
</segment>
</net>
<net name="D_READY" class="0">
<segment>
<pinref part="SVORKA41" gate="G$1" pin="4"/>
<wire x1="96.52" y1="86.36" x2="99.06" y2="88.9" width="0.1524" layer="91"/>
<wire x1="99.06" y1="88.9" x2="129.54" y2="88.9" width="0.1524" layer="91"/>
<label x="121.92" y="88.9" size="1.778" layer="95" rot="MR0"/>
</segment>
<segment>
<pinref part="SVORKA41" gate="G$1" pin="3"/>
<wire x1="139.7" y1="88.9" x2="287.02" y2="88.9" width="0.1524" layer="91"/>
<wire x1="287.02" y1="88.9" x2="287.02" y2="190.5" width="0.1524" layer="91"/>
<label x="147.32" y="88.9" size="1.778" layer="95"/>
<pinref part="R302" gate="G$1" pin="1"/>
</segment>
</net>
<net name="BODY_D" class="0">
<segment>
<pinref part="SVORKA41" gate="G$1" pin="5"/>
<wire x1="96.52" y1="83.82" x2="99.06" y2="86.36" width="0.1524" layer="91"/>
<wire x1="99.06" y1="86.36" x2="127" y2="86.36" width="0.1524" layer="91"/>
<label x="121.92" y="86.36" size="1.778" layer="95" rot="MR0"/>
</segment>
<segment>
<pinref part="SVORKA41" gate="G$1" pin="2"/>
<wire x1="142.24" y1="86.36" x2="312.42" y2="86.36" width="0.1524" layer="91"/>
<wire x1="312.42" y1="86.36" x2="312.42" y2="218.44" width="0.1524" layer="91"/>
<label x="147.32" y="86.36" size="1.778" layer="95"/>
<pinref part="RE_D2" gate="B" pin="S"/>
<wire x1="314.96" y1="218.44" x2="312.42" y2="218.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PWR_D" class="0">
<segment>
<pinref part="SVORKA41" gate="G$1" pin="6"/>
<wire x1="96.52" y1="81.28" x2="99.06" y2="83.82" width="0.1524" layer="91"/>
<wire x1="99.06" y1="83.82" x2="124.46" y2="83.82" width="0.1524" layer="91"/>
<label x="121.92" y="83.82" size="1.778" layer="95" rot="MR0"/>
</segment>
<segment>
<pinref part="U$100" gate="G$1" pin="CO"/>
<wire x1="220.98" y1="215.9" x2="220.98" y2="210.82" width="0.1524" layer="91"/>
<wire x1="220.98" y1="210.82" x2="259.08" y2="210.82" width="0.1524" layer="91"/>
<label x="226.06" y="210.82" size="1.778" layer="95"/>
<wire x1="259.08" y1="210.82" x2="259.08" y2="83.82" width="0.1524" layer="91"/>
<pinref part="SVORKA41" gate="G$1" pin="1"/>
<wire x1="259.08" y1="83.82" x2="144.78" y2="83.82" width="0.1524" layer="91"/>
<label x="147.32" y="83.82" size="1.778" layer="95"/>
</segment>
</net>
<net name="WHEEL_D" class="0">
<segment>
<pinref part="SVORKA43" gate="G$1" pin="5"/>
<wire x1="96.52" y1="63.5" x2="99.06" y2="66.04" width="0.1524" layer="91"/>
<wire x1="99.06" y1="66.04" x2="127" y2="66.04" width="0.1524" layer="91"/>
<label x="121.92" y="66.04" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="VENT_D" class="0">
<segment>
<pinref part="SVORKA43" gate="G$1" pin="6"/>
<wire x1="96.52" y1="60.96" x2="99.06" y2="63.5" width="0.1524" layer="91"/>
<wire x1="99.06" y1="63.5" x2="124.46" y2="63.5" width="0.1524" layer="91"/>
<label x="121.92" y="63.5" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="GNDDESI" class="0">
<segment>
<wire x1="96.52" y1="91.44" x2="99.06" y2="93.98" width="0.1524" layer="91"/>
<wire x1="99.06" y1="93.98" x2="165.1" y2="93.98" width="0.1524" layer="91"/>
<wire x1="165.1" y1="93.98" x2="165.1" y2="104.14" width="0.1524" layer="91"/>
<pinref part="SVORKA14" gate="G$1" pin="1"/>
<wire x1="165.1" y1="104.14" x2="144.78" y2="104.14" width="0.1524" layer="91"/>
<label x="147.32" y="104.14" size="1.778" layer="95"/>
</segment>
</net>
<net name="WHEELDIS" class="0">
<segment>
<pinref part="TMC1" gate="30" pin="WHEELDIS"/>
<pinref part="SVORKA45" gate="G$1" pin="5"/>
<wire x1="109.22" y1="180.34" x2="127" y2="180.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="BODYDIS" class="0">
<segment>
<pinref part="TMC1" gate="28" pin="BODYDIS"/>
<pinref part="SVORKA45" gate="G$1" pin="6"/>
<wire x1="109.22" y1="177.8" x2="124.46" y2="177.8" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SVORKA45" gate="G$1" pin="1"/>
<wire x1="144.78" y1="177.8" x2="304.8" y2="177.8" width="0.1524" layer="91"/>
<wire x1="304.8" y1="177.8" x2="304.8" y2="213.36" width="0.1524" layer="91"/>
<wire x1="304.8" y1="213.36" x2="332.74" y2="213.36" width="0.1524" layer="91"/>
<pinref part="RE_D3" gate="A" pin="2"/>
<wire x1="332.74" y1="213.36" x2="332.74" y2="215.9" width="0.1524" layer="91"/>
<pinref part="RE_D2" gate="A" pin="2"/>
<wire x1="304.8" y1="213.36" x2="304.8" y2="215.9" width="0.1524" layer="91"/>
<junction x="304.8" y="213.36"/>
<label x="149.86" y="177.8" size="1.778" layer="95"/>
</segment>
</net>
<net name="LOGO_ON" class="0">
<segment>
<pinref part="TMC1" gate="26" pin="LOGO_ON"/>
<wire x1="109.22" y1="205.74" x2="129.54" y2="205.74" width="0.1524" layer="91"/>
<pinref part="SVORKA4" gate="G$1" pin="4"/>
</segment>
</net>
<net name="DIS_READY" class="0">
<segment>
<pinref part="TMC1" gate="23" pin="DIS_READY"/>
<wire x1="109.22" y1="203.2" x2="127" y2="203.2" width="0.1524" layer="91"/>
<pinref part="SVORKA4" gate="G$2" pin="5"/>
</segment>
<segment>
<pinref part="R303" gate="G$1" pin="1"/>
<pinref part="R301" gate="G$1" pin="2"/>
<pinref part="R302" gate="G$1" pin="2"/>
<pinref part="SVORKA4" gate="G$2" pin="2"/>
<wire x1="142.24" y1="203.2" x2="167.64" y2="203.2" width="0.1524" layer="91"/>
<label x="149.86" y="203.2" size="1.778" layer="95"/>
<wire x1="167.64" y1="203.2" x2="276.86" y2="203.2" width="0.1524" layer="91"/>
<wire x1="276.86" y1="200.66" x2="276.86" y2="203.2" width="0.1524" layer="91"/>
<junction x="276.86" y="203.2"/>
<wire x1="276.86" y1="203.2" x2="276.86" y2="210.82" width="0.1524" layer="91"/>
<wire x1="276.86" y1="203.2" x2="287.02" y2="203.2" width="0.1524" layer="91"/>
<wire x1="287.02" y1="203.2" x2="287.02" y2="200.66" width="0.1524" layer="91"/>
<wire x1="167.64" y1="203.2" x2="167.64" y2="180.34" width="0.1524" layer="91"/>
<junction x="167.64" y="203.2"/>
<pinref part="SVORKA45" gate="G$1" pin="2"/>
<wire x1="167.64" y1="180.34" x2="142.24" y2="180.34" width="0.1524" layer="91"/>
<label x="149.86" y="180.34" size="1.778" layer="95"/>
</segment>
</net>
<net name="+24V" class="0">
<segment>
<wire x1="195.58" y1="251.46" x2="195.58" y2="248.92" width="0.1524" layer="91"/>
<pinref part="U$97" gate="+24V" pin="+24V"/>
</segment>
</net>
<net name="FAIL2" class="0">
<segment>
<pinref part="RE_D1" gate="B" pin="P"/>
<wire x1="347.98" y1="226.06" x2="347.98" y2="231.14" width="0.1524" layer="91"/>
<wire x1="347.98" y1="231.14" x2="365.76" y2="231.14" width="0.1524" layer="91"/>
<label x="365.76" y="231.14" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="FAN_100%" class="0">
<segment>
<pinref part="S5" gate="A" pin="I"/>
<wire x1="365.76" y1="172.72" x2="365.76" y2="180.34" width="0.1524" layer="91"/>
<wire x1="365.76" y1="180.34" x2="332.74" y2="180.34" width="0.1524" layer="91"/>
<label x="332.74" y="180.34" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="FAN_50%" class="0">
<segment>
<pinref part="S5" gate="A" pin="II"/>
<wire x1="368.3" y1="172.72" x2="368.3" y2="185.42" width="0.1524" layer="91"/>
<wire x1="368.3" y1="185.42" x2="340.36" y2="185.42" width="0.1524" layer="91"/>
<label x="332.74" y="185.42" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="RE_D1" gate="B" pin="S"/>
<wire x1="340.36" y1="185.42" x2="332.74" y2="185.42" width="0.1524" layer="91"/>
<wire x1="342.9" y1="218.44" x2="340.36" y2="218.44" width="0.1524" layer="91"/>
<wire x1="340.36" y1="218.44" x2="340.36" y2="185.42" width="0.1524" layer="91"/>
<junction x="340.36" y="185.42"/>
</segment>
</net>
<net name="POW_B" class="0">
<segment>
<pinref part="R303" gate="G$1" pin="2"/>
<wire x1="195.58" y1="228.6" x2="195.58" y2="231.14" width="0.1524" layer="91"/>
<label x="203.2" y="231.14" size="1.778" layer="95"/>
<wire x1="195.58" y1="231.14" x2="195.58" y2="233.68" width="0.1524" layer="91"/>
<wire x1="195.58" y1="231.14" x2="220.98" y2="231.14" width="0.1524" layer="91"/>
<junction x="195.58" y="231.14"/>
<pinref part="U$100" gate="G$1" pin="NO"/>
<wire x1="220.98" y1="226.06" x2="220.98" y2="231.14" width="0.1524" layer="91"/>
<pinref part="RE_D4" gate="G$1" pin="2"/>
<wire x1="276.86" y1="220.98" x2="276.86" y2="231.14" width="0.1524" layer="91"/>
<wire x1="276.86" y1="231.14" x2="220.98" y2="231.14" width="0.1524" layer="91"/>
<junction x="220.98" y="231.14"/>
<pinref part="RE_D2" gate="A" pin="1"/>
<wire x1="276.86" y1="231.14" x2="304.8" y2="231.14" width="0.1524" layer="91"/>
<wire x1="304.8" y1="231.14" x2="304.8" y2="226.06" width="0.1524" layer="91"/>
<junction x="276.86" y="231.14"/>
<pinref part="RE_D2" gate="B" pin="P"/>
<wire x1="304.8" y1="231.14" x2="320.04" y2="231.14" width="0.1524" layer="91"/>
<wire x1="320.04" y1="231.14" x2="320.04" y2="226.06" width="0.1524" layer="91"/>
<junction x="304.8" y="231.14"/>
<pinref part="RE_D3" gate="A" pin="1"/>
<wire x1="320.04" y1="231.14" x2="332.74" y2="231.14" width="0.1524" layer="91"/>
<wire x1="332.74" y1="231.14" x2="332.74" y2="226.06" width="0.1524" layer="91"/>
<junction x="320.04" y="231.14"/>
</segment>
</net>
<net name="S3-1" class="0">
<segment>
<pinref part="RE_D1" gate="B" pin="O"/>
<wire x1="353.06" y1="218.44" x2="355.6" y2="218.44" width="0.1524" layer="91"/>
<wire x1="355.6" y1="218.44" x2="355.6" y2="205.74" width="0.1524" layer="91"/>
<wire x1="355.6" y1="205.74" x2="373.38" y2="205.74" width="0.1524" layer="91"/>
<wire x1="373.38" y1="205.74" x2="373.38" y2="147.32" width="0.1524" layer="91"/>
<pinref part="S5" gate="A" pin="2"/>
<wire x1="368.3" y1="152.4" x2="368.3" y2="147.32" width="0.1524" layer="91"/>
<wire x1="368.3" y1="147.32" x2="365.76" y2="147.32" width="0.1524" layer="91"/>
<pinref part="S5" gate="A" pin="1"/>
<wire x1="365.76" y1="152.4" x2="365.76" y2="147.32" width="0.1524" layer="91"/>
<wire x1="373.38" y1="147.32" x2="368.3" y2="147.32" width="0.1524" layer="91"/>
<junction x="368.3" y="147.32"/>
<label x="363.22" y="205.74" size="1.778" layer="95"/>
</segment>
</net>
<net name="DIS_ON" class="0">
<segment>
<wire x1="139.7" y1="205.74" x2="195.58" y2="205.74" width="0.1524" layer="91"/>
<pinref part="RE_D4" gate="G$1" pin="1"/>
<wire x1="195.58" y1="205.74" x2="195.58" y2="213.36" width="0.1524" layer="91"/>
<pinref part="SVORKA4" gate="G$1" pin="3"/>
<label x="149.86" y="205.74" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$12" class="0">
<segment>
<pinref part="FA_D1" gate="G$1" pin="2A"/>
<pinref part="FA_D1" gate="G$1" pin="2B"/>
<pinref part="FA_D1" gate="G$1" pin="2C"/>
<pinref part="FA_D1" gate="G$1" pin="2D"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="FA_D1" gate="G$1" pin="1A"/>
<pinref part="FA_D1" gate="G$1" pin="1B"/>
<pinref part="FA_D1" gate="G$1" pin="1C"/>
<pinref part="FA_D1" gate="G$1" pin="1D"/>
</segment>
</net>
<net name="!VENTILATION_ON" class="0">
<segment>
<pinref part="SVORKA43" gate="G$1" pin="1"/>
<wire x1="144.78" y1="63.5" x2="165.1" y2="63.5" width="0.1524" layer="91"/>
<label x="165.1" y="63.5" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="GND_REV" class="0">
<segment>
<wire x1="182.88" y1="134.62" x2="180.34" y2="137.16" width="0.1524" layer="91"/>
<wire x1="180.34" y1="137.16" x2="180.34" y2="139.7" width="0.1524" layer="91"/>
<pinref part="SVORKA13" gate="G$1" pin="1"/>
<wire x1="180.34" y1="139.7" x2="144.78" y2="139.7" width="0.1524" layer="91"/>
<label x="147.32" y="139.7" size="1.778" layer="95"/>
</segment>
</net>
<net name="REV_LI" class="0">
<segment>
<wire x1="182.88" y1="134.62" x2="180.34" y2="132.08" width="0.1524" layer="91"/>
<wire x1="180.34" y1="132.08" x2="180.34" y2="121.92" width="0.1524" layer="91"/>
<pinref part="SVORKA40" gate="G$1" pin="1"/>
<wire x1="180.34" y1="121.92" x2="144.78" y2="121.92" width="0.1524" layer="91"/>
<label x="147.32" y="121.92" size="1.778" layer="95"/>
</segment>
</net>
<net name="GNDBLIG" class="0">
<segment>
<wire x1="182.88" y1="111.76" x2="180.34" y2="109.22" width="0.1524" layer="91"/>
<wire x1="180.34" y1="109.22" x2="180.34" y2="106.68" width="0.1524" layer="91"/>
<pinref part="SVORKA14" gate="G$1" pin="2"/>
<wire x1="180.34" y1="106.68" x2="142.24" y2="106.68" width="0.1524" layer="91"/>
<label x="147.32" y="106.68" size="1.778" layer="95"/>
</segment>
</net>
<net name="GNDLOLI" class="0">
<segment>
<wire x1="190.5" y1="121.92" x2="187.96" y2="119.38" width="0.1524" layer="91"/>
<wire x1="187.96" y1="119.38" x2="187.96" y2="116.84" width="0.1524" layer="91"/>
<wire x1="187.96" y1="116.84" x2="165.1" y2="116.84" width="0.1524" layer="91"/>
<wire x1="165.1" y1="116.84" x2="165.1" y2="109.22" width="0.1524" layer="91"/>
<pinref part="SVORKA14" gate="G$1" pin="3"/>
<wire x1="165.1" y1="109.22" x2="139.7" y2="109.22" width="0.1524" layer="91"/>
<label x="147.32" y="109.22" size="1.778" layer="95"/>
</segment>
</net>
<net name="BLIGHT" class="0">
<segment>
<wire x1="182.88" y1="111.76" x2="180.34" y2="114.3" width="0.1524" layer="91"/>
<wire x1="180.34" y1="114.3" x2="172.72" y2="114.3" width="0.1524" layer="91"/>
<wire x1="172.72" y1="114.3" x2="172.72" y2="124.46" width="0.1524" layer="91"/>
<pinref part="SVORKA40" gate="G$1" pin="2"/>
<wire x1="172.72" y1="124.46" x2="142.24" y2="124.46" width="0.1524" layer="91"/>
<label x="147.32" y="124.46" size="1.778" layer="95"/>
</segment>
</net>
<net name="LOADLIGHT" class="0">
<segment>
<wire x1="190.5" y1="121.92" x2="187.96" y2="124.46" width="0.1524" layer="91"/>
<wire x1="187.96" y1="124.46" x2="187.96" y2="127" width="0.1524" layer="91"/>
<pinref part="SVORKA40" gate="G$1" pin="3"/>
<wire x1="187.96" y1="127" x2="139.7" y2="127" width="0.1524" layer="91"/>
<label x="147.32" y="127" size="1.778" layer="95"/>
</segment>
</net>
<net name="SW3" class="0">
<segment>
<pinref part="SVORKA40" gate="G$1" pin="4"/>
<label x="111.76" y="127" size="1.778" layer="95"/>
<pinref part="S10" gate="G$1" pin="B"/>
<wire x1="76.2" y1="149.86" x2="78.74" y2="149.86" width="0.1524" layer="91"/>
<wire x1="78.74" y1="149.86" x2="78.74" y2="144.78" width="0.1524" layer="91"/>
<pinref part="S10" gate="G$1" pin="E"/>
<wire x1="78.74" y1="144.78" x2="76.2" y2="144.78" width="0.1524" layer="91"/>
<wire x1="129.54" y1="127" x2="78.74" y2="127" width="0.1524" layer="91"/>
<wire x1="78.74" y1="127" x2="78.74" y2="144.78" width="0.1524" layer="91"/>
<junction x="78.74" y="144.78"/>
</segment>
</net>
<net name="SW2" class="0">
<segment>
<pinref part="SVORKA40" gate="G$1" pin="5"/>
<label x="111.76" y="124.46" size="1.778" layer="95"/>
<pinref part="S9" gate="G$1" pin="B"/>
<wire x1="43.18" y1="149.86" x2="45.72" y2="149.86" width="0.1524" layer="91"/>
<wire x1="45.72" y1="149.86" x2="45.72" y2="144.78" width="0.1524" layer="91"/>
<pinref part="S9" gate="G$1" pin="E"/>
<wire x1="45.72" y1="144.78" x2="43.18" y2="144.78" width="0.1524" layer="91"/>
<wire x1="127" y1="124.46" x2="45.72" y2="124.46" width="0.1524" layer="91"/>
<wire x1="45.72" y1="124.46" x2="45.72" y2="144.78" width="0.1524" layer="91"/>
<junction x="45.72" y="144.78"/>
</segment>
</net>
<net name="SW4" class="0">
<segment>
<pinref part="SVORKA40" gate="G$1" pin="6"/>
<wire x1="124.46" y1="121.92" x2="109.22" y2="121.92" width="0.1524" layer="91"/>
<label x="111.76" y="121.92" size="1.778" layer="95"/>
</segment>
</net>
<net name="SW1" class="0">
<segment>
<pinref part="SVORKA36" gate="G$1" pin="4"/>
<label x="111.76" y="162.56" size="1.778" layer="95"/>
<wire x1="129.54" y1="162.56" x2="20.32" y2="162.56" width="0.1524" layer="91"/>
<wire x1="20.32" y1="162.56" x2="20.32" y2="149.86" width="0.1524" layer="91"/>
<pinref part="S9" gate="G$1" pin="A"/>
<wire x1="20.32" y1="149.86" x2="22.86" y2="149.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SVORKA36" gate="G$1" pin="5"/>
<label x="111.76" y="160.02" size="1.778" layer="95"/>
<wire x1="127" y1="160.02" x2="53.34" y2="160.02" width="0.1524" layer="91"/>
<wire x1="53.34" y1="160.02" x2="53.34" y2="149.86" width="0.1524" layer="91"/>
<pinref part="S10" gate="G$1" pin="A"/>
<wire x1="53.34" y1="149.86" x2="55.88" y2="149.86" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="SVORKA36" gate="G$1" pin="6"/>
<wire x1="124.46" y1="157.48" x2="96.52" y2="157.48" width="0.1524" layer="91"/>
<label x="111.76" y="157.48" size="1.778" layer="95"/>
<pinref part="U$110" gate="G$1" pin="S"/>
</segment>
</net>
<net name="GNDS1" class="0">
<segment>
<pinref part="SVORKA14" gate="G$1" pin="5"/>
<label x="111.76" y="106.68" size="1.778" layer="95"/>
<wire x1="127" y1="106.68" x2="20.32" y2="106.68" width="0.1524" layer="91"/>
<wire x1="20.32" y1="106.68" x2="20.32" y2="144.78" width="0.1524" layer="91"/>
<pinref part="S9" gate="G$1" pin="F"/>
<wire x1="20.32" y1="144.78" x2="22.86" y2="144.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GNDS12" class="0">
<segment>
<pinref part="SVORKA14" gate="G$1" pin="4"/>
<label x="111.76" y="109.22" size="1.778" layer="95"/>
<wire x1="129.54" y1="109.22" x2="53.34" y2="109.22" width="0.1524" layer="91"/>
<wire x1="53.34" y1="109.22" x2="53.34" y2="144.78" width="0.1524" layer="91"/>
<pinref part="S10" gate="G$1" pin="F"/>
<wire x1="53.34" y1="144.78" x2="55.88" y2="144.78" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<description>Seznam svorek (rejstřík)</description>
<plain>
</plain>
<instances>
<instance part="FRAME12" gate="G$1" x="0" y="0"/>
<instance part="FRAME12" gate="G$2" x="287.02" y="0"/>
<instance part="SVORKA43" gate="G$2" x="299.72" y="137.16"/>
<instance part="SVORKA41" gate="G$2" x="292.1" y="137.16"/>
<instance part="SVORKA40" gate="G$2" x="276.86" y="137.16"/>
<instance part="SVORKA45" gate="G$2" x="93.98" y="137.16"/>
<instance part="SVORKA4" gate="G$7" x="86.36" y="137.16"/>
<instance part="SVORKA14" gate="G$2" x="284.48" y="137.16"/>
<instance part="SVORKA13" gate="G$2" x="269.24" y="137.16"/>
<instance part="SVORKA36" gate="G$2" x="261.62" y="137.16"/>
<instance part="SVORKA48" gate="G$2" x="307.34" y="137.16"/>
<instance part="SVORKA6" gate="G$2" x="314.96" y="137.16"/>
<instance part="SVORKA7" gate="G$2" x="322.58" y="137.16"/>
<instance part="SVORKA23" gate="G$2" x="63.5" y="137.16"/>
<instance part="SVORKA37" gate="G$2" x="223.52" y="137.16"/>
<instance part="U$8" gate="G$2" x="330.2" y="137.16"/>
<instance part="SVORKA42" gate="G$2" x="337.82" y="137.16"/>
<instance part="SVORKA8" gate="G$2" x="345.44" y="137.16"/>
<instance part="U$12" gate="G$2" x="353.06" y="137.16"/>
<instance part="U$11" gate="G$2" x="360.68" y="137.16"/>
<instance part="U$5" gate="G$2" x="368.3" y="137.16"/>
<instance part="SVORKA19" gate="G$2" x="254" y="137.16"/>
<instance part="SVORKA44" gate="G$2" x="246.38" y="137.16"/>
<instance part="SVORKA12" gate="G$2" x="238.76" y="137.16"/>
<instance part="SVORKA46" gate="G$2" x="231.14" y="137.16"/>
<instance part="SVORKA2" gate="G$2" x="215.9" y="137.16"/>
<instance part="SVORKA35" gate="G$2" x="208.28" y="137.16"/>
<instance part="SVORKA5" gate="G$2" x="55.88" y="137.16"/>
<instance part="SVORKA20" gate="G$2" x="124.46" y="137.16"/>
<instance part="SVORKA25" gate="G$2" x="10.16" y="137.16"/>
<instance part="SVORKA26" gate="G$2" x="17.78" y="137.16"/>
<instance part="SVORKA10" gate="G$2" x="25.4" y="137.16"/>
<instance part="SVORKA30" gate="G$2" x="139.7" y="137.16"/>
<instance part="SVORKA29" gate="G$2" x="147.32" y="137.16"/>
<instance part="SVORKA9" gate="G$2" x="154.94" y="137.16"/>
<instance part="SVORKA28" gate="G$2" x="162.56" y="137.16"/>
<instance part="SVORKA31" gate="G$2" x="170.18" y="137.16"/>
<instance part="SVORKA32" gate="G$2" x="177.8" y="137.16"/>
<instance part="SVORKA33" gate="G$2" x="185.42" y="137.16"/>
<instance part="SVORKA11" gate="G$2" x="193.04" y="137.16"/>
<instance part="SVORKA1" gate="G$2" x="200.66" y="137.16"/>
<instance part="SVORKA15" gate="G$2" x="375.92" y="137.16"/>
<instance part="SVORKA22" gate="G$2" x="71.12" y="137.16"/>
<instance part="SVORKA27" gate="G$2" x="101.6" y="137.16"/>
<instance part="SVORKA21" gate="G$2" x="109.22" y="137.16"/>
<instance part="SVORKA24" gate="G$2" x="116.84" y="137.16"/>
<instance part="SVORKA17" gate="G$2" x="40.64" y="137.16"/>
<instance part="SVORKA18" gate="G$2" x="48.26" y="137.16"/>
<instance part="SVORKA34" gate="G$2" x="132.08" y="137.16"/>
<instance part="SVORKA16" gate="G$2" x="33.02" y="137.16"/>
<instance part="SVORKA3" gate="G$2" x="78.74" y="137.16"/>
</instances>
<busses>
</busses>
<nets>
</nets>
</sheet>
<sheet>
<description>Pokyny</description>
<plain>
<text x="2.54" y="261.62" size="3.81" layer="91" align="top-left">Postup:
---------
 - nastavit globální atributy: &gt;VEHICLE_NAME, &gt;VEHICLE_NUMBER, &gt;ABRA ("&gt;" se při definici nezadává)
 - nastavit &gt;VALUE u každého rámu ve všech listech schématu (kontrolováno pomocí ERC)
 - nastavit "Description" u všech listů schématu
 - velké číslo na 3patrové svorkovnici je pořadí svorky v rozvaděči zleva
 - číslování jednotlivých kontaktů na 3patrové svorkovnice je zhora dolů (1, 2, ..., 6)
 - umístění svorek na jednotlivých listech je na listu "Seznam svorek"</text>
<text x="2.54" y="127" size="3.81" layer="91" align="top-left">Kroky:
--------
 - vygenerovat seznam aut, u kterých chybí schémata (i plánovaná auta cca do 1/2 2018)
 - vytvořit MATRIX výbavy auta, včetně veškerých podrobností (počet a typ ventilátorů, verze desky relé, ...)
 - prvky MATRIXu doplnit o číslo skladové karty (+typ)</text>
<text x="299.72" y="241.3" size="3.81" layer="94">Nezapojené piny TMC:</text>
</plain>
<instances>
<instance part="FRAME3" gate="G$1" x="0" y="0"/>
<instance part="FRAME3" gate="G$2" x="287.02" y="0"/>
<instance part="TMC1" gate="1" x="312.42" y="233.68"/>
<instance part="TMC1" gate="2" x="312.42" y="231.14"/>
<instance part="TMC1" gate="3" x="312.42" y="228.6"/>
<instance part="TMC1" gate="4" x="312.42" y="226.06"/>
<instance part="TMC1" gate="6" x="312.42" y="223.52"/>
<instance part="TMC1" gate="8" x="312.42" y="220.98"/>
<instance part="TMC1" gate="10" x="312.42" y="218.44"/>
<instance part="TMC1" gate="12" x="312.42" y="215.9"/>
<instance part="TMC1" gate="14" x="312.42" y="213.36"/>
<instance part="TMC1" gate="16" x="312.42" y="210.82"/>
<instance part="TMC1" gate="18" x="312.42" y="208.28"/>
<instance part="TMC1" gate="33" x="312.42" y="205.74"/>
<instance part="TMC1" gate="42" x="312.42" y="203.2"/>
<instance part="TMC1" gate="55" x="312.42" y="200.66"/>
<instance part="TMC1" gate="56" x="312.42" y="198.12"/>
</instances>
<busses>
</busses>
<nets>
</nets>
</sheet>
<sheet>
<description>pro DEUTZ</description>
<plain>
</plain>
<instances>
<instance part="SVORKA6" gate="G$1" x="106.68" y="68.58" rot="R270"/>
<instance part="SVORKA7" gate="G$1" x="106.68" y="53.34" rot="R270"/>
<instance part="SVORKA48" gate="G$1" x="106.68" y="83.82" rot="R270"/>
<instance part="V73" gate="G$1" x="12.7" y="50.8" smashed="yes">
<attribute name="VALUE" x="13.335" y="50.038" size="1.27" layer="96"/>
</instance>
<instance part="V95" gate="G$1" x="12.7" y="86.36" smashed="yes">
<attribute name="VALUE" x="13.335" y="85.598" size="1.524" layer="96"/>
</instance>
<instance part="V96" gate="G$1" x="12.7" y="68.58" smashed="yes">
<attribute name="VALUE" x="27.305" y="67.818" size="1.4224" layer="96" rot="MR0"/>
</instance>
</instances>
<busses>
<bus name="WATERIN-[1..2]">
<segment>
<wire x1="27.94" y1="50.8" x2="71.12" y2="50.8" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="AIRFILT-[1..2]">
<segment>
<wire x1="27.94" y1="86.36" x2="71.12" y2="86.36" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="CLEVEL-GND,CLEVEL-SUP,CLEVEL-SIG">
<segment>
<wire x1="27.94" y1="68.58" x2="71.12" y2="68.58" width="0.762" layer="92"/>
</segment>
</bus>
</busses>
<nets>
<net name="X23.1.02" class="0">
<segment>
<pinref part="SVORKA48" gate="G$1" pin="2"/>
<wire x1="114.3" y1="83.82" x2="147.32" y2="83.82" width="0.1524" layer="91"/>
<label x="119.38" y="83.82" size="1.778" layer="95"/>
</segment>
</net>
<net name="X23.1.37" class="0">
<segment>
<pinref part="SVORKA6" gate="G$1" pin="3"/>
<wire x1="111.76" y1="71.12" x2="147.32" y2="71.12" width="0.1524" layer="91"/>
<label x="119.38" y="71.12" size="1.778" layer="95"/>
</segment>
</net>
<net name="X23.1.52" class="0">
<segment>
<pinref part="SVORKA6" gate="G$1" pin="2"/>
<wire x1="114.3" y1="68.58" x2="147.32" y2="68.58" width="0.1524" layer="91"/>
<label x="119.38" y="68.58" size="1.778" layer="95"/>
</segment>
</net>
<net name="X23.1.40" class="0">
<segment>
<pinref part="SVORKA6" gate="G$1" pin="1"/>
<wire x1="116.84" y1="66.04" x2="147.32" y2="66.04" width="0.1524" layer="91"/>
<label x="119.38" y="66.04" size="1.778" layer="95"/>
</segment>
</net>
<net name="X23.1.03" class="0">
<segment>
<pinref part="SVORKA7" gate="G$1" pin="2"/>
<wire x1="114.3" y1="53.34" x2="147.32" y2="53.34" width="0.1524" layer="91"/>
<label x="119.38" y="53.34" size="1.778" layer="95"/>
</segment>
</net>
<net name="X23.1.13" class="0">
<segment>
<pinref part="SVORKA7" gate="G$1" pin="1"/>
<wire x1="116.84" y1="50.8" x2="147.32" y2="50.8" width="0.1524" layer="91"/>
<label x="119.38" y="50.8" size="1.778" layer="95"/>
</segment>
</net>
<net name="AIRFILT-1" class="0">
<segment>
<pinref part="SVORKA48" gate="G$1" pin="4"/>
<wire x1="71.12" y1="86.36" x2="101.6" y2="86.36" width="0.1524" layer="91"/>
<label x="93.98" y="86.36" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="AIRFILT-2" class="0">
<segment>
<wire x1="71.12" y1="86.36" x2="73.66" y2="83.82" width="0.1524" layer="91"/>
<wire x1="73.66" y1="83.82" x2="73.66" y2="71.12" width="0.1524" layer="91"/>
<pinref part="SVORKA6" gate="G$1" pin="4"/>
<wire x1="73.66" y1="71.12" x2="101.6" y2="71.12" width="0.1524" layer="91"/>
<label x="93.98" y="71.12" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="WATERIN-2" class="0">
<segment>
<pinref part="SVORKA7" gate="G$1" pin="6"/>
<wire x1="71.12" y1="50.8" x2="96.52" y2="50.8" width="0.1524" layer="91"/>
<label x="93.98" y="50.8" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="WATERIN-1" class="0">
<segment>
<wire x1="71.12" y1="50.8" x2="73.66" y2="53.34" width="0.1524" layer="91"/>
<pinref part="SVORKA7" gate="G$1" pin="5"/>
<wire x1="73.66" y1="53.34" x2="99.06" y2="53.34" width="0.1524" layer="91"/>
<label x="93.98" y="53.34" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="CLEVEL-SUP" class="0">
<segment>
<pinref part="SVORKA6" gate="G$1" pin="5"/>
<wire x1="71.12" y1="68.58" x2="99.06" y2="68.58" width="0.1524" layer="91"/>
<label x="93.98" y="68.58" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="CLEVEL-GND" class="0">
<segment>
<wire x1="71.12" y1="68.58" x2="71.12" y2="81.28" width="0.1524" layer="91"/>
<wire x1="71.12" y1="81.28" x2="78.74" y2="81.28" width="0.1524" layer="91"/>
<wire x1="78.74" y1="81.28" x2="78.74" y2="83.82" width="0.1524" layer="91"/>
<pinref part="SVORKA48" gate="G$1" pin="5"/>
<wire x1="78.74" y1="83.82" x2="99.06" y2="83.82" width="0.1524" layer="91"/>
<label x="93.98" y="83.82" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="CLEVEL-SIG" class="0">
<segment>
<wire x1="71.12" y1="68.58" x2="73.66" y2="66.04" width="0.1524" layer="91"/>
<pinref part="SVORKA6" gate="G$1" pin="6"/>
<wire x1="73.66" y1="66.04" x2="96.52" y2="66.04" width="0.1524" layer="91"/>
<label x="93.98" y="66.04" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
</nets>
</sheet>
<sheet>
<description>přepínače</description>
<plain>
<wire x1="69.85" y1="47.625" x2="66.675" y2="54.61" width="0.1524" layer="94"/>
<wire x1="92.71" y1="47.625" x2="88.9" y2="54.61" width="0.1524" layer="94"/>
<wire x1="81.28" y1="47.625" x2="77.47" y2="54.61" width="0.1524" layer="94"/>
<wire x1="52.07" y1="47.625" x2="48.895" y2="54.61" width="0.1524" layer="96"/>
<wire x1="52.07" y1="47.625" x2="55.88" y2="54.61" width="0.1524" layer="96"/>
<wire x1="106.68" y1="47.625" x2="103.505" y2="54.61" width="0.1524" layer="94"/>
<wire x1="129.54" y1="47.625" x2="125.73" y2="54.61" width="0.1524" layer="94"/>
<wire x1="118.11" y1="47.625" x2="114.3" y2="54.61" width="0.1524" layer="94"/>
<wire x1="55.245" y1="51.435" x2="57.15" y2="51.435" width="0.1524" layer="96"/>
<wire x1="58.7375" y1="51.435" x2="60.6425" y2="51.435" width="0.1524" layer="96"/>
<wire x1="76.2" y1="51.435" x2="78.105" y2="51.435" width="0.1524" layer="96"/>
<wire x1="68.8975" y1="51.435" x2="70.8025" y2="51.435" width="0.1524" layer="96"/>
<wire x1="72.39" y1="51.435" x2="74.295" y2="51.435" width="0.1524" layer="96"/>
<wire x1="87.63" y1="51.435" x2="89.535" y2="51.435" width="0.1524" layer="96"/>
<wire x1="80.3275" y1="51.435" x2="82.2325" y2="51.435" width="0.1524" layer="96"/>
<wire x1="83.82" y1="51.435" x2="85.725" y2="51.435" width="0.1524" layer="96"/>
<wire x1="99.06" y1="51.435" x2="100.965" y2="51.435" width="0.1524" layer="96"/>
<wire x1="91.7575" y1="51.435" x2="93.6625" y2="51.435" width="0.1524" layer="96"/>
<wire x1="95.25" y1="51.435" x2="97.155" y2="51.435" width="0.1524" layer="96"/>
<wire x1="113.03" y1="51.435" x2="114.935" y2="51.435" width="0.1524" layer="96"/>
<wire x1="105.7275" y1="51.435" x2="107.6325" y2="51.435" width="0.1524" layer="96"/>
<wire x1="109.22" y1="51.435" x2="111.125" y2="51.435" width="0.1524" layer="96"/>
<wire x1="124.46" y1="51.435" x2="126.365" y2="51.435" width="0.1524" layer="96"/>
<wire x1="117.1575" y1="51.435" x2="119.0625" y2="51.435" width="0.1524" layer="96"/>
<wire x1="120.65" y1="51.435" x2="122.555" y2="51.435" width="0.1524" layer="96"/>
<wire x1="65.405" y1="51.435" x2="67.31" y2="51.435" width="0.1524" layer="96"/>
<wire x1="102.235" y1="51.435" x2="104.14" y2="51.435" width="0.1524" layer="96"/>
<wire x1="62.23" y1="51.435" x2="64.135" y2="51.435" width="0.1524" layer="96"/>
<circle x="66.675" y="54.61" radius="1.27" width="0.1524" layer="94"/>
<circle x="69.85" y="47.625" radius="1.27" width="0.1524" layer="94"/>
<circle x="77.47" y="54.61" radius="1.27" width="0.1524" layer="94"/>
<circle x="81.28" y="47.625" radius="1.27" width="0.1524" layer="94"/>
<circle x="96.52" y="54.61" radius="1.27" width="0.1524" layer="94"/>
<circle x="92.71" y="47.625" radius="1.27" width="0.1524" layer="94"/>
<circle x="52.07" y="47.625" radius="1.27" width="0.1524" layer="96"/>
<circle x="103.505" y="54.61" radius="1.27" width="0.1524" layer="94"/>
<circle x="106.68" y="47.625" radius="1.27" width="0.1524" layer="94"/>
<circle x="121.92" y="54.61" radius="1.27" width="0.1524" layer="94"/>
<circle x="118.11" y="47.625" radius="1.27" width="0.1524" layer="94"/>
<circle x="133.35" y="54.61" radius="1.27" width="0.1524" layer="94"/>
<circle x="129.54" y="47.625" radius="1.27" width="0.1524" layer="94"/>
<circle x="73.66" y="54.61" radius="1.27" width="0.1524" layer="94"/>
<circle x="88.9" y="54.61" radius="1.27" width="0.1524" layer="94"/>
<circle x="85.09" y="54.61" radius="1.27" width="0.1524" layer="94"/>
<circle x="110.49" y="54.61" radius="1.27" width="0.1524" layer="94"/>
<circle x="114.3" y="54.61" radius="1.27" width="0.1524" layer="94"/>
<circle x="125.73" y="54.61" radius="1.27" width="0.1524" layer="94"/>
<circle x="110.49" y="54.61" radius="1.27" width="0.1524" layer="94"/>
<text x="43.815" y="55.245" size="1.9304" layer="96">HEAT1</text>
<text x="53.34" y="55.245" size="1.9304" layer="96">HEAT2</text>
<text x="69.215" y="44.45" size="1.4224" layer="95" rot="MR0">1</text>
<text x="66.04" y="56.515" size="1.4224" layer="95" rot="MR0">2</text>
<text x="80.645" y="44.45" size="1.4224" layer="95" rot="MR0">5</text>
<text x="73.025" y="56.515" size="1.4224" layer="95" rot="MR0">4</text>
<text x="92.075" y="44.45" size="1.4224" layer="95" rot="MR0">9</text>
<text x="95.885" y="56.515" size="1.4224" layer="95" rot="MR0">12</text>
<text x="46.99" y="44.7675" size="1.778" layer="95">S4</text>
<text x="46.99" y="42.545" size="1.778" layer="95">VS 16 2256 A8</text>
<text x="106.045" y="44.45" size="1.4224" layer="95" rot="MR0">13</text>
<text x="102.87" y="56.515" size="1.4224" layer="95" rot="MR0">14</text>
<text x="117.475" y="44.45" size="1.4224" layer="95" rot="MR0">17</text>
<text x="121.285" y="56.515" size="1.4224" layer="95" rot="MR0">20</text>
<text x="128.905" y="44.45" size="1.4224" layer="95" rot="MR0">21</text>
<text x="132.715" y="56.515" size="1.4224" layer="95" rot="MR0">24</text>
<text x="76.835" y="56.515" size="1.4224" layer="95" rot="MR0">6</text>
<text x="84.455" y="56.515" size="1.4224" layer="95" rot="MR0">8</text>
<text x="88.265" y="56.515" size="1.4224" layer="95" rot="MR0">10</text>
<text x="109.855" y="56.515" size="1.4224" layer="95" rot="MR0">16</text>
<text x="113.665" y="56.515" size="1.4224" layer="95" rot="MR0">18</text>
<text x="125.095" y="56.515" size="1.4224" layer="95" rot="MR0">22</text>
<wire x1="-65.405" y1="28.575" x2="-68.58" y2="35.56" width="0.1524" layer="94"/>
<wire x1="-65.405" y1="28.575" x2="-65.405" y2="35.56" width="0.1524" layer="96"/>
<wire x1="-65.405" y1="28.575" x2="-61.595" y2="35.56" width="0.1524" layer="96"/>
<wire x1="-53.975" y1="28.575" x2="-53.975" y2="35.56" width="0.1524" layer="94"/>
<wire x1="-53.975" y1="28.575" x2="-50.165" y2="35.56" width="0.1524" layer="96"/>
<wire x1="-42.545" y1="28.575" x2="-42.545" y2="35.56" width="0.1524" layer="94"/>
<wire x1="-42.545" y1="28.575" x2="-46.355" y2="35.56" width="0.1524" layer="96"/>
<wire x1="-42.545" y1="28.575" x2="-38.735" y2="35.56" width="0.1524" layer="96"/>
<wire x1="-53.975" y1="28.575" x2="-57.785" y2="35.56" width="0.1524" layer="96"/>
<wire x1="-78.105" y1="28.575" x2="-81.28" y2="35.56" width="0.1524" layer="96"/>
<wire x1="-78.105" y1="28.575" x2="-78.105" y2="35.56" width="0.1524" layer="96"/>
<wire x1="-78.105" y1="28.575" x2="-74.295" y2="35.56" width="0.1524" layer="96"/>
<wire x1="-74.295" y1="32.385" x2="-72.39" y2="32.385" width="0.1524" layer="96"/>
<wire x1="-70.485" y1="32.385" x2="-68.58" y2="32.385" width="0.1524" layer="96"/>
<wire x1="-62.865" y1="32.385" x2="-60.96" y2="32.385" width="0.1524" layer="96"/>
<wire x1="-59.055" y1="32.385" x2="-57.15" y2="32.385" width="0.1524" layer="96"/>
<wire x1="-50.8" y1="32.385" x2="-48.895" y2="32.385" width="0.1524" layer="96"/>
<wire x1="-46.99" y1="32.385" x2="-45.085" y2="32.385" width="0.1524" layer="96"/>
<circle x="-68.58" y="35.56" radius="1.27" width="0.1524" layer="94"/>
<circle x="-65.405" y="28.575" radius="1.27" width="0.1524" layer="94"/>
<circle x="-53.975" y="35.56" radius="1.27" width="0.1524" layer="94"/>
<circle x="-53.975" y="28.575" radius="1.27" width="0.1524" layer="94"/>
<circle x="-38.735" y="35.56" radius="1.27" width="0.1524" layer="94"/>
<circle x="-42.545" y="28.575" radius="1.27" width="0.1524" layer="94"/>
<circle x="-78.105" y="28.575" radius="1.27" width="0.1524" layer="96"/>
<text x="-85.09" y="36.195" size="1.4224" layer="96">REG.</text>
<text x="-75.565" y="36.195" size="1.4224" layer="96">100%</text>
<text x="-79.375" y="37.465" size="1.4224" layer="96">50%</text>
<text x="-66.675" y="25.4" size="1.4224" layer="95">1</text>
<text x="-69.85" y="37.465" size="1.4224" layer="95">2</text>
<text x="-55.245" y="25.4" size="1.4224" layer="95">3</text>
<text x="-55.245" y="37.465" size="1.4224" layer="95">4</text>
<text x="-43.815" y="25.4" size="1.4224" layer="95">7</text>
<text x="-38.1" y="37.465" size="1.4224" layer="95">8</text>
<text x="-73.66" y="27.305" size="1.778" layer="95">S3</text>
<text x="-76.835" y="23.495" size="1.778" layer="96">A8 2351</text>
<wire x1="45.72" y1="-85.725" x2="43.18" y2="-76.2" width="0.4064" layer="94"/>
<wire x1="67.31" y1="-85.09" x2="64.77" y2="-75.565" width="0.4064" layer="94"/>
<wire x1="90.17" y1="-84.455" x2="87.63" y2="-74.93" width="0.4064" layer="94"/>
<wire x1="45.72" y1="-81.28" x2="47.625" y2="-81.28" width="0.254" layer="94"/>
<wire x1="49.53" y1="-81.28" x2="51.435" y2="-81.28" width="0.254" layer="94"/>
<wire x1="53.975" y1="-81.28" x2="55.88" y2="-81.28" width="0.254" layer="94"/>
<wire x1="58.42" y1="-81.28" x2="60.325" y2="-81.28" width="0.254" layer="94"/>
<wire x1="62.23" y1="-81.28" x2="64.135" y2="-81.28" width="0.254" layer="94"/>
<wire x1="67.945" y1="-81.28" x2="69.85" y2="-81.28" width="0.254" layer="94"/>
<wire x1="72.39" y1="-81.28" x2="74.295" y2="-81.28" width="0.254" layer="94"/>
<wire x1="76.835" y1="-81.28" x2="78.74" y2="-81.28" width="0.254" layer="94"/>
<wire x1="80.645" y1="-81.28" x2="82.55" y2="-81.28" width="0.254" layer="94"/>
<wire x1="84.455" y1="-81.28" x2="86.36" y2="-81.28" width="0.254" layer="94"/>
<wire x1="87.63" y1="-81.28" x2="88.9" y2="-81.28" width="0.254" layer="94"/>
<wire x1="24.765" y1="-81.28" x2="26.67" y2="-81.28" width="0.254" layer="95"/>
<wire x1="28.575" y1="-81.28" x2="30.48" y2="-81.28" width="0.254" layer="95"/>
<wire x1="33.02" y1="-81.28" x2="34.925" y2="-81.28" width="0.254" layer="95"/>
<wire x1="24.765" y1="-86.36" x2="22.225" y2="-76.2" width="0.254" layer="95"/>
<wire x1="38.1" y1="-81.28" x2="40.005" y2="-81.28" width="0.254" layer="95"/>
<wire x1="41.91" y1="-81.28" x2="43.815" y2="-81.28" width="0.254" layer="95"/>
<wire x1="113.665" y1="-84.455" x2="111.125" y2="-74.93" width="0.4064" layer="94"/>
<wire x1="100.33" y1="-81.28" x2="102.235" y2="-81.28" width="0.254" layer="94"/>
<wire x1="104.14" y1="-81.28" x2="106.045" y2="-81.28" width="0.254" layer="94"/>
<wire x1="107.95" y1="-81.28" x2="109.855" y2="-81.28" width="0.254" layer="94"/>
<wire x1="111.125" y1="-81.28" x2="112.395" y2="-81.28" width="0.254" layer="94"/>
<wire x1="91.44" y1="-81.28" x2="93.345" y2="-81.28" width="0.254" layer="94"/>
<wire x1="95.885" y1="-81.28" x2="97.79" y2="-81.28" width="0.254" layer="94"/>
<circle x="22.225" y="-76.2" radius="0.898" width="0.254" layer="95"/>
<circle x="27.305" y="-76.2" radius="0.898" width="0.254" layer="95"/>
<circle x="32.385" y="-78.74" radius="0.898" width="0.254" layer="95"/>
<circle x="34.925" y="-83.82" radius="0.898" width="0.254" layer="95"/>
<circle x="24.765" y="-86.36" radius="0.898" width="0.254" layer="95"/>
<text x="28.575" y="-76.835" size="1.4224" layer="95">VENT.</text>
<text x="36.83" y="-84.455" size="1.4224" layer="95">CHL.</text>
<text x="33.655" y="-79.375" size="1.4224" layer="95">TOP.</text>
<text x="20.32" y="-74.93" size="1.4224" layer="95">JED.</text>
<text x="24.13" y="-89.535" size="1.778" layer="95">S2</text>
<text x="24.13" y="-93.345" size="1.778" layer="95">VS-2455 A8</text>
<wire x1="137.16" y1="-84.455" x2="134.62" y2="-74.93" width="0.4064" layer="94"/>
<wire x1="123.825" y1="-81.28" x2="125.73" y2="-81.28" width="0.254" layer="94"/>
<wire x1="127.635" y1="-81.28" x2="129.54" y2="-81.28" width="0.254" layer="94"/>
<wire x1="131.445" y1="-81.28" x2="133.35" y2="-81.28" width="0.254" layer="94"/>
<wire x1="134.62" y1="-81.28" x2="135.89" y2="-81.28" width="0.254" layer="94"/>
<wire x1="114.935" y1="-81.28" x2="116.84" y2="-81.28" width="0.254" layer="94"/>
<wire x1="119.38" y1="-81.28" x2="121.285" y2="-81.28" width="0.254" layer="94"/>
</plain>
<instances>
<instance part="V42" gate="G$1" x="54.9275" y="80.01" smashed="yes" rot="R270">
<attribute name="VALUE" x="54.1655" y="78.74" size="1.27" layer="96" rot="R270"/>
</instance>
<instance part="V45" gate="G$1" x="96.52" y="80.01" rot="R270"/>
<instance part="V67" gate="G$1" x="73.66" y="80.01" smashed="yes" rot="R270">
<attribute name="VALUE" x="72.898" y="78.105" size="1.27" layer="96" rot="R270"/>
</instance>
<instance part="V68" gate="G$1" x="110.49" y="80.01" smashed="yes" rot="R90">
<attribute name="VALUE" x="109.728" y="78.74" size="1.524" layer="96" rot="R270"/>
</instance>
<instance part="V69" gate="G$1" x="85.09" y="80.01" smashed="yes" rot="R270">
<attribute name="VALUE" x="84.328" y="78.74" size="1.27" layer="96" rot="R270"/>
</instance>
<instance part="V46" gate="G$1" x="66.675" y="80.01" smashed="yes" rot="R270">
<attribute name="VALUE" x="65.913" y="78.105" size="1.27" layer="96" rot="R270"/>
</instance>
<instance part="V71" gate="G$1" x="77.47" y="80.01" smashed="yes" rot="R270">
<attribute name="VALUE" x="76.708" y="78.74" size="1.27" layer="96" rot="R270"/>
</instance>
<instance part="V72" gate="G$1" x="88.9" y="80.01" rot="R270"/>
<instance part="V49" gate="G$1" x="103.505" y="80.01" smashed="yes" rot="R90">
<attribute name="VALUE" x="102.743" y="78.74" size="1.524" layer="96" rot="R270"/>
</instance>
<instance part="V63" gate="G$1" x="49.2125" y="80.01" smashed="yes" rot="R270">
<attribute name="VALUE" x="48.4505" y="78.74" size="1.27" layer="96" rot="R270"/>
</instance>
<instance part="V77" gate="G$1" x="133.35" y="80.01" smashed="yes" rot="R90">
<attribute name="VALUE" x="132.588" y="78.74" size="1.524" layer="96" rot="R270"/>
</instance>
<instance part="V78" gate="G$1" x="125.73" y="80.01" smashed="yes" rot="R90">
<attribute name="VALUE" x="124.968" y="78.74" size="1.524" layer="96" rot="R270"/>
</instance>
<instance part="V79" gate="G$1" x="121.92" y="80.01" smashed="yes" rot="R270">
<attribute name="VALUE" x="121.158" y="78.105" size="1.27" layer="96" rot="R270"/>
</instance>
<instance part="V80" gate="G$1" x="114.3" y="80.01" smashed="yes" rot="R270">
<attribute name="VALUE" x="113.538" y="78.105" size="1.27" layer="96" rot="R270"/>
</instance>
<instance part="V81" gate="G$1" x="118.11" y="22.225" smashed="yes" rot="R270">
<attribute name="VALUE" x="118.872" y="23.495" size="1.524" layer="96" rot="R90"/>
</instance>
<instance part="2" gate="G$1" x="43.18" y="-76.2" smashed="yes" rot="MR270">
<attribute name="NAME" x="43.561" y="-72.39" size="2.032" layer="95" rot="MR180"/>
</instance>
<instance part="4" gate="G$1" x="48.26" y="-76.2" smashed="yes" rot="MR270">
<attribute name="NAME" x="48.641" y="-72.39" size="2.032" layer="95" rot="MR180"/>
</instance>
<instance part="6" gate="G$1" x="53.34" y="-78.74" smashed="yes" rot="MR270">
<attribute name="NAME" x="53.594" y="-74.803" size="2.032" layer="95" rot="MR180"/>
</instance>
<instance part="1" gate="G$1" x="45.72" y="-86.868" smashed="yes" rot="MR270">
<attribute name="NAME" x="41.91" y="-85.598" size="2.032" layer="95" rot="MR180"/>
</instance>
<instance part="8" gate="G$1" x="55.88" y="-83.82" smashed="yes">
<attribute name="NAME" x="59.182" y="-83.058" size="2.032" layer="95" rot="R180"/>
</instance>
<instance part="10" gate="G$1" x="64.77" y="-75.565" smashed="yes" rot="MR270">
<attribute name="NAME" x="60.833" y="-72.009" size="2.032" layer="95" rot="MR180"/>
</instance>
<instance part="12" gate="G$1" x="69.85" y="-75.565" smashed="yes" rot="MR270">
<attribute name="NAME" x="66.421" y="-71.882" size="2.032" layer="95" rot="MR180"/>
</instance>
<instance part="14" gate="G$1" x="74.93" y="-78.105" smashed="yes" rot="MR270">
<attribute name="NAME" x="73.66" y="-74.041" size="2.032" layer="95" rot="MR180"/>
</instance>
<instance part="9" gate="G$1" x="67.31" y="-85.725" smashed="yes" rot="MR90">
<attribute name="NAME" x="65.405" y="-86.36" size="2.032" layer="95" rot="MR0"/>
</instance>
<instance part="16" gate="G$1" x="77.47" y="-83.185" smashed="yes">
<attribute name="NAME" x="78.74" y="-85.09" size="2.032" layer="95" rot="R180"/>
</instance>
<instance part="18" gate="G$1" x="87.63" y="-74.93" smashed="yes" rot="MR270">
<attribute name="NAME" x="83.693" y="-71.628" size="2.032" layer="95" rot="MR180"/>
</instance>
<instance part="20" gate="G$1" x="92.71" y="-74.93" smashed="yes" rot="MR270">
<attribute name="NAME" x="91.948" y="-69.342" size="2.032" layer="95" rot="MR180"/>
</instance>
<instance part="22" gate="G$1" x="97.79" y="-77.47" smashed="yes" rot="MR270">
<attribute name="NAME" x="95.758" y="-73.279" size="2.032" layer="95" rot="MR180"/>
</instance>
<instance part="17" gate="G$1" x="90.17" y="-85.09" smashed="yes" rot="MR90">
<attribute name="NAME" x="88.9" y="-86.36" size="2.032" layer="95" rot="MR0"/>
</instance>
<instance part="24" gate="G$1" x="100.33" y="-82.55" smashed="yes">
<attribute name="NAME" x="101.981" y="-84.455" size="2.032" layer="95" rot="R180"/>
</instance>
<instance part="U$101" gate="GND" x="45.72" y="-99.695"/>
<instance part="D28" gate="1" x="55.88" y="-43.815" smashed="yes" rot="R270">
<attribute name="NAME" x="56.515" y="-41.4274" size="1.524" layer="95"/>
<attribute name="VALUE" x="56.515" y="-47.7774" size="1.524" layer="96"/>
</instance>
<instance part="D29" gate="1" x="53.34" y="-58.42" smashed="yes" rot="R270">
<attribute name="NAME" x="56.515" y="-55.3974" size="1.524" layer="95"/>
<attribute name="VALUE" x="56.515" y="-61.7474" size="1.524" layer="96"/>
</instance>
<instance part="P2" gate="1" x="74.93" y="-104.14" rot="R90"/>
<instance part="R1" gate="1" x="92.71" y="-104.14"/>
<instance part="U$102" gate="GND" x="106.68" y="-104.14" rot="R90"/>
<instance part="U$103" gate="G$1" x="63.5" y="-104.14" rot="R90"/>
<instance part="26" gate="G$1" x="111.125" y="-74.93" smashed="yes" rot="MR270">
<attribute name="NAME" x="107.188" y="-71.628" size="2.032" layer="95" rot="MR180"/>
</instance>
<instance part="28" gate="G$1" x="116.205" y="-74.93" smashed="yes" rot="MR270">
<attribute name="NAME" x="114.808" y="-68.707" size="2.032" layer="95" rot="MR180"/>
</instance>
<instance part="30" gate="G$1" x="121.285" y="-77.47" smashed="yes" rot="MR270">
<attribute name="NAME" x="119.888" y="-73.279" size="2.032" layer="95" rot="MR180"/>
</instance>
<instance part="25" gate="G$1" x="113.665" y="-85.09" smashed="yes" rot="MR90">
<attribute name="NAME" x="111.76" y="-86.995" size="2.032" layer="95" rot="MR0"/>
</instance>
<instance part="32" gate="G$1" x="123.825" y="-82.55" smashed="yes">
<attribute name="NAME" x="125.476" y="-84.455" size="2.032" layer="95" rot="R180"/>
</instance>
<instance part="U$105" gate="GND" x="121.285" y="-94.615"/>
<instance part="U$106" gate="GND" x="77.47" y="-58.42" rot="R180"/>
<instance part="U$107" gate="G$1" x="100.33" y="-52.07"/>
<instance part="U$109" gate="GND" x="165.1" y="-95.25"/>
<instance part="R2" gate="G$1" x="163.83" y="-84.455" smashed="yes" rot="R90">
<attribute name="NAME" x="171.45" y="-80.645" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="171.45" y="-83.185" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="R3" gate="G$1" x="163.83" y="-66.675" smashed="yes" rot="R90">
<attribute name="NAME" x="172.085" y="-62.23" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="172.085" y="-65.405" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="34" gate="G$1" x="134.62" y="-74.93" smashed="yes" rot="MR270">
<attribute name="NAME" x="131.318" y="-71.628" size="2.032" layer="95" rot="MR180"/>
</instance>
<instance part="36" gate="G$1" x="139.7" y="-74.93" smashed="yes" rot="MR270">
<attribute name="NAME" x="140.843" y="-69.977" size="2.032" layer="95" rot="MR180"/>
</instance>
<instance part="38" gate="G$1" x="144.78" y="-77.47" smashed="yes" rot="MR270">
<attribute name="NAME" x="147.828" y="-74.549" size="2.032" layer="95" rot="MR180"/>
</instance>
<instance part="33" gate="G$1" x="137.16" y="-85.09" smashed="yes" rot="MR90">
<attribute name="NAME" x="135.255" y="-86.36" size="2.032" layer="95" rot="MR0"/>
</instance>
<instance part="40" gate="G$1" x="147.32" y="-82.55" smashed="yes">
<attribute name="NAME" x="148.971" y="-84.455" size="2.032" layer="95" rot="R180"/>
</instance>
<instance part="S6" gate="A" x="-53.34" y="-2.54" rot="R90"/>
<instance part="S6" gate="G$2" x="-68.58" y="-2.54"/>
<instance part="S7" gate="A" x="33.02" y="-142.24" rot="R90"/>
<instance part="S7" gate="G$1" x="15.24" y="-142.24"/>
<instance part="S7" gate="B" x="48.26" y="-142.24" rot="R90"/>
<instance part="S7" gate="C" x="63.5" y="-142.24" rot="R90"/>
<instance part="S7" gate="D" x="78.74" y="-142.24" rot="R90"/>
<instance part="S7" gate="E" x="93.98" y="-142.24" rot="R90"/>
<instance part="S8" gate="G$1" x="53.34" y="0"/>
<instance part="S8" gate="A" x="68.58" y="0" rot="R90"/>
<instance part="S8" gate="B" x="81.28" y="0" rot="R90"/>
<instance part="S8" gate="C" x="93.98" y="0" rot="R90"/>
<instance part="S8" gate="D" x="106.68" y="0" rot="R90"/>
<instance part="S8" gate="E" x="119.38" y="0" rot="R90"/>
<instance part="S8" gate="F" x="132.08" y="0" rot="R90"/>
</instances>
<busses>
</busses>
<nets>
<net name="FLAME" class="0">
<segment>
<wire x1="25.4" y1="95.885" x2="22.86" y2="98.425" width="0.1524" layer="91"/>
<wire x1="25.4" y1="95.885" x2="25.4" y2="23.495" width="0.1524" layer="91"/>
<wire x1="25.4" y1="23.495" x2="106.68" y2="23.495" width="0.1524" layer="91"/>
<wire x1="106.68" y1="23.495" x2="106.68" y2="47.625" width="0.1524" layer="91"/>
<label x="22.86" y="95.885" size="1.778" layer="95" rot="R270"/>
<label x="97.79" y="24.13" size="1.778" layer="95"/>
</segment>
</net>
<net name="FUELHEAT" class="0">
<segment>
<wire x1="19.05" y1="98.425" x2="21.59" y2="95.885" width="0.1524" layer="91"/>
<wire x1="21.59" y1="95.885" x2="21.59" y2="17.78" width="0.1524" layer="91"/>
<wire x1="21.59" y1="17.78" x2="129.54" y2="17.78" width="0.1524" layer="91"/>
<wire x1="129.54" y1="17.78" x2="129.54" y2="47.625" width="0.1524" layer="91"/>
<label x="116.205" y="18.415" size="1.778" layer="95"/>
<label x="19.05" y="95.885" size="1.778" layer="95" rot="R270"/>
</segment>
</net>
<net name="HEATSTART" class="0">
<segment>
<wire x1="40.64" y1="95.885" x2="38.1" y2="98.425" width="0.1524" layer="91"/>
<wire x1="40.64" y1="38.735" x2="40.64" y2="60.96" width="0.1524" layer="91"/>
<wire x1="40.64" y1="60.96" x2="40.64" y2="95.885" width="0.1524" layer="91"/>
<wire x1="40.64" y1="38.735" x2="69.85" y2="38.735" width="0.1524" layer="91"/>
<wire x1="69.85" y1="38.735" x2="69.85" y2="47.625" width="0.1524" layer="91"/>
<label x="38.1" y="95.25" size="1.778" layer="95" rot="R270"/>
<label x="55.88" y="39.37" size="1.778" layer="95"/>
<wire x1="40.64" y1="60.96" x2="49.2125" y2="60.96" width="0.1524" layer="91"/>
<wire x1="49.2125" y1="60.96" x2="49.2125" y2="64.77" width="0.1524" layer="91"/>
<junction x="40.64" y="60.96"/>
<wire x1="49.2125" y1="60.96" x2="54.9275" y2="60.96" width="0.1524" layer="91"/>
<wire x1="54.9275" y1="60.96" x2="54.9275" y2="64.77" width="0.1524" layer="91"/>
<junction x="49.2125" y="60.96"/>
</segment>
</net>
<net name="PUMPSTART" class="0">
<segment>
<wire x1="35.56" y1="95.885" x2="33.02" y2="98.425" width="0.1524" layer="91"/>
<wire x1="35.56" y1="95.885" x2="35.56" y2="33.655" width="0.1524" layer="91"/>
<wire x1="35.56" y1="33.655" x2="81.28" y2="33.655" width="0.1524" layer="91"/>
<wire x1="81.28" y1="33.655" x2="81.28" y2="47.625" width="0.1524" layer="91"/>
<label x="33.02" y="95.25" size="1.778" layer="95" rot="R270"/>
<label x="66.675" y="34.29" size="1.778" layer="95"/>
</segment>
</net>
<net name="FAIL5" class="0">
<segment>
<wire x1="30.48" y1="95.885" x2="27.94" y2="98.425" width="0.1524" layer="91"/>
<wire x1="30.48" y1="95.885" x2="30.48" y2="28.575" width="0.1524" layer="91"/>
<wire x1="30.48" y1="28.575" x2="92.71" y2="28.575" width="0.1524" layer="91"/>
<wire x1="92.71" y1="28.575" x2="92.71" y2="47.625" width="0.1524" layer="91"/>
<label x="27.94" y="95.25" size="1.778" layer="95" rot="R270"/>
<label x="85.09" y="29.21" size="1.778" layer="95"/>
</segment>
</net>
<net name="N$11" class="0">
<segment>
<wire x1="66.675" y1="54.61" x2="66.675" y2="64.77" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<wire x1="73.66" y1="54.61" x2="73.66" y2="64.77" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<wire x1="77.47" y1="54.61" x2="77.47" y2="64.77" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<wire x1="85.09" y1="54.61" x2="85.09" y2="64.77" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<wire x1="88.9" y1="54.61" x2="88.9" y2="64.77" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<wire x1="96.52" y1="54.61" x2="96.52" y2="64.77" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<wire x1="103.505" y1="54.61" x2="103.505" y2="64.77" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$41" class="0">
<segment>
<wire x1="110.49" y1="54.61" x2="110.49" y2="64.77" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$42" class="0">
<segment>
<wire x1="125.73" y1="54.61" x2="125.73" y2="64.77" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$43" class="0">
<segment>
<wire x1="133.35" y1="54.61" x2="133.35" y2="64.77" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$44" class="0">
<segment>
<wire x1="114.3" y1="54.61" x2="114.3" y2="64.77" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$45" class="0">
<segment>
<wire x1="121.92" y1="54.61" x2="121.92" y2="64.77" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$81" class="0">
<segment>
<wire x1="118.11" y1="47.625" x2="118.11" y2="37.465" width="0.1524" layer="91"/>
</segment>
</net>
<net name="FAN_50%" class="0">
<segment>
<wire x1="-57.15" y1="85.725" x2="-53.975" y2="83.185" width="0.1524" layer="91"/>
<wire x1="-53.975" y1="83.185" x2="-53.975" y2="35.56" width="0.1524" layer="91"/>
<label x="-56.515" y="82.55" size="1.9304" layer="95" rot="R270"/>
</segment>
</net>
<net name="FAN_REG" class="0">
<segment>
<wire x1="-71.12" y1="85.725" x2="-68.58" y2="83.185" width="0.1524" layer="91"/>
<wire x1="-68.58" y1="83.185" x2="-68.58" y2="35.56" width="0.1524" layer="91"/>
<label x="-71.12" y="82.55" size="1.9304" layer="95" rot="R270"/>
</segment>
</net>
<net name="S3-1" class="0">
<segment>
<wire x1="-34.29" y1="83.185" x2="-36.83" y2="85.725" width="0.1524" layer="91"/>
<wire x1="-65.405" y1="28.575" x2="-65.405" y2="23.495" width="0.1524" layer="91"/>
<wire x1="-65.405" y1="23.495" x2="-53.975" y2="23.495" width="0.1524" layer="91"/>
<wire x1="-53.975" y1="28.575" x2="-53.975" y2="23.495" width="0.1524" layer="91"/>
<wire x1="-53.975" y1="23.495" x2="-42.545" y2="23.495" width="0.1524" layer="91"/>
<wire x1="-42.545" y1="23.495" x2="-42.545" y2="28.575" width="0.1524" layer="91"/>
<wire x1="-34.29" y1="83.185" x2="-34.29" y2="23.495" width="0.1524" layer="91"/>
<wire x1="-34.29" y1="23.495" x2="-42.545" y2="23.495" width="0.1524" layer="91"/>
<junction x="-53.975" y="23.495"/>
<junction x="-42.545" y="23.495"/>
<label x="-36.83" y="82.55" size="1.9304" layer="95" rot="R270"/>
</segment>
</net>
<net name="EM_PWR" class="0">
<segment>
<wire x1="-41.275" y1="85.725" x2="-38.735" y2="83.185" width="0.1524" layer="91"/>
<wire x1="-38.735" y1="83.185" x2="-38.735" y2="35.56" width="0.1524" layer="91"/>
<label x="-41.275" y="82.55" size="1.9304" layer="95" rot="R270"/>
</segment>
<segment>
<wire x1="137.16" y1="-28.575" x2="134.62" y2="-31.115" width="0.1524" layer="91"/>
<wire x1="134.62" y1="-31.115" x2="134.62" y2="-74.93" width="0.1524" layer="91"/>
<pinref part="34" gate="G$1" pin="P$1"/>
<label x="132.08" y="-31.115" size="1.778" layer="95" rot="R270"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<wire x1="45.72" y1="-86.868" x2="45.72" y2="-94.615" width="0.1524" layer="91"/>
<pinref part="1" gate="G$1" pin="P$1"/>
<pinref part="U$101" gate="GND" pin="GND"/>
</segment>
<segment>
<wire x1="101.6" y1="-104.14" x2="97.79" y2="-104.14" width="0.1524" layer="91"/>
<pinref part="R1" gate="1" pin="2"/>
<pinref part="U$102" gate="GND" pin="GND"/>
</segment>
<segment>
<wire x1="69.85" y1="-75.565" x2="69.85" y2="-66.675" width="0.1524" layer="91"/>
<wire x1="69.85" y1="-66.675" x2="77.47" y2="-66.675" width="0.1524" layer="91"/>
<wire x1="77.47" y1="-66.675" x2="77.47" y2="-63.5" width="0.1524" layer="91"/>
<wire x1="77.47" y1="-83.185" x2="77.47" y2="-66.675" width="0.1524" layer="91"/>
<junction x="77.47" y="-66.675"/>
<pinref part="12" gate="G$1" pin="P$1"/>
<pinref part="U$106" gate="GND" pin="GND"/>
<pinref part="16" gate="G$1" pin="P$1"/>
</segment>
<segment>
<wire x1="121.285" y1="-89.535" x2="121.285" y2="-77.47" width="0.1524" layer="91"/>
<pinref part="U$105" gate="GND" pin="GND"/>
<pinref part="30" gate="G$1" pin="P$1"/>
</segment>
<segment>
<wire x1="165.1" y1="-90.17" x2="165.1" y2="-89.535" width="0.1524" layer="91"/>
<pinref part="U$109" gate="GND" pin="GND"/>
<pinref part="R2" gate="G$1" pin="1"/>
</segment>
</net>
<net name="CON1" class="0">
<segment>
<wire x1="87.63" y1="-74.93" x2="87.63" y2="-31.115" width="0.1524" layer="91"/>
<wire x1="87.63" y1="-31.115" x2="90.17" y2="-28.575" width="0.1524" layer="91"/>
<label x="85.09" y="-31.75" size="1.9304" layer="95" rot="R270"/>
<pinref part="18" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="CON2" class="0">
<segment>
<wire x1="111.125" y1="-74.93" x2="111.125" y2="-31.115" width="0.1524" layer="91"/>
<wire x1="111.125" y1="-31.115" x2="113.665" y2="-28.575" width="0.1524" layer="91"/>
<label x="108.585" y="-31.75" size="1.9304" layer="95" rot="R270"/>
<pinref part="26" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="CON3" class="0">
<segment>
<wire x1="64.77" y1="-74.93" x2="64.77" y2="-31.115" width="0.1524" layer="91"/>
<wire x1="64.77" y1="-31.115" x2="67.31" y2="-28.575" width="0.1524" layer="91"/>
<label x="62.23" y="-31.75" size="1.9304" layer="95" rot="R270"/>
</segment>
</net>
<net name="EMERGENCY" class="0">
<segment>
<wire x1="28.575" y1="-28.575" x2="26.67" y2="-30.48" width="0.1524" layer="91"/>
<wire x1="26.67" y1="-30.48" x2="26.67" y2="-66.675" width="0.1524" layer="91"/>
<wire x1="43.18" y1="-76.2" x2="43.18" y2="-66.675" width="0.1524" layer="91"/>
<wire x1="43.18" y1="-66.675" x2="26.67" y2="-66.675" width="0.1524" layer="91"/>
<label x="24.13" y="-30.48" size="1.778" layer="95" rot="R270"/>
<pinref part="2" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="HEAT" class="0">
<segment>
<wire x1="31.75" y1="-30.48" x2="33.655" y2="-28.575" width="0.1524" layer="91"/>
<wire x1="31.75" y1="-62.865" x2="31.75" y2="-30.48" width="0.1524" layer="91"/>
<label x="29.21" y="-31.115" size="1.9304" layer="95" rot="R270"/>
<wire x1="31.75" y1="-62.865" x2="53.34" y2="-62.865" width="0.1524" layer="91"/>
<pinref part="D29" gate="1" pin="K"/>
<wire x1="53.34" y1="-78.105" x2="53.34" y2="-62.865" width="0.1524" layer="91"/>
<junction x="53.34" y="-62.865"/>
<wire x1="53.34" y1="-62.865" x2="53.34" y2="-60.96" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+10V" class="0">
<segment>
<wire x1="66.04" y1="-104.14" x2="67.31" y2="-104.14" width="0.1524" layer="91"/>
<pinref part="U$103" gate="G$1" pin="+10V"/>
<pinref part="P2" gate="1" pin="3"/>
</segment>
<segment>
<wire x1="123.825" y1="-71.755" x2="123.825" y2="-82.55" width="0.1524" layer="91"/>
<wire x1="116.205" y1="-71.755" x2="123.825" y2="-71.755" width="0.1524" layer="91"/>
<wire x1="100.33" y1="-71.755" x2="100.33" y2="-82.55" width="0.1524" layer="91"/>
<wire x1="92.71" y1="-71.755" x2="100.33" y2="-71.755" width="0.1524" layer="91"/>
<wire x1="100.33" y1="-71.755" x2="100.33" y2="-59.055" width="0.1524" layer="91"/>
<wire x1="100.33" y1="-54.61" x2="100.33" y2="-59.055" width="0.1524" layer="91"/>
<wire x1="100.33" y1="-59.055" x2="123.825" y2="-59.055" width="0.1524" layer="91"/>
<wire x1="123.825" y1="-59.055" x2="123.825" y2="-71.755" width="0.1524" layer="91"/>
<wire x1="165.1" y1="-61.595" x2="165.1" y2="-59.055" width="0.1524" layer="91"/>
<wire x1="165.1" y1="-59.055" x2="123.825" y2="-59.055" width="0.1524" layer="91"/>
<wire x1="123.825" y1="-59.055" x2="115.57" y2="-59.055" width="0.1524" layer="91"/>
<junction x="123.825" y="-71.755"/>
<junction x="100.33" y="-71.755"/>
<junction x="100.33" y="-59.055"/>
<junction x="123.825" y="-59.055"/>
<pinref part="32" gate="G$1" pin="P$1"/>
<pinref part="24" gate="G$1" pin="P$1"/>
<pinref part="U$107" gate="G$1" pin="+10V"/>
<pinref part="R3" gate="G$1" pin="2"/>
</segment>
</net>
<net name="CON4" class="0">
<segment>
<wire x1="90.17" y1="-85.09" x2="90.17" y2="-100.33" width="0.1524" layer="91"/>
<wire x1="90.17" y1="-100.33" x2="178.435" y2="-100.33" width="0.1524" layer="91"/>
<wire x1="178.435" y1="-100.33" x2="178.435" y2="-31.115" width="0.1524" layer="91"/>
<wire x1="178.435" y1="-31.115" x2="180.975" y2="-28.575" width="0.1524" layer="91"/>
<label x="175.895" y="-31.75" size="1.9304" layer="95" rot="R270"/>
<pinref part="17" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="CON6" class="0">
<segment>
<wire x1="67.31" y1="-85.725" x2="67.31" y2="-94.615" width="0.1524" layer="91"/>
<wire x1="67.31" y1="-94.615" x2="55.88" y2="-94.615" width="0.1524" layer="91"/>
<wire x1="55.88" y1="-94.615" x2="55.88" y2="-113.03" width="0.1524" layer="91"/>
<wire x1="182.88" y1="-113.03" x2="55.88" y2="-113.03" width="0.1524" layer="91"/>
<wire x1="182.88" y1="-113.03" x2="182.88" y2="-31.115" width="0.1524" layer="91"/>
<wire x1="182.88" y1="-31.115" x2="185.42" y2="-28.575" width="0.1524" layer="91"/>
<label x="180.34" y="-31.75" size="1.9304" layer="95" rot="R270"/>
<pinref part="9" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="COOL" class="0">
<segment>
<wire x1="38.735" y1="-28.575" x2="36.195" y2="-31.115" width="0.1524" layer="91"/>
<wire x1="36.195" y1="-31.115" x2="36.195" y2="-50.165" width="0.1524" layer="91"/>
<label x="33.655" y="-31.115" size="1.9304" layer="95" rot="R270"/>
<wire x1="36.195" y1="-50.165" x2="55.88" y2="-50.165" width="0.1524" layer="91"/>
<wire x1="55.88" y1="-83.82" x2="55.88" y2="-50.165" width="0.1524" layer="91"/>
<pinref part="D28" gate="1" pin="K"/>
<pinref part="8" gate="G$1" pin="P$1"/>
<junction x="55.88" y="-50.165"/>
<wire x1="55.88" y1="-50.165" x2="55.88" y2="-46.355" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CON5" class="0">
<segment>
<wire x1="113.665" y1="-85.09" x2="113.665" y2="-98.425" width="0.1524" layer="91"/>
<wire x1="113.665" y1="-98.425" x2="173.99" y2="-98.425" width="0.1524" layer="91"/>
<wire x1="173.99" y1="-98.425" x2="173.99" y2="-31.115" width="0.1524" layer="91"/>
<wire x1="173.99" y1="-31.115" x2="176.53" y2="-28.575" width="0.1524" layer="91"/>
<label x="171.45" y="-31.75" size="1.9304" layer="95" rot="R270"/>
<pinref part="25" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<wire x1="87.63" y1="-104.14" x2="82.55" y2="-104.14" width="0.1524" layer="91"/>
<pinref part="R1" gate="1" pin="1"/>
<pinref part="P2" gate="1" pin="1"/>
</segment>
</net>
<net name="N$69" class="0">
<segment>
<wire x1="92.71" y1="-74.93" x2="92.71" y2="-71.755" width="0.1524" layer="91"/>
<pinref part="20" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="N$71" class="0">
<segment>
<wire x1="116.205" y1="-74.93" x2="116.205" y2="-71.755" width="0.1524" layer="91"/>
<pinref part="28" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<wire x1="74.93" y1="-78.105" x2="74.93" y2="-99.06" width="0.1524" layer="91"/>
<pinref part="14" gate="G$1" pin="P$1"/>
<pinref part="P2" gate="1" pin="2"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<wire x1="97.79" y1="-96.52" x2="97.79" y2="-77.47" width="0.1524" layer="91"/>
<wire x1="97.79" y1="-96.52" x2="159.385" y2="-96.52" width="0.1524" layer="91"/>
<wire x1="165.1" y1="-79.375" x2="165.1" y2="-76.2" width="0.1524" layer="91"/>
<wire x1="165.1" y1="-71.755" x2="165.1" y2="-76.2" width="0.1524" layer="91"/>
<wire x1="165.1" y1="-76.2" x2="159.385" y2="-76.2" width="0.1524" layer="91"/>
<wire x1="159.385" y1="-96.52" x2="159.385" y2="-76.2" width="0.1524" layer="91"/>
<junction x="165.1" y="-76.2"/>
<pinref part="22" gate="G$1" pin="P$1"/>
<pinref part="R2" gate="G$1" pin="2"/>
<pinref part="R3" gate="G$1" pin="1"/>
</segment>
</net>
<net name="FAN_EM" class="0">
<segment>
<wire x1="50.8" y1="-28.575" x2="48.26" y2="-31.115" width="0.1524" layer="91"/>
<wire x1="55.88" y1="-41.275" x2="55.88" y2="-37.465" width="0.1524" layer="91"/>
<wire x1="55.88" y1="-37.465" x2="48.26" y2="-37.465" width="0.1524" layer="91"/>
<wire x1="48.26" y1="-53.34" x2="48.26" y2="-37.465" width="0.1524" layer="91"/>
<wire x1="53.34" y1="-55.88" x2="53.34" y2="-53.34" width="0.1524" layer="91"/>
<wire x1="53.34" y1="-53.34" x2="48.26" y2="-53.34" width="0.1524" layer="91"/>
<wire x1="48.26" y1="-76.2" x2="48.26" y2="-53.34" width="0.1524" layer="91"/>
<wire x1="48.26" y1="-31.115" x2="48.26" y2="-37.465" width="0.1524" layer="91"/>
<junction x="48.26" y="-53.34"/>
<junction x="48.26" y="-37.465"/>
<label x="45.72" y="-31.115" size="1.9304" layer="95" rot="R270"/>
<pinref part="D28" gate="1" pin="A"/>
<pinref part="D29" gate="1" pin="A"/>
<pinref part="4" gate="G$1" pin="P$1"/>
</segment>
<segment>
<wire x1="142.24" y1="-28.575" x2="139.7" y2="-31.115" width="0.1524" layer="91"/>
<pinref part="36" gate="G$1" pin="P$1"/>
<wire x1="139.7" y1="-31.115" x2="139.7" y2="-73.025" width="0.1524" layer="91"/>
<pinref part="38" gate="G$1" pin="P$1"/>
<wire x1="139.7" y1="-73.025" x2="139.7" y2="-74.93" width="0.1524" layer="91"/>
<wire x1="139.7" y1="-73.025" x2="144.78" y2="-73.025" width="0.1524" layer="91"/>
<wire x1="144.78" y1="-73.025" x2="144.78" y2="-77.47" width="0.1524" layer="91"/>
<junction x="139.7" y="-73.025"/>
<pinref part="40" gate="G$1" pin="P$1"/>
<wire x1="144.78" y1="-73.025" x2="147.32" y2="-73.025" width="0.1524" layer="91"/>
<wire x1="147.32" y1="-73.025" x2="147.32" y2="-82.55" width="0.1524" layer="91"/>
<junction x="144.78" y="-73.025"/>
<label x="137.16" y="-31.115" size="1.778" layer="95" rot="R270"/>
</segment>
</net>
<net name="INVLIN" class="0">
<segment>
<wire x1="154.94" y1="-28.575" x2="152.4" y2="-31.115" width="0.1524" layer="91"/>
<wire x1="152.4" y1="-31.115" x2="152.4" y2="-94.615" width="0.1524" layer="91"/>
<wire x1="152.4" y1="-94.615" x2="137.16" y2="-94.615" width="0.1524" layer="91"/>
<pinref part="33" gate="G$1" pin="P$1"/>
<wire x1="137.16" y1="-94.615" x2="137.16" y2="-85.09" width="0.1524" layer="91"/>
<label x="149.86" y="-31.75" size="1.778" layer="95" rot="R270"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
</compatibility>
</eagle>
