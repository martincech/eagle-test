<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.6.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="3" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="5" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="6" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="14" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="neosazovat" color="11" fill="1" visible="yes" active="yes"/>
<layer number="102" name="Mittellin" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="fp3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="Beschreib" color="9" fill="1" visible="yes" active="yes"/>
<layer number="106" name="BGA-Top" color="4" fill="1" visible="yes" active="yes"/>
<layer number="107" name="BD-Top" color="5" fill="1" visible="yes" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="yes" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="ReferenceLS" color="7" fill="1" visible="no" active="no"/>
<layer number="114" name="FRNTMAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="115" name="FRNTMAAT2" color="7" fill="1" visible="no" active="no"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="no" active="no"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="zmeny" color="14" fill="4" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="no" active="no"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="no" active="no"/>
<layer number="134" name="PUVODNI_VALUES" color="13" fill="1" visible="no" active="yes"/>
<layer number="135" name="PUVODNI_OTVORY" color="14" fill="1" visible="no" active="yes"/>
<layer number="136" name="PUVODNI_OBRYS_DPS" color="9" fill="1" visible="no" active="yes"/>
<layer number="137" name="NOVY_OBRYS_V_GRIDU" color="7" fill="1" visible="no" active="yes"/>
<layer number="138" name="NOVE_OTVORY_V_GRIDU" color="7" fill="1" visible="no" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="no" active="no"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="OrgLBR" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic>
<libraries>
<library name="FRAMES">
<packages>
</packages>
<symbols>
<symbol name="DOCFIELD">
<wire x1="0" y1="0" x2="71.12" y2="0" width="0.254" layer="94"/>
<wire x1="101.6" y1="15.24" x2="87.63" y2="15.24" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="71.12" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="15.24" width="0.254" layer="94"/>
<wire x1="101.6" y1="15.24" x2="101.6" y2="5.08" width="0.254" layer="94"/>
<wire x1="71.12" y1="5.08" x2="71.12" y2="0" width="0.254" layer="94"/>
<wire x1="71.12" y1="5.08" x2="87.63" y2="5.08" width="0.254" layer="94"/>
<wire x1="71.12" y1="0" x2="101.6" y2="0" width="0.254" layer="94"/>
<wire x1="87.63" y1="15.24" x2="87.63" y2="5.08" width="0.254" layer="94"/>
<wire x1="87.63" y1="15.24" x2="0" y2="15.24" width="0.254" layer="94"/>
<wire x1="87.63" y1="5.08" x2="101.6" y2="5.08" width="0.254" layer="94"/>
<wire x1="101.6" y1="5.08" x2="101.6" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="15.24" x2="0" y2="22.86" width="0.254" layer="94"/>
<wire x1="101.6" y1="35.56" x2="0" y2="35.56" width="0.254" layer="94"/>
<wire x1="101.6" y1="35.56" x2="101.6" y2="22.86" width="0.254" layer="94"/>
<wire x1="0" y1="22.86" x2="101.6" y2="22.86" width="0.254" layer="94"/>
<wire x1="0" y1="22.86" x2="0" y2="35.56" width="0.254" layer="94"/>
<wire x1="101.6" y1="22.86" x2="101.6" y2="15.24" width="0.254" layer="94"/>
<text x="1.27" y="1.27" size="2.54" layer="94">Date:</text>
<text x="12.7" y="1.27" size="2.54" layer="94">&gt;LAST_DATE_TIME</text>
<text x="72.39" y="1.27" size="2.54" layer="94">Sheet:</text>
<text x="86.36" y="1.27" size="2.54" layer="94">&gt;SHEET</text>
<text x="88.9" y="11.43" size="2.54" layer="94">REV:</text>
<text x="1.27" y="19.05" size="2.54" layer="94">TITLE:</text>
<text x="1.27" y="11.43" size="2.54" layer="94">Document Number:</text>
<text x="17.78" y="19.05" size="2.54" layer="94">&gt;DRAWING_NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="TAB" uservalue="yes">
<gates>
<gate name="G$1" symbol="DOCFIELD" x="-48.26" y="-12.7"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SUPPLY2">
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.27" y1="2.54" x2="1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="0" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<text x="-1.905" y="-0.635" size="1.524" layer="94">GND</text>
<pin name="GND" x="0" y="5.08" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="V--&gt;">
<wire x1="15.24" y1="0" x2="13.97" y2="1.27" width="0.1524" layer="94"/>
<wire x1="13.97" y1="1.27" x2="0" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="1.27" x2="0" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.27" x2="13.97" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="15.24" y1="0" x2="13.97" y2="-1.27" width="0.1524" layer="94"/>
<text x="1.27" y="-0.762" size="1.524" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="PE">
<wire x1="-1.905" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-0.762" x2="1.27" y2="-0.762" width="0.254" layer="94"/>
<wire x1="-0.635" y1="-1.524" x2="0.635" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-0.381" y="0.635" size="1.524" layer="94" rot="R90">PE</text>
<pin name="PE" x="0" y="5.08" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="AKU">
<wire x1="-0.635" y1="3.81" x2="0.635" y2="3.81" width="0.1524" layer="94"/>
<wire x1="0" y1="3.175" x2="0" y2="4.445" width="0.1524" layer="94"/>
<circle x="0" y="3.81" radius="1.27" width="0.254" layer="94"/>
<text x="-1.778" y="5.715" size="1.524" layer="94">AKU</text>
<pin name="AKU" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="+24V@2">
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="0.635" x2="0" y2="1.905" width="0.1524" layer="94"/>
<circle x="0" y="1.27" radius="1.27" width="0.254" layer="94"/>
<text x="-2.54" y="3.175" size="1.524" layer="94">+24V</text>
<pin name="+24V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND">
<gates>
<gate name="GND" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="V--&gt;" prefix="V" uservalue="yes">
<gates>
<gate name="G$1" symbol="V--&gt;" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PE">
<gates>
<gate name="PE" symbol="PE" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="AKU">
<gates>
<gate name="G$1" symbol="AKU" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+24V@2">
<gates>
<gate name="+24V" symbol="+24V@2" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SMDA">
<packages>
<package name="MELF">
<wire x1="-2.351" y1="1.135" x2="2.2684" y2="1.135" width="0.127" layer="21"/>
<wire x1="2.2684" y1="1.135" x2="2.2684" y2="-1.135" width="0.127" layer="21"/>
<wire x1="2.2684" y1="-1.135" x2="-2.351" y2="-1.135" width="0.127" layer="21"/>
<wire x1="-2.351" y1="-1.135" x2="-2.351" y2="1.135" width="0.127" layer="21"/>
<smd name="1" x="2.6" y="0" dx="1.8" dy="2.65" layer="1"/>
<smd name="2" x="-2.7" y="0" dx="1.8" dy="2.65" layer="1"/>
<text x="-3.5527" y="1.87" size="1.27" layer="27">&gt;VALUE</text>
<text x="-3.5527" y="3.775" size="1.27" layer="25">&gt;NAME</text>
<rectangle x1="-2.5" y1="-1.2" x2="-1.9" y2="1.2" layer="27"/>
<rectangle x1="1.8" y1="-1.2" x2="2.4" y2="1.2" layer="27"/>
<rectangle x1="-1.5" y1="-1.1" x2="-1.1" y2="1.1" layer="27"/>
</package>
</packages>
<symbols>
<symbol name="DIODE">
<wire x1="-2.54" y1="-1.905" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-2.54" y2="1.905" width="0.254" layer="94"/>
<wire x1="-2.54" y1="1.905" x2="-2.54" y2="-1.905" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="1.3716" y2="0" width="0.1524" layer="94"/>
<wire x1="1.397" y1="1.905" x2="1.397" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.3114" y="2.6416" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.5654" y="-4.4958" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
<pin name="K" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="DIOD-MELF" prefix="D" uservalue="yes">
<gates>
<gate name="G$1" symbol="DIODE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MELF">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="K" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SKLADKA2">
<packages>
<package name="NIC1">
<pad name="1" x="0" y="0" drill="0.8128" diameter="1.397" shape="square"/>
<pad name="2" x="2.54" y="0" drill="0.8128" diameter="1.397" shape="square"/>
<pad name="3" x="3.81" y="0" drill="0.8128" diameter="1.397" shape="square"/>
<pad name="4" x="6.35" y="0" drill="0.8128" diameter="1.397" shape="square"/>
<pad name="5" x="8.89" y="0" drill="0.8128" diameter="1.397" shape="square"/>
<pad name="6" x="11.43" y="0" drill="0.8128" diameter="1.397" shape="square"/>
<pad name="7" x="13.97" y="0" drill="0.8128" diameter="1.397" shape="square"/>
</package>
</packages>
<symbols>
<symbol name="SPINAC1">
<wire x1="0.762" y1="0.508" x2="2.54" y2="5.08" width="0.4064" layer="94"/>
<circle x="0" y="0" radius="0.762" width="0.254" layer="94"/>
<text x="5.08" y="3.048" size="2.032" layer="95">&gt;NAME</text>
<text x="5.08" y="0.508" size="2.032" layer="96">&gt;VALUE</text>
<pin name="1" x="0" y="-5.08" visible="off" length="middle" rot="R90"/>
<pin name="2" x="0" y="10.16" visible="off" length="middle" rot="R270"/>
</symbol>
<symbol name="JISTVYP">
<wire x1="0.762" y1="0.508" x2="3.048" y2="6.096" width="0.4064" layer="94"/>
<wire x1="3.048" y1="6.096" x2="4.064" y2="5.588" width="0.254" layer="94"/>
<wire x1="4.064" y1="5.588" x2="3.302" y2="3.556" width="0.254" layer="94"/>
<wire x1="3.302" y1="3.556" x2="2.286" y2="4.064" width="0.254" layer="94"/>
<wire x1="1.905" y1="2.54" x2="5.08" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="1.905" x2="5.08" y2="1.905" width="0.254" layer="94"/>
<wire x1="5.08" y1="3.175" x2="5.08" y2="1.27" width="0.254" layer="94"/>
<circle x="0" y="0" radius="0.762" width="0.254" layer="94"/>
<text x="2.54" y="-2.032" size="2.032" layer="95">&gt;NAME</text>
<text x="2.54" y="-4.572" size="2.032" layer="96">&gt;VALUE</text>
<pin name="1" x="0" y="-5.08" visible="off" length="middle" rot="R90"/>
<pin name="2" x="0" y="10.16" visible="off" length="middle" rot="R270"/>
</symbol>
<symbol name="ZAROVKA">
<wire x1="-1.778" y1="-1.778" x2="1.778" y2="1.778" width="0.254" layer="94"/>
<wire x1="-1.778" y1="1.778" x2="1.778" y2="-1.778" width="0.254" layer="94"/>
<circle x="0" y="0" radius="2.54" width="0.254" layer="94"/>
<text x="5.08" y="0.508" size="2.032" layer="95">&gt;NAME</text>
<text x="5.08" y="-2.032" size="2.032" layer="96">&gt;VALUE</text>
<pin name="1" x="0" y="-7.62" visible="off" length="middle" rot="R90"/>
<pin name="2" x="0" y="7.62" visible="off" length="middle" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SPINAC1" uservalue="yes">
<gates>
<gate name="G$1" symbol="SPINAC1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="NIC1">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="JIS_VYP" prefix="FA" uservalue="yes">
<gates>
<gate name="G$1" symbol="JISTVYP" x="0" y="-2.54"/>
</gates>
<devices>
<device name="" package="NIC1">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ZAROVKA" uservalue="yes">
<gates>
<gate name="G$1" symbol="ZAROVKA" x="0" y="0"/>
</gates>
<devices>
<device name="" package="NIC1">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="RELAIS">
<packages>
<package name="RELEAUTO">
<wire x1="-5.08" y1="4.445" x2="-10.795" y2="4.445" width="0.127" layer="22"/>
<wire x1="-10.795" y1="-4.445" x2="-5.08" y2="-4.445" width="0.127" layer="22"/>
<wire x1="-11.43" y1="3.81" x2="-10.795" y2="4.445" width="0.127" layer="22" curve="-53.130102"/>
<wire x1="-11.43" y1="-3.81" x2="-10.795" y2="-4.445" width="0.127" layer="22" curve="53.130102"/>
<wire x1="-5.08" y1="4.445" x2="-5.08" y2="12.7" width="0.127" layer="22"/>
<wire x1="-5.08" y1="-4.445" x2="-5.08" y2="-12.065" width="0.127" layer="22"/>
<wire x1="3.81" y1="-12.065" x2="3.81" y2="-3.81" width="0.127" layer="22"/>
<wire x1="3.81" y1="-3.81" x2="13.97" y2="-3.81" width="0.127" layer="22"/>
<wire x1="13.97" y1="-3.81" x2="14.605" y2="-3.175" width="0.127" layer="22" curve="53.130102"/>
<wire x1="-11.43" y1="3.81" x2="-11.43" y2="-3.81" width="0.127" layer="22"/>
<wire x1="13.97" y1="3.81" x2="14.605" y2="3.175" width="0.127" layer="22" curve="-53.130102"/>
<wire x1="14.605" y1="3.175" x2="14.605" y2="-3.175" width="0.127" layer="22"/>
<wire x1="3.81" y1="3.81" x2="13.97" y2="3.81" width="0.127" layer="22"/>
<wire x1="3.81" y1="3.81" x2="3.81" y2="12.7" width="0.127" layer="22"/>
<wire x1="-5.08" y1="-12.065" x2="-4.445" y2="-12.7" width="0.127" layer="22" curve="53.130102"/>
<wire x1="3.175" y1="-12.7" x2="3.81" y2="-12.065" width="0.127" layer="22" curve="90"/>
<wire x1="-5.08" y1="12.7" x2="-4.445" y2="13.335" width="0.127" layer="22" curve="-90"/>
<wire x1="3.175" y1="13.335" x2="3.81" y2="12.7" width="0.127" layer="22" curve="-53.130102"/>
<wire x1="3.175" y1="13.335" x2="-4.445" y2="13.335" width="0.127" layer="22"/>
<wire x1="-4.445" y1="-12.7" x2="3.175" y2="-12.7" width="0.127" layer="22"/>
<wire x1="-7.62" y1="3.81" x2="-7.62" y2="-3.81" width="0.127" layer="22"/>
<wire x1="-7.62" y1="-3.81" x2="-10.16" y2="-3.81" width="0.127" layer="22"/>
<wire x1="-10.16" y1="-3.81" x2="-10.16" y2="3.81" width="0.127" layer="22"/>
<wire x1="-10.16" y1="3.81" x2="-7.62" y2="3.81" width="0.127" layer="22"/>
<wire x1="0" y1="3.81" x2="0" y2="-3.81" width="0.127" layer="22"/>
<wire x1="0" y1="-3.81" x2="-2.54" y2="-3.81" width="0.127" layer="22"/>
<wire x1="-2.54" y1="-3.81" x2="-2.54" y2="3.81" width="0.127" layer="22"/>
<wire x1="-2.54" y1="3.81" x2="0" y2="3.81" width="0.127" layer="22"/>
<wire x1="3.175" y1="7.62" x2="-4.445" y2="7.62" width="0.127" layer="22"/>
<wire x1="-4.445" y1="7.62" x2="-4.445" y2="10.16" width="0.127" layer="22"/>
<wire x1="-4.445" y1="10.16" x2="3.175" y2="10.16" width="0.127" layer="22"/>
<wire x1="3.175" y1="10.16" x2="3.175" y2="7.62" width="0.127" layer="22"/>
<wire x1="3.175" y1="-10.16" x2="-4.445" y2="-10.16" width="0.127" layer="22"/>
<wire x1="-4.445" y1="-10.16" x2="-4.445" y2="-7.62" width="0.127" layer="22"/>
<wire x1="-4.445" y1="-7.62" x2="3.175" y2="-7.62" width="0.127" layer="22"/>
<wire x1="3.175" y1="-7.62" x2="3.175" y2="-10.16" width="0.127" layer="22"/>
<wire x1="13.335" y1="-1.27" x2="5.715" y2="-1.27" width="0.127" layer="51"/>
<wire x1="5.715" y1="-1.27" x2="5.715" y2="1.27" width="0.127" layer="51"/>
<wire x1="5.715" y1="1.27" x2="13.335" y2="1.27" width="0.127" layer="51"/>
<wire x1="13.335" y1="1.27" x2="13.335" y2="-1.27" width="0.127" layer="51"/>
<wire x1="3.81" y1="-12.065" x2="13.97" y2="-3.81" width="0.127" layer="22"/>
<wire x1="-10.795" y1="4.445" x2="-5.08" y2="12.7" width="0.127" layer="22"/>
<wire x1="-12.065" y1="12.7" x2="-10.795" y2="13.97" width="0.127" layer="22" curve="-90"/>
<wire x1="-12.065" y1="12.7" x2="-12.065" y2="-12.065" width="0.127" layer="22"/>
<wire x1="-12.065" y1="-12.065" x2="-10.795" y2="-13.335" width="0.127" layer="22" curve="67.380135"/>
<wire x1="-10.795" y1="-13.335" x2="13.97" y2="-13.335" width="0.127" layer="22"/>
<wire x1="13.97" y1="-13.335" x2="15.24" y2="-12.065" width="0.127" layer="22" curve="90"/>
<wire x1="15.24" y1="-12.065" x2="15.24" y2="12.7" width="0.127" layer="22"/>
<wire x1="13.97" y1="13.97" x2="-10.795" y2="13.97" width="0.127" layer="22"/>
<wire x1="13.97" y1="13.97" x2="15.24" y2="12.7" width="0.127" layer="22" curve="-90"/>
<pad name="87" x="-8.89" y="0" drill="2.54" diameter="5.08"/>
<pad name="30" x="9.017" y="0" drill="2.54" diameter="5.08"/>
<pad name="87A" x="-0.889" y="0" drill="2.54" diameter="5.08"/>
<pad name="86" x="-0.762" y="8.636" drill="2.54" diameter="5.08"/>
<pad name="85" x="-0.508" y="-8.509" drill="2.54" diameter="5.08"/>
<pad name="P$6" x="-6.985" y="6.477" drill="2.1844" diameter="2.54"/>
<pad name="P$7" x="6.985" y="-6.477" drill="2.1844" diameter="2.54"/>
<text x="-9.525" y="17.145" size="1.778" layer="26" ratio="10">&gt;NAME</text>
<text x="-9.652" y="14.605" size="1.778" layer="28" ratio="10">&gt;VALUE</text>
<rectangle x1="-12.7" y1="-9.525" x2="-11.43" y2="10.795" layer="22"/>
</package>
</packages>
<symbols>
<symbol name="K">
<wire x1="3.81" y1="-1.905" x2="3.81" y2="1.905" width="0.254" layer="94"/>
<wire x1="-3.81" y1="1.905" x2="-3.81" y2="-1.905" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.905" x2="3.81" y2="-1.905" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="1.905" x2="-3.81" y2="1.905" width="0.254" layer="94"/>
<wire x1="-3.81" y1="-1.905" x2="0" y2="-1.905" width="0.254" layer="94"/>
<wire x1="3.81" y1="1.905" x2="0" y2="1.905" width="0.254" layer="94"/>
<text x="1.27" y="2.921" size="1.524" layer="96">&gt;VALUE</text>
<text x="0.635" y="3.175" size="0.8636" layer="93">1</text>
<text x="0.635" y="-3.81" size="0.8636" layer="93">2</text>
<text x="-2.54" y="-0.635" size="1.27" layer="95">&gt;PART</text>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
</symbol>
<symbol name="U">
<wire x1="3.175" y1="5.08" x2="1.905" y2="5.08" width="0.254" layer="94"/>
<wire x1="-3.175" y1="5.08" x2="-1.905" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="2.54" y2="5.715" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="0" y2="0" width="0.254" layer="94"/>
<circle x="0" y="1.27" radius="0.127" width="0.4064" layer="94"/>
<text x="0.635" y="0.635" size="0.8636" layer="93">P</text>
<text x="-2.54" y="3.81" size="0.8636" layer="93">S</text>
<text x="2.54" y="3.81" size="0.8636" layer="93">O</text>
<pin name="O" x="5.08" y="5.08" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="S" x="-5.08" y="5.08" visible="off" length="short" direction="pas"/>
<pin name="P" x="0" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="AUTORELE" prefix="RE" uservalue="yes">
<gates>
<gate name="A" symbol="K" x="0" y="-2.54"/>
<gate name="B" symbol="U" x="17.78" y="-5.08"/>
</gates>
<devices>
<device name="" package="RELEAUTO">
<connects>
<connect gate="A" pin="1" pad="86"/>
<connect gate="A" pin="2" pad="85"/>
<connect gate="B" pin="O" pad="87A"/>
<connect gate="B" pin="P" pad="30"/>
<connect gate="B" pin="S" pad="87"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="untitled">
<packages>
</packages>
<symbols>
<symbol name="POKUS">
<wire x1="-5.08" y1="-7.62" x2="-5.08" y2="-3.81" width="0.3048" layer="94"/>
<wire x1="-5.08" y1="-3.81" x2="-7.62" y2="3.81" width="0.3048" layer="94"/>
<wire x1="-5.08" y1="3.175" x2="-5.08" y2="7.62" width="0.3048" layer="94"/>
<wire x1="-10.16" y1="2.54" x2="-13.97" y2="2.54" width="0.3048" layer="94"/>
<wire x1="-13.97" y1="2.54" x2="-17.78" y2="2.54" width="0.3048" layer="94"/>
<wire x1="-17.78" y1="2.54" x2="-17.78" y2="-2.54" width="0.3048" layer="94"/>
<wire x1="-17.78" y1="-2.54" x2="-13.97" y2="-2.54" width="0.3048" layer="94"/>
<wire x1="-13.97" y1="-2.54" x2="-10.16" y2="-2.54" width="0.3048" layer="94"/>
<wire x1="-10.16" y1="-2.54" x2="-10.16" y2="0" width="0.3048" layer="94"/>
<wire x1="-10.16" y1="0" x2="-10.16" y2="2.54" width="0.3048" layer="94"/>
<wire x1="2.54" y1="-7.62" x2="2.54" y2="-3.81" width="0.3048" layer="94"/>
<wire x1="2.54" y1="-3.81" x2="0" y2="3.81" width="0.3048" layer="94"/>
<wire x1="2.54" y1="3.175" x2="2.54" y2="7.62" width="0.3048" layer="94"/>
<wire x1="10.16" y1="-7.62" x2="10.16" y2="-3.81" width="0.3048" layer="94"/>
<wire x1="10.16" y1="-3.81" x2="7.62" y2="3.81" width="0.3048" layer="94"/>
<wire x1="10.16" y1="3.175" x2="10.16" y2="7.62" width="0.3048" layer="94"/>
<wire x1="-13.97" y1="2.54" x2="-13.97" y2="5.08" width="0.3048" layer="94"/>
<wire x1="-13.97" y1="-2.54" x2="-13.97" y2="-5.08" width="0.3048" layer="94"/>
<wire x1="17.78" y1="-7.62" x2="17.78" y2="-3.81" width="0.3048" layer="94"/>
<wire x1="17.78" y1="-3.81" x2="20.32" y2="3.81" width="0.3048" layer="94"/>
<wire x1="17.78" y1="3.175" x2="17.78" y2="7.62" width="0.3048" layer="94"/>
<wire x1="17.78" y1="3.175" x2="20.955" y2="3.175" width="0.3048" layer="94"/>
<wire x1="-10.16" y1="0" x2="25.4" y2="0" width="0.3048" layer="94" style="shortdash"/>
<wire x1="45.72" y1="-7.62" x2="45.72" y2="-3.81" width="0.3048" layer="94"/>
<wire x1="45.72" y1="-3.81" x2="43.18" y2="3.81" width="0.3048" layer="94"/>
<wire x1="45.72" y1="3.175" x2="45.72" y2="7.62" width="0.3048" layer="94"/>
<wire x1="40.64" y1="2.54" x2="36.83" y2="2.54" width="0.3048" layer="94"/>
<wire x1="36.83" y1="2.54" x2="33.02" y2="2.54" width="0.3048" layer="94"/>
<wire x1="33.02" y1="2.54" x2="33.02" y2="-2.54" width="0.3048" layer="94"/>
<wire x1="33.02" y1="-2.54" x2="36.83" y2="-2.54" width="0.3048" layer="94"/>
<wire x1="36.83" y1="-2.54" x2="40.64" y2="-2.54" width="0.3048" layer="94"/>
<wire x1="40.64" y1="-2.54" x2="40.64" y2="0" width="0.3048" layer="94"/>
<wire x1="40.64" y1="0" x2="40.64" y2="2.54" width="0.3048" layer="94"/>
<wire x1="53.34" y1="-7.62" x2="53.34" y2="-3.81" width="0.3048" layer="94"/>
<wire x1="53.34" y1="-3.81" x2="50.8" y2="3.81" width="0.3048" layer="94"/>
<wire x1="53.34" y1="3.175" x2="53.34" y2="7.62" width="0.3048" layer="94"/>
<wire x1="60.96" y1="-7.62" x2="60.96" y2="-3.81" width="0.3048" layer="94"/>
<wire x1="60.96" y1="-3.81" x2="58.42" y2="3.81" width="0.3048" layer="94"/>
<wire x1="60.96" y1="3.175" x2="60.96" y2="7.62" width="0.3048" layer="94"/>
<wire x1="36.83" y1="2.54" x2="36.83" y2="5.08" width="0.3048" layer="94"/>
<wire x1="36.83" y1="-2.54" x2="36.83" y2="-5.08" width="0.3048" layer="94"/>
<wire x1="68.58" y1="-7.62" x2="68.58" y2="-3.81" width="0.3048" layer="94"/>
<wire x1="68.58" y1="-3.81" x2="69.85" y2="0" width="0.3048" layer="94"/>
<wire x1="69.85" y1="0" x2="71.12" y2="3.81" width="0.3048" layer="94"/>
<wire x1="68.58" y1="3.175" x2="68.58" y2="7.62" width="0.3048" layer="94"/>
<wire x1="68.58" y1="3.175" x2="71.755" y2="3.175" width="0.3048" layer="94"/>
<wire x1="40.64" y1="0" x2="69.85" y2="0" width="0.3048" layer="94" style="shortdash"/>
<wire x1="24.765" y1="1.27" x2="29.845" y2="1.27" width="0.3048" layer="94"/>
<wire x1="27.305" y1="-1.27" x2="29.845" y2="1.27" width="0.3048" layer="94"/>
<wire x1="24.765" y1="1.27" x2="27.305" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="29.21" y1="0" x2="32.385" y2="0" width="0.3048" layer="94" style="shortdash"/>
<text x="-20.32" y="5.08" size="1.778" layer="95">Q1</text>
<text x="30.48" y="5.08" size="1.778" layer="95">Q2</text>
<text x="-12.7" y="3.81" size="1.778" layer="95">A1</text>
<text x="38.1" y="3.81" size="1.778" layer="95">A1</text>
<text x="-12.7" y="-5.08" size="1.778" layer="95">A2</text>
<text x="38.1" y="-5.08" size="1.778" layer="95">A2</text>
<text x="-3.81" y="6.35" size="1.778" layer="95">1</text>
<text x="46.99" y="6.35" size="1.778" layer="95">1</text>
<text x="3.81" y="6.35" size="1.778" layer="95">3</text>
<text x="54.61" y="6.35" size="1.778" layer="95">3</text>
<text x="11.43" y="6.35" size="1.778" layer="95">5</text>
<text x="62.23" y="6.35" size="1.778" layer="95">5</text>
<text x="-3.81" y="-7.62" size="1.778" layer="95">2</text>
<text x="46.99" y="-7.62" size="1.778" layer="95">2</text>
<text x="3.81" y="-7.62" size="1.778" layer="95">4</text>
<text x="54.61" y="-7.62" size="1.778" layer="95">4</text>
<text x="11.43" y="-7.62" size="1.778" layer="95">6</text>
<text x="62.23" y="-7.62" size="1.778" layer="95">6</text>
<text x="19.05" y="6.35" size="1.778" layer="95">122</text>
<text x="69.85" y="6.35" size="1.778" layer="95">111</text>
<text x="19.05" y="-7.62" size="1.778" layer="95">121</text>
<text x="69.85" y="-7.62" size="1.778" layer="95">112</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="POKUS">
<gates>
<gate name="G$1" symbol="POKUS" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="supply1">
<packages>
</packages>
<symbols>
<symbol name="GNDA">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<wire x1="-1.0922" y1="-0.508" x2="1.0922" y2="-0.508" width="0.254" layer="94"/>
<wire x1="-0.6223" y1="-1.016" x2="0.6223" y2="-1.016" width="0.254" layer="94"/>
<wire x1="-0.3048" y1="-1.524" x2="0.3302" y2="-1.524" width="0.254" layer="94"/>
<pin name="GNDA" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="GND">
<wire x1="-1.905" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-2.54" y="-2.54" size="1.778" layer="96">&gt;VALUE</text>
<pin name="GND" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GNDA" prefix="GND" uservalue="yes">
<gates>
<gate name="1" symbol="GNDA" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="GND" prefix="GND">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="resistor">
<description>&lt;b&gt;Resistors, Capacitors, Inductors&lt;/b&gt;&lt;p&gt;
Based on the previous libraries:
&lt;ul&gt;
&lt;li&gt;r.lbr
&lt;li&gt;cap.lbr 
&lt;li&gt;cap-fe.lbr
&lt;li&gt;captant.lbr
&lt;li&gt;polcap.lbr
&lt;li&gt;ipc-smd.lbr
&lt;/ul&gt;
All SMD packages are defined according to the IPC specifications and  CECC&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;&lt;p&gt;
&lt;p&gt;
for Electrolyt Capacitors see also :&lt;p&gt;
www.bccomponents.com &lt;p&gt;
www.panasonic.com&lt;p&gt;
www.kemet.com&lt;p&gt;
&lt;p&gt;
for trimmer refence see : &lt;u&gt;www.electrospec-inc.com/cross_references/trimpotcrossref.asp&lt;/u&gt;&lt;p&gt;

&lt;map name="nav_main"&gt;
&lt;area shape="rect" coords="0,1,140,23" href="../military_specs.asp" title=""&gt;
&lt;area shape="rect" coords="0,24,140,51" href="../about.asp" title=""&gt;
&lt;area shape="rect" coords="1,52,140,77" href="../rfq.asp" title=""&gt;
&lt;area shape="rect" coords="0,78,139,103" href="../products.asp" title=""&gt;
&lt;area shape="rect" coords="1,102,138,128" href="../excess_inventory.asp" title=""&gt;
&lt;area shape="rect" coords="1,129,138,150" href="../edge.asp" title=""&gt;
&lt;area shape="rect" coords="1,151,139,178" href="../industry_links.asp" title=""&gt;
&lt;area shape="rect" coords="0,179,139,201" href="../comments.asp" title=""&gt;
&lt;area shape="rect" coords="1,203,138,231" href="../directory.asp" title=""&gt;
&lt;area shape="default" nohref&gt;
&lt;/map&gt;

&lt;html&gt;

&lt;title&gt;&lt;/title&gt;

 &lt;LINK REL="StyleSheet" TYPE="text/css" HREF="style-sheet.css"&gt;

&lt;body bgcolor="#ffffff" text="#000000" marginwidth="0" marginheight="0" topmargin="0" leftmargin="0"&gt;
&lt;table border=0 cellspacing=0 cellpadding=0 width="100%" cellpaddding=0 height="55%"&gt;
&lt;tr valign="top"&gt;

&lt;/td&gt;
&lt;! &lt;td width="10"&gt;&amp;nbsp;&lt;/td&gt;
&lt;td width="90%"&gt;

&lt;b&gt;&lt;font color="#0000FF" size="4"&gt;TRIM-POT CROSS REFERENCE&lt;/font&gt;&lt;/b&gt;
&lt;P&gt;
&lt;TABLE BORDER=0 CELLSPACING=1 CELLPADDING=2&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;RECTANGULAR MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;BOURNS&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;BI&amp;nbsp;TECH&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;DALE-VISHAY&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;PHILIPS/MEPCO&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;MURATA&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;PANASONIC&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;SPECTROL&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;MILSPEC&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;&lt;TD&gt;&amp;nbsp;&lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3 &gt;
      3005P&lt;BR&gt;
      3006P&lt;BR&gt;
      3006W&lt;BR&gt;
      3006Y&lt;BR&gt;
      3009P&lt;BR&gt;
      3009W&lt;BR&gt;
      3009Y&lt;BR&gt;
      3057J&lt;BR&gt;
      3057L&lt;BR&gt;
      3057P&lt;BR&gt;
      3057Y&lt;BR&gt;
      3059J&lt;BR&gt;
      3059L&lt;BR&gt;
      3059P&lt;BR&gt;
      3059Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      89P&lt;BR&gt;
      89W&lt;BR&gt;
      89X&lt;BR&gt;
      89PH&lt;BR&gt;
      76P&lt;BR&gt;
      89XH&lt;BR&gt;
      78SLT&lt;BR&gt;
      78L&amp;nbsp;ALT&lt;BR&gt;
      56P&amp;nbsp;ALT&lt;BR&gt;
      78P&amp;nbsp;ALT&lt;BR&gt;
      T8S&lt;BR&gt;
      78L&lt;BR&gt;
      56P&lt;BR&gt;
      78P&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      T18/784&lt;BR&gt;
      783&lt;BR&gt;
      781&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      2199&lt;BR&gt;
      1697/1897&lt;BR&gt;
      1680/1880&lt;BR&gt;
      2187&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      8035EKP/CT20/RJ-20P&lt;BR&gt;
      -&lt;BR&gt;
      RJ-20X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      1211L&lt;BR&gt;
      8012EKQ&amp;nbsp;ALT&lt;BR&gt;
      8012EKR&amp;nbsp;ALT&lt;BR&gt;
      1211P&lt;BR&gt;
      8012EKJ&lt;BR&gt;
      8012EKL&lt;BR&gt;
      8012EKQ&lt;BR&gt;
      8012EKR&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      2101P&lt;BR&gt;
      2101W&lt;BR&gt;
      2101Y&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      2102L&lt;BR&gt;
      2102S&lt;BR&gt;
      2102Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      EVMCOG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      43P&lt;BR&gt;
      43W&lt;BR&gt;
      43Y&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      40L&lt;BR&gt;
      40P&lt;BR&gt;
      40Y&lt;BR&gt;
      70Y-T602&lt;BR&gt;
      70L&lt;BR&gt;
      70P&lt;BR&gt;
      70Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      -&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SQUARE MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
   &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MURATA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;SPECTROL&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MILSPEC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3250L&lt;BR&gt;
      3250P&lt;BR&gt;
      3250W&lt;BR&gt;
      3250X&lt;BR&gt;
      3252P&lt;BR&gt;
      3252W&lt;BR&gt;
      3252X&lt;BR&gt;
      3260P&lt;BR&gt;
      3260W&lt;BR&gt;
      3260X&lt;BR&gt;
      3262P&lt;BR&gt;
      3262W&lt;BR&gt;
      3262X&lt;BR&gt;
      3266P&lt;BR&gt;
      3266W&lt;BR&gt;
      3266X&lt;BR&gt;
      3290H&lt;BR&gt;
      3290P&lt;BR&gt;
      3290W&lt;BR&gt;
      3292P&lt;BR&gt;
      3292W&lt;BR&gt;
      3292X&lt;BR&gt;
      3296P&lt;BR&gt;
      3296W&lt;BR&gt;
      3296X&lt;BR&gt;
      3296Y&lt;BR&gt;
      3296Z&lt;BR&gt;
      3299P&lt;BR&gt;
      3299W&lt;BR&gt;
      3299X&lt;BR&gt;
      3299Y&lt;BR&gt;
      3299Z&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      -&lt;BR&gt;
      64W&amp;nbsp;ALT&lt;BR&gt;
      -&lt;BR&gt;
      64P&amp;nbsp;ALT&lt;BR&gt;
      64W&amp;nbsp;ALT&lt;BR&gt;
      64X&amp;nbsp;ALT&lt;BR&gt;
      64P&lt;BR&gt;
      64W&lt;BR&gt;
      64X&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66P&lt;BR&gt;
      66W&lt;BR&gt;
      66X&lt;BR&gt;
      67P&lt;BR&gt;
      67W&lt;BR&gt;
      67X&lt;BR&gt;
      67Y&lt;BR&gt;
      67Z&lt;BR&gt;
      68P&lt;BR&gt;
      68W&lt;BR&gt;
      68X&lt;BR&gt;
      67Y&amp;nbsp;ALT&lt;BR&gt;
      67Z&amp;nbsp;ALT&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      5050&lt;BR&gt;
      5091&lt;BR&gt;
      5080&lt;BR&gt;
      5087&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T63YB&lt;BR&gt;
      T63XB&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      5887&lt;BR&gt;
      5891&lt;BR&gt;
      5880&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T93Z&lt;BR&gt;
      T93YA&lt;BR&gt;
      T93XA&lt;BR&gt;
      T93YB&lt;BR&gt;
      T93XB&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8026EKP&lt;BR&gt;
      8026EKW&lt;BR&gt;
      8026EKM&lt;BR&gt;
      8026EKP&lt;BR&gt;
      8026EKB&lt;BR&gt;
      8026EKM&lt;BR&gt;
      1309X&lt;BR&gt;
      1309P&lt;BR&gt;
      1309W&lt;BR&gt;
      8024EKP&lt;BR&gt;
      8024EKW&lt;BR&gt;
      8024EKN&lt;BR&gt;
      RJ-9P/CT9P&lt;BR&gt;
      RJ-9W&lt;BR&gt;
      RJ-9X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3103P&lt;BR&gt;
      3103Y&lt;BR&gt;
      3103Z&lt;BR&gt;
      3103P&lt;BR&gt;
      3103Y&lt;BR&gt;
      3103Z&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3105P/3106P&lt;BR&gt;
      3105W/3106W&lt;BR&gt;
      3105X/3106X&lt;BR&gt;
      3105Y/3106Y&lt;BR&gt;
      3105Z/3105Z&lt;BR&gt;
      3102P&lt;BR&gt;
      3102W&lt;BR&gt;
      3102X&lt;BR&gt;
      3102Y&lt;BR&gt;
      3102Z&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMCBG&lt;BR&gt;
      EVMCCG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      55-1-X&lt;BR&gt;
      55-4-X&lt;BR&gt;
      55-3-X&lt;BR&gt;
      55-2-X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      50-2-X&lt;BR&gt;
      50-4-X&lt;BR&gt;
      50-3-X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      64P&lt;BR&gt;
      64W&lt;BR&gt;
      64X&lt;BR&gt;
      64Y&lt;BR&gt;
      64Z&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SINGLE TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MURATA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;SPECTROL&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MILSPEC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3323P&lt;BR&gt;
      3323S&lt;BR&gt;
      3323W&lt;BR&gt;
      3329H&lt;BR&gt;
      3329P&lt;BR&gt;
      3329W&lt;BR&gt;
      3339H&lt;BR&gt;
      3339P&lt;BR&gt;
      3339W&lt;BR&gt;
      3352E&lt;BR&gt;
      3352H&lt;BR&gt;
      3352K&lt;BR&gt;
      3352P&lt;BR&gt;
      3352T&lt;BR&gt;
      3352V&lt;BR&gt;
      3352W&lt;BR&gt;
      3362H&lt;BR&gt;
      3362M&lt;BR&gt;
      3362P&lt;BR&gt;
      3362R&lt;BR&gt;
      3362S&lt;BR&gt;
      3362U&lt;BR&gt;
      3362W&lt;BR&gt;
      3362X&lt;BR&gt;
      3386B&lt;BR&gt;
      3386C&lt;BR&gt;
      3386F&lt;BR&gt;
      3386H&lt;BR&gt;
      3386K&lt;BR&gt;
      3386M&lt;BR&gt;
      3386P&lt;BR&gt;
      3386S&lt;BR&gt;
      3386W&lt;BR&gt;
      3386X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      25P&lt;BR&gt;
      25S&lt;BR&gt;
      25RX&lt;BR&gt;
      82P&lt;BR&gt;
      82M&lt;BR&gt;
      82PA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      91E&lt;BR&gt;
      91X&lt;BR&gt;
      91T&lt;BR&gt;
      91B&lt;BR&gt;
      91A&lt;BR&gt;
      91V&lt;BR&gt;
      91W&lt;BR&gt;
      25W&lt;BR&gt;
      25V&lt;BR&gt;
      25P&lt;BR&gt;
      -&lt;BR&gt;
      25S&lt;BR&gt;
      25U&lt;BR&gt;
      25RX&lt;BR&gt;
      25X&lt;BR&gt;
      72XW&lt;BR&gt;
      72XL&lt;BR&gt;
      72PM&lt;BR&gt;
      72RX&lt;BR&gt;
      -&lt;BR&gt;
      72PX&lt;BR&gt;
      72P&lt;BR&gt;
      72RXW&lt;BR&gt;
      72RXL&lt;BR&gt;
      72X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T7YB&lt;BR&gt;
      T7YA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      TXD&lt;BR&gt;
      TYA&lt;BR&gt;
      TYP&lt;BR&gt;
      -&lt;BR&gt;
      TYD&lt;BR&gt;
      TX&lt;BR&gt;
      -&lt;BR&gt;
      150SX&lt;BR&gt;
      100SX&lt;BR&gt;
      102T&lt;BR&gt;
      101S&lt;BR&gt;
      190T&lt;BR&gt;
      150TX&lt;BR&gt;
      101&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      101SX&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ET6P&lt;BR&gt;
      ET6S&lt;BR&gt;
      ET6X&lt;BR&gt;
      RJ-6W/8014EMW&lt;BR&gt;
      RJ-6P/8014EMP&lt;BR&gt;
      RJ-6X/8014EMX&lt;BR&gt;
      TM7W&lt;BR&gt;
      TM7P&lt;BR&gt;
      TM7X&lt;BR&gt;
      -&lt;BR&gt;
      8017SMS&lt;BR&gt;
      -&lt;BR&gt;
      8017SMB&lt;BR&gt;
      8017SMA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      CT-6W&lt;BR&gt;
      CT-6H&lt;BR&gt;
      CT-6P&lt;BR&gt;
      CT-6R&lt;BR&gt;
      -&lt;BR&gt;
      CT-6V&lt;BR&gt;
      CT-6X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8038EKV&lt;BR&gt;
      -&lt;BR&gt;
      8038EKX&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8038EKP&lt;BR&gt;
      8038EKZ&lt;BR&gt;
      8038EKW&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3321H&lt;BR&gt;
      3321P&lt;BR&gt;
      3321N&lt;BR&gt;
      1102H&lt;BR&gt;
      1102P&lt;BR&gt;
      1102T&lt;BR&gt;
      RVA0911V304A&lt;BR&gt;
      -&lt;BR&gt;
      RVA0911H413A&lt;BR&gt;
      RVG0707V100A&lt;BR&gt;
      RVA0607V(H)306A&lt;BR&gt;
      RVA1214H213A&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3104B&lt;BR&gt;
      3104C&lt;BR&gt;
      3104F&lt;BR&gt;
      3104H&lt;BR&gt;
      -&lt;BR&gt;
      3104M&lt;BR&gt;
      3104P&lt;BR&gt;
      3104S&lt;BR&gt;
      3104W&lt;BR&gt;
      3104X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      EVMQ0G&lt;BR&gt;
      EVMQIG&lt;BR&gt;
      EVMQ3G&lt;BR&gt;
      EVMS0G&lt;BR&gt;
      EVMQ0G&lt;BR&gt;
      EVMG0G&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMK4GA00B&lt;BR&gt;
      EVM30GA00B&lt;BR&gt;
      EVMK0GA00B&lt;BR&gt;
      EVM38GA00B&lt;BR&gt;
      EVMB6&lt;BR&gt;
      EVLQ0&lt;BR&gt;
      -&lt;BR&gt;
      EVMMSG&lt;BR&gt;
      EVMMBG&lt;BR&gt;
      EVMMAG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMMCS&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM1&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM0&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM3&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      62-3-1&lt;BR&gt;
      62-1-2&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      67R&lt;BR&gt;
      -&lt;BR&gt;
      67P&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      67X&lt;BR&gt;
      63V&lt;BR&gt;
      63S&lt;BR&gt;
      63M&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      63H&lt;BR&gt;
      63P&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      63X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
&lt;/TABLE&gt;
&lt;P&gt;&amp;nbsp;&lt;P&gt;
&lt;TABLE BORDER=0 CELLSPACING=1 CELLPADDING=3&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;
      &lt;FONT color="#0000FF" SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SMD TRIM-POT CROSS REFERENCE&lt;/B&gt;&lt;/FONT&gt;
      &lt;P&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURNS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;TOCOS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;AUX/KYOCERA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3224G&lt;BR&gt;
      3224J&lt;BR&gt;
      3224W&lt;BR&gt;
      3269P&lt;BR&gt;
      3269W&lt;BR&gt;
      3269X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      44G&lt;BR&gt;
      44J&lt;BR&gt;
      44W&lt;BR&gt;
      84P&lt;BR&gt;
      84W&lt;BR&gt;
      84X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST63Z&lt;BR&gt;
      ST63Y&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST5P&lt;BR&gt;
      ST5W&lt;BR&gt;
      ST5X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SINGLE TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURNS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;TOCOS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;AUX/KYOCERA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3314G&lt;BR&gt;
      3314J&lt;BR&gt;
      3364A/B&lt;BR&gt;
      3364C/D&lt;BR&gt;
      3364W/X&lt;BR&gt;
      3313G&lt;BR&gt;
      3313J&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      23B&lt;BR&gt;
      23A&lt;BR&gt;
      21X&lt;BR&gt;
      21W&lt;BR&gt;
      -&lt;BR&gt;
      22B&lt;BR&gt;
      22A&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ST5YL/ST53YL&lt;BR&gt;
      ST5YJ/5T53YJ&lt;BR&gt;
      ST-23A&lt;BR&gt;
      ST-22B&lt;BR&gt;
      ST-22&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ST-4B&lt;BR&gt;
      ST-4A&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST-3B&lt;BR&gt;
      ST-3A&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      EVM-6YS&lt;BR&gt;
      EVM-1E&lt;BR&gt;
      EVM-1G&lt;BR&gt;
      EVM-1D&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      G4B&lt;BR&gt;
      G4A&lt;BR&gt;
      TR04-3S1&lt;BR&gt;
      TRG04-2S1&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      DVR-43A&lt;BR&gt;
      CVR-42C&lt;BR&gt;
      CVR-42A/C&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
&lt;/TABLE&gt;
&lt;P&gt;
&lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;ALT =&amp;nbsp;ALTERNATE&lt;/B&gt;&lt;/FONT&gt;
&lt;P&gt;

&amp;nbsp;
&lt;P&gt;
&lt;/td&gt;
&lt;/tr&gt;
&lt;/table&gt;
&lt;/BODY&gt;&lt;/HTML&gt;</description>
<packages>
<package name="R0402">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R0603">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R0805">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="R0805W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; wave soldering&lt;p&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.0525" y="0" dx="1.5" dy="1" layer="1"/>
<smd name="2" x="1.0525" y="0" dx="1.5" dy="1" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="R1206">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R1206W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R1210">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-0.8999" x2="0.3" y2="0.8999" layer="35"/>
</package>
<package name="R1210W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="R2010">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="R2010W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.311" y="0" dx="2" dy="1.8" layer="1"/>
<smd name="2" x="2.311" y="0" dx="2" dy="1.8" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="R2012">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1001" y1="-0.5999" x2="0.1001" y2="0.5999" layer="35"/>
</package>
<package name="R2012W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.94" y="0" dx="1.5" dy="1" layer="1"/>
<smd name="2" x="0.94" y="0" dx="1.5" dy="1" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1001" y1="-0.5999" x2="0.1001" y2="0.5999" layer="35"/>
</package>
<package name="R2512">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R2512W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-2.896" y="0" dx="2" dy="2.1" layer="1"/>
<smd name="2" x="2.896" y="0" dx="2" dy="2.1" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R3216">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R3216W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R3225">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-1" x2="0.3" y2="1" layer="35"/>
</package>
<package name="R3225W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-1" x2="0.3" y2="1" layer="35"/>
</package>
<package name="R5025">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R5025W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.311" y="0" dx="2" dy="1.8" layer="1"/>
<smd name="2" x="2.311" y="0" dx="2" dy="1.8" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R6332">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
Source: http://download.siliconexpert.com/pdfs/2005/02/24/Semi_Ap/2/VSH/Resistor/dcrcwfre.pdf</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-3.1" y="0" dx="1" dy="3.2" layer="1"/>
<smd name="2" x="3.1" y="0" dx="1" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R6332W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; wave soldering&lt;p&gt;
Source: http://download.siliconexpert.com/pdfs/2005/02/24/Semi_Ap/2/VSH/Resistor/dcrcwfre.pdf</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-3.196" y="0" dx="1.2" dy="3.2" layer="1"/>
<smd name="2" x="3.196" y="0" dx="1.2" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="M0805">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.10 W</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="0.7112" y1="0.635" x2="-0.7112" y2="0.635" width="0.1524" layer="51"/>
<wire x1="0.7112" y1="-0.635" x2="-0.7112" y2="-0.635" width="0.1524" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0414" y1="-0.7112" x2="-0.6858" y2="0.7112" layer="51"/>
<rectangle x1="0.6858" y1="-0.7112" x2="1.0414" y2="0.7112" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5999" x2="0.1999" y2="0.5999" layer="35"/>
</package>
<package name="M1206">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="1.143" y1="0.8382" x2="-1.143" y2="0.8382" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.8382" x2="-1.143" y2="-0.8382" width="0.1524" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.9144" x2="-1.1176" y2="0.9144" layer="51"/>
<rectangle x1="1.1176" y1="-0.9144" x2="1.7018" y2="0.9144" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="M1406">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.12 W</description>
<wire x1="-2.973" y1="0.983" x2="2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-0.983" x2="-2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-0.983" x2="-2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="0.983" x2="2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.3208" y1="0.762" x2="-1.3208" y2="0.762" width="0.1524" layer="51"/>
<wire x1="1.3208" y1="-0.762" x2="-1.3208" y2="-0.762" width="0.1524" layer="51"/>
<smd name="1" x="-1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<smd name="2" x="1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.8542" y1="-0.8382" x2="-1.2954" y2="0.8382" layer="51"/>
<rectangle x1="1.2954" y1="-0.8382" x2="1.8542" y2="0.8382" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="M2012">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.10 W</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="0.7112" y1="0.635" x2="-0.7112" y2="0.635" width="0.1524" layer="51"/>
<wire x1="0.7112" y1="-0.635" x2="-0.7112" y2="-0.635" width="0.1524" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0414" y1="-0.7112" x2="-0.6858" y2="0.7112" layer="51"/>
<rectangle x1="0.6858" y1="-0.7112" x2="1.0414" y2="0.7112" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5999" x2="0.1999" y2="0.5999" layer="35"/>
</package>
<package name="M2309">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-4.473" y1="1.483" x2="4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="-1.483" x2="-4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-4.473" y1="-1.483" x2="-4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="1.483" x2="4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.413" y1="1.1684" x2="-2.4384" y2="1.1684" width="0.1524" layer="51"/>
<wire x1="2.413" y1="-1.1684" x2="-2.413" y2="-1.1684" width="0.1524" layer="51"/>
<smd name="1" x="-2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<smd name="2" x="2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.048" y1="-1.2446" x2="-2.3876" y2="1.2446" layer="51"/>
<rectangle x1="2.3876" y1="-1.2446" x2="3.048" y2="1.2446" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="M3216">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="1.143" y1="0.8382" x2="-1.143" y2="0.8382" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.8382" x2="-1.143" y2="-0.8382" width="0.1524" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.9144" x2="-1.1176" y2="0.9144" layer="51"/>
<rectangle x1="1.1176" y1="-0.9144" x2="1.7018" y2="0.9144" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="M3516">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.12 W</description>
<wire x1="-2.973" y1="0.983" x2="2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-0.983" x2="-2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-0.983" x2="-2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="0.983" x2="2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.3208" y1="0.762" x2="-1.3208" y2="0.762" width="0.1524" layer="51"/>
<wire x1="1.3208" y1="-0.762" x2="-1.3208" y2="-0.762" width="0.1524" layer="51"/>
<smd name="1" x="-1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<smd name="2" x="1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.8542" y1="-0.8382" x2="-1.2954" y2="0.8382" layer="51"/>
<rectangle x1="1.2954" y1="-0.8382" x2="1.8542" y2="0.8382" layer="51"/>
<rectangle x1="-0.4001" y1="-0.7" x2="0.4001" y2="0.7" layer="35"/>
</package>
<package name="M5923">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-4.473" y1="1.483" x2="4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="-1.483" x2="-4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-4.473" y1="-1.483" x2="-4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="1.483" x2="4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.413" y1="1.1684" x2="-2.4384" y2="1.1684" width="0.1524" layer="51"/>
<wire x1="2.413" y1="-1.1684" x2="-2.413" y2="-1.1684" width="0.1524" layer="51"/>
<smd name="1" x="-2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<smd name="2" x="2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.048" y1="-1.2446" x2="-2.3876" y2="1.2446" layer="51"/>
<rectangle x1="2.3876" y1="-1.2446" x2="3.048" y2="1.2446" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="0204/5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0" x2="-2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-1.778" y1="0.635" x2="-1.524" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.778" y1="-0.635" x2="-1.524" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="1.524" y1="-0.889" x2="1.778" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="1.524" y1="0.889" x2="1.778" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.778" y1="-0.635" x2="-1.778" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-1.524" y1="0.889" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0.762" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="-0.889" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-0.762" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="-1.143" y2="0.762" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="-1.143" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="1.524" y1="0.889" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.524" y1="-0.889" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.635" x2="1.778" y2="0.635" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.0066" y="1.1684" size="0.9906" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.1336" y="-2.3114" size="0.9906" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-0.254" x2="-1.778" y2="0.254" layer="51"/>
<rectangle x1="1.778" y1="-0.254" x2="2.032" y2="0.254" layer="51"/>
</package>
<package name="0204/7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 7.5 mm</description>
<wire x1="3.81" y1="0" x2="2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-3.81" y1="0" x2="-2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0.762" x2="-2.286" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.286" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="-0.762" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="1.016" x2="2.54" y2="0.762" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="1.016" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="0.889" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.016" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="-0.889" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="-1.778" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="-1.778" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="2.286" y1="1.016" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.286" y1="-1.016" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="2.54" y2="0.762" width="0.1524" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.2954" size="0.9906" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.6256" y="-0.4826" size="0.9906" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.54" y1="-0.254" x2="2.921" y2="0.254" layer="21"/>
<rectangle x1="-2.921" y1="-0.254" x2="-2.54" y2="0.254" layer="21"/>
</package>
<package name="0204V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 2.5 mm</description>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.508" layer="51"/>
<wire x1="-0.127" y1="0" x2="0.127" y2="0" width="0.508" layer="21"/>
<circle x="-1.27" y="0" radius="0.889" width="0.1524" layer="51"/>
<circle x="-1.27" y="0" radius="0.635" width="0.0508" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.1336" y="1.1684" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.1336" y="-2.3114" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0207/10">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 10 mm</description>
<wire x1="5.08" y1="0" x2="4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-5.08" y1="0" x2="-4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.048" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.2606" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
</package>
<package name="0207/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 12 mm</description>
<wire x1="6.35" y1="0" x2="5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="4.445" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-4.445" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="4.445" y1="-0.3048" x2="5.3086" y2="0.3048" layer="21"/>
<rectangle x1="-5.3086" y1="-0.3048" x2="-4.445" y2="0.3048" layer="21"/>
</package>
<package name="0207/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 15mm</description>
<wire x1="7.62" y1="0" x2="6.604" y2="0" width="0.6096" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.604" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="5.715" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-5.715" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="5.715" y1="-0.3048" x2="6.5786" y2="0.3048" layer="21"/>
<rectangle x1="-6.5786" y1="-0.3048" x2="-5.715" y2="0.3048" layer="21"/>
</package>
<package name="0207/2V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 2.5 mm</description>
<wire x1="-1.27" y1="0" x2="-0.381" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.254" y1="0" x2="0.254" y2="0" width="0.6096" layer="21"/>
<wire x1="0.381" y1="0" x2="1.27" y2="0" width="0.6096" layer="51"/>
<circle x="-1.27" y="0" radius="1.27" width="0.1524" layer="21"/>
<circle x="-1.27" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-0.0508" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.0508" y="-2.2352" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0207/5V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 5 mm</description>
<wire x1="-2.54" y1="0" x2="-0.889" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.762" y1="0" x2="0.762" y2="0" width="0.6096" layer="21"/>
<wire x1="0.889" y1="0" x2="2.54" y2="0" width="0.6096" layer="51"/>
<circle x="-2.54" y="0" radius="1.27" width="0.1016" layer="21"/>
<circle x="-2.54" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.143" y="0.889" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.143" y="-2.159" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0207/7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 7.5 mm</description>
<wire x1="-3.81" y1="0" x2="-3.429" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="51"/>
<wire x1="3.429" y1="0" x2="3.81" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.5588" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-3.429" y1="-0.3048" x2="-3.175" y2="0.3048" layer="51"/>
<rectangle x1="3.175" y1="-0.3048" x2="3.429" y2="0.3048" layer="51"/>
</package>
<package name="0309/10">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 10mm</description>
<wire x1="-4.699" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.318" y1="1.27" x2="-4.064" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.064" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="-1.524" x2="4.318" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="1.524" x2="4.318" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.318" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-4.064" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.397" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.524" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.397" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="-3.302" y2="1.397" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="-3.302" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="4.064" y1="1.524" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="4.064" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-1.27" x2="4.318" y2="1.27" width="0.1524" layer="51"/>
<wire x1="5.08" y1="0" x2="4.699" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" shape="octagon"/>
<text x="-4.191" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.6228" y1="-0.3048" x2="-4.318" y2="0.3048" layer="51"/>
<rectangle x1="4.318" y1="-0.3048" x2="4.6228" y2="0.3048" layer="51"/>
</package>
<package name="0309/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 12.5 mm</description>
<wire x1="6.35" y1="0" x2="5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.318" y1="1.27" x2="-4.064" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.064" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="-1.524" x2="4.318" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="1.524" x2="4.318" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.318" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.397" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.524" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.397" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="-3.302" y2="1.397" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="-3.302" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="4.064" y1="1.524" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="4.064" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-1.27" x2="4.318" y2="1.27" width="0.1524" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.8128" shape="octagon"/>
<text x="-4.191" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="4.318" y1="-0.3048" x2="5.1816" y2="0.3048" layer="21"/>
<rectangle x1="-5.1816" y1="-0.3048" x2="-4.318" y2="0.3048" layer="21"/>
</package>
<package name="0309V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 2.5 mm</description>
<wire x1="1.27" y1="0" x2="0.635" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.635" y1="0" x2="-1.27" y2="0" width="0.6096" layer="51"/>
<circle x="-1.27" y="0" radius="1.524" width="0.1524" layer="21"/>
<circle x="-1.27" y="0" radius="0.762" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="0.254" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0.254" y="-2.2098" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.254" y1="-0.3048" x2="0.5588" y2="0.3048" layer="51"/>
<rectangle x1="-0.635" y1="-0.3048" x2="-0.3302" y2="0.3048" layer="51"/>
<rectangle x1="-0.3302" y1="-0.3048" x2="0.254" y2="0.3048" layer="21"/>
</package>
<package name="0411/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 12.5 mm</description>
<wire x1="6.35" y1="0" x2="5.461" y2="0" width="0.762" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.461" y2="0" width="0.762" layer="51"/>
<wire x1="5.08" y1="-1.651" x2="5.08" y2="1.651" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.032" x2="5.08" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="-1.651" x2="-4.699" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="4.699" y1="-2.032" x2="5.08" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.651" x2="-4.699" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="2.032" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="1.905" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.032" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="-1.905" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="3.937" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="3.937" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.651" x2="-5.08" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="2.032" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-2.032" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.9144" shape="octagon"/>
<text x="-5.08" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.5814" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.3594" y1="-0.381" x2="-5.08" y2="0.381" layer="21"/>
<rectangle x1="5.08" y1="-0.381" x2="5.3594" y2="0.381" layer="21"/>
</package>
<package name="0411/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 15 mm</description>
<wire x1="5.08" y1="-1.651" x2="5.08" y2="1.651" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.032" x2="5.08" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="-1.651" x2="-4.699" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="4.699" y1="-2.032" x2="5.08" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.651" x2="-4.699" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="2.032" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="1.905" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.032" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="-1.905" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="3.937" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="3.937" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.651" x2="-5.08" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="2.032" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-2.032" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0" x2="-6.35" y2="0" width="0.762" layer="51"/>
<wire x1="6.35" y1="0" x2="7.62" y2="0" width="0.762" layer="51"/>
<pad name="1" x="-7.62" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="0.9144" shape="octagon"/>
<text x="-5.08" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.5814" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="5.08" y1="-0.381" x2="6.477" y2="0.381" layer="21"/>
<rectangle x1="-6.477" y1="-0.381" x2="-5.08" y2="0.381" layer="21"/>
</package>
<package name="0411V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 3.81 mm</description>
<wire x1="1.27" y1="0" x2="0.3048" y2="0" width="0.762" layer="51"/>
<wire x1="-1.5748" y1="0" x2="-2.54" y2="0" width="0.762" layer="51"/>
<circle x="-2.54" y="0" radius="2.032" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.9144" shape="octagon"/>
<text x="-0.508" y="1.143" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.5334" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.4732" y1="-0.381" x2="0.2032" y2="0.381" layer="21"/>
</package>
<package name="0414/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0414, grid 15 mm</description>
<wire x1="7.62" y1="0" x2="6.604" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.604" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.096" y1="1.905" x2="-5.842" y2="2.159" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.096" y1="-1.905" x2="-5.842" y2="-2.159" width="0.1524" layer="21" curve="90"/>
<wire x1="5.842" y1="-2.159" x2="6.096" y2="-1.905" width="0.1524" layer="21" curve="90"/>
<wire x1="5.842" y1="2.159" x2="6.096" y2="1.905" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.096" y1="-1.905" x2="-6.096" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-5.842" y1="2.159" x2="-4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="2.032" x2="-4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-5.842" y1="-2.159" x2="-4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-2.032" x2="-4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="-4.826" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.826" y1="-2.032" x2="4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="-2.032" x2="-4.826" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="5.842" y1="2.159" x2="4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="5.842" y1="-2.159" x2="4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-1.905" x2="6.096" y2="1.905" width="0.1524" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="2.5654" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="6.096" y1="-0.4064" x2="6.5024" y2="0.4064" layer="21"/>
<rectangle x1="-6.5024" y1="-0.4064" x2="-6.096" y2="0.4064" layer="21"/>
</package>
<package name="0414V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0414, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-2.54" y1="0" x2="-1.397" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="2.159" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.381" y="1.1684" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.381" y="-2.3622" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.2954" y1="-0.4064" x2="1.2954" y2="0.4064" layer="21"/>
</package>
<package name="0617/17">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 17.5 mm</description>
<wire x1="-8.89" y1="0" x2="-8.636" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.874" y1="3.048" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="2.794" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="-3.048" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="-2.794" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="-6.731" y2="2.794" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="-6.731" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="7.874" y1="3.048" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="7.874" y1="-3.048" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="-2.667" x2="-8.255" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="1.016" x2="-8.255" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-8.255" y1="1.016" x2="-8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-2.667" x2="8.255" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="8.255" y1="1.016" x2="8.255" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="8.255" y1="1.016" x2="8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="8.636" y1="0" x2="8.89" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="2.667" x2="-7.874" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="7.874" y1="3.048" x2="8.255" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.255" y1="-2.667" x2="-7.874" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="7.874" y1="-3.048" x2="8.255" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-8.89" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="8.89" y="0" drill="1.016" shape="octagon"/>
<text x="-8.128" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.096" y="-0.7112" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-8.5344" y1="-0.4064" x2="-8.2296" y2="0.4064" layer="51"/>
<rectangle x1="8.2296" y1="-0.4064" x2="8.5344" y2="0.4064" layer="51"/>
</package>
<package name="0617/22">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 22.5 mm</description>
<wire x1="-10.287" y1="0" x2="-11.43" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="-2.667" x2="-8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="3.048" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="2.794" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="-3.048" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="-2.794" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="-6.731" y2="2.794" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="-6.731" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="7.874" y1="3.048" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="7.874" y1="-3.048" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-2.667" x2="8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="11.43" y1="0" x2="10.287" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="2.667" x2="-7.874" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.255" y1="-2.667" x2="-7.874" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="7.874" y1="3.048" x2="8.255" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="7.874" y1="-3.048" x2="8.255" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-8.255" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.477" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.1854" y1="-0.4064" x2="-8.255" y2="0.4064" layer="21"/>
<rectangle x1="8.255" y1="-0.4064" x2="10.1854" y2="0.4064" layer="21"/>
</package>
<package name="0617V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 5 mm</description>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.8128" layer="51"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="3.048" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="0.635" y="1.4224" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0.635" y="-2.6162" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.3208" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="0922/22">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0922, grid 22.5 mm</description>
<wire x1="11.43" y1="0" x2="10.795" y2="0" width="0.8128" layer="51"/>
<wire x1="-11.43" y1="0" x2="-10.795" y2="0" width="0.8128" layer="51"/>
<wire x1="-10.16" y1="-4.191" x2="-10.16" y2="4.191" width="0.1524" layer="21"/>
<wire x1="-9.779" y1="4.572" x2="-8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="-8.636" y1="4.318" x2="-8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="-9.779" y1="-4.572" x2="-8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="-8.636" y1="-4.318" x2="-8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="4.318" x2="8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="4.318" x2="-8.636" y2="4.318" width="0.1524" layer="21"/>
<wire x1="8.636" y1="-4.318" x2="8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="-4.318" x2="-8.636" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="9.779" y1="4.572" x2="8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="9.779" y1="-4.572" x2="8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-4.191" x2="10.16" y2="4.191" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-4.191" x2="-9.779" y2="-4.572" width="0.1524" layer="21" curve="90"/>
<wire x1="-10.16" y1="4.191" x2="-9.779" y2="4.572" width="0.1524" layer="21" curve="-90"/>
<wire x1="9.779" y1="-4.572" x2="10.16" y2="-4.191" width="0.1524" layer="21" curve="90"/>
<wire x1="9.779" y1="4.572" x2="10.16" y2="4.191" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-10.16" y="5.1054" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.477" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.7188" y1="-0.4064" x2="-10.16" y2="0.4064" layer="51"/>
<rectangle x1="10.16" y1="-0.4064" x2="10.3124" y2="0.4064" layer="21"/>
<rectangle x1="-10.3124" y1="-0.4064" x2="-10.16" y2="0.4064" layer="21"/>
<rectangle x1="10.16" y1="-0.4064" x2="10.7188" y2="0.4064" layer="51"/>
</package>
<package name="P0613V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0613, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-2.54" y1="0" x2="-1.397" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="2.286" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.254" y="1.143" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.254" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.2954" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="P0613/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0613, grid 15 mm</description>
<wire x1="7.62" y1="0" x2="6.985" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.985" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.477" y1="2.032" x2="-6.223" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.477" y1="-2.032" x2="-6.223" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="6.223" y1="-2.286" x2="6.477" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="6.223" y1="2.286" x2="6.477" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.223" y1="2.286" x2="-5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.159" x2="-5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-6.223" y1="-2.286" x2="-5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="-2.159" x2="-5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="2.159" x2="5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="2.159" x2="-5.207" y2="2.159" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.159" x2="5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.159" x2="-5.207" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="6.223" y1="2.286" x2="5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="6.223" y1="-2.286" x2="5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-0.635" x2="6.477" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-0.635" x2="6.477" y2="0.635" width="0.1524" layer="51"/>
<wire x1="6.477" y1="2.032" x2="6.477" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="-2.032" x2="-6.477" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="0.635" x2="-6.477" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-6.477" y1="0.635" x2="-6.477" y2="2.032" width="0.1524" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.477" y="2.6924" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.7112" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-7.0358" y1="-0.4064" x2="-6.477" y2="0.4064" layer="51"/>
<rectangle x1="6.477" y1="-0.4064" x2="7.0358" y2="0.4064" layer="51"/>
</package>
<package name="P0817/22">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0817, grid 22.5 mm</description>
<wire x1="-10.414" y1="0" x2="-11.43" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.509" y1="-3.429" x2="-8.509" y2="3.429" width="0.1524" layer="21"/>
<wire x1="-8.128" y1="3.81" x2="-7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="3.556" x2="-7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-8.128" y1="-3.81" x2="-7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="-3.556" x2="-7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="3.556" x2="7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="3.556" x2="-6.985" y2="3.556" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-3.556" x2="7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-3.556" x2="-6.985" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="8.128" y1="3.81" x2="7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="8.128" y1="-3.81" x2="7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-3.429" x2="8.509" y2="3.429" width="0.1524" layer="21"/>
<wire x1="11.43" y1="0" x2="10.414" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.509" y1="3.429" x2="-8.128" y2="3.81" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.509" y1="-3.429" x2="-8.128" y2="-3.81" width="0.1524" layer="21" curve="90"/>
<wire x1="8.128" y1="3.81" x2="8.509" y2="3.429" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.128" y1="-3.81" x2="8.509" y2="-3.429" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-8.382" y="4.2164" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.223" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="6.604" y="-2.2606" size="1.27" layer="51" ratio="10" rot="R90">0817</text>
<rectangle x1="8.509" y1="-0.4064" x2="10.3124" y2="0.4064" layer="21"/>
<rectangle x1="-10.3124" y1="-0.4064" x2="-8.509" y2="0.4064" layer="21"/>
</package>
<package name="P0817V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0817, grid 6.35 mm</description>
<wire x1="-3.81" y1="0" x2="-5.08" y2="0" width="0.8128" layer="51"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.8128" layer="51"/>
<circle x="-5.08" y="0" radius="3.81" width="0.1524" layer="21"/>
<circle x="-5.08" y="0" radius="1.27" width="0.1524" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="1.016" shape="octagon"/>
<text x="-1.016" y="1.27" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.016" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-6.858" y="2.032" size="1.016" layer="21" ratio="12">0817</text>
<rectangle x1="-3.81" y1="-0.4064" x2="0" y2="0.4064" layer="21"/>
</package>
<package name="V234/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V234, grid 12.5 mm</description>
<wire x1="-4.953" y1="1.524" x2="-4.699" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="1.778" x2="4.953" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-1.778" x2="4.953" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.953" y1="-1.524" x2="-4.699" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.699" y1="1.778" x2="4.699" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="1.524" x2="-4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.778" x2="-4.699" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="4.953" y1="1.524" x2="4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="6.35" y1="0" x2="5.461" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.461" y2="0" width="0.8128" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="1.016" shape="octagon"/>
<text x="-4.953" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="4.953" y1="-0.4064" x2="5.4102" y2="0.4064" layer="21"/>
<rectangle x1="-5.4102" y1="-0.4064" x2="-4.953" y2="0.4064" layer="21"/>
</package>
<package name="V235/17">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V235, grid 17.78 mm</description>
<wire x1="-6.731" y1="2.921" x2="6.731" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-7.112" y1="2.54" x2="-7.112" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.921" x2="-6.731" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="7.112" y1="2.54" x2="7.112" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="8.89" y1="0" x2="7.874" y2="0" width="1.016" layer="51"/>
<wire x1="-7.874" y1="0" x2="-8.89" y2="0" width="1.016" layer="51"/>
<wire x1="-7.112" y1="-2.54" x2="-6.731" y2="-2.921" width="0.1524" layer="21" curve="90"/>
<wire x1="6.731" y1="2.921" x2="7.112" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.731" y1="-2.921" x2="7.112" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-7.112" y1="2.54" x2="-6.731" y2="2.921" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-8.89" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="8.89" y="0" drill="1.1938" shape="octagon"/>
<text x="-6.858" y="3.302" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.842" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="7.112" y1="-0.508" x2="7.747" y2="0.508" layer="21"/>
<rectangle x1="-7.747" y1="-0.508" x2="-7.112" y2="0.508" layer="21"/>
</package>
<package name="V526-0">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V526-0, grid 2.5 mm</description>
<wire x1="-2.54" y1="1.016" x2="-2.286" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.286" y1="1.27" x2="2.54" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.286" y1="-1.27" x2="2.54" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.54" y1="-1.016" x2="-2.286" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="1.27" x2="-2.286" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.016" x2="2.54" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.27" x2="2.286" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.016" x2="-2.54" y2="-1.016" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.413" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.413" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0102R">
<description>&lt;b&gt;CECC Size RC2211&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1" y1="-0.5" x2="1" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="-0.5" x2="1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="0.5" x2="-1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.5" x2="-1" y2="-0.5" width="0.2032" layer="51"/>
<smd name="1" x="-0.9" y="0" dx="0.5" dy="1.3" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.5" dy="1.3" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0102W">
<description>&lt;b&gt;CECC Size RC2211&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1" y1="-0.5" x2="1" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="-0.5" x2="1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="0.5" x2="-1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.5" x2="-1" y2="-0.5" width="0.2032" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="0.6" dy="1.3" layer="1"/>
<smd name="2" x="0.95" y="0" dx="0.6" dy="1.3" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0204R">
<description>&lt;b&gt;CECC Size RC3715&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1.7" y1="-0.6" x2="1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="-0.6" x2="1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="0.6" x2="-1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="-1.7" y1="0.6" x2="-1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="0.938" y1="0.6" x2="-0.938" y2="0.6" width="0.2032" layer="21"/>
<wire x1="-0.938" y1="-0.6" x2="0.938" y2="-0.6" width="0.2032" layer="21"/>
<smd name="1" x="-1.5" y="0" dx="0.8" dy="1.6" layer="1"/>
<smd name="2" x="1.5" y="0" dx="0.8" dy="1.6" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0204W">
<description>&lt;b&gt;CECC Size RC3715&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1.7" y1="-0.6" x2="1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="-0.6" x2="1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="0.6" x2="-1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="-1.7" y1="0.6" x2="-1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="0.684" y1="0.6" x2="-0.684" y2="0.6" width="0.2032" layer="21"/>
<wire x1="-0.684" y1="-0.6" x2="0.684" y2="-0.6" width="0.2032" layer="21"/>
<smd name="1" x="-1.5" y="0" dx="1.2" dy="1.6" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.2" dy="1.6" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0207R">
<description>&lt;b&gt;CECC Size RC6123&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-2.8" y1="-1" x2="2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="-1" x2="2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="1" x2="-2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="-2.8" y1="1" x2="-2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="1.2125" y1="1" x2="-1.2125" y2="1" width="0.2032" layer="21"/>
<wire x1="-1.2125" y1="-1" x2="1.2125" y2="-1" width="0.2032" layer="21"/>
<smd name="1" x="-2.25" y="0" dx="1.6" dy="2.5" layer="1"/>
<smd name="2" x="2.25" y="0" dx="1.6" dy="2.5" layer="1"/>
<text x="-2.2225" y="1.5875" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.2225" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0207W">
<description>&lt;b&gt;CECC Size RC6123&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-2.8" y1="-1" x2="2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="-1" x2="2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="1" x2="-2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="-2.8" y1="1" x2="-2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="1.149" y1="1" x2="-1.149" y2="1" width="0.2032" layer="21"/>
<wire x1="-1.149" y1="-1" x2="1.149" y2="-1" width="0.2032" layer="21"/>
<smd name="1" x="-2.6" y="0" dx="2.4" dy="2.5" layer="1"/>
<smd name="2" x="2.6" y="0" dx="2.4" dy="2.5" layer="1"/>
<text x="-2.54" y="1.5875" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="0922V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0922, grid 7.5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-5.08" y1="0" x2="-3.81" y2="0" width="0.8128" layer="51"/>
<circle x="-5.08" y="0" radius="4.572" width="0.1524" layer="21"/>
<circle x="-5.08" y="0" radius="1.905" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.508" y="1.6764" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.508" y="-2.9972" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-6.858" y="2.54" size="1.016" layer="21" ratio="12">0922</text>
<rectangle x1="-3.81" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="RDH/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type RDH, grid 15 mm</description>
<wire x1="-7.62" y1="0" x2="-6.858" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.096" y1="3.048" x2="-5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="2.794" x2="-5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="-3.048" x2="-5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-2.794" x2="-5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.794" x2="5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.794" x2="-4.953" y2="2.794" width="0.1524" layer="21"/>
<wire x1="4.953" y1="-2.794" x2="5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="-2.794" x2="-4.953" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="6.096" y1="3.048" x2="5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-3.048" x2="5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="-2.667" x2="-6.477" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="1.016" x2="-6.477" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-6.477" y1="1.016" x2="-6.477" y2="2.667" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-2.667" x2="6.477" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="6.477" y1="1.016" x2="6.477" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="6.477" y1="1.016" x2="6.477" y2="2.667" width="0.1524" layer="21"/>
<wire x1="6.858" y1="0" x2="7.62" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.477" y1="2.667" x2="-6.096" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="3.048" x2="6.477" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.477" y1="-2.667" x2="-6.096" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="6.096" y1="-3.048" x2="6.477" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.35" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="4.572" y="-1.7272" size="1.27" layer="51" ratio="10" rot="R90">RDH</text>
<rectangle x1="-6.7564" y1="-0.4064" x2="-6.4516" y2="0.4064" layer="51"/>
<rectangle x1="6.4516" y1="-0.4064" x2="6.7564" y2="0.4064" layer="51"/>
</package>
<package name="MINI_MELF-0102AX">
<description>&lt;b&gt;Mini MELF 0102 Axial&lt;/b&gt;</description>
<circle x="0" y="0" radius="0.6" width="0" layer="51"/>
<circle x="0" y="0" radius="0.6" width="0" layer="52"/>
<smd name="1" x="0" y="0" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="2" x="0" y="0" dx="1.9" dy="1.9" layer="16" roundness="100"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
<hole x="0" y="0" drill="1.3"/>
</package>
<package name="R0201">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; chip&lt;p&gt;
Source: http://www.vishay.com/docs/20008/dcrcw.pdf</description>
<smd name="1" x="-0.255" y="0" dx="0.28" dy="0.43" layer="1"/>
<smd name="2" x="0.255" y="0" dx="0.28" dy="0.43" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-0.15" x2="-0.15" y2="0.15" layer="51"/>
<rectangle x1="0.15" y1="-0.15" x2="0.3" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="-0.15" x2="0.15" y2="0.15" layer="21"/>
</package>
<package name="VTA52">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR52&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-15.24" y1="0" x2="-13.97" y2="0" width="0.6096" layer="51"/>
<wire x1="12.6225" y1="0.025" x2="12.6225" y2="4.725" width="0.1524" layer="21"/>
<wire x1="12.6225" y1="4.725" x2="-12.6225" y2="4.725" width="0.1524" layer="21"/>
<wire x1="-12.6225" y1="4.725" x2="-12.6225" y2="0.025" width="0.1524" layer="21"/>
<wire x1="-12.6225" y1="0.025" x2="-12.6225" y2="-4.65" width="0.1524" layer="21"/>
<wire x1="-12.6225" y1="-4.65" x2="12.6225" y2="-4.65" width="0.1524" layer="21"/>
<wire x1="12.6225" y1="-4.65" x2="12.6225" y2="0.025" width="0.1524" layer="21"/>
<wire x1="13.97" y1="0" x2="15.24" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-15.24" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="15.24" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="5.08" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-13.97" y1="-0.3048" x2="-12.5675" y2="0.3048" layer="21"/>
<rectangle x1="12.5675" y1="-0.3048" x2="13.97" y2="0.3048" layer="21"/>
</package>
<package name="VTA53">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR53&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-12.065" y1="0" x2="-10.795" y2="0" width="0.6096" layer="51"/>
<wire x1="9.8975" y1="0" x2="9.8975" y2="4.7" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="4.7" x2="-9.8975" y2="4.7" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="4.7" x2="-9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="0" x2="-9.8975" y2="-4.675" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="-4.675" x2="9.8975" y2="-4.675" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="-4.675" x2="9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="10.795" y1="0" x2="12.065" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-12.065" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="12.065" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="5.08" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.795" y1="-0.3048" x2="-9.8425" y2="0.3048" layer="21"/>
<rectangle x1="9.8425" y1="-0.3048" x2="10.795" y2="0.3048" layer="21"/>
</package>
<package name="VTA54">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR54&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-12.065" y1="0" x2="-10.795" y2="0" width="0.6096" layer="51"/>
<wire x1="9.8975" y1="0" x2="9.8975" y2="3.3" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="3.3" x2="-9.8975" y2="3.3" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="3.3" x2="-9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="0" x2="-9.8975" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="-3.3" x2="9.8975" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="-3.3" x2="9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="10.795" y1="0" x2="12.065" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-12.065" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="12.065" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.795" y1="-0.3048" x2="-9.8425" y2="0.3048" layer="21"/>
<rectangle x1="9.8425" y1="-0.3048" x2="10.795" y2="0.3048" layer="21"/>
</package>
<package name="VTA55">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR55&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-8.255" y1="0" x2="-6.985" y2="0" width="0.6096" layer="51"/>
<wire x1="6.405" y1="0" x2="6.405" y2="3.3" width="0.1524" layer="21"/>
<wire x1="6.405" y1="3.3" x2="-6.405" y2="3.3" width="0.1524" layer="21"/>
<wire x1="-6.405" y1="3.3" x2="-6.405" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.405" y1="0" x2="-6.405" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="-6.405" y1="-3.3" x2="6.405" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="6.405" y1="-3.3" x2="6.405" y2="0" width="0.1524" layer="21"/>
<wire x1="6.985" y1="0" x2="8.255" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-8.255" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="8.255" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-6.985" y1="-0.3048" x2="-6.35" y2="0.3048" layer="21"/>
<rectangle x1="6.35" y1="-0.3048" x2="6.985" y2="0.3048" layer="21"/>
</package>
<package name="VTA56">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR56&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-6.35" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="4.5" y1="0" x2="4.5" y2="3.3" width="0.1524" layer="21"/>
<wire x1="4.5" y1="3.3" x2="-4.5" y2="3.3" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="3.3" x2="-4.5" y2="0" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="0" x2="-4.5" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="-3.3" x2="4.5" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="4.5" y1="-3.3" x2="4.5" y2="0" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0" x2="6.35" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.08" y1="-0.3048" x2="-4.445" y2="0.3048" layer="21"/>
<rectangle x1="4.445" y1="-0.3048" x2="5.08" y2="0.3048" layer="21"/>
</package>
<package name="VMTA55">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RNC55&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-5.08" y1="0" x2="-4.26" y2="0" width="0.6096" layer="51"/>
<wire x1="3.3375" y1="-1.45" x2="3.3375" y2="1.45" width="0.1524" layer="21"/>
<wire x1="3.3375" y1="1.45" x2="-3.3625" y2="1.45" width="0.1524" layer="21"/>
<wire x1="-3.3625" y1="1.45" x2="-3.3625" y2="-1.45" width="0.1524" layer="21"/>
<wire x1="-3.3625" y1="-1.45" x2="3.3375" y2="-1.45" width="0.1524" layer="21"/>
<wire x1="4.235" y1="0" x2="5.08" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.1" shape="octagon"/>
<text x="-3.175" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.26" y1="-0.3048" x2="-3.3075" y2="0.3048" layer="21"/>
<rectangle x1="3.2825" y1="-0.3048" x2="4.235" y2="0.3048" layer="21"/>
</package>
<package name="VMTB60">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RNC60&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-6.35" y1="0" x2="-5.585" y2="0" width="0.6096" layer="51"/>
<wire x1="4.6875" y1="-1.95" x2="4.6875" y2="1.95" width="0.1524" layer="21"/>
<wire x1="4.6875" y1="1.95" x2="-4.6875" y2="1.95" width="0.1524" layer="21"/>
<wire x1="-4.6875" y1="1.95" x2="-4.6875" y2="-1.95" width="0.1524" layer="21"/>
<wire x1="-4.6875" y1="-1.95" x2="4.6875" y2="-1.95" width="0.1524" layer="21"/>
<wire x1="5.585" y1="0" x2="6.35" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="1.1" shape="octagon"/>
<text x="-4.445" y="2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.445" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.585" y1="-0.3048" x2="-4.6325" y2="0.3048" layer="21"/>
<rectangle x1="4.6325" y1="-0.3048" x2="5.585" y2="0.3048" layer="21"/>
</package>
<package name="R4527">
<description>&lt;b&gt;Package 4527&lt;/b&gt;&lt;p&gt;
Source: http://www.vishay.com/docs/31059/wsrhigh.pdf</description>
<wire x1="-5.675" y1="-3.375" x2="5.65" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="5.65" y1="-3.375" x2="5.65" y2="3.375" width="0.2032" layer="51"/>
<wire x1="5.65" y1="3.375" x2="-5.675" y2="3.375" width="0.2032" layer="21"/>
<wire x1="-5.675" y1="3.375" x2="-5.675" y2="-3.375" width="0.2032" layer="51"/>
<smd name="1" x="-4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<smd name="2" x="4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<text x="-5.715" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.715" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC0001">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3.075" y1="-1.8" x2="3.075" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.075" y1="-1.8" x2="3.075" y2="1.8" width="0.2032" layer="51"/>
<wire x1="3.075" y1="1.8" x2="-3.075" y2="1.8" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="1.606" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="-1.606" x2="-3.075" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.075" y1="1.606" x2="3.075" y2="1.8" width="0.2032" layer="21"/>
<wire x1="3.075" y1="-1.8" x2="3.075" y2="-1.606" width="0.2032" layer="21"/>
<smd name="1" x="-2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<smd name="2" x="2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<text x="-2.544" y="2.229" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.544" y="-3.501" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC0002">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-5.55" y1="3.375" x2="-5.55" y2="-3.375" width="0.2032" layer="51"/>
<wire x1="-5.55" y1="-3.375" x2="5.55" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="5.55" y1="-3.375" x2="5.55" y2="3.375" width="0.2032" layer="51"/>
<wire x1="5.55" y1="3.375" x2="-5.55" y2="3.375" width="0.2032" layer="21"/>
<smd name="1" x="-4.575" y="0.025" dx="3.94" dy="5.84" layer="1"/>
<smd name="2" x="4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<text x="-5.65" y="3.9" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.65" y="-5.15" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC01/2">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-2.45" y1="1.475" x2="-2.45" y2="-1.475" width="0.2032" layer="51"/>
<wire x1="-2.45" y1="-1.475" x2="2.45" y2="-1.475" width="0.2032" layer="21"/>
<wire x1="2.45" y1="-1.475" x2="2.45" y2="1.475" width="0.2032" layer="51"/>
<wire x1="2.45" y1="1.475" x2="-2.45" y2="1.475" width="0.2032" layer="21"/>
<wire x1="-2.45" y1="1.475" x2="-2.45" y2="1.106" width="0.2032" layer="21"/>
<wire x1="-2.45" y1="-1.106" x2="-2.45" y2="-1.475" width="0.2032" layer="21"/>
<wire x1="2.45" y1="1.106" x2="2.45" y2="1.475" width="0.2032" layer="21"/>
<wire x1="2.45" y1="-1.475" x2="2.45" y2="-1.106" width="0.2032" layer="21"/>
<smd name="1" x="-2.1" y="0" dx="2.16" dy="1.78" layer="1"/>
<smd name="2" x="2.1" y="0" dx="2.16" dy="1.78" layer="1"/>
<text x="-2.544" y="1.904" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.544" y="-3.176" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC2515">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3.075" y1="-1.8" x2="3.05" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.05" y1="-1.8" x2="3.05" y2="1.8" width="0.2032" layer="51"/>
<wire x1="3.05" y1="1.8" x2="-3.075" y2="1.8" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="1.606" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="-1.606" x2="-3.075" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.05" y1="1.606" x2="3.05" y2="1.8" width="0.2032" layer="21"/>
<wire x1="3.05" y1="-1.8" x2="3.05" y2="-1.606" width="0.2032" layer="21"/>
<smd name="1" x="-2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<smd name="2" x="2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<text x="-3.2" y="2.15" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.2" y="-3.4" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC4527">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-5.675" y1="3.4" x2="-5.675" y2="-3.375" width="0.2032" layer="51"/>
<wire x1="-5.675" y1="-3.375" x2="5.675" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="5.675" y1="-3.375" x2="5.675" y2="3.4" width="0.2032" layer="51"/>
<wire x1="5.675" y1="3.4" x2="-5.675" y2="3.4" width="0.2032" layer="21"/>
<smd name="1" x="-4.575" y="0.025" dx="3.94" dy="5.84" layer="1"/>
<smd name="2" x="4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<text x="-5.775" y="3.925" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.775" y="-5.15" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC6927">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-8.65" y1="3.375" x2="-8.65" y2="-3.375" width="0.2032" layer="51"/>
<wire x1="-8.65" y1="-3.375" x2="8.65" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="8.65" y1="-3.375" x2="8.65" y2="3.375" width="0.2032" layer="51"/>
<wire x1="8.65" y1="3.375" x2="-8.65" y2="3.375" width="0.2032" layer="21"/>
<smd name="1" x="-7.95" y="0.025" dx="3.94" dy="5.97" layer="1"/>
<smd name="2" x="7.95" y="0" dx="3.94" dy="5.97" layer="1"/>
<text x="-8.75" y="3.9" size="1.27" layer="25">&gt;NAME</text>
<text x="-8.75" y="-5.15" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="R1218">
<description>&lt;b&gt;CRCW1218 Thick Film, Rectangular Chip Resistors&lt;/b&gt;&lt;p&gt;
Source: http://www.vishay.com .. dcrcw.pdf</description>
<wire x1="-0.913" y1="-2.219" x2="0.939" y2="-2.219" width="0.1524" layer="51"/>
<wire x1="0.913" y1="2.219" x2="-0.939" y2="2.219" width="0.1524" layer="51"/>
<smd name="1" x="-1.475" y="0" dx="1.05" dy="4.9" layer="1"/>
<smd name="2" x="1.475" y="0" dx="1.05" dy="4.9" layer="1"/>
<text x="-2.54" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-2.3" x2="-0.9009" y2="2.3" layer="51"/>
<rectangle x1="0.9144" y1="-2.3" x2="1.6645" y2="2.3" layer="51"/>
</package>
<package name="1812X7R">
<description>&lt;b&gt;Chip Monolithic Ceramic Capacitors&lt;/b&gt; Medium Voltage High Capacitance for General Use&lt;p&gt;
Source: http://www.murata.com .. GRM43DR72E224KW01.pdf</description>
<wire x1="-1.1" y1="1.5" x2="1.1" y2="1.5" width="0.2032" layer="51"/>
<wire x1="1.1" y1="-1.5" x2="-1.1" y2="-1.5" width="0.2032" layer="51"/>
<wire x1="-0.6" y1="1.5" x2="0.6" y2="1.5" width="0.2032" layer="21"/>
<wire x1="0.6" y1="-1.5" x2="-0.6" y2="-1.5" width="0.2032" layer="21"/>
<smd name="1" x="-1.425" y="0" dx="0.8" dy="3.5" layer="1"/>
<smd name="2" x="1.425" y="0" dx="0.8" dy="3.5" layer="1" rot="R180"/>
<text x="-1.9456" y="1.9958" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.9456" y="-3.7738" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.4" y1="-1.6" x2="-1.1" y2="1.6" layer="51"/>
<rectangle x1="1.1" y1="-1.6" x2="1.4" y2="1.6" layer="51" rot="R180"/>
</package>
<package name="PRL1632">
<description>&lt;b&gt;PRL1632 are realized as 1W for 3.2 × 1.6mm(1206)&lt;/b&gt;&lt;p&gt;
Source: http://www.mouser.com/ds/2/392/products_18-2245.pdf</description>
<wire x1="0.7275" y1="-1.5228" x2="-0.7277" y2="-1.5228" width="0.1524" layer="51"/>
<wire x1="0.7275" y1="1.5228" x2="-0.7152" y2="1.5228" width="0.1524" layer="51"/>
<smd name="2" x="0.822" y="0" dx="1" dy="3.2" layer="1"/>
<smd name="1" x="-0.822" y="0" dx="1" dy="3.2" layer="1"/>
<text x="-1.4" y="1.8" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.4" y="-3" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8" y1="-1.6" x2="-0.4" y2="1.6" layer="51"/>
<rectangle x1="0.4" y1="-1.6" x2="0.8" y2="1.6" layer="51"/>
</package>
<package name="R01005">
<smd name="1" x="-0.1625" y="0" dx="0.2" dy="0.25" layer="1"/>
<smd name="2" x="0.1625" y="0" dx="0.2" dy="0.25" layer="1"/>
<text x="-0.4" y="0.3" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.4" y="-1.6" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2" y1="-0.1" x2="-0.075" y2="0.1" layer="51"/>
<rectangle x1="0.075" y1="-0.1" x2="0.2" y2="0.1" layer="51"/>
<rectangle x1="-0.15" y1="0.05" x2="0.15" y2="0.1" layer="51"/>
<rectangle x1="-0.15" y1="-0.1" x2="0.15" y2="-0.05" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="R-EU-1">
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="R-EU_" prefix="R" uservalue="yes">
<description>&lt;B&gt;RESISTOR&lt;/B&gt;, European symbol</description>
<gates>
<gate name="G$1" symbol="R-EU-1" x="0" y="0"/>
</gates>
<devices>
<device name="R0402" package="R0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0603" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0805" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0805W" package="R0805W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1206" package="R1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1206W" package="R1206W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1210" package="R1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1210W" package="R1210W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2010" package="R2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2010W" package="R2010W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2012" package="R2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2012W" package="R2012W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2512" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2512W" package="R2512W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3216" package="R3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3216W" package="R3216W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3225" package="R3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3225W" package="R3225W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R5025" package="R5025">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R5025W" package="R5025W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R6332" package="R6332">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R6332W" package="R6332W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M0805" package="M0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M1206" package="M1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M1406" package="M1406">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M2012" package="M2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M2309" package="M2309">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M3216" package="M3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M3516" package="M3516">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M5923" package="M5923">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204/5" package="0204/5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204/7" package="0204/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204/2V" package="0204V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/10" package="0207/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/12" package="0207/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/15" package="0207/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/2V" package="0207/2V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/5V" package="0207/5V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/7" package="0207/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0309/10" package="0309/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0309/12" package="0309/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0309/V" package="0309V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0411/12" package="0411/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0411/15" package="0411/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0411/3V" package="0411V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0414/15" package="0414/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0414/5V" package="0414V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0617/17" package="0617/17">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0617/22" package="0617/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0617/5V" package="0617V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0922/22" package="0922/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0613/5V" package="P0613V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0613/15" package="P0613/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0817/22" package="P0817/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0817/7V" package="P0817V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="V234/12" package="V234/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="V235/17" package="V235/17">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="V526-0" package="V526-0">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0102R" package="MINI_MELF-0102R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0102W" package="MINI_MELF-0102W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0204R" package="MINI_MELF-0204R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0204W" package="MINI_MELF-0204W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0207R" package="MINI_MELF-0207R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0207W" package="MINI_MELF-0207W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0922V" package="0922V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RDH/15" package="RDH/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0102AX" package="MINI_MELF-0102AX">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0201" package="R0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA52" package="VTA52">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA53" package="VTA53">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA54" package="VTA54">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA55" package="VTA55">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA56" package="VTA56">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VMTA55" package="VMTA55">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VMTB60" package="VMTB60">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R4527" package="R4527">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC0001" package="WSC0001">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC0002" package="WSC0002">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC01/2" package="WSC01/2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC2515" package="WSC2515">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC4527" package="WSC4527">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC6927" package="WSC6927">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1218" package="R1218">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1812X7R" package="1812X7R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PRL1632" package="PRL1632">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="01005" package="R01005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="connect">
<packages>
<package name="MINIFIT12">
<wire x1="-12.2" y1="-3" x2="-1.2" y2="-3" width="0.127" layer="21"/>
<wire x1="-1.2" y1="-3" x2="2.2" y2="-3" width="0.127" layer="21"/>
<wire x1="2.2" y1="-3" x2="13.2" y2="-3" width="0.127" layer="21"/>
<wire x1="-12.2" y1="6.6" x2="13.2" y2="6.6" width="0.127" layer="21"/>
<wire x1="-12.7" y1="6.1" x2="-12.7" y2="4.5" width="0.127" layer="21"/>
<wire x1="-12.7" y1="3.7" x2="-12.7" y2="-2.5" width="0.127" layer="21"/>
<wire x1="13.7" y1="6.1" x2="13.7" y2="-0.4" width="0.127" layer="21"/>
<wire x1="13.7" y1="-2.4" x2="13.7" y2="-2.5" width="0.127" layer="21"/>
<wire x1="-12.7" y1="6.1" x2="-12.2" y2="6.6" width="0.127" layer="21" curve="-79.611142"/>
<wire x1="13.2" y1="6.6" x2="13.7" y2="6.1" width="0.127" layer="21" curve="-79.611142"/>
<wire x1="-12.2" y1="-3" x2="-12.7" y2="-2.5" width="0.127" layer="21" curve="-79.611142"/>
<wire x1="13.7" y1="-2.5" x2="13.2" y2="-3" width="0.127" layer="21" curve="-79.611142"/>
<wire x1="-12.7" y1="4.5" x2="-12.7" y2="3.7" width="0.127" layer="21" curve="180"/>
<wire x1="13.7" y1="-2.4" x2="14.1" y2="-2" width="0.127" layer="21"/>
<wire x1="14.1" y1="-2" x2="14.1" y2="-0.7" width="0.127" layer="21"/>
<wire x1="14.1" y1="-0.7" x2="13.7" y2="-0.4" width="0.127" layer="21"/>
<wire x1="-1.2" y1="-3" x2="-1.2" y2="-4.4" width="0.127" layer="21"/>
<wire x1="2.2" y1="-3" x2="2.2" y2="-4.4" width="0.127" layer="21"/>
<wire x1="-1.2" y1="-4.4" x2="2.2" y2="-4.4" width="0.127" layer="21"/>
<wire x1="-8.1" y1="2" x2="-8.1" y2="6.2" width="0.127" layer="21"/>
<wire x1="-8.1" y1="6.2" x2="-11.9" y2="6.2" width="0.127" layer="21"/>
<wire x1="-11.9" y1="6.2" x2="-11.9" y2="2" width="0.127" layer="21"/>
<wire x1="-11.9" y1="2" x2="-8.1" y2="2" width="0.127" layer="21"/>
<wire x1="-3.9" y1="-2.7" x2="-3.9" y2="1.5" width="0.127" layer="21"/>
<wire x1="-3.9" y1="1.5" x2="-7.7" y2="1.5" width="0.127" layer="21"/>
<wire x1="-7.7" y1="1.5" x2="-7.7" y2="-2.7" width="0.127" layer="21"/>
<wire x1="-7.7" y1="-2.7" x2="-3.9" y2="-2.7" width="0.127" layer="21"/>
<wire x1="0.3" y1="-2.7" x2="0.3" y2="1.5" width="0.127" layer="21"/>
<wire x1="0.3" y1="1.5" x2="-3.5" y2="1.5" width="0.127" layer="21"/>
<wire x1="-3.5" y1="1.5" x2="-3.5" y2="-2.7" width="0.127" layer="21"/>
<wire x1="-3.5" y1="-2.7" x2="0.3" y2="-2.7" width="0.127" layer="21"/>
<wire x1="8.8" y1="2" x2="8.8" y2="6.2" width="0.127" layer="21"/>
<wire x1="8.8" y1="6.2" x2="5" y2="6.2" width="0.127" layer="21"/>
<wire x1="5" y1="6.2" x2="5" y2="2" width="0.127" layer="21"/>
<wire x1="5" y1="2" x2="8.8" y2="2" width="0.127" layer="21"/>
<wire x1="4.5" y1="2" x2="4.5" y2="6.2" width="0.127" layer="21"/>
<wire x1="4.5" y1="6.2" x2="0.7" y2="6.2" width="0.127" layer="21"/>
<wire x1="0.7" y1="6.2" x2="0.7" y2="2" width="0.127" layer="21"/>
<wire x1="0.7" y1="2" x2="4.5" y2="2" width="0.127" layer="21"/>
<wire x1="13" y1="-2.7" x2="13" y2="1.5" width="0.127" layer="21"/>
<wire x1="13" y1="1.5" x2="9.2" y2="1.5" width="0.127" layer="21"/>
<wire x1="9.2" y1="1.5" x2="9.2" y2="-2.7" width="0.127" layer="21"/>
<wire x1="9.2" y1="-2.7" x2="13" y2="-2.7" width="0.127" layer="21"/>
<wire x1="-3.9" y1="2" x2="-3.9" y2="5.3" width="0.127" layer="21"/>
<wire x1="-7.7" y1="5.3" x2="-7.7" y2="2" width="0.127" layer="21"/>
<wire x1="-7.7" y1="2" x2="-3.9" y2="2" width="0.127" layer="21"/>
<wire x1="-7.7" y1="5.3" x2="-6.8" y2="6.2" width="0.127" layer="21"/>
<wire x1="-3.9" y1="5.3" x2="-4.8" y2="6.2" width="0.127" layer="21"/>
<wire x1="-4.8" y1="6.2" x2="-6.8" y2="6.2" width="0.127" layer="21"/>
<wire x1="0.3" y1="2" x2="0.3" y2="5.3" width="0.127" layer="21"/>
<wire x1="-3.5" y1="5.3" x2="-3.5" y2="2" width="0.127" layer="21"/>
<wire x1="-3.5" y1="2" x2="0.3" y2="2" width="0.127" layer="21"/>
<wire x1="-3.5" y1="5.3" x2="-2.6" y2="6.2" width="0.127" layer="21"/>
<wire x1="0.3" y1="5.3" x2="-0.6" y2="6.2" width="0.127" layer="21"/>
<wire x1="-0.6" y1="6.2" x2="-2.6" y2="6.2" width="0.127" layer="21"/>
<wire x1="-8.1" y1="-2.7" x2="-8.1" y2="0.6" width="0.127" layer="21"/>
<wire x1="-11.9" y1="0.6" x2="-11.9" y2="-2.7" width="0.127" layer="21"/>
<wire x1="-11.9" y1="-2.7" x2="-8.1" y2="-2.7" width="0.127" layer="21"/>
<wire x1="-11.9" y1="0.6" x2="-11" y2="1.5" width="0.127" layer="21"/>
<wire x1="-8.1" y1="0.6" x2="-9" y2="1.5" width="0.127" layer="21"/>
<wire x1="-9" y1="1.5" x2="-11" y2="1.5" width="0.127" layer="21"/>
<wire x1="8.8" y1="-2.7" x2="8.8" y2="0.6" width="0.127" layer="21"/>
<wire x1="5" y1="0.6" x2="5" y2="-2.7" width="0.127" layer="21"/>
<wire x1="5" y1="-2.7" x2="8.8" y2="-2.7" width="0.127" layer="21"/>
<wire x1="5" y1="0.6" x2="5.9" y2="1.5" width="0.127" layer="21"/>
<wire x1="8.8" y1="0.6" x2="7.9" y2="1.5" width="0.127" layer="21"/>
<wire x1="7.9" y1="1.5" x2="5.9" y2="1.5" width="0.127" layer="21"/>
<wire x1="4.5" y1="-2.7" x2="4.5" y2="0.6" width="0.127" layer="21"/>
<wire x1="0.7" y1="0.6" x2="0.7" y2="-2.7" width="0.127" layer="21"/>
<wire x1="0.7" y1="-2.7" x2="4.5" y2="-2.7" width="0.127" layer="21"/>
<wire x1="0.7" y1="0.6" x2="1.6" y2="1.5" width="0.127" layer="21"/>
<wire x1="4.5" y1="0.6" x2="3.6" y2="1.5" width="0.127" layer="21"/>
<wire x1="3.6" y1="1.5" x2="1.6" y2="1.5" width="0.127" layer="21"/>
<wire x1="13" y1="2" x2="13" y2="5.3" width="0.127" layer="21"/>
<wire x1="9.2" y1="5.3" x2="9.2" y2="2" width="0.127" layer="21"/>
<wire x1="9.2" y1="2" x2="13" y2="2" width="0.127" layer="21"/>
<wire x1="9.2" y1="5.3" x2="10.1" y2="6.2" width="0.127" layer="21"/>
<wire x1="13" y1="5.3" x2="12.1" y2="6.2" width="0.127" layer="21"/>
<wire x1="12.1" y1="6.2" x2="10.1" y2="6.2" width="0.127" layer="21"/>
<pad name="2" x="-5.8" y="4.3" drill="1.4224" diameter="2.54"/>
<pad name="8" x="-5.8" y="-1.2" drill="1.4224" diameter="2.54"/>
<pad name="3" x="-1.6" y="4.3" drill="1.4224" diameter="2.54"/>
<pad name="9" x="-1.6" y="-1.2" drill="1.4224" diameter="2.54"/>
<pad name="1" x="-10" y="4.3" drill="1.4224" diameter="2.54"/>
<pad name="7" x="-10" y="-1.2" drill="1.4224" diameter="2.54"/>
<pad name="5" x="6.8" y="4.3" drill="1.4224" diameter="2.54"/>
<pad name="11" x="6.8" y="-1.2" drill="1.4224" diameter="2.54"/>
<pad name="6" x="11" y="4.3" drill="1.4224" diameter="2.54"/>
<pad name="12" x="11" y="-1.2" drill="1.4224" diameter="2.54"/>
<pad name="4" x="2.6" y="4.3" drill="1.4224" diameter="2.54"/>
<pad name="10" x="2.6" y="-1.2" drill="1.4224" diameter="2.54"/>
<text x="-4.305" y="7.22" size="1.778" layer="25">&gt;NAME</text>
<text x="-4.405" y="-6.59" size="1.778" layer="27">&gt;VALUE</text>
<text x="-11.811" y="2.286" size="1.27" layer="21">1</text>
<text x="11.811" y="2.286" size="1.27" layer="21">6</text>
<text x="-11.811" y="-2.54" size="1.27" layer="21">7</text>
<text x="10.922" y="-2.54" size="1.27" layer="21">12</text>
</package>
<package name="SCREW_TERMINAL_7798_KEYSTONE">
<wire x1="-6.44" y1="-3.95" x2="6.44" y2="-3.95" width="0.127" layer="21"/>
<wire x1="6.44" y1="-3.95" x2="6.44" y2="-2" width="0.127" layer="21"/>
<wire x1="6.44" y1="-2" x2="6.44" y2="-1.5" width="0.127" layer="21"/>
<wire x1="6.44" y1="-1.5" x2="6.44" y2="-0.5" width="0.127" layer="21"/>
<wire x1="6.44" y1="-0.5" x2="6.44" y2="0" width="0.127" layer="21"/>
<wire x1="6.44" y1="0" x2="6.44" y2="0.5" width="0.127" layer="21"/>
<wire x1="6.44" y1="0.5" x2="6.44" y2="1.5" width="0.127" layer="21"/>
<wire x1="6.44" y1="1.5" x2="6.44" y2="2" width="0.127" layer="21"/>
<wire x1="6.44" y1="2" x2="6.44" y2="3.95" width="0.127" layer="21"/>
<wire x1="6.44" y1="3.95" x2="-6.44" y2="3.95" width="0.127" layer="21"/>
<wire x1="-6.44" y1="3.95" x2="-6.44" y2="2" width="0.127" layer="21"/>
<wire x1="-6.44" y1="2" x2="-6.44" y2="1.5" width="0.127" layer="21"/>
<wire x1="-6.44" y1="1.5" x2="-6.44" y2="0.5" width="0.127" layer="21"/>
<wire x1="-6.44" y1="0.5" x2="-6.44" y2="0" width="0.127" layer="21"/>
<wire x1="-6.44" y1="0" x2="-6.44" y2="-0.5" width="0.127" layer="21"/>
<wire x1="-6.44" y1="-0.5" x2="-6.44" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-6.44" y1="-1.5" x2="-6.44" y2="-2" width="0.127" layer="21"/>
<wire x1="-6.44" y1="-2" x2="-6.44" y2="-3.95" width="0.127" layer="21"/>
<wire x1="-6.44" y1="2" x2="6.44" y2="2" width="0.127" layer="21"/>
<wire x1="-6.44" y1="-2" x2="6.44" y2="-2" width="0.127" layer="21"/>
<wire x1="-6.44" y1="-1.5" x2="-1.5" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-6.44" y1="1.5" x2="-1.5" y2="1.5" width="0.127" layer="21"/>
<wire x1="1.5" y1="1.5" x2="6.44" y2="1.5" width="0.127" layer="21"/>
<wire x1="1.5" y1="-1.5" x2="6.44" y2="-1.5" width="0.127" layer="21"/>
<wire x1="-6.44" y1="0.5" x2="-2" y2="0.5" width="0.127" layer="21"/>
<wire x1="-6.44" y1="0" x2="-2" y2="0" width="0.127" layer="21"/>
<wire x1="-6.44" y1="-0.5" x2="-2" y2="-0.5" width="0.127" layer="21"/>
<wire x1="2" y1="-0.5" x2="6.44" y2="-0.5" width="0.127" layer="21"/>
<wire x1="2" y1="0" x2="6.44" y2="0" width="0.127" layer="21"/>
<wire x1="2" y1="0.5" x2="6.44" y2="0.5" width="0.127" layer="21"/>
<circle x="0" y="0" radius="2" width="0.127" layer="21"/>
<pad name="P$1" x="0" y="3.5" drill="2"/>
<pad name="P$2" x="0" y="-3.5" drill="2"/>
<pad name="P$3" x="-5" y="3.5" drill="2"/>
<pad name="P$4" x="-5" y="-3.5" drill="2"/>
<pad name="P$5" x="5" y="-3.5" drill="2"/>
<pad name="P$6" x="5" y="3.5" drill="2"/>
<rectangle x1="-5.5" y1="-4.5" x2="-4.5" y2="-4" layer="21"/>
<rectangle x1="-0.5" y1="-4.5" x2="0.5" y2="-4" layer="21"/>
<rectangle x1="4.5" y1="-4.5" x2="5.5" y2="-4" layer="21"/>
<rectangle x1="-5.5" y1="4" x2="-4.5" y2="4.5" layer="21"/>
<rectangle x1="-0.5" y1="4" x2="0.5" y2="4.5" layer="21"/>
<rectangle x1="4.5" y1="4" x2="5.5" y2="4.5" layer="21"/>
</package>
<package name="4P">
<wire x1="-3.81" y1="-3.81" x2="-3.81" y2="-2.54" width="0.127" layer="22"/>
<wire x1="-3.81" y1="-2.54" x2="-3.81" y2="2.54" width="0.127" layer="22"/>
<wire x1="-3.81" y1="2.54" x2="-3.81" y2="3.81" width="0.127" layer="22"/>
<wire x1="-3.81" y1="-3.81" x2="-0.635" y2="-3.81" width="0.127" layer="22"/>
<wire x1="0.635" y1="-3.81" x2="2.54" y2="-3.81" width="0.127" layer="22"/>
<wire x1="5.715" y1="-3.81" x2="13.335" y2="-3.81" width="0.127" layer="22"/>
<wire x1="16.51" y1="-3.81" x2="22.86" y2="-3.81" width="0.127" layer="22"/>
<wire x1="22.86" y1="-3.81" x2="22.86" y2="3.81" width="0.127" layer="22"/>
<wire x1="22.86" y1="3.81" x2="16.51" y2="3.81" width="0.127" layer="22"/>
<wire x1="13.335" y1="3.81" x2="5.715" y2="3.81" width="0.127" layer="22"/>
<wire x1="2.54" y1="3.81" x2="-3.81" y2="3.81" width="0.127" layer="22"/>
<wire x1="2.54" y1="3.81" x2="2.54" y2="7.62" width="0.127" layer="22"/>
<wire x1="2.54" y1="7.62" x2="16.51" y2="7.62" width="0.127" layer="22"/>
<wire x1="16.51" y1="7.62" x2="16.51" y2="3.81" width="0.127" layer="22"/>
<wire x1="2.54" y1="-3.81" x2="2.54" y2="-7.62" width="0.127" layer="22"/>
<wire x1="2.54" y1="-7.62" x2="16.51" y2="-7.62" width="0.127" layer="22"/>
<wire x1="16.51" y1="-7.62" x2="16.51" y2="-3.81" width="0.127" layer="22"/>
<wire x1="5.715" y1="3.81" x2="5.715" y2="6.35" width="0.127" layer="22"/>
<wire x1="5.715" y1="6.35" x2="13.335" y2="6.35" width="0.127" layer="22"/>
<wire x1="13.335" y1="6.35" x2="13.335" y2="3.81" width="0.127" layer="22"/>
<wire x1="5.715" y1="-3.81" x2="5.715" y2="-6.35" width="0.127" layer="22"/>
<wire x1="5.715" y1="-6.35" x2="13.335" y2="-6.35" width="0.127" layer="22"/>
<wire x1="13.335" y1="-6.35" x2="13.335" y2="-3.81" width="0.127" layer="22"/>
<wire x1="-8.255" y1="3.81" x2="-8.255" y2="-3.81" width="0.127" layer="22"/>
<wire x1="-8.255" y1="-3.81" x2="-6.985" y2="-3.81" width="0.127" layer="22"/>
<wire x1="-6.985" y1="-3.81" x2="-6.985" y2="-2.54" width="0.127" layer="22"/>
<wire x1="-6.985" y1="-2.54" x2="-6.985" y2="2.54" width="0.127" layer="22"/>
<wire x1="-6.985" y1="2.54" x2="-6.985" y2="3.81" width="0.127" layer="22"/>
<wire x1="-6.985" y1="3.81" x2="-8.255" y2="3.81" width="0.127" layer="22"/>
<wire x1="-3.81" y1="3.81" x2="-6.985" y2="3.81" width="0.127" layer="22"/>
<wire x1="-3.81" y1="2.54" x2="-6.985" y2="2.54" width="0.127" layer="22"/>
<wire x1="-3.81" y1="-2.54" x2="-6.985" y2="-2.54" width="0.127" layer="22"/>
<wire x1="-6.985" y1="-3.81" x2="-3.81" y2="-3.81" width="0.127" layer="22"/>
<wire x1="27.305" y1="-3.81" x2="27.305" y2="3.81" width="0.127" layer="22"/>
<wire x1="27.305" y1="3.81" x2="26.035" y2="3.81" width="0.127" layer="22"/>
<wire x1="26.035" y1="3.81" x2="26.035" y2="2.54" width="0.127" layer="22"/>
<wire x1="26.035" y1="2.54" x2="26.035" y2="-2.54" width="0.127" layer="22"/>
<wire x1="26.035" y1="-2.54" x2="26.035" y2="-3.81" width="0.127" layer="22"/>
<wire x1="26.035" y1="-3.81" x2="27.305" y2="-3.81" width="0.127" layer="22"/>
<wire x1="22.86" y1="-3.81" x2="26.035" y2="-3.81" width="0.127" layer="22"/>
<wire x1="22.86" y1="-2.54" x2="26.035" y2="-2.54" width="0.127" layer="22"/>
<wire x1="22.86" y1="2.54" x2="26.035" y2="2.54" width="0.127" layer="22"/>
<wire x1="26.035" y1="3.81" x2="22.86" y2="3.81" width="0.127" layer="22"/>
<wire x1="-2.286" y1="0" x2="2.286" y2="0" width="0.127" layer="22" curve="-180"/>
<wire x1="16.764" y1="0" x2="21.336" y2="0" width="0.127" layer="22" curve="-180"/>
<wire x1="-2.286" y1="0" x2="-2.286" y2="-3.302" width="0.127" layer="22"/>
<wire x1="-2.286" y1="-3.302" x2="2.286" y2="-3.302" width="0.127" layer="22"/>
<wire x1="2.286" y1="-3.302" x2="2.286" y2="0" width="0.127" layer="22"/>
<wire x1="16.764" y1="0" x2="16.764" y2="-3.302" width="0.127" layer="22"/>
<wire x1="16.764" y1="-3.302" x2="21.336" y2="-3.302" width="0.127" layer="22"/>
<wire x1="21.336" y1="-3.302" x2="21.336" y2="0" width="0.127" layer="22"/>
<wire x1="-0.635" y1="-3.81" x2="-0.635" y2="-5.715" width="0.127" layer="22"/>
<wire x1="-0.635" y1="-5.715" x2="0.635" y2="-5.715" width="0.127" layer="22"/>
<wire x1="0.635" y1="-5.715" x2="0.635" y2="-3.81" width="0.127" layer="22"/>
<circle x="6.35" y="0" radius="2.1552" width="0.127" layer="22"/>
<circle x="12.7" y="0" radius="2.1701" width="0.127" layer="22"/>
<pad name="1" x="0" y="0" drill="1.9304" diameter="3.81"/>
<pad name="2" x="6.35" y="0" drill="1.9304" diameter="3.81"/>
<pad name="4" x="19.05" y="0" drill="1.9304" diameter="3.81"/>
<pad name="3" x="12.7" y="0" drill="1.9304" diameter="3.81"/>
<text x="0.508" y="3.556" size="1.27" layer="22" rot="R180">1</text>
<text x="7.112" y="3.556" size="1.27" layer="22" rot="R180">2</text>
<text x="13.462" y="3.556" size="1.27" layer="22" rot="R180">3</text>
<text x="19.812" y="3.556" size="1.27" layer="22" rot="R180">4</text>
<text x="-7.112" y="7.366" size="1.778" layer="25" ratio="10">&gt;NAME</text>
<text x="-7.62" y="5.08" size="1.778" layer="27" ratio="10">&gt;VALUE</text>
</package>
</packages>
<symbols>
<symbol name="12P">
<wire x1="3.81" y1="-15.24" x2="-2.54" y2="-15.24" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="-15.24" x2="-2.54" y2="17.78" width="0.4064" layer="94"/>
<wire x1="-2.54" y1="17.78" x2="3.81" y2="17.78" width="0.4064" layer="94"/>
<wire x1="3.81" y1="-15.24" x2="3.81" y2="17.78" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="2.54" y2="-2.54" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-5.08" x2="2.54" y2="-5.08" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-7.62" x2="2.54" y2="-7.62" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-10.16" x2="2.54" y2="-10.16" width="0.6096" layer="94"/>
<wire x1="1.27" y1="-12.7" x2="2.54" y2="-12.7" width="0.6096" layer="94"/>
<wire x1="2.54" y1="0" x2="1.27" y2="0" width="0.6096" layer="94"/>
<wire x1="2.54" y1="2.54" x2="1.27" y2="2.54" width="0.6096" layer="94"/>
<wire x1="2.54" y1="5.08" x2="1.27" y2="5.08" width="0.6096" layer="94"/>
<wire x1="2.54" y1="7.62" x2="1.27" y2="7.62" width="0.6096" layer="94"/>
<wire x1="2.54" y1="10.16" x2="1.27" y2="10.16" width="0.6096" layer="94"/>
<wire x1="2.54" y1="12.7" x2="1.27" y2="12.7" width="0.6096" layer="94"/>
<wire x1="2.54" y1="15.24" x2="1.27" y2="15.24" width="0.6096" layer="94"/>
<text x="-3.81" y="-17.78" size="1.778" layer="96">&gt;VALUE</text>
<text x="-3.81" y="18.542" size="1.778" layer="95">&gt;NAME</text>
<text x="-0.762" y="14.478" size="1.4224" layer="94">1</text>
<text x="-0.762" y="11.938" size="1.4224" layer="94">2</text>
<text x="-0.762" y="9.398" size="1.4224" layer="94">3</text>
<text x="-0.762" y="6.858" size="1.4224" layer="94">4</text>
<text x="-0.508" y="4.318" size="1.4224" layer="94">5</text>
<text x="-0.508" y="1.778" size="1.4224" layer="94">6</text>
<text x="-0.508" y="-0.762" size="1.4224" layer="94">7</text>
<text x="-0.508" y="-3.302" size="1.4224" layer="94">8</text>
<text x="-0.508" y="-5.842" size="1.4224" layer="94">9</text>
<text x="-1.524" y="-8.382" size="1.4224" layer="94">10</text>
<text x="-1.27" y="-10.922" size="1.4224" layer="94">11</text>
<text x="-1.524" y="-13.462" size="1.4224" layer="94">12</text>
<pin name="12" x="7.62" y="-12.7" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="3" x="7.62" y="10.16" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="11" x="7.62" y="-10.16" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="4" x="7.62" y="7.62" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="10" x="7.62" y="-7.62" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="5" x="7.62" y="5.08" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="9" x="7.62" y="-5.08" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="6" x="7.62" y="2.54" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="8" x="7.62" y="-2.54" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="7" x="7.62" y="0" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="2" x="7.62" y="12.7" visible="pad" length="middle" direction="pas" rot="R180"/>
<pin name="1" x="7.62" y="15.24" visible="pad" length="middle" direction="pas" rot="R180"/>
</symbol>
<symbol name="SCREW_TERMINAL">
<wire x1="-3.429" y1="3.556" x2="-3.429" y2="-1.016" width="0.254" layer="94"/>
<wire x1="-3.429" y1="-1.016" x2="3.429" y2="-1.016" width="0.254" layer="94"/>
<wire x1="3.429" y1="-1.016" x2="3.429" y2="3.556" width="0.254" layer="94"/>
<wire x1="3.429" y1="3.556" x2="-3.429" y2="3.556" width="0.254" layer="94"/>
<circle x="0" y="1.27" radius="1.27" width="0.254" layer="94"/>
<text x="5.715" y="-0.635" size="1.778" layer="96">&gt;VALUE</text>
<text x="6.35" y="2.032" size="1.778" layer="95">&gt;NAME</text>
<text x="-1.524" y="0" size="1.27" layer="94" rot="R90">M4</text>
<pin name="P$1" x="-2.54" y="-2.54" visible="off" length="short" rot="R90"/>
<pin name="P$2" x="0" y="-2.54" visible="off" length="short" rot="R90"/>
<pin name="P$3" x="2.54" y="-2.54" visible="off" length="short" rot="R90"/>
<pin name="P$4" x="-2.54" y="5.08" visible="off" length="short" rot="R270"/>
<pin name="P$5" x="0" y="5.08" visible="off" length="short" rot="R270"/>
<pin name="P$6" x="2.54" y="5.08" visible="off" length="short" rot="R270"/>
</symbol>
<symbol name="4P">
<wire x1="-22.225" y1="2.54" x2="-22.225" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-22.225" y1="-1.27" x2="-3.175" y2="-1.27" width="0.254" layer="94"/>
<wire x1="-3.175" y1="-1.27" x2="-3.175" y2="2.54" width="0.254" layer="94"/>
<wire x1="-3.175" y1="2.54" x2="-22.225" y2="2.54" width="0.254" layer="94"/>
<wire x1="-21.59" y1="0.635" x2="-19.05" y2="0.635" width="0.254" layer="94" curve="-180"/>
<wire x1="-6.35" y1="0.635" x2="-3.81" y2="0.635" width="0.254" layer="94" curve="-180"/>
<wire x1="-21.59" y1="0.635" x2="-21.59" y2="-0.635" width="0.254" layer="94"/>
<wire x1="-21.59" y1="-0.635" x2="-19.05" y2="-0.635" width="0.254" layer="94"/>
<wire x1="-19.05" y1="-0.635" x2="-19.05" y2="0.635" width="0.254" layer="94"/>
<wire x1="-6.35" y1="0.635" x2="-6.35" y2="-0.635" width="0.254" layer="94"/>
<wire x1="-6.35" y1="-0.635" x2="-3.81" y2="-0.635" width="0.254" layer="94"/>
<wire x1="-3.81" y1="-0.635" x2="-3.81" y2="0.635" width="0.254" layer="94"/>
<circle x="-15.24" y="0.635" radius="1.4199" width="0.254" layer="94"/>
<circle x="-10.16" y="0.635" radius="1.4199" width="0.254" layer="94"/>
<text x="-8.255" y="-4.445" size="1.778" layer="96" rot="R180">&gt;VALUE</text>
<text x="-8.89" y="-2.032" size="1.778" layer="95" rot="R180">&gt;NAME</text>
<text x="-20.574" y="0" size="1.27" layer="94">1</text>
<text x="-15.875" y="0" size="1.27" layer="94">2</text>
<text x="-10.795" y="0" size="1.27" layer="94">3</text>
<text x="-5.715" y="0" size="1.27" layer="94">4</text>
<pin name="1" x="-20.32" y="7.62" visible="off" length="middle" rot="R270"/>
<pin name="2" x="-15.24" y="7.62" visible="off" length="middle" rot="R270"/>
<pin name="3" x="-10.16" y="7.62" visible="off" length="middle" rot="R270"/>
<pin name="4" x="-5.08" y="7.62" visible="off" length="middle" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MINIFIT12" prefix="JP" uservalue="yes">
<gates>
<gate name="G$1" symbol="12P" x="-10.16" y="0"/>
</gates>
<devices>
<device name="" package="MINIFIT12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="10" pad="10"/>
<connect gate="G$1" pin="11" pad="11"/>
<connect gate="G$1" pin="12" pad="12"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
<connect gate="G$1" pin="5" pad="5"/>
<connect gate="G$1" pin="6" pad="6"/>
<connect gate="G$1" pin="7" pad="7"/>
<connect gate="G$1" pin="8" pad="8"/>
<connect gate="G$1" pin="9" pad="9"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SCREW_TERMINAL" prefix="X">
<description>Keystone electronic 7798 M4 sroub, 30A zatizeni</description>
<gates>
<gate name="G$1" symbol="SCREW_TERMINAL" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SCREW_TERMINAL_7798_KEYSTONE">
<connects>
<connect gate="G$1" pin="P$1" pad="P$1"/>
<connect gate="G$1" pin="P$2" pad="P$2"/>
<connect gate="G$1" pin="P$3" pad="P$3"/>
<connect gate="G$1" pin="P$4" pad="P$4"/>
<connect gate="G$1" pin="P$5" pad="P$5"/>
<connect gate="G$1" pin="P$6" pad="P$6"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="4P" prefix="JP" uservalue="yes">
<gates>
<gate name="G$1" symbol="4P" x="0" y="0"/>
</gates>
<devices>
<device name="" package="4P">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
<connect gate="G$1" pin="3" pad="3"/>
<connect gate="G$1" pin="4" pad="4"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="FUSE">
<packages>
<package name="AUTOPOJ">
<wire x1="-3.81" y1="-1.905" x2="-3.81" y2="4.445" width="0.127" layer="22"/>
<wire x1="-3.81" y1="4.445" x2="16.51" y2="4.445" width="0.127" layer="22"/>
<wire x1="16.51" y1="4.445" x2="16.51" y2="-1.905" width="0.127" layer="22"/>
<wire x1="16.51" y1="-1.905" x2="-3.81" y2="-1.905" width="0.127" layer="22"/>
<wire x1="-2.286" y1="4.318" x2="-2.286" y2="-1.778" width="0.127" layer="22"/>
<wire x1="14.732" y1="4.318" x2="14.732" y2="-1.778" width="0.127" layer="22"/>
<wire x1="-1.778" y1="4.064" x2="-1.778" y2="-1.524" width="0.127" layer="22"/>
<wire x1="-1.778" y1="-1.524" x2="4.572" y2="-1.524" width="0.127" layer="22"/>
<wire x1="4.572" y1="-1.524" x2="4.572" y2="4.064" width="0.127" layer="22"/>
<wire x1="4.572" y1="4.064" x2="-1.778" y2="4.064" width="0.127" layer="22"/>
<wire x1="8.128" y1="4.064" x2="8.128" y2="-1.524" width="0.127" layer="22"/>
<wire x1="8.128" y1="-1.524" x2="14.478" y2="-1.524" width="0.127" layer="22"/>
<wire x1="14.478" y1="-1.524" x2="14.478" y2="4.064" width="0.127" layer="22"/>
<wire x1="14.478" y1="4.064" x2="8.128" y2="4.064" width="0.127" layer="22"/>
<circle x="6.35" y="1.27" radius="0.4016" width="0.127" layer="18"/>
<pad name="2C" x="12.7" y="2.54" drill="1.4224" diameter="3.81" shape="square"/>
<pad name="2D" x="12.7" y="0" drill="1.4224" diameter="3.81" shape="square"/>
<pad name="1A" x="0" y="2.54" drill="1.4224" diameter="3.81" shape="square"/>
<pad name="1B" x="0" y="0" drill="1.4224" diameter="3.81" shape="square"/>
<pad name="1C" x="3.81" y="2.54" drill="1.4224" diameter="1.905" shape="long" rot="R90"/>
<pad name="1D" x="3.81" y="0" drill="1.4224" diameter="1.905" shape="long" rot="R90"/>
<pad name="2A" x="8.89" y="2.54" drill="1.4224" diameter="1.905" shape="long" rot="R90"/>
<pad name="2B" x="8.89" y="0" drill="1.4224" diameter="1.905" shape="long" rot="R90"/>
<text x="10.795" y="6.477" size="1.778" layer="26" ratio="10" rot="R180">&gt;NAME</text>
<text x="11.811" y="-2.413" size="1.778" layer="28" ratio="10" rot="R180">&gt;VALUE</text>
<hole x="6.35" y="1.27" drill="2.54"/>
<polygon width="0.127" layer="17">
<vertex x="1.778" y="4.318"/>
<vertex x="1.778" y="-1.778"/>
<vertex x="3.81" y="-1.778"/>
<vertex x="3.048" y="-1.016"/>
<vertex x="3.048" y="3.81"/>
<vertex x="3.556" y="4.318"/>
</polygon>
<polygon width="0.127" layer="17">
<vertex x="10.922" y="4.318"/>
<vertex x="9.144" y="4.318"/>
<vertex x="9.144" y="4.064"/>
<vertex x="9.652" y="3.556"/>
<vertex x="9.652" y="-1.27"/>
<vertex x="9.144" y="-1.778"/>
<vertex x="10.922" y="-1.778"/>
</polygon>
</package>
</packages>
<symbols>
<symbol name="FUSE4">
<wire x1="-3.81" y1="-0.762" x2="3.81" y2="-0.762" width="0.254" layer="94"/>
<wire x1="3.81" y1="0.762" x2="-3.81" y2="0.762" width="0.254" layer="94"/>
<wire x1="3.81" y1="-0.762" x2="3.81" y2="0" width="0.254" layer="94"/>
<wire x1="5.08" y1="0" x2="3.81" y2="0" width="0.1524" layer="94"/>
<wire x1="3.81" y1="0" x2="3.81" y2="0.762" width="0.254" layer="94"/>
<wire x1="-5.08" y1="0" x2="-3.81" y2="0" width="0.1524" layer="94"/>
<wire x1="-3.81" y1="0" x2="-3.81" y2="-0.762" width="0.254" layer="94"/>
<wire x1="-3.81" y1="0" x2="3.81" y2="0" width="0.1524" layer="94"/>
<wire x1="-3.81" y1="0.762" x2="-3.81" y2="0" width="0.254" layer="94"/>
<text x="-3.81" y="1.397" size="1.524" layer="95">&gt;NAME</text>
<text x="-3.81" y="-2.921" size="1.524" layer="96">&gt;VALUE</text>
<pin name="1A" x="-7.62" y="0" visible="off" length="short"/>
<pin name="1B" x="-7.62" y="0" visible="off" length="short"/>
<pin name="1C" x="-7.62" y="0" visible="off" length="short"/>
<pin name="1D" x="-7.62" y="0" visible="off" length="short"/>
<pin name="2A" x="7.62" y="0" visible="off" length="short" rot="R180"/>
<pin name="2B" x="7.62" y="0" visible="off" length="short" rot="R180"/>
<pin name="2C" x="7.62" y="0" visible="off" length="short" rot="R180"/>
<pin name="2D" x="7.62" y="0" visible="off" length="short" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="AUTOPOJ" prefix="POJ" uservalue="yes">
<gates>
<gate name="G$1" symbol="FUSE4" x="-2.54" y="-2.54"/>
</gates>
<devices>
<device name="" package="AUTOPOJ">
<connects>
<connect gate="G$1" pin="1A" pad="1A"/>
<connect gate="G$1" pin="1B" pad="1B"/>
<connect gate="G$1" pin="1C" pad="1C"/>
<connect gate="G$1" pin="1D" pad="1D"/>
<connect gate="G$1" pin="2A" pad="2A"/>
<connect gate="G$1" pin="2B" pad="2B"/>
<connect gate="G$1" pin="2C" pad="2C"/>
<connect gate="G$1" pin="2D" pad="2D"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U$2" library="FRAMES" deviceset="TAB" device="" value="TAB"/>
<part name="U$4" library="SUPPLY2" deviceset="PE" device=""/>
<part name="V5" library="SUPPLY2" deviceset="V--&gt;" device="" value="EL_MOTOR"/>
<part name="V9" library="SUPPLY2" deviceset="V--&gt;" device="" value="CON1"/>
<part name="V7" library="SUPPLY2" deviceset="V--&gt;" device="" value="BACK UP"/>
<part name="D16" library="SMDA" deviceset="DIOD-MELF" device="" value="30BQ60"/>
<part name="D20" library="SMDA" deviceset="DIOD-MELF" device="" value="30BQ60"/>
<part name="S105" library="SKLADKA2" deviceset="SPINAC1" device="" value="ODPOJOVAC"/>
<part name="D23" library="SMDA" deviceset="DIOD-MELF" device="" value="30BQ60"/>
<part name="D24" library="SMDA" deviceset="DIOD-MELF" device="" value="30BQ60"/>
<part name="S106" library="SKLADKA2" deviceset="SPINAC1" device="" value="ODPOJOVAC"/>
<part name="RE4" library="RELAIS" deviceset="AUTORELE" device="" value="AUTORELE"/>
<part name="S7" library="SKLADKA2" deviceset="SPINAC1" device="" value="ODPOJOVAC"/>
<part name="S8" library="SKLADKA2" deviceset="SPINAC1" device="" value="ODPOJOVAC"/>
<part name="U$47" library="SUPPLY2" deviceset="GND" device=""/>
<part name="RE1200" library="RELAIS" deviceset="AUTORELE" device="" value="AUTORELE"/>
<part name="RE1" library="RELAIS" deviceset="AUTORELE" device="" value="AUTORELE"/>
<part name="U$73" library="untitled" deviceset="POKUS" device=""/>
<part name="GND2" library="SUPPLY2" deviceset="GND" device=""/>
<part name="GND1" library="SUPPLY2" deviceset="GND" device=""/>
<part name="U$1" library="SUPPLY2" deviceset="PE" device=""/>
<part name="U$70" library="SUPPLY2" deviceset="PE" device=""/>
<part name="GND14" library="supply1" deviceset="GNDA" device=""/>
<part name="RTERM2" library="resistor" deviceset="R-EU_" device="0207/10" value="120R/0,5W"/>
<part name="RTERM3" library="resistor" deviceset="R-EU_" device="0207/10" value="120R/0,5W"/>
<part name="V23" library="SUPPLY2" deviceset="V--&gt;" device="" value="X23.2"/>
<part name="V76" library="SUPPLY2" deviceset="V--&gt;" device="" value="X23.1"/>
<part name="V90" library="SUPPLY2" deviceset="V--&gt;" device="" value="FUEL PUMP"/>
<part name="V91" library="SUPPLY2" deviceset="V--&gt;" device="" value="ALT D+"/>
<part name="GND3" library="supply1" deviceset="GNDA" device=""/>
<part name="GND4" library="supply1" deviceset="GNDA" device=""/>
<part name="JP_3" library="connect" deviceset="MINIFIT12" device="" value="PH"/>
<part name="P_1" library="connect" deviceset="SCREW_TERMINAL" device="" value="+AKU"/>
<part name="JP_2" library="connect" deviceset="MINIFIT12" device="" value="PH"/>
<part name="JP_1" library="connect" deviceset="MINIFIT12" device="" value="PH"/>
<part name="P_2" library="connect" deviceset="SCREW_TERMINAL" device="" value="PREHEAT_A"/>
<part name="P_3" library="connect" deviceset="SCREW_TERMINAL" device="" value="PREHEAT_B"/>
<part name="L_1" library="connect" deviceset="SCREW_TERMINAL" device="" value="ECU_POWER"/>
<part name="X_2" library="connect" deviceset="SCREW_TERMINAL" device="" value="GLOW_1"/>
<part name="X_4" library="connect" deviceset="SCREW_TERMINAL" device="" value="GLOW_2"/>
<part name="JP_4" library="connect" deviceset="4P" device=""/>
<part name="GND8" library="supply1" deviceset="GND" device=""/>
<part name="U$30" library="SUPPLY2" deviceset="AKU" device=""/>
<part name="U$28" library="SUPPLY2" deviceset="+24V@2" device="" value="+24V"/>
<part name="U$33" library="SUPPLY2" deviceset="+24V@2" device="" value="+24V"/>
<part name="V38" library="SUPPLY2" deviceset="V--&gt;" device="" value="GLOW_1"/>
<part name="V28" library="SUPPLY2" deviceset="V--&gt;" device="" value="GLOW_2"/>
<part name="FA2" library="SKLADKA2" deviceset="JIS_VYP" device="" value="30A"/>
<part name="U$34" library="SUPPLY2" deviceset="AKU" device=""/>
<part name="GND5" library="supply1" deviceset="GND" device=""/>
<part name="GND6" library="supply1" deviceset="GNDA" device=""/>
<part name="V10" library="SUPPLY2" deviceset="V--&gt;" device="" value="WATER IN FUEL"/>
<part name="V25" library="SUPPLY2" deviceset="V--&gt;" device="" value="AIR FILTER"/>
<part name="V26" library="SUPPLY2" deviceset="V--&gt;" device="" value="COOLANT LEVEL"/>
<part name="V1" library="SUPPLY2" deviceset="V--&gt;" device="" value="GENERATOR"/>
<part name="FA15" library="FUSE" deviceset="AUTOPOJ" device="" value="5A"/>
<part name="U$81" library="SUPPLY2" deviceset="AKU" device=""/>
<part name="S9" library="SKLADKA2" deviceset="SPINAC1" device="" value="ODPOJOVAC"/>
<part name="S10" library="SKLADKA2" deviceset="SPINAC1" device="" value="ODPOJOVAC"/>
<part name="Z3" library="SKLADKA2" deviceset="ZAROVKA" device="" value="asd"/>
<part name="U$60" library="SUPPLY2" deviceset="GND" device=""/>
<part name="GND7" library="supply1" deviceset="GNDA" device=""/>
<part name="GND10" library="SUPPLY2" deviceset="GND" device=""/>
<part name="RE2" library="RELAIS" deviceset="AUTORELE" device="" value="AUTORELE"/>
<part name="RE3" library="RELAIS" deviceset="AUTORELE" device="" value="AUTORELE"/>
<part name="GND9" library="SUPPLY2" deviceset="GND" device=""/>
<part name="GND11" library="SUPPLY2" deviceset="GND" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<wire x1="127" y1="259.715" x2="-321.945" y2="259.715" width="0.1524" layer="94"/>
<wire x1="-321.945" y1="259.715" x2="-321.945" y2="605.79" width="0.1524" layer="94"/>
<wire x1="-321.945" y1="605.79" x2="228.6" y2="605.79" width="0.1524" layer="94"/>
<wire x1="228.6" y1="605.79" x2="228.6" y2="295.275" width="0.1524" layer="94"/>
<wire x1="-198.4375" y1="522.9225" x2="-197.485" y2="522.9225" width="0.3048" layer="94"/>
<wire x1="-188.2775" y1="524.1925" x2="-189.23" y2="524.1925" width="0.3048" layer="94"/>
<wire x1="-171.7675" y1="522.9225" x2="-172.72" y2="522.9225" width="0.3048" layer="94"/>
<wire x1="-181.9275" y1="524.1925" x2="-180.975" y2="524.1925" width="0.3048" layer="94"/>
<wire x1="-206.6925" y1="537.5275" x2="-196.5325" y2="537.5275" width="0.4064" layer="94"/>
<wire x1="-196.5325" y1="537.5275" x2="-185.1025" y2="537.5275" width="0.4064" layer="94"/>
<wire x1="-185.1025" y1="537.5275" x2="-173.6725" y2="537.5275" width="0.4064" layer="94"/>
<wire x1="-173.6725" y1="537.5275" x2="-152.0825" y2="537.5275" width="0.4064" layer="94"/>
<wire x1="-152.0825" y1="537.5275" x2="-144.4625" y2="537.5275" width="0.4064" layer="94"/>
<wire x1="-144.4625" y1="537.5275" x2="-131.7625" y2="537.5275" width="0.4064" layer="94"/>
<wire x1="-131.7625" y1="537.5275" x2="-111.4425" y2="537.5275" width="0.4064" layer="94"/>
<wire x1="-111.4425" y1="537.5275" x2="-98.7425" y2="537.5275" width="0.4064" layer="94"/>
<wire x1="-98.7425" y1="537.5275" x2="-98.7425" y2="494.3475" width="0.4064" layer="94"/>
<wire x1="-98.7425" y1="494.3475" x2="-105.0925" y2="494.3475" width="0.4064" layer="94"/>
<wire x1="-105.0925" y1="494.3475" x2="-117.7925" y2="494.3475" width="0.4064" layer="94"/>
<wire x1="-117.7925" y1="494.3475" x2="-124.1425" y2="494.3475" width="0.4064" layer="94"/>
<wire x1="-124.1425" y1="494.3475" x2="-131.7625" y2="494.3475" width="0.4064" layer="94"/>
<wire x1="-131.7625" y1="494.3475" x2="-152.0825" y2="494.3475" width="0.4064" layer="94"/>
<wire x1="-152.0825" y1="494.3475" x2="-173.6725" y2="494.3475" width="0.4064" layer="94"/>
<wire x1="-173.6725" y1="494.3475" x2="-185.1025" y2="494.3475" width="0.4064" layer="94"/>
<wire x1="-185.1025" y1="494.3475" x2="-196.5325" y2="494.3475" width="0.4064" layer="94"/>
<wire x1="-196.5325" y1="494.3475" x2="-206.6925" y2="494.3475" width="0.4064" layer="94"/>
<wire x1="-157.1625" y1="528.6375" x2="-157.1625" y2="504.5075" width="0.1524" layer="94"/>
<wire x1="-157.1625" y1="504.5075" x2="-139.3825" y2="504.5075" width="0.1524" layer="94"/>
<wire x1="-139.3825" y1="504.5075" x2="-139.3825" y2="528.6375" width="0.1524" layer="94"/>
<wire x1="-139.3825" y1="528.6375" x2="-157.1625" y2="528.6375" width="0.1524" layer="94"/>
<wire x1="-196.5325" y1="537.5275" x2="-196.5325" y2="540.0675" width="0.3048" layer="94"/>
<wire x1="-185.1025" y1="537.5275" x2="-185.1025" y2="540.0675" width="0.3048" layer="94"/>
<wire x1="-173.6725" y1="537.5275" x2="-173.6725" y2="540.0675" width="0.3048" layer="94"/>
<wire x1="-196.5325" y1="494.3475" x2="-196.5325" y2="491.8075" width="0.3048" layer="94"/>
<wire x1="-185.1025" y1="494.3475" x2="-185.1025" y2="491.8075" width="0.3048" layer="94"/>
<wire x1="-173.6725" y1="494.3475" x2="-173.6725" y2="491.8075" width="0.3048" layer="94"/>
<wire x1="-152.0825" y1="494.3475" x2="-152.0825" y2="491.8075" width="0.3048" layer="94"/>
<wire x1="-152.0825" y1="537.5275" x2="-152.0825" y2="540.0675" width="0.3048" layer="94"/>
<wire x1="-206.6925" y1="494.3475" x2="-206.6925" y2="537.5275" width="0.4064" layer="94"/>
<wire x1="-131.7625" y1="537.5275" x2="-131.7625" y2="540.0675" width="0.3048" layer="94"/>
<wire x1="-131.7625" y1="494.3475" x2="-131.7625" y2="491.8075" width="0.3048" layer="94"/>
<wire x1="-124.1425" y1="494.3475" x2="-124.1425" y2="491.8075" width="0.3048" layer="94"/>
<wire x1="-117.7925" y1="494.3475" x2="-117.7925" y2="491.8075" width="0.3048" layer="94"/>
<wire x1="-105.0925" y1="494.3475" x2="-105.0925" y2="491.8075" width="0.3048" layer="94"/>
<wire x1="-111.4425" y1="537.5275" x2="-111.4425" y2="540.0675" width="0.3048" layer="94"/>
<wire x1="-144.4625" y1="537.5275" x2="-144.4625" y2="540.0675" width="0.3048" layer="94"/>
<wire x1="-264.4775" y1="553.7835" x2="-258.1275" y2="553.7835" width="0.3048" layer="94"/>
<wire x1="-264.4775" y1="558.8635" x2="-258.1275" y2="558.8635" width="0.3048" layer="94"/>
<wire x1="-264.4775" y1="563.9435" x2="-258.1275" y2="563.9435" width="0.3048" layer="94"/>
<wire x1="-264.4775" y1="569.0235" x2="-258.1275" y2="569.0235" width="0.3048" layer="94"/>
<wire x1="-260.6675" y1="574.1035" x2="-258.1275" y2="574.1035" width="0.3048" layer="94"/>
<wire x1="-253.0475" y1="553.7835" x2="-258.445" y2="551.561" width="0.3048" layer="94"/>
<wire x1="-253.0475" y1="558.8635" x2="-258.445" y2="556.641" width="0.3048" layer="94"/>
<wire x1="-253.0475" y1="563.9435" x2="-258.445" y2="561.721" width="0.3048" layer="94"/>
<wire x1="-253.0475" y1="569.0235" x2="-258.445" y2="566.801" width="0.3048" layer="94"/>
<wire x1="-253.0475" y1="574.1035" x2="-258.445" y2="571.881" width="0.3048" layer="94"/>
<wire x1="-253.0475" y1="553.7835" x2="-237.8075" y2="553.7835" width="0.3048" layer="94"/>
<wire x1="-253.0475" y1="558.8635" x2="-237.8075" y2="558.8635" width="0.3048" layer="94"/>
<wire x1="-253.0475" y1="563.9435" x2="-240.9825" y2="563.9435" width="0.3048" layer="94"/>
<wire x1="-240.9825" y1="563.9435" x2="-237.8075" y2="563.9435" width="0.3048" layer="94"/>
<wire x1="-253.0475" y1="569.0235" x2="-250.19" y2="569.0235" width="0.3048" layer="94"/>
<wire x1="-250.19" y1="569.0235" x2="-237.8075" y2="569.0235" width="0.3048" layer="94"/>
<wire x1="-253.0475" y1="574.1035" x2="-250.19" y2="574.1035" width="0.3048" layer="94"/>
<wire x1="-250.19" y1="569.0235" x2="-250.19" y2="574.1035" width="0.3048" layer="94"/>
<wire x1="-260.6675" y1="574.1035" x2="-260.6675" y2="577.9135" width="0.3048" layer="94"/>
<wire x1="-240.9825" y1="577.9135" x2="-240.9825" y2="563.9435" width="0.3048" layer="94"/>
<wire x1="-243.5225" y1="552.196" x2="-245.11" y2="552.196" width="0.3048" layer="94"/>
<wire x1="-245.11" y1="552.196" x2="-246.6975" y2="552.196" width="0.3048" layer="94"/>
<wire x1="-246.6975" y1="552.196" x2="-246.6975" y2="570.611" width="0.3048" layer="94"/>
<wire x1="-246.6975" y1="570.611" x2="-243.5225" y2="570.611" width="0.3048" layer="94"/>
<wire x1="-243.5225" y1="570.611" x2="-243.5225" y2="553.1485" width="0.3048" layer="94"/>
<wire x1="-243.5225" y1="553.1485" x2="-243.5225" y2="552.196" width="0.3048" layer="94"/>
<wire x1="-245.11" y1="552.196" x2="-245.11" y2="548.7035" width="0.1524" layer="94"/>
<wire x1="-245.11" y1="548.7035" x2="-240.9825" y2="548.7035" width="0.3048" layer="94"/>
<wire x1="-240.9825" y1="548.7035" x2="-240.9825" y2="544.8935" width="0.3048" layer="94"/>
<wire x1="-240.9825" y1="544.8935" x2="-245.11" y2="544.8935" width="0.3048" layer="94"/>
<wire x1="-245.11" y1="544.8935" x2="-249.2375" y2="544.8935" width="0.3048" layer="94"/>
<wire x1="-249.2375" y1="544.8935" x2="-249.2375" y2="548.7035" width="0.3048" layer="94"/>
<wire x1="-249.2375" y1="548.7035" x2="-245.11" y2="548.7035" width="0.3048" layer="94"/>
<wire x1="-245.11" y1="544.8935" x2="-245.11" y2="543.6235" width="0.3048" layer="94"/>
<wire x1="-245.11" y1="543.6235" x2="-239.7125" y2="543.6235" width="0.3048" layer="94"/>
<wire x1="-239.7125" y1="543.6235" x2="-239.7125" y2="549.656" width="0.3048" layer="94"/>
<wire x1="-239.7125" y1="549.656" x2="-243.5225" y2="553.1485" width="0.3048" layer="94"/>
<wire x1="-254.3175" y1="548.7035" x2="-256.2225" y2="548.7035" width="0.3048" layer="94"/>
<wire x1="-256.2225" y1="548.7035" x2="-258.1275" y2="548.7035" width="0.3048" layer="94"/>
<wire x1="-258.1275" y1="548.7035" x2="-258.1275" y2="546.7985" width="0.3048" layer="94"/>
<wire x1="-258.1275" y1="546.7985" x2="-258.1275" y2="544.8935" width="0.3048" layer="94"/>
<wire x1="-258.1275" y1="544.8935" x2="-256.2225" y2="544.8935" width="0.3048" layer="94"/>
<wire x1="-256.2225" y1="544.8935" x2="-254.3175" y2="544.8935" width="0.3048" layer="94"/>
<wire x1="-254.3175" y1="544.8935" x2="-254.3175" y2="546.7985" width="0.3048" layer="94"/>
<wire x1="-254.3175" y1="546.7985" x2="-254.3175" y2="548.7035" width="0.3048" layer="94"/>
<wire x1="-249.2375" y1="546.7985" x2="-254.3175" y2="546.7985" width="0.3048" layer="94" style="shortdash"/>
<wire x1="-256.2225" y1="544.2585" x2="-256.2225" y2="544.8935" width="0.3048" layer="94" style="shortdash"/>
<wire x1="-258.1275" y1="546.7985" x2="-258.7625" y2="546.7985" width="0.3048" layer="94" style="shortdash"/>
<wire x1="-256.2225" y1="548.7035" x2="-256.2225" y2="572.8335" width="0.3048" layer="94" style="shortdash"/>
<wire x1="-256.2225" y1="546.1635" x2="-256.2225" y2="546.7985" width="0.3048" layer="94" style="shortdash"/>
<wire x1="-256.2225" y1="546.7985" x2="-256.2225" y2="547.4335" width="0.3048" layer="94" style="shortdash"/>
<wire x1="-256.2225" y1="546.7985" x2="-256.8575" y2="546.7985" width="0.3048" layer="94" style="shortdash"/>
<wire x1="-255.5875" y1="546.7985" x2="-256.2225" y2="546.7985" width="0.3048" layer="94" style="shortdash"/>
<wire x1="-258.7625" y1="576.961" x2="-258.7625" y2="577.9135" width="0.3048" layer="94"/>
<wire x1="-258.7625" y1="577.9135" x2="-258.7625" y2="578.866" width="0.3048" layer="94"/>
<wire x1="-258.7625" y1="578.866" x2="-251.7775" y2="578.866" width="0.3048" layer="94"/>
<wire x1="-251.7775" y1="578.866" x2="-251.7775" y2="577.9135" width="0.3048" layer="94"/>
<wire x1="-251.7775" y1="577.9135" x2="-251.7775" y2="576.961" width="0.3048" layer="94"/>
<wire x1="-251.7775" y1="576.961" x2="-258.7625" y2="576.961" width="0.3048" layer="94"/>
<wire x1="-260.6675" y1="577.9135" x2="-258.7625" y2="577.9135" width="0.3048" layer="94"/>
<wire x1="-251.7775" y1="577.9135" x2="-247.3325" y2="577.9135" width="0.3048" layer="94"/>
<wire x1="-240.9825" y1="577.9135" x2="-242.2525" y2="577.9135" width="0.3048" layer="94"/>
<wire x1="-247.65" y1="580.136" x2="-242.2525" y2="577.9135" width="0.3048" layer="94"/>
<wire x1="-245.4275" y1="579.1835" x2="-245.4275" y2="580.136" width="0.3048" layer="94"/>
<wire x1="-245.4275" y1="580.771" x2="-245.4275" y2="581.7235" width="0.3048" layer="94"/>
<wire x1="-246.6975" y1="581.7235" x2="-244.1575" y2="581.7235" width="0.3048" layer="94"/>
<wire x1="-244.1575" y1="581.7235" x2="-244.1575" y2="581.0885" width="0.3048" layer="94"/>
<wire x1="-246.6975" y1="581.7235" x2="-246.6975" y2="581.0885" width="0.3048" layer="94"/>
<circle x="-240.9825" y="563.9435" radius="0.1588" width="0.6096" layer="94"/>
<circle x="-250.19" y="569.0235" radius="0.1588" width="0.6096" layer="94"/>
<text x="161.29" y="271.145" size="1.9304" layer="94">07</text>
<text x="148.59" y="287.655" size="3.81" layer="94">FULL BACKUP 208VAC BOX</text>
<text x="-231.14" y="487.68" size="2.54" layer="104" rot="MR180">MACHINE ROOM</text>
<text x="-304.165" y="317.5" size="2.54" layer="104" rot="MR180">208V AC</text>
<text x="-304.8" y="282.2575" size="2.54" layer="104">WIRING BOX</text>
<text x="-304.165" y="312.1025" size="2.54" layer="104" rot="MR180">INLET</text>
<text x="-154.94" y="477.52" size="2.1844" layer="97">FINAL_SETTING:</text>
<text x="-154.94" y="472.44" size="1.778" layer="97">CURRENT LIMITING - 4</text>
<text x="-154.94" y="469.9" size="1.778" layer="97">RAMP UP TIME - 10s</text>
<text x="-154.94" y="467.36" size="1.778" layer="97">STARTING VOLTAGE - 50%</text>
<text x="-154.94" y="464.82" size="1.778" layer="97">RAMP DOWN TIME - 0s</text>
<text x="-154.94" y="462.28" size="1.778" layer="97">TRIP CLASS - 10</text>
<text x="-154.94" y="459.74" size="1.778" layer="97">MOTOR CURRENT - 33A</text>
<text x="-195.2625" y="534.9875" size="1.778" layer="95">1/L1</text>
<text x="-183.8325" y="534.9875" size="1.778" layer="95">3/L2</text>
<text x="-172.4025" y="534.9875" size="1.778" layer="95">5/L3</text>
<text x="-195.2625" y="495.6175" size="1.778" layer="95">2/T1</text>
<text x="-183.8325" y="495.6175" size="1.778" layer="95">4/T2</text>
<text x="-172.4025" y="495.6175" size="1.778" layer="95">6/T3</text>
<text x="-150.8125" y="534.9875" size="1.778" layer="95">A1</text>
<text x="-150.8125" y="495.6175" size="1.778" layer="95">A2</text>
<text x="-130.4925" y="534.9875" size="1.778" layer="95">14/24</text>
<text x="-110.1725" y="534.9875" size="1.778" layer="95">95</text>
<text x="-116.5225" y="495.6175" size="1.778" layer="95">96</text>
<text x="-122.8725" y="495.6175" size="1.778" layer="95">23</text>
<text x="-103.8225" y="495.6175" size="1.778" layer="95">98</text>
<text x="-130.4925" y="495.6175" size="1.778" layer="95">13</text>
<text x="-129.2225" y="499.4275" size="1.778" layer="95" rot="R90">ON/RUN</text>
<text x="-121.6025" y="499.4275" size="1.778" layer="95" rot="R90">BYPASED</text>
<text x="-102.5525" y="498.1575" size="1.778" layer="95" rot="R90">OVERLOAD/FAILURE</text>
<text x="-209.2325" y="535.6225" size="2.54" layer="95" rot="R180">Q3</text>
<text x="-143.1925" y="534.9875" size="1.778" layer="95">1 IN</text>
<text x="-209.55" y="529.9075" size="2.54" layer="95" rot="R180">3RW4038-1BB04</text>
<text x="-261.9375" y="551.2435" size="1.778" layer="94" rot="R90">1</text>
<text x="-238.4425" y="551.2435" size="1.778" layer="94" rot="R90">2</text>
<text x="-261.9375" y="556.3235" size="1.778" layer="94" rot="R90">3</text>
<text x="-238.4425" y="556.3235" size="1.778" layer="94" rot="R90">4</text>
<text x="-261.9375" y="561.4035" size="1.778" layer="94" rot="R90">5</text>
<text x="-238.4425" y="561.4035" size="1.778" layer="94" rot="R90">6</text>
<text x="-261.9375" y="566.4835" size="1.778" layer="94" rot="R90">N</text>
<text x="-238.4425" y="566.4835" size="1.778" layer="94" rot="R90">N</text>
<text x="-276.86" y="577.9135" size="2.54" layer="95">FI1</text>
<text x="-276.86" y="574.421" size="2.54" layer="95">63A/0,1A</text>
<wire x1="-230.505" y1="563.9435" x2="-222.885" y2="563.9435" width="0.3048" layer="94"/>
<wire x1="-223.52" y1="562.0385" x2="-217.805" y2="563.9435" width="0.3048" layer="94"/>
<wire x1="-217.805" y1="563.9435" x2="-214.63" y2="563.9435" width="0.3048" layer="94"/>
<wire x1="-214.63" y1="563.9435" x2="-214.63" y2="565.8485" width="0.3048" layer="94"/>
<wire x1="-214.63" y1="565.8485" x2="-212.09" y2="565.8485" width="0.3048" layer="94"/>
<wire x1="-212.09" y1="565.8485" x2="-212.09" y2="563.9435" width="0.3048" layer="94"/>
<wire x1="-212.09" y1="563.9435" x2="-210.185" y2="563.9435" width="0.3048" layer="94"/>
<wire x1="-208.915" y1="563.3085" x2="-205.105" y2="563.3085" width="0.3048" layer="94"/>
<wire x1="-208.28" y1="563.9435" x2="-207.01" y2="566.4835" width="0.3048" layer="94"/>
<wire x1="-207.01" y1="566.4835" x2="-205.74" y2="563.9435" width="0.3048" layer="94"/>
<wire x1="-216.535" y1="562.0385" x2="-216.535" y2="567.1185" width="0.3048" layer="94"/>
<wire x1="-216.535" y1="567.1185" x2="-210.185" y2="567.1185" width="0.3048" layer="94"/>
<wire x1="-210.185" y1="567.1185" x2="-210.185" y2="562.0385" width="0.3048" layer="94"/>
<wire x1="-203.835" y1="562.0385" x2="-203.835" y2="563.9435" width="0.3048" layer="94"/>
<wire x1="-203.835" y1="563.9435" x2="-203.835" y2="567.1185" width="0.3048" layer="94"/>
<wire x1="-203.835" y1="567.1185" x2="-210.185" y2="567.1185" width="0.3048" layer="94"/>
<wire x1="-203.835" y1="563.9435" x2="-200.025" y2="563.9435" width="0.3048" layer="94"/>
<wire x1="-230.505" y1="558.8635" x2="-222.885" y2="558.8635" width="0.3048" layer="94"/>
<wire x1="-223.52" y1="556.9585" x2="-217.805" y2="558.8635" width="0.3048" layer="94"/>
<wire x1="-217.805" y1="558.8635" x2="-214.63" y2="558.8635" width="0.3048" layer="94"/>
<wire x1="-214.63" y1="558.8635" x2="-214.63" y2="560.7685" width="0.3048" layer="94"/>
<wire x1="-214.63" y1="560.7685" x2="-212.09" y2="560.7685" width="0.3048" layer="94"/>
<wire x1="-212.09" y1="560.7685" x2="-212.09" y2="558.8635" width="0.3048" layer="94"/>
<wire x1="-212.09" y1="558.8635" x2="-210.185" y2="558.8635" width="0.3048" layer="94"/>
<wire x1="-208.915" y1="558.2285" x2="-205.105" y2="558.2285" width="0.3048" layer="94"/>
<wire x1="-208.28" y1="558.8635" x2="-207.01" y2="561.4035" width="0.3048" layer="94"/>
<wire x1="-207.01" y1="561.4035" x2="-205.74" y2="558.8635" width="0.3048" layer="94"/>
<wire x1="-216.535" y1="556.9585" x2="-216.535" y2="562.0385" width="0.3048" layer="94"/>
<wire x1="-216.535" y1="562.0385" x2="-210.185" y2="562.0385" width="0.3048" layer="94"/>
<wire x1="-210.185" y1="562.0385" x2="-210.185" y2="556.9585" width="0.3048" layer="94"/>
<wire x1="-203.835" y1="556.9585" x2="-203.835" y2="558.8635" width="0.3048" layer="94"/>
<wire x1="-203.835" y1="558.8635" x2="-203.835" y2="562.0385" width="0.3048" layer="94"/>
<wire x1="-203.835" y1="562.0385" x2="-210.185" y2="562.0385" width="0.3048" layer="94"/>
<wire x1="-203.835" y1="558.8635" x2="-200.025" y2="558.8635" width="0.3048" layer="94"/>
<wire x1="-230.505" y1="553.7835" x2="-222.885" y2="553.7835" width="0.3048" layer="94"/>
<wire x1="-223.52" y1="551.8785" x2="-217.805" y2="553.7835" width="0.3048" layer="94"/>
<wire x1="-217.805" y1="553.7835" x2="-214.63" y2="553.7835" width="0.3048" layer="94"/>
<wire x1="-214.63" y1="553.7835" x2="-214.63" y2="555.6885" width="0.3048" layer="94"/>
<wire x1="-214.63" y1="555.6885" x2="-212.09" y2="555.6885" width="0.3048" layer="94"/>
<wire x1="-212.09" y1="555.6885" x2="-212.09" y2="553.7835" width="0.3048" layer="94"/>
<wire x1="-212.09" y1="553.7835" x2="-210.185" y2="553.7835" width="0.3048" layer="94"/>
<wire x1="-208.915" y1="553.1485" x2="-205.105" y2="553.1485" width="0.3048" layer="94"/>
<wire x1="-208.28" y1="553.7835" x2="-207.01" y2="556.3235" width="0.3048" layer="94"/>
<wire x1="-207.01" y1="556.3235" x2="-205.74" y2="553.7835" width="0.3048" layer="94"/>
<wire x1="-216.535" y1="551.8785" x2="-216.535" y2="556.9585" width="0.3048" layer="94"/>
<wire x1="-216.535" y1="556.9585" x2="-210.185" y2="556.9585" width="0.3048" layer="94"/>
<wire x1="-210.185" y1="556.9585" x2="-210.185" y2="551.8785" width="0.3048" layer="94"/>
<wire x1="-210.185" y1="551.8785" x2="-213.36" y2="551.8785" width="0.3048" layer="94"/>
<wire x1="-213.36" y1="551.8785" x2="-216.535" y2="551.8785" width="0.3048" layer="94"/>
<wire x1="-210.185" y1="551.8785" x2="-207.01" y2="551.8785" width="0.3048" layer="94"/>
<wire x1="-207.01" y1="551.8785" x2="-203.835" y2="551.8785" width="0.3048" layer="94"/>
<wire x1="-203.835" y1="551.8785" x2="-203.835" y2="553.7835" width="0.3048" layer="94"/>
<wire x1="-203.835" y1="553.7835" x2="-203.835" y2="556.9585" width="0.3048" layer="94"/>
<wire x1="-203.835" y1="556.9585" x2="-210.185" y2="556.9585" width="0.3048" layer="94"/>
<wire x1="-203.835" y1="553.7835" x2="-200.025" y2="553.7835" width="0.3048" layer="94"/>
<wire x1="-220.98" y1="562.6735" x2="-220.98" y2="549.9735" width="0.1524" layer="94" style="shortdash"/>
<wire x1="-213.36" y1="551.8785" x2="-213.36" y2="548.0685" width="0.1524" layer="94" style="shortdash"/>
<wire x1="-213.36" y1="548.0685" x2="-207.01" y2="548.0685" width="0.1524" layer="94" style="shortdash"/>
<wire x1="-207.01" y1="548.0685" x2="-207.01" y2="551.8785" width="0.1524" layer="94" style="shortdash"/>
<wire x1="-213.36" y1="548.0685" x2="-218.44" y2="548.0685" width="0.1524" layer="94" style="shortdash"/>
<wire x1="-222.885" y1="549.9735" x2="-220.98" y2="549.9735" width="0.3048" layer="94"/>
<wire x1="-220.98" y1="549.9735" x2="-219.075" y2="549.9735" width="0.3048" layer="94"/>
<wire x1="-219.075" y1="549.9735" x2="-219.075" y2="546.1635" width="0.3048" layer="94"/>
<wire x1="-219.075" y1="546.1635" x2="-222.885" y2="546.1635" width="0.3048" layer="94"/>
<wire x1="-222.885" y1="546.1635" x2="-222.885" y2="549.9735" width="0.3048" layer="94"/>
<wire x1="-220.98" y1="549.9735" x2="-220.98" y2="544.2585" width="0.1524" layer="94"/>
<wire x1="-220.98" y1="544.2585" x2="-219.71" y2="544.2585" width="0.1524" layer="94"/>
<wire x1="-220.98" y1="544.2585" x2="-222.25" y2="544.2585" width="0.1524" layer="94"/>
<wire x1="-224.155" y1="548.0685" x2="-218.44" y2="548.0685" width="0.1524" layer="94"/>
<text x="-228.2825" y="551.561" size="1.778" layer="94" rot="R90">1</text>
<text x="-228.2825" y="556.641" size="1.778" layer="94" rot="R90">3</text>
<text x="-228.2825" y="561.721" size="1.778" layer="94" rot="R90">5</text>
<text x="-200.3425" y="551.561" size="1.778" layer="94" rot="R90">2</text>
<text x="-200.3425" y="556.641" size="1.778" layer="94" rot="R90">4</text>
<text x="-200.3425" y="561.721" size="1.778" layer="94" rot="R90">6</text>
<text x="-215.9" y="577.9135" size="2.54" layer="95">FA14</text>
<text x="-215.9" y="574.421" size="2.54" layer="95">GV3P65</text>
<wire x1="-223.52" y1="399.415" x2="-218.44" y2="399.415" width="0.4064" layer="94"/>
<wire x1="-218.44" y1="399.415" x2="-210.82" y2="399.415" width="0.4064" layer="94"/>
<wire x1="-210.82" y1="399.415" x2="-210.82" y2="391.795" width="0.4064" layer="94"/>
<wire x1="-210.82" y1="391.795" x2="-213.36" y2="391.795" width="0.4064" layer="94"/>
<wire x1="-213.36" y1="391.795" x2="-218.44" y2="391.795" width="0.4064" layer="94"/>
<wire x1="-218.44" y1="391.795" x2="-220.98" y2="391.795" width="0.4064" layer="94"/>
<wire x1="-220.98" y1="391.795" x2="-226.06" y2="391.795" width="0.4064" layer="94"/>
<wire x1="-226.06" y1="391.795" x2="-226.06" y2="399.415" width="0.4064" layer="94"/>
<wire x1="-226.06" y1="399.415" x2="-220.98" y2="399.415" width="0.4064" layer="94"/>
<wire x1="-220.98" y1="399.415" x2="-220.98" y2="391.795" width="0.4064" layer="94"/>
<wire x1="-218.44" y1="399.415" x2="-218.44" y2="402.59" width="0.4064" layer="94"/>
<wire x1="-218.44" y1="391.795" x2="-218.44" y2="388.62" width="0.4064" layer="94"/>
<wire x1="-213.36" y1="391.795" x2="-213.36" y2="388.62" width="0.4064" layer="94"/>
<wire x1="-207.645" y1="395.605" x2="-206.375" y2="395.605" width="0.1524" layer="94"/>
<wire x1="-205.105" y1="395.605" x2="-203.835" y2="395.605" width="0.1524" layer="94"/>
<wire x1="-202.565" y1="395.605" x2="-201.295" y2="395.605" width="0.1524" layer="94"/>
<wire x1="-194.945" y1="395.605" x2="-193.675" y2="395.605" width="0.1524" layer="94"/>
<wire x1="-192.405" y1="395.605" x2="-191.135" y2="395.605" width="0.1524" layer="94"/>
<wire x1="-197.485" y1="395.605" x2="-196.215" y2="395.605" width="0.1524" layer="94"/>
<wire x1="-189.865" y1="395.605" x2="-188.595" y2="395.605" width="0.1524" layer="94"/>
<wire x1="-187.325" y1="395.605" x2="-186.055" y2="395.605" width="0.1524" layer="94"/>
<text x="-224.79" y="396.875" size="1.778" layer="95">3~</text>
<text x="-218.44" y="394.335" size="2.1844" layer="95">&lt;U</text>
<text x="-216.535" y="389.255" size="1.778" layer="95">L1</text>
<text x="-221.615" y="389.255" size="1.778" layer="95">L2</text>
<text x="-221.615" y="400.05" size="1.778" layer="95">L3</text>
<text x="-203.835" y="389.89" size="1.778" layer="95">11</text>
<text x="-205.74" y="398.145" size="1.778" layer="95">14</text>
<text x="-197.485" y="398.145" size="1.778" layer="95">12</text>
<text x="-213.6775" y="406.4" size="1.778" layer="95">RE104</text>
<text x="-213.6775" y="402.59" size="1.778" layer="95">3UG46 17</text>
<text x="-181.61" y="398.145" size="1.778" layer="95">22</text>
<text x="-190.5" y="398.145" size="1.778" layer="95">24</text>
<text x="-188.595" y="389.89" size="1.778" layer="95">21</text>
<wire x1="-210.185" y1="395.605" x2="-208.915" y2="395.605" width="0.1524" layer="94"/>
<wire x1="-158.75" y1="430.53" x2="-158.75" y2="434.34" width="0.3048" layer="94"/>
<wire x1="-158.75" y1="434.34" x2="-157.48" y2="438.15" width="0.3048" layer="94"/>
<wire x1="-157.48" y1="438.15" x2="-156.21" y2="441.96" width="0.3048" layer="94"/>
<wire x1="-158.75" y1="441.325" x2="-158.75" y2="445.77" width="0.3048" layer="94"/>
<wire x1="-165.1" y1="430.53" x2="-165.1" y2="434.34" width="0.3048" layer="94"/>
<wire x1="-165.1" y1="434.34" x2="-167.64" y2="441.96" width="0.3048" layer="94"/>
<wire x1="-165.1" y1="441.325" x2="-165.1" y2="445.77" width="0.3048" layer="94"/>
<text x="-167.005" y="419.735" size="1.4224" layer="95" rot="MR90">AUX_Q1</text>
<text x="-160.655" y="419.735" size="1.4224" layer="95" rot="MR90">AUX_Q2</text>
<text x="-165.735" y="444.5" size="1.4224" layer="95" rot="MR0">63</text>
<text x="-165.735" y="430.53" size="1.4224" layer="95" rot="MR0">64</text>
<text x="-159.385" y="430.53" size="1.4224" layer="95" rot="MR0">84</text>
<text x="-159.385" y="444.5" size="1.4224" layer="95" rot="MR0">83</text>
<wire x1="-175.26" y1="438.15" x2="-157.48" y2="438.15" width="0.3048" layer="94" style="shortdash"/>
<text x="-215.9" y="570.865" size="2.54" layer="95">48-65A</text>
<text x="-279.7175" y="455.93" size="1.778" layer="95">3RT1036-1BB44</text>
<wire x1="-261.62" y1="350.52" x2="-264.795" y2="343.535" width="0.1524" layer="94"/>
<wire x1="-238.76" y1="350.52" x2="-242.57" y2="343.535" width="0.1524" layer="94"/>
<wire x1="-250.19" y1="350.52" x2="-254" y2="343.535" width="0.1524" layer="94"/>
<wire x1="-279.4" y1="350.52" x2="-282.575" y2="343.535" width="0.1524" layer="96"/>
<wire x1="-279.4" y1="350.52" x2="-279.4" y2="343.535" width="0.1524" layer="96"/>
<wire x1="-279.4" y1="350.52" x2="-275.59" y2="343.535" width="0.1524" layer="96"/>
<wire x1="-276.225" y1="346.71" x2="-274.32" y2="346.71" width="0.1524" layer="96"/>
<wire x1="-272.7325" y1="346.71" x2="-270.8275" y2="346.71" width="0.1524" layer="96"/>
<wire x1="-255.27" y1="346.71" x2="-253.365" y2="346.71" width="0.1524" layer="96"/>
<wire x1="-262.5725" y1="346.71" x2="-260.6675" y2="346.71" width="0.1524" layer="96"/>
<wire x1="-259.08" y1="346.71" x2="-257.175" y2="346.71" width="0.1524" layer="96"/>
<wire x1="-243.84" y1="346.71" x2="-241.935" y2="346.71" width="0.1524" layer="96"/>
<wire x1="-251.1425" y1="346.71" x2="-249.2375" y2="346.71" width="0.1524" layer="96"/>
<wire x1="-247.65" y1="346.71" x2="-245.745" y2="346.71" width="0.1524" layer="96"/>
<wire x1="-266.065" y1="346.71" x2="-264.16" y2="346.71" width="0.1524" layer="96"/>
<wire x1="-269.24" y1="346.71" x2="-267.335" y2="346.71" width="0.1524" layer="96"/>
<circle x="-264.795" y="343.535" radius="1.27" width="0.1524" layer="94"/>
<circle x="-261.62" y="350.52" radius="1.27" width="0.1524" layer="94"/>
<circle x="-254" y="343.535" radius="1.27" width="0.1524" layer="94"/>
<circle x="-250.19" y="350.52" radius="1.27" width="0.1524" layer="94"/>
<circle x="-234.95" y="343.535" radius="1.27" width="0.1524" layer="94"/>
<circle x="-238.76" y="350.52" radius="1.27" width="0.1524" layer="94"/>
<circle x="-279.4" y="350.52" radius="1.27" width="0.1524" layer="96"/>
<circle x="-257.81" y="343.535" radius="1.27" width="0.1524" layer="94"/>
<circle x="-242.57" y="343.535" radius="1.27" width="0.1524" layer="94"/>
<circle x="-246.38" y="343.535" radius="1.27" width="0.1524" layer="94"/>
<text x="-285.115" y="342.9" size="1.27" layer="96" rot="MR180">MAIN</text>
<text x="-278.13" y="342.5825" size="1.27" layer="96" rot="MR180">BACKUP</text>
<text x="-280.9875" y="340.0425" size="1.27" layer="96" rot="MR180">OFF</text>
<text x="-262.255" y="353.695" size="1.4224" layer="95" rot="R180">1</text>
<text x="-265.43" y="341.63" size="1.4224" layer="95" rot="R180">2</text>
<text x="-250.825" y="353.695" size="1.4224" layer="95" rot="R180">5</text>
<text x="-258.445" y="341.63" size="1.4224" layer="95" rot="R180">4</text>
<text x="-239.395" y="353.695" size="1.4224" layer="95" rot="R180">9</text>
<text x="-235.585" y="341.63" size="1.4224" layer="95" rot="R180">12</text>
<text x="-284.48" y="353.3775" size="1.778" layer="95" rot="MR180">S4</text>
<text x="-284.48" y="356.235" size="1.778" layer="95" rot="MR180">VSN63-2205-C8</text>
<text x="-254.635" y="341.63" size="1.4224" layer="95" rot="R180">6</text>
<text x="-247.015" y="341.63" size="1.4224" layer="95" rot="R180">8</text>
<text x="-243.205" y="341.63" size="1.4224" layer="95" rot="R180">10</text>
<text x="162.56" y="350.52" size="1.778" layer="94" rot="R270" align="bottom-center">Twisted pair cable</text>
<text x="186.055" y="501.015" size="1.778" layer="94" rot="R180" align="bottom-center">DIAG SOCKET</text>
<text x="114.3" y="441.96" size="1.778" layer="94" rot="R90" align="bottom-center">Twisted pair cable</text>
<text x="134.62" y="441.96" size="1.778" layer="94" rot="R90" align="bottom-center">Twisted pair cable</text>
<text x="150.495" y="495.935" size="1.778" layer="94" rot="MR180" align="center-right">1,5 mm2</text>
<text x="66.04" y="435.483" size="1.778" layer="94" rot="MR0" align="center-right">10mm2</text>
<text x="150.495" y="483.235" size="1.778" layer="94" rot="MR180" align="center-right">twist</text>
<text x="150.495" y="465.455" size="1.778" layer="94" rot="MR180" align="center-right">twist</text>
<wire x1="168.275" y1="495.935" x2="203.835" y2="495.935" width="0.1524" layer="94" style="shortdash"/>
<wire x1="203.835" y1="495.935" x2="203.835" y2="445.135" width="0.1524" layer="94" style="shortdash"/>
<wire x1="203.835" y1="445.135" x2="168.275" y2="445.135" width="0.1524" layer="94" style="shortdash"/>
<wire x1="168.275" y1="445.135" x2="168.275" y2="495.935" width="0.1524" layer="94" style="shortdash"/>
<circle x="196.215" y="493.395" radius="1.27" width="0.254" layer="94"/>
<circle x="196.215" y="488.315" radius="1.27" width="0.254" layer="94"/>
<circle x="196.215" y="473.075" radius="1.27" width="0.254" layer="94"/>
<circle x="196.215" y="467.995" radius="1.27" width="0.254" layer="94"/>
<circle x="196.215" y="452.755" radius="1.27" width="0.254" layer="94"/>
<circle x="196.215" y="447.675" radius="1.27" width="0.254" layer="94"/>
<text x="198.755" y="493.395" size="1.778" layer="94" align="center-left">A</text>
<text x="198.755" y="473.075" size="1.778" layer="94" align="center-left">F</text>
<text x="198.755" y="488.315" size="1.778" layer="94" align="center-left">M</text>
<text x="198.755" y="467.995" size="1.778" layer="94" align="center-left">H</text>
<text x="198.755" y="452.755" size="1.778" layer="94" align="center-left">G</text>
<text x="198.755" y="447.675" size="1.778" layer="94" align="center-left">B</text>
<wire x1="180.34" y1="372.745" x2="180.34" y2="377.825" width="0.3048" layer="94"/>
<wire x1="185.42" y1="372.745" x2="185.42" y2="377.825" width="0.3048" layer="94"/>
<wire x1="190.5" y1="372.745" x2="190.5" y2="377.825" width="0.3048" layer="94"/>
<wire x1="195.58" y1="372.745" x2="195.58" y2="377.825" width="0.3048" layer="94"/>
<wire x1="200.66" y1="372.745" x2="200.66" y2="377.825" width="0.3048" layer="94"/>
<wire x1="205.74" y1="372.745" x2="205.74" y2="377.825" width="0.3048" layer="94"/>
<wire x1="165.1" y1="377.825" x2="220.98" y2="377.825" width="0.4064" layer="94"/>
<wire x1="220.98" y1="377.825" x2="220.98" y2="410.845" width="0.4064" layer="94"/>
<wire x1="220.98" y1="421.005" x2="220.98" y2="431.165" width="0.4064" layer="94"/>
<wire x1="220.98" y1="431.165" x2="165.1" y2="431.165" width="0.4064" layer="94"/>
<wire x1="165.1" y1="431.165" x2="165.1" y2="421.005" width="0.4064" layer="94"/>
<wire x1="165.1" y1="421.005" x2="165.1" y2="410.845" width="0.4064" layer="94"/>
<wire x1="165.1" y1="410.845" x2="165.1" y2="377.825" width="0.4064" layer="94"/>
<wire x1="165.1" y1="421.005" x2="172.72" y2="421.005" width="0.3048" layer="94"/>
<wire x1="172.72" y1="421.005" x2="213.36" y2="421.005" width="0.3048" layer="94"/>
<wire x1="213.36" y1="421.005" x2="220.98" y2="421.005" width="0.3048" layer="94"/>
<wire x1="220.98" y1="421.005" x2="220.98" y2="410.845" width="0.4064" layer="94"/>
<wire x1="220.98" y1="410.845" x2="213.36" y2="410.845" width="0.3048" layer="94"/>
<wire x1="213.36" y1="410.845" x2="172.72" y2="410.845" width="0.3048" layer="94"/>
<wire x1="172.72" y1="410.845" x2="165.1" y2="410.845" width="0.3048" layer="94"/>
<wire x1="172.72" y1="410.845" x2="172.72" y2="421.005" width="0.3048" layer="94"/>
<wire x1="213.36" y1="410.845" x2="213.36" y2="421.005" width="0.3048" layer="94"/>
<circle x="207.01" y="394.335" radius="9.1581" width="0.3048" layer="94"/>
<circle x="207.01" y="394.335" radius="8.032184375" width="0.3048" layer="94"/>
<wire x1="205.74" y1="400.685" x2="205.74" y2="387.985" width="0.3048" layer="94"/>
<wire x1="205.74" y1="387.985" x2="208.28" y2="387.985" width="0.3048" layer="94"/>
<wire x1="208.28" y1="387.985" x2="208.28" y2="400.685" width="0.3048" layer="94"/>
<wire x1="208.28" y1="400.685" x2="205.74" y2="400.685" width="0.3048" layer="94"/>
<wire x1="172.72" y1="399.415" x2="173.99" y2="398.145" width="0.3048" layer="94"/>
<wire x1="173.99" y1="398.145" x2="176.53" y2="400.685" width="0.3048" layer="94"/>
<wire x1="176.53" y1="400.685" x2="177.8" y2="399.415" width="0.3048" layer="94"/>
<wire x1="177.8" y1="399.415" x2="177.8" y2="403.225" width="0.3048" layer="94"/>
<wire x1="177.8" y1="403.225" x2="173.99" y2="403.225" width="0.3048" layer="94"/>
<wire x1="173.99" y1="403.225" x2="175.26" y2="401.955" width="0.3048" layer="94"/>
<wire x1="175.26" y1="401.955" x2="172.72" y2="399.415" width="0.3048" layer="94"/>
<wire x1="177.8" y1="389.255" x2="176.53" y2="390.525" width="0.3048" layer="94"/>
<wire x1="176.53" y1="390.525" x2="173.99" y2="387.985" width="0.3048" layer="94"/>
<wire x1="173.99" y1="387.985" x2="172.72" y2="389.255" width="0.3048" layer="94"/>
<wire x1="172.72" y1="389.255" x2="172.72" y2="385.445" width="0.3048" layer="94"/>
<wire x1="172.72" y1="385.445" x2="176.53" y2="385.445" width="0.3048" layer="94"/>
<wire x1="176.53" y1="385.445" x2="175.26" y2="386.715" width="0.3048" layer="94"/>
<wire x1="175.26" y1="386.715" x2="177.8" y2="389.255" width="0.3048" layer="94"/>
<text x="185.42" y="434.975" size="2.54" layer="94">CANarmatur</text>
<text x="180.34" y="379.095" size="1.778" layer="94" rot="R90" align="center-left">1</text>
<text x="185.42" y="379.095" size="1.778" layer="94" rot="R90" align="center-left">3</text>
<text x="190.5" y="379.095" size="1.778" layer="94" rot="R90" align="center-left">5</text>
<text x="195.58" y="379.095" size="1.778" layer="94" rot="R90" align="center-left">8</text>
<text x="200.66" y="379.095" size="1.778" layer="94" rot="R90" align="center-left">14</text>
<text x="205.74" y="379.095" size="1.778" layer="94" rot="R90" align="center-left">18</text>
<wire x1="-25.4" y1="474.98" x2="76.2" y2="474.98" width="0.3048" layer="94"/>
<wire x1="76.2" y1="474.98" x2="76.2" y2="393.7" width="0.3048" layer="94"/>
<wire x1="76.2" y1="393.7" x2="-25.4" y2="393.7" width="0.3048" layer="94"/>
<wire x1="-25.4" y1="393.7" x2="-25.4" y2="474.98" width="0.3048" layer="94"/>
<text x="-18.415" y="408.94" size="2.54" layer="94">AUTOSTART_LITE01</text>
<text x="40.64" y="419.1" size="1.778" layer="91">4x2,5 mm2</text>
<text x="66.04" y="450.723" size="1.778" layer="94" rot="MR0" align="center-right">10mm2</text>
<wire x1="151.13" y1="542.29" x2="151.13" y2="567.055" width="0.1524" layer="94"/>
<wire x1="156.21" y1="567.055" x2="161.29" y2="567.055" width="0.1524" layer="94"/>
<wire x1="161.29" y1="542.29" x2="156.21" y2="542.29" width="0.1524" layer="94"/>
<wire x1="156.21" y1="542.29" x2="156.21" y2="567.055" width="0.1524" layer="94"/>
<wire x1="161.29" y1="567.055" x2="166.37" y2="567.055" width="0.1524" layer="94"/>
<wire x1="166.37" y1="542.29" x2="161.29" y2="542.29" width="0.1524" layer="94"/>
<wire x1="161.29" y1="542.29" x2="161.29" y2="567.055" width="0.1524" layer="94"/>
<wire x1="166.37" y1="567.055" x2="171.45" y2="567.055" width="0.1524" layer="94"/>
<wire x1="171.45" y1="542.29" x2="166.37" y2="542.29" width="0.1524" layer="94"/>
<wire x1="166.37" y1="542.29" x2="166.37" y2="567.055" width="0.1524" layer="94"/>
<wire x1="171.45" y1="567.055" x2="176.53" y2="567.055" width="0.1524" layer="94"/>
<wire x1="176.53" y1="542.29" x2="171.45" y2="542.29" width="0.1524" layer="94"/>
<wire x1="171.45" y1="542.29" x2="171.45" y2="567.055" width="0.1524" layer="94"/>
<wire x1="176.53" y1="567.055" x2="181.61" y2="567.055" width="0.1524" layer="94"/>
<wire x1="181.61" y1="542.29" x2="176.53" y2="542.29" width="0.1524" layer="94"/>
<wire x1="176.53" y1="542.29" x2="176.53" y2="567.055" width="0.1524" layer="94"/>
<wire x1="181.61" y1="567.055" x2="186.69" y2="567.055" width="0.1524" layer="94"/>
<wire x1="186.69" y1="542.29" x2="181.61" y2="542.29" width="0.1524" layer="94"/>
<wire x1="181.61" y1="542.29" x2="181.61" y2="567.055" width="0.1524" layer="94"/>
<wire x1="151.13" y1="567.055" x2="156.21" y2="567.055" width="0.1524" layer="94"/>
<wire x1="151.13" y1="542.29" x2="156.21" y2="542.29" width="0.1524" layer="94"/>
<text x="156.845" y="563.753" size="2.1844" layer="94">79</text>
<text x="152.273" y="563.753" size="2.1844" layer="94">78</text>
<text x="161.925" y="563.753" size="2.1844" layer="94">80</text>
<text x="167.005" y="563.753" size="2.1844" layer="94">81</text>
<text x="172.085" y="563.753" size="2.1844" layer="94">82</text>
<text x="177.165" y="563.753" size="2.1844" layer="94">83</text>
<text x="182.245" y="563.753" size="2.1844" layer="94">84</text>
<text x="163.449" y="568.071" size="1.27" layer="95" rot="R90">GRAY</text>
<text x="168.529" y="568.071" size="1.27" layer="95" rot="R90">BLACK</text>
<text x="173.609" y="568.071" size="1.27" layer="95" rot="R90">BROWN</text>
<wire x1="186.69" y1="542.29" x2="186.69" y2="567.055" width="0.1524" layer="94"/>
<text x="-304.165" y="332.4225" size="2.54" layer="104" rot="MR180">DEUTZ GENERATOR</text>
<text x="-304.165" y="335.28" size="2.54" layer="104">208V AC</text>
<wire x1="-195.834" y1="335.28" x2="-195.834" y2="337.82" width="0.1524" layer="94"/>
<wire x1="-195.834" y1="337.82" x2="-193.04" y2="337.82" width="0.1524" layer="94"/>
<text x="-179.3875" y="342.5825" size="1.778" layer="95">ON</text>
<text x="-199.7075" y="342.5825" size="1.778" layer="95">OFF</text>
<text x="54.2925" y="286.0675" size="1.6764" layer="96">PHASE_ORDER_OK</text>
<text x="-200.3425" y="347.98" size="2.54" layer="96">ELECTRO MOTOR</text>
<wire x1="-215.9" y1="350.52" x2="-219.71" y2="343.535" width="0.1524" layer="94"/>
<wire x1="-227.33" y1="350.52" x2="-231.14" y2="343.535" width="0.1524" layer="94"/>
<wire x1="-220.98" y1="346.71" x2="-219.075" y2="346.71" width="0.1524" layer="96"/>
<wire x1="-228.2825" y1="346.71" x2="-226.3775" y2="346.71" width="0.1524" layer="96"/>
<wire x1="-224.79" y1="346.71" x2="-222.885" y2="346.71" width="0.1524" layer="96"/>
<circle x="-231.14" y="343.535" radius="1.27" width="0.1524" layer="94"/>
<circle x="-227.33" y="350.52" radius="1.27" width="0.1524" layer="94"/>
<circle x="-212.09" y="343.535" radius="1.27" width="0.1524" layer="94"/>
<circle x="-215.9" y="350.52" radius="1.27" width="0.1524" layer="94"/>
<circle x="-219.71" y="343.535" radius="1.27" width="0.1524" layer="94"/>
<circle x="-223.52" y="343.535" radius="1.27" width="0.1524" layer="94"/>
<text x="-227.965" y="353.695" size="1.4224" layer="95" rot="R180">13</text>
<text x="-216.535" y="353.695" size="1.4224" layer="95" rot="R180">17</text>
<text x="-212.725" y="341.63" size="1.4224" layer="95" rot="R180">20</text>
<text x="-231.775" y="341.63" size="1.4224" layer="95" rot="R180">14</text>
<text x="-224.155" y="341.63" size="1.4224" layer="95" rot="R180">16</text>
<text x="-220.345" y="341.63" size="1.4224" layer="95" rot="R180">18</text>
<wire x1="-232.41" y1="346.71" x2="-230.505" y2="346.71" width="0.1524" layer="96"/>
<wire x1="-239.7125" y1="346.71" x2="-237.8075" y2="346.71" width="0.1524" layer="96"/>
<wire x1="-236.22" y1="346.71" x2="-234.315" y2="346.71" width="0.1524" layer="96"/>
<wire x1="-114.3" y1="325.12" x2="-109.22" y2="325.12" width="0.4064" layer="94"/>
<wire x1="-109.22" y1="325.12" x2="-104.14" y2="325.12" width="0.4064" layer="94"/>
<wire x1="-104.14" y1="325.12" x2="-104.14" y2="318.77" width="0.4064" layer="94"/>
<wire x1="-104.14" y1="318.77" x2="-109.22" y2="318.77" width="0.4064" layer="94"/>
<wire x1="-109.22" y1="318.77" x2="-114.3" y2="318.77" width="0.4064" layer="94"/>
<wire x1="-114.3" y1="318.77" x2="-116.84" y2="318.77" width="0.4064" layer="94"/>
<wire x1="-116.84" y1="318.77" x2="-119.38" y2="318.77" width="0.4064" layer="94"/>
<wire x1="-119.38" y1="318.77" x2="-119.38" y2="325.12" width="0.4064" layer="94"/>
<wire x1="-119.38" y1="325.12" x2="-114.3" y2="325.12" width="0.4064" layer="94"/>
<wire x1="-114.3" y1="325.12" x2="-114.3" y2="318.77" width="0.4064" layer="94"/>
<wire x1="-109.22" y1="325.12" x2="-109.22" y2="327.66" width="0.4064" layer="94"/>
<wire x1="-109.22" y1="318.77" x2="-109.22" y2="316.23" width="0.4064" layer="94"/>
<wire x1="-100.965" y1="321.945" x2="-99.695" y2="321.945" width="0.1524" layer="94"/>
<wire x1="-98.425" y1="321.945" x2="-97.155" y2="321.945" width="0.1524" layer="94"/>
<wire x1="-95.885" y1="321.945" x2="-94.615" y2="321.945" width="0.1524" layer="94"/>
<text x="-107.95" y="316.23" size="1.778" layer="95">A2</text>
<text x="-118.618" y="316.23" size="1.778" layer="95">S</text>
<text x="-108.204" y="327.152" size="1.778" layer="95">A1</text>
<text x="-97.155" y="317.5" size="1.778" layer="95">11</text>
<text x="-99.06" y="325.755" size="1.778" layer="95">14</text>
<text x="-90.805" y="325.755" size="1.778" layer="95">12</text>
<text x="-111.76" y="334.01" size="1.778" layer="95" rot="R180">RE105</text>
<text x="-111.76" y="331.47" size="1.778" layer="95" rot="R180">CRM91H</text>
<wire x1="-103.505" y1="321.945" x2="-102.235" y2="321.945" width="0.1524" layer="94"/>
<wire x1="-119.38" y1="318.77" x2="-114.3" y2="325.12" width="0.1524" layer="94"/>
<wire x1="-119.38" y1="325.12" x2="-114.3" y2="318.77" width="0.1524" layer="94"/>
<wire x1="-116.84" y1="318.77" x2="-116.84" y2="316.23" width="0.4064" layer="94"/>
<wire x1="-154.94" y1="325.12" x2="-149.86" y2="325.12" width="0.4064" layer="94"/>
<wire x1="-149.86" y1="325.12" x2="-144.78" y2="325.12" width="0.4064" layer="94"/>
<wire x1="-144.78" y1="325.12" x2="-144.78" y2="318.77" width="0.4064" layer="94"/>
<wire x1="-144.78" y1="318.77" x2="-149.86" y2="318.77" width="0.4064" layer="94"/>
<wire x1="-149.86" y1="318.77" x2="-154.94" y2="318.77" width="0.4064" layer="94"/>
<wire x1="-154.94" y1="325.12" x2="-154.94" y2="318.77" width="0.4064" layer="94"/>
<wire x1="-149.86" y1="325.12" x2="-149.86" y2="327.66" width="0.4064" layer="94"/>
<wire x1="-149.86" y1="318.77" x2="-149.86" y2="316.23" width="0.4064" layer="94"/>
<wire x1="-141.605" y1="321.945" x2="-140.335" y2="321.945" width="0.1524" layer="94"/>
<wire x1="-139.065" y1="321.945" x2="-137.795" y2="321.945" width="0.1524" layer="94"/>
<wire x1="-136.525" y1="321.945" x2="-135.255" y2="321.945" width="0.1524" layer="94"/>
<text x="-148.844" y="316.23" size="1.778" layer="95">A2</text>
<text x="-148.844" y="325.882" size="1.778" layer="95">A1</text>
<text x="-137.795" y="317.5" size="1.778" layer="95">11</text>
<text x="-139.7" y="325.755" size="1.778" layer="95">14</text>
<text x="-131.445" y="325.755" size="1.778" layer="95">12</text>
<text x="-152.4" y="332.74" size="1.778" layer="95" rot="R180">RE106</text>
<text x="-152.4" y="330.2" size="1.778" layer="95" rot="R180">3TX7005</text>
<wire x1="-144.145" y1="321.945" x2="-142.875" y2="321.945" width="0.1524" layer="94"/>
<text x="-119.38" y="302.26" size="2.1844" layer="95">SETTINGS:</text>
<text x="-119.38" y="297.18" size="1.778" layer="95">FUNCTION: "A" </text>
<text x="-119.38" y="294.64" size="1.778" layer="95">TIME: 5sec</text>
<wire x1="-208.28" y1="345.44" x2="-208.28" y2="284.48" width="0.4064" layer="97" style="longdash"/>
<wire x1="-208.28" y1="284.48" x2="-167.64" y2="284.48" width="0.4064" layer="97" style="longdash"/>
<wire x1="-167.64" y1="284.48" x2="-167.64" y2="345.44" width="0.4064" layer="97" style="longdash"/>
<wire x1="-167.64" y1="345.44" x2="-208.28" y2="345.44" width="0.4064" layer="97" style="longdash"/>
<wire x1="-53.34" y1="304.8" x2="-53.34" y2="297.18" width="0.3048" layer="94"/>
<wire x1="-53.34" y1="297.18" x2="-53.34" y2="289.56" width="0.3048" layer="94"/>
<wire x1="-53.34" y1="289.56" x2="-53.34" y2="271.78" width="0.3048" layer="94"/>
<wire x1="10.16" y1="271.78" x2="2.54" y2="271.78" width="0.3048" layer="94"/>
<wire x1="2.54" y1="271.78" x2="-15.24" y2="271.78" width="0.3048" layer="94"/>
<wire x1="-15.24" y1="271.78" x2="-35.56" y2="271.78" width="0.3048" layer="94"/>
<wire x1="-35.56" y1="271.78" x2="-45.72" y2="271.78" width="0.3048" layer="94"/>
<wire x1="-45.72" y1="271.78" x2="-53.34" y2="271.78" width="0.3048" layer="94"/>
<wire x1="-53.34" y1="289.56" x2="-35.56" y2="289.56" width="0.3048" layer="94"/>
<wire x1="-22.86" y1="289.56" x2="2.54" y2="289.56" width="0.3048" layer="94"/>
<wire x1="-53.34" y1="297.18" x2="-35.56" y2="297.18" width="0.1524" layer="94"/>
<wire x1="-35.56" y1="297.18" x2="-35.56" y2="289.56" width="0.1524" layer="94"/>
<wire x1="-35.56" y1="289.56" x2="-22.86" y2="289.56" width="0.3048" layer="94"/>
<wire x1="-22.86" y1="289.56" x2="-22.86" y2="297.18" width="0.1524" layer="94"/>
<wire x1="-22.86" y1="297.18" x2="2.54" y2="297.18" width="0.1524" layer="94"/>
<wire x1="2.54" y1="297.18" x2="2.54" y2="289.56" width="0.1524" layer="94"/>
<wire x1="2.54" y1="289.56" x2="10.16" y2="289.56" width="0.3048" layer="94"/>
<wire x1="-45.72" y1="271.78" x2="-45.72" y2="279.4" width="0.1524" layer="94"/>
<wire x1="-45.72" y1="279.4" x2="-35.56" y2="279.4" width="0.1524" layer="94"/>
<wire x1="-35.56" y1="279.4" x2="-35.56" y2="271.78" width="0.1524" layer="94"/>
<wire x1="-15.24" y1="271.78" x2="-15.24" y2="279.4" width="0.1524" layer="94"/>
<wire x1="-15.24" y1="279.4" x2="2.54" y2="279.4" width="0.1524" layer="94"/>
<wire x1="2.54" y1="279.4" x2="2.54" y2="271.78" width="0.1524" layer="94"/>
<wire x1="-45.72" y1="289.56" x2="-45.72" y2="279.4" width="0.1524" layer="97"/>
<wire x1="-22.86" y1="289.56" x2="-22.86" y2="271.78" width="0.1524" layer="97"/>
<text x="-61.0235" y="297.2435" size="2.54" layer="96">U</text>
<text x="-40.7035" y="304.8635" size="1.6764" layer="96">CRM91 "A" - DELAYED START</text>
<text x="-49.5935" y="279.4635" size="1.6764" layer="96">t</text>
<text x="-19.3675" y="279.7175" size="1.6764" layer="96">t</text>
<wire x1="-62.992" y1="282.702" x2="-55.372" y2="282.702" width="0.1524" layer="97"/>
<wire x1="-55.372" y1="282.702" x2="-55.372" y2="278.638" width="0.1524" layer="97"/>
<wire x1="-55.372" y1="278.638" x2="-62.992" y2="278.638" width="0.1524" layer="97"/>
<wire x1="-62.992" y1="278.638" x2="-62.992" y2="282.702" width="0.1524" layer="97"/>
<wire x1="-62.992" y1="278.638" x2="-55.372" y2="282.702" width="0.1524" layer="97"/>
<wire x1="-15.24" y1="289.56" x2="-15.24" y2="279.4" width="0.1524" layer="97"/>
<wire x1="-45.72" y1="276.86" x2="-45.72" y2="271.78" width="0.1524" layer="97"/>
<wire x1="-66.04" y1="312.42" x2="17.78" y2="312.42" width="0.1524" layer="97"/>
<wire x1="17.78" y1="312.42" x2="17.78" y2="264.16" width="0.1524" layer="97"/>
<wire x1="17.78" y1="264.16" x2="-66.04" y2="264.16" width="0.1524" layer="97"/>
<wire x1="-66.04" y1="264.16" x2="-66.04" y2="312.42" width="0.1524" layer="97"/>
</plain>
<instances>
<instance part="U$2" gate="G$1" x="127" y="259.715"/>
<instance part="U$4" gate="PE" x="-201.93" y="472.44" rot="MR0"/>
<instance part="V5" gate="G$1" x="-225.7425" y="480.3775" rot="MR180"/>
<instance part="V9" gate="G$1" x="-294.3225" y="306.07"/>
<instance part="V7" gate="G$1" x="-294.3225" y="276.86" smashed="yes" rot="MR180">
<attribute name="VALUE" x="-294.005" y="277.622" size="1.524" layer="96" rot="MR180"/>
</instance>
<instance part="D16" gate="G$1" x="-196.5325" y="523.5575" smashed="yes" rot="R90"/>
<instance part="D20" gate="G$1" x="-190.1825" y="523.5575" smashed="yes" rot="R270"/>
<instance part="S105" gate="G$1" x="-201.6125" y="521.0175" smashed="yes" rot="MR0"/>
<instance part="D23" gate="G$1" x="-173.6725" y="523.5575" smashed="yes" rot="MR90"/>
<instance part="D24" gate="G$1" x="-180.0225" y="523.5575" smashed="yes" rot="MR270"/>
<instance part="S106" gate="G$1" x="-168.5925" y="521.0175" smashed="yes"/>
<instance part="RE4" gate="B" x="-111.4425" y="512.1275" rot="R180"/>
<instance part="S7" gate="G$1" x="-131.7625" y="515.9375" smashed="yes" rot="MR0"/>
<instance part="S8" gate="G$1" x="-124.1425" y="515.9375" smashed="yes" rot="MR0"/>
<instance part="U$47" gate="GND" x="-152.0825" y="484.1875"/>
<instance part="RE1200" gate="B" x="-200.66" y="392.43"/>
<instance part="RE1" gate="B" x="-185.42" y="392.43"/>
<instance part="U$73" gate="G$1" x="-246.38" y="438.15" smashed="yes"/>
<instance part="GND2" gate="GND" x="-260.35" y="420.37"/>
<instance part="GND1" gate="GND" x="-158.75" y="407.67"/>
<instance part="U$1" gate="PE" x="-284.48" y="561.086" rot="MR0"/>
<instance part="U$70" gate="PE" x="-273.05" y="297.18" rot="MR0"/>
<instance part="GND14" gate="1" x="7.62" y="533.4"/>
<instance part="RTERM2" gate="G$1" x="186.055" y="480.695" smashed="yes" rot="R90">
<attribute name="NAME" x="184.5564" y="476.885" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="189.357" y="474.345" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="RTERM3" gate="G$1" x="186.055" y="460.375" smashed="yes" rot="R90">
<attribute name="NAME" x="184.5564" y="456.565" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="189.357" y="454.025" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="V23" gate="G$1" x="-53.34" y="548.64" smashed="yes">
<attribute name="VALUE" x="-52.705" y="547.878" size="1.524" layer="96"/>
</instance>
<instance part="V76" gate="G$1" x="-53.34" y="508" smashed="yes">
<attribute name="VALUE" x="-52.705" y="507.238" size="1.524" layer="96"/>
</instance>
<instance part="V90" gate="G$1" x="96.52" y="378.46" rot="R90"/>
<instance part="V91" gate="G$1" x="12.7" y="373.38" rot="R90"/>
<instance part="GND3" gate="1" x="154.305" y="453.39"/>
<instance part="GND4" gate="1" x="154.305" y="471.17"/>
<instance part="JP_3" gate="G$1" x="0" y="464.82" rot="R90"/>
<instance part="P_1" gate="G$1" x="0" y="447.04" smashed="yes" rot="R270">
<attribute name="VALUE" x="1.143" y="451.231" size="1.778" layer="96" align="bottom-center"/>
<attribute name="NAME" x="1.27" y="453.898" size="1.778" layer="95" align="bottom-center"/>
</instance>
<instance part="JP_2" gate="G$1" x="53.34" y="464.82" rot="R90"/>
<instance part="JP_1" gate="G$1" x="-2.54" y="403.86" rot="R270"/>
<instance part="P_2" gate="G$1" x="0" y="434.34" smashed="yes" rot="R270">
<attribute name="VALUE" x="1.143" y="438.531" size="1.778" layer="96" align="bottom-center"/>
<attribute name="NAME" x="1.27" y="441.198" size="1.778" layer="95" align="bottom-center"/>
</instance>
<instance part="P_3" gate="G$1" x="0" y="421.64" smashed="yes" rot="R270">
<attribute name="VALUE" x="1.143" y="425.831" size="1.778" layer="96" align="bottom-center"/>
<attribute name="NAME" x="1.27" y="428.498" size="1.778" layer="95" align="bottom-center"/>
</instance>
<instance part="L_1" gate="G$1" x="30.48" y="447.04" smashed="yes" rot="R270">
<attribute name="VALUE" x="31.623" y="451.231" size="1.778" layer="96" align="bottom-center"/>
<attribute name="NAME" x="31.75" y="453.898" size="1.778" layer="95" align="bottom-center"/>
</instance>
<instance part="X_2" gate="G$1" x="55.88" y="449.58" smashed="yes" rot="R270">
<attribute name="VALUE" x="57.277" y="453.771" size="1.778" layer="96" align="bottom-center"/>
<attribute name="NAME" x="57.15" y="456.438" size="1.778" layer="95" align="bottom-center"/>
</instance>
<instance part="X_4" gate="G$1" x="55.88" y="434.34" smashed="yes" rot="R270">
<attribute name="VALUE" x="57.277" y="438.531" size="1.778" layer="96" align="bottom-center"/>
<attribute name="NAME" x="57.15" y="441.198" size="1.778" layer="95" align="bottom-center"/>
</instance>
<instance part="JP_4" gate="G$1" x="35.56" y="393.7" rot="R270"/>
<instance part="GND8" gate="1" x="88.9" y="414.02" rot="MR270"/>
<instance part="U$30" gate="G$1" x="-30.48" y="449.58"/>
<instance part="U$28" gate="+24V" x="-30.48" y="439.42" rot="MR0"/>
<instance part="U$33" gate="+24V" x="-30.48" y="426.72" rot="MR0"/>
<instance part="V38" gate="G$1" x="96.52" y="449.58" rot="R180"/>
<instance part="V28" gate="G$1" x="96.52" y="434.34" rot="R180"/>
<instance part="FA2" gate="G$1" x="83.82" y="388.62"/>
<instance part="U$34" gate="G$1" x="83.82" y="383.54" rot="R180"/>
<instance part="GND5" gate="1" x="101.6" y="393.7" rot="MR0"/>
<instance part="GND6" gate="1" x="12.7" y="533.4"/>
<instance part="V10" gate="G$1" x="181.61" y="593.725" smashed="yes" rot="R270">
<attribute name="VALUE" x="180.848" y="593.09" size="1.27" layer="96" rot="R270"/>
</instance>
<instance part="V25" gate="G$1" x="156.21" y="593.725" smashed="yes" rot="R270">
<attribute name="VALUE" x="155.448" y="593.09" size="1.524" layer="96" rot="R270"/>
</instance>
<instance part="V26" gate="G$1" x="168.91" y="593.725" smashed="yes" rot="R270">
<attribute name="VALUE" x="169.672" y="593.09" size="1.27" layer="96" rot="MR270"/>
</instance>
<instance part="V1" gate="G$1" x="-294.5765" y="325.12" rot="MR180"/>
<instance part="FA15" gate="G$1" x="-172.72" y="373.38" smashed="yes" rot="MR180">
<attribute name="NAME" x="-175.895" y="379.603" size="1.524" layer="95" rot="MR180"/>
<attribute name="VALUE" x="-175.895" y="376.936" size="1.524" layer="96" rot="MR180"/>
</instance>
<instance part="U$81" gate="G$1" x="-162.56" y="373.38" rot="MR90"/>
<instance part="S9" gate="G$1" x="-180.34" y="337.82" smashed="yes" rot="MR90"/>
<instance part="S10" gate="G$1" x="-200.66" y="337.82" smashed="yes" rot="R270"/>
<instance part="Z3" gate="G$1" x="-187.96" y="292.1" smashed="yes" rot="R270">
<attribute name="NAME" x="-182.753" y="287.655" size="2.032" layer="95" rot="R90"/>
</instance>
<instance part="U$60" gate="GND" x="-203.2" y="292.1" rot="MR90"/>
<instance part="GND7" gate="1" x="55.88" y="297.18"/>
<instance part="GND10" gate="GND" x="66.04" y="292.1"/>
<instance part="RE2" gate="B" x="-93.98" y="320.04"/>
<instance part="RE3" gate="B" x="-134.62" y="320.04"/>
<instance part="GND9" gate="GND" x="-149.86" y="307.34"/>
<instance part="GND11" gate="GND" x="-109.22" y="307.34"/>
</instances>
<busses>
<bus name="L1,L2,L3,N,PE">
<segment>
<wire x1="-185.1025" y1="480.3775" x2="-210.5025" y2="480.3775" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="L1,L2,L3">
<segment>
<wire x1="-276.86" y1="477.52" x2="-276.86" y2="558.8" width="0.762" layer="92"/>
<wire x1="-276.86" y1="558.8" x2="-271.145" y2="558.8635" width="0.762" layer="92"/>
</segment>
<segment>
<wire x1="-273.05" y1="325.12" x2="-279.4" y2="325.12" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="T1,T2,T3">
<segment>
<wire x1="-185.1025" y1="486.7275" x2="-185.1025" y2="480.3775" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="HP4-4,IGN,ELMOT,1IN,14/24,PWR_BACKUP">
<segment>
<wire x1="-83.82" y1="276.86" x2="-83.82" y2="561.34" width="0.762" layer="92"/>
<wire x1="-83.82" y1="276.86" x2="-279.4" y2="276.86" width="0.762" layer="92"/>
<wire x1="-279.4" y1="276.86" x2="-279.4" y2="274.32" width="0.762" layer="92"/>
<wire x1="-279.4" y1="274.32" x2="-297.18" y2="274.32" width="0.762" layer="92"/>
<wire x1="-297.18" y1="274.32" x2="-297.18" y2="271.78" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="B$2">
<segment>
<wire x1="-276.86" y1="477.52" x2="-238.76" y2="477.52" width="0.762" layer="92"/>
</segment>
<segment>
<wire x1="-38.1" y1="548.64" x2="25.4" y2="548.64" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="L1,L2,L3,PE">
<segment>
<wire x1="-254" y1="314.325" x2="-254" y2="306.07" width="0.762" layer="92"/>
<wire x1="-254" y1="306.07" x2="-279.4" y2="306.07" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="B$1">
<segment>
<wire x1="-250.19" y1="369.57" x2="-209.55" y2="369.57" width="0.762" layer="92"/>
<wire x1="-250.19" y1="369.57" x2="-250.19" y2="358.14" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="ELMOT,X23.1.05,COMPRESSOR,X23.1.57,X23.1.08,X23.1.22,X23.1.32,X23.1.33,MOTOR,START,IG,GND,+24,X23.1.02,X23.1.23">
<segment>
<wire x1="-38.1" y1="508" x2="-35.56" y2="508" width="0.762" layer="92"/>
<wire x1="-35.56" y1="508" x2="114.3" y2="508" width="0.762" layer="92"/>
<wire x1="-35.56" y1="508" x2="-35.56" y2="370.84" width="0.762" layer="92"/>
<wire x1="-35.56" y1="370.84" x2="175.26" y2="370.84" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="FAIL[1..4],CON[1..8],CHK[1..10],VENTCOOL[0..2],TOPZEH,POWERTMC,ALT,H/LSWITCH2,VENTILATION,EMERGENCY,HEAT,VENTSTART,MOTOR,PUMP,PWR[1..6],CLAMP[1..2],COOL,COOLSTART,MTEMP,STARTACCU,D+,COMPRESSOR,SW[1..2],R1,HEATTEMP,HP,LP,FAN_REG,FAN_100%,FAN_50%,HEATSTART,FAILHEAT,+5V,X23.1.55,X23.1.56,X23.1.53,X23.1.58,X23.1.31,X23.1.26,X23.1.25,X23.1.22,X23.1.23,X23.1.09,X23.1.24,X23.1.42,X23.1.37,X23.1.02,X23.1.40,X23.1.52,X23.1.61,X23.1.62,X23.1.50,X23.1.51,X23.1.46,X23.1.47,X23.1.48,X23.1.49,X23.1.57,X23.1.05,X23.2.36,X23.2.37,X23.1.08,X23.1.32,X23.1.33,X23.1.03,X23.1.13,KL30F,KL31,MOTOR_IG,START,ALT-N,ALT-P,DIAG-P,DIAG-N,CAN0-L,CAN0-H,SHIELD,X23.2.04,X23.2.05,X23.2.06,X23.2.07,COMPRESSOR,MOTOR,ELMOT,KL30F">
<segment>
<wire x1="-58.42" y1="320.04" x2="5.08" y2="320.04" width="0.762" layer="92"/>
<wire x1="182.88" y1="528.32" x2="-58.42" y2="528.32" width="0.762" layer="92"/>
<wire x1="-58.42" y1="528.32" x2="-58.42" y2="320.04" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="GND,IG">
<segment>
<wire x1="-5.08" y1="345.44" x2="149.86" y2="345.44" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="CAN0-H,CAN0-L,GND,START,KL30F,IG">
<segment>
<wire x1="203.2" y1="332.74" x2="-22.86" y2="332.74" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="T7A,T7B">
<segment>
<wire x1="181.61" y1="578.485" x2="181.61" y2="573.405" width="0.762" layer="92"/>
</segment>
<segment>
<wire x1="156.21" y1="578.485" x2="156.21" y2="573.405" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="VENTSTART,GND,PRESSFILTER">
<segment>
<wire x1="168.91" y1="578.485" x2="168.91" y2="576.58" width="0.762" layer="92"/>
</segment>
</bus>
</busses>
<nets>
<net name="PE" class="0">
<segment>
<wire x1="-203.835" y1="480.3775" x2="-201.93" y2="478.4725" width="0.1524" layer="91"/>
<wire x1="-201.93" y1="478.4725" x2="-201.93" y2="477.52" width="0.1524" layer="91"/>
<pinref part="U$4" gate="PE" pin="PE"/>
</segment>
<segment>
<wire x1="-264.414" y1="568.96" x2="-284.48" y2="568.96" width="0.1524" layer="91"/>
<wire x1="-284.48" y1="568.96" x2="-284.48" y2="566.166" width="0.1524" layer="91"/>
<pinref part="U$1" gate="PE" pin="PE"/>
</segment>
<segment>
<wire x1="-275.59" y1="306.07" x2="-273.05" y2="303.53" width="0.1524" layer="91"/>
<wire x1="-273.05" y1="303.53" x2="-273.05" y2="302.26" width="0.1524" layer="91"/>
<pinref part="U$70" gate="PE" pin="PE"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<wire x1="-152.0825" y1="491.8075" x2="-152.0825" y2="489.2675" width="0.1524" layer="91"/>
<pinref part="U$47" gate="GND" pin="GND"/>
</segment>
<segment>
<wire x1="-260.35" y1="433.07" x2="-260.35" y2="427.99" width="0.1524" layer="91"/>
<wire x1="-260.35" y1="427.99" x2="-209.55" y2="427.99" width="0.1524" layer="91"/>
<wire x1="-209.55" y1="433.07" x2="-209.55" y2="427.99" width="0.1524" layer="91"/>
<wire x1="-260.35" y1="427.99" x2="-260.35" y2="425.45" width="0.1524" layer="91"/>
<junction x="-260.35" y="427.99"/>
<pinref part="GND2" gate="GND" pin="GND"/>
</segment>
<segment>
<wire x1="-165.1" y1="430.53" x2="-165.1" y2="416.56" width="0.1524" layer="91"/>
<wire x1="-165.1" y1="416.56" x2="-158.75" y2="416.56" width="0.1524" layer="91"/>
<wire x1="-158.75" y1="416.56" x2="-158.75" y2="430.53" width="0.1524" layer="91"/>
<pinref part="GND1" gate="GND" pin="GND"/>
<wire x1="-158.75" y1="416.56" x2="-158.75" y2="412.75" width="0.1524" layer="91"/>
<junction x="-158.75" y="416.56"/>
</segment>
<segment>
<pinref part="JP_4" gate="G$1" pin="1"/>
<pinref part="GND8" gate="1" pin="GND"/>
<wire x1="43.18" y1="414.02" x2="86.36" y2="414.02" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP_4" gate="G$1" pin="2"/>
<pinref part="GND5" gate="1" pin="GND"/>
<wire x1="43.18" y1="408.94" x2="101.6" y2="408.94" width="0.1524" layer="91"/>
<wire x1="101.6" y1="408.94" x2="101.6" y2="396.24" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP_1" gate="G$1" pin="9"/>
<wire x1="-7.62" y1="396.24" x2="-7.62" y2="335.28" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="332.74" x2="-7.62" y2="335.28" width="0.1524" layer="91"/>
<label x="-7.62" y="335.28" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="187.96" y1="332.74" x2="190.5" y2="335.28" width="0.3048" layer="91"/>
<wire x1="190.5" y1="335.28" x2="190.5" y2="341.63" width="0.3048" layer="91"/>
<label x="189.23" y="335.28" size="1.778" layer="95" rot="R90"/>
<wire x1="190.5" y1="341.63" x2="190.5" y2="372.745" width="0.3048" layer="91"/>
<wire x1="187.0075" y1="341.63" x2="190.5" y2="341.63" width="0.1524" layer="91"/>
<junction x="190.5" y="341.63"/>
</segment>
<segment>
<pinref part="JP_1" gate="G$1" pin="7"/>
<wire x1="-2.54" y1="396.24" x2="-2.54" y2="347.98" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="345.44" x2="-2.54" y2="347.98" width="0.1524" layer="91"/>
<label x="-2.54" y="347.98" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="149.86" y1="345.44" x2="152.4" y2="347.98" width="0.1524" layer="91"/>
<wire x1="152.4" y1="347.98" x2="152.4" y2="447.802" width="0.1524" layer="91"/>
<wire x1="152.4" y1="447.802" x2="194.818" y2="447.802" width="0.1524" layer="91"/>
<label x="153.67" y="448.056" size="1.778" layer="95"/>
<label x="152.4" y="347.98" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="168.91" y1="576.58" x2="170.815" y2="574.675" width="0.1524" layer="91"/>
<wire x1="170.815" y1="574.675" x2="173.99" y2="574.675" width="0.1524" layer="91"/>
<wire x1="173.99" y1="574.675" x2="173.99" y2="567.055" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="-195.58" y1="292.1" x2="-198.12" y2="292.1" width="0.1524" layer="91"/>
<pinref part="Z3" gate="G$1" pin="1"/>
<pinref part="U$60" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="GND7" gate="1" pin="GNDA"/>
<wire x1="55.88" y1="299.72" x2="55.88" y2="307.34" width="0.1524" layer="91"/>
<pinref part="GND10" gate="GND" pin="GND"/>
<wire x1="55.88" y1="307.34" x2="66.04" y2="307.34" width="0.1524" layer="91"/>
<wire x1="66.04" y1="307.34" x2="66.04" y2="297.18" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND9" gate="GND" pin="GND"/>
<wire x1="-149.86" y1="312.42" x2="-149.86" y2="316.23" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="GND11" gate="GND" pin="GND"/>
<wire x1="-109.22" y1="312.42" x2="-109.22" y2="316.23" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ELMOT" class="0">
<segment>
<wire x1="-83.82" y1="455.6125" x2="-87.3125" y2="452.12" width="0.1524" layer="91"/>
<label x="-87.3125" y="454.3425" size="1.778" layer="95" rot="R180"/>
<wire x1="-158.75" y1="445.77" x2="-158.75" y2="452.12" width="0.1524" layer="91"/>
<wire x1="-165.1" y1="452.12" x2="-165.1" y2="445.77" width="0.1524" layer="91"/>
<wire x1="-165.1" y1="452.12" x2="-158.75" y2="452.12" width="0.1524" layer="91"/>
<wire x1="-158.75" y1="452.12" x2="-87.3125" y2="452.12" width="0.1524" layer="91"/>
<junction x="-158.75" y="452.12"/>
<label x="-165.1" y="452.4375" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="2.54" y1="505.46" x2="5.08" y2="508" width="0.1524" layer="91"/>
<label x="2.54" y="505.46" size="1.778" layer="95" rot="MR270"/>
</segment>
<segment>
<wire x1="86.36" y1="528.32" x2="83.82" y2="525.78" width="0.1524" layer="91"/>
<wire x1="83.82" y1="525.78" x2="83.82" y2="510.54" width="0.1524" layer="91"/>
<wire x1="83.82" y1="510.54" x2="81.28" y2="508" width="0.1524" layer="91"/>
<label x="83.82" y="510.54" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="HP4-4" class="0">
<segment>
<wire x1="-83.82" y1="389.89" x2="-88.9" y2="384.81" width="0.1524" layer="91"/>
<wire x1="-88.9" y1="384.81" x2="-185.42" y2="384.81" width="0.1524" layer="91"/>
<pinref part="RE1200" gate="B" pin="S"/>
<wire x1="-205.74" y1="397.51" x2="-208.28" y2="397.51" width="0.1524" layer="91"/>
<wire x1="-208.28" y1="397.51" x2="-208.28" y2="384.81" width="0.1524" layer="91"/>
<wire x1="-208.28" y1="384.81" x2="-185.42" y2="384.81" width="0.1524" layer="91"/>
<pinref part="RE1" gate="B" pin="P"/>
<wire x1="-185.42" y1="384.81" x2="-185.42" y2="389.89" width="0.1524" layer="91"/>
<junction x="-185.42" y="384.81"/>
<label x="-89.535" y="387.35" size="1.778" layer="95" rot="R180"/>
<label x="-182.88" y="386.08" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-86.36" y1="558.8" x2="-152.0825" y2="558.8" width="0.1524" layer="91"/>
<wire x1="-152.0825" y1="540.0675" x2="-152.0825" y2="558.8" width="0.1524" layer="91"/>
<label x="-93.853" y="559.562" size="1.778" layer="95"/>
<wire x1="-83.82" y1="561.34" x2="-86.36" y2="558.8" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<wire x1="-201.6125" y1="531.1775" x2="-201.6125" y2="532.4475" width="0.1524" layer="91"/>
<wire x1="-201.6125" y1="532.4475" x2="-196.5325" y2="532.4475" width="0.1524" layer="91"/>
<wire x1="-190.1825" y1="529.9075" x2="-190.1825" y2="528.6375" width="0.1524" layer="91"/>
<wire x1="-196.5325" y1="528.6375" x2="-196.5325" y2="529.9075" width="0.1524" layer="91"/>
<wire x1="-196.5325" y1="529.9075" x2="-196.5325" y2="532.4475" width="0.1524" layer="91"/>
<wire x1="-196.5325" y1="532.4475" x2="-196.5325" y2="537.5275" width="0.1524" layer="91"/>
<wire x1="-196.5325" y1="529.9075" x2="-190.1825" y2="529.9075" width="0.1524" layer="91"/>
<junction x="-196.5325" y="532.4475"/>
<junction x="-196.5325" y="529.9075"/>
<pinref part="S105" gate="G$1" pin="2"/>
<pinref part="D20" gate="G$1" pin="A"/>
<pinref part="D16" gate="G$1" pin="K"/>
</segment>
</net>
<net name="N$77" class="0">
<segment>
<wire x1="-201.6125" y1="515.9375" x2="-201.6125" y2="514.6675" width="0.1524" layer="91"/>
<wire x1="-201.6125" y1="514.6675" x2="-196.5325" y2="514.6675" width="0.1524" layer="91"/>
<wire x1="-190.1825" y1="517.2075" x2="-190.1825" y2="518.4775" width="0.1524" layer="91"/>
<wire x1="-196.5325" y1="518.4775" x2="-196.5325" y2="517.2075" width="0.1524" layer="91"/>
<wire x1="-196.5325" y1="517.2075" x2="-196.5325" y2="514.6675" width="0.1524" layer="91"/>
<wire x1="-196.5325" y1="517.2075" x2="-190.1825" y2="517.2075" width="0.1524" layer="91"/>
<wire x1="-196.5325" y1="514.6675" x2="-196.5325" y2="508.3175" width="0.1524" layer="91"/>
<wire x1="-196.5325" y1="508.3175" x2="-200.3425" y2="508.3175" width="0.1524" layer="91"/>
<wire x1="-200.3425" y1="508.3175" x2="-200.3425" y2="504.5075" width="0.1524" layer="91"/>
<wire x1="-200.3425" y1="504.5075" x2="-196.5325" y2="504.5075" width="0.1524" layer="91"/>
<wire x1="-196.5325" y1="504.5075" x2="-196.5325" y2="494.3475" width="0.1524" layer="91"/>
<junction x="-196.5325" y="517.2075"/>
<junction x="-196.5325" y="514.6675"/>
<pinref part="S105" gate="G$1" pin="1"/>
<pinref part="D20" gate="G$1" pin="K"/>
<pinref part="D16" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$79" class="0">
<segment>
<wire x1="-168.5925" y1="531.1775" x2="-168.5925" y2="532.4475" width="0.1524" layer="91"/>
<wire x1="-168.5925" y1="532.4475" x2="-173.6725" y2="532.4475" width="0.1524" layer="91"/>
<wire x1="-180.0225" y1="529.9075" x2="-180.0225" y2="528.6375" width="0.1524" layer="91"/>
<wire x1="-173.6725" y1="528.6375" x2="-173.6725" y2="529.9075" width="0.1524" layer="91"/>
<wire x1="-173.6725" y1="529.9075" x2="-173.6725" y2="532.4475" width="0.1524" layer="91"/>
<wire x1="-173.6725" y1="532.4475" x2="-173.6725" y2="537.5275" width="0.1524" layer="91"/>
<wire x1="-173.6725" y1="529.9075" x2="-180.0225" y2="529.9075" width="0.1524" layer="91"/>
<junction x="-173.6725" y="532.4475"/>
<junction x="-173.6725" y="529.9075"/>
<pinref part="S106" gate="G$1" pin="2"/>
<pinref part="D24" gate="G$1" pin="A"/>
<pinref part="D23" gate="G$1" pin="K"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<wire x1="-168.5925" y1="515.9375" x2="-168.5925" y2="514.6675" width="0.1524" layer="91"/>
<wire x1="-168.5925" y1="514.6675" x2="-173.6725" y2="514.6675" width="0.1524" layer="91"/>
<wire x1="-180.0225" y1="517.2075" x2="-180.0225" y2="518.4775" width="0.1524" layer="91"/>
<wire x1="-173.6725" y1="518.4775" x2="-173.6725" y2="517.2075" width="0.1524" layer="91"/>
<wire x1="-173.6725" y1="517.2075" x2="-173.6725" y2="514.6675" width="0.1524" layer="91"/>
<wire x1="-173.6725" y1="517.2075" x2="-180.0225" y2="517.2075" width="0.1524" layer="91"/>
<wire x1="-173.6725" y1="514.6675" x2="-173.6725" y2="508.3175" width="0.1524" layer="91"/>
<wire x1="-173.6725" y1="508.3175" x2="-177.4825" y2="508.3175" width="0.1524" layer="91"/>
<wire x1="-177.4825" y1="508.3175" x2="-177.4825" y2="504.5075" width="0.1524" layer="91"/>
<wire x1="-177.4825" y1="504.5075" x2="-173.6725" y2="504.5075" width="0.1524" layer="91"/>
<wire x1="-173.6725" y1="504.5075" x2="-173.6725" y2="494.3475" width="0.1524" layer="91"/>
<junction x="-173.6725" y="517.2075"/>
<junction x="-173.6725" y="514.6675"/>
<pinref part="S106" gate="G$1" pin="1"/>
<pinref part="D24" gate="G$1" pin="K"/>
<pinref part="D23" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$81" class="0">
<segment>
<wire x1="-152.0825" y1="528.6375" x2="-152.0825" y2="537.5275" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$82" class="0">
<segment>
<wire x1="-144.4625" y1="528.6375" x2="-144.4625" y2="537.5275" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$83" class="0">
<segment>
<wire x1="-152.0825" y1="504.5075" x2="-152.0825" y2="494.3475" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$84" class="0">
<segment>
<wire x1="-131.7625" y1="526.0975" x2="-131.7625" y2="528.6375" width="0.1524" layer="91"/>
<wire x1="-131.7625" y1="528.6375" x2="-131.7625" y2="537.5275" width="0.1524" layer="91"/>
<wire x1="-124.1425" y1="526.0975" x2="-124.1425" y2="528.6375" width="0.1524" layer="91"/>
<wire x1="-124.1425" y1="528.6375" x2="-131.7625" y2="528.6375" width="0.1524" layer="91"/>
<junction x="-131.7625" y="528.6375"/>
<pinref part="S7" gate="G$1" pin="2"/>
<pinref part="S8" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$90" class="0">
<segment>
<wire x1="-124.1425" y1="510.8575" x2="-124.1425" y2="494.3475" width="0.1524" layer="91"/>
<pinref part="S8" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$91" class="0">
<segment>
<wire x1="-131.7625" y1="510.8575" x2="-131.7625" y2="494.3475" width="0.1524" layer="91"/>
<pinref part="S7" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$92" class="0">
<segment>
<wire x1="-111.4425" y1="514.6675" x2="-111.4425" y2="537.5275" width="0.1524" layer="91"/>
<pinref part="RE4" gate="B" pin="P"/>
</segment>
</net>
<net name="N$93" class="0">
<segment>
<wire x1="-116.5225" y1="507.0475" x2="-117.7925" y2="507.0475" width="0.1524" layer="91"/>
<wire x1="-117.7925" y1="507.0475" x2="-117.7925" y2="494.3475" width="0.1524" layer="91"/>
<pinref part="RE4" gate="B" pin="O"/>
</segment>
</net>
<net name="N$94" class="0">
<segment>
<wire x1="-106.3625" y1="507.0475" x2="-105.0925" y2="507.0475" width="0.1524" layer="91"/>
<wire x1="-105.0925" y1="507.0475" x2="-105.0925" y2="494.3475" width="0.1524" layer="91"/>
<pinref part="RE4" gate="B" pin="S"/>
</segment>
</net>
<net name="N$95" class="0">
<segment>
<wire x1="-185.1025" y1="537.5275" x2="-185.1025" y2="508.3175" width="0.1524" layer="91"/>
<wire x1="-185.1025" y1="508.3175" x2="-188.9125" y2="508.3175" width="0.1524" layer="91"/>
<wire x1="-188.9125" y1="508.3175" x2="-188.9125" y2="504.5075" width="0.1524" layer="91"/>
<wire x1="-188.9125" y1="504.5075" x2="-185.1025" y2="504.5075" width="0.1524" layer="91"/>
<wire x1="-185.1025" y1="504.5075" x2="-185.1025" y2="494.3475" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$96" class="0">
<segment>
<wire x1="-196.5325" y1="540.0675" x2="-196.5325" y2="547.116" width="0.1524" layer="91"/>
</segment>
</net>
<net name="T12" class="0">
<segment>
<wire x1="-185.1025" y1="486.7275" x2="-196.5325" y2="486.7275" width="0.1524" layer="91"/>
<wire x1="-196.5325" y1="486.7275" x2="-196.5325" y2="491.8075" width="0.1524" layer="91"/>
</segment>
</net>
<net name="T13" class="0">
<segment>
<wire x1="-185.1025" y1="486.7275" x2="-185.1025" y2="491.8075" width="0.1524" layer="91"/>
</segment>
</net>
<net name="T14" class="0">
<segment>
<wire x1="-185.1025" y1="486.7275" x2="-173.6725" y2="486.7275" width="0.1524" layer="91"/>
<wire x1="-173.6725" y1="486.7275" x2="-173.6725" y2="491.8075" width="0.1524" layer="91"/>
</segment>
</net>
<net name="L8" class="0">
<segment>
<wire x1="-271.145" y1="558.8635" x2="-271.145" y2="563.9435" width="0.1524" layer="91"/>
<wire x1="-271.145" y1="563.9435" x2="-264.4775" y2="563.9435" width="0.1524" layer="91"/>
</segment>
</net>
<net name="L9" class="0">
<segment>
<wire x1="-271.145" y1="558.8635" x2="-264.4775" y2="558.8635" width="0.1524" layer="91"/>
</segment>
</net>
<net name="L13" class="0">
<segment>
<wire x1="-271.145" y1="558.8635" x2="-271.145" y2="553.7835" width="0.1524" layer="91"/>
<wire x1="-271.145" y1="553.7835" x2="-264.4775" y2="553.7835" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="-200.025" y1="563.9435" x2="-173.6725" y2="563.9435" width="0.1524" layer="91"/>
<wire x1="-173.6725" y1="540.0675" x2="-173.6725" y2="563.9435" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<wire x1="-196.5325" y1="553.7835" x2="-196.5325" y2="547.116" width="0.1524" layer="91"/>
<wire x1="-200.025" y1="553.7835" x2="-196.5325" y2="553.7835" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$46" class="0">
<segment>
<wire x1="-200.025" y1="558.8635" x2="-185.1025" y2="558.8635" width="0.1524" layer="91"/>
<wire x1="-185.1025" y1="540.0675" x2="-185.1025" y2="558.8635" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$49" class="0">
<segment>
<wire x1="-237.8075" y1="553.7835" x2="-230.505" y2="553.7835" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$52" class="0">
<segment>
<wire x1="-237.8075" y1="558.8635" x2="-230.505" y2="558.8635" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$55" class="0">
<segment>
<wire x1="-237.8075" y1="563.9435" x2="-230.505" y2="563.9435" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IGN" class="0">
<segment>
<wire x1="-124.1425" y1="491.8075" x2="-124.1425" y2="482.2825" width="0.1524" layer="91"/>
<wire x1="-83.82" y1="485.4575" x2="-86.995" y2="482.2825" width="0.1524" layer="91"/>
<wire x1="-124.1425" y1="482.2825" x2="-86.995" y2="482.2825" width="0.1524" layer="91"/>
<label x="-121.285" y="482.6" size="1.778" layer="95"/>
<label x="-87.9475" y="484.8225" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="T1" class="0">
<segment>
<wire x1="-251.46" y1="445.77" x2="-251.46" y2="463.55" width="0.1524" layer="91"/>
<wire x1="-251.46" y1="463.55" x2="-251.46" y2="474.98" width="0.1524" layer="91"/>
<wire x1="-200.66" y1="445.77" x2="-200.66" y2="463.55" width="0.1524" layer="91"/>
<wire x1="-200.66" y1="463.55" x2="-251.46" y2="463.55" width="0.1524" layer="91"/>
<junction x="-251.46" y="463.55"/>
<wire x1="-254" y1="477.52" x2="-251.46" y2="474.98" width="0.1524" layer="91"/>
<label x="-254" y="474.98" size="1.778" layer="95" rot="R270"/>
</segment>
</net>
<net name="T2" class="0">
<segment>
<wire x1="-243.84" y1="445.77" x2="-243.84" y2="461.01" width="0.1524" layer="91"/>
<wire x1="-243.84" y1="461.01" x2="-243.84" y2="474.98" width="0.1524" layer="91"/>
<wire x1="-193.04" y1="445.77" x2="-193.04" y2="461.01" width="0.1524" layer="91"/>
<wire x1="-193.04" y1="461.01" x2="-243.84" y2="461.01" width="0.1524" layer="91"/>
<junction x="-243.84" y="461.01"/>
<wire x1="-246.38" y1="477.52" x2="-243.84" y2="474.98" width="0.1524" layer="91"/>
<label x="-246.38" y="474.98" size="1.778" layer="95" rot="R270"/>
</segment>
</net>
<net name="T3" class="0">
<segment>
<wire x1="-236.22" y1="445.77" x2="-236.22" y2="458.47" width="0.1524" layer="91"/>
<wire x1="-236.22" y1="458.47" x2="-236.22" y2="474.98" width="0.1524" layer="91"/>
<wire x1="-185.42" y1="445.77" x2="-185.42" y2="458.47" width="0.1524" layer="91"/>
<wire x1="-185.42" y1="458.47" x2="-236.22" y2="458.47" width="0.1524" layer="91"/>
<junction x="-236.22" y="458.47"/>
<wire x1="-238.76" y1="477.52" x2="-236.22" y2="474.98" width="0.1524" layer="91"/>
<label x="-238.76" y="474.98" size="1.778" layer="95" rot="R270"/>
</segment>
</net>
<net name="L1" class="0">
<segment>
<wire x1="-185.42" y1="417.83" x2="-185.42" y2="430.53" width="0.1524" layer="91"/>
<wire x1="-251.46" y1="430.53" x2="-251.46" y2="417.83" width="0.1524" layer="91"/>
<wire x1="-185.42" y1="417.83" x2="-251.46" y2="417.83" width="0.1524" layer="91"/>
<junction x="-251.46" y="417.83"/>
<wire x1="-251.46" y1="417.83" x2="-251.46" y2="372.11" width="0.1524" layer="91"/>
<wire x1="-248.92" y1="369.57" x2="-251.46" y2="372.11" width="0.1524" layer="91"/>
<label x="-252.73" y="372.11" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="-210.82" y1="369.57" x2="-213.36" y2="372.11" width="0.1524" layer="91"/>
<wire x1="-213.36" y1="372.11" x2="-213.36" y2="388.62" width="0.1524" layer="91"/>
<label x="-214.63" y="372.11" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="-254" y1="314.325" x2="-264.795" y2="314.325" width="0.1524" layer="91"/>
<wire x1="-264.795" y1="314.325" x2="-264.795" y2="343.535" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="-257.81" y1="343.535" x2="-257.81" y2="323.5325" width="0.1524" layer="91"/>
<wire x1="-257.81" y1="323.5325" x2="-273.05" y2="323.5325" width="0.1524" layer="91"/>
<wire x1="-273.05" y1="323.5325" x2="-273.05" y2="325.12" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="-250.19" y1="358.14" x2="-261.62" y2="358.14" width="0.1524" layer="91"/>
<wire x1="-261.62" y1="358.14" x2="-261.62" y2="350.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="L2" class="0">
<segment>
<wire x1="-193.04" y1="420.37" x2="-193.04" y2="430.53" width="0.1524" layer="91"/>
<wire x1="-243.84" y1="430.53" x2="-243.84" y2="420.37" width="0.1524" layer="91"/>
<wire x1="-193.04" y1="420.37" x2="-243.84" y2="420.37" width="0.1524" layer="91"/>
<junction x="-243.84" y="420.37"/>
<wire x1="-241.3" y1="369.57" x2="-243.84" y2="372.11" width="0.1524" layer="91"/>
<wire x1="-243.84" y1="372.11" x2="-243.84" y2="420.37" width="0.1524" layer="91"/>
<label x="-245.11" y="372.11" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="-215.9" y1="369.57" x2="-218.44" y2="372.11" width="0.1524" layer="91"/>
<wire x1="-218.44" y1="372.11" x2="-218.44" y2="388.62" width="0.1524" layer="91"/>
<label x="-219.71" y="372.11" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="-254" y1="314.325" x2="-254" y2="343.535" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="-246.38" y1="343.535" x2="-246.38" y2="325.12" width="0.1524" layer="91"/>
<wire x1="-246.38" y1="325.12" x2="-273.05" y2="325.12" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="-250.19" y1="358.14" x2="-250.19" y2="350.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="L3" class="0">
<segment>
<wire x1="-200.66" y1="422.91" x2="-200.66" y2="430.53" width="0.1524" layer="91"/>
<wire x1="-236.22" y1="430.53" x2="-236.22" y2="422.91" width="0.1524" layer="91"/>
<wire x1="-200.66" y1="422.91" x2="-236.22" y2="422.91" width="0.1524" layer="91"/>
<junction x="-236.22" y="422.91"/>
<wire x1="-233.68" y1="369.57" x2="-236.22" y2="372.11" width="0.1524" layer="91"/>
<wire x1="-236.22" y1="372.11" x2="-236.22" y2="422.91" width="0.1524" layer="91"/>
<label x="-237.49" y="372.11" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="-226.06" y1="369.57" x2="-228.6" y2="372.11" width="0.1524" layer="91"/>
<wire x1="-228.6" y1="372.11" x2="-228.6" y2="405.13" width="0.1524" layer="91"/>
<wire x1="-228.6" y1="405.13" x2="-218.44" y2="405.13" width="0.1524" layer="91"/>
<wire x1="-218.44" y1="405.13" x2="-218.44" y2="402.59" width="0.1524" layer="91"/>
<label x="-229.87" y="372.11" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="-254" y1="314.325" x2="-242.57" y2="314.325" width="0.1524" layer="91"/>
<wire x1="-242.57" y1="314.325" x2="-242.57" y2="343.535" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="-234.95" y1="343.535" x2="-234.95" y2="326.7075" width="0.1524" layer="91"/>
<wire x1="-234.95" y1="326.7075" x2="-273.05" y2="326.7075" width="0.1524" layer="91"/>
<wire x1="-273.05" y1="326.7075" x2="-273.05" y2="325.12" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="-250.19" y1="358.14" x2="-238.76" y2="358.14" width="0.1524" layer="91"/>
<wire x1="-238.76" y1="358.14" x2="-238.76" y2="350.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<wire x1="-228.6" y1="445.77" x2="-228.6" y2="448.31" width="0.1524" layer="91"/>
<wire x1="-228.6" y1="448.31" x2="-209.55" y2="448.31" width="0.1524" layer="91"/>
<wire x1="-209.55" y1="448.31" x2="-209.55" y2="443.23" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$107" class="0">
<segment>
<wire x1="-228.6" y1="430.53" x2="-228.6" y2="412.75" width="0.1524" layer="91"/>
<wire x1="-228.6" y1="412.75" x2="-193.04" y2="412.75" width="0.1524" layer="91"/>
<wire x1="-193.04" y1="397.51" x2="-193.04" y2="412.75" width="0.1524" layer="91"/>
<pinref part="RE1" gate="B" pin="S"/>
<wire x1="-193.04" y1="397.51" x2="-190.5" y2="397.51" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$109" class="0">
<segment>
<wire x1="-260.35" y1="453.39" x2="-177.8" y2="453.39" width="0.1524" layer="91"/>
<wire x1="-177.8" y1="453.39" x2="-177.8" y2="445.77" width="0.1524" layer="91"/>
<wire x1="-260.35" y1="453.39" x2="-260.35" y2="443.23" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$110" class="0">
<segment>
<wire x1="-177.8" y1="430.53" x2="-177.8" y2="397.51" width="0.1524" layer="91"/>
<pinref part="RE1" gate="B" pin="O"/>
<wire x1="-177.8" y1="397.51" x2="-180.34" y2="397.51" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PWR_BACKUP" class="0">
<segment>
<wire x1="-180.34" y1="373.38" x2="-200.66" y2="373.38" width="0.1524" layer="91"/>
<pinref part="RE1200" gate="B" pin="P"/>
<wire x1="-200.66" y1="389.89" x2="-200.66" y2="373.38" width="0.1524" layer="91"/>
<label x="-198.755" y="374.65" size="1.778" layer="95"/>
<wire x1="-200.66" y1="373.38" x2="-200.66" y2="358.14" width="0.1524" layer="91"/>
<junction x="-200.66" y="373.38"/>
<wire x1="-200.66" y1="358.14" x2="-215.9" y2="358.14" width="0.1524" layer="91"/>
<wire x1="-215.9" y1="358.14" x2="-215.9" y2="350.52" width="0.1524" layer="91"/>
</segment>
</net>
<net name="+24V" class="0">
<segment>
<pinref part="U$28" gate="+24V" pin="+24V"/>
<pinref part="P_2" gate="G$1" pin="P$2"/>
<wire x1="-30.48" y1="436.88" x2="-30.48" y2="434.34" width="0.1524" layer="91"/>
<wire x1="-30.48" y1="434.34" x2="-2.54" y2="434.34" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$33" gate="+24V" pin="+24V"/>
<pinref part="P_3" gate="G$1" pin="P$2"/>
<wire x1="-30.48" y1="424.18" x2="-30.48" y2="421.64" width="0.1524" layer="91"/>
<wire x1="-30.48" y1="421.64" x2="-2.54" y2="421.64" width="0.1524" layer="91"/>
</segment>
</net>
<net name="AKU" class="0">
<segment>
<pinref part="U$30" gate="G$1" pin="AKU"/>
<pinref part="P_1" gate="G$1" pin="P$2"/>
<wire x1="-30.48" y1="449.58" x2="-30.48" y2="447.04" width="0.1524" layer="91"/>
<wire x1="-30.48" y1="447.04" x2="-2.54" y2="447.04" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$34" gate="G$1" pin="AKU"/>
<pinref part="FA2" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="-162.56" y1="373.38" x2="-165.1" y2="373.38" width="0.1524" layer="91"/>
<pinref part="FA15" gate="G$1" pin="2A"/>
<pinref part="FA15" gate="G$1" pin="2B"/>
<pinref part="FA15" gate="G$1" pin="2C"/>
<pinref part="FA15" gate="G$1" pin="2D"/>
<pinref part="U$81" gate="G$1" pin="AKU"/>
</segment>
</net>
<net name="COMPRESSOR" class="0">
<segment>
<pinref part="JP_3" gate="G$1" pin="1"/>
<wire x1="-12.7" y1="528.32" x2="-15.24" y2="525.78" width="0.1524" layer="91"/>
<wire x1="-15.24" y1="525.78" x2="-15.24" y2="472.44" width="0.1524" layer="91"/>
<label x="-15.24" y="525.78" size="1.778" layer="95" rot="MR270"/>
</segment>
<segment>
<pinref part="S9" gate="G$1" pin="2"/>
<wire x1="-170.18" y1="337.82" x2="-149.86" y2="337.82" width="0.1524" layer="91"/>
<wire x1="-149.86" y1="337.82" x2="-142.24" y2="337.82" width="0.1524" layer="91"/>
<wire x1="-149.86" y1="327.66" x2="-149.86" y2="337.82" width="0.1524" layer="91"/>
<junction x="-149.86" y="337.82"/>
<pinref part="RE3" gate="B" pin="S"/>
<wire x1="-139.7" y1="325.12" x2="-142.24" y2="325.12" width="0.1524" layer="91"/>
<wire x1="-142.24" y1="325.12" x2="-142.24" y2="337.82" width="0.1524" layer="91"/>
<junction x="-142.24" y="337.82"/>
<wire x1="-142.24" y1="337.82" x2="-109.22" y2="337.82" width="0.1524" layer="91"/>
<wire x1="-109.22" y1="337.82" x2="-101.6" y2="337.82" width="0.1524" layer="91"/>
<wire x1="-101.6" y1="337.82" x2="-60.96" y2="337.82" width="0.1524" layer="91"/>
<wire x1="-58.42" y1="340.36" x2="-60.96" y2="337.82" width="0.1524" layer="91"/>
<label x="-60.96" y="340.36" size="1.778" layer="95" rot="R180"/>
<wire x1="-109.22" y1="327.66" x2="-109.22" y2="337.82" width="0.1524" layer="91"/>
<junction x="-109.22" y="337.82"/>
<pinref part="RE2" gate="B" pin="S"/>
<wire x1="-99.06" y1="325.12" x2="-101.6" y2="325.12" width="0.1524" layer="91"/>
<wire x1="-101.6" y1="325.12" x2="-101.6" y2="337.82" width="0.1524" layer="91"/>
<junction x="-101.6" y="337.82"/>
</segment>
</net>
<net name="X23.1.37" class="0">
<segment>
<wire x1="86.36" y1="508" x2="88.9" y2="510.54" width="0.1524" layer="91"/>
<wire x1="88.9" y1="510.54" x2="88.9" y2="525.78" width="0.1524" layer="91"/>
<label x="88.9" y="510.54" size="1.778" layer="95" rot="R90"/>
<wire x1="88.9" y1="525.78" x2="91.44" y2="528.32" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="156.21" y1="528.32" x2="158.75" y2="530.86" width="0.1524" layer="91"/>
<wire x1="158.75" y1="530.86" x2="158.75" y2="542.29" width="0.1524" layer="91"/>
<label x="158.75" y="530.86" size="1.778" layer="95" rot="R90"/>
<label x="160.02" y="547.37" size="2.1844" layer="95" rot="R90"/>
</segment>
</net>
<net name="X23.1.02" class="0">
<segment>
<wire x1="91.44" y1="508" x2="93.98" y2="510.54" width="0.1524" layer="91"/>
<wire x1="93.98" y1="510.54" x2="93.98" y2="525.78" width="0.1524" layer="91"/>
<label x="93.98" y="510.54" size="1.778" layer="95" rot="R90"/>
<wire x1="93.98" y1="525.78" x2="96.52" y2="528.32" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="JP_2" gate="G$1" pin="7"/>
<wire x1="55.88" y1="508" x2="53.34" y2="505.46" width="0.1524" layer="91"/>
<wire x1="53.34" y1="505.46" x2="53.34" y2="485.14" width="0.1524" layer="91"/>
<junction x="53.34" y="485.14"/>
<wire x1="53.34" y1="485.14" x2="53.34" y2="472.44" width="0.1524" layer="91"/>
<label x="53.34" y="505.46" size="1.778" layer="95" rot="MR270"/>
</segment>
<segment>
<wire x1="151.13" y1="528.32" x2="153.67" y2="530.86" width="0.1524" layer="91"/>
<wire x1="153.67" y1="530.86" x2="153.67" y2="542.29" width="0.1524" layer="91"/>
<label x="153.67" y="530.86" size="1.778" layer="95" rot="R90"/>
<label x="154.94" y="547.37" size="2.1844" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="161.29" y1="528.32" x2="163.83" y2="530.86" width="0.1524" layer="91"/>
<wire x1="163.83" y1="530.86" x2="163.83" y2="542.29" width="0.1524" layer="91"/>
<label x="163.83" y="530.86" size="1.778" layer="95" rot="R90"/>
<label x="165.1" y="547.37" size="2.1844" layer="95" rot="R90"/>
</segment>
</net>
<net name="X23.1.40" class="0">
<segment>
<wire x1="99.06" y1="510.54" x2="99.06" y2="525.78" width="0.1524" layer="91"/>
<wire x1="96.52" y1="508" x2="99.06" y2="510.54" width="0.1524" layer="91"/>
<label x="99.06" y="510.54" size="1.778" layer="95" rot="R90"/>
<wire x1="99.06" y1="525.78" x2="101.6" y2="528.32" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="166.37" y1="528.32" x2="168.91" y2="530.86" width="0.1524" layer="91"/>
<wire x1="168.91" y1="530.86" x2="168.91" y2="542.29" width="0.1524" layer="91"/>
<label x="168.91" y="530.86" size="1.778" layer="95" rot="R90"/>
<label x="170.18" y="547.37" size="2.1844" layer="95" rot="R90"/>
</segment>
</net>
<net name="X23.1.52" class="0">
<segment>
<wire x1="101.6" y1="508" x2="104.14" y2="510.54" width="0.1524" layer="91"/>
<wire x1="104.14" y1="510.54" x2="104.14" y2="525.78" width="0.1524" layer="91"/>
<label x="104.14" y="510.54" size="1.778" layer="95" rot="R90"/>
<wire x1="104.14" y1="525.78" x2="106.68" y2="528.32" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="171.45" y1="528.32" x2="173.99" y2="530.86" width="0.1524" layer="91"/>
<wire x1="173.99" y1="530.86" x2="173.99" y2="542.29" width="0.1524" layer="91"/>
<label x="173.99" y="530.86" size="1.778" layer="95" rot="R90"/>
<label x="175.26" y="547.37" size="2.1844" layer="95" rot="R90"/>
</segment>
</net>
<net name="X23.1.03" class="0">
<segment>
<wire x1="106.68" y1="508" x2="109.22" y2="510.54" width="0.1524" layer="91"/>
<wire x1="109.22" y1="510.54" x2="109.22" y2="525.78" width="0.1524" layer="91"/>
<label x="109.22" y="510.54" size="1.778" layer="95" rot="R90"/>
<wire x1="109.22" y1="525.78" x2="111.76" y2="528.32" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="176.53" y1="528.32" x2="179.07" y2="530.86" width="0.1524" layer="91"/>
<wire x1="179.07" y1="530.86" x2="179.07" y2="542.29" width="0.1524" layer="91"/>
<label x="179.07" y="530.86" size="1.778" layer="95" rot="R90"/>
<label x="180.34" y="547.37" size="2.1844" layer="95" rot="R90"/>
</segment>
</net>
<net name="X23.1.13" class="0">
<segment>
<wire x1="111.76" y1="508" x2="114.3" y2="510.54" width="0.1524" layer="91"/>
<wire x1="114.3" y1="510.54" x2="114.3" y2="525.78" width="0.1524" layer="91"/>
<label x="114.3" y="510.54" size="1.778" layer="95" rot="R90"/>
<wire x1="114.3" y1="525.78" x2="116.84" y2="528.32" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="181.61" y1="528.32" x2="184.15" y2="530.86" width="0.1524" layer="91"/>
<wire x1="184.15" y1="530.86" x2="184.15" y2="542.29" width="0.1524" layer="91"/>
<label x="184.15" y="530.86" size="1.778" layer="95" rot="R90"/>
<label x="185.42" y="547.37" size="2.1844" layer="95" rot="R90"/>
</segment>
</net>
<net name="GNDA" class="0">
<segment>
<wire x1="154.305" y1="457.2" x2="154.305" y2="455.93" width="0.1524" layer="91" style="shortdash"/>
<pinref part="GND3" gate="1" pin="GNDA"/>
</segment>
<segment>
<wire x1="154.305" y1="474.98" x2="154.305" y2="473.71" width="0.1524" layer="91" style="shortdash"/>
<pinref part="GND4" gate="1" pin="GNDA"/>
</segment>
<segment>
<pinref part="GND6" gate="1" pin="GNDA"/>
<wire x1="12.7" y1="546.1" x2="12.7" y2="535.94" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CAN0-H" class="0">
<segment>
<wire x1="121.92" y1="480.695" x2="178.435" y2="480.695" width="0.1524" layer="91"/>
<wire x1="116.84" y1="454.66" x2="121.92" y2="459.74" width="0.1524" layer="91"/>
<wire x1="116.84" y1="452.12" x2="116.84" y2="454.66" width="0.1524" layer="91"/>
<wire x1="121.92" y1="447.04" x2="116.84" y2="452.12" width="0.1524" layer="91"/>
<wire x1="121.92" y1="444.5" x2="121.92" y2="447.04" width="0.1524" layer="91"/>
<wire x1="116.84" y1="439.42" x2="121.92" y2="444.5" width="0.1524" layer="91"/>
<wire x1="116.84" y1="436.88" x2="116.84" y2="439.42" width="0.1524" layer="91"/>
<wire x1="121.92" y1="431.8" x2="116.84" y2="436.88" width="0.1524" layer="91"/>
<wire x1="116.84" y1="424.18" x2="121.92" y2="429.26" width="0.1524" layer="91"/>
<wire x1="121.92" y1="429.26" x2="121.92" y2="431.8" width="0.1524" layer="91"/>
<label x="114.935" y="424.18" size="1.778" layer="95" rot="R270"/>
<wire x1="121.92" y1="459.74" x2="121.92" y2="480.695" width="0.1524" layer="91"/>
<label x="158.115" y="483.235" size="1.778" layer="95" rot="MR180"/>
<wire x1="178.435" y1="480.695" x2="178.435" y2="488.315" width="0.1524" layer="91"/>
<wire x1="178.435" y1="488.315" x2="186.055" y2="488.315" width="0.1524" layer="91"/>
<pinref part="RTERM2" gate="G$1" pin="2"/>
<wire x1="186.055" y1="488.315" x2="194.945" y2="488.315" width="0.1524" layer="91"/>
<wire x1="186.055" y1="485.775" x2="186.055" y2="488.315" width="0.1524" layer="91"/>
<junction x="186.055" y="488.315"/>
</segment>
<segment>
<wire x1="167.5765" y1="335.3435" x2="167.64" y2="347.98" width="0.1524" layer="91"/>
<wire x1="172.72" y1="353.06" x2="167.64" y2="347.98" width="0.1524" layer="91"/>
<label x="174.625" y="353.06" size="1.778" layer="95" rot="R90"/>
<label x="167.5765" y="344.8685" size="1.778" layer="95" rot="R270"/>
<wire x1="167.5765" y1="335.3435" x2="165.0365" y2="332.8035" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="177.8" y1="332.74" x2="180.34" y2="335.28" width="0.1524" layer="91"/>
<wire x1="180.34" y1="335.28" x2="180.34" y2="372.745" width="0.1524" layer="91"/>
<label x="179.07" y="335.28" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="CAN0-L" class="0">
<segment>
<wire x1="172.6565" y1="335.3435" x2="172.72" y2="347.98" width="0.1524" layer="91"/>
<wire x1="167.64" y1="353.06" x2="172.72" y2="347.98" width="0.1524" layer="91"/>
<label x="169.545" y="353.06" size="1.778" layer="95" rot="R90"/>
<label x="172.6565" y="344.8685" size="1.778" layer="95" rot="R270"/>
<wire x1="172.6565" y1="335.3435" x2="170.1165" y2="332.8035" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="116.84" y1="478.155" x2="116.84" y2="459.74" width="0.1524" layer="91"/>
<wire x1="116.84" y1="478.155" x2="178.435" y2="478.155" width="0.1524" layer="91"/>
<wire x1="121.92" y1="454.66" x2="116.84" y2="459.74" width="0.1524" layer="91"/>
<wire x1="121.92" y1="452.12" x2="121.92" y2="454.66" width="0.1524" layer="91"/>
<wire x1="116.84" y1="447.04" x2="121.92" y2="452.12" width="0.1524" layer="91"/>
<wire x1="116.84" y1="444.5" x2="116.84" y2="447.04" width="0.1524" layer="91"/>
<wire x1="121.92" y1="439.42" x2="116.84" y2="444.5" width="0.1524" layer="91"/>
<wire x1="121.92" y1="436.88" x2="121.92" y2="439.42" width="0.1524" layer="91"/>
<wire x1="116.84" y1="431.8" x2="121.92" y2="436.88" width="0.1524" layer="91"/>
<wire x1="121.92" y1="424.18" x2="116.84" y2="429.26" width="0.1524" layer="91"/>
<wire x1="116.84" y1="429.26" x2="116.84" y2="431.8" width="0.1524" layer="91"/>
<label x="120.015" y="424.18" size="1.778" layer="95" rot="R270"/>
<label x="158.115" y="475.615" size="1.778" layer="95"/>
<wire x1="178.435" y1="478.155" x2="178.435" y2="473.075" width="0.1524" layer="91"/>
<wire x1="178.435" y1="473.075" x2="186.055" y2="473.075" width="0.1524" layer="91"/>
<pinref part="RTERM2" gate="G$1" pin="1"/>
<wire x1="186.055" y1="473.075" x2="194.945" y2="473.075" width="0.1524" layer="91"/>
<wire x1="186.055" y1="475.615" x2="186.055" y2="473.075" width="0.1524" layer="91"/>
<junction x="186.055" y="473.075"/>
</segment>
<segment>
<wire x1="182.88" y1="332.74" x2="185.42" y2="335.28" width="0.1524" layer="91"/>
<wire x1="185.42" y1="335.28" x2="185.42" y2="372.745" width="0.1524" layer="91"/>
<label x="184.15" y="335.28" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="CAN1-L" class="0">
<segment>
<wire x1="178.435" y1="460.375" x2="142.24" y2="460.375" width="0.1524" layer="91"/>
<label x="158.115" y="457.835" size="1.778" layer="95"/>
<wire x1="178.435" y1="460.375" x2="178.435" y2="452.755" width="0.1524" layer="91"/>
<wire x1="178.435" y1="452.755" x2="186.055" y2="452.755" width="0.1524" layer="91"/>
<pinref part="RTERM3" gate="G$1" pin="1"/>
<wire x1="186.055" y1="452.755" x2="194.945" y2="452.755" width="0.1524" layer="91"/>
<wire x1="186.055" y1="455.295" x2="186.055" y2="452.755" width="0.1524" layer="91"/>
<junction x="186.055" y="452.755"/>
<wire x1="137.16" y1="447.04" x2="142.24" y2="452.12" width="0.1524" layer="91"/>
<wire x1="137.16" y1="444.5" x2="137.16" y2="447.04" width="0.1524" layer="91"/>
<wire x1="142.24" y1="439.42" x2="137.16" y2="444.5" width="0.1524" layer="91"/>
<wire x1="142.24" y1="436.88" x2="142.24" y2="439.42" width="0.1524" layer="91"/>
<wire x1="137.16" y1="431.8" x2="142.24" y2="436.88" width="0.1524" layer="91"/>
<wire x1="137.16" y1="429.26" x2="137.16" y2="431.8" width="0.1524" layer="91"/>
<wire x1="142.24" y1="424.18" x2="137.16" y2="429.26" width="0.1524" layer="91"/>
<label x="140.335" y="424.18" size="1.778" layer="95" rot="R270"/>
<wire x1="142.24" y1="452.12" x2="142.24" y2="460.375" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CAN1-H" class="0">
<segment>
<wire x1="137.16" y1="462.915" x2="178.435" y2="462.915" width="0.1524" layer="91"/>
<label x="158.115" y="465.455" size="1.778" layer="95" rot="MR180"/>
<wire x1="178.435" y1="462.915" x2="178.435" y2="467.995" width="0.1524" layer="91"/>
<wire x1="178.435" y1="467.995" x2="186.055" y2="467.995" width="0.1524" layer="91"/>
<pinref part="RTERM3" gate="G$1" pin="2"/>
<wire x1="186.055" y1="467.995" x2="194.945" y2="467.995" width="0.1524" layer="91"/>
<wire x1="186.055" y1="465.455" x2="186.055" y2="467.995" width="0.1524" layer="91"/>
<junction x="186.055" y="467.995"/>
<wire x1="142.24" y1="447.04" x2="137.16" y2="452.12" width="0.1524" layer="91"/>
<wire x1="142.24" y1="444.5" x2="142.24" y2="447.04" width="0.1524" layer="91"/>
<wire x1="137.16" y1="439.42" x2="142.24" y2="444.5" width="0.1524" layer="91"/>
<wire x1="137.16" y1="436.88" x2="137.16" y2="439.42" width="0.1524" layer="91"/>
<wire x1="142.24" y1="431.8" x2="137.16" y2="436.88" width="0.1524" layer="91"/>
<wire x1="142.24" y1="429.26" x2="142.24" y2="431.8" width="0.1524" layer="91"/>
<wire x1="137.16" y1="424.18" x2="142.24" y2="429.26" width="0.1524" layer="91"/>
<label x="135.255" y="424.18" size="1.778" layer="95" rot="R270"/>
<wire x1="137.16" y1="452.12" x2="137.16" y2="462.915" width="0.1524" layer="91"/>
</segment>
</net>
<net name="X23.2.07" class="0">
<segment>
<wire x1="22.86" y1="548.64" x2="20.32" y2="546.1" width="0.1524" layer="91"/>
<label x="18.415" y="546.1" size="1.778" layer="95" rot="R270"/>
<wire x1="20.32" y1="546.1" x2="20.32" y2="447.04" width="0.1524" layer="91"/>
<pinref part="L_1" gate="G$1" pin="P$2"/>
<wire x1="20.32" y1="447.04" x2="27.94" y2="447.04" width="0.1524" layer="91"/>
</segment>
</net>
<net name="X23.2.06" class="0">
<segment>
<wire x1="25.4" y1="548.64" x2="22.86" y2="546.1" width="0.1524" layer="91"/>
<label x="20.955" y="546.1" size="1.778" layer="95" rot="R270"/>
<wire x1="22.86" y1="546.1" x2="22.86" y2="449.58" width="0.1524" layer="91"/>
<pinref part="L_1" gate="G$1" pin="P$1"/>
<wire x1="22.86" y1="449.58" x2="27.94" y2="449.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="X23.2.04" class="0">
<segment>
<wire x1="15.24" y1="548.64" x2="12.7" y2="546.1" width="0.1524" layer="91"/>
<label x="13.97" y="546.1" size="1.778" layer="95" rot="R270"/>
</segment>
</net>
<net name="X23.1.25" class="0">
<segment>
<wire x1="66.04" y1="508" x2="63.5" y2="505.46" width="0.1524" layer="91"/>
<label x="63.5" y="505.46" size="1.778" layer="95" rot="MR270"/>
<pinref part="JP_2" gate="G$1" pin="9"/>
<wire x1="63.5" y1="505.46" x2="63.5" y2="480.06" width="0.1524" layer="91"/>
<wire x1="63.5" y1="480.06" x2="58.42" y2="480.06" width="0.1524" layer="91"/>
<wire x1="58.42" y1="480.06" x2="58.42" y2="472.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="X23.1.31" class="0">
<segment>
<wire x1="53.34" y1="508" x2="50.8" y2="505.46" width="0.1524" layer="91"/>
<wire x1="50.8" y1="505.46" x2="50.8" y2="472.44" width="0.1524" layer="91"/>
<label x="50.8" y="505.46" size="1.778" layer="95" rot="MR270"/>
<pinref part="JP_2" gate="G$1" pin="6"/>
</segment>
</net>
<net name="X23.1.56" class="0">
<segment>
<wire x1="63.5" y1="508" x2="60.96" y2="505.46" width="0.1524" layer="91"/>
<label x="60.96" y="505.46" size="1.778" layer="95" rot="MR270"/>
<pinref part="JP_2" gate="G$1" pin="8"/>
<wire x1="60.96" y1="505.46" x2="60.96" y2="482.6" width="0.1524" layer="91"/>
<wire x1="60.96" y1="482.6" x2="55.88" y2="482.6" width="0.1524" layer="91"/>
<wire x1="55.88" y1="482.6" x2="55.88" y2="472.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="KL30F" class="0">
<segment>
<wire x1="198.12" y1="332.74" x2="200.66" y2="335.28" width="0.3048" layer="91"/>
<wire x1="200.66" y1="335.28" x2="200.66" y2="372.745" width="0.3048" layer="91"/>
<label x="199.39" y="335.28" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="-22.86" y1="332.74" x2="-20.32" y2="335.28" width="0.1524" layer="91"/>
<wire x1="-20.32" y1="335.28" x2="-20.32" y2="388.62" width="0.1524" layer="91"/>
<pinref part="JP_1" gate="G$1" pin="12"/>
<wire x1="-15.24" y1="396.24" x2="-15.24" y2="388.62" width="0.1524" layer="91"/>
<wire x1="-20.32" y1="388.62" x2="-15.24" y2="388.62" width="0.1524" layer="91"/>
<label x="-20.32" y="335.28" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="X23.1.22" class="0">
<segment>
<wire x1="2.54" y1="508" x2="0" y2="505.46" width="0.1524" layer="91"/>
<label x="0" y="505.46" size="1.778" layer="95" rot="MR270"/>
<pinref part="JP_3" gate="G$1" pin="7"/>
<wire x1="0" y1="505.46" x2="0" y2="472.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="X23.1.58" class="0">
<segment>
<wire x1="50.8" y1="508" x2="48.26" y2="505.46" width="0.1524" layer="91"/>
<wire x1="48.26" y1="505.46" x2="48.26" y2="472.44" width="0.1524" layer="91"/>
<label x="48.26" y="505.46" size="1.778" layer="95" rot="MR270"/>
<pinref part="JP_2" gate="G$1" pin="5"/>
</segment>
</net>
<net name="X23.2.05" class="0">
<segment>
<pinref part="GND14" gate="1" pin="GNDA"/>
<wire x1="7.62" y1="535.94" x2="7.62" y2="546.1" width="0.1524" layer="91"/>
<wire x1="10.16" y1="548.64" x2="7.62" y2="546.1" width="0.1524" layer="91"/>
<label x="8.89" y="546.1" size="1.778" layer="95" rot="R270"/>
</segment>
</net>
<net name="X23.1.09" class="0">
<segment>
<wire x1="-12.7" y1="373.38" x2="-15.24" y2="370.84" width="0.1524" layer="91"/>
<label x="-12.827" y="373.38" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="X23.1.24" class="0">
<segment>
<wire x1="-12.7" y1="370.84" x2="-10.16" y2="373.38" width="0.1524" layer="91"/>
<label x="-10.287" y="373.38" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="X23.1.42" class="0">
<segment>
<wire x1="-12.7" y1="505.46" x2="-10.16" y2="508" width="0.1524" layer="91"/>
<label x="-12.7" y="505.46" size="1.778" layer="95" rot="MR270"/>
</segment>
</net>
<net name="X23.1.61" class="0">
<segment>
<wire x1="175.26" y1="370.84" x2="172.72" y2="368.3" width="0.1524" layer="91"/>
<label x="172.72" y="368.3" size="1.778" layer="95" rot="MR270"/>
<wire x1="172.72" y1="368.3" x2="172.72" y2="353.06" width="0.1524" layer="91"/>
</segment>
</net>
<net name="START" class="0">
<segment>
<wire x1="193.04" y1="332.74" x2="195.58" y2="335.28" width="0.1524" layer="91"/>
<wire x1="195.58" y1="335.28" x2="195.58" y2="372.745" width="0.1524" layer="91"/>
<label x="194.31" y="335.28" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="2.54" y1="332.74" x2="5.08" y2="335.28" width="0.1524" layer="91"/>
<pinref part="JP_1" gate="G$1" pin="4"/>
<wire x1="5.08" y1="335.28" x2="5.08" y2="396.24" width="0.1524" layer="91"/>
<label x="5.08" y="335.28" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="X23.1.62" class="0">
<segment>
<wire x1="170.18" y1="370.84" x2="167.64" y2="368.3" width="0.1524" layer="91"/>
<label x="167.64" y="368.3" size="1.778" layer="95" rot="MR270"/>
<wire x1="167.64" y1="368.3" x2="167.64" y2="353.06" width="0.1524" layer="91"/>
</segment>
</net>
<net name="X23.1.50" class="0">
<segment>
<wire x1="114.3" y1="370.84" x2="116.84" y2="373.38" width="0.1524" layer="91"/>
<label x="115.57" y="373.38" size="1.778" layer="95" rot="R90"/>
<wire x1="116.84" y1="373.38" x2="116.84" y2="424.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="X23.1.51" class="0">
<segment>
<wire x1="119.38" y1="370.84" x2="121.92" y2="373.38" width="0.1524" layer="91"/>
<label x="120.65" y="373.38" size="1.778" layer="95" rot="R90"/>
<wire x1="121.92" y1="424.18" x2="121.92" y2="373.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="X23.1.46" class="0">
<segment>
<wire x1="134.62" y1="370.84" x2="137.16" y2="373.38" width="0.1524" layer="91"/>
<label x="135.89" y="373.38" size="1.778" layer="95" rot="R90"/>
<wire x1="137.16" y1="373.38" x2="137.16" y2="424.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="X23.1.47" class="0">
<segment>
<wire x1="139.7" y1="370.84" x2="142.24" y2="373.38" width="0.1524" layer="91"/>
<label x="140.97" y="373.38" size="1.778" layer="95" rot="R90"/>
<wire x1="142.24" y1="373.38" x2="142.24" y2="424.18" width="0.1524" layer="91"/>
</segment>
</net>
<net name="X23.1.57" class="0">
<segment>
<pinref part="JP_3" gate="G$1" pin="4"/>
<wire x1="-5.08" y1="508" x2="-7.62" y2="505.46" width="0.1524" layer="91"/>
<wire x1="-7.62" y1="505.46" x2="-7.62" y2="472.44" width="0.1524" layer="91"/>
<label x="-7.62" y="505.46" size="1.778" layer="95" rot="MR270"/>
</segment>
</net>
<net name="X23.1.05" class="0">
<segment>
<pinref part="JP_3" gate="G$1" pin="3"/>
<wire x1="-7.62" y1="508" x2="-10.16" y2="505.46" width="0.1524" layer="91"/>
<wire x1="-10.16" y1="505.46" x2="-10.16" y2="472.44" width="0.1524" layer="91"/>
<label x="-10.16" y="505.46" size="1.778" layer="95" rot="MR270"/>
</segment>
</net>
<net name="X23.1.08" class="0">
<segment>
<pinref part="JP_3" gate="G$1" pin="5"/>
<wire x1="-2.54" y1="508" x2="-5.08" y2="505.46" width="0.1524" layer="91"/>
<wire x1="-5.08" y1="505.46" x2="-5.08" y2="472.44" width="0.1524" layer="91"/>
<label x="-5.08" y="505.46" size="1.778" layer="95" rot="MR270"/>
</segment>
</net>
<net name="X23.1.32" class="0">
<segment>
<pinref part="JP_3" gate="G$1" pin="10"/>
<wire x1="10.16" y1="508" x2="7.62" y2="505.46" width="0.1524" layer="91"/>
<wire x1="7.62" y1="505.46" x2="7.62" y2="472.44" width="0.1524" layer="91"/>
<label x="7.62" y="505.46" size="1.778" layer="95" rot="MR270"/>
</segment>
</net>
<net name="X23.1.33" class="0">
<segment>
<pinref part="JP_3" gate="G$1" pin="11"/>
<wire x1="12.7" y1="508" x2="10.16" y2="505.46" width="0.1524" layer="91"/>
<wire x1="10.16" y1="505.46" x2="10.16" y2="472.44" width="0.1524" layer="91"/>
<label x="10.16" y="505.46" size="1.778" layer="95" rot="MR270"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<wire x1="152.4" y1="481.965" x2="156.21" y2="481.965" width="0.1524" layer="91" style="shortdash" curve="-180"/>
<wire x1="156.21" y1="476.885" x2="152.4" y2="476.885" width="0.1524" layer="91" style="shortdash" curve="-180"/>
<wire x1="152.4" y1="481.965" x2="152.4" y2="476.885" width="0.1524" layer="91" style="shortdash"/>
<wire x1="156.21" y1="481.965" x2="156.21" y2="476.885" width="0.1524" layer="91" style="shortdash"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<wire x1="152.4" y1="464.185" x2="156.21" y2="464.185" width="0.1524" layer="91" style="shortdash" curve="-180"/>
<wire x1="156.21" y1="459.105" x2="152.4" y2="459.105" width="0.1524" layer="91" style="shortdash" curve="-180"/>
<wire x1="152.4" y1="464.185" x2="152.4" y2="459.105" width="0.1524" layer="91" style="shortdash"/>
<wire x1="156.21" y1="464.185" x2="156.21" y2="459.105" width="0.1524" layer="91" style="shortdash"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<wire x1="180.0225" y1="339.725" x2="180.0225" y2="343.535" width="0.1524" layer="91" style="shortdash" curve="-180"/>
<wire x1="185.1025" y1="343.535" x2="185.1025" y2="339.725" width="0.1524" layer="91" style="shortdash" curve="-180"/>
<wire x1="180.0225" y1="339.725" x2="185.1025" y2="339.725" width="0.1524" layer="91" style="shortdash"/>
<wire x1="180.0225" y1="343.535" x2="185.1025" y2="343.535" width="0.1524" layer="91" style="shortdash"/>
</segment>
</net>
<net name="X23.1.07" class="0">
<segment>
<pinref part="JP_3" gate="G$1" pin="8"/>
<wire x1="2.54" y1="472.44" x2="2.54" y2="505.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="X23.1.10" class="0">
<segment>
<pinref part="JP_3" gate="G$1" pin="2"/>
<wire x1="-12.7" y1="472.44" x2="-12.7" y2="505.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="!VENTILATION_ON2" class="0">
<segment>
<pinref part="JP_1" gate="G$1" pin="11"/>
<wire x1="-12.7" y1="396.24" x2="-12.7" y2="373.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="X23.1.21" class="0">
<segment>
<pinref part="JP_1" gate="G$1" pin="10"/>
<wire x1="-10.16" y1="396.24" x2="-10.16" y2="373.38" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<pinref part="JP_1" gate="G$1" pin="1"/>
<wire x1="12.7" y1="396.24" x2="12.7" y2="388.62" width="0.1524" layer="91"/>
</segment>
</net>
<net name="X23.1.26" class="0">
<segment>
<wire x1="43.18" y1="508" x2="40.64" y2="505.46" width="0.1524" layer="91"/>
<label x="40.64" y="505.46" size="1.778" layer="95" rot="MR270"/>
<pinref part="JP_2" gate="G$1" pin="2"/>
<wire x1="40.64" y1="505.46" x2="40.64" y2="472.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="X23.1.55" class="0">
<segment>
<wire x1="68.58" y1="508" x2="66.04" y2="505.46" width="0.1524" layer="91"/>
<label x="66.04" y="505.46" size="1.778" layer="95" rot="MR270"/>
<pinref part="JP_2" gate="G$1" pin="10"/>
<wire x1="66.04" y1="505.46" x2="66.04" y2="477.52" width="0.1524" layer="91"/>
<wire x1="66.04" y1="477.52" x2="60.96" y2="477.52" width="0.1524" layer="91"/>
<wire x1="60.96" y1="477.52" x2="60.96" y2="472.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="X23.1.53" class="0">
<segment>
<wire x1="45.72" y1="508" x2="43.18" y2="505.46" width="0.1524" layer="91"/>
<label x="43.18" y="505.46" size="1.778" layer="95" rot="MR270"/>
<pinref part="JP_2" gate="G$1" pin="3"/>
<wire x1="43.18" y1="505.46" x2="43.18" y2="472.44" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="X_2" gate="G$1" pin="P$5"/>
<wire x1="60.96" y1="449.58" x2="81.28" y2="449.58" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="X_4" gate="G$1" pin="P$5"/>
<wire x1="60.96" y1="434.34" x2="81.28" y2="434.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<pinref part="JP_4" gate="G$1" pin="4"/>
<pinref part="FA2" gate="G$1" pin="2"/>
<wire x1="43.18" y1="398.78" x2="83.82" y2="398.78" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<pinref part="JP_4" gate="G$1" pin="3"/>
<wire x1="43.18" y1="403.86" x2="96.52" y2="403.86" width="0.1524" layer="91"/>
<wire x1="96.52" y1="403.86" x2="96.52" y2="393.7" width="0.1524" layer="91"/>
</segment>
</net>
<net name="MOTOR" class="0">
<segment>
<pinref part="JP_1" gate="G$1" pin="3"/>
<wire x1="7.62" y1="396.24" x2="7.62" y2="322.58" width="0.1524" layer="91"/>
<wire x1="5.08" y1="320.04" x2="7.62" y2="322.58" width="0.1524" layer="91"/>
<label x="7.62" y="322.58" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="IG" class="0">
<segment>
<pinref part="JP_1" gate="G$1" pin="6"/>
<wire x1="0" y1="396.24" x2="0" y2="347.98" width="0.1524" layer="91"/>
<wire x1="-2.54" y1="345.44" x2="0" y2="347.98" width="0.1524" layer="91"/>
<label x="0" y="347.98" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="106.68" y1="345.44" x2="109.22" y2="347.98" width="0.1524" layer="91"/>
<wire x1="109.22" y1="347.98" x2="109.22" y2="493.522" width="0.1524" layer="91"/>
<wire x1="109.22" y1="493.522" x2="195.072" y2="493.522" width="0.1524" layer="91"/>
<label x="158.496" y="494.03" size="1.778" layer="95"/>
<label x="109.22" y="347.98" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="JP_1" gate="G$1" pin="5"/>
<wire x1="2.54" y1="396.24" x2="2.54" y2="335.28" width="0.1524" layer="91"/>
<wire x1="0" y1="332.74" x2="2.54" y2="335.28" width="0.1524" layer="91"/>
<label x="2.54" y="335.28" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="203.2" y1="332.74" x2="205.74" y2="335.28" width="0.3048" layer="91"/>
<wire x1="205.74" y1="335.28" x2="205.74" y2="372.745" width="0.3048" layer="91"/>
<label x="204.47" y="335.28" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="X23.1.23" class="0">
<segment>
<wire x1="58.42" y1="508" x2="55.88" y2="505.46" width="0.1524" layer="91"/>
<wire x1="55.88" y1="505.46" x2="55.88" y2="485.14" width="0.1524" layer="91"/>
<wire x1="55.88" y1="485.14" x2="53.594" y2="485.14" width="0.1524" layer="91"/>
<label x="55.88" y="505.46" size="1.778" layer="95" rot="MR270"/>
</segment>
</net>
<net name="VENTSTART" class="0">
<segment>
<wire x1="168.91" y1="576.58" x2="168.91" y2="567.055" width="0.1524" layer="91"/>
</segment>
</net>
<net name="T7A3" class="0">
<segment>
<wire x1="181.61" y1="573.405" x2="184.15" y2="570.23" width="0.1524" layer="91"/>
<wire x1="184.15" y1="570.23" x2="184.15" y2="567.055" width="0.1524" layer="91"/>
</segment>
</net>
<net name="T7B3" class="0">
<segment>
<wire x1="181.61" y1="573.405" x2="179.07" y2="570.23" width="0.1524" layer="91"/>
<wire x1="179.07" y1="570.23" x2="179.07" y2="567.055" width="0.1524" layer="91"/>
</segment>
</net>
<net name="T7A4" class="0">
<segment>
<wire x1="156.21" y1="573.405" x2="158.75" y2="570.23" width="0.1524" layer="91"/>
<wire x1="158.75" y1="570.23" x2="158.75" y2="567.055" width="0.1524" layer="91"/>
</segment>
</net>
<net name="T7B4" class="0">
<segment>
<wire x1="156.21" y1="573.405" x2="153.67" y2="570.23" width="0.1524" layer="91"/>
<wire x1="153.67" y1="570.23" x2="153.67" y2="567.055" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CO1" class="0">
<segment>
<wire x1="168.91" y1="576.58" x2="167.005" y2="574.675" width="0.1524" layer="91"/>
<wire x1="167.005" y1="574.675" x2="163.83" y2="574.675" width="0.1524" layer="91"/>
<wire x1="163.83" y1="574.675" x2="163.83" y2="567.055" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$102" class="0">
<segment>
<pinref part="FA15" gate="G$1" pin="1A"/>
<pinref part="FA15" gate="G$1" pin="1B"/>
<pinref part="FA15" gate="G$1" pin="1C"/>
<pinref part="FA15" gate="G$1" pin="1D"/>
</segment>
</net>
<net name="N$99" class="0">
<segment>
<wire x1="-185.42" y1="337.82" x2="-187.96" y2="337.82" width="0.1524" layer="91"/>
<pinref part="S9" gate="G$1" pin="1"/>
<pinref part="S10" gate="G$1" pin="2"/>
<wire x1="-187.96" y1="337.82" x2="-190.5" y2="337.82" width="0.1524" layer="91"/>
<wire x1="-187.96" y1="337.82" x2="-187.96" y2="304.8" width="0.1524" layer="91"/>
<wire x1="-187.96" y1="304.8" x2="-134.62" y2="304.8" width="0.1524" layer="91"/>
<junction x="-187.96" y="337.82"/>
<pinref part="RE3" gate="B" pin="P"/>
<wire x1="-134.62" y1="304.8" x2="-134.62" y2="317.5" width="0.1524" layer="91"/>
</segment>
</net>
<net name="1IN" class="0">
<segment>
<wire x1="-86.36" y1="551.18" x2="-144.4625" y2="551.18" width="0.1524" layer="91"/>
<label x="-142.24" y="551.942" size="1.778" layer="95"/>
<wire x1="-144.4625" y1="551.18" x2="-144.4625" y2="540.0675" width="0.1524" layer="91"/>
<wire x1="-83.82" y1="553.72" x2="-86.36" y2="551.18" width="0.1524" layer="91"/>
<label x="-86.36" y="553.72" size="1.778" layer="95" rot="R180"/>
</segment>
<segment>
<wire x1="-83.82" y1="317.5" x2="-86.36" y2="314.96" width="0.1524" layer="91"/>
<wire x1="-86.36" y1="314.96" x2="-93.98" y2="314.96" width="0.1524" layer="91"/>
<pinref part="RE2" gate="B" pin="P"/>
<wire x1="-93.98" y1="314.96" x2="-93.98" y2="317.5" width="0.1524" layer="91"/>
<label x="-86.36" y="317.5" size="1.778" layer="95" rot="R180"/>
<pinref part="Z3" gate="G$1" pin="2"/>
<wire x1="-180.34" y1="292.1" x2="-93.98" y2="292.1" width="0.1524" layer="91"/>
<wire x1="-93.98" y1="292.1" x2="-93.98" y2="314.96" width="0.1524" layer="91"/>
<junction x="-93.98" y="314.96"/>
</segment>
</net>
<net name="N$6" class="0">
<segment>
<pinref part="S10" gate="G$1" pin="1"/>
<wire x1="-205.74" y1="337.82" x2="-210.82" y2="337.82" width="0.1524" layer="91"/>
<wire x1="-210.82" y1="337.82" x2="-212.09" y2="337.82" width="0.1524" layer="91"/>
<wire x1="-212.09" y1="337.82" x2="-212.09" y2="343.662" width="0.1524" layer="91"/>
<wire x1="-212.09" y1="337.82" x2="-219.71" y2="337.82" width="0.1524" layer="91"/>
<wire x1="-219.71" y1="337.82" x2="-219.71" y2="343.662" width="0.1524" layer="91"/>
<junction x="-212.09" y="337.82"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
