<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.6.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="3" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="5" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="6" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="14" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="neosazovat" color="11" fill="1" visible="yes" active="yes"/>
<layer number="102" name="Mittellin" color="7" fill="1" visible="yes" active="yes"/>
<layer number="103" name="fp3" color="7" fill="1" visible="yes" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="yes" active="yes"/>
<layer number="105" name="Beschreib" color="9" fill="1" visible="yes" active="yes"/>
<layer number="106" name="BGA-Top" color="4" fill="1" visible="yes" active="yes"/>
<layer number="107" name="BD-Top" color="5" fill="1" visible="yes" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="yes" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="yes" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="yes" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="yes" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="113" name="ReferenceLS" color="7" fill="1" visible="no" active="no"/>
<layer number="114" name="FRNTMAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="115" name="FRNTMAAT2" color="7" fill="1" visible="no" active="no"/>
<layer number="116" name="Patch_BOT" color="9" fill="4" visible="yes" active="yes"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="no" active="no"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="yes" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="yes" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="127" name="zmeny" color="14" fill="4" visible="yes" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="no" active="no"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="yes" active="yes"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="no" active="no"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="yes" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="yes" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="yes" active="yes"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="199" name="Contour" color="7" fill="1" visible="yes" active="yes"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="4" fill="10" visible="yes" active="yes"/>
<layer number="204" name="204bmp" color="5" fill="10" visible="yes" active="yes"/>
<layer number="205" name="205bmp" color="6" fill="10" visible="yes" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="yes" active="yes"/>
<layer number="207" name="207bmp" color="8" fill="10" visible="yes" active="yes"/>
<layer number="208" name="208bmp" color="9" fill="10" visible="yes" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="no" active="no"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="yes" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="yes" active="yes"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
<layer number="254" name="OrgLBR" color="7" fill="1" visible="yes" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic>
<libraries>
<library name="FRAMES">
<packages>
</packages>
<symbols>
<symbol name="DOCFIELD">
<wire x1="0" y1="0" x2="71.12" y2="0" width="0.254" layer="94"/>
<wire x1="101.6" y1="15.24" x2="87.63" y2="15.24" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="71.12" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="15.24" width="0.254" layer="94"/>
<wire x1="101.6" y1="15.24" x2="101.6" y2="5.08" width="0.254" layer="94"/>
<wire x1="71.12" y1="5.08" x2="71.12" y2="0" width="0.254" layer="94"/>
<wire x1="71.12" y1="5.08" x2="87.63" y2="5.08" width="0.254" layer="94"/>
<wire x1="71.12" y1="0" x2="101.6" y2="0" width="0.254" layer="94"/>
<wire x1="87.63" y1="15.24" x2="87.63" y2="5.08" width="0.254" layer="94"/>
<wire x1="87.63" y1="15.24" x2="0" y2="15.24" width="0.254" layer="94"/>
<wire x1="87.63" y1="5.08" x2="101.6" y2="5.08" width="0.254" layer="94"/>
<wire x1="101.6" y1="5.08" x2="101.6" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="15.24" x2="0" y2="22.86" width="0.254" layer="94"/>
<wire x1="101.6" y1="35.56" x2="0" y2="35.56" width="0.254" layer="94"/>
<wire x1="101.6" y1="35.56" x2="101.6" y2="22.86" width="0.254" layer="94"/>
<wire x1="0" y1="22.86" x2="101.6" y2="22.86" width="0.254" layer="94"/>
<wire x1="0" y1="22.86" x2="0" y2="35.56" width="0.254" layer="94"/>
<wire x1="101.6" y1="22.86" x2="101.6" y2="15.24" width="0.254" layer="94"/>
<text x="1.27" y="1.27" size="2.54" layer="94">Date:</text>
<text x="12.7" y="1.27" size="2.54" layer="94">&gt;LAST_DATE_TIME</text>
<text x="72.39" y="1.27" size="2.54" layer="94">Sheet:</text>
<text x="86.36" y="1.27" size="2.54" layer="94">&gt;SHEET</text>
<text x="88.9" y="11.43" size="2.54" layer="94">REV:</text>
<text x="1.27" y="19.05" size="2.54" layer="94">TITLE:</text>
<text x="1.27" y="11.43" size="2.54" layer="94">Document Number:</text>
<text x="17.78" y="19.05" size="2.54" layer="94">&gt;DRAWING_NAME</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="TAB" uservalue="yes">
<gates>
<gate name="G$1" symbol="DOCFIELD" x="-48.26" y="-12.7"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SUPPLY2">
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.27" y1="2.54" x2="1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="0" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<text x="-1.905" y="-0.635" size="1.524" layer="94">GND</text>
<pin name="GND" x="0" y="5.08" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="V--&gt;">
<wire x1="15.24" y1="0" x2="13.97" y2="1.27" width="0.1524" layer="94"/>
<wire x1="13.97" y1="1.27" x2="0" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="1.27" x2="0" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.27" x2="13.97" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="15.24" y1="0" x2="13.97" y2="-1.27" width="0.1524" layer="94"/>
<text x="1.27" y="-0.762" size="1.524" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="PE">
<wire x1="-1.905" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-0.762" x2="1.27" y2="-0.762" width="0.254" layer="94"/>
<wire x1="-0.635" y1="-1.524" x2="0.635" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-0.381" y="0.635" size="1.524" layer="94" rot="R90">PE</text>
<pin name="PE" x="0" y="5.08" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND">
<gates>
<gate name="GND" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="V--&gt;" prefix="V" uservalue="yes">
<gates>
<gate name="G$1" symbol="V--&gt;" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PE">
<gates>
<gate name="PE" symbol="PE" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SMDA">
<packages>
<package name="MELF">
<wire x1="-2.351" y1="1.135" x2="2.2684" y2="1.135" width="0.127" layer="21"/>
<wire x1="2.2684" y1="1.135" x2="2.2684" y2="-1.135" width="0.127" layer="21"/>
<wire x1="2.2684" y1="-1.135" x2="-2.351" y2="-1.135" width="0.127" layer="21"/>
<wire x1="-2.351" y1="-1.135" x2="-2.351" y2="1.135" width="0.127" layer="21"/>
<smd name="1" x="2.6" y="0" dx="1.8" dy="2.65" layer="1"/>
<smd name="2" x="-2.7" y="0" dx="1.8" dy="2.65" layer="1"/>
<text x="-3.5527" y="1.87" size="1.27" layer="27">&gt;VALUE</text>
<text x="-3.5527" y="3.775" size="1.27" layer="25">&gt;NAME</text>
<rectangle x1="-2.5" y1="-1.2" x2="-1.9" y2="1.2" layer="27"/>
<rectangle x1="1.8" y1="-1.2" x2="2.4" y2="1.2" layer="27"/>
<rectangle x1="-1.5" y1="-1.1" x2="-1.1" y2="1.1" layer="27"/>
</package>
</packages>
<symbols>
<symbol name="DIODE">
<wire x1="-2.54" y1="-1.905" x2="1.27" y2="0" width="0.254" layer="94"/>
<wire x1="1.27" y1="0" x2="-2.54" y2="1.905" width="0.254" layer="94"/>
<wire x1="-2.54" y1="1.905" x2="-2.54" y2="-1.905" width="0.254" layer="94"/>
<wire x1="2.54" y1="0" x2="1.3716" y2="0" width="0.1524" layer="94"/>
<wire x1="1.397" y1="1.905" x2="1.397" y2="-1.905" width="0.254" layer="94"/>
<text x="-2.3114" y="2.6416" size="1.778" layer="95">&gt;NAME</text>
<text x="-2.5654" y="-4.4958" size="1.778" layer="96">&gt;VALUE</text>
<pin name="A" x="-5.08" y="0" visible="off" length="short" direction="pas"/>
<pin name="K" x="5.08" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="DIOD-MELF" prefix="D" uservalue="yes">
<gates>
<gate name="G$1" symbol="DIODE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="MELF">
<connects>
<connect gate="G$1" pin="A" pad="1"/>
<connect gate="G$1" pin="K" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SKLADKA2">
<packages>
<package name="NIC1">
<pad name="1" x="0" y="0" drill="0.8128" diameter="1.397" shape="square"/>
<pad name="2" x="2.54" y="0" drill="0.8128" diameter="1.397" shape="square"/>
<pad name="3" x="3.81" y="0" drill="0.8128" diameter="1.397" shape="square"/>
<pad name="4" x="6.35" y="0" drill="0.8128" diameter="1.397" shape="square"/>
<pad name="5" x="8.89" y="0" drill="0.8128" diameter="1.397" shape="square"/>
<pad name="6" x="11.43" y="0" drill="0.8128" diameter="1.397" shape="square"/>
<pad name="7" x="13.97" y="0" drill="0.8128" diameter="1.397" shape="square"/>
</package>
</packages>
<symbols>
<symbol name="SPINAC1">
<wire x1="0.762" y1="0.508" x2="2.54" y2="5.08" width="0.4064" layer="94"/>
<circle x="0" y="0" radius="0.762" width="0.254" layer="94"/>
<text x="5.08" y="3.048" size="2.032" layer="95">&gt;NAME</text>
<text x="5.08" y="0.508" size="2.032" layer="96">&gt;VALUE</text>
<pin name="1" x="0" y="-5.08" visible="off" length="middle" rot="R90"/>
<pin name="2" x="0" y="10.16" visible="off" length="middle" rot="R270"/>
</symbol>
<symbol name="ZAROVKA">
<wire x1="-1.778" y1="-1.778" x2="1.778" y2="1.778" width="0.254" layer="94"/>
<wire x1="-1.778" y1="1.778" x2="1.778" y2="-1.778" width="0.254" layer="94"/>
<circle x="0" y="0" radius="2.54" width="0.254" layer="94"/>
<text x="5.08" y="0.508" size="2.032" layer="95">&gt;NAME</text>
<text x="5.08" y="-2.032" size="2.032" layer="96">&gt;VALUE</text>
<pin name="1" x="0" y="-7.62" visible="off" length="middle" rot="R90"/>
<pin name="2" x="0" y="7.62" visible="off" length="middle" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SPINAC1" uservalue="yes">
<gates>
<gate name="G$1" symbol="SPINAC1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="NIC1">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ZAROVKA" uservalue="yes">
<gates>
<gate name="G$1" symbol="ZAROVKA" x="0" y="0"/>
</gates>
<devices>
<device name="" package="NIC1">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="RELAIS">
<packages>
<package name="RELEAUTO">
<wire x1="-5.08" y1="4.445" x2="-10.795" y2="4.445" width="0.127" layer="22"/>
<wire x1="-10.795" y1="-4.445" x2="-5.08" y2="-4.445" width="0.127" layer="22"/>
<wire x1="-11.43" y1="3.81" x2="-10.795" y2="4.445" width="0.127" layer="22" curve="-53.130102"/>
<wire x1="-11.43" y1="-3.81" x2="-10.795" y2="-4.445" width="0.127" layer="22" curve="53.130102"/>
<wire x1="-5.08" y1="4.445" x2="-5.08" y2="12.7" width="0.127" layer="22"/>
<wire x1="-5.08" y1="-4.445" x2="-5.08" y2="-12.065" width="0.127" layer="22"/>
<wire x1="3.81" y1="-12.065" x2="3.81" y2="-3.81" width="0.127" layer="22"/>
<wire x1="3.81" y1="-3.81" x2="13.97" y2="-3.81" width="0.127" layer="22"/>
<wire x1="13.97" y1="-3.81" x2="14.605" y2="-3.175" width="0.127" layer="22" curve="53.130102"/>
<wire x1="-11.43" y1="3.81" x2="-11.43" y2="-3.81" width="0.127" layer="22"/>
<wire x1="13.97" y1="3.81" x2="14.605" y2="3.175" width="0.127" layer="22" curve="-53.130102"/>
<wire x1="14.605" y1="3.175" x2="14.605" y2="-3.175" width="0.127" layer="22"/>
<wire x1="3.81" y1="3.81" x2="13.97" y2="3.81" width="0.127" layer="22"/>
<wire x1="3.81" y1="3.81" x2="3.81" y2="12.7" width="0.127" layer="22"/>
<wire x1="-5.08" y1="-12.065" x2="-4.445" y2="-12.7" width="0.127" layer="22" curve="53.130102"/>
<wire x1="3.175" y1="-12.7" x2="3.81" y2="-12.065" width="0.127" layer="22" curve="90"/>
<wire x1="-5.08" y1="12.7" x2="-4.445" y2="13.335" width="0.127" layer="22" curve="-90"/>
<wire x1="3.175" y1="13.335" x2="3.81" y2="12.7" width="0.127" layer="22" curve="-53.130102"/>
<wire x1="3.175" y1="13.335" x2="-4.445" y2="13.335" width="0.127" layer="22"/>
<wire x1="-4.445" y1="-12.7" x2="3.175" y2="-12.7" width="0.127" layer="22"/>
<wire x1="-7.62" y1="3.81" x2="-7.62" y2="-3.81" width="0.127" layer="22"/>
<wire x1="-7.62" y1="-3.81" x2="-10.16" y2="-3.81" width="0.127" layer="22"/>
<wire x1="-10.16" y1="-3.81" x2="-10.16" y2="3.81" width="0.127" layer="22"/>
<wire x1="-10.16" y1="3.81" x2="-7.62" y2="3.81" width="0.127" layer="22"/>
<wire x1="0" y1="3.81" x2="0" y2="-3.81" width="0.127" layer="22"/>
<wire x1="0" y1="-3.81" x2="-2.54" y2="-3.81" width="0.127" layer="22"/>
<wire x1="-2.54" y1="-3.81" x2="-2.54" y2="3.81" width="0.127" layer="22"/>
<wire x1="-2.54" y1="3.81" x2="0" y2="3.81" width="0.127" layer="22"/>
<wire x1="3.175" y1="7.62" x2="-4.445" y2="7.62" width="0.127" layer="22"/>
<wire x1="-4.445" y1="7.62" x2="-4.445" y2="10.16" width="0.127" layer="22"/>
<wire x1="-4.445" y1="10.16" x2="3.175" y2="10.16" width="0.127" layer="22"/>
<wire x1="3.175" y1="10.16" x2="3.175" y2="7.62" width="0.127" layer="22"/>
<wire x1="3.175" y1="-10.16" x2="-4.445" y2="-10.16" width="0.127" layer="22"/>
<wire x1="-4.445" y1="-10.16" x2="-4.445" y2="-7.62" width="0.127" layer="22"/>
<wire x1="-4.445" y1="-7.62" x2="3.175" y2="-7.62" width="0.127" layer="22"/>
<wire x1="3.175" y1="-7.62" x2="3.175" y2="-10.16" width="0.127" layer="22"/>
<wire x1="13.335" y1="-1.27" x2="5.715" y2="-1.27" width="0.127" layer="51"/>
<wire x1="5.715" y1="-1.27" x2="5.715" y2="1.27" width="0.127" layer="51"/>
<wire x1="5.715" y1="1.27" x2="13.335" y2="1.27" width="0.127" layer="51"/>
<wire x1="13.335" y1="1.27" x2="13.335" y2="-1.27" width="0.127" layer="51"/>
<wire x1="3.81" y1="-12.065" x2="13.97" y2="-3.81" width="0.127" layer="22"/>
<wire x1="-10.795" y1="4.445" x2="-5.08" y2="12.7" width="0.127" layer="22"/>
<wire x1="-12.065" y1="12.7" x2="-10.795" y2="13.97" width="0.127" layer="22" curve="-90"/>
<wire x1="-12.065" y1="12.7" x2="-12.065" y2="-12.065" width="0.127" layer="22"/>
<wire x1="-12.065" y1="-12.065" x2="-10.795" y2="-13.335" width="0.127" layer="22" curve="67.380135"/>
<wire x1="-10.795" y1="-13.335" x2="13.97" y2="-13.335" width="0.127" layer="22"/>
<wire x1="13.97" y1="-13.335" x2="15.24" y2="-12.065" width="0.127" layer="22" curve="90"/>
<wire x1="15.24" y1="-12.065" x2="15.24" y2="12.7" width="0.127" layer="22"/>
<wire x1="13.97" y1="13.97" x2="-10.795" y2="13.97" width="0.127" layer="22"/>
<wire x1="13.97" y1="13.97" x2="15.24" y2="12.7" width="0.127" layer="22" curve="-90"/>
<pad name="87" x="-8.89" y="0" drill="2.54" diameter="5.08"/>
<pad name="30" x="9.017" y="0" drill="2.54" diameter="5.08"/>
<pad name="87A" x="-0.889" y="0" drill="2.54" diameter="5.08"/>
<pad name="86" x="-0.762" y="8.636" drill="2.54" diameter="5.08"/>
<pad name="85" x="-0.508" y="-8.509" drill="2.54" diameter="5.08"/>
<pad name="P$6" x="-6.985" y="6.477" drill="2.1844" diameter="2.54"/>
<pad name="P$7" x="6.985" y="-6.477" drill="2.1844" diameter="2.54"/>
<text x="-9.525" y="17.145" size="1.778" layer="26" ratio="10">&gt;NAME</text>
<text x="-9.652" y="14.605" size="1.778" layer="28" ratio="10">&gt;VALUE</text>
<rectangle x1="-12.7" y1="-9.525" x2="-11.43" y2="10.795" layer="22"/>
</package>
</packages>
<symbols>
<symbol name="K">
<wire x1="3.81" y1="-1.905" x2="3.81" y2="1.905" width="0.254" layer="94"/>
<wire x1="-3.81" y1="1.905" x2="-3.81" y2="-1.905" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.905" x2="3.81" y2="-1.905" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="1.905" x2="-3.81" y2="1.905" width="0.254" layer="94"/>
<wire x1="-3.81" y1="-1.905" x2="0" y2="-1.905" width="0.254" layer="94"/>
<wire x1="3.81" y1="1.905" x2="0" y2="1.905" width="0.254" layer="94"/>
<text x="1.27" y="2.921" size="1.524" layer="96">&gt;VALUE</text>
<text x="0.635" y="3.175" size="0.8636" layer="93">1</text>
<text x="0.635" y="-3.81" size="0.8636" layer="93">2</text>
<text x="-2.54" y="-0.635" size="1.27" layer="95">&gt;PART</text>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="1" x="0" y="5.08" visible="off" length="short" direction="pas" rot="R270"/>
</symbol>
<symbol name="U">
<wire x1="3.175" y1="5.08" x2="1.905" y2="5.08" width="0.254" layer="94"/>
<wire x1="-3.175" y1="5.08" x2="-1.905" y2="5.08" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="2.54" y2="5.715" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="0" y2="0" width="0.254" layer="94"/>
<circle x="0" y="1.27" radius="0.127" width="0.4064" layer="94"/>
<text x="0.635" y="0.635" size="0.8636" layer="93">P</text>
<text x="-2.54" y="3.81" size="0.8636" layer="93">S</text>
<text x="2.54" y="3.81" size="0.8636" layer="93">O</text>
<pin name="O" x="5.08" y="5.08" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="S" x="-5.08" y="5.08" visible="off" length="short" direction="pas"/>
<pin name="P" x="0" y="-2.54" visible="off" length="short" direction="pas" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="AUTORELE" prefix="RE" uservalue="yes">
<gates>
<gate name="A" symbol="K" x="0" y="-2.54"/>
<gate name="B" symbol="U" x="17.78" y="-5.08"/>
</gates>
<devices>
<device name="" package="RELEAUTO">
<connects>
<connect gate="A" pin="1" pad="86"/>
<connect gate="A" pin="2" pad="85"/>
<connect gate="B" pin="O" pad="87A"/>
<connect gate="B" pin="P" pad="30"/>
<connect gate="B" pin="S" pad="87"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="untitled">
<packages>
</packages>
<symbols>
<symbol name="POKUS">
<wire x1="-5.08" y1="-7.62" x2="-5.08" y2="-3.81" width="0.3048" layer="94"/>
<wire x1="-5.08" y1="-3.81" x2="-7.62" y2="3.81" width="0.3048" layer="94"/>
<wire x1="-5.08" y1="3.175" x2="-5.08" y2="7.62" width="0.3048" layer="94"/>
<wire x1="-10.16" y1="2.54" x2="-13.97" y2="2.54" width="0.3048" layer="94"/>
<wire x1="-13.97" y1="2.54" x2="-17.78" y2="2.54" width="0.3048" layer="94"/>
<wire x1="-17.78" y1="2.54" x2="-17.78" y2="-2.54" width="0.3048" layer="94"/>
<wire x1="-17.78" y1="-2.54" x2="-13.97" y2="-2.54" width="0.3048" layer="94"/>
<wire x1="-13.97" y1="-2.54" x2="-10.16" y2="-2.54" width="0.3048" layer="94"/>
<wire x1="-10.16" y1="-2.54" x2="-10.16" y2="0" width="0.3048" layer="94"/>
<wire x1="-10.16" y1="0" x2="-10.16" y2="2.54" width="0.3048" layer="94"/>
<wire x1="2.54" y1="-7.62" x2="2.54" y2="-3.81" width="0.3048" layer="94"/>
<wire x1="2.54" y1="-3.81" x2="0" y2="3.81" width="0.3048" layer="94"/>
<wire x1="2.54" y1="3.175" x2="2.54" y2="7.62" width="0.3048" layer="94"/>
<wire x1="10.16" y1="-7.62" x2="10.16" y2="-3.81" width="0.3048" layer="94"/>
<wire x1="10.16" y1="-3.81" x2="7.62" y2="3.81" width="0.3048" layer="94"/>
<wire x1="10.16" y1="3.175" x2="10.16" y2="7.62" width="0.3048" layer="94"/>
<wire x1="-13.97" y1="2.54" x2="-13.97" y2="5.08" width="0.3048" layer="94"/>
<wire x1="-13.97" y1="-2.54" x2="-13.97" y2="-5.08" width="0.3048" layer="94"/>
<wire x1="17.78" y1="-7.62" x2="17.78" y2="-3.81" width="0.3048" layer="94"/>
<wire x1="17.78" y1="-3.81" x2="20.32" y2="3.81" width="0.3048" layer="94"/>
<wire x1="17.78" y1="3.175" x2="17.78" y2="7.62" width="0.3048" layer="94"/>
<wire x1="17.78" y1="3.175" x2="20.955" y2="3.175" width="0.3048" layer="94"/>
<wire x1="-10.16" y1="0" x2="25.4" y2="0" width="0.3048" layer="94" style="shortdash"/>
<wire x1="45.72" y1="-7.62" x2="45.72" y2="-3.81" width="0.3048" layer="94"/>
<wire x1="45.72" y1="-3.81" x2="43.18" y2="3.81" width="0.3048" layer="94"/>
<wire x1="45.72" y1="3.175" x2="45.72" y2="7.62" width="0.3048" layer="94"/>
<wire x1="40.64" y1="2.54" x2="36.83" y2="2.54" width="0.3048" layer="94"/>
<wire x1="36.83" y1="2.54" x2="33.02" y2="2.54" width="0.3048" layer="94"/>
<wire x1="33.02" y1="2.54" x2="33.02" y2="-2.54" width="0.3048" layer="94"/>
<wire x1="33.02" y1="-2.54" x2="36.83" y2="-2.54" width="0.3048" layer="94"/>
<wire x1="36.83" y1="-2.54" x2="40.64" y2="-2.54" width="0.3048" layer="94"/>
<wire x1="40.64" y1="-2.54" x2="40.64" y2="0" width="0.3048" layer="94"/>
<wire x1="40.64" y1="0" x2="40.64" y2="2.54" width="0.3048" layer="94"/>
<wire x1="53.34" y1="-7.62" x2="53.34" y2="-3.81" width="0.3048" layer="94"/>
<wire x1="53.34" y1="-3.81" x2="50.8" y2="3.81" width="0.3048" layer="94"/>
<wire x1="53.34" y1="3.175" x2="53.34" y2="7.62" width="0.3048" layer="94"/>
<wire x1="60.96" y1="-7.62" x2="60.96" y2="-3.81" width="0.3048" layer="94"/>
<wire x1="60.96" y1="-3.81" x2="58.42" y2="3.81" width="0.3048" layer="94"/>
<wire x1="60.96" y1="3.175" x2="60.96" y2="7.62" width="0.3048" layer="94"/>
<wire x1="36.83" y1="2.54" x2="36.83" y2="5.08" width="0.3048" layer="94"/>
<wire x1="36.83" y1="-2.54" x2="36.83" y2="-5.08" width="0.3048" layer="94"/>
<wire x1="68.58" y1="-7.62" x2="68.58" y2="-3.81" width="0.3048" layer="94"/>
<wire x1="68.58" y1="-3.81" x2="69.85" y2="0" width="0.3048" layer="94"/>
<wire x1="69.85" y1="0" x2="71.12" y2="3.81" width="0.3048" layer="94"/>
<wire x1="68.58" y1="3.175" x2="68.58" y2="7.62" width="0.3048" layer="94"/>
<wire x1="68.58" y1="3.175" x2="71.755" y2="3.175" width="0.3048" layer="94"/>
<wire x1="40.64" y1="0" x2="69.85" y2="0" width="0.3048" layer="94" style="shortdash"/>
<wire x1="24.765" y1="1.27" x2="29.845" y2="1.27" width="0.3048" layer="94"/>
<wire x1="27.305" y1="-1.27" x2="29.845" y2="1.27" width="0.3048" layer="94"/>
<wire x1="24.765" y1="1.27" x2="27.305" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="29.21" y1="0" x2="32.385" y2="0" width="0.3048" layer="94" style="shortdash"/>
<text x="-20.32" y="5.08" size="1.778" layer="95">Q1</text>
<text x="30.48" y="5.08" size="1.778" layer="95">Q2</text>
<text x="-12.7" y="3.81" size="1.778" layer="95">A1</text>
<text x="38.1" y="3.81" size="1.778" layer="95">A1</text>
<text x="-12.7" y="-5.08" size="1.778" layer="95">A2</text>
<text x="38.1" y="-5.08" size="1.778" layer="95">A2</text>
<text x="-3.81" y="6.35" size="1.778" layer="95">1</text>
<text x="46.99" y="6.35" size="1.778" layer="95">1</text>
<text x="3.81" y="6.35" size="1.778" layer="95">3</text>
<text x="54.61" y="6.35" size="1.778" layer="95">3</text>
<text x="11.43" y="6.35" size="1.778" layer="95">5</text>
<text x="62.23" y="6.35" size="1.778" layer="95">5</text>
<text x="-3.81" y="-7.62" size="1.778" layer="95">2</text>
<text x="46.99" y="-7.62" size="1.778" layer="95">2</text>
<text x="3.81" y="-7.62" size="1.778" layer="95">4</text>
<text x="54.61" y="-7.62" size="1.778" layer="95">4</text>
<text x="11.43" y="-7.62" size="1.778" layer="95">6</text>
<text x="62.23" y="-7.62" size="1.778" layer="95">6</text>
<text x="19.05" y="6.35" size="1.778" layer="95">122</text>
<text x="69.85" y="6.35" size="1.778" layer="95">111</text>
<text x="19.05" y="-7.62" size="1.778" layer="95">121</text>
<text x="69.85" y="-7.62" size="1.778" layer="95">112</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="POKUS">
<gates>
<gate name="G$1" symbol="POKUS" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="U$2" library="FRAMES" deviceset="TAB" device="" value="TAB"/>
<part name="U$4" library="SUPPLY2" deviceset="PE" device=""/>
<part name="V5" library="SUPPLY2" deviceset="V--&gt;" device="" value="EL_MOTOR"/>
<part name="V9" library="SUPPLY2" deviceset="V--&gt;" device="" value="CON1"/>
<part name="V7" library="SUPPLY2" deviceset="V--&gt;" device="" value="BACK UP"/>
<part name="D16" library="SMDA" deviceset="DIOD-MELF" device="" value="30BQ60"/>
<part name="D20" library="SMDA" deviceset="DIOD-MELF" device="" value="30BQ60"/>
<part name="S105" library="SKLADKA2" deviceset="SPINAC1" device="" value="ODPOJOVAC"/>
<part name="D23" library="SMDA" deviceset="DIOD-MELF" device="" value="30BQ60"/>
<part name="D24" library="SMDA" deviceset="DIOD-MELF" device="" value="30BQ60"/>
<part name="S106" library="SKLADKA2" deviceset="SPINAC1" device="" value="ODPOJOVAC"/>
<part name="RE4" library="RELAIS" deviceset="AUTORELE" device="" value="AUTORELE"/>
<part name="S7" library="SKLADKA2" deviceset="SPINAC1" device="" value="ODPOJOVAC"/>
<part name="S8" library="SKLADKA2" deviceset="SPINAC1" device="" value="ODPOJOVAC"/>
<part name="U$47" library="SUPPLY2" deviceset="GND" device=""/>
<part name="RE1200" library="RELAIS" deviceset="AUTORELE" device="" value="AUTORELE"/>
<part name="RE1" library="RELAIS" deviceset="AUTORELE" device="" value="AUTORELE"/>
<part name="U$73" library="untitled" deviceset="POKUS" device=""/>
<part name="GND2" library="SUPPLY2" deviceset="GND" device=""/>
<part name="GND1" library="SUPPLY2" deviceset="GND" device=""/>
<part name="U$1" library="SUPPLY2" deviceset="PE" device=""/>
<part name="S9" library="SKLADKA2" deviceset="SPINAC1" device="" value="ODPOJOVAC"/>
<part name="S10" library="SKLADKA2" deviceset="SPINAC1" device="" value="ODPOJOVAC"/>
<part name="Z3" library="SKLADKA2" deviceset="ZAROVKA" device="" value="asd"/>
<part name="U$60" library="SUPPLY2" deviceset="GND" device=""/>
<part name="U$3" library="SUPPLY2" deviceset="GND" device=""/>
<part name="U$5" library="SUPPLY2" deviceset="GND" device=""/>
<part name="U$6" library="SUPPLY2" deviceset="PE" device=""/>
</parts>
<sheets>
<sheet>
<plain>
<wire x1="-116.84" y1="150.495" x2="-342.265" y2="150.495" width="0.1524" layer="94"/>
<wire x1="-342.265" y1="150.495" x2="-342.265" y2="605.79" width="0.1524" layer="94"/>
<wire x1="-342.265" y1="605.79" x2="-15.24" y2="605.79" width="0.1524" layer="94"/>
<wire x1="-15.24" y1="605.79" x2="-15.24" y2="186.055" width="0.1524" layer="94"/>
<wire x1="-185.7375" y1="467.0425" x2="-184.785" y2="467.0425" width="0.3048" layer="94"/>
<wire x1="-175.5775" y1="468.3125" x2="-176.53" y2="468.3125" width="0.3048" layer="94"/>
<wire x1="-159.0675" y1="467.0425" x2="-160.02" y2="467.0425" width="0.3048" layer="94"/>
<wire x1="-169.2275" y1="468.3125" x2="-168.275" y2="468.3125" width="0.3048" layer="94"/>
<wire x1="-193.9925" y1="481.6475" x2="-183.8325" y2="481.6475" width="0.4064" layer="94"/>
<wire x1="-183.8325" y1="481.6475" x2="-172.4025" y2="481.6475" width="0.4064" layer="94"/>
<wire x1="-172.4025" y1="481.6475" x2="-160.9725" y2="481.6475" width="0.4064" layer="94"/>
<wire x1="-160.9725" y1="481.6475" x2="-139.3825" y2="481.6475" width="0.4064" layer="94"/>
<wire x1="-139.3825" y1="481.6475" x2="-131.7625" y2="481.6475" width="0.4064" layer="94"/>
<wire x1="-131.7625" y1="481.6475" x2="-119.0625" y2="481.6475" width="0.4064" layer="94"/>
<wire x1="-119.0625" y1="481.6475" x2="-98.7425" y2="481.6475" width="0.4064" layer="94"/>
<wire x1="-98.7425" y1="481.6475" x2="-86.0425" y2="481.6475" width="0.4064" layer="94"/>
<wire x1="-86.0425" y1="481.6475" x2="-86.0425" y2="438.4675" width="0.4064" layer="94"/>
<wire x1="-86.0425" y1="438.4675" x2="-92.3925" y2="438.4675" width="0.4064" layer="94"/>
<wire x1="-92.3925" y1="438.4675" x2="-105.0925" y2="438.4675" width="0.4064" layer="94"/>
<wire x1="-105.0925" y1="438.4675" x2="-111.4425" y2="438.4675" width="0.4064" layer="94"/>
<wire x1="-111.4425" y1="438.4675" x2="-119.0625" y2="438.4675" width="0.4064" layer="94"/>
<wire x1="-119.0625" y1="438.4675" x2="-139.3825" y2="438.4675" width="0.4064" layer="94"/>
<wire x1="-139.3825" y1="438.4675" x2="-160.9725" y2="438.4675" width="0.4064" layer="94"/>
<wire x1="-160.9725" y1="438.4675" x2="-172.4025" y2="438.4675" width="0.4064" layer="94"/>
<wire x1="-172.4025" y1="438.4675" x2="-183.8325" y2="438.4675" width="0.4064" layer="94"/>
<wire x1="-183.8325" y1="438.4675" x2="-193.9925" y2="438.4675" width="0.4064" layer="94"/>
<wire x1="-144.4625" y1="472.7575" x2="-144.4625" y2="448.6275" width="0.1524" layer="94"/>
<wire x1="-144.4625" y1="448.6275" x2="-126.6825" y2="448.6275" width="0.1524" layer="94"/>
<wire x1="-126.6825" y1="448.6275" x2="-126.6825" y2="472.7575" width="0.1524" layer="94"/>
<wire x1="-126.6825" y1="472.7575" x2="-144.4625" y2="472.7575" width="0.1524" layer="94"/>
<wire x1="-183.8325" y1="481.6475" x2="-183.8325" y2="484.1875" width="0.3048" layer="94"/>
<wire x1="-172.4025" y1="481.6475" x2="-172.4025" y2="484.1875" width="0.3048" layer="94"/>
<wire x1="-160.9725" y1="481.6475" x2="-160.9725" y2="484.1875" width="0.3048" layer="94"/>
<wire x1="-183.8325" y1="438.4675" x2="-183.8325" y2="435.9275" width="0.3048" layer="94"/>
<wire x1="-172.4025" y1="438.4675" x2="-172.4025" y2="435.9275" width="0.3048" layer="94"/>
<wire x1="-160.9725" y1="438.4675" x2="-160.9725" y2="435.9275" width="0.3048" layer="94"/>
<wire x1="-139.3825" y1="438.4675" x2="-139.3825" y2="435.9275" width="0.3048" layer="94"/>
<wire x1="-139.3825" y1="481.6475" x2="-139.3825" y2="484.1875" width="0.3048" layer="94"/>
<wire x1="-193.9925" y1="438.4675" x2="-193.9925" y2="481.6475" width="0.4064" layer="94"/>
<wire x1="-119.0625" y1="481.6475" x2="-119.0625" y2="484.1875" width="0.3048" layer="94"/>
<wire x1="-119.0625" y1="438.4675" x2="-119.0625" y2="435.9275" width="0.3048" layer="94"/>
<wire x1="-111.4425" y1="438.4675" x2="-111.4425" y2="435.9275" width="0.3048" layer="94"/>
<wire x1="-105.0925" y1="438.4675" x2="-105.0925" y2="435.9275" width="0.3048" layer="94"/>
<wire x1="-92.3925" y1="438.4675" x2="-92.3925" y2="435.9275" width="0.3048" layer="94"/>
<wire x1="-98.7425" y1="481.6475" x2="-98.7425" y2="484.1875" width="0.3048" layer="94"/>
<wire x1="-131.7625" y1="481.6475" x2="-131.7625" y2="484.1875" width="0.3048" layer="94"/>
<wire x1="-251.7775" y1="503.2375" x2="-245.4275" y2="503.2375" width="0.3048" layer="94"/>
<wire x1="-251.7775" y1="508.3175" x2="-245.4275" y2="508.3175" width="0.3048" layer="94"/>
<wire x1="-251.7775" y1="513.3975" x2="-245.4275" y2="513.3975" width="0.3048" layer="94"/>
<wire x1="-251.7775" y1="518.4775" x2="-245.4275" y2="518.4775" width="0.3048" layer="94"/>
<wire x1="-247.9675" y1="523.5575" x2="-245.4275" y2="523.5575" width="0.3048" layer="94"/>
<wire x1="-240.3475" y1="503.2375" x2="-245.745" y2="501.015" width="0.3048" layer="94"/>
<wire x1="-240.3475" y1="508.3175" x2="-245.745" y2="506.095" width="0.3048" layer="94"/>
<wire x1="-240.3475" y1="513.3975" x2="-245.745" y2="511.175" width="0.3048" layer="94"/>
<wire x1="-240.3475" y1="518.4775" x2="-245.745" y2="516.255" width="0.3048" layer="94"/>
<wire x1="-240.3475" y1="523.5575" x2="-245.745" y2="521.335" width="0.3048" layer="94"/>
<wire x1="-240.3475" y1="503.2375" x2="-225.1075" y2="503.2375" width="0.3048" layer="94"/>
<wire x1="-240.3475" y1="508.3175" x2="-225.1075" y2="508.3175" width="0.3048" layer="94"/>
<wire x1="-240.3475" y1="513.3975" x2="-228.2825" y2="513.3975" width="0.3048" layer="94"/>
<wire x1="-228.2825" y1="513.3975" x2="-225.1075" y2="513.3975" width="0.3048" layer="94"/>
<wire x1="-240.3475" y1="518.4775" x2="-237.49" y2="518.4775" width="0.3048" layer="94"/>
<wire x1="-237.49" y1="518.4775" x2="-225.1075" y2="518.4775" width="0.3048" layer="94"/>
<wire x1="-240.3475" y1="523.5575" x2="-237.49" y2="523.5575" width="0.3048" layer="94"/>
<wire x1="-237.49" y1="518.4775" x2="-237.49" y2="523.5575" width="0.3048" layer="94"/>
<wire x1="-247.9675" y1="523.5575" x2="-247.9675" y2="527.3675" width="0.3048" layer="94"/>
<wire x1="-228.2825" y1="527.3675" x2="-228.2825" y2="513.3975" width="0.3048" layer="94"/>
<wire x1="-230.8225" y1="501.65" x2="-232.41" y2="501.65" width="0.3048" layer="94"/>
<wire x1="-232.41" y1="501.65" x2="-233.9975" y2="501.65" width="0.3048" layer="94"/>
<wire x1="-233.9975" y1="501.65" x2="-233.9975" y2="520.065" width="0.3048" layer="94"/>
<wire x1="-233.9975" y1="520.065" x2="-230.8225" y2="520.065" width="0.3048" layer="94"/>
<wire x1="-230.8225" y1="520.065" x2="-230.8225" y2="502.6025" width="0.3048" layer="94"/>
<wire x1="-230.8225" y1="502.6025" x2="-230.8225" y2="501.65" width="0.3048" layer="94"/>
<wire x1="-232.41" y1="501.65" x2="-232.41" y2="498.1575" width="0.1524" layer="94"/>
<wire x1="-232.41" y1="498.1575" x2="-228.2825" y2="498.1575" width="0.3048" layer="94"/>
<wire x1="-228.2825" y1="498.1575" x2="-228.2825" y2="494.3475" width="0.3048" layer="94"/>
<wire x1="-228.2825" y1="494.3475" x2="-232.41" y2="494.3475" width="0.3048" layer="94"/>
<wire x1="-232.41" y1="494.3475" x2="-236.5375" y2="494.3475" width="0.3048" layer="94"/>
<wire x1="-236.5375" y1="494.3475" x2="-236.5375" y2="498.1575" width="0.3048" layer="94"/>
<wire x1="-236.5375" y1="498.1575" x2="-232.41" y2="498.1575" width="0.3048" layer="94"/>
<wire x1="-232.41" y1="494.3475" x2="-232.41" y2="493.0775" width="0.3048" layer="94"/>
<wire x1="-232.41" y1="493.0775" x2="-227.0125" y2="493.0775" width="0.3048" layer="94"/>
<wire x1="-227.0125" y1="493.0775" x2="-227.0125" y2="499.11" width="0.3048" layer="94"/>
<wire x1="-227.0125" y1="499.11" x2="-230.8225" y2="502.6025" width="0.3048" layer="94"/>
<wire x1="-241.6175" y1="498.1575" x2="-243.5225" y2="498.1575" width="0.3048" layer="94"/>
<wire x1="-243.5225" y1="498.1575" x2="-245.4275" y2="498.1575" width="0.3048" layer="94"/>
<wire x1="-245.4275" y1="498.1575" x2="-245.4275" y2="496.2525" width="0.3048" layer="94"/>
<wire x1="-245.4275" y1="496.2525" x2="-245.4275" y2="494.3475" width="0.3048" layer="94"/>
<wire x1="-245.4275" y1="494.3475" x2="-243.5225" y2="494.3475" width="0.3048" layer="94"/>
<wire x1="-243.5225" y1="494.3475" x2="-241.6175" y2="494.3475" width="0.3048" layer="94"/>
<wire x1="-241.6175" y1="494.3475" x2="-241.6175" y2="496.2525" width="0.3048" layer="94"/>
<wire x1="-241.6175" y1="496.2525" x2="-241.6175" y2="498.1575" width="0.3048" layer="94"/>
<wire x1="-236.5375" y1="496.2525" x2="-241.6175" y2="496.2525" width="0.3048" layer="94" style="shortdash"/>
<wire x1="-243.5225" y1="493.7125" x2="-243.5225" y2="494.3475" width="0.3048" layer="94" style="shortdash"/>
<wire x1="-245.4275" y1="496.2525" x2="-246.0625" y2="496.2525" width="0.3048" layer="94" style="shortdash"/>
<wire x1="-243.5225" y1="498.1575" x2="-243.5225" y2="522.2875" width="0.3048" layer="94" style="shortdash"/>
<wire x1="-243.5225" y1="495.6175" x2="-243.5225" y2="496.2525" width="0.3048" layer="94" style="shortdash"/>
<wire x1="-243.5225" y1="496.2525" x2="-243.5225" y2="496.8875" width="0.3048" layer="94" style="shortdash"/>
<wire x1="-243.5225" y1="496.2525" x2="-244.1575" y2="496.2525" width="0.3048" layer="94" style="shortdash"/>
<wire x1="-242.8875" y1="496.2525" x2="-243.5225" y2="496.2525" width="0.3048" layer="94" style="shortdash"/>
<wire x1="-246.0625" y1="526.415" x2="-246.0625" y2="527.3675" width="0.3048" layer="94"/>
<wire x1="-246.0625" y1="527.3675" x2="-246.0625" y2="528.32" width="0.3048" layer="94"/>
<wire x1="-246.0625" y1="528.32" x2="-239.0775" y2="528.32" width="0.3048" layer="94"/>
<wire x1="-239.0775" y1="528.32" x2="-239.0775" y2="527.3675" width="0.3048" layer="94"/>
<wire x1="-239.0775" y1="527.3675" x2="-239.0775" y2="526.415" width="0.3048" layer="94"/>
<wire x1="-239.0775" y1="526.415" x2="-246.0625" y2="526.415" width="0.3048" layer="94"/>
<wire x1="-247.9675" y1="527.3675" x2="-246.0625" y2="527.3675" width="0.3048" layer="94"/>
<wire x1="-239.0775" y1="527.3675" x2="-234.6325" y2="527.3675" width="0.3048" layer="94"/>
<wire x1="-228.2825" y1="527.3675" x2="-229.5525" y2="527.3675" width="0.3048" layer="94"/>
<wire x1="-234.95" y1="529.59" x2="-229.5525" y2="527.3675" width="0.3048" layer="94"/>
<wire x1="-232.7275" y1="528.6375" x2="-232.7275" y2="529.59" width="0.3048" layer="94"/>
<wire x1="-232.7275" y1="530.225" x2="-232.7275" y2="531.1775" width="0.3048" layer="94"/>
<wire x1="-233.9975" y1="531.1775" x2="-231.4575" y2="531.1775" width="0.3048" layer="94"/>
<wire x1="-231.4575" y1="531.1775" x2="-231.4575" y2="530.5425" width="0.3048" layer="94"/>
<wire x1="-233.9975" y1="531.1775" x2="-233.9975" y2="530.5425" width="0.3048" layer="94"/>
<circle x="-228.2825" y="513.3975" radius="0.1588" width="0.6096" layer="94"/>
<circle x="-237.49" y="518.4775" radius="0.1588" width="0.6096" layer="94"/>
<text x="-82.55" y="161.925" size="1.9304" layer="94">07</text>
<text x="-92.71" y="178.435" size="3.81" layer="94">400VAC ELMOT BACKUP</text>
<text x="-309.88" y="419.1" size="2.54" layer="104" rot="MR180">MACHINE ROOM</text>
<text x="-309.245" y="248.92" size="2.54" layer="104">400V AC</text>
<text x="-309.88" y="574.3575" size="2.54" layer="104">WIRING BOX</text>
<text x="-309.245" y="254.3175" size="2.54" layer="104">INLET</text>
<text x="-132.08" y="414.02" size="2.1844" layer="97">FINAL_SETTING:</text>
<text x="-132.08" y="408.94" size="1.778" layer="97">CURRENT LIMITING - 4</text>
<text x="-132.08" y="406.4" size="1.778" layer="97">RAMP UP TIME - 10s</text>
<text x="-132.08" y="403.86" size="1.778" layer="97">STARTING VOLTAGE - 50%</text>
<text x="-132.08" y="401.32" size="1.778" layer="97">RAMP DOWN TIME - 0s</text>
<text x="-132.08" y="398.78" size="1.778" layer="97">TRIP CLASS - 10</text>
<text x="-132.08" y="396.24" size="1.778" layer="97">MOTOR CURRENT - 33A</text>
<text x="-182.5625" y="479.1075" size="1.778" layer="95">1/L1</text>
<text x="-171.1325" y="479.1075" size="1.778" layer="95">3/L2</text>
<text x="-159.7025" y="479.1075" size="1.778" layer="95">5/L3</text>
<text x="-182.5625" y="439.7375" size="1.778" layer="95">2/T1</text>
<text x="-171.1325" y="439.7375" size="1.778" layer="95">4/T2</text>
<text x="-159.7025" y="439.7375" size="1.778" layer="95">6/T3</text>
<text x="-138.1125" y="479.1075" size="1.778" layer="95">A1</text>
<text x="-138.1125" y="439.7375" size="1.778" layer="95">A2</text>
<text x="-117.7925" y="479.1075" size="1.778" layer="95">14/24</text>
<text x="-97.4725" y="479.1075" size="1.778" layer="95">95</text>
<text x="-103.8225" y="439.7375" size="1.778" layer="95">96</text>
<text x="-110.1725" y="439.7375" size="1.778" layer="95">23</text>
<text x="-91.1225" y="439.7375" size="1.778" layer="95">98</text>
<text x="-117.7925" y="439.7375" size="1.778" layer="95">13</text>
<text x="-116.5225" y="443.5475" size="1.778" layer="95" rot="R90">ON/RUN</text>
<text x="-108.9025" y="443.5475" size="1.778" layer="95" rot="R90">BYPASED</text>
<text x="-89.8525" y="442.2775" size="1.778" layer="95" rot="R90">OVERLOAD/FAILURE</text>
<text x="-202.2475" y="429.5775" size="2.54" layer="95">Q3</text>
<text x="-130.4925" y="479.1075" size="1.778" layer="95">1 IN</text>
<text x="-201.93" y="425.1325" size="2.54" layer="95">3RW4037-1BB04</text>
<text x="-249.2375" y="500.6975" size="1.778" layer="94" rot="R90">1</text>
<text x="-225.7425" y="500.6975" size="1.778" layer="94" rot="R90">2</text>
<text x="-249.2375" y="505.7775" size="1.778" layer="94" rot="R90">3</text>
<text x="-225.7425" y="505.7775" size="1.778" layer="94" rot="R90">4</text>
<text x="-249.2375" y="510.8575" size="1.778" layer="94" rot="R90">5</text>
<text x="-225.7425" y="510.8575" size="1.778" layer="94" rot="R90">6</text>
<text x="-249.2375" y="515.9375" size="1.778" layer="94" rot="R90">N</text>
<text x="-225.7425" y="515.9375" size="1.778" layer="94" rot="R90">N</text>
<text x="-246.38" y="537.5275" size="2.54" layer="95">FI1</text>
<text x="-246.38" y="534.035" size="2.54" layer="95">40A/100mA</text>
<wire x1="-217.805" y1="513.3975" x2="-210.185" y2="513.3975" width="0.3048" layer="94"/>
<wire x1="-210.82" y1="511.4925" x2="-205.105" y2="513.3975" width="0.3048" layer="94"/>
<wire x1="-205.105" y1="513.3975" x2="-201.93" y2="513.3975" width="0.3048" layer="94"/>
<wire x1="-201.93" y1="513.3975" x2="-201.93" y2="515.3025" width="0.3048" layer="94"/>
<wire x1="-201.93" y1="515.3025" x2="-199.39" y2="515.3025" width="0.3048" layer="94"/>
<wire x1="-199.39" y1="515.3025" x2="-199.39" y2="513.3975" width="0.3048" layer="94"/>
<wire x1="-199.39" y1="513.3975" x2="-197.485" y2="513.3975" width="0.3048" layer="94"/>
<wire x1="-196.215" y1="512.7625" x2="-192.405" y2="512.7625" width="0.3048" layer="94"/>
<wire x1="-195.58" y1="513.3975" x2="-194.31" y2="515.9375" width="0.3048" layer="94"/>
<wire x1="-194.31" y1="515.9375" x2="-193.04" y2="513.3975" width="0.3048" layer="94"/>
<wire x1="-203.835" y1="511.4925" x2="-203.835" y2="516.5725" width="0.3048" layer="94"/>
<wire x1="-203.835" y1="516.5725" x2="-197.485" y2="516.5725" width="0.3048" layer="94"/>
<wire x1="-197.485" y1="516.5725" x2="-197.485" y2="511.4925" width="0.3048" layer="94"/>
<wire x1="-191.135" y1="511.4925" x2="-191.135" y2="513.3975" width="0.3048" layer="94"/>
<wire x1="-191.135" y1="513.3975" x2="-191.135" y2="516.5725" width="0.3048" layer="94"/>
<wire x1="-191.135" y1="516.5725" x2="-197.485" y2="516.5725" width="0.3048" layer="94"/>
<wire x1="-191.135" y1="513.3975" x2="-187.325" y2="513.3975" width="0.3048" layer="94"/>
<wire x1="-217.805" y1="508.3175" x2="-210.185" y2="508.3175" width="0.3048" layer="94"/>
<wire x1="-210.82" y1="506.4125" x2="-205.105" y2="508.3175" width="0.3048" layer="94"/>
<wire x1="-205.105" y1="508.3175" x2="-201.93" y2="508.3175" width="0.3048" layer="94"/>
<wire x1="-201.93" y1="508.3175" x2="-201.93" y2="510.2225" width="0.3048" layer="94"/>
<wire x1="-201.93" y1="510.2225" x2="-199.39" y2="510.2225" width="0.3048" layer="94"/>
<wire x1="-199.39" y1="510.2225" x2="-199.39" y2="508.3175" width="0.3048" layer="94"/>
<wire x1="-199.39" y1="508.3175" x2="-197.485" y2="508.3175" width="0.3048" layer="94"/>
<wire x1="-196.215" y1="507.6825" x2="-192.405" y2="507.6825" width="0.3048" layer="94"/>
<wire x1="-195.58" y1="508.3175" x2="-194.31" y2="510.8575" width="0.3048" layer="94"/>
<wire x1="-194.31" y1="510.8575" x2="-193.04" y2="508.3175" width="0.3048" layer="94"/>
<wire x1="-203.835" y1="506.4125" x2="-203.835" y2="511.4925" width="0.3048" layer="94"/>
<wire x1="-203.835" y1="511.4925" x2="-197.485" y2="511.4925" width="0.3048" layer="94"/>
<wire x1="-197.485" y1="511.4925" x2="-197.485" y2="506.4125" width="0.3048" layer="94"/>
<wire x1="-191.135" y1="506.4125" x2="-191.135" y2="508.3175" width="0.3048" layer="94"/>
<wire x1="-191.135" y1="508.3175" x2="-191.135" y2="511.4925" width="0.3048" layer="94"/>
<wire x1="-191.135" y1="511.4925" x2="-197.485" y2="511.4925" width="0.3048" layer="94"/>
<wire x1="-191.135" y1="508.3175" x2="-187.325" y2="508.3175" width="0.3048" layer="94"/>
<wire x1="-217.805" y1="503.2375" x2="-210.185" y2="503.2375" width="0.3048" layer="94"/>
<wire x1="-210.82" y1="501.3325" x2="-205.105" y2="503.2375" width="0.3048" layer="94"/>
<wire x1="-205.105" y1="503.2375" x2="-201.93" y2="503.2375" width="0.3048" layer="94"/>
<wire x1="-201.93" y1="503.2375" x2="-201.93" y2="505.1425" width="0.3048" layer="94"/>
<wire x1="-201.93" y1="505.1425" x2="-199.39" y2="505.1425" width="0.3048" layer="94"/>
<wire x1="-199.39" y1="505.1425" x2="-199.39" y2="503.2375" width="0.3048" layer="94"/>
<wire x1="-199.39" y1="503.2375" x2="-197.485" y2="503.2375" width="0.3048" layer="94"/>
<wire x1="-196.215" y1="502.6025" x2="-192.405" y2="502.6025" width="0.3048" layer="94"/>
<wire x1="-195.58" y1="503.2375" x2="-194.31" y2="505.7775" width="0.3048" layer="94"/>
<wire x1="-194.31" y1="505.7775" x2="-193.04" y2="503.2375" width="0.3048" layer="94"/>
<wire x1="-203.835" y1="501.3325" x2="-203.835" y2="506.4125" width="0.3048" layer="94"/>
<wire x1="-203.835" y1="506.4125" x2="-197.485" y2="506.4125" width="0.3048" layer="94"/>
<wire x1="-197.485" y1="506.4125" x2="-197.485" y2="501.3325" width="0.3048" layer="94"/>
<wire x1="-197.485" y1="501.3325" x2="-200.66" y2="501.3325" width="0.3048" layer="94"/>
<wire x1="-200.66" y1="501.3325" x2="-203.835" y2="501.3325" width="0.3048" layer="94"/>
<wire x1="-197.485" y1="501.3325" x2="-194.31" y2="501.3325" width="0.3048" layer="94"/>
<wire x1="-194.31" y1="501.3325" x2="-191.135" y2="501.3325" width="0.3048" layer="94"/>
<wire x1="-191.135" y1="501.3325" x2="-191.135" y2="503.2375" width="0.3048" layer="94"/>
<wire x1="-191.135" y1="503.2375" x2="-191.135" y2="506.4125" width="0.3048" layer="94"/>
<wire x1="-191.135" y1="506.4125" x2="-197.485" y2="506.4125" width="0.3048" layer="94"/>
<wire x1="-191.135" y1="503.2375" x2="-187.325" y2="503.2375" width="0.3048" layer="94"/>
<wire x1="-208.28" y1="512.1275" x2="-208.28" y2="499.4275" width="0.1524" layer="94" style="shortdash"/>
<wire x1="-200.66" y1="501.3325" x2="-200.66" y2="497.5225" width="0.1524" layer="94" style="shortdash"/>
<wire x1="-200.66" y1="497.5225" x2="-194.31" y2="497.5225" width="0.1524" layer="94" style="shortdash"/>
<wire x1="-194.31" y1="497.5225" x2="-194.31" y2="501.3325" width="0.1524" layer="94" style="shortdash"/>
<wire x1="-200.66" y1="497.5225" x2="-205.74" y2="497.5225" width="0.1524" layer="94" style="shortdash"/>
<wire x1="-210.185" y1="499.4275" x2="-208.28" y2="499.4275" width="0.3048" layer="94"/>
<wire x1="-208.28" y1="499.4275" x2="-206.375" y2="499.4275" width="0.3048" layer="94"/>
<wire x1="-206.375" y1="499.4275" x2="-206.375" y2="495.6175" width="0.3048" layer="94"/>
<wire x1="-206.375" y1="495.6175" x2="-210.185" y2="495.6175" width="0.3048" layer="94"/>
<wire x1="-210.185" y1="495.6175" x2="-210.185" y2="499.4275" width="0.3048" layer="94"/>
<wire x1="-208.28" y1="499.4275" x2="-208.28" y2="493.7125" width="0.1524" layer="94"/>
<wire x1="-208.28" y1="493.7125" x2="-207.01" y2="493.7125" width="0.1524" layer="94"/>
<wire x1="-208.28" y1="493.7125" x2="-209.55" y2="493.7125" width="0.1524" layer="94"/>
<wire x1="-211.455" y1="497.5225" x2="-205.74" y2="497.5225" width="0.1524" layer="94"/>
<text x="-215.5825" y="501.015" size="1.778" layer="94" rot="R90">1</text>
<text x="-215.5825" y="506.095" size="1.778" layer="94" rot="R90">3</text>
<text x="-215.5825" y="511.175" size="1.778" layer="94" rot="R90">5</text>
<text x="-187.6425" y="501.015" size="1.778" layer="94" rot="R90">2</text>
<text x="-187.6425" y="506.095" size="1.778" layer="94" rot="R90">4</text>
<text x="-187.6425" y="511.175" size="1.778" layer="94" rot="R90">6</text>
<text x="-208.28" y="537.5275" size="2.54" layer="95">FA14</text>
<wire x1="-210.82" y1="269.875" x2="-205.74" y2="269.875" width="0.4064" layer="94"/>
<wire x1="-205.74" y1="269.875" x2="-198.12" y2="269.875" width="0.4064" layer="94"/>
<wire x1="-198.12" y1="269.875" x2="-198.12" y2="262.255" width="0.4064" layer="94"/>
<wire x1="-198.12" y1="262.255" x2="-200.66" y2="262.255" width="0.4064" layer="94"/>
<wire x1="-200.66" y1="262.255" x2="-205.74" y2="262.255" width="0.4064" layer="94"/>
<wire x1="-205.74" y1="262.255" x2="-208.28" y2="262.255" width="0.4064" layer="94"/>
<wire x1="-208.28" y1="262.255" x2="-213.36" y2="262.255" width="0.4064" layer="94"/>
<wire x1="-213.36" y1="262.255" x2="-213.36" y2="269.875" width="0.4064" layer="94"/>
<wire x1="-213.36" y1="269.875" x2="-208.28" y2="269.875" width="0.4064" layer="94"/>
<wire x1="-208.28" y1="269.875" x2="-208.28" y2="262.255" width="0.4064" layer="94"/>
<wire x1="-205.74" y1="269.875" x2="-205.74" y2="273.05" width="0.4064" layer="94"/>
<wire x1="-205.74" y1="262.255" x2="-205.74" y2="259.08" width="0.4064" layer="94"/>
<wire x1="-200.66" y1="262.255" x2="-200.66" y2="259.08" width="0.4064" layer="94"/>
<wire x1="-194.945" y1="266.065" x2="-193.675" y2="266.065" width="0.1524" layer="94"/>
<wire x1="-192.405" y1="266.065" x2="-191.135" y2="266.065" width="0.1524" layer="94"/>
<wire x1="-189.865" y1="266.065" x2="-188.595" y2="266.065" width="0.1524" layer="94"/>
<wire x1="-182.245" y1="266.065" x2="-180.975" y2="266.065" width="0.1524" layer="94"/>
<wire x1="-179.705" y1="266.065" x2="-178.435" y2="266.065" width="0.1524" layer="94"/>
<wire x1="-184.785" y1="266.065" x2="-183.515" y2="266.065" width="0.1524" layer="94"/>
<wire x1="-177.165" y1="266.065" x2="-175.895" y2="266.065" width="0.1524" layer="94"/>
<wire x1="-174.625" y1="266.065" x2="-173.355" y2="266.065" width="0.1524" layer="94"/>
<text x="-212.09" y="267.335" size="1.778" layer="95">3~</text>
<text x="-205.74" y="264.795" size="2.1844" layer="95">&lt;U</text>
<text x="-203.835" y="259.715" size="1.778" layer="95">L1</text>
<text x="-208.915" y="259.715" size="1.778" layer="95">L2</text>
<text x="-208.915" y="270.51" size="1.778" layer="95">L3</text>
<text x="-191.135" y="260.35" size="1.778" layer="95">11</text>
<text x="-193.04" y="268.605" size="1.778" layer="95">14</text>
<text x="-184.785" y="268.605" size="1.778" layer="95">12</text>
<text x="-216.2175" y="281.94" size="1.778" layer="95">RE104</text>
<text x="-216.2175" y="278.13" size="1.778" layer="95">3UG46 17</text>
<text x="-168.91" y="268.605" size="1.778" layer="95">22</text>
<text x="-177.8" y="268.605" size="1.778" layer="95">24</text>
<text x="-175.895" y="260.35" size="1.778" layer="95">21</text>
<wire x1="-197.485" y1="266.065" x2="-196.215" y2="266.065" width="0.1524" layer="94"/>
<wire x1="-146.05" y1="311.15" x2="-146.05" y2="314.96" width="0.3048" layer="94"/>
<wire x1="-146.05" y1="314.96" x2="-144.78" y2="318.77" width="0.3048" layer="94"/>
<wire x1="-144.78" y1="318.77" x2="-143.51" y2="322.58" width="0.3048" layer="94"/>
<wire x1="-146.05" y1="321.945" x2="-146.05" y2="326.39" width="0.3048" layer="94"/>
<wire x1="-152.4" y1="311.15" x2="-152.4" y2="314.96" width="0.3048" layer="94"/>
<wire x1="-152.4" y1="314.96" x2="-154.94" y2="322.58" width="0.3048" layer="94"/>
<wire x1="-152.4" y1="321.945" x2="-152.4" y2="326.39" width="0.3048" layer="94"/>
<text x="-154.305" y="300.355" size="1.4224" layer="95" rot="MR90">AUX_Q1</text>
<text x="-147.955" y="300.355" size="1.4224" layer="95" rot="MR90">AUX_Q2</text>
<text x="-153.035" y="325.12" size="1.4224" layer="95" rot="MR0">63</text>
<text x="-153.035" y="311.15" size="1.4224" layer="95" rot="MR0">64</text>
<text x="-146.685" y="311.15" size="1.4224" layer="95" rot="MR0">84</text>
<text x="-146.685" y="325.12" size="1.4224" layer="95" rot="MR0">83</text>
<wire x1="-162.56" y1="318.77" x2="-144.78" y2="318.77" width="0.3048" layer="94" style="shortdash"/>
<text x="-208.28" y="533.019" size="2.54" layer="95">3RV2031-4EB10</text>
<text x="-267.0175" y="336.55" size="1.778" layer="95">3RT1035-1BB4</text>
<wire x1="-134.3025" y1="521.0175" x2="-131.7625" y2="521.0175" width="0.1524" layer="94"/>
<wire x1="-131.7625" y1="521.0175" x2="-131.7625" y2="518.4775" width="0.1524" layer="94"/>
<wire x1="-136.2075" y1="541.3375" x2="-86.36" y2="541.655" width="0.3048" layer="97" style="longdash"/>
<wire x1="-86.36" y1="541.655" x2="-86.36" y2="498.475" width="0.3048" layer="97" style="longdash"/>
<wire x1="-86.36" y1="498.475" x2="-136.8425" y2="498.1575" width="0.3048" layer="97" style="longdash"/>
<wire x1="-136.8425" y1="498.1575" x2="-136.8425" y2="541.3375" width="0.3048" layer="97" style="longdash"/>
<text x="-122.8725" y="505.7775" size="1.778" layer="95" rot="R180">ON</text>
<text x="-122.8725" y="523.5575" size="1.778" layer="95" rot="R180">OFF</text>
<text x="-113.3475" y="512.1275" size="1.6764" layer="96">PHASE_ORDER_OK</text>
<text x="-124.1425" y="535.94" size="2.54" layer="96">ELECTRO MOTOR</text>
</plain>
<instances>
<instance part="U$2" gate="G$1" x="-116.84" y="150.495"/>
<instance part="U$4" gate="PE" x="-279.4" y="393.7" rot="MR0"/>
<instance part="V5" gate="G$1" x="-304.4825" y="411.7975" rot="MR180"/>
<instance part="V9" gate="G$1" x="-304.4825" y="242.57" rot="MR180"/>
<instance part="V7" gate="G$1" x="-304.4825" y="568.96" smashed="yes" rot="MR180">
<attribute name="VALUE" x="-304.165" y="569.722" size="1.524" layer="96" rot="MR180"/>
</instance>
<instance part="D16" gate="G$1" x="-183.8325" y="467.6775" smashed="yes" rot="R90"/>
<instance part="D20" gate="G$1" x="-177.4825" y="467.6775" smashed="yes" rot="R270"/>
<instance part="S105" gate="G$1" x="-188.9125" y="465.1375" smashed="yes" rot="MR0"/>
<instance part="D23" gate="G$1" x="-160.9725" y="467.6775" smashed="yes" rot="MR90"/>
<instance part="D24" gate="G$1" x="-167.3225" y="467.6775" smashed="yes" rot="MR270"/>
<instance part="S106" gate="G$1" x="-155.8925" y="465.1375" smashed="yes"/>
<instance part="RE4" gate="B" x="-98.7425" y="456.2475" rot="R180"/>
<instance part="S7" gate="G$1" x="-119.0625" y="460.0575" smashed="yes" rot="MR0"/>
<instance part="S8" gate="G$1" x="-111.4425" y="460.0575" smashed="yes" rot="MR0"/>
<instance part="U$47" gate="GND" x="-139.3825" y="428.3075"/>
<instance part="RE1200" gate="B" x="-187.96" y="262.89"/>
<instance part="RE1" gate="B" x="-172.72" y="262.89"/>
<instance part="U$73" gate="G$1" x="-233.68" y="318.77"/>
<instance part="GND2" gate="GND" x="-247.65" y="300.99"/>
<instance part="GND1" gate="GND" x="-146.05" y="288.29"/>
<instance part="U$1" gate="PE" x="-271.78" y="510.54" rot="MR0"/>
<instance part="S9" gate="G$1" x="-131.7625" y="507.0475" smashed="yes" rot="MR180"/>
<instance part="S10" gate="G$1" x="-131.7625" y="524.8275" smashed="yes" rot="R180"/>
<instance part="Z3" gate="G$1" x="-114.935" y="519.43" smashed="yes">
<attribute name="NAME" x="-110.49" y="524.637" size="2.032" layer="95" rot="R180"/>
</instance>
<instance part="U$60" gate="GND" x="-114.935" y="500.6975" rot="MR0"/>
<instance part="U$3" gate="GND" x="-276.86" y="558.8"/>
<instance part="U$5" gate="GND" x="-271.78" y="393.7"/>
<instance part="U$6" gate="PE" x="-276.86" y="228.6" rot="MR0"/>
</instances>
<busses>
<bus name="L1,L2,L3,N,PE">
<segment>
<wire x1="-172.4025" y1="411.7975" x2="-289.2425" y2="411.7975" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="L1,L2,L3">
<segment>
<wire x1="-262.5725" y1="360.68" x2="-262.5725" y2="508.3175" width="0.762" layer="92"/>
<wire x1="-262.5725" y1="508.3175" x2="-258.445" y2="508.3175" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="T1,T2,T3">
<segment>
<wire x1="-172.4025" y1="430.8475" x2="-172.4025" y2="411.7975" width="0.762" layer="92"/>
</segment>
<segment>
<wire x1="-262.5725" y1="360.68" x2="-226.06" y2="360.68" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="HP4-4,IGN,ELMOT,1IN,14/24,PWR_BACKUP,GND">
<segment>
<wire x1="-53.34" y1="249.555" x2="-53.34" y2="568.96" width="0.762" layer="92"/>
<wire x1="-53.34" y1="568.96" x2="-289.2425" y2="568.96" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="L1,L2,L3,PE">
<segment>
<wire x1="-198.12" y1="242.57" x2="-289.2425" y2="242.57" width="0.762" layer="92"/>
</segment>
</bus>
</busses>
<nets>
<net name="PE" class="0">
<segment>
<wire x1="-251.714" y1="518.414" x2="-271.78" y2="518.414" width="0.1524" layer="91"/>
<wire x1="-271.78" y1="518.414" x2="-271.78" y2="515.62" width="0.1524" layer="91"/>
<pinref part="U$1" gate="PE" pin="PE"/>
</segment>
<segment>
<wire x1="-279.4" y1="242.57" x2="-276.86" y2="240.03" width="0.1524" layer="91"/>
<pinref part="U$6" gate="PE" pin="PE"/>
<wire x1="-276.86" y1="240.03" x2="-276.86" y2="233.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<wire x1="-139.3825" y1="435.9275" x2="-139.3825" y2="433.3875" width="0.1524" layer="91"/>
<pinref part="U$47" gate="GND" pin="GND"/>
</segment>
<segment>
<wire x1="-247.65" y1="313.69" x2="-247.65" y2="308.61" width="0.1524" layer="91"/>
<wire x1="-247.65" y1="308.61" x2="-196.85" y2="308.61" width="0.1524" layer="91"/>
<wire x1="-196.85" y1="313.69" x2="-196.85" y2="308.61" width="0.1524" layer="91"/>
<wire x1="-247.65" y1="308.61" x2="-247.65" y2="306.07" width="0.1524" layer="91"/>
<junction x="-247.65" y="308.61"/>
<pinref part="GND2" gate="GND" pin="GND"/>
</segment>
<segment>
<wire x1="-152.4" y1="311.15" x2="-152.4" y2="297.18" width="0.1524" layer="91"/>
<wire x1="-152.4" y1="297.18" x2="-146.05" y2="297.18" width="0.1524" layer="91"/>
<wire x1="-146.05" y1="297.18" x2="-146.05" y2="311.15" width="0.1524" layer="91"/>
<pinref part="GND1" gate="GND" pin="GND"/>
<wire x1="-146.05" y1="297.18" x2="-146.05" y2="293.37" width="0.1524" layer="91"/>
<junction x="-146.05" y="297.18"/>
</segment>
<segment>
<wire x1="-114.935" y1="511.81" x2="-114.935" y2="505.7775" width="0.1524" layer="91"/>
<pinref part="Z3" gate="G$1" pin="1"/>
<pinref part="U$60" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U$3" gate="GND" pin="GND"/>
<wire x1="-279.4" y1="568.96" x2="-276.86" y2="566.42" width="0.1524" layer="91"/>
<wire x1="-276.86" y1="566.42" x2="-276.86" y2="563.88" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="-282.575" y1="411.7975" x2="-279.4" y2="408.94" width="0.1524" layer="91"/>
<wire x1="-279.4" y1="408.94" x2="-279.4" y2="403.86" width="0.1524" layer="91"/>
<pinref part="U$4" gate="PE" pin="PE"/>
<wire x1="-279.4" y1="403.86" x2="-279.4" y2="398.78" width="0.1524" layer="91"/>
<wire x1="-279.4" y1="403.86" x2="-271.78" y2="403.86" width="0.1524" layer="91"/>
<junction x="-279.4" y="403.86"/>
<wire x1="-271.78" y1="403.86" x2="-271.78" y2="398.78" width="0.1524" layer="91"/>
<pinref part="U$5" gate="GND" pin="GND"/>
</segment>
</net>
<net name="ELMOT" class="0">
<segment>
<wire x1="-53.34" y1="341.3125" x2="-56.8325" y2="337.82" width="0.1524" layer="91"/>
<label x="-56.8325" y="340.0425" size="1.778" layer="95" rot="R180"/>
<wire x1="-146.05" y1="326.39" x2="-146.05" y2="337.82" width="0.1524" layer="91"/>
<wire x1="-152.4" y1="337.82" x2="-152.4" y2="326.39" width="0.1524" layer="91"/>
<wire x1="-152.4" y1="337.82" x2="-146.05" y2="337.82" width="0.1524" layer="91"/>
<wire x1="-146.05" y1="337.82" x2="-56.8325" y2="337.82" width="0.1524" layer="91"/>
<junction x="-146.05" y="337.82"/>
<label x="-137.16" y="340.0425" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="HP4-4" class="0">
<segment>
<wire x1="-53.34" y1="260.35" x2="-58.42" y2="255.27" width="0.1524" layer="91"/>
<wire x1="-58.42" y1="255.27" x2="-172.72" y2="255.27" width="0.1524" layer="91"/>
<pinref part="RE1200" gate="B" pin="S"/>
<wire x1="-193.04" y1="267.97" x2="-195.58" y2="267.97" width="0.1524" layer="91"/>
<wire x1="-195.58" y1="267.97" x2="-195.58" y2="255.27" width="0.1524" layer="91"/>
<wire x1="-195.58" y1="255.27" x2="-172.72" y2="255.27" width="0.1524" layer="91"/>
<pinref part="RE1" gate="B" pin="P"/>
<wire x1="-172.72" y1="255.27" x2="-172.72" y2="260.35" width="0.1524" layer="91"/>
<junction x="-172.72" y="255.27"/>
<label x="-59.055" y="257.81" size="1.778" layer="95" rot="R180"/>
<label x="-170.18" y="256.54" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="-55.88" y1="553.72" x2="-131.7625" y2="553.72" width="0.1524" layer="91"/>
<wire x1="-53.34" y1="556.26" x2="-55.88" y2="553.72" width="0.1524" layer="91"/>
<label x="-55.88" y="556.26" size="1.778" layer="95" rot="R180"/>
<label x="-139.7" y="496.57" size="1.778" layer="95" rot="R90"/>
<wire x1="-114.935" y1="532.4475" x2="-114.935" y2="527.05" width="0.1524" layer="91"/>
<wire x1="-131.7625" y1="529.9075" x2="-131.7625" y2="532.4475" width="0.1524" layer="91"/>
<wire x1="-131.7625" y1="532.4475" x2="-114.935" y2="532.4475" width="0.1524" layer="91"/>
<wire x1="-131.7625" y1="553.72" x2="-131.7625" y2="532.4475" width="0.1524" layer="91"/>
<wire x1="-139.3825" y1="532.4475" x2="-131.7625" y2="532.4475" width="0.1524" layer="91"/>
<wire x1="-139.3825" y1="484.1875" x2="-139.3825" y2="532.4475" width="0.1524" layer="91"/>
<junction x="-131.7625" y="532.4475"/>
<pinref part="Z3" gate="G$1" pin="2"/>
<pinref part="S10" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$5" class="0">
<segment>
<wire x1="-188.9125" y1="475.2975" x2="-188.9125" y2="476.5675" width="0.1524" layer="91"/>
<wire x1="-188.9125" y1="476.5675" x2="-183.8325" y2="476.5675" width="0.1524" layer="91"/>
<wire x1="-177.4825" y1="474.0275" x2="-177.4825" y2="472.7575" width="0.1524" layer="91"/>
<wire x1="-183.8325" y1="472.7575" x2="-183.8325" y2="474.0275" width="0.1524" layer="91"/>
<wire x1="-183.8325" y1="474.0275" x2="-183.8325" y2="476.5675" width="0.1524" layer="91"/>
<wire x1="-183.8325" y1="476.5675" x2="-183.8325" y2="481.6475" width="0.1524" layer="91"/>
<wire x1="-183.8325" y1="474.0275" x2="-177.4825" y2="474.0275" width="0.1524" layer="91"/>
<junction x="-183.8325" y="476.5675"/>
<junction x="-183.8325" y="474.0275"/>
<pinref part="S105" gate="G$1" pin="2"/>
<pinref part="D20" gate="G$1" pin="A"/>
<pinref part="D16" gate="G$1" pin="K"/>
</segment>
</net>
<net name="N$77" class="0">
<segment>
<wire x1="-188.9125" y1="460.0575" x2="-188.9125" y2="458.7875" width="0.1524" layer="91"/>
<wire x1="-188.9125" y1="458.7875" x2="-183.8325" y2="458.7875" width="0.1524" layer="91"/>
<wire x1="-177.4825" y1="461.3275" x2="-177.4825" y2="462.5975" width="0.1524" layer="91"/>
<wire x1="-183.8325" y1="462.5975" x2="-183.8325" y2="461.3275" width="0.1524" layer="91"/>
<wire x1="-183.8325" y1="461.3275" x2="-183.8325" y2="458.7875" width="0.1524" layer="91"/>
<wire x1="-183.8325" y1="461.3275" x2="-177.4825" y2="461.3275" width="0.1524" layer="91"/>
<wire x1="-183.8325" y1="458.7875" x2="-183.8325" y2="452.4375" width="0.1524" layer="91"/>
<wire x1="-183.8325" y1="452.4375" x2="-187.6425" y2="452.4375" width="0.1524" layer="91"/>
<wire x1="-187.6425" y1="452.4375" x2="-187.6425" y2="448.6275" width="0.1524" layer="91"/>
<wire x1="-187.6425" y1="448.6275" x2="-183.8325" y2="448.6275" width="0.1524" layer="91"/>
<wire x1="-183.8325" y1="448.6275" x2="-183.8325" y2="438.4675" width="0.1524" layer="91"/>
<junction x="-183.8325" y="461.3275"/>
<junction x="-183.8325" y="458.7875"/>
<pinref part="S105" gate="G$1" pin="1"/>
<pinref part="D20" gate="G$1" pin="K"/>
<pinref part="D16" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$79" class="0">
<segment>
<wire x1="-155.8925" y1="475.2975" x2="-155.8925" y2="476.5675" width="0.1524" layer="91"/>
<wire x1="-155.8925" y1="476.5675" x2="-160.9725" y2="476.5675" width="0.1524" layer="91"/>
<wire x1="-167.3225" y1="474.0275" x2="-167.3225" y2="472.7575" width="0.1524" layer="91"/>
<wire x1="-160.9725" y1="472.7575" x2="-160.9725" y2="474.0275" width="0.1524" layer="91"/>
<wire x1="-160.9725" y1="474.0275" x2="-160.9725" y2="476.5675" width="0.1524" layer="91"/>
<wire x1="-160.9725" y1="476.5675" x2="-160.9725" y2="481.6475" width="0.1524" layer="91"/>
<wire x1="-160.9725" y1="474.0275" x2="-167.3225" y2="474.0275" width="0.1524" layer="91"/>
<junction x="-160.9725" y="476.5675"/>
<junction x="-160.9725" y="474.0275"/>
<pinref part="S106" gate="G$1" pin="2"/>
<pinref part="D24" gate="G$1" pin="A"/>
<pinref part="D23" gate="G$1" pin="K"/>
</segment>
</net>
<net name="N$8" class="0">
<segment>
<wire x1="-155.8925" y1="460.0575" x2="-155.8925" y2="458.7875" width="0.1524" layer="91"/>
<wire x1="-155.8925" y1="458.7875" x2="-160.9725" y2="458.7875" width="0.1524" layer="91"/>
<wire x1="-167.3225" y1="461.3275" x2="-167.3225" y2="462.5975" width="0.1524" layer="91"/>
<wire x1="-160.9725" y1="462.5975" x2="-160.9725" y2="461.3275" width="0.1524" layer="91"/>
<wire x1="-160.9725" y1="461.3275" x2="-160.9725" y2="458.7875" width="0.1524" layer="91"/>
<wire x1="-160.9725" y1="461.3275" x2="-167.3225" y2="461.3275" width="0.1524" layer="91"/>
<wire x1="-160.9725" y1="458.7875" x2="-160.9725" y2="452.4375" width="0.1524" layer="91"/>
<wire x1="-160.9725" y1="452.4375" x2="-164.7825" y2="452.4375" width="0.1524" layer="91"/>
<wire x1="-164.7825" y1="452.4375" x2="-164.7825" y2="448.6275" width="0.1524" layer="91"/>
<wire x1="-164.7825" y1="448.6275" x2="-160.9725" y2="448.6275" width="0.1524" layer="91"/>
<wire x1="-160.9725" y1="448.6275" x2="-160.9725" y2="438.4675" width="0.1524" layer="91"/>
<junction x="-160.9725" y="461.3275"/>
<junction x="-160.9725" y="458.7875"/>
<pinref part="S106" gate="G$1" pin="1"/>
<pinref part="D24" gate="G$1" pin="K"/>
<pinref part="D23" gate="G$1" pin="A"/>
</segment>
</net>
<net name="N$81" class="0">
<segment>
<wire x1="-139.3825" y1="472.7575" x2="-139.3825" y2="481.6475" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$82" class="0">
<segment>
<wire x1="-131.7625" y1="472.7575" x2="-131.7625" y2="481.6475" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$83" class="0">
<segment>
<wire x1="-139.3825" y1="448.6275" x2="-139.3825" y2="438.4675" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$84" class="0">
<segment>
<wire x1="-119.0625" y1="470.2175" x2="-119.0625" y2="472.7575" width="0.1524" layer="91"/>
<wire x1="-119.0625" y1="472.7575" x2="-119.0625" y2="481.6475" width="0.1524" layer="91"/>
<wire x1="-111.4425" y1="470.2175" x2="-111.4425" y2="472.7575" width="0.1524" layer="91"/>
<wire x1="-111.4425" y1="472.7575" x2="-119.0625" y2="472.7575" width="0.1524" layer="91"/>
<junction x="-119.0625" y="472.7575"/>
<pinref part="S7" gate="G$1" pin="2"/>
<pinref part="S8" gate="G$1" pin="2"/>
</segment>
</net>
<net name="N$90" class="0">
<segment>
<wire x1="-111.4425" y1="454.9775" x2="-111.4425" y2="438.4675" width="0.1524" layer="91"/>
<pinref part="S8" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$91" class="0">
<segment>
<wire x1="-119.0625" y1="454.9775" x2="-119.0625" y2="438.4675" width="0.1524" layer="91"/>
<pinref part="S7" gate="G$1" pin="1"/>
</segment>
</net>
<net name="N$92" class="0">
<segment>
<wire x1="-98.7425" y1="458.7875" x2="-98.7425" y2="481.6475" width="0.1524" layer="91"/>
<pinref part="RE4" gate="B" pin="P"/>
</segment>
</net>
<net name="N$93" class="0">
<segment>
<wire x1="-103.8225" y1="451.1675" x2="-105.0925" y2="451.1675" width="0.1524" layer="91"/>
<wire x1="-105.0925" y1="451.1675" x2="-105.0925" y2="438.4675" width="0.1524" layer="91"/>
<pinref part="RE4" gate="B" pin="O"/>
</segment>
</net>
<net name="N$94" class="0">
<segment>
<wire x1="-93.6625" y1="451.1675" x2="-92.3925" y2="451.1675" width="0.1524" layer="91"/>
<wire x1="-92.3925" y1="451.1675" x2="-92.3925" y2="438.4675" width="0.1524" layer="91"/>
<pinref part="RE4" gate="B" pin="S"/>
</segment>
</net>
<net name="N$95" class="0">
<segment>
<wire x1="-172.4025" y1="481.6475" x2="-172.4025" y2="452.4375" width="0.1524" layer="91"/>
<wire x1="-172.4025" y1="452.4375" x2="-176.2125" y2="452.4375" width="0.1524" layer="91"/>
<wire x1="-176.2125" y1="452.4375" x2="-176.2125" y2="448.6275" width="0.1524" layer="91"/>
<wire x1="-176.2125" y1="448.6275" x2="-172.4025" y2="448.6275" width="0.1524" layer="91"/>
<wire x1="-172.4025" y1="448.6275" x2="-172.4025" y2="438.4675" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$96" class="0">
<segment>
<wire x1="-183.8325" y1="484.1875" x2="-183.8325" y2="496.57" width="0.1524" layer="91"/>
</segment>
</net>
<net name="T12" class="0">
<segment>
<wire x1="-172.4025" y1="430.8475" x2="-183.8325" y2="430.8475" width="0.1524" layer="91"/>
<wire x1="-183.8325" y1="430.8475" x2="-183.8325" y2="435.9275" width="0.1524" layer="91"/>
</segment>
</net>
<net name="T13" class="0">
<segment>
<wire x1="-172.4025" y1="430.8475" x2="-172.4025" y2="435.9275" width="0.1524" layer="91"/>
</segment>
</net>
<net name="T14" class="0">
<segment>
<wire x1="-172.4025" y1="430.8475" x2="-160.9725" y2="430.8475" width="0.1524" layer="91"/>
<wire x1="-160.9725" y1="430.8475" x2="-160.9725" y2="435.9275" width="0.1524" layer="91"/>
</segment>
</net>
<net name="L8" class="0">
<segment>
<wire x1="-258.445" y1="508.3175" x2="-258.445" y2="513.3975" width="0.1524" layer="91"/>
<wire x1="-258.445" y1="513.3975" x2="-251.7775" y2="513.3975" width="0.1524" layer="91"/>
</segment>
</net>
<net name="L9" class="0">
<segment>
<wire x1="-258.445" y1="508.3175" x2="-251.7775" y2="508.3175" width="0.1524" layer="91"/>
</segment>
</net>
<net name="L13" class="0">
<segment>
<wire x1="-258.445" y1="508.3175" x2="-258.445" y2="503.2375" width="0.1524" layer="91"/>
<wire x1="-258.445" y1="503.2375" x2="-251.7775" y2="503.2375" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="-187.325" y1="513.3975" x2="-160.9725" y2="513.3975" width="0.1524" layer="91"/>
<wire x1="-160.9725" y1="484.1875" x2="-160.9725" y2="513.3975" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<wire x1="-183.8325" y1="503.2375" x2="-183.8325" y2="496.57" width="0.1524" layer="91"/>
<wire x1="-187.325" y1="503.2375" x2="-183.8325" y2="503.2375" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$46" class="0">
<segment>
<wire x1="-187.325" y1="508.3175" x2="-172.4025" y2="508.3175" width="0.1524" layer="91"/>
<wire x1="-172.4025" y1="484.1875" x2="-172.4025" y2="508.3175" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$49" class="0">
<segment>
<wire x1="-225.1075" y1="503.2375" x2="-217.805" y2="503.2375" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$52" class="0">
<segment>
<wire x1="-225.1075" y1="508.3175" x2="-217.805" y2="508.3175" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$55" class="0">
<segment>
<wire x1="-225.1075" y1="513.3975" x2="-217.805" y2="513.3975" width="0.1524" layer="91"/>
</segment>
</net>
<net name="IGN" class="0">
<segment>
<wire x1="-111.4425" y1="435.9275" x2="-111.4425" y2="426.4025" width="0.1524" layer="91"/>
<wire x1="-53.34" y1="429.5775" x2="-56.515" y2="426.4025" width="0.1524" layer="91"/>
<wire x1="-111.4425" y1="426.4025" x2="-56.515" y2="426.4025" width="0.1524" layer="91"/>
<label x="-108.585" y="426.72" size="1.778" layer="95"/>
<label x="-57.4675" y="428.9425" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="T1" class="0">
<segment>
<wire x1="-238.76" y1="326.39" x2="-238.76" y2="349.25" width="0.1524" layer="91"/>
<wire x1="-238.76" y1="349.25" x2="-238.76" y2="358.14" width="0.1524" layer="91"/>
<wire x1="-187.96" y1="326.39" x2="-187.96" y2="349.25" width="0.1524" layer="91"/>
<wire x1="-187.96" y1="349.25" x2="-238.76" y2="349.25" width="0.1524" layer="91"/>
<junction x="-238.76" y="349.25"/>
<wire x1="-241.3" y1="360.68" x2="-238.76" y2="358.14" width="0.1524" layer="91"/>
<label x="-241.3" y="358.14" size="1.778" layer="95" rot="R270"/>
</segment>
</net>
<net name="T2" class="0">
<segment>
<wire x1="-231.14" y1="326.39" x2="-231.14" y2="344.17" width="0.1524" layer="91"/>
<wire x1="-231.14" y1="344.17" x2="-231.14" y2="358.14" width="0.1524" layer="91"/>
<wire x1="-180.34" y1="326.39" x2="-180.34" y2="344.17" width="0.1524" layer="91"/>
<wire x1="-180.34" y1="344.17" x2="-231.14" y2="344.17" width="0.1524" layer="91"/>
<junction x="-231.14" y="344.17"/>
<wire x1="-233.68" y1="360.68" x2="-231.14" y2="358.14" width="0.1524" layer="91"/>
<label x="-233.68" y="358.14" size="1.778" layer="95" rot="R270"/>
</segment>
</net>
<net name="T3" class="0">
<segment>
<wire x1="-223.52" y1="326.39" x2="-223.52" y2="339.09" width="0.1524" layer="91"/>
<wire x1="-223.52" y1="339.09" x2="-223.52" y2="358.14" width="0.1524" layer="91"/>
<wire x1="-172.72" y1="326.39" x2="-172.72" y2="339.09" width="0.1524" layer="91"/>
<wire x1="-172.72" y1="339.09" x2="-223.52" y2="339.09" width="0.1524" layer="91"/>
<junction x="-223.52" y="339.09"/>
<wire x1="-226.06" y1="360.68" x2="-223.52" y2="358.14" width="0.1524" layer="91"/>
<label x="-226.06" y="358.14" size="1.778" layer="95" rot="R270"/>
</segment>
</net>
<net name="L1" class="0">
<segment>
<wire x1="-172.72" y1="293.37" x2="-172.72" y2="311.15" width="0.1524" layer="91"/>
<wire x1="-238.76" y1="311.15" x2="-238.76" y2="293.37" width="0.1524" layer="91"/>
<wire x1="-172.72" y1="293.37" x2="-238.76" y2="293.37" width="0.1524" layer="91"/>
<junction x="-238.76" y="293.37"/>
<wire x1="-238.76" y1="293.37" x2="-238.76" y2="245.11" width="0.1524" layer="91"/>
<wire x1="-236.22" y1="242.57" x2="-238.76" y2="245.11" width="0.1524" layer="91"/>
<label x="-240.03" y="245.11" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="-198.12" y1="242.57" x2="-200.66" y2="245.11" width="0.1524" layer="91"/>
<wire x1="-200.66" y1="245.11" x2="-200.66" y2="259.08" width="0.1524" layer="91"/>
<label x="-201.93" y="245.11" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="L2" class="0">
<segment>
<wire x1="-180.34" y1="298.45" x2="-180.34" y2="311.15" width="0.1524" layer="91"/>
<wire x1="-231.14" y1="311.15" x2="-231.14" y2="298.45" width="0.1524" layer="91"/>
<wire x1="-180.34" y1="298.45" x2="-231.14" y2="298.45" width="0.1524" layer="91"/>
<junction x="-231.14" y="298.45"/>
<wire x1="-228.6" y1="242.57" x2="-231.14" y2="245.11" width="0.1524" layer="91"/>
<wire x1="-231.14" y1="245.11" x2="-231.14" y2="298.45" width="0.1524" layer="91"/>
<label x="-232.41" y="245.11" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="-203.2" y1="242.57" x2="-205.74" y2="245.11" width="0.1524" layer="91"/>
<wire x1="-205.74" y1="245.11" x2="-205.74" y2="259.08" width="0.1524" layer="91"/>
<label x="-207.01" y="245.11" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="L3" class="0">
<segment>
<wire x1="-187.96" y1="303.53" x2="-187.96" y2="311.15" width="0.1524" layer="91"/>
<wire x1="-223.52" y1="311.15" x2="-223.52" y2="303.53" width="0.1524" layer="91"/>
<wire x1="-187.96" y1="303.53" x2="-223.52" y2="303.53" width="0.1524" layer="91"/>
<junction x="-223.52" y="303.53"/>
<wire x1="-220.98" y1="242.57" x2="-223.52" y2="245.11" width="0.1524" layer="91"/>
<wire x1="-223.52" y1="245.11" x2="-223.52" y2="303.53" width="0.1524" layer="91"/>
<label x="-224.79" y="245.11" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="-213.36" y1="242.57" x2="-215.9" y2="245.11" width="0.1524" layer="91"/>
<wire x1="-215.9" y1="245.11" x2="-215.9" y2="275.59" width="0.1524" layer="91"/>
<wire x1="-215.9" y1="275.59" x2="-205.74" y2="275.59" width="0.1524" layer="91"/>
<wire x1="-205.74" y1="275.59" x2="-205.74" y2="273.05" width="0.1524" layer="91"/>
<label x="-217.17" y="245.11" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<wire x1="-215.9" y1="326.39" x2="-215.9" y2="328.93" width="0.1524" layer="91"/>
<wire x1="-215.9" y1="328.93" x2="-196.85" y2="328.93" width="0.1524" layer="91"/>
<wire x1="-196.85" y1="328.93" x2="-196.85" y2="323.85" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$107" class="0">
<segment>
<wire x1="-215.9" y1="311.15" x2="-215.9" y2="288.29" width="0.1524" layer="91"/>
<wire x1="-215.9" y1="288.29" x2="-180.34" y2="288.29" width="0.1524" layer="91"/>
<wire x1="-180.34" y1="267.97" x2="-180.34" y2="288.29" width="0.1524" layer="91"/>
<pinref part="RE1" gate="B" pin="S"/>
<wire x1="-180.34" y1="267.97" x2="-177.8" y2="267.97" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$109" class="0">
<segment>
<wire x1="-247.65" y1="334.01" x2="-165.1" y2="334.01" width="0.1524" layer="91"/>
<wire x1="-165.1" y1="334.01" x2="-165.1" y2="326.39" width="0.1524" layer="91"/>
<wire x1="-247.65" y1="334.01" x2="-247.65" y2="323.85" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$110" class="0">
<segment>
<wire x1="-165.1" y1="311.15" x2="-165.1" y2="267.97" width="0.1524" layer="91"/>
<pinref part="RE1" gate="B" pin="O"/>
<wire x1="-165.1" y1="267.97" x2="-167.64" y2="267.97" width="0.1524" layer="91"/>
</segment>
</net>
<net name="PWR_BACKUP" class="0">
<segment>
<wire x1="-53.34" y1="249.555" x2="-58.42" y2="244.475" width="0.1524" layer="91"/>
<wire x1="-58.42" y1="244.475" x2="-187.96" y2="244.475" width="0.1524" layer="91"/>
<pinref part="RE1200" gate="B" pin="P"/>
<wire x1="-187.96" y1="260.35" x2="-187.96" y2="244.475" width="0.1524" layer="91"/>
<label x="-186.055" y="245.11" size="1.778" layer="95"/>
<label x="-59.055" y="247.015" size="1.778" layer="95" rot="R180"/>
</segment>
</net>
<net name="N$99" class="0">
<segment>
<wire x1="-131.7625" y1="512.1275" x2="-131.7625" y2="513.3975" width="0.1524" layer="91"/>
<wire x1="-131.7625" y1="513.3975" x2="-131.7625" y2="514.6675" width="0.1524" layer="91"/>
<wire x1="-131.7625" y1="513.3975" x2="-119.0625" y2="513.3975" width="0.1524" layer="91"/>
<wire x1="-119.0625" y1="513.3975" x2="-119.0625" y2="484.1875" width="0.1524" layer="91"/>
<junction x="-131.7625" y="513.3975"/>
<pinref part="S9" gate="G$1" pin="1"/>
<pinref part="S10" gate="G$1" pin="2"/>
</segment>
</net>
<net name="1_IN" class="0">
<segment>
<wire x1="-119.0625" y1="435.9275" x2="-119.0625" y2="432.1175" width="0.1524" layer="91"/>
<wire x1="-119.0625" y1="432.1175" x2="-81.5975" y2="432.1175" width="0.1524" layer="91"/>
<wire x1="-81.5975" y1="432.1175" x2="-81.5975" y2="487.3625" width="0.1524" layer="91"/>
<wire x1="-81.5975" y1="487.3625" x2="-131.7625" y2="487.3625" width="0.1524" layer="91"/>
<wire x1="-131.7625" y1="487.3625" x2="-131.7625" y2="484.1875" width="0.1524" layer="91"/>
<pinref part="S9" gate="G$1" pin="2"/>
<wire x1="-131.7625" y1="487.3625" x2="-131.7625" y2="496.8875" width="0.1524" layer="91"/>
<junction x="-131.7625" y="487.3625"/>
<label x="-129.54" y="487.68" size="1.778" layer="95"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
</eagle>
