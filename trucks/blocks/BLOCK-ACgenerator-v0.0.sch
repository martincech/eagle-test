<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.5.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="113" name="ReferenceLS" color="7" fill="1" visible="no" active="no"/>
<layer number="114" name="FRNTMAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="115" name="FRNTMAAT2" color="7" fill="1" visible="no" active="no"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="no" active="no"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="no" active="no"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="no" active="no"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="no" active="no"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
</layers>
<schematic xreflabel="%F%N/%S" xrefpart="/%S.%C%R">
<libraries>
<library name="VEIT_logotyp">
<packages>
</packages>
<symbols>
<symbol name="DINA3_L">
<wire x1="0" y1="0" x2="388.62" y2="0" width="0.4064" layer="94"/>
<wire x1="0" y1="264.16" x2="0" y2="0" width="0.4064" layer="94"/>
<wire x1="388.62" y1="264.16" x2="0" y2="264.16" width="0.4064" layer="94"/>
<wire x1="388.62" y1="264.16" x2="388.62" y2="0" width="0.4064" layer="94"/>
</symbol>
<symbol name="DOCFIELD-AUTA">
<wire x1="0" y1="0" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="101.6" y1="10.16" x2="87.63" y2="10.16" width="0.1016" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="71.12" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="10.16" width="0.1016" layer="94"/>
<wire x1="101.6" y1="10.16" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="0" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="87.63" y1="10.16" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="87.63" y1="10.16" x2="0" y2="10.16" width="0.1016" layer="94"/>
<wire x1="87.63" y1="5.08" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="101.6" y1="5.08" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="0" y1="10.16" x2="0" y2="22.86" width="0.1016" layer="94"/>
<wire x1="101.6" y1="38.1" x2="64.77" y2="38.1" width="0.1016" layer="94"/>
<wire x1="64.77" y1="38.1" x2="0" y2="38.1" width="0.1016" layer="94"/>
<wire x1="101.6" y1="38.1" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="64.77" y2="22.86" width="0.1016" layer="94"/>
<wire x1="64.77" y1="22.86" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="0" y2="38.1" width="0.1016" layer="94"/>
<wire x1="101.6" y1="22.86" x2="101.6" y2="10.16" width="0.1016" layer="94"/>
<text x="1.27" y="1.27" size="2.54" layer="94">Date:</text>
<text x="15.24" y="1.27" size="2.54" layer="94" font="vector">&gt;LAST_DATE_TIME</text>
<text x="72.39" y="1.27" size="2.54" layer="94">Sheet:</text>
<text x="86.36" y="1.27" size="2.54" layer="94">&gt;SHEET</text>
<text x="88.9" y="6.35" size="2.54" layer="94">REV:</text>
<text x="1.27" y="6.35" size="2.54" layer="94">TITLE:</text>
<text x="1.27" y="30.48" size="1.9304" layer="94" align="top-left">Vehicle Number:</text>
<text x="15.24" y="6.35" size="2.54" layer="94" font="vector">&gt;DRAWING_NAME</text>
<wire x1="64.77" y1="38.1" x2="64.77" y2="22.86" width="0.1016" layer="94"/>
<wire x1="97.487340625" y1="34.81106875" x2="90.66211875" y2="34.81106875" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="34.81106875" x2="90.66211875" y2="34.739" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="34.739" x2="90.66211875" y2="34.514" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="34.514" x2="90.66211875" y2="34.289" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="34.289" x2="90.66211875" y2="34.064" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="34.064" x2="90.66211875" y2="33.839" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="33.839" x2="90.66211875" y2="33.614" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="33.614" x2="90.66211875" y2="33.389" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="33.389" x2="90.66211875" y2="33.23621875" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="33.23621875" x2="93.287040625" y2="33.23621875" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="33.23621875" x2="93.287040625" y2="33.164" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="33.164" x2="93.287040625" y2="32.939" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="32.939" x2="93.287040625" y2="32.714" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="32.714" x2="93.287040625" y2="32.489" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="32.489" x2="93.287040625" y2="32.264" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="32.264" x2="93.287040625" y2="32.039" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="32.039" x2="93.287040625" y2="31.814" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="31.814" x2="93.287040625" y2="31.589" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="31.589" x2="93.287040625" y2="31.364" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="31.364" x2="93.287040625" y2="31.139" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="31.139" x2="93.287040625" y2="30.914" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="30.914" x2="93.287040625" y2="30.689" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="30.689" x2="93.287040625" y2="30.464" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="30.464" x2="93.287040625" y2="30.239" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="30.239" x2="93.287040625" y2="30.014" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="30.014" x2="93.287040625" y2="29.789" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="29.789" x2="93.287040625" y2="29.564" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="29.564" x2="93.287040625" y2="29.45076875" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="29.45076875" x2="94.862340625" y2="29.45076875" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="29.45076875" x2="94.862340625" y2="29.564" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="29.564" x2="94.862340625" y2="29.789" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="29.789" x2="94.862340625" y2="30.014" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="30.014" x2="94.862340625" y2="30.239" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="30.239" x2="94.862340625" y2="30.464" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="30.464" x2="94.862340625" y2="30.689" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="30.689" x2="94.862340625" y2="30.914" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="30.914" x2="94.862340625" y2="31.139" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="31.139" x2="94.862340625" y2="31.364" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="31.364" x2="94.862340625" y2="31.589" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="31.589" x2="94.862340625" y2="31.814" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="31.814" x2="94.862340625" y2="32.039" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="32.039" x2="94.862340625" y2="32.264" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="32.264" x2="94.862340625" y2="32.489" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="32.489" x2="94.862340625" y2="32.714" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="32.714" x2="94.862340625" y2="32.939" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="32.939" x2="94.862340625" y2="33.164" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="33.164" x2="94.862340625" y2="33.23621875" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="33.23621875" x2="97.487340625" y2="33.23621875" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="33.23621875" x2="97.487340625" y2="33.389" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="33.389" x2="97.487340625" y2="33.614" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="33.614" x2="97.487340625" y2="33.839" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="33.839" x2="97.487340625" y2="34.064" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="34.064" x2="97.487340625" y2="34.289" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="34.289" x2="97.487340625" y2="34.514" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="34.514" x2="97.487340625" y2="34.739" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="34.739" x2="97.487340625" y2="34.81106875" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="29.497059375" x2="87.068640625" y2="29.564" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="29.564" x2="87.068640625" y2="29.789" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="29.789" x2="87.068640625" y2="30.014" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="30.014" x2="87.068640625" y2="30.239" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="30.239" x2="87.068640625" y2="30.464" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="30.464" x2="87.068640625" y2="30.689" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="30.689" x2="87.068640625" y2="30.914" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="30.914" x2="87.068640625" y2="31.139" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="31.139" x2="87.068640625" y2="31.364" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="31.364" x2="87.068640625" y2="31.589" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="31.589" x2="87.068640625" y2="31.814" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="31.814" x2="87.068640625" y2="32.039" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="32.039" x2="87.068640625" y2="32.264" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="32.264" x2="87.068640625" y2="32.489" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="32.489" x2="87.068640625" y2="32.714" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="32.714" x2="87.068640625" y2="32.939" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="32.939" x2="87.068640625" y2="33.164" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="33.164" x2="87.068640625" y2="33.389" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="33.389" x2="87.068640625" y2="33.614" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="33.614" x2="87.068640625" y2="33.839" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="33.839" x2="87.068640625" y2="34.064" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="34.064" x2="87.068640625" y2="34.289" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="34.289" x2="87.068640625" y2="34.514" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="34.514" x2="87.068640625" y2="34.739" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="34.739" x2="87.068640625" y2="34.813409375" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="34.813409375" x2="88.64378125" y2="34.813409375" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="34.813409375" x2="88.64378125" y2="34.739" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="34.739" x2="88.64378125" y2="34.514" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="34.514" x2="88.64378125" y2="34.289" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="34.289" x2="88.64378125" y2="34.064" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="34.064" x2="88.64378125" y2="33.839" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="33.839" x2="88.64378125" y2="33.614" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="33.614" x2="88.64378125" y2="33.389" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="33.389" x2="88.64378125" y2="33.164" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="33.164" x2="88.64378125" y2="32.939" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="32.939" x2="88.64378125" y2="32.714" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="32.714" x2="88.64378125" y2="32.489" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="32.489" x2="88.64378125" y2="32.264" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="32.264" x2="88.64378125" y2="32.039" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="32.039" x2="88.64378125" y2="31.814" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="31.814" x2="88.64378125" y2="31.589" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="31.589" x2="88.64378125" y2="31.364" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="31.364" x2="88.64378125" y2="31.139" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="31.139" x2="88.64378125" y2="30.914" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="30.914" x2="88.64378125" y2="30.689" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="30.689" x2="88.64378125" y2="30.464" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="30.464" x2="88.64378125" y2="30.239" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="30.239" x2="88.64378125" y2="30.014" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="30.014" x2="88.64378125" y2="29.789" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="29.789" x2="88.64378125" y2="29.564" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="29.564" x2="88.64378125" y2="29.497059375" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="29.497059375" x2="87.068640625" y2="29.497059375" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="29.49851875" x2="78.286090625" y2="29.564" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="29.564" x2="78.286090625" y2="29.789" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="29.789" x2="78.286090625" y2="30.014" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="30.014" x2="78.286090625" y2="30.239" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="30.239" x2="78.286090625" y2="30.464" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="30.464" x2="78.286090625" y2="30.689" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="30.689" x2="78.286090625" y2="30.914" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="30.914" x2="78.286090625" y2="31.07366875" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="31.07366875" x2="84.81116875" y2="31.07366875" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="31.07366875" x2="84.81116875" y2="30.914" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="30.914" x2="84.81116875" y2="30.689" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="30.689" x2="84.81116875" y2="30.464" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="30.464" x2="84.81116875" y2="30.239" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="30.239" x2="84.81116875" y2="30.014" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="30.014" x2="84.81116875" y2="29.789" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="29.789" x2="84.81116875" y2="29.564" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="29.564" x2="84.81116875" y2="29.49851875" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="29.49851875" x2="78.286090625" y2="29.49851875" width="0.225" layer="94"/>
<wire x1="78.286240625" y1="31.44236875" x2="78.286240625" y2="31.589" width="0.225" layer="94"/>
<wire x1="78.286240625" y1="31.589" x2="78.286240625" y2="31.814" width="0.225" layer="94"/>
<wire x1="78.286240625" y1="31.814" x2="78.286240625" y2="32.039" width="0.225" layer="94"/>
<wire x1="78.286240625" y1="32.039" x2="78.286240625" y2="32.264" width="0.225" layer="94"/>
<wire x1="78.286240625" y1="32.264" x2="78.286240625" y2="32.489" width="0.225" layer="94"/>
<wire x1="78.286240625" y1="32.489" x2="78.286240625" y2="32.714" width="0.225" layer="94"/>
<wire x1="78.286240625" y1="32.714" x2="78.286240625" y2="32.86721875" width="0.225" layer="94"/>
<wire x1="78.286240625" y1="32.86721875" x2="81.96101875" y2="32.86721875" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="32.86721875" x2="81.96101875" y2="32.714" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="32.714" x2="81.96101875" y2="32.489" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="32.489" x2="81.96101875" y2="32.264" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="32.264" x2="81.96101875" y2="32.039" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="32.039" x2="81.96101875" y2="31.814" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="31.814" x2="81.96101875" y2="31.589" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="31.589" x2="81.96101875" y2="31.44236875" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="31.44236875" x2="78.286240625" y2="31.44236875" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="33.23621875" x2="78.286090625" y2="33.389" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="33.389" x2="78.286090625" y2="33.614" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="33.614" x2="78.286090625" y2="33.839" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="33.839" x2="78.286090625" y2="34.064" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="34.064" x2="78.286090625" y2="34.289" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="34.289" x2="78.286090625" y2="34.514" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="34.514" x2="78.286090625" y2="34.739" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="34.739" x2="78.286090625" y2="34.81106875" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="34.81106875" x2="84.81101875" y2="34.81106875" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="34.81106875" x2="84.81101875" y2="34.739" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="34.739" x2="84.81101875" y2="34.514" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="34.514" x2="84.81101875" y2="34.289" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="34.289" x2="84.81101875" y2="34.064" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="34.064" x2="84.81101875" y2="33.839" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="33.839" x2="84.81101875" y2="33.614" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="33.614" x2="84.81101875" y2="33.389" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="33.389" x2="84.81101875" y2="33.23621875" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="33.23621875" x2="78.286090625" y2="33.23621875" width="0.225" layer="94"/>
<wire x1="68.6921" y1="33.944190625" x2="69.073209375" y2="34.289" width="0.225" layer="94"/>
<wire x1="69.073209375" y1="34.289" x2="69.3219" y2="34.514" width="0.225" layer="94"/>
<wire x1="69.3219" y1="34.514" x2="69.570590625" y2="34.739" width="0.225" layer="94"/>
<wire x1="69.570590625" y1="34.739" x2="69.86021875" y2="35.001040625" width="0.225" layer="94"/>
<wire x1="85.115190625" y1="26.34016875" x2="85.115190625" y2="26.189" width="0.225" layer="94"/>
<wire x1="85.115190625" y1="26.189" x2="85.115190625" y2="25.97718125" width="0.225" layer="94"/>
<wire x1="84.32008125" y1="25.980109375" x2="84.341215625" y2="25.70643125" width="0.225" layer="94" curve="8.832242"/>
<wire x1="84.341209375" y1="25.70643125" x2="84.4424375" y2="25.45343125" width="0.225" layer="94" curve="25.94924"/>
<wire x1="84.442440625" y1="25.45343125" x2="84.662590625" y2="25.2962625" width="0.225" layer="94" curve="39.389578"/>
<wire x1="84.662590625" y1="25.296259375" x2="84.93363125" y2="25.258525" width="0.225" layer="94" curve="15.805998"/>
<wire x1="85.52626875" y1="25.706240625" x2="85.547246875" y2="25.980109375" width="0.225" layer="94" curve="8.760059"/>
<wire x1="85.42531875" y1="25.45295" x2="85.46248125" y2="25.514" width="0.225" layer="94" curve="6.74589"/>
<wire x1="85.46248125" y1="25.514" x2="85.526271875" y2="25.706240625" width="0.225" layer="94" curve="19.196299"/>
<wire x1="85.204909375" y1="25.295909375" x2="85.425315625" y2="25.452953125" width="0.225" layer="94" curve="39.656265"/>
<wire x1="84.93363125" y1="25.25853125" x2="85.204909375" y2="25.295909375" width="0.225" layer="94" curve="15.593829"/>
<wire x1="84.32008125" y1="25.980109375" x2="84.32008125" y2="26.189" width="0.225" layer="94"/>
<wire x1="84.32008125" y1="26.189" x2="84.32008125" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="84.341340625" y1="26.61343125" x2="84.32161875" y2="26.414" width="0.225" layer="94" curve="6.503067"/>
<wire x1="84.32161875" y1="26.414" x2="84.320078125" y2="26.34016875" width="0.225" layer="94" curve="2.395218"/>
<wire x1="84.44311875" y1="26.86573125" x2="84.341340625" y2="26.61343125" width="0.225" layer="94" curve="26.142719"/>
<wire x1="84.663740625" y1="27.02155" x2="84.443121875" y2="26.865728125" width="0.225" layer="94" curve="39.450584"/>
<wire x1="84.93363125" y1="27.05881875" x2="84.663740625" y2="27.02155" width="0.225" layer="94" curve="15.292594"/>
<wire x1="85.54725" y1="25.980109375" x2="85.54725" y2="26.189" width="0.225" layer="94"/>
<wire x1="85.54725" y1="26.189" x2="85.54725" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="85.54725" y1="26.34016875" x2="85.52646875" y2="26.61348125" width="0.225" layer="94" curve="8.69613"/>
<wire x1="85.52646875" y1="26.61348125" x2="85.4257" y2="26.8662" width="0.225" layer="94" curve="26.08534"/>
<wire x1="85.4257" y1="26.8662" x2="85.2053" y2="27.02220625" width="0.225" layer="94" curve="39.852867"/>
<wire x1="85.2053" y1="27.022209375" x2="84.93363125" y2="27.058825" width="0.225" layer="94" curve="15.379893"/>
<wire x1="84.752059375" y1="26.34016875" x2="84.752059375" y2="26.189" width="0.225" layer="94"/>
<wire x1="84.752059375" y1="26.189" x2="84.752059375" y2="25.97718125" width="0.225" layer="94"/>
<wire x1="85.115190625" y1="25.97718125" x2="85.11286875" y2="25.87078125" width="0.225" layer="94"/>
<wire x1="85.11286875" y1="25.87078125" x2="85.10523125" y2="25.791359375" width="0.225" layer="94"/>
<wire x1="85.10523125" y1="25.791359375" x2="85.100290625" y2="25.765209375" width="0.225" layer="94"/>
<wire x1="85.100290625" y1="25.765209375" x2="85.09306875" y2="25.73961875" width="0.225" layer="94"/>
<wire x1="85.09306875" y1="25.73961875" x2="85.081940625" y2="25.715509375" width="0.225" layer="94"/>
<wire x1="85.081940625" y1="25.715509375" x2="85.06321875" y2="25.696990625" width="0.225" layer="94"/>
<wire x1="85.06321875" y1="25.696990625" x2="85.0387" y2="25.68683125" width="0.225" layer="94"/>
<wire x1="85.0387" y1="25.68683125" x2="85.012740625" y2="25.68106875" width="0.225" layer="94"/>
<wire x1="85.012740625" y1="25.68106875" x2="84.986340625" y2="25.6778" width="0.225" layer="94"/>
<wire x1="84.986340625" y1="25.6778" x2="84.95978125" y2="25.67618125" width="0.225" layer="94"/>
<wire x1="84.752059375" y1="26.34016875" x2="84.75605" y2="26.47246875" width="0.225" layer="94"/>
<wire x1="84.75605" y1="26.47246875" x2="84.76195" y2="26.52508125" width="0.225" layer="94"/>
<wire x1="84.76195" y1="26.52508125" x2="84.77411875" y2="26.57655" width="0.225" layer="94"/>
<wire x1="84.77411875" y1="26.57655" x2="84.78536875" y2="26.600440625" width="0.225" layer="94"/>
<wire x1="84.78536875" y1="26.600440625" x2="84.804459375" y2="26.618359375" width="0.225" layer="94"/>
<wire x1="84.804459375" y1="26.618359375" x2="84.82903125" y2="26.62806875" width="0.225" layer="94"/>
<wire x1="84.82903125" y1="26.62806875" x2="84.854909375" y2="26.63358125" width="0.225" layer="94"/>
<wire x1="84.854909375" y1="26.63358125" x2="84.8812" y2="26.6367" width="0.225" layer="94"/>
<wire x1="84.8812" y1="26.6367" x2="84.90763125" y2="26.63826875" width="0.225" layer="94"/>
<wire x1="84.90763125" y1="26.63826875" x2="84.96056875" y2="26.63825" width="0.225" layer="94"/>
<wire x1="84.96056875" y1="26.63825" x2="84.987" y2="26.63666875" width="0.225" layer="94"/>
<wire x1="84.987" y1="26.63666875" x2="85.013290625" y2="26.63358125" width="0.225" layer="94"/>
<wire x1="85.013290625" y1="26.63358125" x2="85.039190625" y2="26.628140625" width="0.225" layer="94"/>
<wire x1="85.039190625" y1="26.628140625" x2="85.063809375" y2="26.618559375" width="0.225" layer="94"/>
<wire x1="85.063809375" y1="26.618559375" x2="85.0829" y2="26.60066875" width="0.225" layer="94"/>
<wire x1="85.0829" y1="26.60066875" x2="85.093940625" y2="26.57666875" width="0.225" layer="94"/>
<wire x1="85.093940625" y1="26.57666875" x2="85.105759375" y2="26.52511875" width="0.225" layer="94"/>
<wire x1="85.105759375" y1="26.52511875" x2="85.111390625" y2="26.47248125" width="0.225" layer="94"/>
<wire x1="85.111390625" y1="26.47248125" x2="85.11306875" y2="26.414" width="0.225" layer="94"/>
<wire x1="85.11306875" y1="26.414" x2="85.115190625" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="84.752059375" y1="25.97718125" x2="84.754290625" y2="25.87078125" width="0.225" layer="94"/>
<wire x1="84.754290625" y1="25.87078125" x2="84.76156875" y2="25.79131875" width="0.225" layer="94"/>
<wire x1="84.76156875" y1="25.79131875" x2="84.7664" y2="25.76515" width="0.225" layer="94"/>
<wire x1="84.7664" y1="25.76515" x2="84.773409375" y2="25.7395" width="0.225" layer="94"/>
<wire x1="84.773409375" y1="25.7395" x2="84.784340625" y2="25.7153" width="0.225" layer="94"/>
<wire x1="84.784340625" y1="25.7153" x2="84.803059375" y2="25.6968" width="0.225" layer="94"/>
<wire x1="84.803059375" y1="25.6968" x2="84.82763125" y2="25.686759375" width="0.225" layer="94"/>
<wire x1="84.82763125" y1="25.686759375" x2="84.853609375" y2="25.681059375" width="0.225" layer="94"/>
<wire x1="84.853609375" y1="25.681059375" x2="84.90656875" y2="25.6762" width="0.225" layer="94"/>
<wire x1="84.90656875" y1="25.6762" x2="84.95978125" y2="25.6762" width="0.225" layer="94"/>
<wire x1="88.41505" y1="27.03186875" x2="88.835090625" y2="27.03186875" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="27.03186875" x2="88.835090625" y2="26.864" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="26.864" x2="88.835090625" y2="26.639" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="26.639" x2="88.835090625" y2="26.414" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="26.414" x2="88.835090625" y2="26.189" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="26.189" x2="88.835090625" y2="25.964" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="25.964" x2="88.835090625" y2="25.739" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="25.739" x2="88.835090625" y2="25.514" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="25.514" x2="88.835090625" y2="25.289" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="25.289" x2="88.835090625" y2="25.28548125" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="25.28548125" x2="88.41505" y2="25.28548125" width="0.225" layer="94"/>
<wire x1="88.41505" y1="25.28548125" x2="88.41505" y2="25.289" width="0.225" layer="94"/>
<wire x1="88.41505" y1="25.289" x2="88.41505" y2="25.514" width="0.225" layer="94"/>
<wire x1="88.41505" y1="25.514" x2="88.41505" y2="25.739" width="0.225" layer="94"/>
<wire x1="88.41505" y1="25.739" x2="88.41505" y2="25.964" width="0.225" layer="94"/>
<wire x1="88.41505" y1="25.964" x2="88.41505" y2="26.189" width="0.225" layer="94"/>
<wire x1="88.41505" y1="26.189" x2="88.41505" y2="26.414" width="0.225" layer="94"/>
<wire x1="88.41505" y1="26.414" x2="88.41505" y2="26.639" width="0.225" layer="94"/>
<wire x1="88.41505" y1="26.639" x2="88.41505" y2="26.864" width="0.225" layer="94"/>
<wire x1="88.41505" y1="26.864" x2="88.41505" y2="27.03186875" width="0.225" layer="94"/>
<wire x1="88.41505" y1="27.205890625" x2="88.835090625" y2="27.205890625" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="27.205890625" x2="88.835090625" y2="27.314" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="27.314" x2="88.835090625" y2="27.539" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="27.539" x2="88.835090625" y2="27.68606875" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="27.68606875" x2="88.41505" y2="27.68606875" width="0.225" layer="94"/>
<wire x1="88.41505" y1="27.68606875" x2="88.41505" y2="27.539" width="0.225" layer="94"/>
<wire x1="88.41505" y1="27.539" x2="88.41505" y2="27.314" width="0.225" layer="94"/>
<wire x1="88.41505" y1="27.314" x2="88.41505" y2="27.205890625" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="27.725040625" x2="75.911209375" y2="27.725040625" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="27.725040625" x2="75.911209375" y2="27.539" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="27.539" x2="75.911209375" y2="27.314" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="27.314" x2="75.911209375" y2="27.089" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="27.089" x2="75.911209375" y2="26.864" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="26.864" x2="75.911209375" y2="26.639" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="26.639" x2="75.911209375" y2="26.414" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="26.414" x2="75.911209375" y2="26.189" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="26.189" x2="75.911209375" y2="25.964" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="25.964" x2="75.911209375" y2="25.739" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="25.739" x2="75.911209375" y2="25.514" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="25.514" x2="75.911209375" y2="25.289" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="25.289" x2="75.911209375" y2="25.28548125" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="25.28548125" x2="75.49116875" y2="25.28548125" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="25.28548125" x2="75.49116875" y2="25.289" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="25.289" x2="75.49116875" y2="25.514" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="25.514" x2="75.49116875" y2="25.739" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="25.739" x2="75.49116875" y2="25.964" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="25.964" x2="75.49116875" y2="26.189" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="26.189" x2="75.49116875" y2="26.414" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="26.414" x2="75.49116875" y2="26.639" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="26.639" x2="75.49116875" y2="26.864" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="26.864" x2="75.49116875" y2="27.089" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="27.089" x2="75.49116875" y2="27.314" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="27.314" x2="75.49116875" y2="27.539" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="27.539" x2="75.49116875" y2="27.725040625" width="0.225" layer="94"/>
<wire x1="90.092" y1="25.97718125" x2="90.092" y2="26.189" width="0.225" layer="94"/>
<wire x1="90.092" y1="26.189" x2="90.092" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="90.41016875" y1="26.44828125" x2="90.41016875" y2="26.414" width="0.225" layer="94"/>
<wire x1="90.41016875" y1="26.414" x2="90.41016875" y2="26.30881875" width="0.225" layer="94"/>
<wire x1="90.41016875" y1="26.30881875" x2="90.830209375" y2="26.30881875" width="0.225" layer="94"/>
<wire x1="90.43711875" y1="26.071809375" x2="90.43711875" y2="25.964" width="0.225" layer="94"/>
<wire x1="90.43711875" y1="25.964" x2="90.43711875" y2="25.89925" width="0.225" layer="94"/>
<wire x1="90.43711875" y1="26.071809375" x2="90.86316875" y2="26.071809375" width="0.225" layer="94"/>
<wire x1="90.092" y1="26.34016875" x2="90.09408125" y2="26.45038125" width="0.225" layer="94"/>
<wire x1="90.09408125" y1="26.45038125" x2="90.09926875" y2="26.5163" width="0.225" layer="94"/>
<wire x1="90.09926875" y1="26.5163" x2="90.106540625" y2="26.55976875" width="0.225" layer="94"/>
<wire x1="90.106540625" y1="26.55976875" x2="90.112390625" y2="26.58101875" width="0.225" layer="94"/>
<wire x1="90.112390625" y1="26.58101875" x2="90.12096875" y2="26.6013" width="0.225" layer="94"/>
<wire x1="90.12096875" y1="26.6013" x2="90.135190625" y2="26.617890625" width="0.225" layer="94"/>
<wire x1="90.135190625" y1="26.617890625" x2="90.1551" y2="26.6272" width="0.225" layer="94"/>
<wire x1="90.1551" y1="26.6272" x2="90.17645" y2="26.63263125" width="0.225" layer="94"/>
<wire x1="90.17645" y1="26.63263125" x2="90.19825" y2="26.6359" width="0.225" layer="94"/>
<wire x1="90.19825" y1="26.6359" x2="90.220209375" y2="26.637740625" width="0.225" layer="94"/>
<wire x1="90.220209375" y1="26.637740625" x2="90.242240625" y2="26.63858125" width="0.225" layer="94"/>
<wire x1="90.242240625" y1="26.63858125" x2="90.264290625" y2="26.638640625" width="0.225" layer="94"/>
<wire x1="90.264290625" y1="26.638640625" x2="90.28631875" y2="26.6378" width="0.225" layer="94"/>
<wire x1="90.28631875" y1="26.6378" x2="90.30826875" y2="26.63575" width="0.225" layer="94"/>
<wire x1="90.30826875" y1="26.63575" x2="90.33" y2="26.63208125" width="0.225" layer="94"/>
<wire x1="90.33" y1="26.63208125" x2="90.351140625" y2="26.625909375" width="0.225" layer="94"/>
<wire x1="90.351140625" y1="26.625909375" x2="90.3705" y2="26.615509375" width="0.225" layer="94"/>
<wire x1="90.3705" y1="26.615509375" x2="90.38486875" y2="26.598990625" width="0.225" layer="94"/>
<wire x1="90.38486875" y1="26.598990625" x2="90.40023125" y2="26.557840625" width="0.225" layer="94"/>
<wire x1="90.40023125" y1="26.557840625" x2="90.407140625" y2="26.51431875" width="0.225" layer="94"/>
<wire x1="90.407140625" y1="26.51431875" x2="90.41016875" y2="26.44828125" width="0.225" layer="94"/>
<wire x1="90.092" y1="25.97718125" x2="90.09478125" y2="25.85786875" width="0.225" layer="94"/>
<wire x1="90.09478125" y1="25.85786875" x2="90.09865" y2="25.810290625" width="0.225" layer="94"/>
<wire x1="90.09865" y1="25.810290625" x2="90.11223125" y2="25.74008125" width="0.225" layer="94"/>
<wire x1="90.11223125" y1="25.74008125" x2="90.121240625" y2="25.718009375" width="0.225" layer="94"/>
<wire x1="90.121240625" y1="25.718009375" x2="90.136040625" y2="25.6995" width="0.225" layer="94"/>
<wire x1="90.136040625" y1="25.6995" x2="90.15721875" y2="25.688690625" width="0.225" layer="94"/>
<wire x1="90.15721875" y1="25.688690625" x2="90.18025" y2="25.68248125" width="0.225" layer="94"/>
<wire x1="90.18025" y1="25.68248125" x2="90.20383125" y2="25.678809375" width="0.225" layer="94"/>
<wire x1="90.20383125" y1="25.678809375" x2="90.251459375" y2="25.675840625" width="0.225" layer="94"/>
<wire x1="90.251459375" y1="25.675840625" x2="90.299190625" y2="25.67656875" width="0.225" layer="94"/>
<wire x1="90.299190625" y1="25.67656875" x2="90.34663125" y2="25.681690625" width="0.225" layer="94"/>
<wire x1="90.34663125" y1="25.681690625" x2="90.391890625" y2="25.696190625" width="0.225" layer="94"/>
<wire x1="90.391890625" y1="25.696190625" x2="90.409190625" y2="25.71226875" width="0.225" layer="94"/>
<wire x1="90.409190625" y1="25.71226875" x2="90.419459375" y2="25.733759375" width="0.225" layer="94"/>
<wire x1="90.419459375" y1="25.733759375" x2="90.425940625" y2="25.75671875" width="0.225" layer="94"/>
<wire x1="90.425940625" y1="25.75671875" x2="90.4332" y2="25.80388125" width="0.225" layer="94"/>
<wire x1="90.4332" y1="25.80388125" x2="90.436309375" y2="25.85151875" width="0.225" layer="94"/>
<wire x1="90.436309375" y1="25.85151875" x2="90.43711875" y2="25.89925" width="0.225" layer="94"/>
<wire x1="89.66001875" y1="25.980109375" x2="89.67878125" y2="25.718659375" width="0.225" layer="94" curve="8.208716"/>
<wire x1="89.67878125" y1="25.718659375" x2="89.74463125" y2="25.514" width="0.225" layer="94" curve="19.254744"/>
<wire x1="89.74463125" y1="25.514" x2="89.76749375" y2="25.47364375" width="0.225" layer="94" curve="4.135389"/>
<wire x1="89.767490625" y1="25.473640625" x2="89.966896875" y2="25.309234375" width="0.225" layer="94" curve="37.791863"/>
<wire x1="89.9669" y1="25.309240625" x2="90.02831875" y2="25.289" width="0.225" layer="94" curve="4.744864"/>
<wire x1="90.02831875" y1="25.289" x2="90.22315" y2="25.259475" width="0.225" layer="94" curve="14.492988"/>
<wire x1="90.86316875" y1="26.071809375" x2="90.86316875" y2="25.964" width="0.225" layer="94"/>
<wire x1="90.86316875" y1="25.964" x2="90.86316875" y2="25.89603125" width="0.225" layer="94"/>
<wire x1="90.83383125" y1="25.63598125" x2="90.852559375" y2="25.739" width="0.225" layer="94" curve="5.141671"/>
<wire x1="90.852559375" y1="25.739" x2="90.86316875" y2="25.89603125" width="0.225" layer="94" curve="7.731903"/>
<wire x1="90.710259375" y1="25.40781875" x2="90.83383125" y2="25.63598125" width="0.225" layer="94" curve="31.131749"/>
<wire x1="90.483690625" y1="25.282109375" x2="90.710259375" y2="25.40781875" width="0.225" layer="94" curve="33.941784"/>
<wire x1="90.22315" y1="25.25946875" x2="90.483690625" y2="25.282103125" width="0.225" layer="94" curve="14.175253"/>
<wire x1="89.66001875" y1="25.980109375" x2="89.66001875" y2="26.189" width="0.225" layer="94"/>
<wire x1="89.66001875" y1="26.189" x2="89.66001875" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="89.6773" y1="26.590590625" x2="89.66001875" y2="26.34016875" width="0.225" layer="94" curve="7.895583"/>
<wire x1="89.75726875" y1="26.82721875" x2="89.685840625" y2="26.639" width="0.225" layer="94" curve="17.336884"/>
<wire x1="89.685840625" y1="26.639" x2="89.6773" y2="26.590590625" width="0.225" layer="94" curve="4.218105"/>
<wire x1="89.93936875" y1="26.995190625" x2="89.75726875" y2="26.82721875" width="0.225" layer="94" curve="35.721204"/>
<wire x1="90.18176875" y1="27.05536875" x2="89.93936875" y2="26.995190625" width="0.225" layer="94" curve="21.771833"/>
<wire x1="90.830209375" y1="26.30881875" x2="90.830209375" y2="26.414" width="0.225" layer="94"/>
<wire x1="90.830209375" y1="26.414" x2="90.830209375" y2="26.49631875" width="0.225" layer="94"/>
<wire x1="90.830209375" y1="26.49631875" x2="90.7978375" y2="26.744540625" width="0.225" layer="94" curve="14.861165"/>
<wire x1="90.797840625" y1="26.744540625" x2="90.6616625" y2="26.951284375" width="0.225" layer="94" curve="37.022842"/>
<wire x1="90.661659375" y1="26.95128125" x2="90.432340625" y2="27.047065625" width="0.225" layer="94" curve="30.892228"/>
<wire x1="90.432340625" y1="27.04706875" x2="90.18176875" y2="27.055365625" width="0.225" layer="94" curve="10.656803"/>
<wire x1="68.6921" y1="33.944190625" x2="68.78723125" y2="33.839" width="0.225" layer="94"/>
<wire x1="68.78723125" y1="33.839" x2="69.804659375" y2="32.714" width="0.225" layer="94"/>
<wire x1="69.804659375" y1="32.714" x2="70.21163125" y2="32.264" width="0.225" layer="94"/>
<wire x1="70.21163125" y1="32.264" x2="71.229059375" y2="31.139" width="0.225" layer="94"/>
<wire x1="71.229059375" y1="31.139" x2="71.63603125" y2="30.689" width="0.225" layer="94"/>
<wire x1="71.63603125" y1="30.689" x2="72.653459375" y2="29.564" width="0.225" layer="94"/>
<wire x1="72.653459375" y1="29.564" x2="72.94835" y2="29.23793125" width="0.225" layer="94"/>
<wire x1="72.94835" y1="29.23793125" x2="73.44671875" y2="29.789" width="0.225" layer="94"/>
<wire x1="73.44671875" y1="29.789" x2="73.6502" y2="30.014" width="0.225" layer="94"/>
<wire x1="73.6502" y1="30.014" x2="74.26065" y2="30.689" width="0.225" layer="94"/>
<wire x1="74.26065" y1="30.689" x2="74.46413125" y2="30.914" width="0.225" layer="94"/>
<wire x1="74.46413125" y1="30.914" x2="75.278059375" y2="31.814" width="0.225" layer="94"/>
<wire x1="75.278059375" y1="31.814" x2="75.481540625" y2="32.039" width="0.225" layer="94"/>
<wire x1="75.481540625" y1="32.039" x2="76.29546875" y2="32.939" width="0.225" layer="94"/>
<wire x1="76.29546875" y1="32.939" x2="76.49895" y2="33.164" width="0.225" layer="94"/>
<wire x1="76.49895" y1="33.164" x2="76.70243125" y2="33.389" width="0.225" layer="94"/>
<wire x1="76.70243125" y1="33.389" x2="77.204709375" y2="33.944390625" width="0.225" layer="94"/>
<wire x1="77.204709375" y1="33.944390625" x2="76.036390625" y2="35.00103125" width="0.225" layer="94"/>
<wire x1="76.036390625" y1="35.00103125" x2="74.57861875" y2="33.389" width="0.225" layer="94"/>
<wire x1="74.57861875" y1="33.389" x2="74.37515" y2="33.164" width="0.225" layer="94"/>
<wire x1="74.37515" y1="33.164" x2="74.17168125" y2="32.939" width="0.225" layer="94"/>
<wire x1="74.17168125" y1="32.939" x2="73.764740625" y2="32.489" width="0.225" layer="94"/>
<wire x1="73.764740625" y1="32.489" x2="72.94835" y2="31.58621875" width="0.225" layer="94"/>
<wire x1="72.94835" y1="31.58621875" x2="72.742359375" y2="31.814" width="0.225" layer="94"/>
<wire x1="72.742359375" y1="31.814" x2="72.131940625" y2="32.489" width="0.225" layer="94"/>
<wire x1="72.131940625" y1="32.489" x2="71.724990625" y2="32.939" width="0.225" layer="94"/>
<wire x1="71.724990625" y1="32.939" x2="71.318040625" y2="33.389" width="0.225" layer="94"/>
<wire x1="71.318040625" y1="33.389" x2="70.911090625" y2="33.839" width="0.225" layer="94"/>
<wire x1="70.911090625" y1="33.839" x2="70.504140625" y2="34.289" width="0.225" layer="94"/>
<wire x1="70.504140625" y1="34.289" x2="70.097190625" y2="34.739" width="0.225" layer="94"/>
<wire x1="70.097190625" y1="34.739" x2="69.86021875" y2="35.001040625" width="0.225" layer="94"/>
<wire x1="81.341240625" y1="26.03255" x2="81.74326875" y2="26.03255" width="0.225" layer="94"/>
<wire x1="81.74326875" y1="26.03255" x2="81.74326875" y2="25.964" width="0.225" layer="94"/>
<wire x1="81.74326875" y1="25.964" x2="81.74326875" y2="25.812240625" width="0.225" layer="94"/>
<wire x1="81.723590625" y1="25.602390625" x2="81.743275" y2="25.812240625" width="0.225" layer="94" curve="10.717258"/>
<wire x1="81.637890625" y1="25.41138125" x2="81.7235875" y2="25.602390625" width="0.225" layer="94" curve="26.892331"/>
<wire x1="81.467640625" y1="25.291140625" x2="81.6378875" y2="25.41138125" width="0.225" layer="94" curve="34.316783"/>
<wire x1="81.260009375" y1="25.258559375" x2="81.467640625" y2="25.291140625" width="0.225" layer="94" curve="18.312496"/>
<wire x1="81.05158125" y1="25.28623125" x2="81.260009375" y2="25.258559375" width="0.225" layer="94" curve="14.64866"/>
<wire x1="80.880909375" y1="25.405009375" x2="81.05158125" y2="25.286234375" width="0.225" layer="94" curve="39.895242"/>
<wire x1="80.80178125" y1="25.5987" x2="80.880903125" y2="25.40500625" width="0.225" layer="94" curve="25.996239"/>
<wire x1="80.786140625" y1="25.80901875" x2="80.786140625" y2="25.964" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="25.964" x2="80.786140625" y2="26.189" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="26.189" x2="80.786140625" y2="26.414" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="26.414" x2="80.786140625" y2="26.626990625" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="25.80901875" x2="80.78728125" y2="25.739" width="0.225" layer="94" curve="3.295417"/>
<wire x1="80.78728125" y1="25.739" x2="80.801784375" y2="25.5987" width="0.225" layer="94" curve="6.640297"/>
<wire x1="81.206190625" y1="25.86908125" x2="81.206190625" y2="25.964" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="25.964" x2="81.206190625" y2="26.189" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="26.189" x2="81.206190625" y2="26.414" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="26.414" x2="81.206190625" y2="26.626990625" width="0.225" layer="94"/>
<wire x1="81.341240625" y1="26.03255" x2="81.341240625" y2="25.964" width="0.225" layer="94"/>
<wire x1="81.341240625" y1="25.964" x2="81.341240625" y2="25.779140625" width="0.225" layer="94"/>
<wire x1="81.317509375" y1="25.6654" x2="81.327084375" y2="25.6739125" width="0.225" layer="94" curve="49.818215"/>
<wire x1="81.30451875" y1="25.66235" x2="81.3175125" y2="25.66539375" width="0.225" layer="94" curve="7.103613"/>
<wire x1="81.291240625" y1="25.66093125" x2="81.30451875" y2="25.66235625" width="0.225" layer="94" curve="7.003084"/>
<wire x1="81.277890625" y1="25.660490625" x2="81.291240625" y2="25.660934375" width="0.225" layer="94" curve="1.449937"/>
<wire x1="81.26453125" y1="25.66073125" x2="81.277890625" y2="25.6604875" width="0.225" layer="94" curve="4.441603"/>
<wire x1="81.33501875" y1="25.69935" x2="81.33183125" y2="25.68638125" width="0.225" layer="94"/>
<wire x1="81.33183125" y1="25.68638125" x2="81.327090625" y2="25.673909375" width="0.225" layer="94"/>
<wire x1="81.33501875" y1="25.69935" x2="81.3373" y2="25.712509375" width="0.225" layer="94"/>
<wire x1="81.3373" y1="25.712509375" x2="81.340009375" y2="25.739" width="0.225" layer="94"/>
<wire x1="81.340009375" y1="25.739" x2="81.34001875" y2="25.739090625" width="0.225" layer="94"/>
<wire x1="81.34001875" y1="25.739090625" x2="81.341240625" y2="25.779140625" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="25.86908125" x2="81.20651875" y2="25.818159375" width="0.225" layer="94"/>
<wire x1="81.20651875" y1="25.818159375" x2="81.208009375" y2="25.76726875" width="0.225" layer="94"/>
<wire x1="81.208009375" y1="25.76726875" x2="81.209609375" y2="25.741859375" width="0.225" layer="94"/>
<wire x1="81.209609375" y1="25.741859375" x2="81.21228125" y2="25.716540625" width="0.225" layer="94"/>
<wire x1="81.21228125" y1="25.716540625" x2="81.21671875" y2="25.69148125" width="0.225" layer="94"/>
<wire x1="81.21671875" y1="25.69148125" x2="81.225321875" y2="25.66765" width="0.225" layer="94" curve="19.627877"/>
<wire x1="81.26453125" y1="25.66073125" x2="81.251209375" y2="25.66166875" width="0.225" layer="94"/>
<wire x1="81.238" y1="25.663609375" x2="81.251209375" y2="25.661671875" width="0.225" layer="94" curve="8.648156"/>
<wire x1="81.22531875" y1="25.66765" x2="81.238" y2="25.66360625" width="0.225" layer="94" curve="10.044676"/>
<wire x1="81.7013" y1="26.626990625" x2="81.206190625" y2="26.626990625" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="27.03186875" x2="81.7013" y2="27.03186875" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="27.401009375" x2="81.206190625" y2="27.314" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="27.314" x2="81.206190625" y2="27.089" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="27.089" x2="81.206190625" y2="27.03186875" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="27.03186875" x2="80.786140625" y2="27.089" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="27.089" x2="80.786140625" y2="27.314" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="27.314" x2="80.786140625" y2="27.401009375" width="0.225" layer="94"/>
<wire x1="80.627059375" y1="27.03186875" x2="80.786140625" y2="27.03186875" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="26.626990625" x2="80.627059375" y2="26.626990625" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="27.401009375" x2="81.206190625" y2="27.401009375" width="0.225" layer="94"/>
<wire x1="81.7013" y1="27.03186875" x2="81.7013" y2="26.864" width="0.225" layer="94"/>
<wire x1="81.7013" y1="26.864" x2="81.7013" y2="26.639" width="0.225" layer="94"/>
<wire x1="81.7013" y1="26.639" x2="81.7013" y2="26.626990625" width="0.225" layer="94"/>
<wire x1="80.627059375" y1="27.03186875" x2="80.627059375" y2="26.864" width="0.225" layer="94"/>
<wire x1="80.627059375" y1="26.864" x2="80.627059375" y2="26.639" width="0.225" layer="94"/>
<wire x1="80.627059375" y1="26.639" x2="80.627059375" y2="26.626990625" width="0.225" layer="94"/>
<wire x1="92.6429" y1="26.95253125" x2="92.469709375" y2="27.040159375" width="0.225" layer="94" curve="32.338936"/>
<wire x1="79.95228125" y1="26.071809375" x2="79.52623125" y2="26.071809375" width="0.225" layer="94"/>
<wire x1="79.49928125" y1="26.30881875" x2="79.91931875" y2="26.30881875" width="0.225" layer="94"/>
<wire x1="79.91931875" y1="26.30881875" x2="79.91931875" y2="26.414" width="0.225" layer="94"/>
<wire x1="79.91931875" y1="26.414" x2="79.91931875" y2="26.49631875" width="0.225" layer="94"/>
<wire x1="79.91931875" y1="26.49631875" x2="79.886946875" y2="26.744540625" width="0.225" layer="94" curve="14.861165"/>
<wire x1="79.88695" y1="26.744540625" x2="79.750771875" y2="26.951284375" width="0.225" layer="94" curve="37.022842"/>
<wire x1="79.75076875" y1="26.95128125" x2="79.521459375" y2="27.047065625" width="0.225" layer="94" curve="30.891086"/>
<wire x1="79.521459375" y1="27.04706875" x2="79.27088125" y2="27.055365625" width="0.225" layer="94" curve="10.657067"/>
<wire x1="79.27088125" y1="27.05536875" x2="79.02848125" y2="26.995190625" width="0.225" layer="94" curve="21.019898"/>
<wire x1="79.02848125" y1="26.995190625" x2="78.84638125" y2="26.82721875" width="0.225" layer="94" curve="36.47327"/>
<wire x1="78.84638125" y1="26.82721875" x2="78.77521875" y2="26.639" width="0.225" layer="94" curve="16.726592"/>
<wire x1="78.77521875" y1="26.639" x2="78.7664125" y2="26.590590625" width="0.225" layer="94" curve="4.076419"/>
<wire x1="78.766409375" y1="26.590590625" x2="78.75028125" y2="26.414" width="0.225" layer="94" curve="6.105964"/>
<wire x1="78.75028125" y1="26.414" x2="78.749128125" y2="26.34016875" width="0.225" layer="94" curve="2.541594"/>
<wire x1="79.95228125" y1="26.071809375" x2="79.95228125" y2="25.964" width="0.225" layer="94"/>
<wire x1="79.95228125" y1="25.964" x2="79.95228125" y2="25.89603125" width="0.225" layer="94"/>
<wire x1="79.922940625" y1="25.63598125" x2="79.94166875" y2="25.739" width="0.225" layer="94" curve="5.141671"/>
<wire x1="79.94166875" y1="25.739" x2="79.952278125" y2="25.89603125" width="0.225" layer="94" curve="7.731903"/>
<wire x1="79.79936875" y1="25.40781875" x2="79.922940625" y2="25.63598125" width="0.225" layer="94" curve="31.131749"/>
<wire x1="79.572809375" y1="25.282109375" x2="79.60065" y2="25.289" width="0.225" layer="94" curve="3.702975"/>
<wire x1="79.60065" y1="25.289" x2="79.799371875" y2="25.407815625" width="0.225" layer="94" curve="30.237845"/>
<wire x1="79.312259375" y1="25.25946875" x2="79.572809375" y2="25.28210625" width="0.225" layer="94" curve="14.175788"/>
<wire x1="79.056009375" y1="25.309240625" x2="79.119840625" y2="25.289" width="0.225" layer="94" curve="4.53295"/>
<wire x1="79.119840625" y1="25.289" x2="79.312259375" y2="25.259475" width="0.225" layer="94" curve="13.203582"/>
<wire x1="78.856609375" y1="25.473640625" x2="79.056009375" y2="25.3092375" width="0.225" layer="94" curve="39.29237"/>
<wire x1="78.767890625" y1="25.718659375" x2="78.85660625" y2="25.473640625" width="0.225" layer="94" curve="21.889189"/>
<wire x1="78.74913125" y1="25.980109375" x2="78.76789375" y2="25.718659375" width="0.225" layer="94" curve="9.71004"/>
<wire x1="78.74913125" y1="25.980109375" x2="78.74913125" y2="26.189" width="0.225" layer="94"/>
<wire x1="78.74913125" y1="26.189" x2="78.74913125" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="79.49928125" y1="26.30881875" x2="79.49928125" y2="26.414" width="0.225" layer="94"/>
<wire x1="79.49928125" y1="26.414" x2="79.49928125" y2="26.44828125" width="0.225" layer="94"/>
<wire x1="79.49928125" y1="26.44828125" x2="79.496253125" y2="26.51431875" width="0.225" layer="94" curve="5.249884"/>
<wire x1="79.49625" y1="26.51431875" x2="79.48934375" y2="26.557840625" width="0.225" layer="94" curve="7.529032"/>
<wire x1="79.489340625" y1="26.557840625" x2="79.483196875" y2="26.579" width="0.225" layer="94" curve="6.832689"/>
<wire x1="79.4832" y1="26.579" x2="79.47398125" y2="26.598990625" width="0.225" layer="94" curve="10.282953"/>
<wire x1="79.47398125" y1="26.598990625" x2="79.459609375" y2="26.615509375" width="0.225" layer="94" curve="22.257449"/>
<wire x1="79.459609375" y1="26.615509375" x2="79.440259375" y2="26.625909375" width="0.225" layer="94" curve="19.188066"/>
<wire x1="79.440259375" y1="26.625909375" x2="79.39738125" y2="26.63575" width="0.225" layer="94" curve="11.463239"/>
<wire x1="79.39738125" y1="26.63575" x2="79.3534" y2="26.6386375" width="0.225" layer="94" curve="6.876925"/>
<wire x1="79.3534" y1="26.638640625" x2="79.30931875" y2="26.63774375" width="0.225" layer="94" curve="2.970191"/>
<wire x1="79.30931875" y1="26.637740625" x2="79.265559375" y2="26.632625" width="0.225" layer="94" curve="8.0302"/>
<wire x1="79.265559375" y1="26.63263125" x2="79.244209375" y2="26.6272" width="0.225" layer="94" curve="7.184068"/>
<wire x1="79.244209375" y1="26.6272" x2="79.2243" y2="26.617890625" width="0.225" layer="94" curve="14.385749"/>
<wire x1="79.2243" y1="26.617890625" x2="79.210078125" y2="26.6013" width="0.225" layer="94" curve="34.295039"/>
<wire x1="79.21008125" y1="26.6013" x2="79.191453125" y2="26.53813125" width="0.225" layer="94" curve="14.037627"/>
<wire x1="79.19145" y1="26.53813125" x2="79.1844125" y2="26.472390625" width="0.225" layer="94" curve="6.60122"/>
<wire x1="79.52623125" y1="26.071809375" x2="79.52623125" y2="25.964" width="0.225" layer="94"/>
<wire x1="79.52623125" y1="25.964" x2="79.52623125" y2="25.89925" width="0.225" layer="94"/>
<wire x1="79.51938125" y1="25.780190625" x2="79.526234375" y2="25.89925" width="0.225" layer="94" curve="6.589587"/>
<wire x1="79.50856875" y1="25.733759375" x2="79.519378125" y2="25.780190625" width="0.225" layer="94" curve="13.026604"/>
<wire x1="79.45898125" y1="25.6871" x2="79.508565625" y2="25.733759375" width="0.225" layer="94" curve="54.248333"/>
<wire x1="79.4121" y1="25.6784" x2="79.45898125" y2="25.687103125" width="0.225" layer="94" curve="11.235116"/>
<wire x1="79.292940625" y1="25.678809375" x2="79.4121" y2="25.678396875" width="0.225" layer="94" curve="10.193817"/>
<wire x1="79.24633125" y1="25.688690625" x2="79.292940625" y2="25.6788125" width="0.225" layer="94" curve="13.338819"/>
<wire x1="79.22515" y1="25.6995" x2="79.24633125" y2="25.6886875" width="0.225" layer="94" curve="16.819291"/>
<wire x1="79.21035" y1="25.718009375" x2="79.225146875" y2="25.69949375" width="0.225" layer="94" curve="31.833994"/>
<wire x1="79.195259375" y1="25.763159375" x2="79.21035625" y2="25.7180125" width="0.225" layer="94" curve="8.452794"/>
<wire x1="79.187759375" y1="25.810290625" x2="79.19525625" y2="25.763159375" width="0.225" layer="94" curve="10.449934"/>
<wire x1="79.187759375" y1="25.810290625" x2="79.18275" y2="25.881709375" width="0.225" layer="94"/>
<wire x1="79.18441875" y1="26.472390625" x2="79.181740625" y2="26.406309375" width="0.225" layer="94"/>
<wire x1="79.181740625" y1="26.406309375" x2="79.181109375" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="79.181109375" y1="26.34016875" x2="79.181109375" y2="26.189" width="0.225" layer="94"/>
<wire x1="79.181109375" y1="26.189" x2="79.181109375" y2="25.97718125" width="0.225" layer="94"/>
<wire x1="79.181109375" y1="25.97718125" x2="79.18275" y2="25.881709375" width="0.225" layer="94"/>
<wire x1="77.1742" y1="26.422790625" x2="77.54935" y2="26.422790625" width="0.225" layer="94"/>
<wire x1="77.1742" y1="26.00881875" x2="77.969390625" y2="26.00881875" width="0.225" layer="94"/>
<wire x1="77.969390625" y1="26.00881875" x2="77.969390625" y2="26.189" width="0.225" layer="94"/>
<wire x1="77.969390625" y1="26.189" x2="77.969390625" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="77.969390625" y1="26.34016875" x2="77.948609375" y2="26.61348125" width="0.225" layer="94" curve="8.696144"/>
<wire x1="77.948609375" y1="26.61348125" x2="77.84784375" y2="26.866196875" width="0.225" layer="94" curve="26.085225"/>
<wire x1="77.84785" y1="26.8662" x2="77.62745" y2="27.022209375" width="0.225" layer="94" curve="39.85306"/>
<wire x1="77.62745" y1="27.022209375" x2="77.35661875" y2="27.058828125" width="0.225" layer="94" curve="15.332321"/>
<wire x1="77.35661875" y1="27.05881875" x2="77.08588125" y2="27.02155" width="0.225" layer="94" curve="15.743623"/>
<wire x1="77.08588125" y1="27.02155" x2="76.865265625" y2="26.865728125" width="0.225" layer="94" curve="39.046943"/>
<wire x1="76.865259375" y1="26.86573125" x2="76.864040625" y2="26.864" width="0.225" layer="94" curve="0.204743"/>
<wire x1="76.864040625" y1="26.864" x2="76.763478125" y2="26.61343125" width="0.225" layer="94" curve="26.341038"/>
<wire x1="76.76348125" y1="26.61343125" x2="76.74395" y2="26.414" width="0.225" layer="94" curve="6.208124"/>
<wire x1="76.74395" y1="26.414" x2="76.74221875" y2="26.34016875" width="0.225" layer="94" curve="2.287022"/>
<wire x1="77.561359375" y1="25.98158125" x2="77.966390625" y2="25.98158125" width="0.225" layer="94"/>
<wire x1="77.966390625" y1="25.98158125" x2="77.966390625" y2="25.964" width="0.225" layer="94"/>
<wire x1="77.966390625" y1="25.964" x2="77.966390625" y2="25.8181" width="0.225" layer="94"/>
<wire x1="77.92765" y1="25.565709375" x2="77.966390625" y2="25.8181" width="0.225" layer="94" curve="17.452125"/>
<wire x1="77.77643125" y1="25.363190625" x2="77.92765" y2="25.565709375" width="0.225" layer="94" curve="38.592139"/>
<wire x1="77.53896875" y1="25.271940625" x2="77.618090625" y2="25.289" width="0.225" layer="94" curve="8.168491"/>
<wire x1="77.618090625" y1="25.289" x2="77.77643125" y2="25.363190625" width="0.225" layer="94" curve="17.702304"/>
<wire x1="77.28301875" y1="25.26058125" x2="77.53896875" y2="25.271940625" width="0.225" layer="94" curve="11.086795"/>
<wire x1="77.03403125" y1="25.31643125" x2="77.11308125" y2="25.289" width="0.225" layer="94" curve="6.296209"/>
<wire x1="77.11308125" y1="25.289" x2="77.28301875" y2="25.260578125" width="0.225" layer="94" curve="12.986111"/>
<wire x1="76.844159375" y1="25.483540625" x2="77.034028125" y2="25.316421875" width="0.225" layer="94" curve="38.138006"/>
<wire x1="76.760209375" y1="25.724359375" x2="76.8441625" y2="25.483540625" width="0.225" layer="94" curve="20.717901"/>
<wire x1="76.74221875" y1="25.980109375" x2="76.758" y2="25.739" width="0.225" layer="94" curve="9.114768"/>
<wire x1="76.758" y1="25.739" x2="76.760209375" y2="25.724359375" width="0.225" layer="94" curve="0.557951"/>
<wire x1="76.74221875" y1="25.980109375" x2="76.74221875" y2="26.189" width="0.225" layer="94"/>
<wire x1="76.74221875" y1="26.189" x2="76.74221875" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="77.1742" y1="26.00881875" x2="77.1742" y2="25.97718125" width="0.225" layer="94"/>
<wire x1="77.1742" y1="25.97718125" x2="77.17648125" y2="25.863140625" width="0.225" layer="94"/>
<wire x1="77.17648125" y1="25.863140625" x2="77.179640625" y2="25.81763125" width="0.225" layer="94"/>
<wire x1="77.179640625" y1="25.81763125" x2="77.185759375" y2="25.77241875" width="0.225" layer="94"/>
<wire x1="77.185759375" y1="25.77241875" x2="77.19780625" y2="25.728490625" width="0.225" layer="94" curve="15.249939"/>
<wire x1="77.197809375" y1="25.728490625" x2="77.22771875" y2="25.69585" width="0.225" layer="94" curve="39.077584"/>
<wire x1="77.22771875" y1="25.69585" x2="77.2493" y2="25.688528125" width="0.225" layer="94" curve="18.445063"/>
<wire x1="77.2493" y1="25.68853125" x2="77.45375" y2="25.6834" width="0.225" layer="94" curve="16.158964"/>
<wire x1="77.45375" y1="25.6834" x2="77.498609375" y2="25.691559375" width="0.225" layer="94" curve="7.337797"/>
<wire x1="77.498609375" y1="25.691559375" x2="77.520209375" y2="25.6988375" width="0.225" layer="94" curve="9.286376"/>
<wire x1="77.561359375" y1="25.98158125" x2="77.561359375" y2="25.964" width="0.225" layer="94"/>
<wire x1="77.561359375" y1="25.964" x2="77.561359375" y2="25.82103125" width="0.225" layer="94"/>
<wire x1="77.561359375" y1="25.82103125" x2="77.559340625" y2="25.775459375" width="0.225" layer="94"/>
<wire x1="77.559340625" y1="25.775459375" x2="77.55613125" y2="25.75288125" width="0.225" layer="94"/>
<wire x1="77.520209375" y1="25.698840625" x2="77.556125" y2="25.75288125" width="0.225" layer="94" curve="51.030296"/>
<wire x1="77.184159375" y1="26.547240625" x2="77.174196875" y2="26.422790625" width="0.225" layer="94" curve="9.151585"/>
<wire x1="77.2077" y1="26.6134" x2="77.184159375" y2="26.547240625" width="0.225" layer="94" curve="20.872634"/>
<wire x1="77.54935" y1="26.422790625" x2="77.539775" y2="26.547290625" width="0.225" layer="94" curve="8.794744"/>
<wire x1="77.53978125" y1="26.547290625" x2="77.527525" y2="26.592734375" width="0.225" layer="94" curve="12.594721"/>
<wire x1="77.52751875" y1="26.59273125" x2="77.516803125" y2="26.613665625" width="0.225" layer="94" curve="11.429217"/>
<wire x1="77.516809375" y1="26.61366875" x2="77.5000125" y2="26.629975" width="0.225" layer="94" curve="26.064892"/>
<wire x1="77.500009375" y1="26.62996875" x2="77.47873125" y2="26.63995" width="0.225" layer="94" curve="11.965711"/>
<wire x1="77.47873125" y1="26.63995" x2="77.455990625" y2="26.64610625" width="0.225" layer="94" curve="8.00028"/>
<wire x1="77.455990625" y1="26.646109375" x2="77.38576875" y2="26.653571875" width="0.225" layer="94" curve="10.160203"/>
<wire x1="77.38576875" y1="26.65356875" x2="77.315109375" y2="26.652346875" width="0.225" layer="94" curve="3.952821"/>
<wire x1="77.315109375" y1="26.65235" x2="77.268440625" y2="26.646071875" width="0.225" layer="94" curve="9.390964"/>
<wire x1="77.268440625" y1="26.64606875" x2="77.245740625" y2="26.639796875" width="0.225" layer="94" curve="6.177099"/>
<wire x1="77.245740625" y1="26.6398" x2="77.24345" y2="26.639" width="0.225" layer="94" curve="1.397304"/>
<wire x1="77.24345" y1="26.639" x2="77.22448125" y2="26.629740625" width="0.225" layer="94" curve="12.178653"/>
<wire x1="77.22448125" y1="26.629740625" x2="77.207703125" y2="26.613396875" width="0.225" layer="94" curve="24.279547"/>
<wire x1="73.9395" y1="26.629740625" x2="73.922721875" y2="26.613396875" width="0.225" layer="94" curve="24.279547"/>
<wire x1="73.96075" y1="26.6398" x2="73.958459375" y2="26.639" width="0.225" layer="94" curve="1.397249"/>
<wire x1="73.958459375" y1="26.639" x2="73.939496875" y2="26.62974375" width="0.225" layer="94" curve="12.174138"/>
<wire x1="73.98345" y1="26.64606875" x2="73.96075" y2="26.639796875" width="0.225" layer="94" curve="6.177099"/>
<wire x1="74.03013125" y1="26.65235" x2="73.98345" y2="26.64606875" width="0.225" layer="94" curve="9.393557"/>
<wire x1="74.100790625" y1="26.65356875" x2="74.03013125" y2="26.652346875" width="0.225" layer="94" curve="3.952821"/>
<wire x1="74.171009375" y1="26.646109375" x2="74.100790625" y2="26.653571875" width="0.225" layer="94" curve="10.159745"/>
<wire x1="74.193740625" y1="26.63995" x2="74.171009375" y2="26.646103125" width="0.225" layer="94" curve="7.996993"/>
<wire x1="74.21501875" y1="26.62996875" x2="74.193740625" y2="26.63995" width="0.225" layer="94" curve="11.965711"/>
<wire x1="74.23181875" y1="26.61366875" x2="74.215021875" y2="26.629975" width="0.225" layer="94" curve="26.067092"/>
<wire x1="74.242540625" y1="26.59273125" x2="74.231821875" y2="26.613671875" width="0.225" layer="94" curve="11.432049"/>
<wire x1="74.254790625" y1="26.547290625" x2="74.2425375" y2="26.59273125" width="0.225" layer="94" curve="12.593993"/>
<wire x1="74.264359375" y1="26.422790625" x2="74.254784375" y2="26.547290625" width="0.225" layer="94" curve="8.794744"/>
<wire x1="73.92271875" y1="26.6134" x2="73.899178125" y2="26.547240625" width="0.225" layer="94" curve="20.872634"/>
<wire x1="73.89918125" y1="26.547240625" x2="73.88921875" y2="26.422790625" width="0.225" layer="94" curve="9.151622"/>
<wire x1="74.23521875" y1="25.698840625" x2="74.2678" y2="25.739" width="0.225" layer="94" curve="40.155698"/>
<wire x1="74.2678" y1="25.739" x2="74.271134375" y2="25.75288125" width="0.225" layer="94" curve="10.876304"/>
<wire x1="74.27435" y1="25.775459375" x2="74.271140625" y2="25.75288125" width="0.225" layer="94"/>
<wire x1="74.27638125" y1="25.82103125" x2="74.27435" y2="25.775459375" width="0.225" layer="94"/>
<wire x1="74.27638125" y1="25.98158125" x2="74.27638125" y2="25.964" width="0.225" layer="94"/>
<wire x1="74.27638125" y1="25.964" x2="74.27638125" y2="25.82103125" width="0.225" layer="94"/>
<wire x1="74.21361875" y1="25.691559375" x2="74.23521875" y2="25.6988375" width="0.225" layer="94" curve="9.286507"/>
<wire x1="74.16876875" y1="25.6834" x2="74.21361875" y2="25.691559375" width="0.225" layer="94" curve="7.336311"/>
<wire x1="73.964309375" y1="25.68853125" x2="74.16876875" y2="25.6834" width="0.225" layer="94" curve="16.159693"/>
<wire x1="73.94273125" y1="25.69585" x2="73.964309375" y2="25.68853125" width="0.225" layer="94" curve="18.441658"/>
<wire x1="73.91281875" y1="25.728490625" x2="73.942728125" y2="25.695846875" width="0.225" layer="94" curve="39.07874"/>
<wire x1="73.90078125" y1="25.77241875" x2="73.912828125" y2="25.72849375" width="0.225" layer="94" curve="15.249051"/>
<wire x1="73.894659375" y1="25.81763125" x2="73.90078125" y2="25.77241875" width="0.225" layer="94"/>
<wire x1="73.891490625" y1="25.863140625" x2="73.894659375" y2="25.81763125" width="0.225" layer="94"/>
<wire x1="73.88921875" y1="25.97718125" x2="73.88948125" y2="25.964" width="0.225" layer="94"/>
<wire x1="73.88948125" y1="25.964" x2="73.891490625" y2="25.863140625" width="0.225" layer="94"/>
<wire x1="73.88921875" y1="26.00881875" x2="73.88921875" y2="25.97718125" width="0.225" layer="94"/>
<wire x1="73.457240625" y1="25.980109375" x2="73.457240625" y2="26.189" width="0.225" layer="94"/>
<wire x1="73.457240625" y1="26.189" x2="73.457240625" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="73.457240625" y1="25.980109375" x2="73.47523125" y2="25.724359375" width="0.225" layer="94" curve="9.672758"/>
<wire x1="73.47523125" y1="25.724359375" x2="73.559184375" y2="25.48354375" width="0.225" layer="94" curve="20.717853"/>
<wire x1="73.55918125" y1="25.483540625" x2="73.749046875" y2="25.316425" width="0.225" layer="94" curve="38.137176"/>
<wire x1="73.74905" y1="25.31643125" x2="73.998040625" y2="25.26058125" width="0.225" layer="94" curve="19.282594"/>
<wire x1="73.998040625" y1="25.26058125" x2="74.25398125" y2="25.271940625" width="0.225" layer="94" curve="11.086392"/>
<wire x1="74.25398125" y1="25.271940625" x2="74.3331" y2="25.289" width="0.225" layer="94" curve="8.168179"/>
<wire x1="74.3331" y1="25.289" x2="74.491446875" y2="25.36319375" width="0.225" layer="94" curve="17.703003"/>
<wire x1="74.49145" y1="25.363190625" x2="74.642665625" y2="25.565709375" width="0.225" layer="94" curve="38.59175"/>
<wire x1="74.64266875" y1="25.565709375" x2="74.681409375" y2="25.8181" width="0.225" layer="94" curve="17.451998"/>
<wire x1="74.681409375" y1="25.98158125" x2="74.681409375" y2="25.964" width="0.225" layer="94"/>
<wire x1="74.681409375" y1="25.964" x2="74.681409375" y2="25.8181" width="0.225" layer="94"/>
<wire x1="74.27638125" y1="25.98158125" x2="74.681409375" y2="25.98158125" width="0.225" layer="94"/>
<wire x1="73.4785" y1="26.61343125" x2="73.45896875" y2="26.414" width="0.225" layer="94" curve="6.208134"/>
<wire x1="73.45896875" y1="26.414" x2="73.4572375" y2="26.34016875" width="0.225" layer="94" curve="2.287026"/>
<wire x1="73.58028125" y1="26.86573125" x2="73.4785" y2="26.61343125" width="0.225" layer="94" curve="26.545833"/>
<wire x1="73.8009" y1="27.02155" x2="73.58028125" y2="26.86573125" width="0.225" layer="94" curve="39.047008"/>
<wire x1="74.071640625" y1="27.05881875" x2="73.8009" y2="27.02155" width="0.225" layer="94" curve="15.743809"/>
<wire x1="74.342459375" y1="27.022209375" x2="74.071640625" y2="27.058825" width="0.225" layer="94" curve="15.331635"/>
<wire x1="74.562859375" y1="26.8662" x2="74.342459375" y2="27.022209375" width="0.225" layer="94" curve="39.85306"/>
<wire x1="74.66363125" y1="26.61348125" x2="74.5628625" y2="26.866203125" width="0.225" layer="94" curve="26.085633"/>
<wire x1="74.684409375" y1="26.34016875" x2="74.663628125" y2="26.61348125" width="0.225" layer="94" curve="8.69613"/>
<wire x1="74.684409375" y1="26.00881875" x2="74.684409375" y2="26.189" width="0.225" layer="94"/>
<wire x1="74.684409375" y1="26.189" x2="74.684409375" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="73.88921875" y1="26.00881875" x2="74.684409375" y2="26.00881875" width="0.225" layer="94"/>
<wire x1="73.88921875" y1="26.422790625" x2="74.264359375" y2="26.422790625" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="25.28548125" x2="82.92818125" y2="25.28548125" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="27.03186875" x2="82.80066875" y2="27.03186875" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="27.03186875" x2="82.508140625" y2="26.864" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="26.864" x2="82.508140625" y2="26.639" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="26.639" x2="82.508140625" y2="26.414" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="26.414" x2="82.508140625" y2="26.189" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="26.189" x2="82.508140625" y2="25.964" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="25.964" x2="82.508140625" y2="25.739" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="25.739" x2="82.508140625" y2="25.514" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="25.514" x2="82.508140625" y2="25.289" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="25.289" x2="82.508140625" y2="25.28548125" width="0.225" layer="94"/>
<wire x1="83.18028125" y1="26.30266875" x2="83.585309375" y2="26.30266875" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="26.358340625" x2="82.92818125" y2="26.189" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="26.189" x2="82.92818125" y2="25.964" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="25.964" x2="82.92818125" y2="25.739" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="25.739" x2="82.92818125" y2="25.514" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="25.514" x2="82.92818125" y2="25.289" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="25.289" x2="82.92818125" y2="25.28548125" width="0.225" layer="94"/>
<wire x1="82.8883" y1="26.98991875" x2="82.80066875" y2="27.03186875" width="0.225" layer="94" curve="51.162574"/>
<wire x1="86.810090625" y1="25.28548125" x2="86.810090625" y2="25.289" width="0.225" layer="94"/>
<wire x1="86.810090625" y1="25.289" x2="86.810090625" y2="25.514" width="0.225" layer="94"/>
<wire x1="86.810090625" y1="25.514" x2="86.810090625" y2="25.739" width="0.225" layer="94"/>
<wire x1="86.810090625" y1="25.739" x2="86.810090625" y2="25.964" width="0.225" layer="94"/>
<wire x1="86.810090625" y1="25.964" x2="86.810090625" y2="26.189" width="0.225" layer="94"/>
<wire x1="86.810090625" y1="26.189" x2="86.810090625" y2="26.25023125" width="0.225" layer="94"/>
<wire x1="86.39005" y1="25.28548125" x2="86.39005" y2="25.289" width="0.225" layer="94"/>
<wire x1="86.39005" y1="25.289" x2="86.39005" y2="25.514" width="0.225" layer="94"/>
<wire x1="86.39005" y1="25.514" x2="86.39005" y2="25.739" width="0.225" layer="94"/>
<wire x1="86.39005" y1="25.739" x2="86.39005" y2="25.964" width="0.225" layer="94"/>
<wire x1="86.39005" y1="25.964" x2="86.39005" y2="26.189" width="0.225" layer="94"/>
<wire x1="86.39005" y1="26.189" x2="86.39005" y2="26.414" width="0.225" layer="94"/>
<wire x1="86.39005" y1="26.414" x2="86.39005" y2="26.639" width="0.225" layer="94"/>
<wire x1="86.39005" y1="26.639" x2="86.39005" y2="26.864" width="0.225" layer="94"/>
<wire x1="86.39005" y1="26.864" x2="86.39005" y2="27.03186875" width="0.225" layer="94"/>
<wire x1="86.39005" y1="25.28548125" x2="86.810090625" y2="25.28548125" width="0.225" layer="94"/>
<wire x1="87.1462" y1="25.28548125" x2="87.566240625" y2="25.28548125" width="0.225" layer="94"/>
<wire x1="86.68258125" y1="27.03186875" x2="86.39005" y2="27.03186875" width="0.225" layer="94"/>
<wire x1="86.770490625" y1="26.98956875" x2="86.68258125" y2="27.03186875" width="0.225" layer="94" curve="51.389654"/>
<wire x1="83.585309375" y1="26.30266875" x2="83.585309375" y2="26.414" width="0.225" layer="94"/>
<wire x1="83.585309375" y1="26.414" x2="83.585309375" y2="26.532359375" width="0.225" layer="94"/>
<wire x1="83.585309375" y1="26.532359375" x2="83.57331875" y2="26.7009" width="0.225" layer="94" curve="8.13878"/>
<wire x1="83.57331875" y1="26.7009" x2="83.52315625" y2="26.8617" width="0.225" layer="94" curve="18.374099"/>
<wire x1="83.523159375" y1="26.8617" x2="83.41396875" y2="26.9887" width="0.225" layer="94" curve="28.349931"/>
<wire x1="83.41396875" y1="26.9887" x2="83.25806875" y2="27.050721875" width="0.225" layer="94" curve="26.884991"/>
<wire x1="83.25806875" y1="27.05071875" x2="83.0895" y2="27.055371875" width="0.225" layer="94" curve="13.343084"/>
<wire x1="83.0895" y1="27.05538125" x2="82.888296875" y2="26.989925" width="0.225" layer="94" curve="25.861473"/>
<wire x1="87.566240625" y1="25.28548125" x2="87.566240625" y2="25.289" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="25.289" x2="87.566240625" y2="25.514" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="25.514" x2="87.566240625" y2="25.739" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="25.739" x2="87.566240625" y2="25.964" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="25.964" x2="87.566240625" y2="26.189" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="26.189" x2="87.566240625" y2="26.414" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="26.414" x2="87.566240625" y2="26.418390625" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="26.418390625" x2="87.555215625" y2="26.606540625" width="0.225" layer="94" curve="6.706552"/>
<wire x1="87.55521875" y1="26.606540625" x2="87.509115625" y2="26.788871875" width="0.225" layer="94" curve="14.968121"/>
<wire x1="87.509109375" y1="26.78886875" x2="87.403925" y2="26.943603125" width="0.225" layer="94" curve="25.064472"/>
<wire x1="87.40393125" y1="26.943609375" x2="87.240590625" y2="27.034540625" width="0.225" layer="94" curve="28.312195"/>
<wire x1="87.240590625" y1="27.034540625" x2="87.05421875" y2="27.058825" width="0.225" layer="94" curve="15.049352"/>
<wire x1="87.05421875" y1="27.05881875" x2="86.770490625" y2="26.98956875" width="0.225" layer="94" curve="27.231353"/>
<wire x1="87.1462" y1="25.28548125" x2="87.1462" y2="25.289" width="0.225" layer="94"/>
<wire x1="87.1462" y1="25.289" x2="87.1462" y2="25.514" width="0.225" layer="94"/>
<wire x1="87.1462" y1="25.514" x2="87.1462" y2="25.739" width="0.225" layer="94"/>
<wire x1="87.1462" y1="25.739" x2="87.1462" y2="25.964" width="0.225" layer="94"/>
<wire x1="87.1462" y1="25.964" x2="87.1462" y2="26.189" width="0.225" layer="94"/>
<wire x1="87.1462" y1="26.189" x2="87.1462" y2="26.3642" width="0.225" layer="94"/>
<wire x1="87.1462" y1="26.3642" x2="87.139771875" y2="26.502909375" width="0.225" layer="94" curve="5.307835"/>
<wire x1="87.13976875" y1="26.502909375" x2="87.123915625" y2="26.584609375" width="0.225" layer="94" curve="11.346275"/>
<wire x1="87.12391875" y1="26.584609375" x2="87.11383125" y2="26.61046875" width="0.225" layer="94" curve="9.30383"/>
<wire x1="87.11383125" y1="26.61046875" x2="87.098490625" y2="26.633490625" width="0.225" layer="94" curve="15.447128"/>
<wire x1="87.098490625" y1="26.633490625" x2="87.0752" y2="26.648190625" width="0.225" layer="94" curve="32.662799"/>
<wire x1="87.0752" y1="26.648190625" x2="86.99275" y2="26.656371875" width="0.225" layer="94" curve="20.527762"/>
<wire x1="86.99275" y1="26.65636875" x2="86.9651" y2="26.653715625" width="0.225" layer="94" curve="1.764695"/>
<wire x1="86.9651" y1="26.65371875" x2="86.912034375" y2="26.638084375" width="0.225" layer="94" curve="20.107123"/>
<wire x1="86.91203125" y1="26.638090625" x2="86.870678125" y2="26.601821875" width="0.225" layer="94" curve="29.565942"/>
<wire x1="86.87068125" y1="26.60181875" x2="86.84560625" y2="26.55240625" width="0.225" layer="94" curve="14.114289"/>
<wire x1="86.8456" y1="26.552409375" x2="86.824978125" y2="26.471759375" width="0.225" layer="94" curve="11.013017"/>
<wire x1="86.82498125" y1="26.471759375" x2="86.813053125" y2="26.36131875" width="0.225" layer="94" curve="5.344301"/>
<wire x1="86.81305" y1="26.36131875" x2="86.810090625" y2="26.25023125" width="0.225" layer="94" curve="3.932748"/>
<wire x1="83.18028125" y1="26.30266875" x2="83.18028125" y2="26.414" width="0.225" layer="94"/>
<wire x1="83.18028125" y1="26.414" x2="83.18028125" y2="26.487240625" width="0.225" layer="94"/>
<wire x1="83.18028125" y1="26.487240625" x2="83.176690625" y2="26.56661875" width="0.225" layer="94" curve="5.181151"/>
<wire x1="83.176690625" y1="26.56661875" x2="83.16605625" y2="26.625190625" width="0.225" layer="94" curve="10.220705"/>
<wire x1="83.166059375" y1="26.625190625" x2="83.12728125" y2="26.664134375" width="0.225" layer="94" curve="58.95127"/>
<wire x1="83.12728125" y1="26.66413125" x2="82.99755" y2="26.639" width="0.225" layer="94" curve="53.217192"/>
<wire x1="82.99755" y1="26.639" x2="82.95845" y2="26.593540625" width="0.225" layer="94" curve="23.452395"/>
<wire x1="82.95845" y1="26.593540625" x2="82.95161875" y2="26.574890625" width="0.225" layer="94"/>
<wire x1="82.95161875" y1="26.574890625" x2="82.946159375" y2="26.55578125" width="0.225" layer="94"/>
<wire x1="82.946159375" y1="26.55578125" x2="82.941775" y2="26.5364" width="0.225" layer="94" curve="6.402047"/>
<wire x1="82.94176875" y1="26.5364" x2="82.931515625" y2="26.45761875" width="0.225" layer="94" curve="4.264451"/>
<wire x1="82.93151875" y1="26.45761875" x2="82.930209375" y2="26.437790625" width="0.225" layer="94"/>
<wire x1="82.930209375" y1="26.437790625" x2="82.92865" y2="26.39808125" width="0.225" layer="94"/>
<wire x1="82.92865" y1="26.39808125" x2="82.92818125" y2="26.358340625" width="0.225" layer="94"/>
<wire x1="92.67961875" y1="26.22455" x2="92.54725" y2="26.31605" width="0.225" layer="94" curve="24.406928"/>
<wire x1="92.54725" y1="26.31605" x2="92.391190625" y2="26.358428125" width="0.225" layer="94" curve="14.517731"/>
<wire x1="92.391190625" y1="26.35841875" x2="92.2218" y2="26.3835" width="0.225" layer="94"/>
<wire x1="92.2218" y1="26.3835" x2="92.13726875" y2="26.39681875" width="0.225" layer="94"/>
<wire x1="92.09533125" y1="26.40531875" x2="92.13726875" y2="26.39681875" width="0.225" layer="94" curve="5.000072"/>
<wire x1="92.0546" y1="26.41821875" x2="92.0661" y2="26.414" width="0.225" layer="94" curve="2.068384"/>
<wire x1="92.0661" y1="26.414" x2="92.09533125" y2="26.405321875" width="0.225" layer="94" curve="5.150249"/>
<wire x1="92.054009375" y1="26.430409375" x2="92.054596875" y2="26.41821875" width="0.225" layer="94" curve="5.514941"/>
<wire x1="92.054009375" y1="26.430409375" x2="92.054009375" y2="26.57718125" width="0.225" layer="94"/>
<wire x1="92.05665" y1="26.61703125" x2="92.0540125" y2="26.57718125" width="0.225" layer="94" curve="7.573746"/>
<wire x1="92.061659375" y1="26.63633125" x2="92.056646875" y2="26.61703125" width="0.225" layer="94" curve="13.971436"/>
<wire x1="92.068809375" y1="26.642740625" x2="92.0616625" y2="26.636328125" width="0.225" layer="94" curve="41.625207"/>
<wire x1="92.097790625" y1="26.650090625" x2="92.068809375" y2="26.642740625" width="0.225" layer="94" curve="13.721902"/>
<wire x1="92.15758125" y1="26.653940625" x2="92.097790625" y2="26.650090625" width="0.225" layer="94" curve="7.371099"/>
<wire x1="92.15758125" y1="26.653940625" x2="92.2566" y2="26.653940625" width="0.225" layer="94"/>
<wire x1="92.29483125" y1="26.652240625" x2="92.2566" y2="26.653940625" width="0.225" layer="94" curve="5.089448"/>
<wire x1="92.32143125" y1="26.647590625" x2="92.29483125" y2="26.652240625" width="0.225" layer="94" curve="9.654072"/>
<wire x1="92.331859375" y1="26.64346875" x2="92.32143125" y2="26.6475875" width="0.225" layer="94" curve="13.639596"/>
<wire x1="92.33591875" y1="26.63303125" x2="92.33185625" y2="26.64346875" width="0.225" layer="94" curve="15.08669"/>
<wire x1="92.34036875" y1="26.6065" x2="92.335915625" y2="26.63303125" width="0.225" layer="94" curve="8.390546"/>
<wire x1="92.34215" y1="26.568390625" x2="92.340375" y2="26.6065" width="0.225" layer="94" curve="5.332675"/>
<wire x1="92.34215" y1="26.568390625" x2="92.34215" y2="26.414" width="0.225" layer="94"/>
<wire x1="92.34215" y1="26.414" x2="92.34215" y2="26.413709375" width="0.225" layer="94"/>
<wire x1="92.34215" y1="26.413709375" x2="92.759190625" y2="26.413709375" width="0.225" layer="94"/>
<wire x1="92.759190625" y1="26.413709375" x2="92.759190625" y2="26.414" width="0.225" layer="94"/>
<wire x1="92.759190625" y1="26.414" x2="92.759190625" y2="26.5892" width="0.225" layer="94"/>
<wire x1="92.759190625" y1="26.5892" x2="92.738496875" y2="26.78398125" width="0.225" layer="94" curve="12.129007"/>
<wire x1="92.738490625" y1="26.78398125" x2="92.642896875" y2="26.952528125" width="0.225" layer="94" curve="34.862523"/>
<wire x1="92.469709375" y1="27.040159375" x2="92.27461875" y2="27.058825" width="0.225" layer="94" curve="10.407762"/>
<wire x1="92.27461875" y1="27.05881875" x2="92.139559375" y2="27.05881875" width="0.225" layer="94"/>
<wire x1="92.139559375" y1="27.05881875" x2="91.94439375" y2="27.0367625" width="0.225" layer="94" curve="15.066235"/>
<wire x1="91.944390625" y1="27.03676875" x2="91.76905625" y2="26.950671875" width="0.225" layer="94" curve="24.344315"/>
<wire x1="91.769059375" y1="26.95066875" x2="91.656853125" y2="26.79153125" width="0.225" layer="94" curve="32.9752"/>
<wire x1="91.65685" y1="26.79153125" x2="91.626409375" y2="26.639" width="0.225" layer="94" curve="14.82531"/>
<wire x1="91.626409375" y1="26.639" x2="91.62503125" y2="26.59828125" width="0.225" layer="94" curve="3.873269"/>
<wire x1="91.62503125" y1="26.59828125" x2="91.62503125" y2="26.414" width="0.225" layer="94"/>
<wire x1="91.62503125" y1="26.414" x2="91.62503125" y2="26.39436875" width="0.225" layer="94"/>
<wire x1="91.62503125" y1="26.39436875" x2="91.654403125" y2="26.22111875" width="0.225" layer="94" curve="19.244585"/>
<wire x1="91.6544" y1="26.22111875" x2="91.767428125" y2="26.088775" width="0.225" layer="94" curve="42.510013"/>
<wire x1="91.76743125" y1="26.08878125" x2="91.931890625" y2="26.02583125" width="0.225" layer="94" curve="14.601117"/>
<wire x1="91.931890625" y1="26.02583125" x2="92.105790625" y2="25.992246875" width="0.225" layer="94" curve="5.428189"/>
<wire x1="92.105790625" y1="25.99225" x2="92.16531875" y2="25.98243125" width="0.225" layer="94"/>
<wire x1="92.16531875" y1="25.98243125" x2="92.22491875" y2="25.97266875" width="0.225" layer="94"/>
<wire x1="92.22491875" y1="25.97266875" x2="92.28428125" y2="25.96156875" width="0.225" layer="94"/>
<wire x1="92.327340625" y1="25.94943125" x2="92.28428125" y2="25.9615625" width="0.225" layer="94" curve="10.274433"/>
<wire x1="92.34125" y1="25.94243125" x2="92.32734375" y2="25.9494375" width="0.225" layer="94" curve="11.730311"/>
<wire x1="92.346959375" y1="25.91516875" x2="92.34124375" y2="25.942428125" width="0.225" layer="94" curve="13.906979"/>
<wire x1="92.34815" y1="25.887240625" x2="92.346959375" y2="25.91516875" width="0.225" layer="94" curve="4.888175"/>
<wire x1="92.34815" y1="25.887240625" x2="92.34815" y2="25.7431" width="0.225" layer="94"/>
<wire x1="92.346759375" y1="25.71286875" x2="92.348153125" y2="25.7431" width="0.225" layer="94" curve="5.283856"/>
<wire x1="92.34045" y1="25.683359375" x2="92.34675625" y2="25.71286875" width="0.225" layer="94" curve="13.55484"/>
<wire x1="92.333959375" y1="25.67608125" x2="92.340453125" y2="25.683359375" width="0.225" layer="94" curve="45.791754"/>
<wire x1="92.314840625" y1="25.66976875" x2="92.333959375" y2="25.676084375" width="0.225" layer="94" curve="14.183972"/>
<wire x1="92.274840625" y1="25.66463125" x2="92.314840625" y2="25.669771875" width="0.225" layer="94" curve="7.7277"/>
<wire x1="92.24416875" y1="25.663709375" x2="92.274840625" y2="25.664628125" width="0.225" layer="94" curve="3.484382"/>
<wire x1="92.24416875" y1="25.663709375" x2="92.1096" y2="25.663709375" width="0.225" layer="94"/>
<wire x1="92.07173125" y1="25.66515" x2="92.1096" y2="25.66370625" width="0.225" layer="94" curve="4.363033"/>
<wire x1="92.04318125" y1="25.66956875" x2="92.07173125" y2="25.66514375" width="0.225" layer="94" curve="8.889144"/>
<wire x1="92.03463125" y1="25.672309375" x2="92.04318125" y2="25.66956875" width="0.225" layer="94" curve="9.043139"/>
<wire x1="92.03186875" y1="25.680859375" x2="92.034628125" y2="25.672309375" width="0.225" layer="94" curve="7.986668"/>
<wire x1="92.02805" y1="25.70145" x2="92.031871875" y2="25.680859375" width="0.225" layer="94" curve="6.761788"/>
<wire x1="92.02548125" y1="25.73128125" x2="92.028053125" y2="25.70145" width="0.225" layer="94" curve="4.406702"/>
<wire x1="92.024059375" y1="25.79115" x2="92.025484375" y2="25.73128125" width="0.225" layer="94" curve="2.724283"/>
<wire x1="92.024059375" y1="25.79115" x2="92.024059375" y2="25.964" width="0.225" layer="94"/>
<wire x1="92.024059375" y1="25.964" x2="92.024059375" y2="25.99651875" width="0.225" layer="94"/>
<wire x1="92.024059375" y1="25.99651875" x2="91.60701875" y2="25.99651875" width="0.225" layer="94"/>
<wire x1="91.60701875" y1="25.99651875" x2="91.60701875" y2="25.964" width="0.225" layer="94"/>
<wire x1="91.60701875" y1="25.964" x2="91.60701875" y2="25.739" width="0.225" layer="94"/>
<wire x1="91.60701875" y1="25.739" x2="91.60701875" y2="25.728159375" width="0.225" layer="94"/>
<wire x1="91.60701875" y1="25.728159375" x2="91.6277125" y2="25.53338125" width="0.225" layer="94" curve="12.128778"/>
<wire x1="91.627709375" y1="25.53338125" x2="91.72330625" y2="25.364828125" width="0.225" layer="94" curve="34.863434"/>
<wire x1="91.723309375" y1="25.36483125" x2="91.85098125" y2="25.289" width="0.225" layer="94" curve="24.602823"/>
<wire x1="91.85098125" y1="25.289" x2="91.896490625" y2="25.277203125" width="0.225" layer="94" curve="7.735491"/>
<wire x1="91.896490625" y1="25.2772" x2="92.091590625" y2="25.258534375" width="0.225" layer="94" curve="10.40831"/>
<wire x1="92.091590625" y1="25.25853125" x2="92.262609375" y2="25.25853125" width="0.225" layer="94"/>
<wire x1="92.262609375" y1="25.25853125" x2="92.457790625" y2="25.2805875" width="0.225" layer="94" curve="15.067174"/>
<wire x1="92.457790625" y1="25.280590625" x2="92.633140625" y2="25.36668125" width="0.225" layer="94" curve="24.336812"/>
<wire x1="92.633140625" y1="25.36668125" x2="92.74113125" y2="25.514" width="0.225" layer="94" curve="30.878359"/>
<wire x1="92.74113125" y1="25.514" x2="92.74536875" y2="25.525809375" width="0.225" layer="94" curve="2.095521"/>
<wire x1="92.74536875" y1="25.525809375" x2="92.777203125" y2="25.71908125" width="0.225" layer="94" curve="18.707757"/>
<wire x1="92.777209375" y1="25.71908125" x2="92.777209375" y2="25.739" width="0.225" layer="94"/>
<wire x1="92.777209375" y1="25.739" x2="92.777209375" y2="25.92328125" width="0.225" layer="94"/>
<wire x1="92.777209375" y1="25.92328125" x2="92.756978125" y2="26.083621875" width="0.225" layer="94" curve="14.383055"/>
<wire x1="92.75696875" y1="26.08361875" x2="92.6796125" y2="26.22454375" width="0.225" layer="94" curve="28.759193"/>
<wire x1="64.9221" y1="37.86731875" x2="101.4471" y2="37.86731875" width="0.225" layer="94"/>
<wire x1="101.4471" y1="37.86731875" x2="101.4471" y2="37.664" width="0.225" layer="94"/>
<wire x1="101.4471" y1="37.664" x2="101.4471" y2="37.439" width="0.225" layer="94"/>
<wire x1="101.4471" y1="37.439" x2="101.4471" y2="37.214" width="0.225" layer="94"/>
<wire x1="101.4471" y1="37.214" x2="101.4471" y2="36.989" width="0.225" layer="94"/>
<wire x1="101.4471" y1="36.989" x2="101.4471" y2="36.764" width="0.225" layer="94"/>
<wire x1="101.4471" y1="36.764" x2="101.4471" y2="36.539" width="0.225" layer="94"/>
<wire x1="101.4471" y1="36.539" x2="101.4471" y2="36.314" width="0.225" layer="94"/>
<wire x1="101.4471" y1="36.314" x2="101.4471" y2="36.089" width="0.225" layer="94"/>
<wire x1="101.4471" y1="36.089" x2="101.4471" y2="35.864" width="0.225" layer="94"/>
<wire x1="101.4471" y1="35.864" x2="101.4471" y2="35.639" width="0.225" layer="94"/>
<wire x1="101.4471" y1="35.639" x2="101.4471" y2="35.414" width="0.225" layer="94"/>
<wire x1="101.4471" y1="35.414" x2="101.4471" y2="35.189" width="0.225" layer="94"/>
<wire x1="101.4471" y1="35.189" x2="101.4471" y2="34.964" width="0.225" layer="94"/>
<wire x1="101.4471" y1="34.964" x2="101.4471" y2="34.739" width="0.225" layer="94"/>
<wire x1="101.4471" y1="34.739" x2="101.4471" y2="34.514" width="0.225" layer="94"/>
<wire x1="101.4471" y1="34.514" x2="101.4471" y2="34.289" width="0.225" layer="94"/>
<wire x1="101.4471" y1="34.289" x2="101.4471" y2="34.064" width="0.225" layer="94"/>
<wire x1="101.4471" y1="34.064" x2="101.4471" y2="33.839" width="0.225" layer="94"/>
<wire x1="101.4471" y1="33.839" x2="101.4471" y2="33.614" width="0.225" layer="94"/>
<wire x1="101.4471" y1="33.614" x2="101.4471" y2="33.389" width="0.225" layer="94"/>
<wire x1="101.4471" y1="33.389" x2="101.4471" y2="33.164" width="0.225" layer="94"/>
<wire x1="101.4471" y1="33.164" x2="101.4471" y2="32.939" width="0.225" layer="94"/>
<wire x1="101.4471" y1="32.939" x2="101.4471" y2="32.714" width="0.225" layer="94"/>
<wire x1="101.4471" y1="32.714" x2="101.4471" y2="32.489" width="0.225" layer="94"/>
<wire x1="101.4471" y1="32.489" x2="101.4471" y2="32.264" width="0.225" layer="94"/>
<wire x1="101.4471" y1="32.264" x2="101.4471" y2="32.039" width="0.225" layer="94"/>
<wire x1="101.4471" y1="32.039" x2="101.4471" y2="31.814" width="0.225" layer="94"/>
<wire x1="101.4471" y1="31.814" x2="101.4471" y2="31.589" width="0.225" layer="94"/>
<wire x1="101.4471" y1="31.589" x2="101.4471" y2="31.364" width="0.225" layer="94"/>
<wire x1="101.4471" y1="31.364" x2="101.4471" y2="31.139" width="0.225" layer="94"/>
<wire x1="101.4471" y1="31.139" x2="101.4471" y2="30.914" width="0.225" layer="94"/>
<wire x1="101.4471" y1="30.914" x2="101.4471" y2="30.689" width="0.225" layer="94"/>
<wire x1="101.4471" y1="30.689" x2="101.4471" y2="30.464" width="0.225" layer="94"/>
<wire x1="101.4471" y1="30.464" x2="101.4471" y2="30.239" width="0.225" layer="94"/>
<wire x1="101.4471" y1="30.239" x2="101.4471" y2="30.014" width="0.225" layer="94"/>
<wire x1="101.4471" y1="30.014" x2="101.4471" y2="29.789" width="0.225" layer="94"/>
<wire x1="101.4471" y1="29.789" x2="101.4471" y2="29.564" width="0.225" layer="94"/>
<wire x1="101.4471" y1="29.564" x2="101.4471" y2="29.339" width="0.225" layer="94"/>
<wire x1="101.4471" y1="29.339" x2="101.4471" y2="29.114" width="0.225" layer="94"/>
<wire x1="101.4471" y1="29.114" x2="101.4471" y2="28.889" width="0.225" layer="94"/>
<wire x1="101.4471" y1="28.889" x2="101.4471" y2="28.664" width="0.225" layer="94"/>
<wire x1="101.4471" y1="28.664" x2="101.4471" y2="28.439" width="0.225" layer="94"/>
<wire x1="101.4471" y1="28.439" x2="101.4471" y2="28.214" width="0.225" layer="94"/>
<wire x1="101.4471" y1="28.214" x2="101.4471" y2="27.989" width="0.225" layer="94"/>
<wire x1="101.4471" y1="27.989" x2="101.4471" y2="27.764" width="0.225" layer="94"/>
<wire x1="101.4471" y1="27.764" x2="101.4471" y2="27.539" width="0.225" layer="94"/>
<wire x1="101.4471" y1="27.539" x2="101.4471" y2="27.314" width="0.225" layer="94"/>
<wire x1="101.4471" y1="27.314" x2="101.4471" y2="27.089" width="0.225" layer="94"/>
<wire x1="101.4471" y1="27.089" x2="101.4471" y2="26.864" width="0.225" layer="94"/>
<wire x1="101.4471" y1="26.864" x2="101.4471" y2="26.639" width="0.225" layer="94"/>
<wire x1="101.4471" y1="26.639" x2="101.4471" y2="26.414" width="0.225" layer="94"/>
<wire x1="101.4471" y1="26.414" x2="101.4471" y2="26.189" width="0.225" layer="94"/>
<wire x1="101.4471" y1="26.189" x2="101.4471" y2="25.964" width="0.225" layer="94"/>
<wire x1="101.4471" y1="25.964" x2="101.4471" y2="25.739" width="0.225" layer="94"/>
<wire x1="101.4471" y1="25.739" x2="101.4471" y2="25.514" width="0.225" layer="94"/>
<wire x1="101.4471" y1="25.514" x2="101.4471" y2="25.289" width="0.225" layer="94"/>
<wire x1="101.4471" y1="25.289" x2="101.4471" y2="25.064" width="0.225" layer="94"/>
<wire x1="101.4471" y1="25.064" x2="101.4471" y2="24.839" width="0.225" layer="94"/>
<wire x1="101.4471" y1="24.839" x2="101.4471" y2="24.614" width="0.225" layer="94"/>
<wire x1="101.4471" y1="24.614" x2="101.4471" y2="24.389" width="0.225" layer="94"/>
<wire x1="101.4471" y1="24.389" x2="101.4471" y2="24.164" width="0.225" layer="94"/>
<wire x1="101.4471" y1="24.164" x2="101.4471" y2="23.939" width="0.225" layer="94"/>
<wire x1="101.4471" y1="23.939" x2="101.4471" y2="23.714" width="0.225" layer="94"/>
<wire x1="101.4471" y1="23.714" x2="101.4471" y2="23.489" width="0.225" layer="94"/>
<wire x1="101.4471" y1="23.489" x2="101.4471" y2="23.264" width="0.225" layer="94"/>
<wire x1="101.4471" y1="23.264" x2="101.4471" y2="23.09231875" width="0.225" layer="94"/>
<wire x1="101.4471" y1="23.09231875" x2="64.9221" y2="23.09231875" width="0.225" layer="94"/>
<wire x1="64.9221" y1="23.09231875" x2="64.9221" y2="23.264" width="0.225" layer="94"/>
<wire x1="64.9221" y1="23.264" x2="64.9221" y2="23.489" width="0.225" layer="94"/>
<wire x1="64.9221" y1="23.489" x2="64.9221" y2="23.714" width="0.225" layer="94"/>
<wire x1="64.9221" y1="23.714" x2="64.9221" y2="23.939" width="0.225" layer="94"/>
<wire x1="64.9221" y1="23.939" x2="64.9221" y2="24.164" width="0.225" layer="94"/>
<wire x1="64.9221" y1="24.164" x2="64.9221" y2="24.389" width="0.225" layer="94"/>
<wire x1="64.9221" y1="24.389" x2="64.9221" y2="24.614" width="0.225" layer="94"/>
<wire x1="64.9221" y1="24.614" x2="64.9221" y2="24.839" width="0.225" layer="94"/>
<wire x1="64.9221" y1="24.839" x2="64.9221" y2="25.064" width="0.225" layer="94"/>
<wire x1="64.9221" y1="25.064" x2="64.9221" y2="25.289" width="0.225" layer="94"/>
<wire x1="64.9221" y1="25.289" x2="64.9221" y2="25.514" width="0.225" layer="94"/>
<wire x1="64.9221" y1="25.514" x2="64.9221" y2="25.739" width="0.225" layer="94"/>
<wire x1="64.9221" y1="25.739" x2="64.9221" y2="25.964" width="0.225" layer="94"/>
<wire x1="64.9221" y1="25.964" x2="64.9221" y2="26.189" width="0.225" layer="94"/>
<wire x1="64.9221" y1="26.189" x2="64.9221" y2="26.414" width="0.225" layer="94"/>
<wire x1="64.9221" y1="26.414" x2="64.9221" y2="26.639" width="0.225" layer="94"/>
<wire x1="64.9221" y1="26.639" x2="64.9221" y2="26.864" width="0.225" layer="94"/>
<wire x1="64.9221" y1="26.864" x2="64.9221" y2="27.089" width="0.225" layer="94"/>
<wire x1="64.9221" y1="27.089" x2="64.9221" y2="27.314" width="0.225" layer="94"/>
<wire x1="64.9221" y1="27.314" x2="64.9221" y2="27.539" width="0.225" layer="94"/>
<wire x1="64.9221" y1="27.539" x2="64.9221" y2="27.764" width="0.225" layer="94"/>
<wire x1="64.9221" y1="27.764" x2="64.9221" y2="27.989" width="0.225" layer="94"/>
<wire x1="64.9221" y1="27.989" x2="64.9221" y2="28.214" width="0.225" layer="94"/>
<wire x1="64.9221" y1="28.214" x2="64.9221" y2="28.439" width="0.225" layer="94"/>
<wire x1="64.9221" y1="28.439" x2="64.9221" y2="28.664" width="0.225" layer="94"/>
<wire x1="64.9221" y1="28.664" x2="64.9221" y2="28.889" width="0.225" layer="94"/>
<wire x1="64.9221" y1="28.889" x2="64.9221" y2="29.114" width="0.225" layer="94"/>
<wire x1="64.9221" y1="29.114" x2="64.9221" y2="29.339" width="0.225" layer="94"/>
<wire x1="64.9221" y1="29.339" x2="64.9221" y2="29.564" width="0.225" layer="94"/>
<wire x1="64.9221" y1="29.564" x2="64.9221" y2="29.789" width="0.225" layer="94"/>
<wire x1="64.9221" y1="29.789" x2="64.9221" y2="30.014" width="0.225" layer="94"/>
<wire x1="64.9221" y1="30.014" x2="64.9221" y2="30.239" width="0.225" layer="94"/>
<wire x1="64.9221" y1="30.239" x2="64.9221" y2="30.464" width="0.225" layer="94"/>
<wire x1="64.9221" y1="30.464" x2="64.9221" y2="30.689" width="0.225" layer="94"/>
<wire x1="64.9221" y1="30.689" x2="64.9221" y2="30.914" width="0.225" layer="94"/>
<wire x1="64.9221" y1="30.914" x2="64.9221" y2="31.139" width="0.225" layer="94"/>
<wire x1="64.9221" y1="31.139" x2="64.9221" y2="31.364" width="0.225" layer="94"/>
<wire x1="64.9221" y1="31.364" x2="64.9221" y2="31.589" width="0.225" layer="94"/>
<wire x1="64.9221" y1="31.589" x2="64.9221" y2="31.814" width="0.225" layer="94"/>
<wire x1="64.9221" y1="31.814" x2="64.9221" y2="32.039" width="0.225" layer="94"/>
<wire x1="64.9221" y1="32.039" x2="64.9221" y2="32.264" width="0.225" layer="94"/>
<wire x1="64.9221" y1="32.264" x2="64.9221" y2="32.489" width="0.225" layer="94"/>
<wire x1="64.9221" y1="32.489" x2="64.9221" y2="32.714" width="0.225" layer="94"/>
<wire x1="64.9221" y1="32.714" x2="64.9221" y2="32.939" width="0.225" layer="94"/>
<wire x1="64.9221" y1="32.939" x2="64.9221" y2="33.164" width="0.225" layer="94"/>
<wire x1="64.9221" y1="33.164" x2="64.9221" y2="33.389" width="0.225" layer="94"/>
<wire x1="64.9221" y1="33.389" x2="64.9221" y2="33.614" width="0.225" layer="94"/>
<wire x1="64.9221" y1="33.614" x2="64.9221" y2="33.839" width="0.225" layer="94"/>
<wire x1="64.9221" y1="33.839" x2="64.9221" y2="34.064" width="0.225" layer="94"/>
<wire x1="64.9221" y1="34.064" x2="64.9221" y2="34.289" width="0.225" layer="94"/>
<wire x1="64.9221" y1="34.289" x2="64.9221" y2="34.514" width="0.225" layer="94"/>
<wire x1="64.9221" y1="34.514" x2="64.9221" y2="34.739" width="0.225" layer="94"/>
<wire x1="64.9221" y1="34.739" x2="64.9221" y2="34.964" width="0.225" layer="94"/>
<wire x1="64.9221" y1="34.964" x2="64.9221" y2="35.189" width="0.225" layer="94"/>
<wire x1="64.9221" y1="35.189" x2="64.9221" y2="35.414" width="0.225" layer="94"/>
<wire x1="64.9221" y1="35.414" x2="64.9221" y2="35.639" width="0.225" layer="94"/>
<wire x1="64.9221" y1="35.639" x2="64.9221" y2="35.864" width="0.225" layer="94"/>
<wire x1="64.9221" y1="35.864" x2="64.9221" y2="36.089" width="0.225" layer="94"/>
<wire x1="64.9221" y1="36.089" x2="64.9221" y2="36.314" width="0.225" layer="94"/>
<wire x1="64.9221" y1="36.314" x2="64.9221" y2="36.539" width="0.225" layer="94"/>
<wire x1="64.9221" y1="36.539" x2="64.9221" y2="36.764" width="0.225" layer="94"/>
<wire x1="64.9221" y1="36.764" x2="64.9221" y2="36.989" width="0.225" layer="94"/>
<wire x1="64.9221" y1="36.989" x2="64.9221" y2="37.214" width="0.225" layer="94"/>
<wire x1="64.9221" y1="37.214" x2="64.9221" y2="37.439" width="0.225" layer="94"/>
<wire x1="64.9221" y1="37.439" x2="64.9221" y2="37.664" width="0.225" layer="94"/>
<wire x1="64.9221" y1="37.664" x2="64.9221" y2="37.86731875" width="0.225" layer="94"/>
<wire x1="64.9221" y1="37.664" x2="101.4471" y2="37.664" width="0.225" layer="94"/>
<wire x1="64.9221" y1="37.439" x2="101.4471" y2="37.439" width="0.225" layer="94"/>
<wire x1="64.9221" y1="37.214" x2="101.4471" y2="37.214" width="0.225" layer="94"/>
<wire x1="64.9221" y1="36.989" x2="101.4471" y2="36.989" width="0.225" layer="94"/>
<wire x1="64.9221" y1="36.764" x2="101.4471" y2="36.764" width="0.225" layer="94"/>
<wire x1="64.9221" y1="36.539" x2="101.4471" y2="36.539" width="0.225" layer="94"/>
<wire x1="64.9221" y1="36.314" x2="101.4471" y2="36.314" width="0.225" layer="94"/>
<wire x1="64.9221" y1="36.089" x2="101.4471" y2="36.089" width="0.225" layer="94"/>
<wire x1="64.9221" y1="35.864" x2="101.4471" y2="35.864" width="0.225" layer="94"/>
<wire x1="64.9221" y1="35.639" x2="101.4471" y2="35.639" width="0.225" layer="94"/>
<wire x1="64.9221" y1="35.414" x2="101.4471" y2="35.414" width="0.225" layer="94"/>
<wire x1="64.9221" y1="35.189" x2="101.4471" y2="35.189" width="0.225" layer="94"/>
<wire x1="64.9221" y1="34.964" x2="69.81928125" y2="34.964" width="0.225" layer="94"/>
<wire x1="76.07733125" y1="34.964" x2="101.4471" y2="34.964" width="0.225" layer="94"/>
<wire x1="69.89371875" y1="34.964" x2="76.0029" y2="34.964" width="0.225" layer="94"/>
<wire x1="64.9221" y1="34.739" x2="69.570590625" y2="34.739" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="34.739" x2="101.4471" y2="34.739" width="0.225" layer="94"/>
<wire x1="70.097190625" y1="34.739" x2="75.79943125" y2="34.739" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="34.739" x2="90.66211875" y2="34.739" width="0.225" layer="94"/>
<wire x1="76.326109375" y1="34.739" x2="78.286090625" y2="34.739" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="34.739" x2="87.068640625" y2="34.739" width="0.225" layer="94"/>
<wire x1="64.9221" y1="34.514" x2="69.3219" y2="34.514" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="34.514" x2="101.4471" y2="34.514" width="0.225" layer="94"/>
<wire x1="70.30066875" y1="34.514" x2="75.595959375" y2="34.514" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="34.514" x2="90.66211875" y2="34.514" width="0.225" layer="94"/>
<wire x1="76.574890625" y1="34.514" x2="78.286090625" y2="34.514" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="34.514" x2="87.068640625" y2="34.514" width="0.225" layer="94"/>
<wire x1="64.9221" y1="34.289" x2="69.073209375" y2="34.289" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="34.289" x2="101.4471" y2="34.289" width="0.225" layer="94"/>
<wire x1="70.504140625" y1="34.289" x2="75.3925" y2="34.289" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="34.289" x2="90.66211875" y2="34.289" width="0.225" layer="94"/>
<wire x1="76.82366875" y1="34.289" x2="78.286090625" y2="34.289" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="34.289" x2="87.068640625" y2="34.289" width="0.225" layer="94"/>
<wire x1="64.9221" y1="34.064" x2="68.82451875" y2="34.064" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="34.064" x2="101.4471" y2="34.064" width="0.225" layer="94"/>
<wire x1="70.70761875" y1="34.064" x2="75.18903125" y2="34.064" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="34.064" x2="90.66211875" y2="34.064" width="0.225" layer="94"/>
<wire x1="77.07245" y1="34.064" x2="78.286090625" y2="34.064" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="34.064" x2="87.068640625" y2="34.064" width="0.225" layer="94"/>
<wire x1="64.9221" y1="33.839" x2="68.78723125" y2="33.839" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="33.839" x2="101.4471" y2="33.839" width="0.225" layer="94"/>
<wire x1="70.911090625" y1="33.839" x2="74.985559375" y2="33.839" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="33.839" x2="90.66211875" y2="33.839" width="0.225" layer="94"/>
<wire x1="77.1094" y1="33.839" x2="78.286090625" y2="33.839" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="33.839" x2="87.068640625" y2="33.839" width="0.225" layer="94"/>
<wire x1="64.9221" y1="33.614" x2="68.990709375" y2="33.614" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="33.614" x2="101.4471" y2="33.614" width="0.225" layer="94"/>
<wire x1="71.114559375" y1="33.614" x2="74.782090625" y2="33.614" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="33.614" x2="90.66211875" y2="33.614" width="0.225" layer="94"/>
<wire x1="76.905909375" y1="33.614" x2="78.286090625" y2="33.614" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="33.614" x2="87.068640625" y2="33.614" width="0.225" layer="94"/>
<wire x1="64.9221" y1="33.389" x2="69.1942" y2="33.389" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="33.389" x2="101.4471" y2="33.389" width="0.225" layer="94"/>
<wire x1="71.318040625" y1="33.389" x2="74.57861875" y2="33.389" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="33.389" x2="90.66211875" y2="33.389" width="0.225" layer="94"/>
<wire x1="76.70243125" y1="33.389" x2="78.286090625" y2="33.389" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="33.389" x2="87.068640625" y2="33.389" width="0.225" layer="94"/>
<wire x1="64.9221" y1="33.164" x2="69.397690625" y2="33.164" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="33.164" x2="101.4471" y2="33.164" width="0.225" layer="94"/>
<wire x1="71.521509375" y1="33.164" x2="74.37515" y2="33.164" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="33.164" x2="93.287040625" y2="33.164" width="0.225" layer="94"/>
<wire x1="76.49895" y1="33.164" x2="87.068640625" y2="33.164" width="0.225" layer="94"/>
<wire x1="64.9221" y1="32.939" x2="69.60116875" y2="32.939" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="32.939" x2="101.4471" y2="32.939" width="0.225" layer="94"/>
<wire x1="71.724990625" y1="32.939" x2="74.17168125" y2="32.939" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="32.939" x2="93.287040625" y2="32.939" width="0.225" layer="94"/>
<wire x1="76.29546875" y1="32.939" x2="87.068640625" y2="32.939" width="0.225" layer="94"/>
<wire x1="64.9221" y1="32.714" x2="69.804659375" y2="32.714" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="32.714" x2="101.4471" y2="32.714" width="0.225" layer="94"/>
<wire x1="71.928459375" y1="32.714" x2="73.968209375" y2="32.714" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="32.714" x2="93.287040625" y2="32.714" width="0.225" layer="94"/>
<wire x1="76.091990625" y1="32.714" x2="78.286240625" y2="32.714" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="32.714" x2="87.068640625" y2="32.714" width="0.225" layer="94"/>
<wire x1="64.9221" y1="32.489" x2="70.008140625" y2="32.489" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="32.489" x2="101.4471" y2="32.489" width="0.225" layer="94"/>
<wire x1="72.131940625" y1="32.489" x2="73.764740625" y2="32.489" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="32.489" x2="93.287040625" y2="32.489" width="0.225" layer="94"/>
<wire x1="75.8885" y1="32.489" x2="78.286240625" y2="32.489" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="32.489" x2="87.068640625" y2="32.489" width="0.225" layer="94"/>
<wire x1="64.9221" y1="32.264" x2="70.21163125" y2="32.264" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="32.264" x2="101.4471" y2="32.264" width="0.225" layer="94"/>
<wire x1="72.335409375" y1="32.264" x2="73.56126875" y2="32.264" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="32.264" x2="93.287040625" y2="32.264" width="0.225" layer="94"/>
<wire x1="75.68501875" y1="32.264" x2="78.286240625" y2="32.264" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="32.264" x2="87.068640625" y2="32.264" width="0.225" layer="94"/>
<wire x1="64.9221" y1="32.039" x2="70.415109375" y2="32.039" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="32.039" x2="101.4471" y2="32.039" width="0.225" layer="94"/>
<wire x1="72.53888125" y1="32.039" x2="73.3578" y2="32.039" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="32.039" x2="93.287040625" y2="32.039" width="0.225" layer="94"/>
<wire x1="75.481540625" y1="32.039" x2="78.286240625" y2="32.039" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="32.039" x2="87.068640625" y2="32.039" width="0.225" layer="94"/>
<wire x1="64.9221" y1="31.814" x2="70.6186" y2="31.814" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="31.814" x2="101.4471" y2="31.814" width="0.225" layer="94"/>
<wire x1="72.742359375" y1="31.814" x2="73.15433125" y2="31.814" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="31.814" x2="93.287040625" y2="31.814" width="0.225" layer="94"/>
<wire x1="75.278059375" y1="31.814" x2="78.286240625" y2="31.814" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="31.814" x2="87.068640625" y2="31.814" width="0.225" layer="94"/>
<wire x1="64.9221" y1="31.589" x2="70.822090625" y2="31.589" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="31.589" x2="101.4471" y2="31.589" width="0.225" layer="94"/>
<wire x1="72.94583125" y1="31.589" x2="72.950859375" y2="31.589" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="31.589" x2="93.287040625" y2="31.589" width="0.225" layer="94"/>
<wire x1="75.07458125" y1="31.589" x2="78.286240625" y2="31.589" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="31.589" x2="87.068640625" y2="31.589" width="0.225" layer="94"/>
<wire x1="64.9221" y1="31.364" x2="71.02556875" y2="31.364" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="31.364" x2="101.4471" y2="31.364" width="0.225" layer="94"/>
<wire x1="74.871090625" y1="31.364" x2="87.068640625" y2="31.364" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="31.364" x2="93.287040625" y2="31.364" width="0.225" layer="94"/>
<wire x1="64.9221" y1="31.139" x2="71.229059375" y2="31.139" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="31.139" x2="101.4471" y2="31.139" width="0.225" layer="94"/>
<wire x1="74.667609375" y1="31.139" x2="87.068640625" y2="31.139" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="31.139" x2="93.287040625" y2="31.139" width="0.225" layer="94"/>
<wire x1="64.9221" y1="30.914" x2="71.432540625" y2="30.914" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="30.914" x2="101.4471" y2="30.914" width="0.225" layer="94"/>
<wire x1="74.46413125" y1="30.914" x2="78.286090625" y2="30.914" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="30.914" x2="93.287040625" y2="30.914" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="30.914" x2="87.068640625" y2="30.914" width="0.225" layer="94"/>
<wire x1="64.9221" y1="30.689" x2="71.63603125" y2="30.689" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="30.689" x2="101.4471" y2="30.689" width="0.225" layer="94"/>
<wire x1="74.26065" y1="30.689" x2="78.286090625" y2="30.689" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="30.689" x2="93.287040625" y2="30.689" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="30.689" x2="87.068640625" y2="30.689" width="0.225" layer="94"/>
<wire x1="64.9221" y1="30.464" x2="71.839509375" y2="30.464" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="30.464" x2="101.4471" y2="30.464" width="0.225" layer="94"/>
<wire x1="74.057159375" y1="30.464" x2="78.286090625" y2="30.464" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="30.464" x2="93.287040625" y2="30.464" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="30.464" x2="87.068640625" y2="30.464" width="0.225" layer="94"/>
<wire x1="64.9221" y1="30.239" x2="72.043" y2="30.239" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="30.239" x2="101.4471" y2="30.239" width="0.225" layer="94"/>
<wire x1="73.85368125" y1="30.239" x2="78.286090625" y2="30.239" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="30.239" x2="93.287040625" y2="30.239" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="30.239" x2="87.068640625" y2="30.239" width="0.225" layer="94"/>
<wire x1="64.9221" y1="30.014" x2="72.246490625" y2="30.014" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="30.014" x2="101.4471" y2="30.014" width="0.225" layer="94"/>
<wire x1="73.6502" y1="30.014" x2="78.286090625" y2="30.014" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="30.014" x2="93.287040625" y2="30.014" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="30.014" x2="87.068640625" y2="30.014" width="0.225" layer="94"/>
<wire x1="64.9221" y1="29.789" x2="72.44996875" y2="29.789" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="29.789" x2="101.4471" y2="29.789" width="0.225" layer="94"/>
<wire x1="73.44671875" y1="29.789" x2="78.286090625" y2="29.789" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="29.789" x2="93.287040625" y2="29.789" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="29.789" x2="87.068640625" y2="29.789" width="0.225" layer="94"/>
<wire x1="64.9221" y1="29.564" x2="72.653459375" y2="29.564" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="29.564" x2="101.4471" y2="29.564" width="0.225" layer="94"/>
<wire x1="73.243240625" y1="29.564" x2="78.286090625" y2="29.564" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="29.564" x2="93.287040625" y2="29.564" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="29.564" x2="87.068640625" y2="29.564" width="0.225" layer="94"/>
<wire x1="64.9221" y1="29.339" x2="72.856940625" y2="29.339" width="0.225" layer="94"/>
<wire x1="73.03975" y1="29.339" x2="101.4471" y2="29.339" width="0.225" layer="94"/>
<wire x1="64.9221" y1="29.114" x2="101.4471" y2="29.114" width="0.225" layer="94"/>
<wire x1="64.9221" y1="28.889" x2="101.4471" y2="28.889" width="0.225" layer="94"/>
<wire x1="64.9221" y1="28.664" x2="101.4471" y2="28.664" width="0.225" layer="94"/>
<wire x1="64.9221" y1="28.439" x2="101.4471" y2="28.439" width="0.225" layer="94"/>
<wire x1="64.9221" y1="28.214" x2="101.4471" y2="28.214" width="0.225" layer="94"/>
<wire x1="64.9221" y1="27.989" x2="101.4471" y2="27.989" width="0.225" layer="94"/>
<wire x1="64.9221" y1="27.764" x2="101.4471" y2="27.764" width="0.225" layer="94"/>
<wire x1="64.9221" y1="27.539" x2="75.49116875" y2="27.539" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="27.539" x2="101.4471" y2="27.539" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="27.539" x2="88.41505" y2="27.539" width="0.225" layer="94"/>
<wire x1="64.9221" y1="27.314" x2="75.49116875" y2="27.314" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="27.314" x2="101.4471" y2="27.314" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="27.314" x2="80.786140625" y2="27.314" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="27.314" x2="88.41505" y2="27.314" width="0.225" layer="94"/>
<wire x1="64.9221" y1="27.089" x2="75.49116875" y2="27.089" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="27.089" x2="101.4471" y2="27.089" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="27.089" x2="80.786140625" y2="27.089" width="0.225" layer="94"/>
<wire x1="64.9221" y1="26.864" x2="73.579059375" y2="26.864" width="0.225" layer="94"/>
<wire x1="92.70988125" y1="26.864" x2="101.4471" y2="26.864" width="0.225" layer="94"/>
<wire x1="74.56438125" y1="26.864" x2="75.49116875" y2="26.864" width="0.225" layer="94"/>
<wire x1="90.74325" y1="26.864" x2="91.69128125" y2="26.864" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="26.864" x2="76.864040625" y2="26.864" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="26.864" x2="89.78075" y2="26.864" width="0.225" layer="94"/>
<wire x1="77.84936875" y1="26.864" x2="78.869559375" y2="26.864" width="0.225" layer="94"/>
<wire x1="87.470340625" y1="26.864" x2="88.41505" y2="26.864" width="0.225" layer="94"/>
<wire x1="79.832359375" y1="26.864" x2="80.627059375" y2="26.864" width="0.225" layer="94"/>
<wire x1="85.42721875" y1="26.864" x2="86.39005" y2="26.864" width="0.225" layer="94"/>
<wire x1="81.7013" y1="26.864" x2="82.508140625" y2="26.864" width="0.225" layer="94"/>
<wire x1="83.522" y1="26.864" x2="84.441909375" y2="26.864" width="0.225" layer="94"/>
<wire x1="64.9221" y1="26.639" x2="73.48298125" y2="26.639" width="0.225" layer="94"/>
<wire x1="92.75785" y1="26.639" x2="101.4471" y2="26.639" width="0.225" layer="94"/>
<wire x1="73.958459375" y1="26.639" x2="74.19638125" y2="26.639" width="0.225" layer="94"/>
<wire x1="92.06346875" y1="26.639" x2="92.334359375" y2="26.639" width="0.225" layer="94"/>
<wire x1="74.659159375" y1="26.639" x2="75.49116875" y2="26.639" width="0.225" layer="94"/>
<wire x1="90.81963125" y1="26.639" x2="91.626409375" y2="26.639" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="26.639" x2="76.76796875" y2="26.639" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="26.639" x2="89.685840625" y2="26.639" width="0.225" layer="94"/>
<wire x1="77.24345" y1="26.639" x2="77.48136875" y2="26.639" width="0.225" layer="94"/>
<wire x1="87.55065" y1="26.639" x2="88.41505" y2="26.639" width="0.225" layer="94"/>
<wire x1="77.94415" y1="26.639" x2="78.77521875" y2="26.639" width="0.225" layer="94"/>
<wire x1="86.913890625" y1="26.639" x2="87.09276875" y2="26.639" width="0.225" layer="94"/>
<wire x1="79.90875" y1="26.639" x2="80.627059375" y2="26.639" width="0.225" layer="94"/>
<wire x1="85.522" y1="26.639" x2="86.39005" y2="26.639" width="0.225" layer="94"/>
<wire x1="81.7013" y1="26.639" x2="82.508140625" y2="26.639" width="0.225" layer="94"/>
<wire x1="83.58053125" y1="26.639" x2="84.345909375" y2="26.639" width="0.225" layer="94"/>
<wire x1="82.99755" y1="26.639" x2="83.160159375" y2="26.639" width="0.225" layer="94"/>
<wire x1="64.9221" y1="26.414" x2="73.45896875" y2="26.414" width="0.225" layer="94"/>
<wire x1="92.759190625" y1="26.414" x2="101.4471" y2="26.414" width="0.225" layer="94"/>
<wire x1="74.6829" y1="26.414" x2="75.49116875" y2="26.414" width="0.225" layer="94"/>
<wire x1="92.0661" y1="26.414" x2="92.34215" y2="26.414" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="26.414" x2="76.74395" y2="26.414" width="0.225" layer="94"/>
<wire x1="90.830209375" y1="26.414" x2="91.62503125" y2="26.414" width="0.225" layer="94"/>
<wire x1="77.967890625" y1="26.414" x2="78.75028125" y2="26.414" width="0.225" layer="94"/>
<wire x1="90.0934" y1="26.414" x2="90.41016875" y2="26.414" width="0.225" layer="94"/>
<wire x1="79.18205" y1="26.414" x2="79.49928125" y2="26.414" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="26.414" x2="89.661509375" y2="26.414" width="0.225" layer="94"/>
<wire x1="79.91931875" y1="26.414" x2="80.786140625" y2="26.414" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="26.414" x2="88.41505" y2="26.414" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="26.414" x2="82.508140625" y2="26.414" width="0.225" layer="94"/>
<wire x1="86.817440625" y1="26.414" x2="87.14536875" y2="26.414" width="0.225" layer="94"/>
<wire x1="82.92926875" y1="26.414" x2="83.18028125" y2="26.414" width="0.225" layer="94"/>
<wire x1="85.545740625" y1="26.414" x2="86.39005" y2="26.414" width="0.225" layer="94"/>
<wire x1="83.585309375" y1="26.414" x2="84.32161875" y2="26.414" width="0.225" layer="94"/>
<wire x1="84.754290625" y1="26.414" x2="85.11306875" y2="26.414" width="0.225" layer="94"/>
<wire x1="64.9221" y1="26.189" x2="73.457240625" y2="26.189" width="0.225" layer="94"/>
<wire x1="92.708490625" y1="26.189" x2="101.4471" y2="26.189" width="0.225" layer="94"/>
<wire x1="74.684409375" y1="26.189" x2="75.49116875" y2="26.189" width="0.225" layer="94"/>
<wire x1="90.092" y1="26.189" x2="91.66831875" y2="26.189" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="26.189" x2="76.74221875" y2="26.189" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="26.189" x2="89.66001875" y2="26.189" width="0.225" layer="94"/>
<wire x1="77.969390625" y1="26.189" x2="78.74913125" y2="26.189" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="26.189" x2="88.41505" y2="26.189" width="0.225" layer="94"/>
<wire x1="79.181109375" y1="26.189" x2="80.786140625" y2="26.189" width="0.225" layer="94"/>
<wire x1="86.810090625" y1="26.189" x2="87.1462" y2="26.189" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="26.189" x2="82.508140625" y2="26.189" width="0.225" layer="94"/>
<wire x1="85.54725" y1="26.189" x2="86.39005" y2="26.189" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="26.189" x2="84.32008125" y2="26.189" width="0.225" layer="94"/>
<wire x1="84.752059375" y1="26.189" x2="85.115190625" y2="26.189" width="0.225" layer="94"/>
<wire x1="64.9221" y1="25.964" x2="73.457090625" y2="25.964" width="0.225" layer="94"/>
<wire x1="92.77591875" y1="25.964" x2="101.4471" y2="25.964" width="0.225" layer="94"/>
<wire x1="73.88948125" y1="25.964" x2="74.27638125" y2="25.964" width="0.225" layer="94"/>
<wire x1="92.024059375" y1="25.964" x2="92.27126875" y2="25.964" width="0.225" layer="94"/>
<wire x1="74.681409375" y1="25.964" x2="75.49116875" y2="25.964" width="0.225" layer="94"/>
<wire x1="90.86316875" y1="25.964" x2="91.60701875" y2="25.964" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="25.964" x2="76.74208125" y2="25.964" width="0.225" layer="94"/>
<wire x1="90.092309375" y1="25.964" x2="90.43711875" y2="25.964" width="0.225" layer="94"/>
<wire x1="77.17446875" y1="25.964" x2="77.561359375" y2="25.964" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="25.964" x2="89.660090625" y2="25.964" width="0.225" layer="94"/>
<wire x1="77.966390625" y1="25.964" x2="78.749" y2="25.964" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="25.964" x2="88.41505" y2="25.964" width="0.225" layer="94"/>
<wire x1="79.181340625" y1="25.964" x2="79.52623125" y2="25.964" width="0.225" layer="94"/>
<wire x1="86.810090625" y1="25.964" x2="87.1462" y2="25.964" width="0.225" layer="94"/>
<wire x1="79.95228125" y1="25.964" x2="80.786140625" y2="25.964" width="0.225" layer="94"/>
<wire x1="85.54718125" y1="25.964" x2="86.39005" y2="25.964" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="25.964" x2="81.341240625" y2="25.964" width="0.225" layer="94"/>
<wire x1="84.752340625" y1="25.964" x2="85.114909375" y2="25.964" width="0.225" layer="94"/>
<wire x1="81.74326875" y1="25.964" x2="82.508140625" y2="25.964" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="25.964" x2="84.32015" y2="25.964" width="0.225" layer="94"/>
<wire x1="64.9221" y1="25.739" x2="73.47301875" y2="25.739" width="0.225" layer="94"/>
<wire x1="92.777209375" y1="25.739" x2="101.4471" y2="25.739" width="0.225" layer="94"/>
<wire x1="73.90876875" y1="25.739" x2="74.2678" y2="25.739" width="0.225" layer="94"/>
<wire x1="92.025140625" y1="25.739" x2="92.34813125" y2="25.739" width="0.225" layer="94"/>
<wire x1="74.67768125" y1="25.739" x2="75.49116875" y2="25.739" width="0.225" layer="94"/>
<wire x1="90.852559375" y1="25.739" x2="91.60701875" y2="25.739" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="25.739" x2="76.758" y2="25.739" width="0.225" layer="94"/>
<wire x1="90.11266875" y1="25.739" x2="90.420940625" y2="25.739" width="0.225" layer="94"/>
<wire x1="77.193759375" y1="25.739" x2="77.55278125" y2="25.739" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="25.739" x2="89.675959375" y2="25.739" width="0.225" layer="94"/>
<wire x1="77.962659375" y1="25.739" x2="78.76481875" y2="25.739" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="25.739" x2="88.41505" y2="25.739" width="0.225" layer="94"/>
<wire x1="79.202409375" y1="25.739" x2="79.510359375" y2="25.739" width="0.225" layer="94"/>
<wire x1="86.810090625" y1="25.739" x2="87.1462" y2="25.739" width="0.225" layer="94"/>
<wire x1="79.94166875" y1="25.739" x2="80.78728125" y2="25.739" width="0.225" layer="94"/>
<wire x1="85.531009375" y1="25.739" x2="86.39005" y2="25.739" width="0.225" layer="94"/>
<wire x1="81.209909375" y1="25.739" x2="81.340009375" y2="25.739" width="0.225" layer="94"/>
<wire x1="84.77363125" y1="25.739" x2="85.092790625" y2="25.739" width="0.225" layer="94"/>
<wire x1="81.740890625" y1="25.739" x2="82.508140625" y2="25.739" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="25.739" x2="84.336459375" y2="25.739" width="0.225" layer="94"/>
<wire x1="64.9221" y1="25.514" x2="73.542859375" y2="25.514" width="0.225" layer="94"/>
<wire x1="92.74113125" y1="25.514" x2="101.4471" y2="25.514" width="0.225" layer="94"/>
<wire x1="74.62216875" y1="25.514" x2="75.49116875" y2="25.514" width="0.225" layer="94"/>
<wire x1="90.78793125" y1="25.514" x2="91.6325" y2="25.514" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="25.514" x2="76.82785" y2="25.514" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="25.514" x2="89.74463125" y2="25.514" width="0.225" layer="94"/>
<wire x1="77.90715" y1="25.514" x2="78.834309375" y2="25.514" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="25.514" x2="88.41505" y2="25.514" width="0.225" layer="94"/>
<wire x1="79.877040625" y1="25.514" x2="80.82388125" y2="25.514" width="0.225" layer="94"/>
<wire x1="86.810090625" y1="25.514" x2="87.1462" y2="25.514" width="0.225" layer="94"/>
<wire x1="81.697240625" y1="25.514" x2="82.508140625" y2="25.514" width="0.225" layer="94"/>
<wire x1="85.46248125" y1="25.514" x2="86.39005" y2="25.514" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="25.514" x2="84.40543125" y2="25.514" width="0.225" layer="94"/>
<wire x1="64.9221" y1="25.289" x2="73.828090625" y2="25.289" width="0.225" layer="94"/>
<wire x1="92.48735" y1="25.289" x2="101.4471" y2="25.289" width="0.225" layer="94"/>
<wire x1="74.3331" y1="25.289" x2="75.49116875" y2="25.289" width="0.225" layer="94"/>
<wire x1="90.51153125" y1="25.289" x2="91.85098125" y2="25.289" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="25.289" x2="77.11308125" y2="25.289" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="25.289" x2="90.02831875" y2="25.289" width="0.225" layer="94"/>
<wire x1="77.618090625" y1="25.289" x2="79.119840625" y2="25.289" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="25.289" x2="88.41505" y2="25.289" width="0.225" layer="94"/>
<wire x1="79.60065" y1="25.289" x2="81.04181875" y2="25.289" width="0.225" layer="94"/>
<wire x1="86.810090625" y1="25.289" x2="87.1462" y2="25.289" width="0.225" layer="94"/>
<wire x1="81.460959375" y1="25.289" x2="82.508140625" y2="25.289" width="0.225" layer="94"/>
<wire x1="85.1789" y1="25.289" x2="86.39005" y2="25.289" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="25.289" x2="84.689659375" y2="25.289" width="0.225" layer="94"/>
<wire x1="64.9221" y1="25.064" x2="101.4471" y2="25.064" width="0.225" layer="94"/>
<wire x1="64.9221" y1="24.839" x2="101.4471" y2="24.839" width="0.225" layer="94"/>
<wire x1="64.9221" y1="24.614" x2="101.4471" y2="24.614" width="0.225" layer="94"/>
<wire x1="64.9221" y1="24.389" x2="101.4471" y2="24.389" width="0.225" layer="94"/>
<wire x1="64.9221" y1="24.164" x2="101.4471" y2="24.164" width="0.225" layer="94"/>
<wire x1="64.9221" y1="23.939" x2="101.4471" y2="23.939" width="0.225" layer="94"/>
<wire x1="64.9221" y1="23.714" x2="101.4471" y2="23.714" width="0.225" layer="94"/>
<wire x1="64.9221" y1="23.489" x2="101.4471" y2="23.489" width="0.225" layer="94"/>
<wire x1="64.9221" y1="23.264" x2="101.4471" y2="23.264" width="0.225" layer="94"/>
<rectangle x1="93.287040625" y1="29.45076875" x2="93.399540625" y2="33.34871875" layer="94"/>
<rectangle x1="94.749840625" y1="29.45076875" x2="94.862340625" y2="33.34871875" layer="94"/>
<rectangle x1="73.776721875" y1="26.310290625" x2="74.376859375" y2="26.422790625" layer="94"/>
<rectangle x1="77.0617" y1="26.310290625" x2="77.66185" y2="26.422790625" layer="94"/>
<rectangle x1="73.77671875" y1="26.00881875" x2="74.685" y2="26.12131875" layer="94"/>
<rectangle x1="77.0617" y1="26.00881875" x2="77.985" y2="26.12131875" layer="94"/>
<rectangle x1="81.09369375" y1="26.91936875" x2="81.7013" y2="27.314" layer="94"/>
<rectangle x1="80.627059375" y1="26.91936875" x2="80.898640625" y2="27.314" layer="94"/>
<rectangle x1="80.627059375" y1="26.319371875" x2="80.898640625" y2="26.739490625" layer="94"/>
<rectangle x1="81.09369375" y1="26.319371875" x2="81.7013" y2="26.739490625" layer="94"/>
<rectangle x1="64.77" y1="37.7825" x2="101.6" y2="38.1" layer="94"/>
<rectangle x1="101.2825" y1="22.86" x2="101.6" y2="38.1" layer="94"/>
<rectangle x1="64.77" y1="22.86" x2="65.0875" y2="38.1" layer="94"/>
<rectangle x1="64.77" y1="22.86" x2="101.6" y2="23.1775" layer="94"/>
<text x="50.8" y="12.7" size="5.08" layer="94" ratio="10" align="bottom-center">&gt;VALUE</text>
<text x="1.27" y="33.02" size="1.9304" layer="94">Vehicle Name:</text>
<text x="41.91" y="30.48" size="5.08" layer="94" align="top-center">&gt;VEHICLE_NUMBER</text>
<text x="41.91" y="33.02" size="2.54" layer="94" align="bottom-center">&gt;VEHICLE_NAME</text>
<text x="1.27" y="21.59" size="1.9304" layer="94" align="top-left">Sheet Name:</text>
<text x="1.27" y="24.13" size="1.9304" layer="94">ABRA:</text>
<text x="10.16" y="24.13" size="1.9304" layer="94">&gt;ABRA_GLOBAL</text>
</symbol>
<symbol name="DOCFIELD-AUTA-ECO-LOGO">
<wire x1="0" y1="0" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="101.6" y1="10.16" x2="87.63" y2="10.16" width="0.1016" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="71.12" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="10.16" width="0.1016" layer="94"/>
<wire x1="101.6" y1="10.16" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="0" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="87.63" y1="10.16" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="87.63" y1="10.16" x2="0" y2="10.16" width="0.1016" layer="94"/>
<wire x1="87.63" y1="5.08" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="101.6" y1="5.08" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="0" y1="10.16" x2="0" y2="22.86" width="0.1016" layer="94"/>
<wire x1="101.6" y1="38.1" x2="64.77" y2="38.1" width="0.1016" layer="94"/>
<wire x1="64.77" y1="38.1" x2="0" y2="38.1" width="0.1016" layer="94"/>
<wire x1="101.6" y1="38.1" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="64.77" y2="22.86" width="0.1016" layer="94"/>
<wire x1="64.77" y1="22.86" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="0" y2="38.1" width="0.1016" layer="94"/>
<wire x1="101.6" y1="22.86" x2="101.6" y2="10.16" width="0.1016" layer="94"/>
<text x="1.27" y="1.27" size="2.54" layer="94">Date:</text>
<text x="15.24" y="1.27" size="2.54" layer="94" font="vector">&gt;LAST_DATE_TIME</text>
<text x="72.39" y="1.27" size="2.54" layer="94">Sheet:</text>
<text x="86.36" y="1.27" size="2.54" layer="94">&gt;SHEET</text>
<text x="88.9" y="6.35" size="2.54" layer="94">REV:</text>
<text x="1.27" y="6.35" size="2.54" layer="94">TITLE:</text>
<text x="1.27" y="30.48" size="1.9304" layer="94" align="top-left">Vehicle Number:</text>
<text x="15.24" y="6.35" size="2.54" layer="94" font="vector">&gt;DRAWING_NAME</text>
<wire x1="64.77" y1="38.1" x2="64.77" y2="22.86" width="0.1016" layer="94"/>
<wire x1="97.487340625" y1="34.81106875" x2="90.66211875" y2="34.81106875" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="34.81106875" x2="90.66211875" y2="34.739" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="34.739" x2="90.66211875" y2="34.514" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="34.514" x2="90.66211875" y2="34.289" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="34.289" x2="90.66211875" y2="34.064" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="34.064" x2="90.66211875" y2="33.839" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="33.839" x2="90.66211875" y2="33.614" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="33.614" x2="90.66211875" y2="33.389" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="33.389" x2="90.66211875" y2="33.23621875" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="33.23621875" x2="93.287040625" y2="33.23621875" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="33.23621875" x2="93.287040625" y2="33.164" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="33.164" x2="93.287040625" y2="32.939" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="32.939" x2="93.287040625" y2="32.714" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="32.714" x2="93.287040625" y2="32.489" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="32.489" x2="93.287040625" y2="32.264" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="32.264" x2="93.287040625" y2="32.039" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="32.039" x2="93.287040625" y2="31.814" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="31.814" x2="93.287040625" y2="31.589" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="31.589" x2="93.287040625" y2="31.364" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="31.364" x2="93.287040625" y2="31.139" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="31.139" x2="93.287040625" y2="30.914" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="30.914" x2="93.287040625" y2="30.689" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="30.689" x2="93.287040625" y2="30.464" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="30.464" x2="93.287040625" y2="30.239" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="30.239" x2="93.287040625" y2="30.014" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="30.014" x2="93.287040625" y2="29.789" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="29.789" x2="93.287040625" y2="29.564" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="29.564" x2="93.287040625" y2="29.45076875" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="29.45076875" x2="94.862340625" y2="29.45076875" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="29.45076875" x2="94.862340625" y2="29.564" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="29.564" x2="94.862340625" y2="29.789" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="29.789" x2="94.862340625" y2="30.014" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="30.014" x2="94.862340625" y2="30.239" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="30.239" x2="94.862340625" y2="30.464" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="30.464" x2="94.862340625" y2="30.689" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="30.689" x2="94.862340625" y2="30.914" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="30.914" x2="94.862340625" y2="31.139" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="31.139" x2="94.862340625" y2="31.364" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="31.364" x2="94.862340625" y2="31.589" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="31.589" x2="94.862340625" y2="31.814" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="31.814" x2="94.862340625" y2="32.039" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="32.039" x2="94.862340625" y2="32.264" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="32.264" x2="94.862340625" y2="32.489" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="32.489" x2="94.862340625" y2="32.714" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="32.714" x2="94.862340625" y2="32.939" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="32.939" x2="94.862340625" y2="33.164" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="33.164" x2="94.862340625" y2="33.23621875" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="33.23621875" x2="97.487340625" y2="33.23621875" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="33.23621875" x2="97.487340625" y2="33.389" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="33.389" x2="97.487340625" y2="33.614" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="33.614" x2="97.487340625" y2="33.839" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="33.839" x2="97.487340625" y2="34.064" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="34.064" x2="97.487340625" y2="34.289" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="34.289" x2="97.487340625" y2="34.514" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="34.514" x2="97.487340625" y2="34.739" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="34.739" x2="97.487340625" y2="34.81106875" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="29.497059375" x2="87.068640625" y2="29.564" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="29.564" x2="87.068640625" y2="29.789" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="29.789" x2="87.068640625" y2="30.014" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="30.014" x2="87.068640625" y2="30.239" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="30.239" x2="87.068640625" y2="30.464" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="30.464" x2="87.068640625" y2="30.689" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="30.689" x2="87.068640625" y2="30.914" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="30.914" x2="87.068640625" y2="31.139" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="31.139" x2="87.068640625" y2="31.364" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="31.364" x2="87.068640625" y2="31.589" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="31.589" x2="87.068640625" y2="31.814" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="31.814" x2="87.068640625" y2="32.039" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="32.039" x2="87.068640625" y2="32.264" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="32.264" x2="87.068640625" y2="32.489" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="32.489" x2="87.068640625" y2="32.714" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="32.714" x2="87.068640625" y2="32.939" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="32.939" x2="87.068640625" y2="33.164" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="33.164" x2="87.068640625" y2="33.389" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="33.389" x2="87.068640625" y2="33.614" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="33.614" x2="87.068640625" y2="33.839" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="33.839" x2="87.068640625" y2="34.064" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="34.064" x2="87.068640625" y2="34.289" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="34.289" x2="87.068640625" y2="34.514" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="34.514" x2="87.068640625" y2="34.739" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="34.739" x2="87.068640625" y2="34.813409375" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="34.813409375" x2="88.64378125" y2="34.813409375" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="34.813409375" x2="88.64378125" y2="34.739" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="34.739" x2="88.64378125" y2="34.514" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="34.514" x2="88.64378125" y2="34.289" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="34.289" x2="88.64378125" y2="34.064" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="34.064" x2="88.64378125" y2="33.839" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="33.839" x2="88.64378125" y2="33.614" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="33.614" x2="88.64378125" y2="33.389" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="33.389" x2="88.64378125" y2="33.164" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="33.164" x2="88.64378125" y2="32.939" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="32.939" x2="88.64378125" y2="32.714" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="32.714" x2="88.64378125" y2="32.489" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="32.489" x2="88.64378125" y2="32.264" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="32.264" x2="88.64378125" y2="32.039" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="32.039" x2="88.64378125" y2="31.814" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="31.814" x2="88.64378125" y2="31.589" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="31.589" x2="88.64378125" y2="31.364" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="31.364" x2="88.64378125" y2="31.139" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="31.139" x2="88.64378125" y2="30.914" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="30.914" x2="88.64378125" y2="30.689" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="30.689" x2="88.64378125" y2="30.464" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="30.464" x2="88.64378125" y2="30.239" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="30.239" x2="88.64378125" y2="30.014" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="30.014" x2="88.64378125" y2="29.789" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="29.789" x2="88.64378125" y2="29.564" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="29.564" x2="88.64378125" y2="29.497059375" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="29.497059375" x2="87.068640625" y2="29.497059375" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="29.49851875" x2="78.286090625" y2="29.564" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="29.564" x2="78.286090625" y2="29.789" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="29.789" x2="78.286090625" y2="30.014" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="30.014" x2="78.286090625" y2="30.239" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="30.239" x2="78.286090625" y2="30.464" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="30.464" x2="78.286090625" y2="30.689" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="30.689" x2="78.286090625" y2="30.914" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="30.914" x2="78.286090625" y2="31.07366875" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="31.07366875" x2="84.81116875" y2="31.07366875" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="31.07366875" x2="84.81116875" y2="30.914" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="30.914" x2="84.81116875" y2="30.689" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="30.689" x2="84.81116875" y2="30.464" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="30.464" x2="84.81116875" y2="30.239" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="30.239" x2="84.81116875" y2="30.014" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="30.014" x2="84.81116875" y2="29.789" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="29.789" x2="84.81116875" y2="29.564" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="29.564" x2="84.81116875" y2="29.49851875" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="29.49851875" x2="78.286090625" y2="29.49851875" width="0.225" layer="94"/>
<wire x1="78.286240625" y1="31.44236875" x2="78.286240625" y2="31.589" width="0.225" layer="94"/>
<wire x1="78.286240625" y1="31.589" x2="78.286240625" y2="31.814" width="0.225" layer="94"/>
<wire x1="78.286240625" y1="31.814" x2="78.286240625" y2="32.039" width="0.225" layer="94"/>
<wire x1="78.286240625" y1="32.039" x2="78.286240625" y2="32.264" width="0.225" layer="94"/>
<wire x1="78.286240625" y1="32.264" x2="78.286240625" y2="32.489" width="0.225" layer="94"/>
<wire x1="78.286240625" y1="32.489" x2="78.286240625" y2="32.714" width="0.225" layer="94"/>
<wire x1="78.286240625" y1="32.714" x2="78.286240625" y2="32.86721875" width="0.225" layer="94"/>
<wire x1="78.286240625" y1="32.86721875" x2="81.96101875" y2="32.86721875" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="32.86721875" x2="81.96101875" y2="32.714" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="32.714" x2="81.96101875" y2="32.489" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="32.489" x2="81.96101875" y2="32.264" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="32.264" x2="81.96101875" y2="32.039" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="32.039" x2="81.96101875" y2="31.814" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="31.814" x2="81.96101875" y2="31.589" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="31.589" x2="81.96101875" y2="31.44236875" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="31.44236875" x2="78.286240625" y2="31.44236875" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="33.23621875" x2="78.286090625" y2="33.389" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="33.389" x2="78.286090625" y2="33.614" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="33.614" x2="78.286090625" y2="33.839" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="33.839" x2="78.286090625" y2="34.064" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="34.064" x2="78.286090625" y2="34.289" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="34.289" x2="78.286090625" y2="34.514" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="34.514" x2="78.286090625" y2="34.739" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="34.739" x2="78.286090625" y2="34.81106875" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="34.81106875" x2="84.81101875" y2="34.81106875" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="34.81106875" x2="84.81101875" y2="34.739" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="34.739" x2="84.81101875" y2="34.514" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="34.514" x2="84.81101875" y2="34.289" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="34.289" x2="84.81101875" y2="34.064" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="34.064" x2="84.81101875" y2="33.839" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="33.839" x2="84.81101875" y2="33.614" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="33.614" x2="84.81101875" y2="33.389" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="33.389" x2="84.81101875" y2="33.23621875" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="33.23621875" x2="78.286090625" y2="33.23621875" width="0.225" layer="94"/>
<wire x1="68.6921" y1="33.944190625" x2="69.073209375" y2="34.289" width="0.225" layer="94"/>
<wire x1="69.073209375" y1="34.289" x2="69.3219" y2="34.514" width="0.225" layer="94"/>
<wire x1="69.3219" y1="34.514" x2="69.570590625" y2="34.739" width="0.225" layer="94"/>
<wire x1="69.570590625" y1="34.739" x2="69.86021875" y2="35.001040625" width="0.225" layer="94"/>
<wire x1="85.115190625" y1="26.34016875" x2="85.115190625" y2="26.189" width="0.225" layer="94"/>
<wire x1="85.115190625" y1="26.189" x2="85.115190625" y2="25.97718125" width="0.225" layer="94"/>
<wire x1="84.32008125" y1="25.980109375" x2="84.341215625" y2="25.70643125" width="0.225" layer="94" curve="8.832242"/>
<wire x1="84.341209375" y1="25.70643125" x2="84.4424375" y2="25.45343125" width="0.225" layer="94" curve="25.94924"/>
<wire x1="84.442440625" y1="25.45343125" x2="84.662590625" y2="25.2962625" width="0.225" layer="94" curve="39.389578"/>
<wire x1="84.662590625" y1="25.296259375" x2="84.93363125" y2="25.258525" width="0.225" layer="94" curve="15.805998"/>
<wire x1="85.52626875" y1="25.706240625" x2="85.547246875" y2="25.980109375" width="0.225" layer="94" curve="8.760059"/>
<wire x1="85.42531875" y1="25.45295" x2="85.46248125" y2="25.514" width="0.225" layer="94" curve="6.74589"/>
<wire x1="85.46248125" y1="25.514" x2="85.526271875" y2="25.706240625" width="0.225" layer="94" curve="19.196299"/>
<wire x1="85.204909375" y1="25.295909375" x2="85.425315625" y2="25.452953125" width="0.225" layer="94" curve="39.656265"/>
<wire x1="84.93363125" y1="25.25853125" x2="85.204909375" y2="25.295909375" width="0.225" layer="94" curve="15.593829"/>
<wire x1="84.32008125" y1="25.980109375" x2="84.32008125" y2="26.189" width="0.225" layer="94"/>
<wire x1="84.32008125" y1="26.189" x2="84.32008125" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="84.341340625" y1="26.61343125" x2="84.32161875" y2="26.414" width="0.225" layer="94" curve="6.503067"/>
<wire x1="84.32161875" y1="26.414" x2="84.320078125" y2="26.34016875" width="0.225" layer="94" curve="2.395218"/>
<wire x1="84.44311875" y1="26.86573125" x2="84.341340625" y2="26.61343125" width="0.225" layer="94" curve="26.142719"/>
<wire x1="84.663740625" y1="27.02155" x2="84.443121875" y2="26.865728125" width="0.225" layer="94" curve="39.450584"/>
<wire x1="84.93363125" y1="27.05881875" x2="84.663740625" y2="27.02155" width="0.225" layer="94" curve="15.292594"/>
<wire x1="85.54725" y1="25.980109375" x2="85.54725" y2="26.189" width="0.225" layer="94"/>
<wire x1="85.54725" y1="26.189" x2="85.54725" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="85.54725" y1="26.34016875" x2="85.52646875" y2="26.61348125" width="0.225" layer="94" curve="8.69613"/>
<wire x1="85.52646875" y1="26.61348125" x2="85.4257" y2="26.8662" width="0.225" layer="94" curve="26.08534"/>
<wire x1="85.4257" y1="26.8662" x2="85.2053" y2="27.02220625" width="0.225" layer="94" curve="39.852867"/>
<wire x1="85.2053" y1="27.022209375" x2="84.93363125" y2="27.058825" width="0.225" layer="94" curve="15.379893"/>
<wire x1="84.752059375" y1="26.34016875" x2="84.752059375" y2="26.189" width="0.225" layer="94"/>
<wire x1="84.752059375" y1="26.189" x2="84.752059375" y2="25.97718125" width="0.225" layer="94"/>
<wire x1="85.115190625" y1="25.97718125" x2="85.11286875" y2="25.87078125" width="0.225" layer="94"/>
<wire x1="85.11286875" y1="25.87078125" x2="85.10523125" y2="25.791359375" width="0.225" layer="94"/>
<wire x1="85.10523125" y1="25.791359375" x2="85.100290625" y2="25.765209375" width="0.225" layer="94"/>
<wire x1="85.100290625" y1="25.765209375" x2="85.09306875" y2="25.73961875" width="0.225" layer="94"/>
<wire x1="85.09306875" y1="25.73961875" x2="85.081940625" y2="25.715509375" width="0.225" layer="94"/>
<wire x1="85.081940625" y1="25.715509375" x2="85.06321875" y2="25.696990625" width="0.225" layer="94"/>
<wire x1="85.06321875" y1="25.696990625" x2="85.0387" y2="25.68683125" width="0.225" layer="94"/>
<wire x1="85.0387" y1="25.68683125" x2="85.012740625" y2="25.68106875" width="0.225" layer="94"/>
<wire x1="85.012740625" y1="25.68106875" x2="84.986340625" y2="25.6778" width="0.225" layer="94"/>
<wire x1="84.986340625" y1="25.6778" x2="84.95978125" y2="25.67618125" width="0.225" layer="94"/>
<wire x1="84.752059375" y1="26.34016875" x2="84.75605" y2="26.47246875" width="0.225" layer="94"/>
<wire x1="84.75605" y1="26.47246875" x2="84.76195" y2="26.52508125" width="0.225" layer="94"/>
<wire x1="84.76195" y1="26.52508125" x2="84.77411875" y2="26.57655" width="0.225" layer="94"/>
<wire x1="84.77411875" y1="26.57655" x2="84.78536875" y2="26.600440625" width="0.225" layer="94"/>
<wire x1="84.78536875" y1="26.600440625" x2="84.804459375" y2="26.618359375" width="0.225" layer="94"/>
<wire x1="84.804459375" y1="26.618359375" x2="84.82903125" y2="26.62806875" width="0.225" layer="94"/>
<wire x1="84.82903125" y1="26.62806875" x2="84.854909375" y2="26.63358125" width="0.225" layer="94"/>
<wire x1="84.854909375" y1="26.63358125" x2="84.8812" y2="26.6367" width="0.225" layer="94"/>
<wire x1="84.8812" y1="26.6367" x2="84.90763125" y2="26.63826875" width="0.225" layer="94"/>
<wire x1="84.90763125" y1="26.63826875" x2="84.96056875" y2="26.63825" width="0.225" layer="94"/>
<wire x1="84.96056875" y1="26.63825" x2="84.987" y2="26.63666875" width="0.225" layer="94"/>
<wire x1="84.987" y1="26.63666875" x2="85.013290625" y2="26.63358125" width="0.225" layer="94"/>
<wire x1="85.013290625" y1="26.63358125" x2="85.039190625" y2="26.628140625" width="0.225" layer="94"/>
<wire x1="85.039190625" y1="26.628140625" x2="85.063809375" y2="26.618559375" width="0.225" layer="94"/>
<wire x1="85.063809375" y1="26.618559375" x2="85.0829" y2="26.60066875" width="0.225" layer="94"/>
<wire x1="85.0829" y1="26.60066875" x2="85.093940625" y2="26.57666875" width="0.225" layer="94"/>
<wire x1="85.093940625" y1="26.57666875" x2="85.105759375" y2="26.52511875" width="0.225" layer="94"/>
<wire x1="85.105759375" y1="26.52511875" x2="85.111390625" y2="26.47248125" width="0.225" layer="94"/>
<wire x1="85.111390625" y1="26.47248125" x2="85.11306875" y2="26.414" width="0.225" layer="94"/>
<wire x1="85.11306875" y1="26.414" x2="85.115190625" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="84.752059375" y1="25.97718125" x2="84.754290625" y2="25.87078125" width="0.225" layer="94"/>
<wire x1="84.754290625" y1="25.87078125" x2="84.76156875" y2="25.79131875" width="0.225" layer="94"/>
<wire x1="84.76156875" y1="25.79131875" x2="84.7664" y2="25.76515" width="0.225" layer="94"/>
<wire x1="84.7664" y1="25.76515" x2="84.773409375" y2="25.7395" width="0.225" layer="94"/>
<wire x1="84.773409375" y1="25.7395" x2="84.784340625" y2="25.7153" width="0.225" layer="94"/>
<wire x1="84.784340625" y1="25.7153" x2="84.803059375" y2="25.6968" width="0.225" layer="94"/>
<wire x1="84.803059375" y1="25.6968" x2="84.82763125" y2="25.686759375" width="0.225" layer="94"/>
<wire x1="84.82763125" y1="25.686759375" x2="84.853609375" y2="25.681059375" width="0.225" layer="94"/>
<wire x1="84.853609375" y1="25.681059375" x2="84.90656875" y2="25.6762" width="0.225" layer="94"/>
<wire x1="84.90656875" y1="25.6762" x2="84.95978125" y2="25.6762" width="0.225" layer="94"/>
<wire x1="88.41505" y1="27.03186875" x2="88.835090625" y2="27.03186875" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="27.03186875" x2="88.835090625" y2="26.864" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="26.864" x2="88.835090625" y2="26.639" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="26.639" x2="88.835090625" y2="26.414" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="26.414" x2="88.835090625" y2="26.189" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="26.189" x2="88.835090625" y2="25.964" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="25.964" x2="88.835090625" y2="25.739" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="25.739" x2="88.835090625" y2="25.514" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="25.514" x2="88.835090625" y2="25.289" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="25.289" x2="88.835090625" y2="25.28548125" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="25.28548125" x2="88.41505" y2="25.28548125" width="0.225" layer="94"/>
<wire x1="88.41505" y1="25.28548125" x2="88.41505" y2="25.289" width="0.225" layer="94"/>
<wire x1="88.41505" y1="25.289" x2="88.41505" y2="25.514" width="0.225" layer="94"/>
<wire x1="88.41505" y1="25.514" x2="88.41505" y2="25.739" width="0.225" layer="94"/>
<wire x1="88.41505" y1="25.739" x2="88.41505" y2="25.964" width="0.225" layer="94"/>
<wire x1="88.41505" y1="25.964" x2="88.41505" y2="26.189" width="0.225" layer="94"/>
<wire x1="88.41505" y1="26.189" x2="88.41505" y2="26.414" width="0.225" layer="94"/>
<wire x1="88.41505" y1="26.414" x2="88.41505" y2="26.639" width="0.225" layer="94"/>
<wire x1="88.41505" y1="26.639" x2="88.41505" y2="26.864" width="0.225" layer="94"/>
<wire x1="88.41505" y1="26.864" x2="88.41505" y2="27.03186875" width="0.225" layer="94"/>
<wire x1="88.41505" y1="27.205890625" x2="88.835090625" y2="27.205890625" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="27.205890625" x2="88.835090625" y2="27.314" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="27.314" x2="88.835090625" y2="27.539" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="27.539" x2="88.835090625" y2="27.68606875" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="27.68606875" x2="88.41505" y2="27.68606875" width="0.225" layer="94"/>
<wire x1="88.41505" y1="27.68606875" x2="88.41505" y2="27.539" width="0.225" layer="94"/>
<wire x1="88.41505" y1="27.539" x2="88.41505" y2="27.314" width="0.225" layer="94"/>
<wire x1="88.41505" y1="27.314" x2="88.41505" y2="27.205890625" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="27.725040625" x2="75.911209375" y2="27.725040625" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="27.725040625" x2="75.911209375" y2="27.539" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="27.539" x2="75.911209375" y2="27.314" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="27.314" x2="75.911209375" y2="27.089" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="27.089" x2="75.911209375" y2="26.864" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="26.864" x2="75.911209375" y2="26.639" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="26.639" x2="75.911209375" y2="26.414" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="26.414" x2="75.911209375" y2="26.189" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="26.189" x2="75.911209375" y2="25.964" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="25.964" x2="75.911209375" y2="25.739" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="25.739" x2="75.911209375" y2="25.514" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="25.514" x2="75.911209375" y2="25.289" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="25.28548125" x2="75.49116875" y2="25.28548125" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="25.28548125" x2="75.49116875" y2="25.289" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="25.289" x2="75.49116875" y2="25.514" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="25.514" x2="75.49116875" y2="25.739" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="25.739" x2="75.49116875" y2="25.964" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="25.964" x2="75.49116875" y2="26.189" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="26.189" x2="75.49116875" y2="26.414" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="26.414" x2="75.49116875" y2="26.639" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="26.639" x2="75.49116875" y2="26.864" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="26.864" x2="75.49116875" y2="27.089" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="27.089" x2="75.49116875" y2="27.314" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="27.314" x2="75.49116875" y2="27.539" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="27.539" x2="75.49116875" y2="27.725040625" width="0.225" layer="94"/>
<wire x1="90.092" y1="25.97718125" x2="90.092" y2="26.189" width="0.225" layer="94"/>
<wire x1="90.092" y1="26.189" x2="90.092" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="90.41016875" y1="26.44828125" x2="90.41016875" y2="26.414" width="0.225" layer="94"/>
<wire x1="90.41016875" y1="26.414" x2="90.41016875" y2="26.30881875" width="0.225" layer="94"/>
<wire x1="90.41016875" y1="26.30881875" x2="90.830209375" y2="26.30881875" width="0.225" layer="94"/>
<wire x1="90.43711875" y1="26.071809375" x2="90.43711875" y2="25.964" width="0.225" layer="94"/>
<wire x1="90.43711875" y1="25.964" x2="90.43711875" y2="25.89925" width="0.225" layer="94"/>
<wire x1="90.43711875" y1="26.071809375" x2="90.86316875" y2="26.071809375" width="0.225" layer="94"/>
<wire x1="90.092" y1="26.34016875" x2="90.09408125" y2="26.45038125" width="0.225" layer="94"/>
<wire x1="90.09408125" y1="26.45038125" x2="90.09926875" y2="26.5163" width="0.225" layer="94"/>
<wire x1="90.09926875" y1="26.5163" x2="90.106540625" y2="26.55976875" width="0.225" layer="94"/>
<wire x1="90.106540625" y1="26.55976875" x2="90.112390625" y2="26.58101875" width="0.225" layer="94"/>
<wire x1="90.112390625" y1="26.58101875" x2="90.12096875" y2="26.6013" width="0.225" layer="94"/>
<wire x1="90.12096875" y1="26.6013" x2="90.135190625" y2="26.617890625" width="0.225" layer="94"/>
<wire x1="90.135190625" y1="26.617890625" x2="90.1551" y2="26.6272" width="0.225" layer="94"/>
<wire x1="90.1551" y1="26.6272" x2="90.17645" y2="26.63263125" width="0.225" layer="94"/>
<wire x1="90.17645" y1="26.63263125" x2="90.19825" y2="26.6359" width="0.225" layer="94"/>
<wire x1="90.19825" y1="26.6359" x2="90.220209375" y2="26.637740625" width="0.225" layer="94"/>
<wire x1="90.220209375" y1="26.637740625" x2="90.242240625" y2="26.63858125" width="0.225" layer="94"/>
<wire x1="90.242240625" y1="26.63858125" x2="90.264290625" y2="26.638640625" width="0.225" layer="94"/>
<wire x1="90.264290625" y1="26.638640625" x2="90.28631875" y2="26.6378" width="0.225" layer="94"/>
<wire x1="90.28631875" y1="26.6378" x2="90.30826875" y2="26.63575" width="0.225" layer="94"/>
<wire x1="90.30826875" y1="26.63575" x2="90.33" y2="26.63208125" width="0.225" layer="94"/>
<wire x1="90.33" y1="26.63208125" x2="90.351140625" y2="26.625909375" width="0.225" layer="94"/>
<wire x1="90.351140625" y1="26.625909375" x2="90.3705" y2="26.615509375" width="0.225" layer="94"/>
<wire x1="90.3705" y1="26.615509375" x2="90.38486875" y2="26.598990625" width="0.225" layer="94"/>
<wire x1="90.38486875" y1="26.598990625" x2="90.40023125" y2="26.557840625" width="0.225" layer="94"/>
<wire x1="90.40023125" y1="26.557840625" x2="90.407140625" y2="26.51431875" width="0.225" layer="94"/>
<wire x1="90.407140625" y1="26.51431875" x2="90.41016875" y2="26.44828125" width="0.225" layer="94"/>
<wire x1="90.092" y1="25.97718125" x2="90.09478125" y2="25.85786875" width="0.225" layer="94"/>
<wire x1="90.09478125" y1="25.85786875" x2="90.09865" y2="25.810290625" width="0.225" layer="94"/>
<wire x1="90.09865" y1="25.810290625" x2="90.11223125" y2="25.74008125" width="0.225" layer="94"/>
<wire x1="90.11223125" y1="25.74008125" x2="90.121240625" y2="25.718009375" width="0.225" layer="94"/>
<wire x1="90.121240625" y1="25.718009375" x2="90.136040625" y2="25.6995" width="0.225" layer="94"/>
<wire x1="90.136040625" y1="25.6995" x2="90.15721875" y2="25.688690625" width="0.225" layer="94"/>
<wire x1="90.15721875" y1="25.688690625" x2="90.18025" y2="25.68248125" width="0.225" layer="94"/>
<wire x1="90.18025" y1="25.68248125" x2="90.20383125" y2="25.678809375" width="0.225" layer="94"/>
<wire x1="90.20383125" y1="25.678809375" x2="90.251459375" y2="25.675840625" width="0.225" layer="94"/>
<wire x1="90.251459375" y1="25.675840625" x2="90.299190625" y2="25.67656875" width="0.225" layer="94"/>
<wire x1="90.299190625" y1="25.67656875" x2="90.34663125" y2="25.681690625" width="0.225" layer="94"/>
<wire x1="90.34663125" y1="25.681690625" x2="90.391890625" y2="25.696190625" width="0.225" layer="94"/>
<wire x1="90.391890625" y1="25.696190625" x2="90.409190625" y2="25.71226875" width="0.225" layer="94"/>
<wire x1="90.409190625" y1="25.71226875" x2="90.419459375" y2="25.733759375" width="0.225" layer="94"/>
<wire x1="90.419459375" y1="25.733759375" x2="90.425940625" y2="25.75671875" width="0.225" layer="94"/>
<wire x1="90.425940625" y1="25.75671875" x2="90.4332" y2="25.80388125" width="0.225" layer="94"/>
<wire x1="90.4332" y1="25.80388125" x2="90.436309375" y2="25.85151875" width="0.225" layer="94"/>
<wire x1="90.436309375" y1="25.85151875" x2="90.43711875" y2="25.89925" width="0.225" layer="94"/>
<wire x1="89.66001875" y1="25.980109375" x2="89.67878125" y2="25.718659375" width="0.225" layer="94" curve="8.208716"/>
<wire x1="89.67878125" y1="25.718659375" x2="89.74463125" y2="25.514" width="0.225" layer="94" curve="19.254744"/>
<wire x1="89.74463125" y1="25.514" x2="89.76749375" y2="25.47364375" width="0.225" layer="94" curve="4.135389"/>
<wire x1="89.767490625" y1="25.473640625" x2="89.966896875" y2="25.309234375" width="0.225" layer="94" curve="37.791863"/>
<wire x1="89.9669" y1="25.309240625" x2="90.02831875" y2="25.289" width="0.225" layer="94" curve="4.744864"/>
<wire x1="90.02831875" y1="25.289" x2="90.22315" y2="25.259475" width="0.225" layer="94" curve="14.492988"/>
<wire x1="90.86316875" y1="26.071809375" x2="90.86316875" y2="25.964" width="0.225" layer="94"/>
<wire x1="90.86316875" y1="25.964" x2="90.86316875" y2="25.89603125" width="0.225" layer="94"/>
<wire x1="90.83383125" y1="25.63598125" x2="90.852559375" y2="25.739" width="0.225" layer="94" curve="5.141671"/>
<wire x1="90.852559375" y1="25.739" x2="90.86316875" y2="25.89603125" width="0.225" layer="94" curve="7.731903"/>
<wire x1="90.710259375" y1="25.40781875" x2="90.83383125" y2="25.63598125" width="0.225" layer="94" curve="31.131749"/>
<wire x1="90.483690625" y1="25.282109375" x2="90.710259375" y2="25.40781875" width="0.225" layer="94" curve="33.941784"/>
<wire x1="90.22315" y1="25.25946875" x2="90.483690625" y2="25.282103125" width="0.225" layer="94" curve="14.175253"/>
<wire x1="89.66001875" y1="25.980109375" x2="89.66001875" y2="26.189" width="0.225" layer="94"/>
<wire x1="89.66001875" y1="26.189" x2="89.66001875" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="89.6773" y1="26.590590625" x2="89.66001875" y2="26.34016875" width="0.225" layer="94" curve="7.895583"/>
<wire x1="89.75726875" y1="26.82721875" x2="89.685840625" y2="26.639" width="0.225" layer="94" curve="17.336884"/>
<wire x1="89.685840625" y1="26.639" x2="89.6773" y2="26.590590625" width="0.225" layer="94" curve="4.218105"/>
<wire x1="89.93936875" y1="26.995190625" x2="89.75726875" y2="26.82721875" width="0.225" layer="94" curve="35.721204"/>
<wire x1="90.18176875" y1="27.05536875" x2="89.93936875" y2="26.995190625" width="0.225" layer="94" curve="21.771833"/>
<wire x1="90.830209375" y1="26.30881875" x2="90.830209375" y2="26.414" width="0.225" layer="94"/>
<wire x1="90.830209375" y1="26.414" x2="90.830209375" y2="26.49631875" width="0.225" layer="94"/>
<wire x1="90.830209375" y1="26.49631875" x2="90.7978375" y2="26.744540625" width="0.225" layer="94" curve="14.861165"/>
<wire x1="90.797840625" y1="26.744540625" x2="90.6616625" y2="26.951284375" width="0.225" layer="94" curve="37.022842"/>
<wire x1="90.661659375" y1="26.95128125" x2="90.432340625" y2="27.047065625" width="0.225" layer="94" curve="30.892228"/>
<wire x1="90.432340625" y1="27.04706875" x2="90.18176875" y2="27.055365625" width="0.225" layer="94" curve="10.656803"/>
<wire x1="68.6921" y1="33.944190625" x2="68.78723125" y2="33.839" width="0.225" layer="94"/>
<wire x1="68.78723125" y1="33.839" x2="69.804659375" y2="32.714" width="0.225" layer="94"/>
<wire x1="69.804659375" y1="32.714" x2="70.21163125" y2="32.264" width="0.225" layer="94"/>
<wire x1="70.21163125" y1="32.264" x2="71.229059375" y2="31.139" width="0.225" layer="94"/>
<wire x1="71.229059375" y1="31.139" x2="71.63603125" y2="30.689" width="0.225" layer="94"/>
<wire x1="71.63603125" y1="30.689" x2="72.653459375" y2="29.564" width="0.225" layer="94"/>
<wire x1="72.653459375" y1="29.564" x2="72.94835" y2="29.23793125" width="0.225" layer="94"/>
<wire x1="72.94835" y1="29.23793125" x2="73.44671875" y2="29.789" width="0.225" layer="94"/>
<wire x1="73.44671875" y1="29.789" x2="73.6502" y2="30.014" width="0.225" layer="94"/>
<wire x1="73.6502" y1="30.014" x2="74.26065" y2="30.689" width="0.225" layer="94"/>
<wire x1="74.26065" y1="30.689" x2="74.46413125" y2="30.914" width="0.225" layer="94"/>
<wire x1="74.46413125" y1="30.914" x2="75.278059375" y2="31.814" width="0.225" layer="94"/>
<wire x1="75.278059375" y1="31.814" x2="75.481540625" y2="32.039" width="0.225" layer="94"/>
<wire x1="75.481540625" y1="32.039" x2="76.29546875" y2="32.939" width="0.225" layer="94"/>
<wire x1="76.29546875" y1="32.939" x2="76.49895" y2="33.164" width="0.225" layer="94"/>
<wire x1="76.49895" y1="33.164" x2="76.70243125" y2="33.389" width="0.225" layer="94"/>
<wire x1="76.70243125" y1="33.389" x2="77.204709375" y2="33.944390625" width="0.225" layer="94"/>
<wire x1="77.204709375" y1="33.944390625" x2="76.036390625" y2="35.00103125" width="0.225" layer="94"/>
<wire x1="76.036390625" y1="35.00103125" x2="74.57861875" y2="33.389" width="0.225" layer="94"/>
<wire x1="74.57861875" y1="33.389" x2="74.37515" y2="33.164" width="0.225" layer="94"/>
<wire x1="74.37515" y1="33.164" x2="74.17168125" y2="32.939" width="0.225" layer="94"/>
<wire x1="74.17168125" y1="32.939" x2="73.764740625" y2="32.489" width="0.225" layer="94"/>
<wire x1="73.764740625" y1="32.489" x2="72.94835" y2="31.58621875" width="0.225" layer="94"/>
<wire x1="72.94835" y1="31.58621875" x2="72.742359375" y2="31.814" width="0.225" layer="94"/>
<wire x1="72.742359375" y1="31.814" x2="72.131940625" y2="32.489" width="0.225" layer="94"/>
<wire x1="72.131940625" y1="32.489" x2="71.724990625" y2="32.939" width="0.225" layer="94"/>
<wire x1="71.724990625" y1="32.939" x2="71.318040625" y2="33.389" width="0.225" layer="94"/>
<wire x1="71.318040625" y1="33.389" x2="70.911090625" y2="33.839" width="0.225" layer="94"/>
<wire x1="70.911090625" y1="33.839" x2="70.504140625" y2="34.289" width="0.225" layer="94"/>
<wire x1="70.504140625" y1="34.289" x2="70.097190625" y2="34.739" width="0.225" layer="94"/>
<wire x1="70.097190625" y1="34.739" x2="69.86021875" y2="35.001040625" width="0.225" layer="94"/>
<wire x1="81.341240625" y1="26.03255" x2="81.74326875" y2="26.03255" width="0.225" layer="94"/>
<wire x1="81.74326875" y1="26.03255" x2="81.74326875" y2="25.964" width="0.225" layer="94"/>
<wire x1="81.74326875" y1="25.964" x2="81.74326875" y2="25.812240625" width="0.225" layer="94"/>
<wire x1="81.723590625" y1="25.602390625" x2="81.743275" y2="25.812240625" width="0.225" layer="94" curve="10.717258"/>
<wire x1="81.637890625" y1="25.41138125" x2="81.7235875" y2="25.602390625" width="0.225" layer="94" curve="26.892331"/>
<wire x1="81.467640625" y1="25.291140625" x2="81.6378875" y2="25.41138125" width="0.225" layer="94" curve="34.316783"/>
<wire x1="81.260009375" y1="25.258559375" x2="81.467640625" y2="25.291140625" width="0.225" layer="94" curve="18.312496"/>
<wire x1="81.05158125" y1="25.28623125" x2="81.260009375" y2="25.258559375" width="0.225" layer="94" curve="14.64866"/>
<wire x1="80.880909375" y1="25.405009375" x2="81.05158125" y2="25.286234375" width="0.225" layer="94" curve="39.895242"/>
<wire x1="80.80178125" y1="25.5987" x2="80.880903125" y2="25.40500625" width="0.225" layer="94" curve="25.996239"/>
<wire x1="80.786140625" y1="25.80901875" x2="80.786140625" y2="25.964" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="25.964" x2="80.786140625" y2="26.189" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="26.189" x2="80.786140625" y2="26.414" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="26.414" x2="80.786140625" y2="26.626990625" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="25.80901875" x2="80.78728125" y2="25.739" width="0.225" layer="94" curve="3.295417"/>
<wire x1="80.78728125" y1="25.739" x2="80.801784375" y2="25.5987" width="0.225" layer="94" curve="6.640297"/>
<wire x1="81.206190625" y1="25.86908125" x2="81.206190625" y2="25.964" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="25.964" x2="81.206190625" y2="26.189" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="26.189" x2="81.206190625" y2="26.414" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="26.414" x2="81.206190625" y2="26.626990625" width="0.225" layer="94"/>
<wire x1="81.341240625" y1="26.03255" x2="81.341240625" y2="25.964" width="0.225" layer="94"/>
<wire x1="81.341240625" y1="25.964" x2="81.341240625" y2="25.779140625" width="0.225" layer="94"/>
<wire x1="81.317509375" y1="25.6654" x2="81.327084375" y2="25.6739125" width="0.225" layer="94" curve="49.818215"/>
<wire x1="81.30451875" y1="25.66235" x2="81.3175125" y2="25.66539375" width="0.225" layer="94" curve="7.103613"/>
<wire x1="81.291240625" y1="25.66093125" x2="81.30451875" y2="25.66235625" width="0.225" layer="94" curve="7.003084"/>
<wire x1="81.277890625" y1="25.660490625" x2="81.291240625" y2="25.660934375" width="0.225" layer="94" curve="1.449937"/>
<wire x1="81.26453125" y1="25.66073125" x2="81.277890625" y2="25.6604875" width="0.225" layer="94" curve="4.441603"/>
<wire x1="81.33501875" y1="25.69935" x2="81.33183125" y2="25.68638125" width="0.225" layer="94"/>
<wire x1="81.33183125" y1="25.68638125" x2="81.327090625" y2="25.673909375" width="0.225" layer="94"/>
<wire x1="81.33501875" y1="25.69935" x2="81.3373" y2="25.712509375" width="0.225" layer="94"/>
<wire x1="81.3373" y1="25.712509375" x2="81.340009375" y2="25.739" width="0.225" layer="94"/>
<wire x1="81.340009375" y1="25.739" x2="81.34001875" y2="25.739090625" width="0.225" layer="94"/>
<wire x1="81.34001875" y1="25.739090625" x2="81.341240625" y2="25.779140625" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="25.86908125" x2="81.20651875" y2="25.818159375" width="0.225" layer="94"/>
<wire x1="81.20651875" y1="25.818159375" x2="81.208009375" y2="25.76726875" width="0.225" layer="94"/>
<wire x1="81.208009375" y1="25.76726875" x2="81.209609375" y2="25.741859375" width="0.225" layer="94"/>
<wire x1="81.209609375" y1="25.741859375" x2="81.21228125" y2="25.716540625" width="0.225" layer="94"/>
<wire x1="81.21228125" y1="25.716540625" x2="81.21671875" y2="25.69148125" width="0.225" layer="94"/>
<wire x1="81.21671875" y1="25.69148125" x2="81.225321875" y2="25.66765" width="0.225" layer="94" curve="19.627877"/>
<wire x1="81.26453125" y1="25.66073125" x2="81.251209375" y2="25.66166875" width="0.225" layer="94"/>
<wire x1="81.238" y1="25.663609375" x2="81.251209375" y2="25.661671875" width="0.225" layer="94" curve="8.648156"/>
<wire x1="81.22531875" y1="25.66765" x2="81.238" y2="25.66360625" width="0.225" layer="94" curve="10.044676"/>
<wire x1="81.7013" y1="26.626990625" x2="81.206190625" y2="26.626990625" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="27.03186875" x2="81.7013" y2="27.03186875" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="27.401009375" x2="81.206190625" y2="27.314" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="27.314" x2="81.206190625" y2="27.089" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="27.089" x2="81.206190625" y2="27.03186875" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="27.03186875" x2="80.786140625" y2="27.089" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="27.089" x2="80.786140625" y2="27.314" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="27.314" x2="80.786140625" y2="27.401009375" width="0.225" layer="94"/>
<wire x1="80.627059375" y1="27.03186875" x2="80.786140625" y2="27.03186875" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="26.626990625" x2="80.627059375" y2="26.626990625" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="27.401009375" x2="81.206190625" y2="27.401009375" width="0.225" layer="94"/>
<wire x1="81.7013" y1="27.03186875" x2="81.7013" y2="26.864" width="0.225" layer="94"/>
<wire x1="81.7013" y1="26.864" x2="81.7013" y2="26.639" width="0.225" layer="94"/>
<wire x1="81.7013" y1="26.639" x2="81.7013" y2="26.626990625" width="0.225" layer="94"/>
<wire x1="80.627059375" y1="27.03186875" x2="80.627059375" y2="26.864" width="0.225" layer="94"/>
<wire x1="80.627059375" y1="26.864" x2="80.627059375" y2="26.639" width="0.225" layer="94"/>
<wire x1="80.627059375" y1="26.639" x2="80.627059375" y2="26.626990625" width="0.225" layer="94"/>
<wire x1="92.6429" y1="26.95253125" x2="92.469709375" y2="27.040159375" width="0.225" layer="94" curve="32.338936"/>
<wire x1="79.95228125" y1="26.071809375" x2="79.52623125" y2="26.071809375" width="0.225" layer="94"/>
<wire x1="79.49928125" y1="26.30881875" x2="79.91931875" y2="26.30881875" width="0.225" layer="94"/>
<wire x1="79.91931875" y1="26.30881875" x2="79.91931875" y2="26.414" width="0.225" layer="94"/>
<wire x1="79.91931875" y1="26.414" x2="79.91931875" y2="26.49631875" width="0.225" layer="94"/>
<wire x1="79.91931875" y1="26.49631875" x2="79.886946875" y2="26.744540625" width="0.225" layer="94" curve="14.861165"/>
<wire x1="79.88695" y1="26.744540625" x2="79.750771875" y2="26.951284375" width="0.225" layer="94" curve="37.022842"/>
<wire x1="79.75076875" y1="26.95128125" x2="79.521459375" y2="27.047065625" width="0.225" layer="94" curve="30.891086"/>
<wire x1="79.521459375" y1="27.04706875" x2="79.27088125" y2="27.055365625" width="0.225" layer="94" curve="10.657067"/>
<wire x1="79.27088125" y1="27.05536875" x2="79.02848125" y2="26.995190625" width="0.225" layer="94" curve="21.019898"/>
<wire x1="79.02848125" y1="26.995190625" x2="78.84638125" y2="26.82721875" width="0.225" layer="94" curve="36.47327"/>
<wire x1="78.84638125" y1="26.82721875" x2="78.77521875" y2="26.639" width="0.225" layer="94" curve="16.726592"/>
<wire x1="78.77521875" y1="26.639" x2="78.7664125" y2="26.590590625" width="0.225" layer="94" curve="4.076419"/>
<wire x1="78.766409375" y1="26.590590625" x2="78.75028125" y2="26.414" width="0.225" layer="94" curve="6.105964"/>
<wire x1="78.75028125" y1="26.414" x2="78.749128125" y2="26.34016875" width="0.225" layer="94" curve="2.541594"/>
<wire x1="79.95228125" y1="26.071809375" x2="79.95228125" y2="25.964" width="0.225" layer="94"/>
<wire x1="79.95228125" y1="25.964" x2="79.95228125" y2="25.89603125" width="0.225" layer="94"/>
<wire x1="79.922940625" y1="25.63598125" x2="79.94166875" y2="25.739" width="0.225" layer="94" curve="5.141671"/>
<wire x1="79.94166875" y1="25.739" x2="79.952278125" y2="25.89603125" width="0.225" layer="94" curve="7.731903"/>
<wire x1="79.79936875" y1="25.40781875" x2="79.922940625" y2="25.63598125" width="0.225" layer="94" curve="31.131749"/>
<wire x1="79.572809375" y1="25.282109375" x2="79.60065" y2="25.289" width="0.225" layer="94" curve="3.702975"/>
<wire x1="79.60065" y1="25.289" x2="79.799371875" y2="25.407815625" width="0.225" layer="94" curve="30.237845"/>
<wire x1="79.312259375" y1="25.25946875" x2="79.572809375" y2="25.28210625" width="0.225" layer="94" curve="14.175788"/>
<wire x1="79.056009375" y1="25.309240625" x2="79.119840625" y2="25.289" width="0.225" layer="94" curve="4.53295"/>
<wire x1="79.119840625" y1="25.289" x2="79.312259375" y2="25.259475" width="0.225" layer="94" curve="13.203582"/>
<wire x1="78.856609375" y1="25.473640625" x2="79.056009375" y2="25.3092375" width="0.225" layer="94" curve="39.29237"/>
<wire x1="78.767890625" y1="25.718659375" x2="78.85660625" y2="25.473640625" width="0.225" layer="94" curve="21.889189"/>
<wire x1="78.74913125" y1="25.980109375" x2="78.76789375" y2="25.718659375" width="0.225" layer="94" curve="9.71004"/>
<wire x1="78.74913125" y1="25.980109375" x2="78.74913125" y2="26.189" width="0.225" layer="94"/>
<wire x1="78.74913125" y1="26.189" x2="78.74913125" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="79.49928125" y1="26.30881875" x2="79.49928125" y2="26.414" width="0.225" layer="94"/>
<wire x1="79.49928125" y1="26.414" x2="79.49928125" y2="26.44828125" width="0.225" layer="94"/>
<wire x1="79.49928125" y1="26.44828125" x2="79.496253125" y2="26.51431875" width="0.225" layer="94" curve="5.249884"/>
<wire x1="79.49625" y1="26.51431875" x2="79.48934375" y2="26.557840625" width="0.225" layer="94" curve="7.529032"/>
<wire x1="79.489340625" y1="26.557840625" x2="79.483196875" y2="26.579" width="0.225" layer="94" curve="6.832689"/>
<wire x1="79.4832" y1="26.579" x2="79.47398125" y2="26.598990625" width="0.225" layer="94" curve="10.282953"/>
<wire x1="79.47398125" y1="26.598990625" x2="79.459609375" y2="26.615509375" width="0.225" layer="94" curve="22.257449"/>
<wire x1="79.459609375" y1="26.615509375" x2="79.440259375" y2="26.625909375" width="0.225" layer="94" curve="19.188066"/>
<wire x1="79.440259375" y1="26.625909375" x2="79.39738125" y2="26.63575" width="0.225" layer="94" curve="11.463239"/>
<wire x1="79.39738125" y1="26.63575" x2="79.3534" y2="26.6386375" width="0.225" layer="94" curve="6.876925"/>
<wire x1="79.3534" y1="26.638640625" x2="79.30931875" y2="26.63774375" width="0.225" layer="94" curve="2.970191"/>
<wire x1="79.30931875" y1="26.637740625" x2="79.265559375" y2="26.632625" width="0.225" layer="94" curve="8.0302"/>
<wire x1="79.265559375" y1="26.63263125" x2="79.244209375" y2="26.6272" width="0.225" layer="94" curve="7.184068"/>
<wire x1="79.244209375" y1="26.6272" x2="79.2243" y2="26.617890625" width="0.225" layer="94" curve="14.385749"/>
<wire x1="79.2243" y1="26.617890625" x2="79.210078125" y2="26.6013" width="0.225" layer="94" curve="34.295039"/>
<wire x1="79.21008125" y1="26.6013" x2="79.191453125" y2="26.53813125" width="0.225" layer="94" curve="14.037627"/>
<wire x1="79.19145" y1="26.53813125" x2="79.1844125" y2="26.472390625" width="0.225" layer="94" curve="6.60122"/>
<wire x1="79.52623125" y1="26.071809375" x2="79.52623125" y2="25.964" width="0.225" layer="94"/>
<wire x1="79.52623125" y1="25.964" x2="79.52623125" y2="25.89925" width="0.225" layer="94"/>
<wire x1="79.51938125" y1="25.780190625" x2="79.526234375" y2="25.89925" width="0.225" layer="94" curve="6.589587"/>
<wire x1="79.50856875" y1="25.733759375" x2="79.519378125" y2="25.780190625" width="0.225" layer="94" curve="13.026604"/>
<wire x1="79.45898125" y1="25.6871" x2="79.508565625" y2="25.733759375" width="0.225" layer="94" curve="54.248333"/>
<wire x1="79.4121" y1="25.6784" x2="79.45898125" y2="25.687103125" width="0.225" layer="94" curve="11.235116"/>
<wire x1="79.292940625" y1="25.678809375" x2="79.4121" y2="25.678396875" width="0.225" layer="94" curve="10.193817"/>
<wire x1="79.24633125" y1="25.688690625" x2="79.292940625" y2="25.6788125" width="0.225" layer="94" curve="13.338819"/>
<wire x1="79.22515" y1="25.6995" x2="79.24633125" y2="25.6886875" width="0.225" layer="94" curve="16.819291"/>
<wire x1="79.21035" y1="25.718009375" x2="79.225146875" y2="25.69949375" width="0.225" layer="94" curve="31.833994"/>
<wire x1="79.195259375" y1="25.763159375" x2="79.21035625" y2="25.7180125" width="0.225" layer="94" curve="8.452794"/>
<wire x1="79.187759375" y1="25.810290625" x2="79.19525625" y2="25.763159375" width="0.225" layer="94" curve="10.449934"/>
<wire x1="79.187759375" y1="25.810290625" x2="79.18275" y2="25.881709375" width="0.225" layer="94"/>
<wire x1="79.18441875" y1="26.472390625" x2="79.181740625" y2="26.406309375" width="0.225" layer="94"/>
<wire x1="79.181740625" y1="26.406309375" x2="79.181109375" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="79.181109375" y1="26.34016875" x2="79.181109375" y2="26.189" width="0.225" layer="94"/>
<wire x1="79.181109375" y1="26.189" x2="79.181109375" y2="25.97718125" width="0.225" layer="94"/>
<wire x1="79.181109375" y1="25.97718125" x2="79.18275" y2="25.881709375" width="0.225" layer="94"/>
<wire x1="77.1742" y1="26.422790625" x2="77.54935" y2="26.422790625" width="0.225" layer="94"/>
<wire x1="77.1742" y1="26.00881875" x2="77.969390625" y2="26.00881875" width="0.225" layer="94"/>
<wire x1="77.969390625" y1="26.00881875" x2="77.969390625" y2="26.189" width="0.225" layer="94"/>
<wire x1="77.969390625" y1="26.189" x2="77.969390625" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="77.969390625" y1="26.34016875" x2="77.948609375" y2="26.61348125" width="0.225" layer="94" curve="8.696144"/>
<wire x1="77.948609375" y1="26.61348125" x2="77.84784375" y2="26.866196875" width="0.225" layer="94" curve="26.085225"/>
<wire x1="77.84785" y1="26.8662" x2="77.62745" y2="27.022209375" width="0.225" layer="94" curve="39.85306"/>
<wire x1="77.62745" y1="27.022209375" x2="77.35661875" y2="27.058828125" width="0.225" layer="94" curve="15.332321"/>
<wire x1="77.35661875" y1="27.05881875" x2="77.08588125" y2="27.02155" width="0.225" layer="94" curve="15.743623"/>
<wire x1="77.08588125" y1="27.02155" x2="76.865265625" y2="26.865728125" width="0.225" layer="94" curve="39.046943"/>
<wire x1="76.865259375" y1="26.86573125" x2="76.864040625" y2="26.864" width="0.225" layer="94" curve="0.204743"/>
<wire x1="76.864040625" y1="26.864" x2="76.763478125" y2="26.61343125" width="0.225" layer="94" curve="26.341038"/>
<wire x1="76.76348125" y1="26.61343125" x2="76.74395" y2="26.414" width="0.225" layer="94" curve="6.208124"/>
<wire x1="76.74395" y1="26.414" x2="76.74221875" y2="26.34016875" width="0.225" layer="94" curve="2.287022"/>
<wire x1="77.561359375" y1="25.98158125" x2="77.966390625" y2="25.98158125" width="0.225" layer="94"/>
<wire x1="77.966390625" y1="25.98158125" x2="77.966390625" y2="25.964" width="0.225" layer="94"/>
<wire x1="77.966390625" y1="25.964" x2="77.966390625" y2="25.8181" width="0.225" layer="94"/>
<wire x1="77.92765" y1="25.565709375" x2="77.966390625" y2="25.8181" width="0.225" layer="94" curve="17.452125"/>
<wire x1="77.77643125" y1="25.363190625" x2="77.92765" y2="25.565709375" width="0.225" layer="94" curve="38.592139"/>
<wire x1="77.53896875" y1="25.271940625" x2="77.618090625" y2="25.289" width="0.225" layer="94" curve="8.168491"/>
<wire x1="77.618090625" y1="25.289" x2="77.77643125" y2="25.363190625" width="0.225" layer="94" curve="17.702304"/>
<wire x1="77.28301875" y1="25.26058125" x2="77.53896875" y2="25.271940625" width="0.225" layer="94" curve="11.086795"/>
<wire x1="77.03403125" y1="25.31643125" x2="77.11308125" y2="25.289" width="0.225" layer="94" curve="6.296209"/>
<wire x1="77.11308125" y1="25.289" x2="77.28301875" y2="25.260578125" width="0.225" layer="94" curve="12.986111"/>
<wire x1="76.844159375" y1="25.483540625" x2="77.034028125" y2="25.316421875" width="0.225" layer="94" curve="38.138006"/>
<wire x1="76.760209375" y1="25.724359375" x2="76.8441625" y2="25.483540625" width="0.225" layer="94" curve="20.717901"/>
<wire x1="76.74221875" y1="25.980109375" x2="76.758" y2="25.739" width="0.225" layer="94" curve="9.114768"/>
<wire x1="76.758" y1="25.739" x2="76.760209375" y2="25.724359375" width="0.225" layer="94" curve="0.557951"/>
<wire x1="76.74221875" y1="25.980109375" x2="76.74221875" y2="26.189" width="0.225" layer="94"/>
<wire x1="76.74221875" y1="26.189" x2="76.74221875" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="77.1742" y1="26.00881875" x2="77.1742" y2="25.97718125" width="0.225" layer="94"/>
<wire x1="77.1742" y1="25.97718125" x2="77.17648125" y2="25.863140625" width="0.225" layer="94"/>
<wire x1="77.17648125" y1="25.863140625" x2="77.179640625" y2="25.81763125" width="0.225" layer="94"/>
<wire x1="77.179640625" y1="25.81763125" x2="77.185759375" y2="25.77241875" width="0.225" layer="94"/>
<wire x1="77.185759375" y1="25.77241875" x2="77.19780625" y2="25.728490625" width="0.225" layer="94" curve="15.249939"/>
<wire x1="77.197809375" y1="25.728490625" x2="77.22771875" y2="25.69585" width="0.225" layer="94" curve="39.077584"/>
<wire x1="77.22771875" y1="25.69585" x2="77.2493" y2="25.688528125" width="0.225" layer="94" curve="18.445063"/>
<wire x1="77.2493" y1="25.68853125" x2="77.45375" y2="25.6834" width="0.225" layer="94" curve="16.158964"/>
<wire x1="77.45375" y1="25.6834" x2="77.498609375" y2="25.691559375" width="0.225" layer="94" curve="7.337797"/>
<wire x1="77.498609375" y1="25.691559375" x2="77.520209375" y2="25.6988375" width="0.225" layer="94" curve="9.286376"/>
<wire x1="77.561359375" y1="25.98158125" x2="77.561359375" y2="25.964" width="0.225" layer="94"/>
<wire x1="77.561359375" y1="25.964" x2="77.561359375" y2="25.82103125" width="0.225" layer="94"/>
<wire x1="77.561359375" y1="25.82103125" x2="77.559340625" y2="25.775459375" width="0.225" layer="94"/>
<wire x1="77.559340625" y1="25.775459375" x2="77.55613125" y2="25.75288125" width="0.225" layer="94"/>
<wire x1="77.520209375" y1="25.698840625" x2="77.556125" y2="25.75288125" width="0.225" layer="94" curve="51.030296"/>
<wire x1="77.184159375" y1="26.547240625" x2="77.174196875" y2="26.422790625" width="0.225" layer="94" curve="9.151585"/>
<wire x1="77.2077" y1="26.6134" x2="77.184159375" y2="26.547240625" width="0.225" layer="94" curve="20.872634"/>
<wire x1="77.54935" y1="26.422790625" x2="77.539775" y2="26.547290625" width="0.225" layer="94" curve="8.794744"/>
<wire x1="77.53978125" y1="26.547290625" x2="77.527525" y2="26.592734375" width="0.225" layer="94" curve="12.594721"/>
<wire x1="77.52751875" y1="26.59273125" x2="77.516803125" y2="26.613665625" width="0.225" layer="94" curve="11.429217"/>
<wire x1="77.516809375" y1="26.61366875" x2="77.5000125" y2="26.629975" width="0.225" layer="94" curve="26.064892"/>
<wire x1="77.500009375" y1="26.62996875" x2="77.47873125" y2="26.63995" width="0.225" layer="94" curve="11.965711"/>
<wire x1="77.47873125" y1="26.63995" x2="77.455990625" y2="26.64610625" width="0.225" layer="94" curve="8.00028"/>
<wire x1="77.455990625" y1="26.646109375" x2="77.38576875" y2="26.653571875" width="0.225" layer="94" curve="10.160203"/>
<wire x1="77.38576875" y1="26.65356875" x2="77.315109375" y2="26.652346875" width="0.225" layer="94" curve="3.952821"/>
<wire x1="77.315109375" y1="26.65235" x2="77.268440625" y2="26.646071875" width="0.225" layer="94" curve="9.390964"/>
<wire x1="77.268440625" y1="26.64606875" x2="77.245740625" y2="26.639796875" width="0.225" layer="94" curve="6.177099"/>
<wire x1="77.245740625" y1="26.6398" x2="77.24345" y2="26.639" width="0.225" layer="94" curve="1.397304"/>
<wire x1="77.24345" y1="26.639" x2="77.22448125" y2="26.629740625" width="0.225" layer="94" curve="12.178653"/>
<wire x1="77.22448125" y1="26.629740625" x2="77.207703125" y2="26.613396875" width="0.225" layer="94" curve="24.279547"/>
<wire x1="73.9395" y1="26.629740625" x2="73.922721875" y2="26.613396875" width="0.225" layer="94" curve="24.279547"/>
<wire x1="73.96075" y1="26.6398" x2="73.958459375" y2="26.639" width="0.225" layer="94" curve="1.397249"/>
<wire x1="73.958459375" y1="26.639" x2="73.939496875" y2="26.62974375" width="0.225" layer="94" curve="12.174138"/>
<wire x1="73.98345" y1="26.64606875" x2="73.96075" y2="26.639796875" width="0.225" layer="94" curve="6.177099"/>
<wire x1="74.03013125" y1="26.65235" x2="73.98345" y2="26.64606875" width="0.225" layer="94" curve="9.393557"/>
<wire x1="74.100790625" y1="26.65356875" x2="74.03013125" y2="26.652346875" width="0.225" layer="94" curve="3.952821"/>
<wire x1="74.171009375" y1="26.646109375" x2="74.100790625" y2="26.653571875" width="0.225" layer="94" curve="10.159745"/>
<wire x1="74.193740625" y1="26.63995" x2="74.171009375" y2="26.646103125" width="0.225" layer="94" curve="7.996993"/>
<wire x1="74.21501875" y1="26.62996875" x2="74.193740625" y2="26.63995" width="0.225" layer="94" curve="11.965711"/>
<wire x1="74.23181875" y1="26.61366875" x2="74.215021875" y2="26.629975" width="0.225" layer="94" curve="26.067092"/>
<wire x1="74.242540625" y1="26.59273125" x2="74.231821875" y2="26.613671875" width="0.225" layer="94" curve="11.432049"/>
<wire x1="74.254790625" y1="26.547290625" x2="74.2425375" y2="26.59273125" width="0.225" layer="94" curve="12.593993"/>
<wire x1="74.264359375" y1="26.422790625" x2="74.254784375" y2="26.547290625" width="0.225" layer="94" curve="8.794744"/>
<wire x1="73.92271875" y1="26.6134" x2="73.899178125" y2="26.547240625" width="0.225" layer="94" curve="20.872634"/>
<wire x1="73.89918125" y1="26.547240625" x2="73.88921875" y2="26.422790625" width="0.225" layer="94" curve="9.151622"/>
<wire x1="74.23521875" y1="25.698840625" x2="74.2678" y2="25.739" width="0.225" layer="94" curve="40.155698"/>
<wire x1="74.2678" y1="25.739" x2="74.271134375" y2="25.75288125" width="0.225" layer="94" curve="10.876304"/>
<wire x1="74.27435" y1="25.775459375" x2="74.271140625" y2="25.75288125" width="0.225" layer="94"/>
<wire x1="74.27638125" y1="25.82103125" x2="74.27435" y2="25.775459375" width="0.225" layer="94"/>
<wire x1="74.27638125" y1="25.98158125" x2="74.27638125" y2="25.964" width="0.225" layer="94"/>
<wire x1="74.27638125" y1="25.964" x2="74.27638125" y2="25.82103125" width="0.225" layer="94"/>
<wire x1="74.21361875" y1="25.691559375" x2="74.23521875" y2="25.6988375" width="0.225" layer="94" curve="9.286507"/>
<wire x1="74.16876875" y1="25.6834" x2="74.21361875" y2="25.691559375" width="0.225" layer="94" curve="7.336311"/>
<wire x1="73.964309375" y1="25.68853125" x2="74.16876875" y2="25.6834" width="0.225" layer="94" curve="16.159693"/>
<wire x1="73.94273125" y1="25.69585" x2="73.964309375" y2="25.68853125" width="0.225" layer="94" curve="18.441658"/>
<wire x1="73.91281875" y1="25.728490625" x2="73.942728125" y2="25.695846875" width="0.225" layer="94" curve="39.07874"/>
<wire x1="73.90078125" y1="25.77241875" x2="73.912828125" y2="25.72849375" width="0.225" layer="94" curve="15.249051"/>
<wire x1="73.894659375" y1="25.81763125" x2="73.90078125" y2="25.77241875" width="0.225" layer="94"/>
<wire x1="73.891490625" y1="25.863140625" x2="73.894659375" y2="25.81763125" width="0.225" layer="94"/>
<wire x1="73.88921875" y1="25.97718125" x2="73.88948125" y2="25.964" width="0.225" layer="94"/>
<wire x1="73.88948125" y1="25.964" x2="73.891490625" y2="25.863140625" width="0.225" layer="94"/>
<wire x1="73.88921875" y1="26.00881875" x2="73.88921875" y2="25.97718125" width="0.225" layer="94"/>
<wire x1="73.457240625" y1="25.980109375" x2="73.457240625" y2="26.189" width="0.225" layer="94"/>
<wire x1="73.457240625" y1="26.189" x2="73.457240625" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="73.457240625" y1="25.980109375" x2="73.47523125" y2="25.724359375" width="0.225" layer="94" curve="9.672758"/>
<wire x1="73.47523125" y1="25.724359375" x2="73.559184375" y2="25.48354375" width="0.225" layer="94" curve="20.717853"/>
<wire x1="73.55918125" y1="25.483540625" x2="73.749046875" y2="25.316425" width="0.225" layer="94" curve="38.137176"/>
<wire x1="73.74905" y1="25.31643125" x2="73.998040625" y2="25.26058125" width="0.225" layer="94" curve="19.282594"/>
<wire x1="73.998040625" y1="25.26058125" x2="74.25398125" y2="25.271940625" width="0.225" layer="94" curve="11.086392"/>
<wire x1="74.25398125" y1="25.271940625" x2="74.3331" y2="25.289" width="0.225" layer="94" curve="8.168179"/>
<wire x1="74.3331" y1="25.289" x2="74.491446875" y2="25.36319375" width="0.225" layer="94" curve="17.703003"/>
<wire x1="74.49145" y1="25.363190625" x2="74.642665625" y2="25.565709375" width="0.225" layer="94" curve="38.59175"/>
<wire x1="74.64266875" y1="25.565709375" x2="74.681409375" y2="25.8181" width="0.225" layer="94" curve="17.451998"/>
<wire x1="74.681409375" y1="25.98158125" x2="74.681409375" y2="25.964" width="0.225" layer="94"/>
<wire x1="74.681409375" y1="25.964" x2="74.681409375" y2="25.8181" width="0.225" layer="94"/>
<wire x1="74.27638125" y1="25.98158125" x2="74.681409375" y2="25.98158125" width="0.225" layer="94"/>
<wire x1="73.4785" y1="26.61343125" x2="73.45896875" y2="26.414" width="0.225" layer="94" curve="6.208134"/>
<wire x1="73.45896875" y1="26.414" x2="73.4572375" y2="26.34016875" width="0.225" layer="94" curve="2.287026"/>
<wire x1="73.58028125" y1="26.86573125" x2="73.4785" y2="26.61343125" width="0.225" layer="94" curve="26.545833"/>
<wire x1="73.8009" y1="27.02155" x2="73.58028125" y2="26.86573125" width="0.225" layer="94" curve="39.047008"/>
<wire x1="74.071640625" y1="27.05881875" x2="73.8009" y2="27.02155" width="0.225" layer="94" curve="15.743809"/>
<wire x1="74.342459375" y1="27.022209375" x2="74.071640625" y2="27.058825" width="0.225" layer="94" curve="15.331635"/>
<wire x1="74.562859375" y1="26.8662" x2="74.342459375" y2="27.022209375" width="0.225" layer="94" curve="39.85306"/>
<wire x1="74.66363125" y1="26.61348125" x2="74.5628625" y2="26.866203125" width="0.225" layer="94" curve="26.085633"/>
<wire x1="74.684409375" y1="26.34016875" x2="74.663628125" y2="26.61348125" width="0.225" layer="94" curve="8.69613"/>
<wire x1="74.684409375" y1="26.00881875" x2="74.684409375" y2="26.189" width="0.225" layer="94"/>
<wire x1="74.684409375" y1="26.189" x2="74.684409375" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="73.88921875" y1="26.00881875" x2="74.684409375" y2="26.00881875" width="0.225" layer="94"/>
<wire x1="73.88921875" y1="26.422790625" x2="74.264359375" y2="26.422790625" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="25.28548125" x2="82.92818125" y2="25.28548125" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="27.03186875" x2="82.80066875" y2="27.03186875" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="27.03186875" x2="82.508140625" y2="26.864" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="26.864" x2="82.508140625" y2="26.639" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="26.639" x2="82.508140625" y2="26.414" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="26.414" x2="82.508140625" y2="26.189" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="26.189" x2="82.508140625" y2="25.964" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="25.964" x2="82.508140625" y2="25.739" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="25.739" x2="82.508140625" y2="25.514" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="25.514" x2="82.508140625" y2="25.289" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="25.289" x2="82.508140625" y2="25.28548125" width="0.225" layer="94"/>
<wire x1="83.18028125" y1="26.30266875" x2="83.585309375" y2="26.30266875" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="26.358340625" x2="82.92818125" y2="26.189" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="26.189" x2="82.92818125" y2="25.964" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="25.964" x2="82.92818125" y2="25.739" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="25.739" x2="82.92818125" y2="25.514" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="25.514" x2="82.92818125" y2="25.289" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="25.289" x2="82.92818125" y2="25.28548125" width="0.225" layer="94"/>
<wire x1="82.8883" y1="26.98991875" x2="82.80066875" y2="27.03186875" width="0.225" layer="94" curve="51.162574"/>
<wire x1="86.810090625" y1="25.28548125" x2="86.810090625" y2="25.289" width="0.225" layer="94"/>
<wire x1="86.810090625" y1="25.289" x2="86.810090625" y2="25.514" width="0.225" layer="94"/>
<wire x1="86.810090625" y1="25.514" x2="86.810090625" y2="25.739" width="0.225" layer="94"/>
<wire x1="86.810090625" y1="25.739" x2="86.810090625" y2="25.964" width="0.225" layer="94"/>
<wire x1="86.810090625" y1="25.964" x2="86.810090625" y2="26.189" width="0.225" layer="94"/>
<wire x1="86.810090625" y1="26.189" x2="86.810090625" y2="26.25023125" width="0.225" layer="94"/>
<wire x1="86.39005" y1="25.28548125" x2="86.39005" y2="25.289" width="0.225" layer="94"/>
<wire x1="86.39005" y1="25.289" x2="86.39005" y2="25.514" width="0.225" layer="94"/>
<wire x1="86.39005" y1="25.514" x2="86.39005" y2="25.739" width="0.225" layer="94"/>
<wire x1="86.39005" y1="25.739" x2="86.39005" y2="25.964" width="0.225" layer="94"/>
<wire x1="86.39005" y1="25.964" x2="86.39005" y2="26.189" width="0.225" layer="94"/>
<wire x1="86.39005" y1="26.189" x2="86.39005" y2="26.414" width="0.225" layer="94"/>
<wire x1="86.39005" y1="26.414" x2="86.39005" y2="26.639" width="0.225" layer="94"/>
<wire x1="86.39005" y1="26.639" x2="86.39005" y2="26.864" width="0.225" layer="94"/>
<wire x1="86.39005" y1="26.864" x2="86.39005" y2="27.03186875" width="0.225" layer="94"/>
<wire x1="86.39005" y1="25.28548125" x2="86.810090625" y2="25.28548125" width="0.225" layer="94"/>
<wire x1="87.1462" y1="25.28548125" x2="87.566240625" y2="25.28548125" width="0.225" layer="94"/>
<wire x1="86.68258125" y1="27.03186875" x2="86.39005" y2="27.03186875" width="0.225" layer="94"/>
<wire x1="86.770490625" y1="26.98956875" x2="86.68258125" y2="27.03186875" width="0.225" layer="94" curve="51.389654"/>
<wire x1="83.585309375" y1="26.30266875" x2="83.585309375" y2="26.414" width="0.225" layer="94"/>
<wire x1="83.585309375" y1="26.414" x2="83.585309375" y2="26.532359375" width="0.225" layer="94"/>
<wire x1="83.585309375" y1="26.532359375" x2="83.57331875" y2="26.7009" width="0.225" layer="94" curve="8.13878"/>
<wire x1="83.57331875" y1="26.7009" x2="83.52315625" y2="26.8617" width="0.225" layer="94" curve="18.374099"/>
<wire x1="83.523159375" y1="26.8617" x2="83.41396875" y2="26.9887" width="0.225" layer="94" curve="28.349931"/>
<wire x1="83.41396875" y1="26.9887" x2="83.25806875" y2="27.050721875" width="0.225" layer="94" curve="26.884991"/>
<wire x1="83.25806875" y1="27.05071875" x2="83.0895" y2="27.055371875" width="0.225" layer="94" curve="13.343084"/>
<wire x1="83.0895" y1="27.05538125" x2="82.888296875" y2="26.989925" width="0.225" layer="94" curve="25.861473"/>
<wire x1="87.566240625" y1="25.28548125" x2="87.566240625" y2="25.289" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="25.289" x2="87.566240625" y2="25.514" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="25.514" x2="87.566240625" y2="25.739" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="25.739" x2="87.566240625" y2="25.964" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="25.964" x2="87.566240625" y2="26.189" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="26.189" x2="87.566240625" y2="26.414" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="26.414" x2="87.566240625" y2="26.418390625" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="26.418390625" x2="87.555215625" y2="26.606540625" width="0.225" layer="94" curve="6.706552"/>
<wire x1="87.55521875" y1="26.606540625" x2="87.509115625" y2="26.788871875" width="0.225" layer="94" curve="14.968121"/>
<wire x1="87.509109375" y1="26.78886875" x2="87.403925" y2="26.943603125" width="0.225" layer="94" curve="25.064472"/>
<wire x1="87.40393125" y1="26.943609375" x2="87.240590625" y2="27.034540625" width="0.225" layer="94" curve="28.312195"/>
<wire x1="87.240590625" y1="27.034540625" x2="87.05421875" y2="27.058825" width="0.225" layer="94" curve="15.049352"/>
<wire x1="87.05421875" y1="27.05881875" x2="86.770490625" y2="26.98956875" width="0.225" layer="94" curve="27.231353"/>
<wire x1="87.1462" y1="25.28548125" x2="87.1462" y2="25.289" width="0.225" layer="94"/>
<wire x1="87.1462" y1="25.289" x2="87.1462" y2="25.514" width="0.225" layer="94"/>
<wire x1="87.1462" y1="25.514" x2="87.1462" y2="25.739" width="0.225" layer="94"/>
<wire x1="87.1462" y1="25.739" x2="87.1462" y2="25.964" width="0.225" layer="94"/>
<wire x1="87.1462" y1="25.964" x2="87.1462" y2="26.189" width="0.225" layer="94"/>
<wire x1="87.1462" y1="26.189" x2="87.1462" y2="26.3642" width="0.225" layer="94"/>
<wire x1="87.1462" y1="26.3642" x2="87.139771875" y2="26.502909375" width="0.225" layer="94" curve="5.307835"/>
<wire x1="87.13976875" y1="26.502909375" x2="87.123915625" y2="26.584609375" width="0.225" layer="94" curve="11.346275"/>
<wire x1="87.12391875" y1="26.584609375" x2="87.11383125" y2="26.61046875" width="0.225" layer="94" curve="9.30383"/>
<wire x1="87.11383125" y1="26.61046875" x2="87.098490625" y2="26.633490625" width="0.225" layer="94" curve="15.447128"/>
<wire x1="87.098490625" y1="26.633490625" x2="87.0752" y2="26.648190625" width="0.225" layer="94" curve="32.662799"/>
<wire x1="87.0752" y1="26.648190625" x2="86.99275" y2="26.656371875" width="0.225" layer="94" curve="20.527762"/>
<wire x1="86.99275" y1="26.65636875" x2="86.9651" y2="26.653715625" width="0.225" layer="94" curve="1.764695"/>
<wire x1="86.9651" y1="26.65371875" x2="86.912034375" y2="26.638084375" width="0.225" layer="94" curve="20.107123"/>
<wire x1="86.91203125" y1="26.638090625" x2="86.870678125" y2="26.601821875" width="0.225" layer="94" curve="29.565942"/>
<wire x1="86.87068125" y1="26.60181875" x2="86.84560625" y2="26.55240625" width="0.225" layer="94" curve="14.114289"/>
<wire x1="86.8456" y1="26.552409375" x2="86.824978125" y2="26.471759375" width="0.225" layer="94" curve="11.013017"/>
<wire x1="86.82498125" y1="26.471759375" x2="86.813053125" y2="26.36131875" width="0.225" layer="94" curve="5.344301"/>
<wire x1="86.81305" y1="26.36131875" x2="86.810090625" y2="26.25023125" width="0.225" layer="94" curve="3.932748"/>
<wire x1="83.18028125" y1="26.30266875" x2="83.18028125" y2="26.414" width="0.225" layer="94"/>
<wire x1="83.18028125" y1="26.414" x2="83.18028125" y2="26.487240625" width="0.225" layer="94"/>
<wire x1="83.18028125" y1="26.487240625" x2="83.176690625" y2="26.56661875" width="0.225" layer="94" curve="5.181151"/>
<wire x1="83.176690625" y1="26.56661875" x2="83.16605625" y2="26.625190625" width="0.225" layer="94" curve="10.220705"/>
<wire x1="83.166059375" y1="26.625190625" x2="83.12728125" y2="26.664134375" width="0.225" layer="94" curve="58.95127"/>
<wire x1="83.12728125" y1="26.66413125" x2="82.99755" y2="26.639" width="0.225" layer="94" curve="53.217192"/>
<wire x1="82.99755" y1="26.639" x2="82.95845" y2="26.593540625" width="0.225" layer="94" curve="23.452395"/>
<wire x1="82.95845" y1="26.593540625" x2="82.95161875" y2="26.574890625" width="0.225" layer="94"/>
<wire x1="82.95161875" y1="26.574890625" x2="82.946159375" y2="26.55578125" width="0.225" layer="94"/>
<wire x1="82.946159375" y1="26.55578125" x2="82.941775" y2="26.5364" width="0.225" layer="94" curve="6.402047"/>
<wire x1="82.94176875" y1="26.5364" x2="82.931515625" y2="26.45761875" width="0.225" layer="94" curve="4.264451"/>
<wire x1="82.93151875" y1="26.45761875" x2="82.930209375" y2="26.437790625" width="0.225" layer="94"/>
<wire x1="82.930209375" y1="26.437790625" x2="82.92865" y2="26.39808125" width="0.225" layer="94"/>
<wire x1="82.92865" y1="26.39808125" x2="82.92818125" y2="26.358340625" width="0.225" layer="94"/>
<wire x1="92.67961875" y1="26.22455" x2="92.54725" y2="26.31605" width="0.225" layer="94" curve="24.406928"/>
<wire x1="92.54725" y1="26.31605" x2="92.391190625" y2="26.358428125" width="0.225" layer="94" curve="14.517731"/>
<wire x1="92.391190625" y1="26.35841875" x2="92.2218" y2="26.3835" width="0.225" layer="94"/>
<wire x1="92.2218" y1="26.3835" x2="92.13726875" y2="26.39681875" width="0.225" layer="94"/>
<wire x1="92.09533125" y1="26.40531875" x2="92.13726875" y2="26.39681875" width="0.225" layer="94" curve="5.000072"/>
<wire x1="92.0546" y1="26.41821875" x2="92.0661" y2="26.414" width="0.225" layer="94" curve="2.068384"/>
<wire x1="92.0661" y1="26.414" x2="92.09533125" y2="26.405321875" width="0.225" layer="94" curve="5.150249"/>
<wire x1="92.054009375" y1="26.430409375" x2="92.054596875" y2="26.41821875" width="0.225" layer="94" curve="5.514941"/>
<wire x1="92.054009375" y1="26.430409375" x2="92.054009375" y2="26.57718125" width="0.225" layer="94"/>
<wire x1="92.05665" y1="26.61703125" x2="92.0540125" y2="26.57718125" width="0.225" layer="94" curve="7.573746"/>
<wire x1="92.061659375" y1="26.63633125" x2="92.056646875" y2="26.61703125" width="0.225" layer="94" curve="13.971436"/>
<wire x1="92.068809375" y1="26.642740625" x2="92.0616625" y2="26.636328125" width="0.225" layer="94" curve="41.625207"/>
<wire x1="92.097790625" y1="26.650090625" x2="92.068809375" y2="26.642740625" width="0.225" layer="94" curve="13.721902"/>
<wire x1="92.15758125" y1="26.653940625" x2="92.097790625" y2="26.650090625" width="0.225" layer="94" curve="7.371099"/>
<wire x1="92.15758125" y1="26.653940625" x2="92.2566" y2="26.653940625" width="0.225" layer="94"/>
<wire x1="92.29483125" y1="26.652240625" x2="92.2566" y2="26.653940625" width="0.225" layer="94" curve="5.089448"/>
<wire x1="92.32143125" y1="26.647590625" x2="92.29483125" y2="26.652240625" width="0.225" layer="94" curve="9.654072"/>
<wire x1="92.331859375" y1="26.64346875" x2="92.32143125" y2="26.6475875" width="0.225" layer="94" curve="13.639596"/>
<wire x1="92.33591875" y1="26.63303125" x2="92.33185625" y2="26.64346875" width="0.225" layer="94" curve="15.08669"/>
<wire x1="92.34036875" y1="26.6065" x2="92.335915625" y2="26.63303125" width="0.225" layer="94" curve="8.390546"/>
<wire x1="92.34215" y1="26.568390625" x2="92.340375" y2="26.6065" width="0.225" layer="94" curve="5.332675"/>
<wire x1="92.34215" y1="26.568390625" x2="92.34215" y2="26.414" width="0.225" layer="94"/>
<wire x1="92.34215" y1="26.414" x2="92.34215" y2="26.413709375" width="0.225" layer="94"/>
<wire x1="92.34215" y1="26.413709375" x2="92.759190625" y2="26.413709375" width="0.225" layer="94"/>
<wire x1="92.759190625" y1="26.413709375" x2="92.759190625" y2="26.414" width="0.225" layer="94"/>
<wire x1="92.759190625" y1="26.414" x2="92.759190625" y2="26.5892" width="0.225" layer="94"/>
<wire x1="92.759190625" y1="26.5892" x2="92.738496875" y2="26.78398125" width="0.225" layer="94" curve="12.129007"/>
<wire x1="92.738490625" y1="26.78398125" x2="92.642896875" y2="26.952528125" width="0.225" layer="94" curve="34.862523"/>
<wire x1="92.469709375" y1="27.040159375" x2="92.27461875" y2="27.058825" width="0.225" layer="94" curve="10.407762"/>
<wire x1="92.27461875" y1="27.05881875" x2="92.139559375" y2="27.05881875" width="0.225" layer="94"/>
<wire x1="92.139559375" y1="27.05881875" x2="91.94439375" y2="27.0367625" width="0.225" layer="94" curve="15.066235"/>
<wire x1="91.944390625" y1="27.03676875" x2="91.76905625" y2="26.950671875" width="0.225" layer="94" curve="24.344315"/>
<wire x1="91.769059375" y1="26.95066875" x2="91.656853125" y2="26.79153125" width="0.225" layer="94" curve="32.9752"/>
<wire x1="91.65685" y1="26.79153125" x2="91.626409375" y2="26.639" width="0.225" layer="94" curve="14.82531"/>
<wire x1="91.626409375" y1="26.639" x2="91.62503125" y2="26.59828125" width="0.225" layer="94" curve="3.873269"/>
<wire x1="91.62503125" y1="26.59828125" x2="91.62503125" y2="26.414" width="0.225" layer="94"/>
<wire x1="91.62503125" y1="26.414" x2="91.62503125" y2="26.39436875" width="0.225" layer="94"/>
<wire x1="91.62503125" y1="26.39436875" x2="91.654403125" y2="26.22111875" width="0.225" layer="94" curve="19.244585"/>
<wire x1="91.6544" y1="26.22111875" x2="91.767428125" y2="26.088775" width="0.225" layer="94" curve="42.510013"/>
<wire x1="91.76743125" y1="26.08878125" x2="91.931890625" y2="26.02583125" width="0.225" layer="94" curve="14.601117"/>
<wire x1="91.931890625" y1="26.02583125" x2="92.105790625" y2="25.992246875" width="0.225" layer="94" curve="5.428189"/>
<wire x1="92.105790625" y1="25.99225" x2="92.16531875" y2="25.98243125" width="0.225" layer="94"/>
<wire x1="92.16531875" y1="25.98243125" x2="92.22491875" y2="25.97266875" width="0.225" layer="94"/>
<wire x1="92.22491875" y1="25.97266875" x2="92.28428125" y2="25.96156875" width="0.225" layer="94"/>
<wire x1="92.327340625" y1="25.94943125" x2="92.28428125" y2="25.9615625" width="0.225" layer="94" curve="10.274433"/>
<wire x1="92.34125" y1="25.94243125" x2="92.32734375" y2="25.9494375" width="0.225" layer="94" curve="11.730311"/>
<wire x1="92.346959375" y1="25.91516875" x2="92.34124375" y2="25.942428125" width="0.225" layer="94" curve="13.906979"/>
<wire x1="92.34815" y1="25.887240625" x2="92.346959375" y2="25.91516875" width="0.225" layer="94" curve="4.888175"/>
<wire x1="92.34815" y1="25.887240625" x2="92.34815" y2="25.7431" width="0.225" layer="94"/>
<wire x1="92.346759375" y1="25.71286875" x2="92.348153125" y2="25.7431" width="0.225" layer="94" curve="5.283856"/>
<wire x1="92.34045" y1="25.683359375" x2="92.34675625" y2="25.71286875" width="0.225" layer="94" curve="13.55484"/>
<wire x1="92.333959375" y1="25.67608125" x2="92.340453125" y2="25.683359375" width="0.225" layer="94" curve="45.791754"/>
<wire x1="92.314840625" y1="25.66976875" x2="92.333959375" y2="25.676084375" width="0.225" layer="94" curve="14.183972"/>
<wire x1="92.274840625" y1="25.66463125" x2="92.314840625" y2="25.669771875" width="0.225" layer="94" curve="7.7277"/>
<wire x1="92.24416875" y1="25.663709375" x2="92.274840625" y2="25.664628125" width="0.225" layer="94" curve="3.484382"/>
<wire x1="92.24416875" y1="25.663709375" x2="92.1096" y2="25.663709375" width="0.225" layer="94"/>
<wire x1="92.07173125" y1="25.66515" x2="92.1096" y2="25.66370625" width="0.225" layer="94" curve="4.363033"/>
<wire x1="92.04318125" y1="25.66956875" x2="92.07173125" y2="25.66514375" width="0.225" layer="94" curve="8.889144"/>
<wire x1="92.03463125" y1="25.672309375" x2="92.04318125" y2="25.66956875" width="0.225" layer="94" curve="9.043139"/>
<wire x1="92.03186875" y1="25.680859375" x2="92.034628125" y2="25.672309375" width="0.225" layer="94" curve="7.986668"/>
<wire x1="92.02805" y1="25.70145" x2="92.031871875" y2="25.680859375" width="0.225" layer="94" curve="6.761788"/>
<wire x1="92.02548125" y1="25.73128125" x2="92.028053125" y2="25.70145" width="0.225" layer="94" curve="4.406702"/>
<wire x1="92.024059375" y1="25.79115" x2="92.025484375" y2="25.73128125" width="0.225" layer="94" curve="2.724283"/>
<wire x1="92.024059375" y1="25.79115" x2="92.024059375" y2="25.964" width="0.225" layer="94"/>
<wire x1="92.024059375" y1="25.964" x2="92.024059375" y2="25.99651875" width="0.225" layer="94"/>
<wire x1="92.024059375" y1="25.99651875" x2="91.60701875" y2="25.99651875" width="0.225" layer="94"/>
<wire x1="91.60701875" y1="25.99651875" x2="91.60701875" y2="25.964" width="0.225" layer="94"/>
<wire x1="91.60701875" y1="25.964" x2="91.60701875" y2="25.739" width="0.225" layer="94"/>
<wire x1="91.60701875" y1="25.739" x2="91.60701875" y2="25.728159375" width="0.225" layer="94"/>
<wire x1="91.60701875" y1="25.728159375" x2="91.6277125" y2="25.53338125" width="0.225" layer="94" curve="12.128778"/>
<wire x1="91.627709375" y1="25.53338125" x2="91.72330625" y2="25.364828125" width="0.225" layer="94" curve="34.863434"/>
<wire x1="91.723309375" y1="25.36483125" x2="91.85098125" y2="25.289" width="0.225" layer="94" curve="24.602823"/>
<wire x1="91.85098125" y1="25.289" x2="91.896490625" y2="25.277203125" width="0.225" layer="94" curve="7.735491"/>
<wire x1="91.896490625" y1="25.2772" x2="92.091590625" y2="25.258534375" width="0.225" layer="94" curve="10.40831"/>
<wire x1="92.091590625" y1="25.25853125" x2="92.262609375" y2="25.25853125" width="0.225" layer="94"/>
<wire x1="92.262609375" y1="25.25853125" x2="92.457790625" y2="25.2805875" width="0.225" layer="94" curve="15.067174"/>
<wire x1="92.457790625" y1="25.280590625" x2="92.633140625" y2="25.36668125" width="0.225" layer="94" curve="24.336812"/>
<wire x1="92.633140625" y1="25.36668125" x2="92.74113125" y2="25.514" width="0.225" layer="94" curve="30.878359"/>
<wire x1="92.74113125" y1="25.514" x2="92.74536875" y2="25.525809375" width="0.225" layer="94" curve="2.095521"/>
<wire x1="92.74536875" y1="25.525809375" x2="92.777203125" y2="25.71908125" width="0.225" layer="94" curve="18.707757"/>
<wire x1="92.777209375" y1="25.71908125" x2="92.777209375" y2="25.739" width="0.225" layer="94"/>
<wire x1="92.777209375" y1="25.739" x2="92.777209375" y2="25.92328125" width="0.225" layer="94"/>
<wire x1="92.777209375" y1="25.92328125" x2="92.756978125" y2="26.083621875" width="0.225" layer="94" curve="14.383055"/>
<wire x1="92.75696875" y1="26.08361875" x2="92.6796125" y2="26.22454375" width="0.225" layer="94" curve="28.759193"/>
<wire x1="72.94583125" y1="31.589" x2="72.950859375" y2="31.589" width="0.225" layer="94"/>
<wire x1="73.958459375" y1="26.639" x2="74.19638125" y2="26.639" width="0.225" layer="94"/>
<wire x1="92.06346875" y1="26.639" x2="92.334359375" y2="26.639" width="0.225" layer="94"/>
<wire x1="77.24345" y1="26.639" x2="77.48136875" y2="26.639" width="0.225" layer="94"/>
<wire x1="86.913890625" y1="26.639" x2="87.09276875" y2="26.639" width="0.225" layer="94"/>
<wire x1="82.99755" y1="26.639" x2="83.160159375" y2="26.639" width="0.225" layer="94"/>
<wire x1="92.0661" y1="26.414" x2="92.34215" y2="26.414" width="0.225" layer="94"/>
<wire x1="82.92926875" y1="26.414" x2="83.18028125" y2="26.414" width="0.225" layer="94"/>
<wire x1="73.88948125" y1="25.964" x2="74.27638125" y2="25.964" width="0.225" layer="94"/>
<wire x1="92.024059375" y1="25.964" x2="92.27126875" y2="25.964" width="0.225" layer="94"/>
<wire x1="77.17446875" y1="25.964" x2="77.561359375" y2="25.964" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="25.964" x2="81.341240625" y2="25.964" width="0.225" layer="94"/>
<wire x1="73.90876875" y1="25.739" x2="74.2678" y2="25.739" width="0.225" layer="94"/>
<wire x1="92.025140625" y1="25.739" x2="92.34813125" y2="25.739" width="0.225" layer="94"/>
<wire x1="77.193759375" y1="25.739" x2="77.55278125" y2="25.739" width="0.225" layer="94"/>
<wire x1="81.209909375" y1="25.739" x2="81.340009375" y2="25.739" width="0.225" layer="94"/>
<rectangle x1="93.287040625" y1="29.45076875" x2="93.399540625" y2="33.34871875" layer="94"/>
<rectangle x1="94.749840625" y1="29.45076875" x2="94.862340625" y2="33.34871875" layer="94"/>
<rectangle x1="73.776721875" y1="26.310290625" x2="74.376859375" y2="26.422790625" layer="94"/>
<rectangle x1="77.0617" y1="26.310290625" x2="77.66185" y2="26.422790625" layer="94"/>
<rectangle x1="73.77671875" y1="26.00881875" x2="74.685" y2="26.12131875" layer="94"/>
<rectangle x1="77.0617" y1="26.00881875" x2="77.985" y2="26.12131875" layer="94"/>
<text x="50.8" y="12.7" size="5.08" layer="94" ratio="10" align="bottom-center">&gt;VALUE</text>
<text x="1.27" y="33.02" size="1.9304" layer="94">Vehicle Name:</text>
<text x="41.91" y="30.48" size="5.08" layer="94" align="top-center">&gt;VEHICLE_NUMBER</text>
<text x="41.91" y="33.02" size="2.54" layer="94" align="bottom-center">&gt;VEHICLE_NAME</text>
<text x="1.27" y="21.59" size="1.9304" layer="94" align="top-left">Sheet Name:</text>
<text x="1.27" y="24.13" size="1.9304" layer="94">ABRA:</text>
<text x="10.16" y="24.13" size="1.9304" layer="94">&gt;ABRA_GLOBAL</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="DINA3_L-AUTO" prefix="FRAME" uservalue="yes">
<description>&lt;b&gt;VEIT FRAME&lt;/b&gt;&lt;p&gt;
DIN A3, landscape with extra doc field,&lt;br&gt;
customized for vehicle schematics</description>
<gates>
<gate name="G$1" symbol="DINA3_L" x="0" y="0"/>
<gate name="G$2" symbol="DOCFIELD-AUTA" x="287.02" y="0" addlevel="request"/>
<gate name="G$3" symbol="DOCFIELD-AUTA-ECO-LOGO" x="388.62" y="0" addlevel="request"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SUPPLY2">
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.27" y1="2.54" x2="1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="0" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<text x="-1.905" y="-0.635" size="1.524" layer="94">GND</text>
<pin name="GND" x="0" y="5.08" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="V--&gt;">
<wire x1="15.24" y1="0" x2="13.97" y2="1.27" width="0.1524" layer="94"/>
<wire x1="13.97" y1="1.27" x2="0" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="1.27" x2="0" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.27" x2="13.97" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="15.24" y1="0" x2="13.97" y2="-1.27" width="0.1524" layer="94"/>
<text x="1.27" y="-0.762" size="1.524" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="PE">
<wire x1="-1.905" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-0.762" x2="1.27" y2="-0.762" width="0.254" layer="94"/>
<wire x1="-0.635" y1="-1.524" x2="0.635" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<text x="-0.381" y="0.635" size="1.524" layer="94" rot="R90">PE</text>
<pin name="PE" x="0" y="5.08" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="AKU">
<wire x1="-0.635" y1="3.81" x2="0.635" y2="3.81" width="0.1524" layer="94"/>
<wire x1="0" y1="3.175" x2="0" y2="4.445" width="0.1524" layer="94"/>
<circle x="0" y="3.81" radius="1.27" width="0.254" layer="94"/>
<text x="-1.778" y="5.715" size="1.524" layer="94">AKU</text>
<pin name="AKU" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND">
<gates>
<gate name="GND" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="V--&gt;" prefix="V" uservalue="yes">
<gates>
<gate name="G$1" symbol="V--&gt;" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PE">
<gates>
<gate name="PE" symbol="PE" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="AKU">
<gates>
<gate name="G$1" symbol="AKU" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SupplyVeit">
<packages>
</packages>
<symbols>
<symbol name="CHASSIS">
<wire x1="-1.905" y1="0" x2="-1.27" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-0.3175" y2="0" width="0.254" layer="94"/>
<wire x1="-0.3175" y1="0" x2="0.635" y2="0" width="0.254" layer="94"/>
<wire x1="0.635" y1="0" x2="1.5875" y2="0" width="0.254" layer="94"/>
<wire x1="1.5875" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<wire x1="-1.27" y1="0" x2="-1.905" y2="-0.9525" width="0.254" layer="94"/>
<wire x1="-0.3175" y1="0" x2="-0.9525" y2="-0.9525" width="0.254" layer="94"/>
<wire x1="0.635" y1="0" x2="0" y2="-0.9525" width="0.254" layer="94"/>
<wire x1="1.5875" y1="0" x2="0.9525" y2="-0.9525" width="0.254" layer="94"/>
<pin name="CHASSIS" x="0" y="2.54" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="CHASSIS" prefix="CHASSIS">
<gates>
<gate name="G$1" symbol="CHASSIS" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="DEUTZ">
<packages>
</packages>
<symbols>
<symbol name="DEUTZ-SILOVKA">
<wire x1="-20.32" y1="12.7" x2="-20.32" y2="-15.24" width="0.254" layer="94"/>
<wire x1="-20.32" y1="-15.24" x2="22.86" y2="-15.24" width="0.254" layer="94"/>
<wire x1="22.86" y1="-15.24" x2="22.86" y2="12.7" width="0.254" layer="94"/>
<wire x1="22.86" y1="12.7" x2="-20.32" y2="12.7" width="0.254" layer="94"/>
<text x="0" y="0" size="1.778" layer="94" align="center">SILOVKA DEUTZ
(starter, alternator, ...)</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="DEUTZ-SILOVKA">
<gates>
<gate name="G$1" symbol="DEUTZ-SILOVKA" x="0" y="0" addlevel="can"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Elektrické_stroje">
<packages>
</packages>
<symbols>
<symbol name="SERIES-STAR">
<wire x1="0" y1="20.955" x2="0" y2="15.875" width="1.016" layer="94"/>
<wire x1="0" y1="9.525" x2="0" y2="4.445" width="1.016" layer="94"/>
<wire x1="-8.255" y1="-4.7625" x2="-3.81" y2="-2.2225" width="1.016" layer="94"/>
<wire x1="-18.0975" y1="-10.4775" x2="-13.6525" y2="-7.9375" width="1.016" layer="94"/>
<wire x1="3.81" y1="-2.2225" x2="8.255" y2="-4.7625" width="1.016" layer="94"/>
<wire x1="13.6525" y1="-7.9375" x2="18.0975" y2="-10.4775" width="1.016" layer="94"/>
<wire x1="0" y1="0" x2="-21.844" y2="-12.7" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="21.844" y2="-12.7" width="0.1524" layer="94"/>
<wire x1="0" y1="25.4" x2="0" y2="0" width="0.1524" layer="94"/>
<circle x="0" y="0" radius="0.254" width="0.508" layer="94"/>
<circle x="0" y="25.4" radius="0.254" width="0.508" layer="94"/>
<circle x="0" y="12.7" radius="0.254" width="0.508" layer="94"/>
<circle x="-21.844" y="-12.7" radius="0.254" width="0.508" layer="94"/>
<circle x="21.844" y="-12.7" radius="0.254" width="0.508" layer="94"/>
<circle x="10.922" y="-6.35" radius="0.254" width="0.508" layer="94"/>
<circle x="-10.922" y="-6.35" radius="0.254" width="0.508" layer="94"/>
<text x="-2.54" y="25.4" size="1.778" layer="94" align="center">1</text>
<text x="-2.54" y="15.24" size="1.778" layer="94" align="center">2</text>
<text x="-2.54" y="11.43" size="1.778" layer="94" align="center">3</text>
<text x="-20.32" y="-8.89" size="1.778" layer="94" align="center">9</text>
<text x="-13.97" y="-5.08" size="1.778" layer="94" align="center">10</text>
<text x="-10.16" y="-2.54" size="1.778" layer="94" align="center">11</text>
<text x="1.27" y="-3.81" size="1.778" layer="94" align="center">8</text>
<text x="7.62" y="-7.62" size="1.778" layer="94" align="center">7</text>
<text x="10.795" y="-9.525" size="1.778" layer="94" align="center">6</text>
<text x="17.145" y="-13.335" size="1.778" layer="94" align="center">5</text>
<text x="-2.54" y="2.54" size="1.778" layer="94" align="center">4</text>
<text x="-5.08" y="0" size="1.778" layer="94" align="center">12</text>
<wire x1="0" y1="0" x2="5.08" y2="5.08" width="0.1524" layer="94"/>
<wire x1="5.08" y1="5.08" x2="12.7" y2="5.08" width="0.1524" layer="94"/>
<circle x="12.7" y="5.08" radius="0.254" width="0.508" layer="94"/>
<text x="15.24" y="5.08" size="1.778" layer="94" align="center">N</text>
<text x="0" y="30.48" size="1.778" layer="94" align="center">L1</text>
<text x="25.4" y="-15.24" size="1.778" layer="94" align="center">L2</text>
<text x="-25.4" y="-15.24" size="1.778" layer="94" align="center">L3</text>
<text x="-20.32" y="7.62" size="1.778" layer="94" align="center">Zapojení: SERIES STAR</text>
</symbol>
<symbol name="PARALLEL-STAR">
<circle x="0" y="0" radius="0.254" width="0.508" layer="94"/>
<circle x="0" y="25.4" radius="0.254" width="0.508" layer="94"/>
<circle x="-21.844" y="-12.7" radius="0.254" width="0.508" layer="94"/>
<circle x="21.844" y="-12.7" radius="0.254" width="0.508" layer="94"/>
<text x="-2.54" y="20.32" size="1.778" layer="94" align="center">1</text>
<text x="-2.54" y="5.08" size="1.778" layer="94" align="center">2</text>
<text x="2.54" y="20.32" size="1.778" layer="94" align="center">3</text>
<text x="-16.51" y="-12.7" size="1.778" layer="94" align="center">9</text>
<text x="-3.81" y="-5.08" size="1.778" layer="94" align="center">10</text>
<text x="-19.05" y="-7.62" size="1.778" layer="94" align="center">11</text>
<text x="3.81" y="-5.08" size="1.778" layer="94" align="center">8</text>
<text x="16.51" y="-12.7" size="1.778" layer="94" align="center">7</text>
<text x="6.35" y="0" size="1.778" layer="94" align="center">6</text>
<text x="19.05" y="-7.62" size="1.778" layer="94" align="center">5</text>
<text x="2.54" y="5.08" size="1.778" layer="94" align="center">4</text>
<text x="-6.35" y="0" size="1.778" layer="94" align="center">12</text>
<wire x1="0" y1="0" x2="5.08" y2="5.08" width="0.1524" layer="94"/>
<wire x1="5.08" y1="5.08" x2="12.7" y2="5.08" width="0.1524" layer="94"/>
<circle x="12.7" y="5.08" radius="0.254" width="0.508" layer="94"/>
<text x="15.24" y="5.08" size="1.778" layer="94" align="center">N</text>
<text x="0" y="30.48" size="1.778" layer="94" align="center">L1</text>
<text x="25.4" y="-15.24" size="1.778" layer="94" align="center">L2</text>
<text x="-25.4" y="-15.24" size="1.778" layer="94" align="center">L3</text>
<wire x1="-9.2075" y1="-3.175" x2="-14.605" y2="-6.35" width="1.016" layer="94"/>
<wire x1="-7.3025" y1="-6.35" x2="-12.7" y2="-9.525" width="1.016" layer="94"/>
<wire x1="7.3025" y1="-6.35" x2="12.7" y2="-9.525" width="1.016" layer="94"/>
<wire x1="9.2075" y1="-3.175" x2="14.605" y2="-6.35" width="1.016" layer="94"/>
<wire x1="-7.62" y1="-2.2225" x2="-6.6675" y2="-3.81" width="0.1524" layer="94"/>
<wire x1="-6.6675" y1="-3.81" x2="-5.715" y2="-5.3975" width="0.1524" layer="94"/>
<wire x1="-14.2875" y1="-10.4775" x2="-15.24" y2="-8.89" width="0.1524" layer="94"/>
<wire x1="-15.24" y1="-8.89" x2="-16.1925" y2="-7.3025" width="0.1524" layer="94"/>
<wire x1="5.715" y1="-5.3975" x2="6.6675" y2="-3.81" width="0.1524" layer="94"/>
<wire x1="6.6675" y1="-3.81" x2="7.62" y2="-2.2225" width="0.1524" layer="94"/>
<wire x1="14.2875" y1="-10.4775" x2="15.24" y2="-8.89" width="0.1524" layer="94"/>
<wire x1="15.24" y1="-8.89" x2="16.1925" y2="-7.3025" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-2.2225" x2="16.1925" y2="-7.3025" width="0.1524" layer="94"/>
<wire x1="14.2875" y1="-10.4775" x2="5.715" y2="-5.3975" width="0.1524" layer="94"/>
<wire x1="-16.1925" y1="-7.3025" x2="-7.62" y2="-2.2225" width="0.1524" layer="94"/>
<wire x1="-5.715" y1="-5.3975" x2="-14.2875" y2="-10.4775" width="0.1524" layer="94"/>
<circle x="-6.6675" y="-3.81" radius="0.254" width="0.508" layer="94"/>
<circle x="-15.24" y="-8.89" radius="0.254" width="0.508" layer="94"/>
<circle x="6.6675" y="-3.81" radius="0.254" width="0.508" layer="94"/>
<circle x="15.24" y="-8.89" radius="0.254" width="0.508" layer="94"/>
<wire x1="1.905" y1="15.875" x2="1.905" y2="9.525" width="1.016" layer="94"/>
<wire x1="-1.905" y1="15.875" x2="-1.905" y2="9.525" width="1.016" layer="94"/>
<wire x1="-1.905" y1="7.62" x2="0" y2="7.62" width="0.1524" layer="94"/>
<wire x1="0" y1="7.62" x2="1.905" y2="7.62" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="17.78" x2="0" y2="17.78" width="0.1524" layer="94"/>
<circle x="0" y="17.78" radius="0.254" width="0.508" layer="94"/>
<circle x="0" y="7.62" radius="0.254" width="0.508" layer="94"/>
<wire x1="0" y1="17.78" x2="1.905" y2="17.78" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="7.62" x2="-1.905" y2="17.78" width="0.1524" layer="94"/>
<wire x1="1.905" y1="17.78" x2="1.905" y2="7.62" width="0.1524" layer="94"/>
<wire x1="0" y1="25.4" x2="0" y2="17.78" width="0.1524" layer="94"/>
<wire x1="0" y1="7.62" x2="0" y2="0" width="0.1524" layer="94"/>
<wire x1="-15.24" y1="-8.89" x2="-21.844" y2="-12.7" width="0.1524" layer="94"/>
<wire x1="15.24" y1="-8.89" x2="21.844" y2="-12.7" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="-6.6675" y2="-3.81" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="6.6675" y2="-3.81" width="0.1524" layer="94"/>
<text x="-20.32" y="7.62" size="1.778" layer="94" align="center">Zapojení: PARALLEL STAR</text>
</symbol>
<symbol name="SVORKOVNICE-SS">
<pin name="L1" x="-15.24" y="-12.7" visible="off" direction="pas" rot="R90"/>
<pin name="L2" x="-7.62" y="-12.7" visible="off" direction="pas" rot="R90"/>
<pin name="L3" x="0" y="-12.7" visible="off" direction="pas" rot="R90"/>
<circle x="-7.62" y="-3.81" radius="1.27" width="0.254" layer="94"/>
<circle x="0" y="-3.81" radius="1.27" width="0.254" layer="94"/>
<circle x="-15.24" y="-3.81" radius="1.27" width="0.254" layer="94"/>
<circle x="-15.24" y="3.81" radius="1.27" width="0.254" layer="94"/>
<circle x="-7.62" y="3.81" radius="1.27" width="0.254" layer="94"/>
<circle x="0" y="3.81" radius="1.27" width="0.254" layer="94"/>
<circle x="12.7" y="0" radius="1.27" width="0.254" layer="94"/>
<text x="-15.24" y="-1.27" size="1.778" layer="94" align="center">1</text>
<text x="-7.62" y="-1.27" size="1.778" layer="94" align="center">5</text>
<text x="0" y="-1.27" size="1.778" layer="94" align="center">9</text>
<text x="0" y="6.35" size="1.778" layer="94" align="center">6-7</text>
<text x="-7.62" y="6.35" size="1.778" layer="94" align="center">2-3</text>
<text x="-15.24" y="6.35" size="1.778" layer="94" align="center">10-11</text>
<pin name="N" x="7.62" y="-12.7" visible="off" direction="pas" rot="R90"/>
<wire x1="7.62" y1="-5.08" x2="7.62" y2="0" width="0.1524" layer="94"/>
<wire x1="-20.32" y1="10.16" x2="-20.32" y2="-10.16" width="0.4064" layer="94"/>
<wire x1="-20.32" y1="-10.16" x2="17.78" y2="-10.16" width="0.4064" layer="94"/>
<wire x1="17.78" y1="-10.16" x2="17.78" y2="10.16" width="0.4064" layer="94"/>
<wire x1="17.78" y1="10.16" x2="-20.32" y2="10.16" width="0.4064" layer="94"/>
<text x="-17.78" y="-12.7" size="1.778" layer="94" align="center">L1</text>
<text x="-10.16" y="-12.7" size="1.778" layer="94" align="center">L2</text>
<text x="-2.54" y="-12.7" size="1.778" layer="94" align="center">L3</text>
<text x="5.08" y="-12.7" size="1.778" layer="94" align="center">N</text>
<polygon width="0.1524" layer="94" spacing="0.254">
<vertex x="-8.255" y="-6.985"/>
<vertex x="-6.985" y="-6.985"/>
<vertex x="-7.62" y="-8.89"/>
</polygon>
<polygon width="0.1524" layer="94" spacing="0.254">
<vertex x="-0.635" y="-6.985"/>
<vertex x="0.635" y="-6.985"/>
<vertex x="0" y="-8.89"/>
</polygon>
<polygon width="0.1524" layer="94" spacing="0.254">
<vertex x="6.985" y="-6.985"/>
<vertex x="8.255" y="-6.985"/>
<vertex x="7.62" y="-8.89"/>
</polygon>
<polygon width="0.1524" layer="94" spacing="0.254">
<vertex x="-15.875" y="-6.985"/>
<vertex x="-14.605" y="-6.985"/>
<vertex x="-15.24" y="-8.89"/>
</polygon>
<text x="-1.27" y="12.7" size="1.778" layer="94" align="center">Zapojení: SERIES STAR</text>
<wire x1="11.43" y1="0" x2="7.62" y2="0" width="0.1524" layer="94"/>
<text x="12.7" y="2.54" size="1.778" layer="94" align="center">4-8-12</text>
</symbol>
<symbol name="SVORKOVNICE-PS">
<pin name="L1" x="-15.24" y="-12.7" visible="off" direction="pas" rot="R90"/>
<pin name="L2" x="-7.62" y="-12.7" visible="off" direction="pas" rot="R90"/>
<pin name="L3" x="0" y="-12.7" visible="off" direction="pas" rot="R90"/>
<circle x="-7.62" y="-3.81" radius="1.27" width="0.254" layer="94"/>
<circle x="0" y="-3.81" radius="1.27" width="0.254" layer="94"/>
<circle x="-15.24" y="-3.81" radius="1.27" width="0.254" layer="94"/>
<circle x="-15.24" y="3.81" radius="1.27" width="0.254" layer="94"/>
<circle x="-7.62" y="3.81" radius="1.27" width="0.254" layer="94"/>
<circle x="0" y="3.81" radius="1.27" width="0.254" layer="94"/>
<circle x="12.7" y="0" radius="1.27" width="0.254" layer="94"/>
<text x="-15.24" y="-1.27" size="1.778" layer="94" align="center">1-3</text>
<text x="-7.62" y="-1.27" size="1.778" layer="94" align="center">5-7</text>
<text x="0" y="-1.27" size="1.778" layer="94" align="center">9-11</text>
<text x="0" y="6.35" size="1.778" layer="94" align="center">6-8</text>
<text x="-7.62" y="6.35" size="1.778" layer="94" align="center">2-4</text>
<text x="-15.24" y="6.35" size="1.778" layer="94" align="center">10-12</text>
<pin name="N" x="7.62" y="-12.7" visible="off" direction="pas" rot="R90"/>
<wire x1="7.62" y1="-5.08" x2="7.62" y2="3.81" width="0.1524" layer="94"/>
<wire x1="7.62" y1="3.81" x2="1.27" y2="3.81" width="0.1524" layer="94"/>
<wire x1="-15.24" y1="5.08" x2="0" y2="5.08" width="0.254" layer="94"/>
<wire x1="-15.24" y1="2.54" x2="0" y2="2.54" width="0.254" layer="94"/>
<wire x1="-20.32" y1="10.16" x2="-20.32" y2="-10.16" width="0.4064" layer="94"/>
<wire x1="-20.32" y1="-10.16" x2="17.78" y2="-10.16" width="0.4064" layer="94"/>
<wire x1="17.78" y1="-10.16" x2="17.78" y2="10.16" width="0.4064" layer="94"/>
<wire x1="17.78" y1="10.16" x2="-20.32" y2="10.16" width="0.4064" layer="94"/>
<text x="-17.78" y="-12.7" size="1.778" layer="94" align="center">L1</text>
<text x="-10.16" y="-12.7" size="1.778" layer="94" align="center">L2</text>
<text x="-2.54" y="-12.7" size="1.778" layer="94" align="center">L3</text>
<text x="5.08" y="-12.7" size="1.778" layer="94" align="center">N</text>
<polygon width="0.1524" layer="94" spacing="0.254">
<vertex x="-8.255" y="-6.985"/>
<vertex x="-6.985" y="-6.985"/>
<vertex x="-7.62" y="-8.89"/>
</polygon>
<polygon width="0.1524" layer="94" spacing="0.254">
<vertex x="-0.635" y="-6.985"/>
<vertex x="0.635" y="-6.985"/>
<vertex x="0" y="-8.89"/>
</polygon>
<polygon width="0.1524" layer="94" spacing="0.254">
<vertex x="6.985" y="-6.985"/>
<vertex x="8.255" y="-6.985"/>
<vertex x="7.62" y="-8.89"/>
</polygon>
<polygon width="0.1524" layer="94" spacing="0.254">
<vertex x="-15.875" y="-6.985"/>
<vertex x="-14.605" y="-6.985"/>
<vertex x="-15.24" y="-8.89"/>
</polygon>
<text x="-1.27" y="12.7" size="1.778" layer="94" align="center">Zapojení: PARALLEL STAR</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="GENERATOR" prefix="GEN">
<gates>
<gate name="G$1" symbol="SERIES-STAR" x="0" y="0" addlevel="can"/>
<gate name="G$2" symbol="PARALLEL-STAR" x="76.2" y="0" addlevel="can"/>
<gate name="G$3" symbol="SVORKOVNICE-SS" x="0" y="-38.1" addlevel="can"/>
<gate name="G$4" symbol="SVORKOVNICE-PS" x="76.2" y="-38.1" addlevel="can"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name="">
<attribute name="_EXTERNAL_" value=""/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
<attribute name="ABRA_GLOBAL" value="doplnit č.s.k.!"/>
<attribute name="VEHICLE_NAME" value=""/>
<attribute name="VEHICLE_NUMBER" value=""/>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="FRAME5" library="VEIT_logotyp" deviceset="DINA3_L-AUTO" device="" value="BackUp Unit"/>
<part name="V121" library="SUPPLY2" deviceset="V--&gt;" device="" value="AKU"/>
<part name="U$271" library="SUPPLY2" deviceset="GND" device=""/>
<part name="U$272" library="SUPPLY2" deviceset="AKU" device=""/>
<part name="GEN1" library="Elektrické_stroje" deviceset="GENERATOR" device=""/>
<part name="V3" library="SUPPLY2" deviceset="V--&gt;" device="" value="GENERATOR"/>
<part name="V6" library="SUPPLY2" deviceset="V--&gt;" device="" value="AKU"/>
<part name="U$1" library="SUPPLY2" deviceset="GND" device=""/>
<part name="U$2" library="SUPPLY2" deviceset="AKU" device=""/>
<part name="V7" library="SUPPLY2" deviceset="V--&gt;" device="" value="GENERATOR"/>
<part name="U$3" library="SUPPLY2" deviceset="PE" device=""/>
<part name="CHASSIS5" library="SupplyVeit" deviceset="CHASSIS" device=""/>
<part name="U$5" library="SUPPLY2" deviceset="PE" device=""/>
<part name="CHASSIS15" library="SupplyVeit" deviceset="CHASSIS" device=""/>
<part name="U$6" library="DEUTZ" deviceset="DEUTZ-SILOVKA" device=""/>
<part name="U$7" library="DEUTZ" deviceset="DEUTZ-SILOVKA" device=""/>
<part name="U$8" library="SUPPLY2" deviceset="AKU" device=""/>
<part name="U$9" library="SUPPLY2" deviceset="GND" device=""/>
<part name="U$10" library="SUPPLY2" deviceset="AKU" device=""/>
<part name="U$11" library="SUPPLY2" deviceset="GND" device=""/>
<part name="V8" library="SUPPLY2" deviceset="V--&gt;" device="" value="DEUTZ"/>
<part name="V11" library="SUPPLY2" deviceset="V--&gt;" device="" value="DEUTZ"/>
</parts>
<sheets>
<sheet>
<plain>
<text x="368.3" y="254" size="2.54" layer="95" align="top-center">MACHINE ROOM</text>
<wire x1="353.06" y1="256.54" x2="383.54" y2="256.54" width="0.1524" layer="94" style="longdash"/>
<wire x1="353.06" y1="243.84" x2="383.54" y2="243.84" width="0.1524" layer="94" style="longdash"/>
<wire x1="383.54" y1="256.54" x2="383.54" y2="243.84" width="0.1524" layer="94" style="longdash"/>
<wire x1="353.06" y1="256.54" x2="353.06" y2="243.84" width="0.1524" layer="94" style="longdash"/>
<wire x1="193.04" y1="264.16" x2="193.04" y2="0" width="0.1524" layer="94" style="longdash"/>
<text x="96.52" y="259.08" size="6.4516" layer="94" align="top-center">EU 3x400V~50Hz</text>
<text x="292.1" y="259.08" size="6.4516" layer="94" align="top-center">USA 3x208V~60Hz</text>
<text x="109.22" y="154.94" size="2.54" layer="95" rot="MR0">400VAC</text>
<text x="20.32" y="254" size="2.54" layer="95" rot="MR0" align="top-center">MACHINE ROOM</text>
<wire x1="35.56" y1="256.54" x2="5.08" y2="256.54" width="0.1524" layer="94" style="longdash"/>
<wire x1="35.56" y1="243.84" x2="5.08" y2="243.84" width="0.1524" layer="94" style="longdash"/>
<wire x1="5.08" y1="256.54" x2="5.08" y2="243.84" width="0.1524" layer="94" style="longdash"/>
<wire x1="35.56" y1="256.54" x2="35.56" y2="243.84" width="0.1524" layer="94" style="longdash"/>
<text x="276.86" y="154.94" size="2.54" layer="95">208VAC</text>
<text x="353.06" y="190.5" size="1.778" layer="94">Propojka 60 Hz</text>
<text x="358.14" y="96.52" size="2.54" layer="95" align="top-center">JUNCTION BOX 3x400VAC</text>
<wire x1="332.74" y1="99.06" x2="383.54" y2="99.06" width="0.1524" layer="94" style="longdash"/>
<wire x1="332.74" y1="86.36" x2="383.54" y2="86.36" width="0.1524" layer="94" style="longdash"/>
<wire x1="332.74" y1="86.36" x2="332.74" y2="99.06" width="0.1524" layer="94" style="longdash"/>
<wire x1="383.54" y1="86.36" x2="383.54" y2="99.06" width="0.1524" layer="94" style="longdash"/>
<text x="30.48" y="96.52" size="2.54" layer="95" rot="MR0" align="top-center">JUNCTION BOX 3x400VAC</text>
<wire x1="55.88" y1="99.06" x2="5.08" y2="99.06" width="0.1524" layer="94" style="longdash"/>
<wire x1="55.88" y1="86.36" x2="5.08" y2="86.36" width="0.1524" layer="94" style="longdash"/>
<wire x1="55.88" y1="86.36" x2="55.88" y2="99.06" width="0.1524" layer="94" style="longdash"/>
<wire x1="5.08" y1="86.36" x2="5.08" y2="99.06" width="0.1524" layer="94" style="longdash"/>
<text x="121.92" y="147.32" size="1.778" layer="98">Specifikovat kabel (MELE) a barvy</text>
<text x="238.76" y="147.32" size="1.778" layer="98">Specifikovat kabel (MELE) a barvy</text>
<text x="342.9" y="231.14" size="1.778" layer="98">Specifikovat průřezy a barvy</text>
<text x="10.16" y="233.68" size="1.778" layer="98">Specifikovat průřezy a barvy</text>
<text x="322.58" y="81.28" size="1.778" layer="98">Specifikovat průřezy a barvy</text>
<text x="33.02" y="81.28" size="1.778" layer="98">Specifikovat průřezy a barvy</text>
<text x="259.08" y="180.34" size="1.778" layer="94" align="center-left">MECC ALTE AC Generator (ECP28-VL/ 4A)</text>
<text x="121.92" y="180.34" size="1.778" layer="94" rot="MR0" align="center-left">MECC ALTE AC Generator (ECP28-VL/ 4A)</text>
<text x="121.92" y="175.26" size="1.778" layer="94" rot="MR0" align="center-left">1500 rpm</text>
<text x="259.08" y="175.26" size="1.778" layer="94" align="center-left">1800 rpm</text>
</plain>
<instances>
<instance part="FRAME5" gate="G$1" x="0" y="0"/>
<instance part="FRAME5" gate="G$2" x="287.02" y="0"/>
<instance part="V121" gate="G$1" x="375.92" y="248.92" rot="R180"/>
<instance part="U$271" gate="GND" x="345.44" y="238.76" rot="MR0"/>
<instance part="U$272" gate="G$1" x="345.44" y="254"/>
<instance part="GEN1" gate="G$1" x="149.86" y="218.44"/>
<instance part="GEN1" gate="G$2" x="233.68" y="218.44"/>
<instance part="GEN1" gate="G$3" x="149.86" y="180.34"/>
<instance part="GEN1" gate="G$4" x="233.68" y="180.34"/>
<instance part="V3" gate="G$1" x="96.52" y="152.4635"/>
<instance part="V6" gate="G$1" x="12.7" y="248.92" rot="MR180"/>
<instance part="U$1" gate="GND" x="43.18" y="238.76"/>
<instance part="U$2" gate="G$1" x="43.18" y="254" rot="MR0"/>
<instance part="V7" gate="G$1" x="289.56" y="152.4635" rot="MR0"/>
<instance part="U$3" gate="PE" x="170.18" y="157.48" rot="MR0"/>
<instance part="CHASSIS5" gate="G$1" x="177.8" y="157.48"/>
<instance part="U$5" gate="PE" x="254" y="157.48" rot="MR0"/>
<instance part="CHASSIS15" gate="G$1" x="261.62" y="157.48"/>
<instance part="U$6" gate="G$1" x="93.98" y="93.98"/>
<instance part="U$7" gate="G$1" x="292.1" y="93.98"/>
<instance part="U$8" gate="G$1" x="76.2" y="109.22" rot="MR0"/>
<instance part="U$9" gate="GND" x="76.2" y="71.12"/>
<instance part="U$10" gate="G$1" x="274.32" y="109.22" rot="MR0"/>
<instance part="U$11" gate="GND" x="274.32" y="71.12"/>
<instance part="V8" gate="G$1" x="363.22" y="91.44" rot="MR0"/>
<instance part="V11" gate="G$1" x="25.4" y="91.44"/>
</instances>
<busses>
<bus name="AKU,GND">
<segment>
<wire x1="350.52" y1="248.92" x2="360.68" y2="248.92" width="0.762" layer="92"/>
</segment>
<segment>
<wire x1="38.1" y1="248.92" x2="27.94" y2="248.92" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="L1,L2,L3,N,PE">
<segment>
<wire x1="111.76" y1="152.4" x2="162.56" y2="152.4" width="0.762" layer="92"/>
</segment>
<segment>
<wire x1="274.32" y1="152.4" x2="220.98" y2="152.4" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="B$5">
<segment>
<wire x1="314.96" y1="91.44" x2="347.98" y2="91.44" width="0.762" layer="92"/>
</segment>
<segment>
<wire x1="73.66" y1="91.44" x2="40.64" y2="91.44" width="0.762" layer="92"/>
</segment>
</bus>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<wire x1="350.52" y1="248.92" x2="345.44" y2="243.84" width="0.1524" layer="91"/>
<pinref part="U$271" gate="GND" pin="GND"/>
</segment>
<segment>
<wire x1="38.1" y1="248.92" x2="43.18" y2="243.84" width="0.1524" layer="91"/>
<pinref part="U$1" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="U$9" gate="GND" pin="GND"/>
<wire x1="76.2" y1="76.2" x2="76.2" y2="78.74" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$11" gate="GND" pin="GND"/>
<wire x1="274.32" y1="76.2" x2="274.32" y2="78.74" width="0.1524" layer="91"/>
</segment>
</net>
<net name="AKU" class="0">
<segment>
<wire x1="350.52" y1="248.92" x2="345.44" y2="254" width="0.1524" layer="91"/>
<pinref part="U$272" gate="G$1" pin="AKU"/>
</segment>
<segment>
<wire x1="38.1" y1="248.92" x2="43.18" y2="254" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="AKU"/>
</segment>
<segment>
<pinref part="U$8" gate="G$1" pin="AKU"/>
<wire x1="76.2" y1="109.22" x2="76.2" y2="106.68" width="0.1524" layer="91"/>
</segment>
<segment>
<pinref part="U$10" gate="G$1" pin="AKU"/>
<wire x1="274.32" y1="109.22" x2="274.32" y2="106.68" width="0.1524" layer="91"/>
</segment>
</net>
<net name="L1" class="0">
<segment>
<pinref part="GEN1" gate="G$3" pin="L1"/>
<wire x1="132.08" y1="152.4" x2="134.62" y2="154.94" width="0.1524" layer="91"/>
<wire x1="134.62" y1="154.94" x2="134.62" y2="167.64" width="0.1524" layer="91"/>
<label x="134.62" y="157.48" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="GEN1" gate="G$4" pin="L1"/>
<wire x1="220.98" y1="152.4" x2="218.44" y2="154.94" width="0.1524" layer="91"/>
<wire x1="218.44" y1="154.94" x2="218.44" y2="167.64" width="0.1524" layer="91"/>
<label x="218.44" y="157.48" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="L2" class="0">
<segment>
<pinref part="GEN1" gate="G$3" pin="L2"/>
<wire x1="139.7" y1="152.4" x2="142.24" y2="154.94" width="0.1524" layer="91"/>
<wire x1="142.24" y1="154.94" x2="142.24" y2="167.64" width="0.1524" layer="91"/>
<label x="142.24" y="157.48" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="GEN1" gate="G$4" pin="L2"/>
<wire x1="228.6" y1="152.4" x2="226.06" y2="154.94" width="0.1524" layer="91"/>
<wire x1="226.06" y1="154.94" x2="226.06" y2="167.64" width="0.1524" layer="91"/>
<label x="226.06" y="157.48" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="L3" class="0">
<segment>
<pinref part="GEN1" gate="G$3" pin="L3"/>
<wire x1="147.32" y1="152.4" x2="149.86" y2="154.94" width="0.1524" layer="91"/>
<wire x1="149.86" y1="154.94" x2="149.86" y2="167.64" width="0.1524" layer="91"/>
<label x="149.86" y="157.48" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="GEN1" gate="G$4" pin="L3"/>
<wire x1="236.22" y1="152.4" x2="233.68" y2="154.94" width="0.1524" layer="91"/>
<wire x1="233.68" y1="154.94" x2="233.68" y2="167.64" width="0.1524" layer="91"/>
<label x="233.68" y="157.48" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="N" class="0">
<segment>
<pinref part="GEN1" gate="G$4" pin="N"/>
<wire x1="243.84" y1="152.4" x2="241.3" y2="154.94" width="0.1524" layer="91"/>
<wire x1="241.3" y1="154.94" x2="241.3" y2="167.64" width="0.1524" layer="91"/>
<label x="241.3" y="157.48" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="GEN1" gate="G$3" pin="N"/>
<wire x1="157.48" y1="154.94" x2="157.48" y2="167.64" width="0.1524" layer="91"/>
<wire x1="154.94" y1="152.4" x2="157.48" y2="154.94" width="0.1524" layer="91"/>
<label x="157.48" y="157.48" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="PE" class="0">
<segment>
<wire x1="162.56" y1="152.4" x2="165.1" y2="154.94" width="0.1524" layer="91"/>
<wire x1="165.1" y1="154.94" x2="165.1" y2="165.1" width="0.1524" layer="91"/>
<label x="165.1" y="157.48" size="1.778" layer="95" rot="R90"/>
<wire x1="165.1" y1="165.1" x2="160.02" y2="165.1" width="0.1524" layer="91"/>
<wire x1="160.02" y1="165.1" x2="157.48" y2="167.64" width="0.1524" layer="91"/>
<wire x1="165.1" y1="165.1" x2="170.18" y2="165.1" width="0.1524" layer="91"/>
<junction x="165.1" y="165.1"/>
<pinref part="U$3" gate="PE" pin="PE"/>
<wire x1="170.18" y1="165.1" x2="170.18" y2="162.56" width="0.1524" layer="91"/>
<wire x1="170.18" y1="165.1" x2="177.8" y2="165.1" width="0.1524" layer="91"/>
<junction x="170.18" y="165.1"/>
<wire x1="177.8" y1="165.1" x2="177.8" y2="160.02" width="0.1524" layer="91"/>
<pinref part="CHASSIS5" gate="G$1" pin="CHASSIS"/>
</segment>
<segment>
<wire x1="251.46" y1="152.4" x2="248.92" y2="154.94" width="0.1524" layer="91"/>
<wire x1="248.92" y1="154.94" x2="248.92" y2="165.1" width="0.1524" layer="91"/>
<wire x1="248.92" y1="165.1" x2="243.84" y2="165.1" width="0.1524" layer="91"/>
<wire x1="243.84" y1="165.1" x2="241.3" y2="167.64" width="0.1524" layer="91"/>
<wire x1="248.92" y1="165.1" x2="254" y2="165.1" width="0.1524" layer="91"/>
<junction x="248.92" y="165.1"/>
<pinref part="U$5" gate="PE" pin="PE"/>
<wire x1="254" y1="165.1" x2="254" y2="162.56" width="0.1524" layer="91"/>
<wire x1="254" y1="165.1" x2="261.62" y2="165.1" width="0.1524" layer="91"/>
<junction x="254" y="165.1"/>
<wire x1="261.62" y1="165.1" x2="261.62" y2="160.02" width="0.1524" layer="91"/>
<label x="248.92" y="157.48" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<wire x1="330.2" y1="193.04" x2="350.52" y2="193.04" width="0.1524" layer="91"/>
<wire x1="350.52" y1="193.04" x2="350.52" y2="187.96" width="0.1524" layer="91"/>
<wire x1="350.52" y1="187.96" x2="330.2" y2="187.96" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
</compatibility>
</eagle>
