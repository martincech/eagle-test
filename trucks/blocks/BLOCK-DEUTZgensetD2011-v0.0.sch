<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.5.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="113" name="ReferenceLS" color="7" fill="1" visible="no" active="no"/>
<layer number="114" name="FRNTMAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="115" name="FRNTMAAT2" color="7" fill="1" visible="no" active="no"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="no" active="no"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="no" active="no"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="no" active="no"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="no" active="no"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
</layers>
<schematic xreflabel="%F%N/%S" xrefpart="/%S.%C%R">
<libraries>
<library name="CMI">
<description>Project library CMI</description>
<packages>
<package name="EMPTY">
<pad name="1" x="0" y="0" drill="0.8" shape="square"/>
<pad name="2" x="0" y="-1.27" drill="0.8" shape="square"/>
<pad name="3" x="0" y="-2.54" drill="0.8" shape="square"/>
<pad name="4" x="0" y="-3.81" drill="0.8" shape="square"/>
<pad name="5" x="0" y="-5.08" drill="0.8" shape="square"/>
<pad name="6" x="0" y="-6.35" drill="0.8" shape="square"/>
<pad name="7" x="0" y="-7.62" drill="0.8" shape="square"/>
<pad name="8" x="0" y="-8.89" drill="0.8" shape="square"/>
<pad name="9" x="0" y="-10.16" drill="0.8" shape="square"/>
<pad name="10" x="0" y="-11.43" drill="0.8" shape="square"/>
<pad name="11" x="0" y="-12.7" drill="0.8" shape="square"/>
<pad name="12" x="0" y="-13.97" drill="0.8" shape="square"/>
<pad name="13" x="0" y="-15.24" drill="0.8" shape="square"/>
<pad name="14" x="0" y="-16.51" drill="0.8" shape="square"/>
<pad name="15" x="0" y="-17.78" drill="0.8" shape="square"/>
<pad name="16" x="0" y="-19.05" drill="0.8" shape="square"/>
<pad name="17" x="0" y="-20.32" drill="0.8" shape="square"/>
<pad name="18" x="0" y="-21.59" drill="0.8" shape="square"/>
<pad name="19" x="0" y="-22.86" drill="0.8" shape="square"/>
<pad name="20" x="0" y="-24.13" drill="0.8" shape="square"/>
<pad name="21" x="0" y="-25.4" drill="0.8" shape="square"/>
<pad name="22" x="0" y="-26.67" drill="0.8" shape="square"/>
<pad name="23" x="0" y="-27.94" drill="0.8" shape="square"/>
<pad name="24" x="0" y="-29.21" drill="0.8" shape="square"/>
<pad name="25" x="0" y="-30.48" drill="0.8" shape="square"/>
<pad name="26" x="0" y="-31.75" drill="0.8" shape="square"/>
<pad name="27" x="0" y="-33.02" drill="0.8" shape="square"/>
<pad name="28" x="0" y="-34.29" drill="0.8" shape="square"/>
<pad name="29" x="0" y="-35.56" drill="0.8" shape="square"/>
<pad name="30" x="0" y="-36.83" drill="0.8" shape="square"/>
<pad name="31" x="0" y="-38.1" drill="0.8" shape="square"/>
</package>
</packages>
<symbols>
<symbol name="MCFLEX">
<text x="-22.86" y="-23.495" size="1.27" layer="94" distance="100">    Outputs:
All outputs are short circuit-proof 
PIN 02: Generator excitation, terminal D+, 0.5A 
PIN 06: Freely parametrisable, 7.5A (max. 1s) / 6.0A 
PIN 08: Freely parametrisable, 40A (max. 1s) / 20A 
PIN 10: Freely parametrisable, 70A (max. 1s) / 35A 
PIN 11: Freely parametrisable, 40A (max. 1s) / 20A 
PIN 12: Freely parametrisable, 70A (max. 1s) / 35A 
PIN 15: Freely parametrisable, 3.5A (max. 1s) / 3.0A
PIN 18: Freely parametrisable, 40A (max. 1s) / 20A</text>
<text x="-22.86" y="4.445" size="1.27" layer="94" distance="100">    Inputs:
PIN 01: Oil pressure, NO/NC or analog
PIN 03: Temperature, NO/NC or analog
PIN 04: Various, NO/NC 
PIN 07: Auto start, NO/NC 
PIN 09: External stop, NO/NC 
PIN 13: Speed monitoring, terminal W or Sensor</text>
<pin name="ETR-PC+3S" x="43.18" y="-22.86" length="middle" direction="out" rot="R180"/>
<pin name="SPEEDHC" x="43.18" y="-20.32" length="middle" direction="out" rot="R180"/>
<pin name="GLOW@12" x="43.18" y="-17.78" length="middle" direction="out" rot="R180"/>
<pin name="SPEEDPC" x="43.18" y="-15.24" length="middle" direction="out" rot="R180"/>
<pin name="GLOW@10" x="43.18" y="-12.7" length="middle" direction="out" rot="R180"/>
<pin name="KL.50F" x="43.18" y="-10.16" length="middle" direction="out" rot="R180"/>
<pin name="ETR-HC" x="43.18" y="-7.62" length="middle" direction="out" rot="R180"/>
<pin name="D+" x="43.18" y="-5.08" length="middle" direction="out" rot="R180"/>
<pin name="SPEED" x="-43.18" y="5.08" length="middle" direction="in"/>
<pin name="STOP" x="-43.18" y="7.62" length="middle" direction="in"/>
<pin name="AUTOSTART" x="-43.18" y="10.16" length="middle" direction="in"/>
<pin name="DIVERSE" x="-43.18" y="12.7" length="middle" direction="in"/>
<pin name="TEMP" x="-43.18" y="15.24" length="middle" direction="in"/>
<pin name="OIL" x="-43.18" y="17.78" length="middle" direction="in"/>
<pin name="KL30@14" x="-43.18" y="25.4" length="middle" direction="in"/>
<pin name="KL30@16" x="-43.18" y="22.86" length="middle" direction="in"/>
<pin name="KL31" x="-43.18" y="-25.4" length="middle" direction="in"/>
<pin name="CANL" x="43.18" y="22.86" length="middle" direction="in" rot="R180"/>
<pin name="CANH" x="43.18" y="25.4" length="middle" direction="in" rot="R180"/>
<wire x1="-38.1" y1="27.94" x2="38.1" y2="27.94" width="0.4064" layer="94"/>
<wire x1="38.1" y1="27.94" x2="38.1" y2="-27.94" width="0.4064" layer="94"/>
<wire x1="38.1" y1="-27.94" x2="-38.1" y2="-27.94" width="0.4064" layer="94"/>
<wire x1="-38.1" y1="-27.94" x2="-38.1" y2="27.94" width="0.4064" layer="94"/>
<text x="0" y="25.4" size="2.54" layer="94" align="center">MCflex</text>
<text x="0" y="21.59" size="1.778" layer="94" align="center">ehb electronics</text>
<wire x1="35.145" y1="1.135" x2="35.145" y2="18.635" width="0.1" layer="94"/>
<wire x1="35.145" y1="18.635" x2="34.645" y2="19.135" width="0.1" layer="94" curve="90"/>
<wire x1="34.645" y1="19.135" x2="17.645" y2="19.135" width="0.1" layer="94"/>
<wire x1="17.645" y1="19.135" x2="17.145" y2="18.635" width="0.1" layer="94" curve="90"/>
<wire x1="17.145" y1="18.635" x2="17.145" y2="1.135" width="0.1" layer="94"/>
<wire x1="17.145" y1="1.135" x2="17.645" y2="0.635" width="0.1" layer="94" curve="90"/>
<wire x1="17.645" y1="0.635" x2="34.645" y2="0.635" width="0.1" layer="94"/>
<wire x1="34.645" y1="0.635" x2="35.145" y2="1.135" width="0.1" layer="94" curve="90"/>
<wire x1="18.645" y1="17.435" x2="18.845" y2="17.635" width="0.1" layer="94" curve="-90"/>
<wire x1="18.845" y1="17.635" x2="33.445" y2="17.635" width="0.1" layer="94"/>
<wire x1="33.445" y1="17.635" x2="33.645" y2="17.435" width="0.1" layer="94" curve="-90"/>
<wire x1="33.645" y1="17.435" x2="33.645" y2="15.135" width="0.1" layer="94"/>
<wire x1="33.645" y1="15.135" x2="33.645" y2="11.635" width="0.1" layer="94"/>
<wire x1="33.645" y1="11.635" x2="33.645" y2="2.335" width="0.1" layer="94"/>
<wire x1="33.645" y1="2.335" x2="33.445" y2="2.135" width="0.1" layer="94" curve="-90"/>
<wire x1="33.445" y1="2.135" x2="18.845" y2="2.135" width="0.1" layer="94"/>
<wire x1="18.845" y1="2.135" x2="18.645" y2="2.335" width="0.1" layer="94" curve="-90"/>
<wire x1="18.645" y1="2.335" x2="18.645" y2="11.635" width="0.1" layer="94"/>
<wire x1="18.645" y1="11.635" x2="18.645" y2="15.135" width="0.1" layer="94"/>
<wire x1="18.645" y1="15.135" x2="18.645" y2="17.435" width="0.1" layer="94"/>
<wire x1="18.645" y1="15.135" x2="20.145" y2="15.135" width="0.1" layer="94"/>
<circle x="29.645" y="6.635" radius="3.3" width="0.1" layer="94"/>
<wire x1="20.145" y1="15.135" x2="32.145" y2="15.135" width="0.1" layer="94"/>
<wire x1="32.145" y1="15.135" x2="33.645" y2="15.135" width="0.1" layer="94"/>
<wire x1="33.645" y1="11.635" x2="32.145" y2="11.635" width="0.1" layer="94"/>
<wire x1="32.145" y1="11.635" x2="20.145" y2="11.635" width="0.1" layer="94"/>
<wire x1="20.145" y1="11.635" x2="18.645" y2="11.635" width="0.1" layer="94"/>
<wire x1="20.145" y1="11.635" x2="20.145" y2="15.135" width="0.1" layer="94"/>
<wire x1="32.145" y1="11.635" x2="32.145" y2="15.135" width="0.1" layer="94"/>
<wire x1="20.345" y1="3.735" x2="20.345" y2="4.735" width="0.1" layer="94"/>
<wire x1="20.345" y1="4.735" x2="20.645" y2="5.035" width="0.1" layer="94"/>
<wire x1="20.645" y1="5.035" x2="20.645" y2="4.335" width="0.1" layer="94"/>
<wire x1="20.645" y1="4.335" x2="20.745" y2="4.335" width="0.1" layer="94"/>
<wire x1="20.745" y1="4.335" x2="21.445" y2="5.035" width="0.1" layer="94"/>
<wire x1="21.445" y1="5.035" x2="21.645" y2="4.835" width="0.1" layer="94"/>
<wire x1="21.645" y1="4.835" x2="20.945" y2="4.135" width="0.1" layer="94"/>
<wire x1="20.945" y1="4.135" x2="20.945" y2="4.035" width="0.1" layer="94"/>
<wire x1="20.945" y1="4.035" x2="21.645" y2="4.035" width="0.1" layer="94"/>
<wire x1="21.645" y1="4.035" x2="21.345" y2="3.735" width="0.1" layer="94"/>
<wire x1="21.345" y1="3.735" x2="20.345" y2="3.735" width="0.1" layer="94"/>
<wire x1="21.745" y1="9.535" x2="21.745" y2="8.535" width="0.1" layer="94"/>
<wire x1="21.745" y1="8.535" x2="21.445" y2="8.235" width="0.1" layer="94"/>
<wire x1="21.445" y1="8.235" x2="21.445" y2="8.935" width="0.1" layer="94"/>
<wire x1="21.445" y1="8.935" x2="21.345" y2="8.935" width="0.1" layer="94"/>
<wire x1="21.345" y1="8.935" x2="20.645" y2="8.235" width="0.1" layer="94"/>
<wire x1="20.645" y1="8.235" x2="20.445" y2="8.435" width="0.1" layer="94"/>
<wire x1="20.445" y1="8.435" x2="21.145" y2="9.135" width="0.1" layer="94"/>
<wire x1="21.145" y1="9.135" x2="21.145" y2="9.235" width="0.1" layer="94"/>
<wire x1="21.145" y1="9.235" x2="20.445" y2="9.235" width="0.1" layer="94"/>
<wire x1="20.445" y1="9.235" x2="20.745" y2="9.535" width="0.1" layer="94"/>
<wire x1="20.745" y1="9.535" x2="21.745" y2="9.535" width="0.1" layer="94"/>
<circle x="20.945" y="16.435" radius="0.4" width="0" layer="94"/>
<text x="23.345" y="3.835" size="1" layer="94">Set</text>
</symbol>
<symbol name="1SVORKA_POKUS">
<text x="0" y="1.27" size="1.778" layer="96" align="bottom-center">&gt;VALUE</text>
<circle x="0" y="0" radius="0.898025" width="0.254" layer="94"/>
</symbol>
<symbol name="JISTIC_DUMMY">
<wire x1="-2.54" y1="0" x2="3.175" y2="1.016" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-5.08" y2="0" width="0.1524" layer="94"/>
<circle x="-2.54" y="0" radius="0.254" width="0.508" layer="94"/>
<wire x1="5.08" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="0" y="2.54" size="1.778" layer="94" align="bottom-center">&gt;VALUE</text>
<wire x1="1.016" y1="0.635" x2="0.889" y2="1.397" width="0.254" layer="94"/>
<wire x1="2.413" y1="0.889" x2="2.286" y2="1.651" width="0.254" layer="94"/>
<wire x1="0.889" y1="1.397" x2="2.286" y2="1.651" width="0.254" layer="94"/>
</symbol>
<symbol name="SPINAC_DUMMY">
<wire x1="-2.54" y1="0" x2="3.175" y2="1.016" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-5.08" y2="0" width="0.1524" layer="94"/>
<circle x="-2.54" y="0" radius="0.254" width="0.508" layer="94"/>
<wire x1="5.08" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="0" y="2.54" size="1.778" layer="94" align="bottom-center">&gt;VALUE</text>
</symbol>
<symbol name="ROZPINACI_DUMMY">
<wire x1="-2.54" y1="0" x2="3.175" y2="-1.016" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-5.08" y2="0" width="0.1524" layer="94"/>
<circle x="-2.54" y="0" radius="0.254" width="0.508" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="0" y="2.54" size="1.778" layer="94" align="bottom-center">&gt;VALUE</text>
</symbol>
<symbol name="ZEM_DUMMY">
<wire x1="-1.27" y1="-0.762" x2="1.27" y2="-0.762" width="0.254" layer="94"/>
<wire x1="-0.635" y1="-1.524" x2="0.635" y2="-1.524" width="0.254" layer="94"/>
<wire x1="-1.905" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="1.905" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="2.54" width="0.1524" layer="94"/>
</symbol>
<symbol name="MOTOR_DUMMY">
<circle x="0" y="0" radius="2.54" width="0.254" layer="94"/>
<text x="0" y="0" size="2.54" layer="94" align="center">M</text>
</symbol>
<symbol name="SELENOID_VENTIL_DUMMY">
<wire x1="3.81" y1="1.905" x2="1.905" y2="1.905" width="0.254" layer="94"/>
<wire x1="-3.81" y1="1.905" x2="-3.81" y2="-1.905" width="0.254" layer="94"/>
<wire x1="-3.81" y1="-1.905" x2="-1.905" y2="-1.905" width="0.254" layer="94"/>
<wire x1="3.81" y1="-1.905" x2="3.81" y2="0" width="0.254" layer="94"/>
<wire x1="3.81" y1="0" x2="3.81" y2="1.905" width="0.254" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="1.905" x2="-3.81" y2="1.905" width="0.254" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.905" x2="3.81" y2="-1.905" width="0.254" layer="94"/>
<wire x1="1.905" y1="1.905" x2="-1.905" y2="-1.905" width="0.1524" layer="94"/>
<wire x1="1.905" y1="1.905" x2="0" y2="1.905" width="0.254" layer="94"/>
<wire x1="-1.905" y1="-1.905" x2="0" y2="-1.905" width="0.254" layer="94"/>
<text x="10.16" y="0" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<wire x1="6.35" y1="-2.54" x2="8.89" y2="2.54" width="0.254" layer="94"/>
<wire x1="8.89" y1="2.54" x2="7.62" y2="2.54" width="0.254" layer="94"/>
<wire x1="7.62" y1="2.54" x2="6.35" y2="2.54" width="0.254" layer="94"/>
<wire x1="6.35" y1="2.54" x2="8.89" y2="-2.54" width="0.254" layer="94"/>
<wire x1="8.89" y1="-2.54" x2="7.62" y2="-2.54" width="0.254" layer="94"/>
<wire x1="7.62" y1="-2.54" x2="6.35" y2="-2.54" width="0.254" layer="94"/>
<wire x1="3.81" y1="0" x2="4.445" y2="0" width="0.1524" layer="94" style="shortdash"/>
<wire x1="5.08" y1="0" x2="5.715" y2="0" width="0.1524" layer="94" style="shortdash"/>
<wire x1="6.35" y1="0" x2="6.985" y2="0" width="0.1524" layer="94" style="shortdash"/>
<wire x1="7.62" y1="-3.81" x2="7.62" y2="-2.54" width="0.1524" layer="94" style="shortdash"/>
<wire x1="7.62" y1="2.54" x2="7.62" y2="3.81" width="0.1524" layer="94" style="shortdash"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MCFLEX" prefix="MODUL">
<gates>
<gate name="G$1" symbol="MCFLEX" x="0" y="0"/>
</gates>
<devices>
<device name="" package="EMPTY">
<connects>
<connect gate="G$1" pin="AUTOSTART" pad="7"/>
<connect gate="G$1" pin="CANH" pad="19"/>
<connect gate="G$1" pin="CANL" pad="17"/>
<connect gate="G$1" pin="D+" pad="2"/>
<connect gate="G$1" pin="DIVERSE" pad="4"/>
<connect gate="G$1" pin="ETR-HC" pad="6"/>
<connect gate="G$1" pin="ETR-PC+3S" pad="18"/>
<connect gate="G$1" pin="GLOW@10" pad="10"/>
<connect gate="G$1" pin="GLOW@12" pad="12"/>
<connect gate="G$1" pin="KL.50F" pad="8"/>
<connect gate="G$1" pin="KL30@14" pad="14"/>
<connect gate="G$1" pin="KL30@16" pad="16"/>
<connect gate="G$1" pin="KL31" pad="5"/>
<connect gate="G$1" pin="OIL" pad="1"/>
<connect gate="G$1" pin="SPEED" pad="13"/>
<connect gate="G$1" pin="SPEEDHC" pad="15"/>
<connect gate="G$1" pin="SPEEDPC" pad="11"/>
<connect gate="G$1" pin="STOP" pad="9"/>
<connect gate="G$1" pin="TEMP" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="1SVORKA_POKUS" uservalue="yes">
<gates>
<gate name="G$1" symbol="1SVORKA_POKUS" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="JISTIC_DUMMY" uservalue="yes">
<gates>
<gate name="G$1" symbol="JISTIC_DUMMY" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SPINAC_DUMMY" uservalue="yes">
<gates>
<gate name="G$1" symbol="SPINAC_DUMMY" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SPINAC_ROZPINACI_DUMMY" uservalue="yes">
<gates>
<gate name="G$1" symbol="ROZPINACI_DUMMY" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ZEM_DUMMY">
<gates>
<gate name="G$1" symbol="ZEM_DUMMY" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="MOTOR_DUMMY">
<gates>
<gate name="G$1" symbol="MOTOR_DUMMY" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="SELENOID_VENTIL_DUMMY" uservalue="yes">
<gates>
<gate name="G$1" symbol="SELENOID_VENTIL_DUMMY" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="VEIT_logotyp">
<packages>
</packages>
<symbols>
<symbol name="DOCFIELD">
<wire x1="0" y1="0" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="101.6" y1="15.24" x2="87.63" y2="15.24" width="0.1016" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="71.12" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="15.24" width="0.1016" layer="94"/>
<wire x1="101.6" y1="15.24" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="0" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="87.63" y1="15.24" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="87.63" y1="15.24" x2="0" y2="15.24" width="0.1016" layer="94"/>
<wire x1="87.63" y1="5.08" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="101.6" y1="5.08" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="0" y1="15.24" x2="0" y2="20.32" width="0.1016" layer="94"/>
<wire x1="101.6" y1="35.56" x2="66.04" y2="35.56" width="0.1016" layer="94"/>
<wire x1="66.04" y1="35.56" x2="64.77" y2="35.56" width="0.1016" layer="94"/>
<wire x1="64.77" y1="35.56" x2="0" y2="35.56" width="0.1016" layer="94"/>
<wire x1="101.6" y1="35.56" x2="101.6" y2="20.32" width="0.1016" layer="94"/>
<wire x1="0" y1="20.32" x2="64.77" y2="20.32" width="0.1016" layer="94"/>
<wire x1="64.77" y1="20.32" x2="66.04" y2="20.32" width="0.1016" layer="94"/>
<wire x1="66.04" y1="20.32" x2="101.6" y2="20.32" width="0.1016" layer="94"/>
<wire x1="0" y1="20.32" x2="0" y2="35.56" width="0.1016" layer="94"/>
<wire x1="101.6" y1="20.32" x2="101.6" y2="15.24" width="0.1016" layer="94"/>
<text x="1.27" y="1.27" size="2.54" layer="94">Date:</text>
<text x="15.24" y="1.27" size="2.54" layer="94">&gt;LAST_DATE_TIME</text>
<text x="72.39" y="1.27" size="2.54" layer="94">Sheet:</text>
<text x="86.36" y="1.27" size="2.54" layer="94">&gt;SHEET</text>
<text x="88.9" y="11.43" size="2.54" layer="94">REV:</text>
<text x="1.27" y="16.51" size="2.54" layer="94">TITLE:</text>
<text x="1.27" y="11.43" size="2.54" layer="94">Document Number:</text>
<text x="15.24" y="16.51" size="2.54" layer="94">&gt;DRAWING_NAME</text>
<wire x1="64.77" y1="35.56" x2="64.77" y2="20.32" width="0.1016" layer="94"/>
<wire x1="97.487340625" y1="32.27106875" x2="90.66211875" y2="32.27106875" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="32.27106875" x2="90.66211875" y2="32.199" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="32.199" x2="90.66211875" y2="31.974" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="31.974" x2="90.66211875" y2="31.749" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="31.749" x2="90.66211875" y2="31.524" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="31.524" x2="90.66211875" y2="31.299" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="31.299" x2="90.66211875" y2="31.074" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="31.074" x2="90.66211875" y2="30.849" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="30.849" x2="90.66211875" y2="30.69621875" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="30.69621875" x2="93.287040625" y2="30.69621875" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="30.69621875" x2="93.287040625" y2="30.624" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="30.624" x2="93.287040625" y2="30.399" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="30.399" x2="93.287040625" y2="30.174" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="30.174" x2="93.287040625" y2="29.949" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="29.949" x2="93.287040625" y2="29.724" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="29.724" x2="93.287040625" y2="29.499" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="29.499" x2="93.287040625" y2="29.274" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="29.274" x2="93.287040625" y2="29.049" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="29.049" x2="93.287040625" y2="28.824" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="28.824" x2="93.287040625" y2="28.599" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="28.599" x2="93.287040625" y2="28.374" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="28.374" x2="93.287040625" y2="28.149" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="28.149" x2="93.287040625" y2="27.924" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="27.924" x2="93.287040625" y2="27.699" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="27.699" x2="93.287040625" y2="27.474" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="27.474" x2="93.287040625" y2="27.249" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="27.249" x2="93.287040625" y2="27.024" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="27.024" x2="93.287040625" y2="26.91076875" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="26.91076875" x2="94.862340625" y2="26.91076875" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="26.91076875" x2="94.862340625" y2="27.024" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="27.024" x2="94.862340625" y2="27.249" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="27.249" x2="94.862340625" y2="27.474" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="27.474" x2="94.862340625" y2="27.699" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="27.699" x2="94.862340625" y2="27.924" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="27.924" x2="94.862340625" y2="28.149" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="28.149" x2="94.862340625" y2="28.374" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="28.374" x2="94.862340625" y2="28.599" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="28.599" x2="94.862340625" y2="28.824" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="28.824" x2="94.862340625" y2="29.049" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="29.049" x2="94.862340625" y2="29.274" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="29.274" x2="94.862340625" y2="29.499" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="29.499" x2="94.862340625" y2="29.724" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="29.724" x2="94.862340625" y2="29.949" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="29.949" x2="94.862340625" y2="30.174" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="30.174" x2="94.862340625" y2="30.399" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="30.399" x2="94.862340625" y2="30.624" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="30.624" x2="94.862340625" y2="30.69621875" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="30.69621875" x2="97.487340625" y2="30.69621875" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="30.69621875" x2="97.487340625" y2="30.849" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="30.849" x2="97.487340625" y2="31.074" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="31.074" x2="97.487340625" y2="31.299" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="31.299" x2="97.487340625" y2="31.524" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="31.524" x2="97.487340625" y2="31.749" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="31.749" x2="97.487340625" y2="31.974" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="31.974" x2="97.487340625" y2="32.199" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="32.199" x2="97.487340625" y2="32.27106875" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="26.957059375" x2="87.068640625" y2="27.024" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="27.024" x2="87.068640625" y2="27.249" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="27.249" x2="87.068640625" y2="27.474" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="27.474" x2="87.068640625" y2="27.699" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="27.699" x2="87.068640625" y2="27.924" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="27.924" x2="87.068640625" y2="28.149" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="28.149" x2="87.068640625" y2="28.374" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="28.374" x2="87.068640625" y2="28.599" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="28.599" x2="87.068640625" y2="28.824" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="28.824" x2="87.068640625" y2="29.049" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="29.049" x2="87.068640625" y2="29.274" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="29.274" x2="87.068640625" y2="29.499" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="29.499" x2="87.068640625" y2="29.724" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="29.724" x2="87.068640625" y2="29.949" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="29.949" x2="87.068640625" y2="30.174" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="30.174" x2="87.068640625" y2="30.399" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="30.399" x2="87.068640625" y2="30.624" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="30.624" x2="87.068640625" y2="30.849" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="30.849" x2="87.068640625" y2="31.074" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="31.074" x2="87.068640625" y2="31.299" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="31.299" x2="87.068640625" y2="31.524" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="31.524" x2="87.068640625" y2="31.749" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="31.749" x2="87.068640625" y2="31.974" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="31.974" x2="87.068640625" y2="32.199" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="32.199" x2="87.068640625" y2="32.273409375" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="32.273409375" x2="88.64378125" y2="32.273409375" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="32.273409375" x2="88.64378125" y2="32.199" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="32.199" x2="88.64378125" y2="31.974" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="31.974" x2="88.64378125" y2="31.749" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="31.749" x2="88.64378125" y2="31.524" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="31.524" x2="88.64378125" y2="31.299" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="31.299" x2="88.64378125" y2="31.074" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="31.074" x2="88.64378125" y2="30.849" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="30.849" x2="88.64378125" y2="30.624" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="30.624" x2="88.64378125" y2="30.399" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="30.399" x2="88.64378125" y2="30.174" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="30.174" x2="88.64378125" y2="29.949" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="29.949" x2="88.64378125" y2="29.724" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="29.724" x2="88.64378125" y2="29.499" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="29.499" x2="88.64378125" y2="29.274" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="29.274" x2="88.64378125" y2="29.049" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="29.049" x2="88.64378125" y2="28.824" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="28.824" x2="88.64378125" y2="28.599" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="28.599" x2="88.64378125" y2="28.374" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="28.374" x2="88.64378125" y2="28.149" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="28.149" x2="88.64378125" y2="27.924" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="27.924" x2="88.64378125" y2="27.699" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="27.699" x2="88.64378125" y2="27.474" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="27.474" x2="88.64378125" y2="27.249" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="27.249" x2="88.64378125" y2="27.024" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="27.024" x2="88.64378125" y2="26.957059375" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="26.957059375" x2="87.068640625" y2="26.957059375" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="26.95851875" x2="78.286090625" y2="27.024" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="27.024" x2="78.286090625" y2="27.249" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="27.249" x2="78.286090625" y2="27.474" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="27.474" x2="78.286090625" y2="27.699" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="27.699" x2="78.286090625" y2="27.924" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="27.924" x2="78.286090625" y2="28.149" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="28.149" x2="78.286090625" y2="28.374" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="28.374" x2="78.286090625" y2="28.53366875" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="28.53366875" x2="84.81116875" y2="28.53366875" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="28.53366875" x2="84.81116875" y2="28.374" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="28.374" x2="84.81116875" y2="28.149" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="28.149" x2="84.81116875" y2="27.924" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="27.924" x2="84.81116875" y2="27.699" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="27.699" x2="84.81116875" y2="27.474" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="27.474" x2="84.81116875" y2="27.249" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="27.249" x2="84.81116875" y2="27.024" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="27.024" x2="84.81116875" y2="26.95851875" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="26.95851875" x2="78.286090625" y2="26.95851875" width="0.225" layer="94"/>
<wire x1="78.286240625" y1="28.90236875" x2="78.286240625" y2="29.049" width="0.225" layer="94"/>
<wire x1="78.286240625" y1="29.049" x2="78.286240625" y2="29.274" width="0.225" layer="94"/>
<wire x1="78.286240625" y1="29.274" x2="78.286240625" y2="29.499" width="0.225" layer="94"/>
<wire x1="78.286240625" y1="29.499" x2="78.286240625" y2="29.724" width="0.225" layer="94"/>
<wire x1="78.286240625" y1="29.724" x2="78.286240625" y2="29.949" width="0.225" layer="94"/>
<wire x1="78.286240625" y1="29.949" x2="78.286240625" y2="30.174" width="0.225" layer="94"/>
<wire x1="78.286240625" y1="30.174" x2="78.286240625" y2="30.32721875" width="0.225" layer="94"/>
<wire x1="78.286240625" y1="30.32721875" x2="81.96101875" y2="30.32721875" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="30.32721875" x2="81.96101875" y2="30.174" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="30.174" x2="81.96101875" y2="29.949" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="29.949" x2="81.96101875" y2="29.724" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="29.724" x2="81.96101875" y2="29.499" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="29.499" x2="81.96101875" y2="29.274" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="29.274" x2="81.96101875" y2="29.049" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="29.049" x2="81.96101875" y2="28.90236875" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="28.90236875" x2="78.286240625" y2="28.90236875" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="30.69621875" x2="78.286090625" y2="30.849" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="30.849" x2="78.286090625" y2="31.074" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="31.074" x2="78.286090625" y2="31.299" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="31.299" x2="78.286090625" y2="31.524" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="31.524" x2="78.286090625" y2="31.749" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="31.749" x2="78.286090625" y2="31.974" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="31.974" x2="78.286090625" y2="32.199" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="32.199" x2="78.286090625" y2="32.27106875" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="32.27106875" x2="84.81101875" y2="32.27106875" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="32.27106875" x2="84.81101875" y2="32.199" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="32.199" x2="84.81101875" y2="31.974" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="31.974" x2="84.81101875" y2="31.749" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="31.749" x2="84.81101875" y2="31.524" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="31.524" x2="84.81101875" y2="31.299" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="31.299" x2="84.81101875" y2="31.074" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="31.074" x2="84.81101875" y2="30.849" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="30.849" x2="84.81101875" y2="30.69621875" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="30.69621875" x2="78.286090625" y2="30.69621875" width="0.225" layer="94"/>
<wire x1="68.6921" y1="31.404190625" x2="69.073209375" y2="31.749" width="0.225" layer="94"/>
<wire x1="69.073209375" y1="31.749" x2="69.3219" y2="31.974" width="0.225" layer="94"/>
<wire x1="69.3219" y1="31.974" x2="69.570590625" y2="32.199" width="0.225" layer="94"/>
<wire x1="69.570590625" y1="32.199" x2="69.86021875" y2="32.461040625" width="0.225" layer="94"/>
<wire x1="85.115190625" y1="23.80016875" x2="85.115190625" y2="23.649" width="0.225" layer="94"/>
<wire x1="85.115190625" y1="23.649" x2="85.115190625" y2="23.43718125" width="0.225" layer="94"/>
<wire x1="84.32008125" y1="23.440109375" x2="84.341215625" y2="23.16643125" width="0.225" layer="94" curve="8.832242"/>
<wire x1="84.341209375" y1="23.16643125" x2="84.4424375" y2="22.91343125" width="0.225" layer="94" curve="25.94924"/>
<wire x1="84.442440625" y1="22.91343125" x2="84.662590625" y2="22.7562625" width="0.225" layer="94" curve="39.389578"/>
<wire x1="84.662590625" y1="22.756259375" x2="84.93363125" y2="22.718525" width="0.225" layer="94" curve="15.805998"/>
<wire x1="85.52626875" y1="23.166240625" x2="85.547246875" y2="23.440109375" width="0.225" layer="94" curve="8.760059"/>
<wire x1="85.42531875" y1="22.91295" x2="85.46248125" y2="22.974" width="0.225" layer="94" curve="6.74589"/>
<wire x1="85.46248125" y1="22.974" x2="85.526271875" y2="23.166240625" width="0.225" layer="94" curve="19.196299"/>
<wire x1="85.204909375" y1="22.755909375" x2="85.425315625" y2="22.912953125" width="0.225" layer="94" curve="39.656265"/>
<wire x1="84.93363125" y1="22.71853125" x2="85.204909375" y2="22.755909375" width="0.225" layer="94" curve="15.593829"/>
<wire x1="84.32008125" y1="23.440109375" x2="84.32008125" y2="23.649" width="0.225" layer="94"/>
<wire x1="84.32008125" y1="23.649" x2="84.32008125" y2="23.80016875" width="0.225" layer="94"/>
<wire x1="84.341340625" y1="24.07343125" x2="84.32161875" y2="23.874" width="0.225" layer="94" curve="6.503067"/>
<wire x1="84.32161875" y1="23.874" x2="84.320078125" y2="23.80016875" width="0.225" layer="94" curve="2.395218"/>
<wire x1="84.44311875" y1="24.32573125" x2="84.341340625" y2="24.07343125" width="0.225" layer="94" curve="26.142719"/>
<wire x1="84.663740625" y1="24.48155" x2="84.443121875" y2="24.325728125" width="0.225" layer="94" curve="39.450584"/>
<wire x1="84.93363125" y1="24.51881875" x2="84.663740625" y2="24.48155" width="0.225" layer="94" curve="15.292594"/>
<wire x1="85.54725" y1="23.440109375" x2="85.54725" y2="23.649" width="0.225" layer="94"/>
<wire x1="85.54725" y1="23.649" x2="85.54725" y2="23.80016875" width="0.225" layer="94"/>
<wire x1="85.54725" y1="23.80016875" x2="85.52646875" y2="24.07348125" width="0.225" layer="94" curve="8.69613"/>
<wire x1="85.52646875" y1="24.07348125" x2="85.4257" y2="24.3262" width="0.225" layer="94" curve="26.08534"/>
<wire x1="85.4257" y1="24.3262" x2="85.2053" y2="24.48220625" width="0.225" layer="94" curve="39.852867"/>
<wire x1="85.2053" y1="24.482209375" x2="84.93363125" y2="24.518825" width="0.225" layer="94" curve="15.379893"/>
<wire x1="84.752059375" y1="23.80016875" x2="84.752059375" y2="23.649" width="0.225" layer="94"/>
<wire x1="84.752059375" y1="23.649" x2="84.752059375" y2="23.43718125" width="0.225" layer="94"/>
<wire x1="85.115190625" y1="23.43718125" x2="85.11286875" y2="23.33078125" width="0.225" layer="94"/>
<wire x1="85.11286875" y1="23.33078125" x2="85.10523125" y2="23.251359375" width="0.225" layer="94"/>
<wire x1="85.10523125" y1="23.251359375" x2="85.100290625" y2="23.225209375" width="0.225" layer="94"/>
<wire x1="85.100290625" y1="23.225209375" x2="85.09306875" y2="23.19961875" width="0.225" layer="94"/>
<wire x1="85.09306875" y1="23.19961875" x2="85.081940625" y2="23.175509375" width="0.225" layer="94"/>
<wire x1="85.081940625" y1="23.175509375" x2="85.06321875" y2="23.156990625" width="0.225" layer="94"/>
<wire x1="85.06321875" y1="23.156990625" x2="85.0387" y2="23.14683125" width="0.225" layer="94"/>
<wire x1="85.0387" y1="23.14683125" x2="85.012740625" y2="23.14106875" width="0.225" layer="94"/>
<wire x1="85.012740625" y1="23.14106875" x2="84.986340625" y2="23.1378" width="0.225" layer="94"/>
<wire x1="84.986340625" y1="23.1378" x2="84.95978125" y2="23.13618125" width="0.225" layer="94"/>
<wire x1="84.752059375" y1="23.80016875" x2="84.75605" y2="23.93246875" width="0.225" layer="94"/>
<wire x1="84.75605" y1="23.93246875" x2="84.76195" y2="23.98508125" width="0.225" layer="94"/>
<wire x1="84.76195" y1="23.98508125" x2="84.77411875" y2="24.03655" width="0.225" layer="94"/>
<wire x1="84.77411875" y1="24.03655" x2="84.78536875" y2="24.060440625" width="0.225" layer="94"/>
<wire x1="84.78536875" y1="24.060440625" x2="84.804459375" y2="24.078359375" width="0.225" layer="94"/>
<wire x1="84.804459375" y1="24.078359375" x2="84.82903125" y2="24.08806875" width="0.225" layer="94"/>
<wire x1="84.82903125" y1="24.08806875" x2="84.854909375" y2="24.09358125" width="0.225" layer="94"/>
<wire x1="84.854909375" y1="24.09358125" x2="84.8812" y2="24.0967" width="0.225" layer="94"/>
<wire x1="84.8812" y1="24.0967" x2="84.90763125" y2="24.09826875" width="0.225" layer="94"/>
<wire x1="84.90763125" y1="24.09826875" x2="84.96056875" y2="24.09825" width="0.225" layer="94"/>
<wire x1="84.96056875" y1="24.09825" x2="84.987" y2="24.09666875" width="0.225" layer="94"/>
<wire x1="84.987" y1="24.09666875" x2="85.013290625" y2="24.09358125" width="0.225" layer="94"/>
<wire x1="85.013290625" y1="24.09358125" x2="85.039190625" y2="24.088140625" width="0.225" layer="94"/>
<wire x1="85.039190625" y1="24.088140625" x2="85.063809375" y2="24.078559375" width="0.225" layer="94"/>
<wire x1="85.063809375" y1="24.078559375" x2="85.0829" y2="24.06066875" width="0.225" layer="94"/>
<wire x1="85.0829" y1="24.06066875" x2="85.093940625" y2="24.03666875" width="0.225" layer="94"/>
<wire x1="85.093940625" y1="24.03666875" x2="85.105759375" y2="23.98511875" width="0.225" layer="94"/>
<wire x1="85.105759375" y1="23.98511875" x2="85.111390625" y2="23.93248125" width="0.225" layer="94"/>
<wire x1="85.111390625" y1="23.93248125" x2="85.11306875" y2="23.874" width="0.225" layer="94"/>
<wire x1="85.11306875" y1="23.874" x2="85.115190625" y2="23.80016875" width="0.225" layer="94"/>
<wire x1="84.752059375" y1="23.43718125" x2="84.754290625" y2="23.33078125" width="0.225" layer="94"/>
<wire x1="84.754290625" y1="23.33078125" x2="84.76156875" y2="23.25131875" width="0.225" layer="94"/>
<wire x1="84.76156875" y1="23.25131875" x2="84.7664" y2="23.22515" width="0.225" layer="94"/>
<wire x1="84.7664" y1="23.22515" x2="84.773409375" y2="23.1995" width="0.225" layer="94"/>
<wire x1="84.773409375" y1="23.1995" x2="84.784340625" y2="23.1753" width="0.225" layer="94"/>
<wire x1="84.784340625" y1="23.1753" x2="84.803059375" y2="23.1568" width="0.225" layer="94"/>
<wire x1="84.803059375" y1="23.1568" x2="84.82763125" y2="23.146759375" width="0.225" layer="94"/>
<wire x1="84.82763125" y1="23.146759375" x2="84.853609375" y2="23.141059375" width="0.225" layer="94"/>
<wire x1="84.853609375" y1="23.141059375" x2="84.90656875" y2="23.1362" width="0.225" layer="94"/>
<wire x1="84.90656875" y1="23.1362" x2="84.95978125" y2="23.1362" width="0.225" layer="94"/>
<wire x1="88.41505" y1="24.49186875" x2="88.835090625" y2="24.49186875" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="24.49186875" x2="88.835090625" y2="24.324" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="24.324" x2="88.835090625" y2="24.099" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="24.099" x2="88.835090625" y2="23.874" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="23.874" x2="88.835090625" y2="23.649" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="23.649" x2="88.835090625" y2="23.424" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="23.424" x2="88.835090625" y2="23.199" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="23.199" x2="88.835090625" y2="22.974" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="22.974" x2="88.835090625" y2="22.749" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="22.749" x2="88.835090625" y2="22.74548125" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="22.74548125" x2="88.41505" y2="22.74548125" width="0.225" layer="94"/>
<wire x1="88.41505" y1="22.74548125" x2="88.41505" y2="22.749" width="0.225" layer="94"/>
<wire x1="88.41505" y1="22.749" x2="88.41505" y2="22.974" width="0.225" layer="94"/>
<wire x1="88.41505" y1="22.974" x2="88.41505" y2="23.199" width="0.225" layer="94"/>
<wire x1="88.41505" y1="23.199" x2="88.41505" y2="23.424" width="0.225" layer="94"/>
<wire x1="88.41505" y1="23.424" x2="88.41505" y2="23.649" width="0.225" layer="94"/>
<wire x1="88.41505" y1="23.649" x2="88.41505" y2="23.874" width="0.225" layer="94"/>
<wire x1="88.41505" y1="23.874" x2="88.41505" y2="24.099" width="0.225" layer="94"/>
<wire x1="88.41505" y1="24.099" x2="88.41505" y2="24.324" width="0.225" layer="94"/>
<wire x1="88.41505" y1="24.324" x2="88.41505" y2="24.49186875" width="0.225" layer="94"/>
<wire x1="88.41505" y1="24.665890625" x2="88.835090625" y2="24.665890625" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="24.665890625" x2="88.835090625" y2="24.774" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="24.774" x2="88.835090625" y2="24.999" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="24.999" x2="88.835090625" y2="25.14606875" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="25.14606875" x2="88.41505" y2="25.14606875" width="0.225" layer="94"/>
<wire x1="88.41505" y1="25.14606875" x2="88.41505" y2="24.999" width="0.225" layer="94"/>
<wire x1="88.41505" y1="24.999" x2="88.41505" y2="24.774" width="0.225" layer="94"/>
<wire x1="88.41505" y1="24.774" x2="88.41505" y2="24.665890625" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="25.185040625" x2="75.911209375" y2="25.185040625" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="25.185040625" x2="75.911209375" y2="24.999" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="24.999" x2="75.911209375" y2="24.774" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="24.774" x2="75.911209375" y2="24.549" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="24.549" x2="75.911209375" y2="24.324" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="24.324" x2="75.911209375" y2="24.099" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="24.099" x2="75.911209375" y2="23.874" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="23.874" x2="75.911209375" y2="23.649" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="23.649" x2="75.911209375" y2="23.424" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="23.424" x2="75.911209375" y2="23.199" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="23.199" x2="75.911209375" y2="22.974" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="22.974" x2="75.911209375" y2="22.749" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="22.749" x2="75.911209375" y2="22.74548125" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="22.74548125" x2="75.49116875" y2="22.74548125" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="22.74548125" x2="75.49116875" y2="22.749" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="22.749" x2="75.49116875" y2="22.974" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="22.974" x2="75.49116875" y2="23.199" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="23.199" x2="75.49116875" y2="23.424" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="23.424" x2="75.49116875" y2="23.649" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="23.649" x2="75.49116875" y2="23.874" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="23.874" x2="75.49116875" y2="24.099" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="24.099" x2="75.49116875" y2="24.324" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="24.324" x2="75.49116875" y2="24.549" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="24.549" x2="75.49116875" y2="24.774" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="24.774" x2="75.49116875" y2="24.999" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="24.999" x2="75.49116875" y2="25.185040625" width="0.225" layer="94"/>
<wire x1="90.092" y1="23.43718125" x2="90.092" y2="23.649" width="0.225" layer="94"/>
<wire x1="90.092" y1="23.649" x2="90.092" y2="23.80016875" width="0.225" layer="94"/>
<wire x1="90.41016875" y1="23.90828125" x2="90.41016875" y2="23.874" width="0.225" layer="94"/>
<wire x1="90.41016875" y1="23.874" x2="90.41016875" y2="23.76881875" width="0.225" layer="94"/>
<wire x1="90.41016875" y1="23.76881875" x2="90.830209375" y2="23.76881875" width="0.225" layer="94"/>
<wire x1="90.43711875" y1="23.531809375" x2="90.43711875" y2="23.424" width="0.225" layer="94"/>
<wire x1="90.43711875" y1="23.424" x2="90.43711875" y2="23.35925" width="0.225" layer="94"/>
<wire x1="90.43711875" y1="23.531809375" x2="90.86316875" y2="23.531809375" width="0.225" layer="94"/>
<wire x1="90.092" y1="23.80016875" x2="90.09408125" y2="23.91038125" width="0.225" layer="94"/>
<wire x1="90.09408125" y1="23.91038125" x2="90.09926875" y2="23.9763" width="0.225" layer="94"/>
<wire x1="90.09926875" y1="23.9763" x2="90.106540625" y2="24.01976875" width="0.225" layer="94"/>
<wire x1="90.106540625" y1="24.01976875" x2="90.112390625" y2="24.04101875" width="0.225" layer="94"/>
<wire x1="90.112390625" y1="24.04101875" x2="90.12096875" y2="24.0613" width="0.225" layer="94"/>
<wire x1="90.12096875" y1="24.0613" x2="90.135190625" y2="24.077890625" width="0.225" layer="94"/>
<wire x1="90.135190625" y1="24.077890625" x2="90.1551" y2="24.0872" width="0.225" layer="94"/>
<wire x1="90.1551" y1="24.0872" x2="90.17645" y2="24.09263125" width="0.225" layer="94"/>
<wire x1="90.17645" y1="24.09263125" x2="90.19825" y2="24.0959" width="0.225" layer="94"/>
<wire x1="90.19825" y1="24.0959" x2="90.220209375" y2="24.097740625" width="0.225" layer="94"/>
<wire x1="90.220209375" y1="24.097740625" x2="90.242240625" y2="24.09858125" width="0.225" layer="94"/>
<wire x1="90.242240625" y1="24.09858125" x2="90.264290625" y2="24.098640625" width="0.225" layer="94"/>
<wire x1="90.264290625" y1="24.098640625" x2="90.28631875" y2="24.0978" width="0.225" layer="94"/>
<wire x1="90.28631875" y1="24.0978" x2="90.30826875" y2="24.09575" width="0.225" layer="94"/>
<wire x1="90.30826875" y1="24.09575" x2="90.33" y2="24.09208125" width="0.225" layer="94"/>
<wire x1="90.33" y1="24.09208125" x2="90.351140625" y2="24.085909375" width="0.225" layer="94"/>
<wire x1="90.351140625" y1="24.085909375" x2="90.3705" y2="24.075509375" width="0.225" layer="94"/>
<wire x1="90.3705" y1="24.075509375" x2="90.38486875" y2="24.058990625" width="0.225" layer="94"/>
<wire x1="90.38486875" y1="24.058990625" x2="90.40023125" y2="24.017840625" width="0.225" layer="94"/>
<wire x1="90.40023125" y1="24.017840625" x2="90.407140625" y2="23.97431875" width="0.225" layer="94"/>
<wire x1="90.407140625" y1="23.97431875" x2="90.41016875" y2="23.90828125" width="0.225" layer="94"/>
<wire x1="90.092" y1="23.43718125" x2="90.09478125" y2="23.31786875" width="0.225" layer="94"/>
<wire x1="90.09478125" y1="23.31786875" x2="90.09865" y2="23.270290625" width="0.225" layer="94"/>
<wire x1="90.09865" y1="23.270290625" x2="90.11223125" y2="23.20008125" width="0.225" layer="94"/>
<wire x1="90.11223125" y1="23.20008125" x2="90.121240625" y2="23.178009375" width="0.225" layer="94"/>
<wire x1="90.121240625" y1="23.178009375" x2="90.136040625" y2="23.1595" width="0.225" layer="94"/>
<wire x1="90.136040625" y1="23.1595" x2="90.15721875" y2="23.148690625" width="0.225" layer="94"/>
<wire x1="90.15721875" y1="23.148690625" x2="90.18025" y2="23.14248125" width="0.225" layer="94"/>
<wire x1="90.18025" y1="23.14248125" x2="90.20383125" y2="23.138809375" width="0.225" layer="94"/>
<wire x1="90.20383125" y1="23.138809375" x2="90.251459375" y2="23.135840625" width="0.225" layer="94"/>
<wire x1="90.251459375" y1="23.135840625" x2="90.299190625" y2="23.13656875" width="0.225" layer="94"/>
<wire x1="90.299190625" y1="23.13656875" x2="90.34663125" y2="23.141690625" width="0.225" layer="94"/>
<wire x1="90.34663125" y1="23.141690625" x2="90.391890625" y2="23.156190625" width="0.225" layer="94"/>
<wire x1="90.391890625" y1="23.156190625" x2="90.409190625" y2="23.17226875" width="0.225" layer="94"/>
<wire x1="90.409190625" y1="23.17226875" x2="90.419459375" y2="23.193759375" width="0.225" layer="94"/>
<wire x1="90.419459375" y1="23.193759375" x2="90.425940625" y2="23.21671875" width="0.225" layer="94"/>
<wire x1="90.425940625" y1="23.21671875" x2="90.4332" y2="23.26388125" width="0.225" layer="94"/>
<wire x1="90.4332" y1="23.26388125" x2="90.436309375" y2="23.31151875" width="0.225" layer="94"/>
<wire x1="90.436309375" y1="23.31151875" x2="90.43711875" y2="23.35925" width="0.225" layer="94"/>
<wire x1="89.66001875" y1="23.440109375" x2="89.67878125" y2="23.178659375" width="0.225" layer="94" curve="8.208716"/>
<wire x1="89.67878125" y1="23.178659375" x2="89.74463125" y2="22.974" width="0.225" layer="94" curve="19.254744"/>
<wire x1="89.74463125" y1="22.974" x2="89.76749375" y2="22.93364375" width="0.225" layer="94" curve="4.135389"/>
<wire x1="89.767490625" y1="22.933640625" x2="89.966896875" y2="22.769234375" width="0.225" layer="94" curve="37.791863"/>
<wire x1="89.9669" y1="22.769240625" x2="90.02831875" y2="22.749" width="0.225" layer="94" curve="4.744864"/>
<wire x1="90.02831875" y1="22.749" x2="90.22315" y2="22.719475" width="0.225" layer="94" curve="14.492988"/>
<wire x1="90.86316875" y1="23.531809375" x2="90.86316875" y2="23.424" width="0.225" layer="94"/>
<wire x1="90.86316875" y1="23.424" x2="90.86316875" y2="23.35603125" width="0.225" layer="94"/>
<wire x1="90.83383125" y1="23.09598125" x2="90.852559375" y2="23.199" width="0.225" layer="94" curve="5.141671"/>
<wire x1="90.852559375" y1="23.199" x2="90.86316875" y2="23.35603125" width="0.225" layer="94" curve="7.731903"/>
<wire x1="90.710259375" y1="22.86781875" x2="90.83383125" y2="23.09598125" width="0.225" layer="94" curve="31.131749"/>
<wire x1="90.483690625" y1="22.742109375" x2="90.710259375" y2="22.86781875" width="0.225" layer="94" curve="33.941784"/>
<wire x1="90.22315" y1="22.71946875" x2="90.483690625" y2="22.742103125" width="0.225" layer="94" curve="14.175253"/>
<wire x1="89.66001875" y1="23.440109375" x2="89.66001875" y2="23.649" width="0.225" layer="94"/>
<wire x1="89.66001875" y1="23.649" x2="89.66001875" y2="23.80016875" width="0.225" layer="94"/>
<wire x1="89.6773" y1="24.050590625" x2="89.66001875" y2="23.80016875" width="0.225" layer="94" curve="7.895583"/>
<wire x1="89.75726875" y1="24.28721875" x2="89.685840625" y2="24.099" width="0.225" layer="94" curve="17.336884"/>
<wire x1="89.685840625" y1="24.099" x2="89.6773" y2="24.050590625" width="0.225" layer="94" curve="4.218105"/>
<wire x1="89.93936875" y1="24.455190625" x2="89.75726875" y2="24.28721875" width="0.225" layer="94" curve="35.721204"/>
<wire x1="90.18176875" y1="24.51536875" x2="89.93936875" y2="24.455190625" width="0.225" layer="94" curve="21.771833"/>
<wire x1="90.830209375" y1="23.76881875" x2="90.830209375" y2="23.874" width="0.225" layer="94"/>
<wire x1="90.830209375" y1="23.874" x2="90.830209375" y2="23.95631875" width="0.225" layer="94"/>
<wire x1="90.830209375" y1="23.95631875" x2="90.7978375" y2="24.204540625" width="0.225" layer="94" curve="14.861165"/>
<wire x1="90.797840625" y1="24.204540625" x2="90.6616625" y2="24.411284375" width="0.225" layer="94" curve="37.022842"/>
<wire x1="90.661659375" y1="24.41128125" x2="90.432340625" y2="24.507065625" width="0.225" layer="94" curve="30.892228"/>
<wire x1="90.432340625" y1="24.50706875" x2="90.18176875" y2="24.515365625" width="0.225" layer="94" curve="10.656803"/>
<wire x1="68.6921" y1="31.404190625" x2="68.78723125" y2="31.299" width="0.225" layer="94"/>
<wire x1="68.78723125" y1="31.299" x2="69.804659375" y2="30.174" width="0.225" layer="94"/>
<wire x1="69.804659375" y1="30.174" x2="70.21163125" y2="29.724" width="0.225" layer="94"/>
<wire x1="70.21163125" y1="29.724" x2="71.229059375" y2="28.599" width="0.225" layer="94"/>
<wire x1="71.229059375" y1="28.599" x2="71.63603125" y2="28.149" width="0.225" layer="94"/>
<wire x1="71.63603125" y1="28.149" x2="72.653459375" y2="27.024" width="0.225" layer="94"/>
<wire x1="72.653459375" y1="27.024" x2="72.94835" y2="26.69793125" width="0.225" layer="94"/>
<wire x1="72.94835" y1="26.69793125" x2="73.44671875" y2="27.249" width="0.225" layer="94"/>
<wire x1="73.44671875" y1="27.249" x2="73.6502" y2="27.474" width="0.225" layer="94"/>
<wire x1="73.6502" y1="27.474" x2="74.26065" y2="28.149" width="0.225" layer="94"/>
<wire x1="74.26065" y1="28.149" x2="74.46413125" y2="28.374" width="0.225" layer="94"/>
<wire x1="74.46413125" y1="28.374" x2="75.278059375" y2="29.274" width="0.225" layer="94"/>
<wire x1="75.278059375" y1="29.274" x2="75.481540625" y2="29.499" width="0.225" layer="94"/>
<wire x1="75.481540625" y1="29.499" x2="76.29546875" y2="30.399" width="0.225" layer="94"/>
<wire x1="76.29546875" y1="30.399" x2="76.49895" y2="30.624" width="0.225" layer="94"/>
<wire x1="76.49895" y1="30.624" x2="76.70243125" y2="30.849" width="0.225" layer="94"/>
<wire x1="76.70243125" y1="30.849" x2="77.204709375" y2="31.404390625" width="0.225" layer="94"/>
<wire x1="77.204709375" y1="31.404390625" x2="76.036390625" y2="32.46103125" width="0.225" layer="94"/>
<wire x1="76.036390625" y1="32.46103125" x2="74.57861875" y2="30.849" width="0.225" layer="94"/>
<wire x1="74.57861875" y1="30.849" x2="74.37515" y2="30.624" width="0.225" layer="94"/>
<wire x1="74.37515" y1="30.624" x2="74.17168125" y2="30.399" width="0.225" layer="94"/>
<wire x1="74.17168125" y1="30.399" x2="73.764740625" y2="29.949" width="0.225" layer="94"/>
<wire x1="73.764740625" y1="29.949" x2="72.94835" y2="29.04621875" width="0.225" layer="94"/>
<wire x1="72.94835" y1="29.04621875" x2="72.742359375" y2="29.274" width="0.225" layer="94"/>
<wire x1="72.742359375" y1="29.274" x2="72.131940625" y2="29.949" width="0.225" layer="94"/>
<wire x1="72.131940625" y1="29.949" x2="71.724990625" y2="30.399" width="0.225" layer="94"/>
<wire x1="71.724990625" y1="30.399" x2="71.318040625" y2="30.849" width="0.225" layer="94"/>
<wire x1="71.318040625" y1="30.849" x2="70.911090625" y2="31.299" width="0.225" layer="94"/>
<wire x1="70.911090625" y1="31.299" x2="70.504140625" y2="31.749" width="0.225" layer="94"/>
<wire x1="70.504140625" y1="31.749" x2="70.097190625" y2="32.199" width="0.225" layer="94"/>
<wire x1="70.097190625" y1="32.199" x2="69.86021875" y2="32.461040625" width="0.225" layer="94"/>
<wire x1="81.341240625" y1="23.49255" x2="81.74326875" y2="23.49255" width="0.225" layer="94"/>
<wire x1="81.74326875" y1="23.49255" x2="81.74326875" y2="23.424" width="0.225" layer="94"/>
<wire x1="81.74326875" y1="23.424" x2="81.74326875" y2="23.272240625" width="0.225" layer="94"/>
<wire x1="81.723590625" y1="23.062390625" x2="81.743275" y2="23.272240625" width="0.225" layer="94" curve="10.717258"/>
<wire x1="81.637890625" y1="22.87138125" x2="81.7235875" y2="23.062390625" width="0.225" layer="94" curve="26.892331"/>
<wire x1="81.467640625" y1="22.751140625" x2="81.6378875" y2="22.87138125" width="0.225" layer="94" curve="34.316783"/>
<wire x1="81.260009375" y1="22.718559375" x2="81.467640625" y2="22.751140625" width="0.225" layer="94" curve="18.312496"/>
<wire x1="81.05158125" y1="22.74623125" x2="81.260009375" y2="22.718559375" width="0.225" layer="94" curve="14.64866"/>
<wire x1="80.880909375" y1="22.865009375" x2="81.05158125" y2="22.746234375" width="0.225" layer="94" curve="39.895242"/>
<wire x1="80.80178125" y1="23.0587" x2="80.880903125" y2="22.86500625" width="0.225" layer="94" curve="25.996239"/>
<wire x1="80.786140625" y1="23.26901875" x2="80.786140625" y2="23.424" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="23.424" x2="80.786140625" y2="23.649" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="23.649" x2="80.786140625" y2="23.874" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="23.874" x2="80.786140625" y2="24.086990625" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="23.26901875" x2="80.78728125" y2="23.199" width="0.225" layer="94" curve="3.295417"/>
<wire x1="80.78728125" y1="23.199" x2="80.801784375" y2="23.0587" width="0.225" layer="94" curve="6.640297"/>
<wire x1="81.206190625" y1="23.32908125" x2="81.206190625" y2="23.424" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="23.424" x2="81.206190625" y2="23.649" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="23.649" x2="81.206190625" y2="23.874" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="23.874" x2="81.206190625" y2="24.086990625" width="0.225" layer="94"/>
<wire x1="81.341240625" y1="23.49255" x2="81.341240625" y2="23.424" width="0.225" layer="94"/>
<wire x1="81.341240625" y1="23.424" x2="81.341240625" y2="23.239140625" width="0.225" layer="94"/>
<wire x1="81.317509375" y1="23.1254" x2="81.327084375" y2="23.1339125" width="0.225" layer="94" curve="49.818215"/>
<wire x1="81.30451875" y1="23.12235" x2="81.3175125" y2="23.12539375" width="0.225" layer="94" curve="7.103613"/>
<wire x1="81.291240625" y1="23.12093125" x2="81.30451875" y2="23.12235625" width="0.225" layer="94" curve="7.003084"/>
<wire x1="81.277890625" y1="23.120490625" x2="81.291240625" y2="23.120934375" width="0.225" layer="94" curve="1.449937"/>
<wire x1="81.26453125" y1="23.12073125" x2="81.277890625" y2="23.1204875" width="0.225" layer="94" curve="4.441603"/>
<wire x1="81.33501875" y1="23.15935" x2="81.33183125" y2="23.14638125" width="0.225" layer="94"/>
<wire x1="81.33183125" y1="23.14638125" x2="81.327090625" y2="23.133909375" width="0.225" layer="94"/>
<wire x1="81.33501875" y1="23.15935" x2="81.3373" y2="23.172509375" width="0.225" layer="94"/>
<wire x1="81.3373" y1="23.172509375" x2="81.340009375" y2="23.199" width="0.225" layer="94"/>
<wire x1="81.340009375" y1="23.199" x2="81.34001875" y2="23.199090625" width="0.225" layer="94"/>
<wire x1="81.34001875" y1="23.199090625" x2="81.341240625" y2="23.239140625" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="23.32908125" x2="81.20651875" y2="23.278159375" width="0.225" layer="94"/>
<wire x1="81.20651875" y1="23.278159375" x2="81.208009375" y2="23.22726875" width="0.225" layer="94"/>
<wire x1="81.208009375" y1="23.22726875" x2="81.209609375" y2="23.201859375" width="0.225" layer="94"/>
<wire x1="81.209609375" y1="23.201859375" x2="81.21228125" y2="23.176540625" width="0.225" layer="94"/>
<wire x1="81.21228125" y1="23.176540625" x2="81.21671875" y2="23.15148125" width="0.225" layer="94"/>
<wire x1="81.21671875" y1="23.15148125" x2="81.225321875" y2="23.12765" width="0.225" layer="94" curve="19.627877"/>
<wire x1="81.26453125" y1="23.12073125" x2="81.251209375" y2="23.12166875" width="0.225" layer="94"/>
<wire x1="81.238" y1="23.123609375" x2="81.251209375" y2="23.121671875" width="0.225" layer="94" curve="8.648156"/>
<wire x1="81.22531875" y1="23.12765" x2="81.238" y2="23.12360625" width="0.225" layer="94" curve="10.044676"/>
<wire x1="81.7013" y1="24.086990625" x2="81.206190625" y2="24.086990625" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="24.49186875" x2="81.7013" y2="24.49186875" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="24.861009375" x2="81.206190625" y2="24.774" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="24.774" x2="81.206190625" y2="24.549" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="24.549" x2="81.206190625" y2="24.49186875" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="24.49186875" x2="80.786140625" y2="24.549" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="24.549" x2="80.786140625" y2="24.774" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="24.774" x2="80.786140625" y2="24.861009375" width="0.225" layer="94"/>
<wire x1="80.627059375" y1="24.49186875" x2="80.786140625" y2="24.49186875" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="24.086990625" x2="80.627059375" y2="24.086990625" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="24.861009375" x2="81.206190625" y2="24.861009375" width="0.225" layer="94"/>
<wire x1="81.7013" y1="24.49186875" x2="81.7013" y2="24.324" width="0.225" layer="94"/>
<wire x1="81.7013" y1="24.324" x2="81.7013" y2="24.099" width="0.225" layer="94"/>
<wire x1="81.7013" y1="24.099" x2="81.7013" y2="24.086990625" width="0.225" layer="94"/>
<wire x1="80.627059375" y1="24.49186875" x2="80.627059375" y2="24.324" width="0.225" layer="94"/>
<wire x1="80.627059375" y1="24.324" x2="80.627059375" y2="24.099" width="0.225" layer="94"/>
<wire x1="80.627059375" y1="24.099" x2="80.627059375" y2="24.086990625" width="0.225" layer="94"/>
<wire x1="92.6429" y1="24.41253125" x2="92.469709375" y2="24.500159375" width="0.225" layer="94" curve="32.338936"/>
<wire x1="79.95228125" y1="23.531809375" x2="79.52623125" y2="23.531809375" width="0.225" layer="94"/>
<wire x1="79.49928125" y1="23.76881875" x2="79.91931875" y2="23.76881875" width="0.225" layer="94"/>
<wire x1="79.91931875" y1="23.76881875" x2="79.91931875" y2="23.874" width="0.225" layer="94"/>
<wire x1="79.91931875" y1="23.874" x2="79.91931875" y2="23.95631875" width="0.225" layer="94"/>
<wire x1="79.91931875" y1="23.95631875" x2="79.886946875" y2="24.204540625" width="0.225" layer="94" curve="14.861165"/>
<wire x1="79.88695" y1="24.204540625" x2="79.750771875" y2="24.411284375" width="0.225" layer="94" curve="37.022842"/>
<wire x1="79.75076875" y1="24.41128125" x2="79.521459375" y2="24.507065625" width="0.225" layer="94" curve="30.891086"/>
<wire x1="79.521459375" y1="24.50706875" x2="79.27088125" y2="24.515365625" width="0.225" layer="94" curve="10.657067"/>
<wire x1="79.27088125" y1="24.51536875" x2="79.02848125" y2="24.455190625" width="0.225" layer="94" curve="21.019898"/>
<wire x1="79.02848125" y1="24.455190625" x2="78.84638125" y2="24.28721875" width="0.225" layer="94" curve="36.47327"/>
<wire x1="78.84638125" y1="24.28721875" x2="78.77521875" y2="24.099" width="0.225" layer="94" curve="16.726592"/>
<wire x1="78.77521875" y1="24.099" x2="78.7664125" y2="24.050590625" width="0.225" layer="94" curve="4.076419"/>
<wire x1="78.766409375" y1="24.050590625" x2="78.75028125" y2="23.874" width="0.225" layer="94" curve="6.105964"/>
<wire x1="78.75028125" y1="23.874" x2="78.749128125" y2="23.80016875" width="0.225" layer="94" curve="2.541594"/>
<wire x1="79.95228125" y1="23.531809375" x2="79.95228125" y2="23.424" width="0.225" layer="94"/>
<wire x1="79.95228125" y1="23.424" x2="79.95228125" y2="23.35603125" width="0.225" layer="94"/>
<wire x1="79.922940625" y1="23.09598125" x2="79.94166875" y2="23.199" width="0.225" layer="94" curve="5.141671"/>
<wire x1="79.94166875" y1="23.199" x2="79.952278125" y2="23.35603125" width="0.225" layer="94" curve="7.731903"/>
<wire x1="79.79936875" y1="22.86781875" x2="79.922940625" y2="23.09598125" width="0.225" layer="94" curve="31.131749"/>
<wire x1="79.572809375" y1="22.742109375" x2="79.60065" y2="22.749" width="0.225" layer="94" curve="3.702975"/>
<wire x1="79.60065" y1="22.749" x2="79.799371875" y2="22.867815625" width="0.225" layer="94" curve="30.237845"/>
<wire x1="79.312259375" y1="22.71946875" x2="79.572809375" y2="22.74210625" width="0.225" layer="94" curve="14.175788"/>
<wire x1="79.056009375" y1="22.769240625" x2="79.119840625" y2="22.749" width="0.225" layer="94" curve="4.53295"/>
<wire x1="79.119840625" y1="22.749" x2="79.312259375" y2="22.719475" width="0.225" layer="94" curve="13.203582"/>
<wire x1="78.856609375" y1="22.933640625" x2="79.056009375" y2="22.7692375" width="0.225" layer="94" curve="39.29237"/>
<wire x1="78.767890625" y1="23.178659375" x2="78.85660625" y2="22.933640625" width="0.225" layer="94" curve="21.889189"/>
<wire x1="78.74913125" y1="23.440109375" x2="78.76789375" y2="23.178659375" width="0.225" layer="94" curve="9.71004"/>
<wire x1="78.74913125" y1="23.440109375" x2="78.74913125" y2="23.649" width="0.225" layer="94"/>
<wire x1="78.74913125" y1="23.649" x2="78.74913125" y2="23.80016875" width="0.225" layer="94"/>
<wire x1="79.49928125" y1="23.76881875" x2="79.49928125" y2="23.874" width="0.225" layer="94"/>
<wire x1="79.49928125" y1="23.874" x2="79.49928125" y2="23.90828125" width="0.225" layer="94"/>
<wire x1="79.49928125" y1="23.90828125" x2="79.496253125" y2="23.97431875" width="0.225" layer="94" curve="5.249884"/>
<wire x1="79.49625" y1="23.97431875" x2="79.48934375" y2="24.017840625" width="0.225" layer="94" curve="7.529032"/>
<wire x1="79.489340625" y1="24.017840625" x2="79.483196875" y2="24.039" width="0.225" layer="94" curve="6.832689"/>
<wire x1="79.4832" y1="24.039" x2="79.47398125" y2="24.058990625" width="0.225" layer="94" curve="10.282953"/>
<wire x1="79.47398125" y1="24.058990625" x2="79.459609375" y2="24.075509375" width="0.225" layer="94" curve="22.257449"/>
<wire x1="79.459609375" y1="24.075509375" x2="79.440259375" y2="24.085909375" width="0.225" layer="94" curve="19.188066"/>
<wire x1="79.440259375" y1="24.085909375" x2="79.39738125" y2="24.09575" width="0.225" layer="94" curve="11.463239"/>
<wire x1="79.39738125" y1="24.09575" x2="79.3534" y2="24.0986375" width="0.225" layer="94" curve="6.876925"/>
<wire x1="79.3534" y1="24.098640625" x2="79.30931875" y2="24.09774375" width="0.225" layer="94" curve="2.970191"/>
<wire x1="79.30931875" y1="24.097740625" x2="79.265559375" y2="24.092625" width="0.225" layer="94" curve="8.0302"/>
<wire x1="79.265559375" y1="24.09263125" x2="79.244209375" y2="24.0872" width="0.225" layer="94" curve="7.184068"/>
<wire x1="79.244209375" y1="24.0872" x2="79.2243" y2="24.077890625" width="0.225" layer="94" curve="14.385749"/>
<wire x1="79.2243" y1="24.077890625" x2="79.210078125" y2="24.0613" width="0.225" layer="94" curve="34.295039"/>
<wire x1="79.21008125" y1="24.0613" x2="79.191453125" y2="23.99813125" width="0.225" layer="94" curve="14.037627"/>
<wire x1="79.19145" y1="23.99813125" x2="79.1844125" y2="23.932390625" width="0.225" layer="94" curve="6.60122"/>
<wire x1="79.52623125" y1="23.531809375" x2="79.52623125" y2="23.424" width="0.225" layer="94"/>
<wire x1="79.52623125" y1="23.424" x2="79.52623125" y2="23.35925" width="0.225" layer="94"/>
<wire x1="79.51938125" y1="23.240190625" x2="79.526234375" y2="23.35925" width="0.225" layer="94" curve="6.589587"/>
<wire x1="79.50856875" y1="23.193759375" x2="79.519378125" y2="23.240190625" width="0.225" layer="94" curve="13.026604"/>
<wire x1="79.45898125" y1="23.1471" x2="79.508565625" y2="23.193759375" width="0.225" layer="94" curve="54.248333"/>
<wire x1="79.4121" y1="23.1384" x2="79.45898125" y2="23.147103125" width="0.225" layer="94" curve="11.235116"/>
<wire x1="79.292940625" y1="23.138809375" x2="79.4121" y2="23.138396875" width="0.225" layer="94" curve="10.193817"/>
<wire x1="79.24633125" y1="23.148690625" x2="79.292940625" y2="23.1388125" width="0.225" layer="94" curve="13.338819"/>
<wire x1="79.22515" y1="23.1595" x2="79.24633125" y2="23.1486875" width="0.225" layer="94" curve="16.819291"/>
<wire x1="79.21035" y1="23.178009375" x2="79.225146875" y2="23.15949375" width="0.225" layer="94" curve="31.833994"/>
<wire x1="79.195259375" y1="23.223159375" x2="79.21035625" y2="23.1780125" width="0.225" layer="94" curve="8.452794"/>
<wire x1="79.187759375" y1="23.270290625" x2="79.19525625" y2="23.223159375" width="0.225" layer="94" curve="10.449934"/>
<wire x1="79.187759375" y1="23.270290625" x2="79.18275" y2="23.341709375" width="0.225" layer="94"/>
<wire x1="79.18441875" y1="23.932390625" x2="79.181740625" y2="23.866309375" width="0.225" layer="94"/>
<wire x1="79.181740625" y1="23.866309375" x2="79.181109375" y2="23.80016875" width="0.225" layer="94"/>
<wire x1="79.181109375" y1="23.80016875" x2="79.181109375" y2="23.649" width="0.225" layer="94"/>
<wire x1="79.181109375" y1="23.649" x2="79.181109375" y2="23.43718125" width="0.225" layer="94"/>
<wire x1="79.181109375" y1="23.43718125" x2="79.18275" y2="23.341709375" width="0.225" layer="94"/>
<wire x1="77.1742" y1="23.882790625" x2="77.54935" y2="23.882790625" width="0.225" layer="94"/>
<wire x1="77.1742" y1="23.46881875" x2="77.969390625" y2="23.46881875" width="0.225" layer="94"/>
<wire x1="77.969390625" y1="23.46881875" x2="77.969390625" y2="23.649" width="0.225" layer="94"/>
<wire x1="77.969390625" y1="23.649" x2="77.969390625" y2="23.80016875" width="0.225" layer="94"/>
<wire x1="77.969390625" y1="23.80016875" x2="77.948609375" y2="24.07348125" width="0.225" layer="94" curve="8.696144"/>
<wire x1="77.948609375" y1="24.07348125" x2="77.84784375" y2="24.326196875" width="0.225" layer="94" curve="26.085225"/>
<wire x1="77.84785" y1="24.3262" x2="77.62745" y2="24.482209375" width="0.225" layer="94" curve="39.85306"/>
<wire x1="77.62745" y1="24.482209375" x2="77.35661875" y2="24.518828125" width="0.225" layer="94" curve="15.332321"/>
<wire x1="77.35661875" y1="24.51881875" x2="77.08588125" y2="24.48155" width="0.225" layer="94" curve="15.743623"/>
<wire x1="77.08588125" y1="24.48155" x2="76.865265625" y2="24.325728125" width="0.225" layer="94" curve="39.046943"/>
<wire x1="76.865259375" y1="24.32573125" x2="76.864040625" y2="24.324" width="0.225" layer="94" curve="0.204743"/>
<wire x1="76.864040625" y1="24.324" x2="76.763478125" y2="24.07343125" width="0.225" layer="94" curve="26.341038"/>
<wire x1="76.76348125" y1="24.07343125" x2="76.74395" y2="23.874" width="0.225" layer="94" curve="6.208124"/>
<wire x1="76.74395" y1="23.874" x2="76.74221875" y2="23.80016875" width="0.225" layer="94" curve="2.287022"/>
<wire x1="77.561359375" y1="23.44158125" x2="77.966390625" y2="23.44158125" width="0.225" layer="94"/>
<wire x1="77.966390625" y1="23.44158125" x2="77.966390625" y2="23.424" width="0.225" layer="94"/>
<wire x1="77.966390625" y1="23.424" x2="77.966390625" y2="23.2781" width="0.225" layer="94"/>
<wire x1="77.92765" y1="23.025709375" x2="77.966390625" y2="23.2781" width="0.225" layer="94" curve="17.452125"/>
<wire x1="77.77643125" y1="22.823190625" x2="77.92765" y2="23.025709375" width="0.225" layer="94" curve="38.592139"/>
<wire x1="77.53896875" y1="22.731940625" x2="77.618090625" y2="22.749" width="0.225" layer="94" curve="8.168491"/>
<wire x1="77.618090625" y1="22.749" x2="77.77643125" y2="22.823190625" width="0.225" layer="94" curve="17.702304"/>
<wire x1="77.28301875" y1="22.72058125" x2="77.53896875" y2="22.731940625" width="0.225" layer="94" curve="11.086795"/>
<wire x1="77.03403125" y1="22.77643125" x2="77.11308125" y2="22.749" width="0.225" layer="94" curve="6.296209"/>
<wire x1="77.11308125" y1="22.749" x2="77.28301875" y2="22.720578125" width="0.225" layer="94" curve="12.986111"/>
<wire x1="76.844159375" y1="22.943540625" x2="77.034028125" y2="22.776421875" width="0.225" layer="94" curve="38.138006"/>
<wire x1="76.760209375" y1="23.184359375" x2="76.8441625" y2="22.943540625" width="0.225" layer="94" curve="20.717901"/>
<wire x1="76.74221875" y1="23.440109375" x2="76.758" y2="23.199" width="0.225" layer="94" curve="9.114768"/>
<wire x1="76.758" y1="23.199" x2="76.760209375" y2="23.184359375" width="0.225" layer="94" curve="0.557951"/>
<wire x1="76.74221875" y1="23.440109375" x2="76.74221875" y2="23.649" width="0.225" layer="94"/>
<wire x1="76.74221875" y1="23.649" x2="76.74221875" y2="23.80016875" width="0.225" layer="94"/>
<wire x1="77.1742" y1="23.46881875" x2="77.1742" y2="23.43718125" width="0.225" layer="94"/>
<wire x1="77.1742" y1="23.43718125" x2="77.17648125" y2="23.323140625" width="0.225" layer="94"/>
<wire x1="77.17648125" y1="23.323140625" x2="77.179640625" y2="23.27763125" width="0.225" layer="94"/>
<wire x1="77.179640625" y1="23.27763125" x2="77.185759375" y2="23.23241875" width="0.225" layer="94"/>
<wire x1="77.185759375" y1="23.23241875" x2="77.19780625" y2="23.188490625" width="0.225" layer="94" curve="15.249939"/>
<wire x1="77.197809375" y1="23.188490625" x2="77.22771875" y2="23.15585" width="0.225" layer="94" curve="39.077584"/>
<wire x1="77.22771875" y1="23.15585" x2="77.2493" y2="23.148528125" width="0.225" layer="94" curve="18.445063"/>
<wire x1="77.2493" y1="23.14853125" x2="77.45375" y2="23.1434" width="0.225" layer="94" curve="16.158964"/>
<wire x1="77.45375" y1="23.1434" x2="77.498609375" y2="23.151559375" width="0.225" layer="94" curve="7.337797"/>
<wire x1="77.498609375" y1="23.151559375" x2="77.520209375" y2="23.1588375" width="0.225" layer="94" curve="9.286376"/>
<wire x1="77.561359375" y1="23.44158125" x2="77.561359375" y2="23.424" width="0.225" layer="94"/>
<wire x1="77.561359375" y1="23.424" x2="77.561359375" y2="23.28103125" width="0.225" layer="94"/>
<wire x1="77.561359375" y1="23.28103125" x2="77.559340625" y2="23.235459375" width="0.225" layer="94"/>
<wire x1="77.559340625" y1="23.235459375" x2="77.55613125" y2="23.21288125" width="0.225" layer="94"/>
<wire x1="77.520209375" y1="23.158840625" x2="77.556125" y2="23.21288125" width="0.225" layer="94" curve="51.030296"/>
<wire x1="77.184159375" y1="24.007240625" x2="77.174196875" y2="23.882790625" width="0.225" layer="94" curve="9.151585"/>
<wire x1="77.2077" y1="24.0734" x2="77.184159375" y2="24.007240625" width="0.225" layer="94" curve="20.872634"/>
<wire x1="77.54935" y1="23.882790625" x2="77.539775" y2="24.007290625" width="0.225" layer="94" curve="8.794744"/>
<wire x1="77.53978125" y1="24.007290625" x2="77.527525" y2="24.052734375" width="0.225" layer="94" curve="12.594721"/>
<wire x1="77.52751875" y1="24.05273125" x2="77.516803125" y2="24.073665625" width="0.225" layer="94" curve="11.429217"/>
<wire x1="77.516809375" y1="24.07366875" x2="77.5000125" y2="24.089975" width="0.225" layer="94" curve="26.064892"/>
<wire x1="77.500009375" y1="24.08996875" x2="77.47873125" y2="24.09995" width="0.225" layer="94" curve="11.965711"/>
<wire x1="77.47873125" y1="24.09995" x2="77.455990625" y2="24.10610625" width="0.225" layer="94" curve="8.00028"/>
<wire x1="77.455990625" y1="24.106109375" x2="77.38576875" y2="24.113571875" width="0.225" layer="94" curve="10.160203"/>
<wire x1="77.38576875" y1="24.11356875" x2="77.315109375" y2="24.112346875" width="0.225" layer="94" curve="3.952821"/>
<wire x1="77.315109375" y1="24.11235" x2="77.268440625" y2="24.106071875" width="0.225" layer="94" curve="9.390964"/>
<wire x1="77.268440625" y1="24.10606875" x2="77.245740625" y2="24.099796875" width="0.225" layer="94" curve="6.177099"/>
<wire x1="77.245740625" y1="24.0998" x2="77.24345" y2="24.099" width="0.225" layer="94" curve="1.397304"/>
<wire x1="77.24345" y1="24.099" x2="77.22448125" y2="24.089740625" width="0.225" layer="94" curve="12.178653"/>
<wire x1="77.22448125" y1="24.089740625" x2="77.207703125" y2="24.073396875" width="0.225" layer="94" curve="24.279547"/>
<wire x1="73.9395" y1="24.089740625" x2="73.922721875" y2="24.073396875" width="0.225" layer="94" curve="24.279547"/>
<wire x1="73.96075" y1="24.0998" x2="73.958459375" y2="24.099" width="0.225" layer="94" curve="1.397249"/>
<wire x1="73.958459375" y1="24.099" x2="73.939496875" y2="24.08974375" width="0.225" layer="94" curve="12.174138"/>
<wire x1="73.98345" y1="24.10606875" x2="73.96075" y2="24.099796875" width="0.225" layer="94" curve="6.177099"/>
<wire x1="74.03013125" y1="24.11235" x2="73.98345" y2="24.10606875" width="0.225" layer="94" curve="9.393557"/>
<wire x1="74.100790625" y1="24.11356875" x2="74.03013125" y2="24.112346875" width="0.225" layer="94" curve="3.952821"/>
<wire x1="74.171009375" y1="24.106109375" x2="74.100790625" y2="24.113571875" width="0.225" layer="94" curve="10.159745"/>
<wire x1="74.193740625" y1="24.09995" x2="74.171009375" y2="24.106103125" width="0.225" layer="94" curve="7.996993"/>
<wire x1="74.21501875" y1="24.08996875" x2="74.193740625" y2="24.09995" width="0.225" layer="94" curve="11.965711"/>
<wire x1="74.23181875" y1="24.07366875" x2="74.215021875" y2="24.089975" width="0.225" layer="94" curve="26.067092"/>
<wire x1="74.242540625" y1="24.05273125" x2="74.231821875" y2="24.073671875" width="0.225" layer="94" curve="11.432049"/>
<wire x1="74.254790625" y1="24.007290625" x2="74.2425375" y2="24.05273125" width="0.225" layer="94" curve="12.593993"/>
<wire x1="74.264359375" y1="23.882790625" x2="74.254784375" y2="24.007290625" width="0.225" layer="94" curve="8.794744"/>
<wire x1="73.92271875" y1="24.0734" x2="73.899178125" y2="24.007240625" width="0.225" layer="94" curve="20.872634"/>
<wire x1="73.89918125" y1="24.007240625" x2="73.88921875" y2="23.882790625" width="0.225" layer="94" curve="9.151622"/>
<wire x1="74.23521875" y1="23.158840625" x2="74.2678" y2="23.199" width="0.225" layer="94" curve="40.155698"/>
<wire x1="74.2678" y1="23.199" x2="74.271134375" y2="23.21288125" width="0.225" layer="94" curve="10.876304"/>
<wire x1="74.27435" y1="23.235459375" x2="74.271140625" y2="23.21288125" width="0.225" layer="94"/>
<wire x1="74.27638125" y1="23.28103125" x2="74.27435" y2="23.235459375" width="0.225" layer="94"/>
<wire x1="74.27638125" y1="23.44158125" x2="74.27638125" y2="23.424" width="0.225" layer="94"/>
<wire x1="74.27638125" y1="23.424" x2="74.27638125" y2="23.28103125" width="0.225" layer="94"/>
<wire x1="74.21361875" y1="23.151559375" x2="74.23521875" y2="23.1588375" width="0.225" layer="94" curve="9.286507"/>
<wire x1="74.16876875" y1="23.1434" x2="74.21361875" y2="23.151559375" width="0.225" layer="94" curve="7.336311"/>
<wire x1="73.964309375" y1="23.14853125" x2="74.16876875" y2="23.1434" width="0.225" layer="94" curve="16.159693"/>
<wire x1="73.94273125" y1="23.15585" x2="73.964309375" y2="23.14853125" width="0.225" layer="94" curve="18.441658"/>
<wire x1="73.91281875" y1="23.188490625" x2="73.942728125" y2="23.155846875" width="0.225" layer="94" curve="39.07874"/>
<wire x1="73.90078125" y1="23.23241875" x2="73.912828125" y2="23.18849375" width="0.225" layer="94" curve="15.249051"/>
<wire x1="73.894659375" y1="23.27763125" x2="73.90078125" y2="23.23241875" width="0.225" layer="94"/>
<wire x1="73.891490625" y1="23.323140625" x2="73.894659375" y2="23.27763125" width="0.225" layer="94"/>
<wire x1="73.88921875" y1="23.43718125" x2="73.88948125" y2="23.424" width="0.225" layer="94"/>
<wire x1="73.88948125" y1="23.424" x2="73.891490625" y2="23.323140625" width="0.225" layer="94"/>
<wire x1="73.88921875" y1="23.46881875" x2="73.88921875" y2="23.43718125" width="0.225" layer="94"/>
<wire x1="73.457240625" y1="23.440109375" x2="73.457240625" y2="23.649" width="0.225" layer="94"/>
<wire x1="73.457240625" y1="23.649" x2="73.457240625" y2="23.80016875" width="0.225" layer="94"/>
<wire x1="73.457240625" y1="23.440109375" x2="73.47523125" y2="23.184359375" width="0.225" layer="94" curve="9.672758"/>
<wire x1="73.47523125" y1="23.184359375" x2="73.559184375" y2="22.94354375" width="0.225" layer="94" curve="20.717853"/>
<wire x1="73.55918125" y1="22.943540625" x2="73.749046875" y2="22.776425" width="0.225" layer="94" curve="38.137176"/>
<wire x1="73.74905" y1="22.77643125" x2="73.998040625" y2="22.72058125" width="0.225" layer="94" curve="19.282594"/>
<wire x1="73.998040625" y1="22.72058125" x2="74.25398125" y2="22.731940625" width="0.225" layer="94" curve="11.086392"/>
<wire x1="74.25398125" y1="22.731940625" x2="74.3331" y2="22.749" width="0.225" layer="94" curve="8.168179"/>
<wire x1="74.3331" y1="22.749" x2="74.491446875" y2="22.82319375" width="0.225" layer="94" curve="17.703003"/>
<wire x1="74.49145" y1="22.823190625" x2="74.642665625" y2="23.025709375" width="0.225" layer="94" curve="38.59175"/>
<wire x1="74.64266875" y1="23.025709375" x2="74.681409375" y2="23.2781" width="0.225" layer="94" curve="17.451998"/>
<wire x1="74.681409375" y1="23.44158125" x2="74.681409375" y2="23.424" width="0.225" layer="94"/>
<wire x1="74.681409375" y1="23.424" x2="74.681409375" y2="23.2781" width="0.225" layer="94"/>
<wire x1="74.27638125" y1="23.44158125" x2="74.681409375" y2="23.44158125" width="0.225" layer="94"/>
<wire x1="73.4785" y1="24.07343125" x2="73.45896875" y2="23.874" width="0.225" layer="94" curve="6.208134"/>
<wire x1="73.45896875" y1="23.874" x2="73.4572375" y2="23.80016875" width="0.225" layer="94" curve="2.287026"/>
<wire x1="73.58028125" y1="24.32573125" x2="73.4785" y2="24.07343125" width="0.225" layer="94" curve="26.545833"/>
<wire x1="73.8009" y1="24.48155" x2="73.58028125" y2="24.32573125" width="0.225" layer="94" curve="39.047008"/>
<wire x1="74.071640625" y1="24.51881875" x2="73.8009" y2="24.48155" width="0.225" layer="94" curve="15.743809"/>
<wire x1="74.342459375" y1="24.482209375" x2="74.071640625" y2="24.518825" width="0.225" layer="94" curve="15.331635"/>
<wire x1="74.562859375" y1="24.3262" x2="74.342459375" y2="24.482209375" width="0.225" layer="94" curve="39.85306"/>
<wire x1="74.66363125" y1="24.07348125" x2="74.5628625" y2="24.326203125" width="0.225" layer="94" curve="26.085633"/>
<wire x1="74.684409375" y1="23.80016875" x2="74.663628125" y2="24.07348125" width="0.225" layer="94" curve="8.69613"/>
<wire x1="74.684409375" y1="23.46881875" x2="74.684409375" y2="23.649" width="0.225" layer="94"/>
<wire x1="74.684409375" y1="23.649" x2="74.684409375" y2="23.80016875" width="0.225" layer="94"/>
<wire x1="73.88921875" y1="23.46881875" x2="74.684409375" y2="23.46881875" width="0.225" layer="94"/>
<wire x1="73.88921875" y1="23.882790625" x2="74.264359375" y2="23.882790625" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="22.74548125" x2="82.92818125" y2="22.74548125" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="24.49186875" x2="82.80066875" y2="24.49186875" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="24.49186875" x2="82.508140625" y2="24.324" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="24.324" x2="82.508140625" y2="24.099" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="24.099" x2="82.508140625" y2="23.874" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="23.874" x2="82.508140625" y2="23.649" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="23.649" x2="82.508140625" y2="23.424" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="23.424" x2="82.508140625" y2="23.199" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="23.199" x2="82.508140625" y2="22.974" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="22.974" x2="82.508140625" y2="22.749" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="22.749" x2="82.508140625" y2="22.74548125" width="0.225" layer="94"/>
<wire x1="83.18028125" y1="23.76266875" x2="83.585309375" y2="23.76266875" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="23.818340625" x2="82.92818125" y2="23.649" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="23.649" x2="82.92818125" y2="23.424" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="23.424" x2="82.92818125" y2="23.199" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="23.199" x2="82.92818125" y2="22.974" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="22.974" x2="82.92818125" y2="22.749" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="22.749" x2="82.92818125" y2="22.74548125" width="0.225" layer="94"/>
<wire x1="82.8883" y1="24.44991875" x2="82.80066875" y2="24.49186875" width="0.225" layer="94" curve="51.162574"/>
<wire x1="86.810090625" y1="22.74548125" x2="86.810090625" y2="22.749" width="0.225" layer="94"/>
<wire x1="86.810090625" y1="22.749" x2="86.810090625" y2="22.974" width="0.225" layer="94"/>
<wire x1="86.810090625" y1="22.974" x2="86.810090625" y2="23.199" width="0.225" layer="94"/>
<wire x1="86.810090625" y1="23.199" x2="86.810090625" y2="23.424" width="0.225" layer="94"/>
<wire x1="86.810090625" y1="23.424" x2="86.810090625" y2="23.649" width="0.225" layer="94"/>
<wire x1="86.810090625" y1="23.649" x2="86.810090625" y2="23.71023125" width="0.225" layer="94"/>
<wire x1="86.39005" y1="22.74548125" x2="86.39005" y2="22.749" width="0.225" layer="94"/>
<wire x1="86.39005" y1="22.749" x2="86.39005" y2="22.974" width="0.225" layer="94"/>
<wire x1="86.39005" y1="22.974" x2="86.39005" y2="23.199" width="0.225" layer="94"/>
<wire x1="86.39005" y1="23.199" x2="86.39005" y2="23.424" width="0.225" layer="94"/>
<wire x1="86.39005" y1="23.424" x2="86.39005" y2="23.649" width="0.225" layer="94"/>
<wire x1="86.39005" y1="23.649" x2="86.39005" y2="23.874" width="0.225" layer="94"/>
<wire x1="86.39005" y1="23.874" x2="86.39005" y2="24.099" width="0.225" layer="94"/>
<wire x1="86.39005" y1="24.099" x2="86.39005" y2="24.324" width="0.225" layer="94"/>
<wire x1="86.39005" y1="24.324" x2="86.39005" y2="24.49186875" width="0.225" layer="94"/>
<wire x1="86.39005" y1="22.74548125" x2="86.810090625" y2="22.74548125" width="0.225" layer="94"/>
<wire x1="87.1462" y1="22.74548125" x2="87.566240625" y2="22.74548125" width="0.225" layer="94"/>
<wire x1="86.68258125" y1="24.49186875" x2="86.39005" y2="24.49186875" width="0.225" layer="94"/>
<wire x1="86.770490625" y1="24.44956875" x2="86.68258125" y2="24.49186875" width="0.225" layer="94" curve="51.389654"/>
<wire x1="83.585309375" y1="23.76266875" x2="83.585309375" y2="23.874" width="0.225" layer="94"/>
<wire x1="83.585309375" y1="23.874" x2="83.585309375" y2="23.992359375" width="0.225" layer="94"/>
<wire x1="83.585309375" y1="23.992359375" x2="83.57331875" y2="24.1609" width="0.225" layer="94" curve="8.13878"/>
<wire x1="83.57331875" y1="24.1609" x2="83.52315625" y2="24.3217" width="0.225" layer="94" curve="18.374099"/>
<wire x1="83.523159375" y1="24.3217" x2="83.41396875" y2="24.4487" width="0.225" layer="94" curve="28.349931"/>
<wire x1="83.41396875" y1="24.4487" x2="83.25806875" y2="24.510721875" width="0.225" layer="94" curve="26.884991"/>
<wire x1="83.25806875" y1="24.51071875" x2="83.0895" y2="24.515371875" width="0.225" layer="94" curve="13.343084"/>
<wire x1="83.0895" y1="24.51538125" x2="82.888296875" y2="24.449925" width="0.225" layer="94" curve="25.861473"/>
<wire x1="87.566240625" y1="22.74548125" x2="87.566240625" y2="22.749" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="22.749" x2="87.566240625" y2="22.974" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="22.974" x2="87.566240625" y2="23.199" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="23.199" x2="87.566240625" y2="23.424" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="23.424" x2="87.566240625" y2="23.649" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="23.649" x2="87.566240625" y2="23.874" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="23.874" x2="87.566240625" y2="23.878390625" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="23.878390625" x2="87.555215625" y2="24.066540625" width="0.225" layer="94" curve="6.706552"/>
<wire x1="87.55521875" y1="24.066540625" x2="87.509115625" y2="24.248871875" width="0.225" layer="94" curve="14.968121"/>
<wire x1="87.509109375" y1="24.24886875" x2="87.403925" y2="24.403603125" width="0.225" layer="94" curve="25.064472"/>
<wire x1="87.40393125" y1="24.403609375" x2="87.240590625" y2="24.494540625" width="0.225" layer="94" curve="28.312195"/>
<wire x1="87.240590625" y1="24.494540625" x2="87.05421875" y2="24.518825" width="0.225" layer="94" curve="15.049352"/>
<wire x1="87.05421875" y1="24.51881875" x2="86.770490625" y2="24.44956875" width="0.225" layer="94" curve="27.231353"/>
<wire x1="87.1462" y1="22.74548125" x2="87.1462" y2="22.749" width="0.225" layer="94"/>
<wire x1="87.1462" y1="22.749" x2="87.1462" y2="22.974" width="0.225" layer="94"/>
<wire x1="87.1462" y1="22.974" x2="87.1462" y2="23.199" width="0.225" layer="94"/>
<wire x1="87.1462" y1="23.199" x2="87.1462" y2="23.424" width="0.225" layer="94"/>
<wire x1="87.1462" y1="23.424" x2="87.1462" y2="23.649" width="0.225" layer="94"/>
<wire x1="87.1462" y1="23.649" x2="87.1462" y2="23.8242" width="0.225" layer="94"/>
<wire x1="87.1462" y1="23.8242" x2="87.139771875" y2="23.962909375" width="0.225" layer="94" curve="5.307835"/>
<wire x1="87.13976875" y1="23.962909375" x2="87.123915625" y2="24.044609375" width="0.225" layer="94" curve="11.346275"/>
<wire x1="87.12391875" y1="24.044609375" x2="87.11383125" y2="24.07046875" width="0.225" layer="94" curve="9.30383"/>
<wire x1="87.11383125" y1="24.07046875" x2="87.098490625" y2="24.093490625" width="0.225" layer="94" curve="15.447128"/>
<wire x1="87.098490625" y1="24.093490625" x2="87.0752" y2="24.108190625" width="0.225" layer="94" curve="32.662799"/>
<wire x1="87.0752" y1="24.108190625" x2="86.99275" y2="24.116371875" width="0.225" layer="94" curve="20.527762"/>
<wire x1="86.99275" y1="24.11636875" x2="86.9651" y2="24.113715625" width="0.225" layer="94" curve="1.764695"/>
<wire x1="86.9651" y1="24.11371875" x2="86.912034375" y2="24.098084375" width="0.225" layer="94" curve="20.107123"/>
<wire x1="86.91203125" y1="24.098090625" x2="86.870678125" y2="24.061821875" width="0.225" layer="94" curve="29.565942"/>
<wire x1="86.87068125" y1="24.06181875" x2="86.84560625" y2="24.01240625" width="0.225" layer="94" curve="14.114289"/>
<wire x1="86.8456" y1="24.012409375" x2="86.824978125" y2="23.931759375" width="0.225" layer="94" curve="11.013017"/>
<wire x1="86.82498125" y1="23.931759375" x2="86.813053125" y2="23.82131875" width="0.225" layer="94" curve="5.344301"/>
<wire x1="86.81305" y1="23.82131875" x2="86.810090625" y2="23.71023125" width="0.225" layer="94" curve="3.932748"/>
<wire x1="83.18028125" y1="23.76266875" x2="83.18028125" y2="23.874" width="0.225" layer="94"/>
<wire x1="83.18028125" y1="23.874" x2="83.18028125" y2="23.947240625" width="0.225" layer="94"/>
<wire x1="83.18028125" y1="23.947240625" x2="83.176690625" y2="24.02661875" width="0.225" layer="94" curve="5.181151"/>
<wire x1="83.176690625" y1="24.02661875" x2="83.16605625" y2="24.085190625" width="0.225" layer="94" curve="10.220705"/>
<wire x1="83.166059375" y1="24.085190625" x2="83.12728125" y2="24.124134375" width="0.225" layer="94" curve="58.95127"/>
<wire x1="83.12728125" y1="24.12413125" x2="82.99755" y2="24.099" width="0.225" layer="94" curve="53.217192"/>
<wire x1="82.99755" y1="24.099" x2="82.95845" y2="24.053540625" width="0.225" layer="94" curve="23.452395"/>
<wire x1="82.95845" y1="24.053540625" x2="82.95161875" y2="24.034890625" width="0.225" layer="94"/>
<wire x1="82.95161875" y1="24.034890625" x2="82.946159375" y2="24.01578125" width="0.225" layer="94"/>
<wire x1="82.946159375" y1="24.01578125" x2="82.941775" y2="23.9964" width="0.225" layer="94" curve="6.402047"/>
<wire x1="82.94176875" y1="23.9964" x2="82.931515625" y2="23.91761875" width="0.225" layer="94" curve="4.264451"/>
<wire x1="82.93151875" y1="23.91761875" x2="82.930209375" y2="23.897790625" width="0.225" layer="94"/>
<wire x1="82.930209375" y1="23.897790625" x2="82.92865" y2="23.85808125" width="0.225" layer="94"/>
<wire x1="82.92865" y1="23.85808125" x2="82.92818125" y2="23.818340625" width="0.225" layer="94"/>
<wire x1="92.67961875" y1="23.68455" x2="92.54725" y2="23.77605" width="0.225" layer="94" curve="24.406928"/>
<wire x1="92.54725" y1="23.77605" x2="92.391190625" y2="23.818428125" width="0.225" layer="94" curve="14.517731"/>
<wire x1="92.391190625" y1="23.81841875" x2="92.2218" y2="23.8435" width="0.225" layer="94"/>
<wire x1="92.2218" y1="23.8435" x2="92.13726875" y2="23.85681875" width="0.225" layer="94"/>
<wire x1="92.09533125" y1="23.86531875" x2="92.13726875" y2="23.85681875" width="0.225" layer="94" curve="5.000072"/>
<wire x1="92.0546" y1="23.87821875" x2="92.0661" y2="23.874" width="0.225" layer="94" curve="2.068384"/>
<wire x1="92.0661" y1="23.874" x2="92.09533125" y2="23.865321875" width="0.225" layer="94" curve="5.150249"/>
<wire x1="92.054009375" y1="23.890409375" x2="92.054596875" y2="23.87821875" width="0.225" layer="94" curve="5.514941"/>
<wire x1="92.054009375" y1="23.890409375" x2="92.054009375" y2="24.03718125" width="0.225" layer="94"/>
<wire x1="92.05665" y1="24.07703125" x2="92.0540125" y2="24.03718125" width="0.225" layer="94" curve="7.573746"/>
<wire x1="92.061659375" y1="24.09633125" x2="92.056646875" y2="24.07703125" width="0.225" layer="94" curve="13.971436"/>
<wire x1="92.068809375" y1="24.102740625" x2="92.0616625" y2="24.096328125" width="0.225" layer="94" curve="41.625207"/>
<wire x1="92.097790625" y1="24.110090625" x2="92.068809375" y2="24.102740625" width="0.225" layer="94" curve="13.721902"/>
<wire x1="92.15758125" y1="24.113940625" x2="92.097790625" y2="24.110090625" width="0.225" layer="94" curve="7.371099"/>
<wire x1="92.15758125" y1="24.113940625" x2="92.2566" y2="24.113940625" width="0.225" layer="94"/>
<wire x1="92.29483125" y1="24.112240625" x2="92.2566" y2="24.113940625" width="0.225" layer="94" curve="5.089448"/>
<wire x1="92.32143125" y1="24.107590625" x2="92.29483125" y2="24.112240625" width="0.225" layer="94" curve="9.654072"/>
<wire x1="92.331859375" y1="24.10346875" x2="92.32143125" y2="24.1075875" width="0.225" layer="94" curve="13.639596"/>
<wire x1="92.33591875" y1="24.09303125" x2="92.33185625" y2="24.10346875" width="0.225" layer="94" curve="15.08669"/>
<wire x1="92.34036875" y1="24.0665" x2="92.335915625" y2="24.09303125" width="0.225" layer="94" curve="8.390546"/>
<wire x1="92.34215" y1="24.028390625" x2="92.340375" y2="24.0665" width="0.225" layer="94" curve="5.332675"/>
<wire x1="92.34215" y1="24.028390625" x2="92.34215" y2="23.874" width="0.225" layer="94"/>
<wire x1="92.34215" y1="23.874" x2="92.34215" y2="23.873709375" width="0.225" layer="94"/>
<wire x1="92.34215" y1="23.873709375" x2="92.759190625" y2="23.873709375" width="0.225" layer="94"/>
<wire x1="92.759190625" y1="23.873709375" x2="92.759190625" y2="23.874" width="0.225" layer="94"/>
<wire x1="92.759190625" y1="23.874" x2="92.759190625" y2="24.0492" width="0.225" layer="94"/>
<wire x1="92.759190625" y1="24.0492" x2="92.738496875" y2="24.24398125" width="0.225" layer="94" curve="12.129007"/>
<wire x1="92.738490625" y1="24.24398125" x2="92.642896875" y2="24.412528125" width="0.225" layer="94" curve="34.862523"/>
<wire x1="92.469709375" y1="24.500159375" x2="92.27461875" y2="24.518825" width="0.225" layer="94" curve="10.407762"/>
<wire x1="92.27461875" y1="24.51881875" x2="92.139559375" y2="24.51881875" width="0.225" layer="94"/>
<wire x1="92.139559375" y1="24.51881875" x2="91.94439375" y2="24.4967625" width="0.225" layer="94" curve="15.066235"/>
<wire x1="91.944390625" y1="24.49676875" x2="91.76905625" y2="24.410671875" width="0.225" layer="94" curve="24.344315"/>
<wire x1="91.769059375" y1="24.41066875" x2="91.656853125" y2="24.25153125" width="0.225" layer="94" curve="32.9752"/>
<wire x1="91.65685" y1="24.25153125" x2="91.626409375" y2="24.099" width="0.225" layer="94" curve="14.82531"/>
<wire x1="91.626409375" y1="24.099" x2="91.62503125" y2="24.05828125" width="0.225" layer="94" curve="3.873269"/>
<wire x1="91.62503125" y1="24.05828125" x2="91.62503125" y2="23.874" width="0.225" layer="94"/>
<wire x1="91.62503125" y1="23.874" x2="91.62503125" y2="23.85436875" width="0.225" layer="94"/>
<wire x1="91.62503125" y1="23.85436875" x2="91.654403125" y2="23.68111875" width="0.225" layer="94" curve="19.244585"/>
<wire x1="91.6544" y1="23.68111875" x2="91.767428125" y2="23.548775" width="0.225" layer="94" curve="42.510013"/>
<wire x1="91.76743125" y1="23.54878125" x2="91.931890625" y2="23.48583125" width="0.225" layer="94" curve="14.601117"/>
<wire x1="91.931890625" y1="23.48583125" x2="92.105790625" y2="23.452246875" width="0.225" layer="94" curve="5.428189"/>
<wire x1="92.105790625" y1="23.45225" x2="92.16531875" y2="23.44243125" width="0.225" layer="94"/>
<wire x1="92.16531875" y1="23.44243125" x2="92.22491875" y2="23.43266875" width="0.225" layer="94"/>
<wire x1="92.22491875" y1="23.43266875" x2="92.28428125" y2="23.42156875" width="0.225" layer="94"/>
<wire x1="92.327340625" y1="23.40943125" x2="92.28428125" y2="23.4215625" width="0.225" layer="94" curve="10.274433"/>
<wire x1="92.34125" y1="23.40243125" x2="92.32734375" y2="23.4094375" width="0.225" layer="94" curve="11.730311"/>
<wire x1="92.346959375" y1="23.37516875" x2="92.34124375" y2="23.402428125" width="0.225" layer="94" curve="13.906979"/>
<wire x1="92.34815" y1="23.347240625" x2="92.346959375" y2="23.37516875" width="0.225" layer="94" curve="4.888175"/>
<wire x1="92.34815" y1="23.347240625" x2="92.34815" y2="23.2031" width="0.225" layer="94"/>
<wire x1="92.346759375" y1="23.17286875" x2="92.348153125" y2="23.2031" width="0.225" layer="94" curve="5.283856"/>
<wire x1="92.34045" y1="23.143359375" x2="92.34675625" y2="23.17286875" width="0.225" layer="94" curve="13.55484"/>
<wire x1="92.333959375" y1="23.13608125" x2="92.340453125" y2="23.143359375" width="0.225" layer="94" curve="45.791754"/>
<wire x1="92.314840625" y1="23.12976875" x2="92.333959375" y2="23.136084375" width="0.225" layer="94" curve="14.183972"/>
<wire x1="92.274840625" y1="23.12463125" x2="92.314840625" y2="23.129771875" width="0.225" layer="94" curve="7.7277"/>
<wire x1="92.24416875" y1="23.123709375" x2="92.274840625" y2="23.124628125" width="0.225" layer="94" curve="3.484382"/>
<wire x1="92.24416875" y1="23.123709375" x2="92.1096" y2="23.123709375" width="0.225" layer="94"/>
<wire x1="92.07173125" y1="23.12515" x2="92.1096" y2="23.12370625" width="0.225" layer="94" curve="4.363033"/>
<wire x1="92.04318125" y1="23.12956875" x2="92.07173125" y2="23.12514375" width="0.225" layer="94" curve="8.889144"/>
<wire x1="92.03463125" y1="23.132309375" x2="92.04318125" y2="23.12956875" width="0.225" layer="94" curve="9.043139"/>
<wire x1="92.03186875" y1="23.140859375" x2="92.034628125" y2="23.132309375" width="0.225" layer="94" curve="7.986668"/>
<wire x1="92.02805" y1="23.16145" x2="92.031871875" y2="23.140859375" width="0.225" layer="94" curve="6.761788"/>
<wire x1="92.02548125" y1="23.19128125" x2="92.028053125" y2="23.16145" width="0.225" layer="94" curve="4.406702"/>
<wire x1="92.024059375" y1="23.25115" x2="92.025484375" y2="23.19128125" width="0.225" layer="94" curve="2.724283"/>
<wire x1="92.024059375" y1="23.25115" x2="92.024059375" y2="23.424" width="0.225" layer="94"/>
<wire x1="92.024059375" y1="23.424" x2="92.024059375" y2="23.45651875" width="0.225" layer="94"/>
<wire x1="92.024059375" y1="23.45651875" x2="91.60701875" y2="23.45651875" width="0.225" layer="94"/>
<wire x1="91.60701875" y1="23.45651875" x2="91.60701875" y2="23.424" width="0.225" layer="94"/>
<wire x1="91.60701875" y1="23.424" x2="91.60701875" y2="23.199" width="0.225" layer="94"/>
<wire x1="91.60701875" y1="23.199" x2="91.60701875" y2="23.188159375" width="0.225" layer="94"/>
<wire x1="91.60701875" y1="23.188159375" x2="91.6277125" y2="22.99338125" width="0.225" layer="94" curve="12.128778"/>
<wire x1="91.627709375" y1="22.99338125" x2="91.72330625" y2="22.824828125" width="0.225" layer="94" curve="34.863434"/>
<wire x1="91.723309375" y1="22.82483125" x2="91.85098125" y2="22.749" width="0.225" layer="94" curve="24.602823"/>
<wire x1="91.85098125" y1="22.749" x2="91.896490625" y2="22.737203125" width="0.225" layer="94" curve="7.735491"/>
<wire x1="91.896490625" y1="22.7372" x2="92.091590625" y2="22.718534375" width="0.225" layer="94" curve="10.40831"/>
<wire x1="92.091590625" y1="22.71853125" x2="92.262609375" y2="22.71853125" width="0.225" layer="94"/>
<wire x1="92.262609375" y1="22.71853125" x2="92.457790625" y2="22.7405875" width="0.225" layer="94" curve="15.067174"/>
<wire x1="92.457790625" y1="22.740590625" x2="92.633140625" y2="22.82668125" width="0.225" layer="94" curve="24.336812"/>
<wire x1="92.633140625" y1="22.82668125" x2="92.74113125" y2="22.974" width="0.225" layer="94" curve="30.878359"/>
<wire x1="92.74113125" y1="22.974" x2="92.74536875" y2="22.985809375" width="0.225" layer="94" curve="2.095521"/>
<wire x1="92.74536875" y1="22.985809375" x2="92.777203125" y2="23.17908125" width="0.225" layer="94" curve="18.707757"/>
<wire x1="92.777209375" y1="23.17908125" x2="92.777209375" y2="23.199" width="0.225" layer="94"/>
<wire x1="92.777209375" y1="23.199" x2="92.777209375" y2="23.38328125" width="0.225" layer="94"/>
<wire x1="92.777209375" y1="23.38328125" x2="92.756978125" y2="23.543621875" width="0.225" layer="94" curve="14.383055"/>
<wire x1="92.75696875" y1="23.54361875" x2="92.6796125" y2="23.68454375" width="0.225" layer="94" curve="28.759193"/>
<wire x1="64.9221" y1="35.32731875" x2="101.4471" y2="35.32731875" width="0.225" layer="94"/>
<wire x1="101.4471" y1="35.32731875" x2="101.4471" y2="35.124" width="0.225" layer="94"/>
<wire x1="101.4471" y1="35.124" x2="101.4471" y2="34.899" width="0.225" layer="94"/>
<wire x1="101.4471" y1="34.899" x2="101.4471" y2="34.674" width="0.225" layer="94"/>
<wire x1="101.4471" y1="34.674" x2="101.4471" y2="34.449" width="0.225" layer="94"/>
<wire x1="101.4471" y1="34.449" x2="101.4471" y2="34.224" width="0.225" layer="94"/>
<wire x1="101.4471" y1="34.224" x2="101.4471" y2="33.999" width="0.225" layer="94"/>
<wire x1="101.4471" y1="33.999" x2="101.4471" y2="33.774" width="0.225" layer="94"/>
<wire x1="101.4471" y1="33.774" x2="101.4471" y2="33.549" width="0.225" layer="94"/>
<wire x1="101.4471" y1="33.549" x2="101.4471" y2="33.324" width="0.225" layer="94"/>
<wire x1="101.4471" y1="33.324" x2="101.4471" y2="33.099" width="0.225" layer="94"/>
<wire x1="101.4471" y1="33.099" x2="101.4471" y2="32.874" width="0.225" layer="94"/>
<wire x1="101.4471" y1="32.874" x2="101.4471" y2="32.649" width="0.225" layer="94"/>
<wire x1="101.4471" y1="32.649" x2="101.4471" y2="32.424" width="0.225" layer="94"/>
<wire x1="101.4471" y1="32.424" x2="101.4471" y2="32.199" width="0.225" layer="94"/>
<wire x1="101.4471" y1="32.199" x2="101.4471" y2="31.974" width="0.225" layer="94"/>
<wire x1="101.4471" y1="31.974" x2="101.4471" y2="31.749" width="0.225" layer="94"/>
<wire x1="101.4471" y1="31.749" x2="101.4471" y2="31.524" width="0.225" layer="94"/>
<wire x1="101.4471" y1="31.524" x2="101.4471" y2="31.299" width="0.225" layer="94"/>
<wire x1="101.4471" y1="31.299" x2="101.4471" y2="31.074" width="0.225" layer="94"/>
<wire x1="101.4471" y1="31.074" x2="101.4471" y2="30.849" width="0.225" layer="94"/>
<wire x1="101.4471" y1="30.849" x2="101.4471" y2="30.624" width="0.225" layer="94"/>
<wire x1="101.4471" y1="30.624" x2="101.4471" y2="30.399" width="0.225" layer="94"/>
<wire x1="101.4471" y1="30.399" x2="101.4471" y2="30.174" width="0.225" layer="94"/>
<wire x1="101.4471" y1="30.174" x2="101.4471" y2="29.949" width="0.225" layer="94"/>
<wire x1="101.4471" y1="29.949" x2="101.4471" y2="29.724" width="0.225" layer="94"/>
<wire x1="101.4471" y1="29.724" x2="101.4471" y2="29.499" width="0.225" layer="94"/>
<wire x1="101.4471" y1="29.499" x2="101.4471" y2="29.274" width="0.225" layer="94"/>
<wire x1="101.4471" y1="29.274" x2="101.4471" y2="29.049" width="0.225" layer="94"/>
<wire x1="101.4471" y1="29.049" x2="101.4471" y2="28.824" width="0.225" layer="94"/>
<wire x1="101.4471" y1="28.824" x2="101.4471" y2="28.599" width="0.225" layer="94"/>
<wire x1="101.4471" y1="28.599" x2="101.4471" y2="28.374" width="0.225" layer="94"/>
<wire x1="101.4471" y1="28.374" x2="101.4471" y2="28.149" width="0.225" layer="94"/>
<wire x1="101.4471" y1="28.149" x2="101.4471" y2="27.924" width="0.225" layer="94"/>
<wire x1="101.4471" y1="27.924" x2="101.4471" y2="27.699" width="0.225" layer="94"/>
<wire x1="101.4471" y1="27.699" x2="101.4471" y2="27.474" width="0.225" layer="94"/>
<wire x1="101.4471" y1="27.474" x2="101.4471" y2="27.249" width="0.225" layer="94"/>
<wire x1="101.4471" y1="27.249" x2="101.4471" y2="27.024" width="0.225" layer="94"/>
<wire x1="101.4471" y1="27.024" x2="101.4471" y2="26.799" width="0.225" layer="94"/>
<wire x1="101.4471" y1="26.799" x2="101.4471" y2="26.574" width="0.225" layer="94"/>
<wire x1="101.4471" y1="26.574" x2="101.4471" y2="26.349" width="0.225" layer="94"/>
<wire x1="101.4471" y1="26.349" x2="101.4471" y2="26.124" width="0.225" layer="94"/>
<wire x1="101.4471" y1="26.124" x2="101.4471" y2="25.899" width="0.225" layer="94"/>
<wire x1="101.4471" y1="25.899" x2="101.4471" y2="25.674" width="0.225" layer="94"/>
<wire x1="101.4471" y1="25.674" x2="101.4471" y2="25.449" width="0.225" layer="94"/>
<wire x1="101.4471" y1="25.449" x2="101.4471" y2="25.224" width="0.225" layer="94"/>
<wire x1="101.4471" y1="25.224" x2="101.4471" y2="24.999" width="0.225" layer="94"/>
<wire x1="101.4471" y1="24.999" x2="101.4471" y2="24.774" width="0.225" layer="94"/>
<wire x1="101.4471" y1="24.774" x2="101.4471" y2="24.549" width="0.225" layer="94"/>
<wire x1="101.4471" y1="24.549" x2="101.4471" y2="24.324" width="0.225" layer="94"/>
<wire x1="101.4471" y1="24.324" x2="101.4471" y2="24.099" width="0.225" layer="94"/>
<wire x1="101.4471" y1="24.099" x2="101.4471" y2="23.874" width="0.225" layer="94"/>
<wire x1="101.4471" y1="23.874" x2="101.4471" y2="23.649" width="0.225" layer="94"/>
<wire x1="101.4471" y1="23.649" x2="101.4471" y2="23.424" width="0.225" layer="94"/>
<wire x1="101.4471" y1="23.424" x2="101.4471" y2="23.199" width="0.225" layer="94"/>
<wire x1="101.4471" y1="23.199" x2="101.4471" y2="22.974" width="0.225" layer="94"/>
<wire x1="101.4471" y1="22.974" x2="101.4471" y2="22.749" width="0.225" layer="94"/>
<wire x1="101.4471" y1="22.749" x2="101.4471" y2="22.524" width="0.225" layer="94"/>
<wire x1="101.4471" y1="22.524" x2="101.4471" y2="22.299" width="0.225" layer="94"/>
<wire x1="101.4471" y1="22.299" x2="101.4471" y2="22.074" width="0.225" layer="94"/>
<wire x1="101.4471" y1="22.074" x2="101.4471" y2="21.849" width="0.225" layer="94"/>
<wire x1="101.4471" y1="21.849" x2="101.4471" y2="21.624" width="0.225" layer="94"/>
<wire x1="101.4471" y1="21.624" x2="101.4471" y2="21.399" width="0.225" layer="94"/>
<wire x1="101.4471" y1="21.399" x2="101.4471" y2="21.174" width="0.225" layer="94"/>
<wire x1="101.4471" y1="21.174" x2="101.4471" y2="20.949" width="0.225" layer="94"/>
<wire x1="101.4471" y1="20.949" x2="101.4471" y2="20.724" width="0.225" layer="94"/>
<wire x1="101.4471" y1="20.724" x2="101.4471" y2="20.55231875" width="0.225" layer="94"/>
<wire x1="101.4471" y1="20.55231875" x2="64.9221" y2="20.55231875" width="0.225" layer="94"/>
<wire x1="64.9221" y1="20.55231875" x2="64.9221" y2="20.724" width="0.225" layer="94"/>
<wire x1="64.9221" y1="20.724" x2="64.9221" y2="20.949" width="0.225" layer="94"/>
<wire x1="64.9221" y1="20.949" x2="64.9221" y2="21.174" width="0.225" layer="94"/>
<wire x1="64.9221" y1="21.174" x2="64.9221" y2="21.399" width="0.225" layer="94"/>
<wire x1="64.9221" y1="21.399" x2="64.9221" y2="21.624" width="0.225" layer="94"/>
<wire x1="64.9221" y1="21.624" x2="64.9221" y2="21.849" width="0.225" layer="94"/>
<wire x1="64.9221" y1="21.849" x2="64.9221" y2="22.074" width="0.225" layer="94"/>
<wire x1="64.9221" y1="22.074" x2="64.9221" y2="22.299" width="0.225" layer="94"/>
<wire x1="64.9221" y1="22.299" x2="64.9221" y2="22.524" width="0.225" layer="94"/>
<wire x1="64.9221" y1="22.524" x2="64.9221" y2="22.749" width="0.225" layer="94"/>
<wire x1="64.9221" y1="22.749" x2="64.9221" y2="22.974" width="0.225" layer="94"/>
<wire x1="64.9221" y1="22.974" x2="64.9221" y2="23.199" width="0.225" layer="94"/>
<wire x1="64.9221" y1="23.199" x2="64.9221" y2="23.424" width="0.225" layer="94"/>
<wire x1="64.9221" y1="23.424" x2="64.9221" y2="23.649" width="0.225" layer="94"/>
<wire x1="64.9221" y1="23.649" x2="64.9221" y2="23.874" width="0.225" layer="94"/>
<wire x1="64.9221" y1="23.874" x2="64.9221" y2="24.099" width="0.225" layer="94"/>
<wire x1="64.9221" y1="24.099" x2="64.9221" y2="24.324" width="0.225" layer="94"/>
<wire x1="64.9221" y1="24.324" x2="64.9221" y2="24.549" width="0.225" layer="94"/>
<wire x1="64.9221" y1="24.549" x2="64.9221" y2="24.774" width="0.225" layer="94"/>
<wire x1="64.9221" y1="24.774" x2="64.9221" y2="24.999" width="0.225" layer="94"/>
<wire x1="64.9221" y1="24.999" x2="64.9221" y2="25.224" width="0.225" layer="94"/>
<wire x1="64.9221" y1="25.224" x2="64.9221" y2="25.449" width="0.225" layer="94"/>
<wire x1="64.9221" y1="25.449" x2="64.9221" y2="25.674" width="0.225" layer="94"/>
<wire x1="64.9221" y1="25.674" x2="64.9221" y2="25.899" width="0.225" layer="94"/>
<wire x1="64.9221" y1="25.899" x2="64.9221" y2="26.124" width="0.225" layer="94"/>
<wire x1="64.9221" y1="26.124" x2="64.9221" y2="26.349" width="0.225" layer="94"/>
<wire x1="64.9221" y1="26.349" x2="64.9221" y2="26.574" width="0.225" layer="94"/>
<wire x1="64.9221" y1="26.574" x2="64.9221" y2="26.799" width="0.225" layer="94"/>
<wire x1="64.9221" y1="26.799" x2="64.9221" y2="27.024" width="0.225" layer="94"/>
<wire x1="64.9221" y1="27.024" x2="64.9221" y2="27.249" width="0.225" layer="94"/>
<wire x1="64.9221" y1="27.249" x2="64.9221" y2="27.474" width="0.225" layer="94"/>
<wire x1="64.9221" y1="27.474" x2="64.9221" y2="27.699" width="0.225" layer="94"/>
<wire x1="64.9221" y1="27.699" x2="64.9221" y2="27.924" width="0.225" layer="94"/>
<wire x1="64.9221" y1="27.924" x2="64.9221" y2="28.149" width="0.225" layer="94"/>
<wire x1="64.9221" y1="28.149" x2="64.9221" y2="28.374" width="0.225" layer="94"/>
<wire x1="64.9221" y1="28.374" x2="64.9221" y2="28.599" width="0.225" layer="94"/>
<wire x1="64.9221" y1="28.599" x2="64.9221" y2="28.824" width="0.225" layer="94"/>
<wire x1="64.9221" y1="28.824" x2="64.9221" y2="29.049" width="0.225" layer="94"/>
<wire x1="64.9221" y1="29.049" x2="64.9221" y2="29.274" width="0.225" layer="94"/>
<wire x1="64.9221" y1="29.274" x2="64.9221" y2="29.499" width="0.225" layer="94"/>
<wire x1="64.9221" y1="29.499" x2="64.9221" y2="29.724" width="0.225" layer="94"/>
<wire x1="64.9221" y1="29.724" x2="64.9221" y2="29.949" width="0.225" layer="94"/>
<wire x1="64.9221" y1="29.949" x2="64.9221" y2="30.174" width="0.225" layer="94"/>
<wire x1="64.9221" y1="30.174" x2="64.9221" y2="30.399" width="0.225" layer="94"/>
<wire x1="64.9221" y1="30.399" x2="64.9221" y2="30.624" width="0.225" layer="94"/>
<wire x1="64.9221" y1="30.624" x2="64.9221" y2="30.849" width="0.225" layer="94"/>
<wire x1="64.9221" y1="30.849" x2="64.9221" y2="31.074" width="0.225" layer="94"/>
<wire x1="64.9221" y1="31.074" x2="64.9221" y2="31.299" width="0.225" layer="94"/>
<wire x1="64.9221" y1="31.299" x2="64.9221" y2="31.524" width="0.225" layer="94"/>
<wire x1="64.9221" y1="31.524" x2="64.9221" y2="31.749" width="0.225" layer="94"/>
<wire x1="64.9221" y1="31.749" x2="64.9221" y2="31.974" width="0.225" layer="94"/>
<wire x1="64.9221" y1="31.974" x2="64.9221" y2="32.199" width="0.225" layer="94"/>
<wire x1="64.9221" y1="32.199" x2="64.9221" y2="32.424" width="0.225" layer="94"/>
<wire x1="64.9221" y1="32.424" x2="64.9221" y2="32.649" width="0.225" layer="94"/>
<wire x1="64.9221" y1="32.649" x2="64.9221" y2="32.874" width="0.225" layer="94"/>
<wire x1="64.9221" y1="32.874" x2="64.9221" y2="33.099" width="0.225" layer="94"/>
<wire x1="64.9221" y1="33.099" x2="64.9221" y2="33.324" width="0.225" layer="94"/>
<wire x1="64.9221" y1="33.324" x2="64.9221" y2="33.549" width="0.225" layer="94"/>
<wire x1="64.9221" y1="33.549" x2="64.9221" y2="33.774" width="0.225" layer="94"/>
<wire x1="64.9221" y1="33.774" x2="64.9221" y2="33.999" width="0.225" layer="94"/>
<wire x1="64.9221" y1="33.999" x2="64.9221" y2="34.224" width="0.225" layer="94"/>
<wire x1="64.9221" y1="34.224" x2="64.9221" y2="34.449" width="0.225" layer="94"/>
<wire x1="64.9221" y1="34.449" x2="64.9221" y2="34.674" width="0.225" layer="94"/>
<wire x1="64.9221" y1="34.674" x2="64.9221" y2="34.899" width="0.225" layer="94"/>
<wire x1="64.9221" y1="34.899" x2="64.9221" y2="35.124" width="0.225" layer="94"/>
<wire x1="64.9221" y1="35.124" x2="64.9221" y2="35.32731875" width="0.225" layer="94"/>
<wire x1="64.9221" y1="35.124" x2="101.4471" y2="35.124" width="0.225" layer="94"/>
<wire x1="64.9221" y1="34.899" x2="101.4471" y2="34.899" width="0.225" layer="94"/>
<wire x1="64.9221" y1="34.674" x2="101.4471" y2="34.674" width="0.225" layer="94"/>
<wire x1="64.9221" y1="34.449" x2="101.4471" y2="34.449" width="0.225" layer="94"/>
<wire x1="64.9221" y1="34.224" x2="101.4471" y2="34.224" width="0.225" layer="94"/>
<wire x1="64.9221" y1="33.999" x2="101.4471" y2="33.999" width="0.225" layer="94"/>
<wire x1="64.9221" y1="33.774" x2="101.4471" y2="33.774" width="0.225" layer="94"/>
<wire x1="64.9221" y1="33.549" x2="101.4471" y2="33.549" width="0.225" layer="94"/>
<wire x1="64.9221" y1="33.324" x2="101.4471" y2="33.324" width="0.225" layer="94"/>
<wire x1="64.9221" y1="33.099" x2="101.4471" y2="33.099" width="0.225" layer="94"/>
<wire x1="64.9221" y1="32.874" x2="101.4471" y2="32.874" width="0.225" layer="94"/>
<wire x1="64.9221" y1="32.649" x2="101.4471" y2="32.649" width="0.225" layer="94"/>
<wire x1="64.9221" y1="32.424" x2="69.81928125" y2="32.424" width="0.225" layer="94"/>
<wire x1="76.07733125" y1="32.424" x2="101.4471" y2="32.424" width="0.225" layer="94"/>
<wire x1="69.89371875" y1="32.424" x2="76.0029" y2="32.424" width="0.225" layer="94"/>
<wire x1="64.9221" y1="32.199" x2="69.570590625" y2="32.199" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="32.199" x2="101.4471" y2="32.199" width="0.225" layer="94"/>
<wire x1="70.097190625" y1="32.199" x2="75.79943125" y2="32.199" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="32.199" x2="90.66211875" y2="32.199" width="0.225" layer="94"/>
<wire x1="76.326109375" y1="32.199" x2="78.286090625" y2="32.199" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="32.199" x2="87.068640625" y2="32.199" width="0.225" layer="94"/>
<wire x1="64.9221" y1="31.974" x2="69.3219" y2="31.974" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="31.974" x2="101.4471" y2="31.974" width="0.225" layer="94"/>
<wire x1="70.30066875" y1="31.974" x2="75.595959375" y2="31.974" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="31.974" x2="90.66211875" y2="31.974" width="0.225" layer="94"/>
<wire x1="76.574890625" y1="31.974" x2="78.286090625" y2="31.974" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="31.974" x2="87.068640625" y2="31.974" width="0.225" layer="94"/>
<wire x1="64.9221" y1="31.749" x2="69.073209375" y2="31.749" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="31.749" x2="101.4471" y2="31.749" width="0.225" layer="94"/>
<wire x1="70.504140625" y1="31.749" x2="75.3925" y2="31.749" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="31.749" x2="90.66211875" y2="31.749" width="0.225" layer="94"/>
<wire x1="76.82366875" y1="31.749" x2="78.286090625" y2="31.749" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="31.749" x2="87.068640625" y2="31.749" width="0.225" layer="94"/>
<wire x1="64.9221" y1="31.524" x2="68.82451875" y2="31.524" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="31.524" x2="101.4471" y2="31.524" width="0.225" layer="94"/>
<wire x1="70.70761875" y1="31.524" x2="75.18903125" y2="31.524" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="31.524" x2="90.66211875" y2="31.524" width="0.225" layer="94"/>
<wire x1="77.07245" y1="31.524" x2="78.286090625" y2="31.524" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="31.524" x2="87.068640625" y2="31.524" width="0.225" layer="94"/>
<wire x1="64.9221" y1="31.299" x2="68.78723125" y2="31.299" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="31.299" x2="101.4471" y2="31.299" width="0.225" layer="94"/>
<wire x1="70.911090625" y1="31.299" x2="74.985559375" y2="31.299" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="31.299" x2="90.66211875" y2="31.299" width="0.225" layer="94"/>
<wire x1="77.1094" y1="31.299" x2="78.286090625" y2="31.299" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="31.299" x2="87.068640625" y2="31.299" width="0.225" layer="94"/>
<wire x1="64.9221" y1="31.074" x2="68.990709375" y2="31.074" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="31.074" x2="101.4471" y2="31.074" width="0.225" layer="94"/>
<wire x1="71.114559375" y1="31.074" x2="74.782090625" y2="31.074" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="31.074" x2="90.66211875" y2="31.074" width="0.225" layer="94"/>
<wire x1="76.905909375" y1="31.074" x2="78.286090625" y2="31.074" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="31.074" x2="87.068640625" y2="31.074" width="0.225" layer="94"/>
<wire x1="64.9221" y1="30.849" x2="69.1942" y2="30.849" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="30.849" x2="101.4471" y2="30.849" width="0.225" layer="94"/>
<wire x1="71.318040625" y1="30.849" x2="74.57861875" y2="30.849" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="30.849" x2="90.66211875" y2="30.849" width="0.225" layer="94"/>
<wire x1="76.70243125" y1="30.849" x2="78.286090625" y2="30.849" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="30.849" x2="87.068640625" y2="30.849" width="0.225" layer="94"/>
<wire x1="64.9221" y1="30.624" x2="69.397690625" y2="30.624" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="30.624" x2="101.4471" y2="30.624" width="0.225" layer="94"/>
<wire x1="71.521509375" y1="30.624" x2="74.37515" y2="30.624" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="30.624" x2="93.287040625" y2="30.624" width="0.225" layer="94"/>
<wire x1="76.49895" y1="30.624" x2="87.068640625" y2="30.624" width="0.225" layer="94"/>
<wire x1="64.9221" y1="30.399" x2="69.60116875" y2="30.399" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="30.399" x2="101.4471" y2="30.399" width="0.225" layer="94"/>
<wire x1="71.724990625" y1="30.399" x2="74.17168125" y2="30.399" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="30.399" x2="93.287040625" y2="30.399" width="0.225" layer="94"/>
<wire x1="76.29546875" y1="30.399" x2="87.068640625" y2="30.399" width="0.225" layer="94"/>
<wire x1="64.9221" y1="30.174" x2="69.804659375" y2="30.174" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="30.174" x2="101.4471" y2="30.174" width="0.225" layer="94"/>
<wire x1="71.928459375" y1="30.174" x2="73.968209375" y2="30.174" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="30.174" x2="93.287040625" y2="30.174" width="0.225" layer="94"/>
<wire x1="76.091990625" y1="30.174" x2="78.286240625" y2="30.174" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="30.174" x2="87.068640625" y2="30.174" width="0.225" layer="94"/>
<wire x1="64.9221" y1="29.949" x2="70.008140625" y2="29.949" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="29.949" x2="101.4471" y2="29.949" width="0.225" layer="94"/>
<wire x1="72.131940625" y1="29.949" x2="73.764740625" y2="29.949" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="29.949" x2="93.287040625" y2="29.949" width="0.225" layer="94"/>
<wire x1="75.8885" y1="29.949" x2="78.286240625" y2="29.949" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="29.949" x2="87.068640625" y2="29.949" width="0.225" layer="94"/>
<wire x1="64.9221" y1="29.724" x2="70.21163125" y2="29.724" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="29.724" x2="101.4471" y2="29.724" width="0.225" layer="94"/>
<wire x1="72.335409375" y1="29.724" x2="73.56126875" y2="29.724" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="29.724" x2="93.287040625" y2="29.724" width="0.225" layer="94"/>
<wire x1="75.68501875" y1="29.724" x2="78.286240625" y2="29.724" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="29.724" x2="87.068640625" y2="29.724" width="0.225" layer="94"/>
<wire x1="64.9221" y1="29.499" x2="70.415109375" y2="29.499" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="29.499" x2="101.4471" y2="29.499" width="0.225" layer="94"/>
<wire x1="72.53888125" y1="29.499" x2="73.3578" y2="29.499" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="29.499" x2="93.287040625" y2="29.499" width="0.225" layer="94"/>
<wire x1="75.481540625" y1="29.499" x2="78.286240625" y2="29.499" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="29.499" x2="87.068640625" y2="29.499" width="0.225" layer="94"/>
<wire x1="64.9221" y1="29.274" x2="70.6186" y2="29.274" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="29.274" x2="101.4471" y2="29.274" width="0.225" layer="94"/>
<wire x1="72.742359375" y1="29.274" x2="73.15433125" y2="29.274" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="29.274" x2="93.287040625" y2="29.274" width="0.225" layer="94"/>
<wire x1="75.278059375" y1="29.274" x2="78.286240625" y2="29.274" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="29.274" x2="87.068640625" y2="29.274" width="0.225" layer="94"/>
<wire x1="64.9221" y1="29.049" x2="70.822090625" y2="29.049" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="29.049" x2="101.4471" y2="29.049" width="0.225" layer="94"/>
<wire x1="72.94583125" y1="29.049" x2="72.950859375" y2="29.049" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="29.049" x2="93.287040625" y2="29.049" width="0.225" layer="94"/>
<wire x1="75.07458125" y1="29.049" x2="78.286240625" y2="29.049" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="29.049" x2="87.068640625" y2="29.049" width="0.225" layer="94"/>
<wire x1="64.9221" y1="28.824" x2="71.02556875" y2="28.824" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="28.824" x2="101.4471" y2="28.824" width="0.225" layer="94"/>
<wire x1="74.871090625" y1="28.824" x2="87.068640625" y2="28.824" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="28.824" x2="93.287040625" y2="28.824" width="0.225" layer="94"/>
<wire x1="64.9221" y1="28.599" x2="71.229059375" y2="28.599" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="28.599" x2="101.4471" y2="28.599" width="0.225" layer="94"/>
<wire x1="74.667609375" y1="28.599" x2="87.068640625" y2="28.599" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="28.599" x2="93.287040625" y2="28.599" width="0.225" layer="94"/>
<wire x1="64.9221" y1="28.374" x2="71.432540625" y2="28.374" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="28.374" x2="101.4471" y2="28.374" width="0.225" layer="94"/>
<wire x1="74.46413125" y1="28.374" x2="78.286090625" y2="28.374" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="28.374" x2="93.287040625" y2="28.374" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="28.374" x2="87.068640625" y2="28.374" width="0.225" layer="94"/>
<wire x1="64.9221" y1="28.149" x2="71.63603125" y2="28.149" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="28.149" x2="101.4471" y2="28.149" width="0.225" layer="94"/>
<wire x1="74.26065" y1="28.149" x2="78.286090625" y2="28.149" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="28.149" x2="93.287040625" y2="28.149" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="28.149" x2="87.068640625" y2="28.149" width="0.225" layer="94"/>
<wire x1="64.9221" y1="27.924" x2="71.839509375" y2="27.924" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="27.924" x2="101.4471" y2="27.924" width="0.225" layer="94"/>
<wire x1="74.057159375" y1="27.924" x2="78.286090625" y2="27.924" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="27.924" x2="93.287040625" y2="27.924" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="27.924" x2="87.068640625" y2="27.924" width="0.225" layer="94"/>
<wire x1="64.9221" y1="27.699" x2="72.043" y2="27.699" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="27.699" x2="101.4471" y2="27.699" width="0.225" layer="94"/>
<wire x1="73.85368125" y1="27.699" x2="78.286090625" y2="27.699" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="27.699" x2="93.287040625" y2="27.699" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="27.699" x2="87.068640625" y2="27.699" width="0.225" layer="94"/>
<wire x1="64.9221" y1="27.474" x2="72.246490625" y2="27.474" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="27.474" x2="101.4471" y2="27.474" width="0.225" layer="94"/>
<wire x1="73.6502" y1="27.474" x2="78.286090625" y2="27.474" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="27.474" x2="93.287040625" y2="27.474" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="27.474" x2="87.068640625" y2="27.474" width="0.225" layer="94"/>
<wire x1="64.9221" y1="27.249" x2="72.44996875" y2="27.249" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="27.249" x2="101.4471" y2="27.249" width="0.225" layer="94"/>
<wire x1="73.44671875" y1="27.249" x2="78.286090625" y2="27.249" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="27.249" x2="93.287040625" y2="27.249" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="27.249" x2="87.068640625" y2="27.249" width="0.225" layer="94"/>
<wire x1="64.9221" y1="27.024" x2="72.653459375" y2="27.024" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="27.024" x2="101.4471" y2="27.024" width="0.225" layer="94"/>
<wire x1="73.243240625" y1="27.024" x2="78.286090625" y2="27.024" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="27.024" x2="93.287040625" y2="27.024" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="27.024" x2="87.068640625" y2="27.024" width="0.225" layer="94"/>
<wire x1="64.9221" y1="26.799" x2="72.856940625" y2="26.799" width="0.225" layer="94"/>
<wire x1="73.03975" y1="26.799" x2="101.4471" y2="26.799" width="0.225" layer="94"/>
<wire x1="64.9221" y1="26.574" x2="101.4471" y2="26.574" width="0.225" layer="94"/>
<wire x1="64.9221" y1="26.349" x2="101.4471" y2="26.349" width="0.225" layer="94"/>
<wire x1="64.9221" y1="26.124" x2="101.4471" y2="26.124" width="0.225" layer="94"/>
<wire x1="64.9221" y1="25.899" x2="101.4471" y2="25.899" width="0.225" layer="94"/>
<wire x1="64.9221" y1="25.674" x2="101.4471" y2="25.674" width="0.225" layer="94"/>
<wire x1="64.9221" y1="25.449" x2="101.4471" y2="25.449" width="0.225" layer="94"/>
<wire x1="64.9221" y1="25.224" x2="101.4471" y2="25.224" width="0.225" layer="94"/>
<wire x1="64.9221" y1="24.999" x2="75.49116875" y2="24.999" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="24.999" x2="101.4471" y2="24.999" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="24.999" x2="88.41505" y2="24.999" width="0.225" layer="94"/>
<wire x1="64.9221" y1="24.774" x2="75.49116875" y2="24.774" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="24.774" x2="101.4471" y2="24.774" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="24.774" x2="80.786140625" y2="24.774" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="24.774" x2="88.41505" y2="24.774" width="0.225" layer="94"/>
<wire x1="64.9221" y1="24.549" x2="75.49116875" y2="24.549" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="24.549" x2="101.4471" y2="24.549" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="24.549" x2="80.786140625" y2="24.549" width="0.225" layer="94"/>
<wire x1="64.9221" y1="24.324" x2="73.579059375" y2="24.324" width="0.225" layer="94"/>
<wire x1="92.70988125" y1="24.324" x2="101.4471" y2="24.324" width="0.225" layer="94"/>
<wire x1="74.56438125" y1="24.324" x2="75.49116875" y2="24.324" width="0.225" layer="94"/>
<wire x1="90.74325" y1="24.324" x2="91.69128125" y2="24.324" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="24.324" x2="76.864040625" y2="24.324" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="24.324" x2="89.78075" y2="24.324" width="0.225" layer="94"/>
<wire x1="77.84936875" y1="24.324" x2="78.869559375" y2="24.324" width="0.225" layer="94"/>
<wire x1="87.470340625" y1="24.324" x2="88.41505" y2="24.324" width="0.225" layer="94"/>
<wire x1="79.832359375" y1="24.324" x2="80.627059375" y2="24.324" width="0.225" layer="94"/>
<wire x1="85.42721875" y1="24.324" x2="86.39005" y2="24.324" width="0.225" layer="94"/>
<wire x1="81.7013" y1="24.324" x2="82.508140625" y2="24.324" width="0.225" layer="94"/>
<wire x1="83.522" y1="24.324" x2="84.441909375" y2="24.324" width="0.225" layer="94"/>
<wire x1="64.9221" y1="24.099" x2="73.48298125" y2="24.099" width="0.225" layer="94"/>
<wire x1="92.75785" y1="24.099" x2="101.4471" y2="24.099" width="0.225" layer="94"/>
<wire x1="73.958459375" y1="24.099" x2="74.19638125" y2="24.099" width="0.225" layer="94"/>
<wire x1="92.06346875" y1="24.099" x2="92.334359375" y2="24.099" width="0.225" layer="94"/>
<wire x1="74.659159375" y1="24.099" x2="75.49116875" y2="24.099" width="0.225" layer="94"/>
<wire x1="90.81963125" y1="24.099" x2="91.626409375" y2="24.099" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="24.099" x2="76.76796875" y2="24.099" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="24.099" x2="89.685840625" y2="24.099" width="0.225" layer="94"/>
<wire x1="77.24345" y1="24.099" x2="77.48136875" y2="24.099" width="0.225" layer="94"/>
<wire x1="87.55065" y1="24.099" x2="88.41505" y2="24.099" width="0.225" layer="94"/>
<wire x1="77.94415" y1="24.099" x2="78.77521875" y2="24.099" width="0.225" layer="94"/>
<wire x1="86.913890625" y1="24.099" x2="87.09276875" y2="24.099" width="0.225" layer="94"/>
<wire x1="79.90875" y1="24.099" x2="80.627059375" y2="24.099" width="0.225" layer="94"/>
<wire x1="85.522" y1="24.099" x2="86.39005" y2="24.099" width="0.225" layer="94"/>
<wire x1="81.7013" y1="24.099" x2="82.508140625" y2="24.099" width="0.225" layer="94"/>
<wire x1="83.58053125" y1="24.099" x2="84.345909375" y2="24.099" width="0.225" layer="94"/>
<wire x1="82.99755" y1="24.099" x2="83.160159375" y2="24.099" width="0.225" layer="94"/>
<wire x1="64.9221" y1="23.874" x2="73.45896875" y2="23.874" width="0.225" layer="94"/>
<wire x1="92.759190625" y1="23.874" x2="101.4471" y2="23.874" width="0.225" layer="94"/>
<wire x1="74.6829" y1="23.874" x2="75.49116875" y2="23.874" width="0.225" layer="94"/>
<wire x1="92.0661" y1="23.874" x2="92.34215" y2="23.874" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="23.874" x2="76.74395" y2="23.874" width="0.225" layer="94"/>
<wire x1="90.830209375" y1="23.874" x2="91.62503125" y2="23.874" width="0.225" layer="94"/>
<wire x1="77.967890625" y1="23.874" x2="78.75028125" y2="23.874" width="0.225" layer="94"/>
<wire x1="90.0934" y1="23.874" x2="90.41016875" y2="23.874" width="0.225" layer="94"/>
<wire x1="79.18205" y1="23.874" x2="79.49928125" y2="23.874" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="23.874" x2="89.661509375" y2="23.874" width="0.225" layer="94"/>
<wire x1="79.91931875" y1="23.874" x2="80.786140625" y2="23.874" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="23.874" x2="88.41505" y2="23.874" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="23.874" x2="82.508140625" y2="23.874" width="0.225" layer="94"/>
<wire x1="86.817440625" y1="23.874" x2="87.14536875" y2="23.874" width="0.225" layer="94"/>
<wire x1="82.92926875" y1="23.874" x2="83.18028125" y2="23.874" width="0.225" layer="94"/>
<wire x1="85.545740625" y1="23.874" x2="86.39005" y2="23.874" width="0.225" layer="94"/>
<wire x1="83.585309375" y1="23.874" x2="84.32161875" y2="23.874" width="0.225" layer="94"/>
<wire x1="84.754290625" y1="23.874" x2="85.11306875" y2="23.874" width="0.225" layer="94"/>
<wire x1="64.9221" y1="23.649" x2="73.457240625" y2="23.649" width="0.225" layer="94"/>
<wire x1="92.708490625" y1="23.649" x2="101.4471" y2="23.649" width="0.225" layer="94"/>
<wire x1="74.684409375" y1="23.649" x2="75.49116875" y2="23.649" width="0.225" layer="94"/>
<wire x1="90.092" y1="23.649" x2="91.66831875" y2="23.649" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="23.649" x2="76.74221875" y2="23.649" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="23.649" x2="89.66001875" y2="23.649" width="0.225" layer="94"/>
<wire x1="77.969390625" y1="23.649" x2="78.74913125" y2="23.649" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="23.649" x2="88.41505" y2="23.649" width="0.225" layer="94"/>
<wire x1="79.181109375" y1="23.649" x2="80.786140625" y2="23.649" width="0.225" layer="94"/>
<wire x1="86.810090625" y1="23.649" x2="87.1462" y2="23.649" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="23.649" x2="82.508140625" y2="23.649" width="0.225" layer="94"/>
<wire x1="85.54725" y1="23.649" x2="86.39005" y2="23.649" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="23.649" x2="84.32008125" y2="23.649" width="0.225" layer="94"/>
<wire x1="84.752059375" y1="23.649" x2="85.115190625" y2="23.649" width="0.225" layer="94"/>
<wire x1="64.9221" y1="23.424" x2="73.457090625" y2="23.424" width="0.225" layer="94"/>
<wire x1="92.77591875" y1="23.424" x2="101.4471" y2="23.424" width="0.225" layer="94"/>
<wire x1="73.88948125" y1="23.424" x2="74.27638125" y2="23.424" width="0.225" layer="94"/>
<wire x1="92.024059375" y1="23.424" x2="92.27126875" y2="23.424" width="0.225" layer="94"/>
<wire x1="74.681409375" y1="23.424" x2="75.49116875" y2="23.424" width="0.225" layer="94"/>
<wire x1="90.86316875" y1="23.424" x2="91.60701875" y2="23.424" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="23.424" x2="76.74208125" y2="23.424" width="0.225" layer="94"/>
<wire x1="90.092309375" y1="23.424" x2="90.43711875" y2="23.424" width="0.225" layer="94"/>
<wire x1="77.17446875" y1="23.424" x2="77.561359375" y2="23.424" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="23.424" x2="89.660090625" y2="23.424" width="0.225" layer="94"/>
<wire x1="77.966390625" y1="23.424" x2="78.749" y2="23.424" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="23.424" x2="88.41505" y2="23.424" width="0.225" layer="94"/>
<wire x1="79.181340625" y1="23.424" x2="79.52623125" y2="23.424" width="0.225" layer="94"/>
<wire x1="86.810090625" y1="23.424" x2="87.1462" y2="23.424" width="0.225" layer="94"/>
<wire x1="79.95228125" y1="23.424" x2="80.786140625" y2="23.424" width="0.225" layer="94"/>
<wire x1="85.54718125" y1="23.424" x2="86.39005" y2="23.424" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="23.424" x2="81.341240625" y2="23.424" width="0.225" layer="94"/>
<wire x1="84.752340625" y1="23.424" x2="85.114909375" y2="23.424" width="0.225" layer="94"/>
<wire x1="81.74326875" y1="23.424" x2="82.508140625" y2="23.424" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="23.424" x2="84.32015" y2="23.424" width="0.225" layer="94"/>
<wire x1="64.9221" y1="23.199" x2="73.47301875" y2="23.199" width="0.225" layer="94"/>
<wire x1="92.777209375" y1="23.199" x2="101.4471" y2="23.199" width="0.225" layer="94"/>
<wire x1="73.90876875" y1="23.199" x2="74.2678" y2="23.199" width="0.225" layer="94"/>
<wire x1="92.025140625" y1="23.199" x2="92.34813125" y2="23.199" width="0.225" layer="94"/>
<wire x1="74.67768125" y1="23.199" x2="75.49116875" y2="23.199" width="0.225" layer="94"/>
<wire x1="90.852559375" y1="23.199" x2="91.60701875" y2="23.199" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="23.199" x2="76.758" y2="23.199" width="0.225" layer="94"/>
<wire x1="90.11266875" y1="23.199" x2="90.420940625" y2="23.199" width="0.225" layer="94"/>
<wire x1="77.193759375" y1="23.199" x2="77.55278125" y2="23.199" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="23.199" x2="89.675959375" y2="23.199" width="0.225" layer="94"/>
<wire x1="77.962659375" y1="23.199" x2="78.76481875" y2="23.199" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="23.199" x2="88.41505" y2="23.199" width="0.225" layer="94"/>
<wire x1="79.202409375" y1="23.199" x2="79.510359375" y2="23.199" width="0.225" layer="94"/>
<wire x1="86.810090625" y1="23.199" x2="87.1462" y2="23.199" width="0.225" layer="94"/>
<wire x1="79.94166875" y1="23.199" x2="80.78728125" y2="23.199" width="0.225" layer="94"/>
<wire x1="85.531009375" y1="23.199" x2="86.39005" y2="23.199" width="0.225" layer="94"/>
<wire x1="81.209909375" y1="23.199" x2="81.340009375" y2="23.199" width="0.225" layer="94"/>
<wire x1="84.77363125" y1="23.199" x2="85.092790625" y2="23.199" width="0.225" layer="94"/>
<wire x1="81.740890625" y1="23.199" x2="82.508140625" y2="23.199" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="23.199" x2="84.336459375" y2="23.199" width="0.225" layer="94"/>
<wire x1="64.9221" y1="22.974" x2="73.542859375" y2="22.974" width="0.225" layer="94"/>
<wire x1="92.74113125" y1="22.974" x2="101.4471" y2="22.974" width="0.225" layer="94"/>
<wire x1="74.62216875" y1="22.974" x2="75.49116875" y2="22.974" width="0.225" layer="94"/>
<wire x1="90.78793125" y1="22.974" x2="91.6325" y2="22.974" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="22.974" x2="76.82785" y2="22.974" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="22.974" x2="89.74463125" y2="22.974" width="0.225" layer="94"/>
<wire x1="77.90715" y1="22.974" x2="78.834309375" y2="22.974" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="22.974" x2="88.41505" y2="22.974" width="0.225" layer="94"/>
<wire x1="79.877040625" y1="22.974" x2="80.82388125" y2="22.974" width="0.225" layer="94"/>
<wire x1="86.810090625" y1="22.974" x2="87.1462" y2="22.974" width="0.225" layer="94"/>
<wire x1="81.697240625" y1="22.974" x2="82.508140625" y2="22.974" width="0.225" layer="94"/>
<wire x1="85.46248125" y1="22.974" x2="86.39005" y2="22.974" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="22.974" x2="84.40543125" y2="22.974" width="0.225" layer="94"/>
<wire x1="64.9221" y1="22.749" x2="73.828090625" y2="22.749" width="0.225" layer="94"/>
<wire x1="92.48735" y1="22.749" x2="101.4471" y2="22.749" width="0.225" layer="94"/>
<wire x1="74.3331" y1="22.749" x2="75.49116875" y2="22.749" width="0.225" layer="94"/>
<wire x1="90.51153125" y1="22.749" x2="91.85098125" y2="22.749" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="22.749" x2="77.11308125" y2="22.749" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="22.749" x2="90.02831875" y2="22.749" width="0.225" layer="94"/>
<wire x1="77.618090625" y1="22.749" x2="79.119840625" y2="22.749" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="22.749" x2="88.41505" y2="22.749" width="0.225" layer="94"/>
<wire x1="79.60065" y1="22.749" x2="81.04181875" y2="22.749" width="0.225" layer="94"/>
<wire x1="86.810090625" y1="22.749" x2="87.1462" y2="22.749" width="0.225" layer="94"/>
<wire x1="81.460959375" y1="22.749" x2="82.508140625" y2="22.749" width="0.225" layer="94"/>
<wire x1="85.1789" y1="22.749" x2="86.39005" y2="22.749" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="22.749" x2="84.689659375" y2="22.749" width="0.225" layer="94"/>
<wire x1="64.9221" y1="22.524" x2="101.4471" y2="22.524" width="0.225" layer="94"/>
<wire x1="64.9221" y1="22.299" x2="101.4471" y2="22.299" width="0.225" layer="94"/>
<wire x1="64.9221" y1="22.074" x2="101.4471" y2="22.074" width="0.225" layer="94"/>
<wire x1="64.9221" y1="21.849" x2="101.4471" y2="21.849" width="0.225" layer="94"/>
<wire x1="64.9221" y1="21.624" x2="101.4471" y2="21.624" width="0.225" layer="94"/>
<wire x1="64.9221" y1="21.399" x2="101.4471" y2="21.399" width="0.225" layer="94"/>
<wire x1="64.9221" y1="21.174" x2="101.4471" y2="21.174" width="0.225" layer="94"/>
<wire x1="64.9221" y1="20.949" x2="101.4471" y2="20.949" width="0.225" layer="94"/>
<wire x1="64.9221" y1="20.724" x2="101.4471" y2="20.724" width="0.225" layer="94"/>
<rectangle x1="93.287040625" y1="26.91076875" x2="93.399540625" y2="30.80871875" layer="94"/>
<rectangle x1="94.749840625" y1="26.91076875" x2="94.862340625" y2="30.80871875" layer="94"/>
<rectangle x1="73.776721875" y1="23.770290625" x2="74.376859375" y2="23.882790625" layer="94"/>
<rectangle x1="77.0617" y1="23.770290625" x2="77.66185" y2="23.882790625" layer="94"/>
<rectangle x1="73.77671875" y1="23.46881875" x2="74.685" y2="23.58131875" layer="94"/>
<rectangle x1="77.0617" y1="23.46881875" x2="77.985" y2="23.58131875" layer="94"/>
<rectangle x1="81.09369375" y1="24.37936875" x2="81.7013" y2="24.774" layer="94"/>
<rectangle x1="80.627059375" y1="24.37936875" x2="80.898640625" y2="24.774" layer="94"/>
<rectangle x1="80.627059375" y1="23.779371875" x2="80.898640625" y2="24.199490625" layer="94"/>
<rectangle x1="81.09369375" y1="23.779371875" x2="81.7013" y2="24.199490625" layer="94"/>
<rectangle x1="64.77" y1="35.2425" x2="101.6" y2="35.56" layer="94"/>
<rectangle x1="101.2825" y1="20.32" x2="101.6" y2="35.56" layer="94"/>
<rectangle x1="64.77" y1="20.32" x2="65.0875" y2="35.56" layer="94"/>
<rectangle x1="64.77" y1="20.32" x2="101.6" y2="20.6375" layer="94"/>
<text x="33.02" y="27.94" size="5.08" layer="94" ratio="10" align="center">&gt;VALUE</text>
</symbol>
<symbol name="DINA4_L">
<frame x1="0" y1="0" x2="264.16" y2="180.34" columns="4" rows="4" layer="94" border-left="no" border-top="no" border-right="no" border-bottom="no"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="DINA4_L" prefix="FRAME" uservalue="yes">
<description>&lt;b&gt;VEIT FRAME&lt;/b&gt;&lt;p&gt;
DIN A4, landscape with extra doc field</description>
<gates>
<gate name="G$1" symbol="DINA4_L" x="0" y="0"/>
<gate name="G$2" symbol="DOCFIELD" x="162.56" y="0" addlevel="must"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="MODUL4" library="CMI" deviceset="MCFLEX" device=""/>
<part name="U$20" library="CMI" deviceset="JISTIC_DUMMY" device="" value="70A"/>
<part name="U$21" library="CMI" deviceset="SPINAC_DUMMY" device="" value="OIL TEMP"/>
<part name="U$22" library="CMI" deviceset="SPINAC_ROZPINACI_DUMMY" device="" value="OIL PRESS"/>
<part name="U$29" library="CMI" deviceset="1SVORKA_POKUS" device=""/>
<part name="U$25" library="CMI" deviceset="1SVORKA_POKUS" device=""/>
<part name="U$26" library="CMI" deviceset="ZEM_DUMMY" device=""/>
<part name="U$32" library="CMI" deviceset="1SVORKA_POKUS" device=""/>
<part name="U$33" library="CMI" deviceset="1SVORKA_POKUS" device=""/>
<part name="U$34" library="CMI" deviceset="MOTOR_DUMMY" device=""/>
<part name="U$36" library="CMI" deviceset="ZEM_DUMMY" device=""/>
<part name="U$37" library="CMI" deviceset="ZEM_DUMMY" device=""/>
<part name="U$39" library="CMI" deviceset="ZEM_DUMMY" device=""/>
<part name="U$40" library="CMI" deviceset="ZEM_DUMMY" device=""/>
<part name="U$41" library="CMI" deviceset="SELENOID_VENTIL_DUMMY" device=""/>
<part name="U$42" library="CMI" deviceset="ZEM_DUMMY" device=""/>
<part name="U$43" library="CMI" deviceset="1SVORKA_POKUS" device=""/>
<part name="U$44" library="CMI" deviceset="1SVORKA_POKUS" device=""/>
<part name="FRAME2" library="VEIT_logotyp" deviceset="DINA4_L" device=""/>
</parts>
<sheets>
<sheet>
<description>DEUTZ s CANarmatur</description>
<plain>
<text x="33.02" y="160.02" size="3.81" layer="92" align="bottom-center">+24V</text>
<text x="109.22" y="81.28" size="1.778" layer="94" align="top-left"> Nastavení výstupů MCflex:
=================
PIN 02: [10]in/output | gen. D+ | with
PIN 06: fuelpump
PIN 08: KL.50F
PIN 10: glow
PIN 11: off
PIN 12: glow
PIN 15: off
PIN 18: ETR-HC </text>
<text x="76.2" y="81.28" size="1.778" layer="94" align="top-left"> Nastavení vstupů MCflex:
================
PIN 01: NC
PIN 03: NO
PIN 04: w.out
PIN 07: off
PIN 09: w.out
PIN 13: PNP, 14.2 pulzů/ot</text>
<text x="40.64" y="121.92" size="1.27" layer="94" align="center">X1:4</text>
<text x="40.64" y="132.08" size="1.27" layer="94" align="center">X1:6</text>
<text x="22.86" y="104.14" size="1.778" layer="94" align="center">Poloha kontaktů
kreslena při stojícím motoru</text>
<wire x1="149.86" y1="86.36" x2="149.86" y2="109.22" width="0.1524" layer="94"/>
<wire x1="149.86" y1="109.22" x2="137.16" y2="109.22" width="0.1524" layer="94"/>
<wire x1="137.16" y1="109.22" x2="137.16" y2="86.36" width="0.1524" layer="94"/>
<wire x1="137.16" y1="86.36" x2="149.86" y2="86.36" width="0.1524" layer="94"/>
<wire x1="149.86" y1="86.36" x2="137.16" y2="109.22" width="0.1524" layer="94"/>
<wire x1="149.86" y1="109.22" x2="137.16" y2="86.36" width="0.1524" layer="94"/>
<text x="143.51" y="110.49" size="1.778" layer="94" align="center">Default</text>
<text x="182.88" y="127" size="1.27" layer="94" align="center">X1:1</text>
<text x="182.88" y="116.84" size="1.27" layer="94" align="center">X1:2</text>
<wire x1="193.04" y1="137.16" x2="193.04" y2="111.76" width="0.1524" layer="94"/>
<wire x1="193.04" y1="111.76" x2="213.36" y2="111.76" width="0.1524" layer="94"/>
<wire x1="213.36" y1="111.76" x2="213.36" y2="137.16" width="0.1524" layer="94"/>
<wire x1="213.36" y1="137.16" x2="193.04" y2="137.16" width="0.1524" layer="94"/>
<text x="195.58" y="124.46" size="1.778" layer="94" align="center-left">W</text>
<text x="195.58" y="114.3" size="1.778" layer="94" align="center-left">D+</text>
<text x="195.58" y="134.62" size="1.778" layer="94" align="center-left">B+</text>
<text x="203.2" y="138.43" size="1.778" layer="94" align="center">ALTERNÁTOR</text>
<text x="7.62" y="91.44" size="1.778" layer="94">Konektor X1 je umístěn na motoru</text>
<text x="7.62" y="86.36" size="1.778" layer="94">Číslo za dvojtečkou je číslo pinu na konektoru X1</text>
<wire x1="7.62" y1="144.78" x2="40.64" y2="144.78" width="0.1524" layer="94" style="shortdash"/>
<wire x1="40.64" y1="144.78" x2="40.64" y2="134.62" width="0.1524" layer="94" style="shortdash"/>
<wire x1="40.64" y1="127" x2="40.64" y2="124.46" width="0.1524" layer="94" style="shortdash"/>
<wire x1="40.64" y1="116.84" x2="40.64" y2="99.06" width="0.1524" layer="94" style="shortdash"/>
<wire x1="40.64" y1="99.06" x2="7.62" y2="99.06" width="0.1524" layer="94" style="shortdash"/>
<text x="22.86" y="146.05" size="1.778" layer="94" align="center">MOTOR D 2011 L04</text>
<wire x1="233.68" y1="137.16" x2="233.68" y2="121.92" width="0.1524" layer="94"/>
<wire x1="233.68" y1="121.92" x2="254" y2="121.92" width="0.1524" layer="94"/>
<wire x1="254" y1="121.92" x2="254" y2="137.16" width="0.1524" layer="94"/>
<wire x1="254" y1="137.16" x2="233.68" y2="137.16" width="0.1524" layer="94"/>
<text x="236.22" y="124.46" size="1.778" layer="94" align="center-left">50</text>
<text x="236.22" y="134.62" size="1.778" layer="94" align="center-left">30</text>
<text x="243.84" y="138.43" size="1.778" layer="94" align="center">STARTÉR</text>
<text x="246.38" y="83.82" size="1.778" layer="94" align="center">PALIVOVÉ
ČERPADLO</text>
<text x="215.9" y="63.5" size="1.778" layer="94" rot="MR180" align="bottom-center">Ke žhavícím svíčkám</text>
<text x="182.88" y="91.44" size="1.27" layer="94" align="center">X1:7</text>
<text x="182.88" y="81.28" size="1.27" layer="94" align="center">X1:8</text>
<text x="195.58" y="74.93" size="1.778" layer="94" align="center">STOP
SELENOID</text>
<text x="203.2" y="146.05" size="1.778" layer="94" align="center">MOTOR D 2011 L04</text>
<wire x1="256.54" y1="144.78" x2="182.88" y2="144.78" width="0.1524" layer="94" style="shortdash"/>
<wire x1="182.88" y1="144.78" x2="182.88" y2="129.54" width="0.1524" layer="94" style="shortdash"/>
<wire x1="182.88" y1="121.92" x2="182.88" y2="119.38" width="0.1524" layer="94" style="shortdash"/>
<wire x1="182.88" y1="111.76" x2="182.88" y2="93.98" width="0.1524" layer="94" style="shortdash"/>
<wire x1="182.88" y1="86.36" x2="182.88" y2="83.82" width="0.1524" layer="94" style="shortdash"/>
<wire x1="182.88" y1="76.2" x2="182.88" y2="55.88" width="0.1524" layer="94" style="shortdash"/>
<wire x1="182.88" y1="55.88" x2="256.54" y2="55.88" width="0.1524" layer="94" style="shortdash"/>
<circle x="53.34" y="157.48" radius="1.016" width="0" layer="92"/>
<circle x="175.26" y="157.48" radius="1.016" width="0" layer="92"/>
<text x="55.88" y="147.32" size="1.778" layer="94" align="center-left">(Jistič)</text>
<text x="165.1" y="22.86" size="2.1844" layer="94">Generátor ECP28-VL/ 4A (MECC ALTE)
poháněný motorem D 2011 L04 (DEUTZ)
s ovládacím panelem MCflex (ehb electronics)</text>
<text x="256.54" y="7.62" size="2.1844" layer="94" align="center">V0.0</text>
<text x="88.9" y="22.86" size="1.778" layer="98">Doplnit průřezy a barvy</text>
</plain>
<instances>
<instance part="MODUL4" gate="G$1" x="114.3" y="111.76"/>
<instance part="U$20" gate="G$1" x="53.34" y="147.32" smashed="yes" rot="R90">
<attribute name="VALUE" x="50.8" y="147.32" size="1.778" layer="94" align="center-right"/>
</instance>
<instance part="U$21" gate="G$1" x="22.86" y="119.38"/>
<instance part="U$22" gate="G$1" x="22.86" y="129.54"/>
<instance part="U$29" gate="G$1" x="40.64" y="129.54"/>
<instance part="U$25" gate="G$1" x="40.64" y="119.38"/>
<instance part="U$26" gate="G$1" x="66.04" y="78.74"/>
<instance part="U$32" gate="G$1" x="182.88" y="124.46"/>
<instance part="U$33" gate="G$1" x="182.88" y="114.3"/>
<instance part="U$34" gate="G$1" x="236.22" y="83.82"/>
<instance part="U$36" gate="G$1" x="12.7" y="111.76"/>
<instance part="U$37" gate="G$1" x="236.22" y="71.12"/>
<instance part="U$39" gate="G$1" x="243.84" y="114.3"/>
<instance part="U$40" gate="G$1" x="220.98" y="114.3"/>
<instance part="U$41" gate="G$1" x="193.04" y="83.82"/>
<instance part="U$42" gate="G$1" x="175.26" y="71.12"/>
<instance part="U$43" gate="G$1" x="182.88" y="88.9"/>
<instance part="U$44" gate="G$1" x="182.88" y="78.74"/>
<instance part="FRAME2" gate="G$1" x="0" y="0"/>
<instance part="FRAME2" gate="G$2" x="162.56" y="0"/>
</instances>
<busses>
<bus name="KL30">
<segment>
<wire x1="7.62" y1="157.48" x2="53.34" y2="157.48" width="0.762" layer="92"/>
<wire x1="53.34" y1="157.48" x2="53.34" y2="152.4" width="0.762" layer="92"/>
<label x="10.16" y="160.02" size="1.778" layer="95" rot="MR180"/>
<wire x1="53.34" y1="157.48" x2="175.26" y2="157.48" width="0.762" layer="92"/>
<wire x1="175.26" y1="157.48" x2="175.26" y2="134.62" width="0.762" layer="92"/>
<wire x1="228.6" y1="134.62" x2="233.68" y2="134.62" width="0.762" layer="92"/>
<wire x1="175.26" y1="157.48" x2="228.6" y2="157.48" width="0.762" layer="92"/>
<wire x1="228.6" y1="157.48" x2="228.6" y2="134.62" width="0.762" layer="92"/>
<wire x1="175.26" y1="134.62" x2="193.04" y2="134.62" width="0.762" layer="92"/>
</segment>
</bus>
</busses>
<nets>
<net name="N$12" class="0">
<segment>
<pinref part="MODUL4" gate="G$1" pin="KL30@14"/>
<wire x1="71.12" y1="137.16" x2="53.34" y2="137.16" width="0.4064" layer="91"/>
<wire x1="53.34" y1="137.16" x2="53.34" y2="142.24" width="0.4064" layer="91"/>
<pinref part="MODUL4" gate="G$1" pin="KL30@16"/>
<wire x1="71.12" y1="134.62" x2="53.34" y2="134.62" width="0.4064" layer="91"/>
<wire x1="53.34" y1="134.62" x2="53.34" y2="137.16" width="0.4064" layer="91"/>
<junction x="53.34" y="137.16"/>
</segment>
</net>
<net name="N$13" class="0">
<segment>
<pinref part="MODUL4" gate="G$1" pin="OIL"/>
<wire x1="71.12" y1="129.54" x2="27.94" y2="129.54" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$14" class="0">
<segment>
<wire x1="27.94" y1="119.38" x2="53.34" y2="119.38" width="0.1524" layer="91"/>
<wire x1="53.34" y1="119.38" x2="53.34" y2="127" width="0.1524" layer="91"/>
<pinref part="MODUL4" gate="G$1" pin="TEMP"/>
<wire x1="53.34" y1="127" x2="71.12" y2="127" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$15" class="0">
<segment>
<pinref part="MODUL4" gate="G$1" pin="SPEED"/>
<wire x1="71.12" y1="116.84" x2="66.04" y2="116.84" width="0.1524" layer="91"/>
<wire x1="66.04" y1="116.84" x2="66.04" y2="147.32" width="0.1524" layer="91"/>
<wire x1="66.04" y1="147.32" x2="165.1" y2="147.32" width="0.1524" layer="91"/>
<wire x1="165.1" y1="147.32" x2="165.1" y2="124.46" width="0.1524" layer="91"/>
<wire x1="165.1" y1="124.46" x2="193.04" y2="124.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$16" class="0">
<segment>
<pinref part="MODUL4" gate="G$1" pin="D+"/>
<wire x1="157.48" y1="106.68" x2="165.1" y2="106.68" width="0.1524" layer="91"/>
<wire x1="165.1" y1="106.68" x2="165.1" y2="114.3" width="0.1524" layer="91"/>
<wire x1="165.1" y1="114.3" x2="193.04" y2="114.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$17" class="0">
<segment>
<pinref part="MODUL4" gate="G$1" pin="ETR-HC"/>
<wire x1="157.48" y1="104.14" x2="236.22" y2="104.14" width="0.1524" layer="91"/>
<wire x1="236.22" y1="104.14" x2="236.22" y2="86.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$18" class="0">
<segment>
<pinref part="MODUL4" gate="G$1" pin="KL31"/>
<wire x1="71.12" y1="86.36" x2="66.04" y2="86.36" width="0.4064" layer="91"/>
<wire x1="66.04" y1="86.36" x2="66.04" y2="81.28" width="0.4064" layer="91"/>
</segment>
</net>
<net name="N$19" class="0">
<segment>
<wire x1="17.78" y1="129.54" x2="12.7" y2="129.54" width="0.1524" layer="91"/>
<wire x1="12.7" y1="129.54" x2="12.7" y2="119.38" width="0.1524" layer="91"/>
<wire x1="12.7" y1="119.38" x2="12.7" y2="114.3" width="0.1524" layer="91"/>
<wire x1="17.78" y1="119.38" x2="12.7" y2="119.38" width="0.1524" layer="91"/>
<junction x="12.7" y="119.38"/>
</segment>
</net>
<net name="N$20" class="0">
<segment>
<wire x1="236.22" y1="73.66" x2="236.22" y2="81.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$21" class="0">
<segment>
<wire x1="228.6" y1="124.46" x2="233.68" y2="124.46" width="0.1524" layer="91"/>
<pinref part="MODUL4" gate="G$1" pin="KL.50F"/>
<wire x1="157.48" y1="101.6" x2="228.6" y2="101.6" width="0.1524" layer="91"/>
<wire x1="228.6" y1="101.6" x2="228.6" y2="124.46" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$22" class="0">
<segment>
<pinref part="MODUL4" gate="G$1" pin="GLOW@10"/>
<wire x1="157.48" y1="99.06" x2="220.98" y2="99.06" width="0.4064" layer="91"/>
<wire x1="220.98" y1="99.06" x2="220.98" y2="66.04" width="0.4064" layer="91"/>
<wire x1="220.98" y1="66.04" x2="223.52" y2="71.12" width="0.4064" layer="91"/>
<wire x1="220.98" y1="66.04" x2="218.44" y2="71.12" width="0.4064" layer="91"/>
</segment>
</net>
<net name="N$23" class="0">
<segment>
<pinref part="MODUL4" gate="G$1" pin="GLOW@12"/>
<wire x1="157.48" y1="93.98" x2="210.82" y2="93.98" width="0.4064" layer="91"/>
<wire x1="210.82" y1="93.98" x2="210.82" y2="66.04" width="0.4064" layer="91"/>
<wire x1="210.82" y1="66.04" x2="213.36" y2="71.12" width="0.4064" layer="91"/>
<wire x1="210.82" y1="66.04" x2="208.28" y2="71.12" width="0.4064" layer="91"/>
</segment>
</net>
<net name="N$24" class="0">
<segment>
<wire x1="220.98" y1="116.84" x2="220.98" y2="124.46" width="0.4064" layer="91"/>
<wire x1="220.98" y1="124.46" x2="213.36" y2="124.46" width="0.4064" layer="91"/>
</segment>
</net>
<net name="N$25" class="0">
<segment>
<wire x1="243.84" y1="116.84" x2="243.84" y2="121.92" width="0.4064" layer="91"/>
</segment>
</net>
<net name="N$26" class="0">
<segment>
<pinref part="MODUL4" gate="G$1" pin="ETR-PC+3S"/>
<wire x1="157.48" y1="88.9" x2="193.04" y2="88.9" width="0.1524" layer="91"/>
<wire x1="193.04" y1="88.9" x2="193.04" y2="86.36" width="0.1524" layer="91"/>
</segment>
</net>
<net name="N$27" class="0">
<segment>
<wire x1="193.04" y1="81.28" x2="193.04" y2="78.74" width="0.1524" layer="91"/>
<wire x1="193.04" y1="78.74" x2="175.26" y2="78.74" width="0.1524" layer="91"/>
<wire x1="175.26" y1="78.74" x2="175.26" y2="73.66" width="0.1524" layer="91"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
</compatibility>
</eagle>
