<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="7.5.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="0.1" unitdist="inch" unit="inch" style="lines" multiple="1" display="no" altdistance="0.01" altunitdist="inch" altunit="inch"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="1" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="4" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="1" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="4" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="1" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="4" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="1" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="4" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="1" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="4" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="1" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="4" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="1" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="4" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="15" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="7" fill="1" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="5" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="113" name="ReferenceLS" color="7" fill="1" visible="no" active="no"/>
<layer number="114" name="FRNTMAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="115" name="FRNTMAAT2" color="7" fill="1" visible="no" active="no"/>
<layer number="117" name="BACKMAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="119" name="KAP_TEKEN" color="7" fill="1" visible="no" active="no"/>
<layer number="120" name="KAP_MAAT1" color="7" fill="1" visible="no" active="no"/>
<layer number="130" name="SMDSTROOK" color="7" fill="1" visible="no" active="no"/>
<layer number="133" name="bottom_silk" color="7" fill="1" visible="no" active="no"/>
<layer number="153" name="FabDoc1" color="6" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="2" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="200" name="200bmp" color="1" fill="10" visible="no" active="no"/>
<layer number="201" name="201bmp" color="2" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="3" fill="1" visible="no" active="no"/>
<layer number="217" name="217bmp" color="18" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="19" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="20" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="21" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="22" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="23" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="24" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="25" fill="1" visible="no" active="no"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="no" active="no"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="250" name="Descript" color="3" fill="1" visible="no" active="no"/>
<layer number="251" name="SMDround" color="12" fill="11" visible="no" active="no"/>
</layers>
<schematic xreflabel="%F%N/%S" xrefpart="/%S.%C%R">
<libraries>
<library name="VEIT_logotyp">
<packages>
</packages>
<symbols>
<symbol name="DINA3_L">
<wire x1="0" y1="0" x2="388.62" y2="0" width="0.4064" layer="94"/>
<wire x1="0" y1="264.16" x2="0" y2="0" width="0.4064" layer="94"/>
<wire x1="388.62" y1="264.16" x2="0" y2="264.16" width="0.4064" layer="94"/>
<wire x1="388.62" y1="264.16" x2="388.62" y2="0" width="0.4064" layer="94"/>
</symbol>
<symbol name="DOCFIELD-AUTA">
<wire x1="0" y1="0" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="101.6" y1="10.16" x2="87.63" y2="10.16" width="0.1016" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="71.12" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="10.16" width="0.1016" layer="94"/>
<wire x1="101.6" y1="10.16" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="0" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="87.63" y1="10.16" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="87.63" y1="10.16" x2="0" y2="10.16" width="0.1016" layer="94"/>
<wire x1="87.63" y1="5.08" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="101.6" y1="5.08" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="0" y1="10.16" x2="0" y2="22.86" width="0.1016" layer="94"/>
<wire x1="101.6" y1="38.1" x2="64.77" y2="38.1" width="0.1016" layer="94"/>
<wire x1="64.77" y1="38.1" x2="0" y2="38.1" width="0.1016" layer="94"/>
<wire x1="101.6" y1="38.1" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="64.77" y2="22.86" width="0.1016" layer="94"/>
<wire x1="64.77" y1="22.86" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="0" y2="38.1" width="0.1016" layer="94"/>
<wire x1="101.6" y1="22.86" x2="101.6" y2="10.16" width="0.1016" layer="94"/>
<text x="1.27" y="1.27" size="2.54" layer="94">Date:</text>
<text x="15.24" y="1.27" size="2.54" layer="94" font="vector">&gt;LAST_DATE_TIME</text>
<text x="72.39" y="1.27" size="2.54" layer="94">Sheet:</text>
<text x="86.36" y="1.27" size="2.54" layer="94">&gt;SHEET</text>
<text x="88.9" y="6.35" size="2.54" layer="94">REV:</text>
<text x="1.27" y="6.35" size="2.54" layer="94">TITLE:</text>
<text x="1.27" y="30.48" size="1.9304" layer="94" align="top-left">Vehicle Number:</text>
<text x="15.24" y="6.35" size="2.54" layer="94" font="vector">&gt;DRAWING_NAME</text>
<wire x1="64.77" y1="38.1" x2="64.77" y2="22.86" width="0.1016" layer="94"/>
<wire x1="97.487340625" y1="34.81106875" x2="90.66211875" y2="34.81106875" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="34.81106875" x2="90.66211875" y2="34.739" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="34.739" x2="90.66211875" y2="34.514" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="34.514" x2="90.66211875" y2="34.289" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="34.289" x2="90.66211875" y2="34.064" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="34.064" x2="90.66211875" y2="33.839" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="33.839" x2="90.66211875" y2="33.614" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="33.614" x2="90.66211875" y2="33.389" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="33.389" x2="90.66211875" y2="33.23621875" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="33.23621875" x2="93.287040625" y2="33.23621875" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="33.23621875" x2="93.287040625" y2="33.164" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="33.164" x2="93.287040625" y2="32.939" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="32.939" x2="93.287040625" y2="32.714" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="32.714" x2="93.287040625" y2="32.489" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="32.489" x2="93.287040625" y2="32.264" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="32.264" x2="93.287040625" y2="32.039" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="32.039" x2="93.287040625" y2="31.814" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="31.814" x2="93.287040625" y2="31.589" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="31.589" x2="93.287040625" y2="31.364" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="31.364" x2="93.287040625" y2="31.139" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="31.139" x2="93.287040625" y2="30.914" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="30.914" x2="93.287040625" y2="30.689" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="30.689" x2="93.287040625" y2="30.464" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="30.464" x2="93.287040625" y2="30.239" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="30.239" x2="93.287040625" y2="30.014" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="30.014" x2="93.287040625" y2="29.789" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="29.789" x2="93.287040625" y2="29.564" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="29.564" x2="93.287040625" y2="29.45076875" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="29.45076875" x2="94.862340625" y2="29.45076875" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="29.45076875" x2="94.862340625" y2="29.564" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="29.564" x2="94.862340625" y2="29.789" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="29.789" x2="94.862340625" y2="30.014" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="30.014" x2="94.862340625" y2="30.239" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="30.239" x2="94.862340625" y2="30.464" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="30.464" x2="94.862340625" y2="30.689" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="30.689" x2="94.862340625" y2="30.914" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="30.914" x2="94.862340625" y2="31.139" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="31.139" x2="94.862340625" y2="31.364" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="31.364" x2="94.862340625" y2="31.589" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="31.589" x2="94.862340625" y2="31.814" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="31.814" x2="94.862340625" y2="32.039" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="32.039" x2="94.862340625" y2="32.264" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="32.264" x2="94.862340625" y2="32.489" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="32.489" x2="94.862340625" y2="32.714" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="32.714" x2="94.862340625" y2="32.939" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="32.939" x2="94.862340625" y2="33.164" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="33.164" x2="94.862340625" y2="33.23621875" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="33.23621875" x2="97.487340625" y2="33.23621875" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="33.23621875" x2="97.487340625" y2="33.389" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="33.389" x2="97.487340625" y2="33.614" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="33.614" x2="97.487340625" y2="33.839" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="33.839" x2="97.487340625" y2="34.064" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="34.064" x2="97.487340625" y2="34.289" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="34.289" x2="97.487340625" y2="34.514" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="34.514" x2="97.487340625" y2="34.739" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="34.739" x2="97.487340625" y2="34.81106875" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="29.497059375" x2="87.068640625" y2="29.564" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="29.564" x2="87.068640625" y2="29.789" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="29.789" x2="87.068640625" y2="30.014" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="30.014" x2="87.068640625" y2="30.239" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="30.239" x2="87.068640625" y2="30.464" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="30.464" x2="87.068640625" y2="30.689" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="30.689" x2="87.068640625" y2="30.914" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="30.914" x2="87.068640625" y2="31.139" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="31.139" x2="87.068640625" y2="31.364" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="31.364" x2="87.068640625" y2="31.589" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="31.589" x2="87.068640625" y2="31.814" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="31.814" x2="87.068640625" y2="32.039" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="32.039" x2="87.068640625" y2="32.264" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="32.264" x2="87.068640625" y2="32.489" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="32.489" x2="87.068640625" y2="32.714" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="32.714" x2="87.068640625" y2="32.939" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="32.939" x2="87.068640625" y2="33.164" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="33.164" x2="87.068640625" y2="33.389" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="33.389" x2="87.068640625" y2="33.614" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="33.614" x2="87.068640625" y2="33.839" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="33.839" x2="87.068640625" y2="34.064" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="34.064" x2="87.068640625" y2="34.289" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="34.289" x2="87.068640625" y2="34.514" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="34.514" x2="87.068640625" y2="34.739" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="34.739" x2="87.068640625" y2="34.813409375" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="34.813409375" x2="88.64378125" y2="34.813409375" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="34.813409375" x2="88.64378125" y2="34.739" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="34.739" x2="88.64378125" y2="34.514" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="34.514" x2="88.64378125" y2="34.289" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="34.289" x2="88.64378125" y2="34.064" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="34.064" x2="88.64378125" y2="33.839" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="33.839" x2="88.64378125" y2="33.614" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="33.614" x2="88.64378125" y2="33.389" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="33.389" x2="88.64378125" y2="33.164" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="33.164" x2="88.64378125" y2="32.939" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="32.939" x2="88.64378125" y2="32.714" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="32.714" x2="88.64378125" y2="32.489" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="32.489" x2="88.64378125" y2="32.264" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="32.264" x2="88.64378125" y2="32.039" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="32.039" x2="88.64378125" y2="31.814" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="31.814" x2="88.64378125" y2="31.589" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="31.589" x2="88.64378125" y2="31.364" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="31.364" x2="88.64378125" y2="31.139" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="31.139" x2="88.64378125" y2="30.914" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="30.914" x2="88.64378125" y2="30.689" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="30.689" x2="88.64378125" y2="30.464" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="30.464" x2="88.64378125" y2="30.239" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="30.239" x2="88.64378125" y2="30.014" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="30.014" x2="88.64378125" y2="29.789" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="29.789" x2="88.64378125" y2="29.564" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="29.564" x2="88.64378125" y2="29.497059375" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="29.497059375" x2="87.068640625" y2="29.497059375" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="29.49851875" x2="78.286090625" y2="29.564" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="29.564" x2="78.286090625" y2="29.789" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="29.789" x2="78.286090625" y2="30.014" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="30.014" x2="78.286090625" y2="30.239" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="30.239" x2="78.286090625" y2="30.464" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="30.464" x2="78.286090625" y2="30.689" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="30.689" x2="78.286090625" y2="30.914" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="30.914" x2="78.286090625" y2="31.07366875" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="31.07366875" x2="84.81116875" y2="31.07366875" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="31.07366875" x2="84.81116875" y2="30.914" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="30.914" x2="84.81116875" y2="30.689" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="30.689" x2="84.81116875" y2="30.464" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="30.464" x2="84.81116875" y2="30.239" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="30.239" x2="84.81116875" y2="30.014" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="30.014" x2="84.81116875" y2="29.789" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="29.789" x2="84.81116875" y2="29.564" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="29.564" x2="84.81116875" y2="29.49851875" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="29.49851875" x2="78.286090625" y2="29.49851875" width="0.225" layer="94"/>
<wire x1="78.286240625" y1="31.44236875" x2="78.286240625" y2="31.589" width="0.225" layer="94"/>
<wire x1="78.286240625" y1="31.589" x2="78.286240625" y2="31.814" width="0.225" layer="94"/>
<wire x1="78.286240625" y1="31.814" x2="78.286240625" y2="32.039" width="0.225" layer="94"/>
<wire x1="78.286240625" y1="32.039" x2="78.286240625" y2="32.264" width="0.225" layer="94"/>
<wire x1="78.286240625" y1="32.264" x2="78.286240625" y2="32.489" width="0.225" layer="94"/>
<wire x1="78.286240625" y1="32.489" x2="78.286240625" y2="32.714" width="0.225" layer="94"/>
<wire x1="78.286240625" y1="32.714" x2="78.286240625" y2="32.86721875" width="0.225" layer="94"/>
<wire x1="78.286240625" y1="32.86721875" x2="81.96101875" y2="32.86721875" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="32.86721875" x2="81.96101875" y2="32.714" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="32.714" x2="81.96101875" y2="32.489" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="32.489" x2="81.96101875" y2="32.264" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="32.264" x2="81.96101875" y2="32.039" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="32.039" x2="81.96101875" y2="31.814" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="31.814" x2="81.96101875" y2="31.589" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="31.589" x2="81.96101875" y2="31.44236875" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="31.44236875" x2="78.286240625" y2="31.44236875" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="33.23621875" x2="78.286090625" y2="33.389" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="33.389" x2="78.286090625" y2="33.614" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="33.614" x2="78.286090625" y2="33.839" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="33.839" x2="78.286090625" y2="34.064" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="34.064" x2="78.286090625" y2="34.289" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="34.289" x2="78.286090625" y2="34.514" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="34.514" x2="78.286090625" y2="34.739" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="34.739" x2="78.286090625" y2="34.81106875" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="34.81106875" x2="84.81101875" y2="34.81106875" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="34.81106875" x2="84.81101875" y2="34.739" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="34.739" x2="84.81101875" y2="34.514" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="34.514" x2="84.81101875" y2="34.289" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="34.289" x2="84.81101875" y2="34.064" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="34.064" x2="84.81101875" y2="33.839" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="33.839" x2="84.81101875" y2="33.614" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="33.614" x2="84.81101875" y2="33.389" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="33.389" x2="84.81101875" y2="33.23621875" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="33.23621875" x2="78.286090625" y2="33.23621875" width="0.225" layer="94"/>
<wire x1="68.6921" y1="33.944190625" x2="69.073209375" y2="34.289" width="0.225" layer="94"/>
<wire x1="69.073209375" y1="34.289" x2="69.3219" y2="34.514" width="0.225" layer="94"/>
<wire x1="69.3219" y1="34.514" x2="69.570590625" y2="34.739" width="0.225" layer="94"/>
<wire x1="69.570590625" y1="34.739" x2="69.86021875" y2="35.001040625" width="0.225" layer="94"/>
<wire x1="85.115190625" y1="26.34016875" x2="85.115190625" y2="26.189" width="0.225" layer="94"/>
<wire x1="85.115190625" y1="26.189" x2="85.115190625" y2="25.97718125" width="0.225" layer="94"/>
<wire x1="84.32008125" y1="25.980109375" x2="84.341215625" y2="25.70643125" width="0.225" layer="94" curve="8.832242"/>
<wire x1="84.341209375" y1="25.70643125" x2="84.4424375" y2="25.45343125" width="0.225" layer="94" curve="25.94924"/>
<wire x1="84.442440625" y1="25.45343125" x2="84.662590625" y2="25.2962625" width="0.225" layer="94" curve="39.389578"/>
<wire x1="84.662590625" y1="25.296259375" x2="84.93363125" y2="25.258525" width="0.225" layer="94" curve="15.805998"/>
<wire x1="85.52626875" y1="25.706240625" x2="85.547246875" y2="25.980109375" width="0.225" layer="94" curve="8.760059"/>
<wire x1="85.42531875" y1="25.45295" x2="85.46248125" y2="25.514" width="0.225" layer="94" curve="6.74589"/>
<wire x1="85.46248125" y1="25.514" x2="85.526271875" y2="25.706240625" width="0.225" layer="94" curve="19.196299"/>
<wire x1="85.204909375" y1="25.295909375" x2="85.425315625" y2="25.452953125" width="0.225" layer="94" curve="39.656265"/>
<wire x1="84.93363125" y1="25.25853125" x2="85.204909375" y2="25.295909375" width="0.225" layer="94" curve="15.593829"/>
<wire x1="84.32008125" y1="25.980109375" x2="84.32008125" y2="26.189" width="0.225" layer="94"/>
<wire x1="84.32008125" y1="26.189" x2="84.32008125" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="84.341340625" y1="26.61343125" x2="84.32161875" y2="26.414" width="0.225" layer="94" curve="6.503067"/>
<wire x1="84.32161875" y1="26.414" x2="84.320078125" y2="26.34016875" width="0.225" layer="94" curve="2.395218"/>
<wire x1="84.44311875" y1="26.86573125" x2="84.341340625" y2="26.61343125" width="0.225" layer="94" curve="26.142719"/>
<wire x1="84.663740625" y1="27.02155" x2="84.443121875" y2="26.865728125" width="0.225" layer="94" curve="39.450584"/>
<wire x1="84.93363125" y1="27.05881875" x2="84.663740625" y2="27.02155" width="0.225" layer="94" curve="15.292594"/>
<wire x1="85.54725" y1="25.980109375" x2="85.54725" y2="26.189" width="0.225" layer="94"/>
<wire x1="85.54725" y1="26.189" x2="85.54725" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="85.54725" y1="26.34016875" x2="85.52646875" y2="26.61348125" width="0.225" layer="94" curve="8.69613"/>
<wire x1="85.52646875" y1="26.61348125" x2="85.4257" y2="26.8662" width="0.225" layer="94" curve="26.08534"/>
<wire x1="85.4257" y1="26.8662" x2="85.2053" y2="27.02220625" width="0.225" layer="94" curve="39.852867"/>
<wire x1="85.2053" y1="27.022209375" x2="84.93363125" y2="27.058825" width="0.225" layer="94" curve="15.379893"/>
<wire x1="84.752059375" y1="26.34016875" x2="84.752059375" y2="26.189" width="0.225" layer="94"/>
<wire x1="84.752059375" y1="26.189" x2="84.752059375" y2="25.97718125" width="0.225" layer="94"/>
<wire x1="85.115190625" y1="25.97718125" x2="85.11286875" y2="25.87078125" width="0.225" layer="94"/>
<wire x1="85.11286875" y1="25.87078125" x2="85.10523125" y2="25.791359375" width="0.225" layer="94"/>
<wire x1="85.10523125" y1="25.791359375" x2="85.100290625" y2="25.765209375" width="0.225" layer="94"/>
<wire x1="85.100290625" y1="25.765209375" x2="85.09306875" y2="25.73961875" width="0.225" layer="94"/>
<wire x1="85.09306875" y1="25.73961875" x2="85.081940625" y2="25.715509375" width="0.225" layer="94"/>
<wire x1="85.081940625" y1="25.715509375" x2="85.06321875" y2="25.696990625" width="0.225" layer="94"/>
<wire x1="85.06321875" y1="25.696990625" x2="85.0387" y2="25.68683125" width="0.225" layer="94"/>
<wire x1="85.0387" y1="25.68683125" x2="85.012740625" y2="25.68106875" width="0.225" layer="94"/>
<wire x1="85.012740625" y1="25.68106875" x2="84.986340625" y2="25.6778" width="0.225" layer="94"/>
<wire x1="84.986340625" y1="25.6778" x2="84.95978125" y2="25.67618125" width="0.225" layer="94"/>
<wire x1="84.752059375" y1="26.34016875" x2="84.75605" y2="26.47246875" width="0.225" layer="94"/>
<wire x1="84.75605" y1="26.47246875" x2="84.76195" y2="26.52508125" width="0.225" layer="94"/>
<wire x1="84.76195" y1="26.52508125" x2="84.77411875" y2="26.57655" width="0.225" layer="94"/>
<wire x1="84.77411875" y1="26.57655" x2="84.78536875" y2="26.600440625" width="0.225" layer="94"/>
<wire x1="84.78536875" y1="26.600440625" x2="84.804459375" y2="26.618359375" width="0.225" layer="94"/>
<wire x1="84.804459375" y1="26.618359375" x2="84.82903125" y2="26.62806875" width="0.225" layer="94"/>
<wire x1="84.82903125" y1="26.62806875" x2="84.854909375" y2="26.63358125" width="0.225" layer="94"/>
<wire x1="84.854909375" y1="26.63358125" x2="84.8812" y2="26.6367" width="0.225" layer="94"/>
<wire x1="84.8812" y1="26.6367" x2="84.90763125" y2="26.63826875" width="0.225" layer="94"/>
<wire x1="84.90763125" y1="26.63826875" x2="84.96056875" y2="26.63825" width="0.225" layer="94"/>
<wire x1="84.96056875" y1="26.63825" x2="84.987" y2="26.63666875" width="0.225" layer="94"/>
<wire x1="84.987" y1="26.63666875" x2="85.013290625" y2="26.63358125" width="0.225" layer="94"/>
<wire x1="85.013290625" y1="26.63358125" x2="85.039190625" y2="26.628140625" width="0.225" layer="94"/>
<wire x1="85.039190625" y1="26.628140625" x2="85.063809375" y2="26.618559375" width="0.225" layer="94"/>
<wire x1="85.063809375" y1="26.618559375" x2="85.0829" y2="26.60066875" width="0.225" layer="94"/>
<wire x1="85.0829" y1="26.60066875" x2="85.093940625" y2="26.57666875" width="0.225" layer="94"/>
<wire x1="85.093940625" y1="26.57666875" x2="85.105759375" y2="26.52511875" width="0.225" layer="94"/>
<wire x1="85.105759375" y1="26.52511875" x2="85.111390625" y2="26.47248125" width="0.225" layer="94"/>
<wire x1="85.111390625" y1="26.47248125" x2="85.11306875" y2="26.414" width="0.225" layer="94"/>
<wire x1="85.11306875" y1="26.414" x2="85.115190625" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="84.752059375" y1="25.97718125" x2="84.754290625" y2="25.87078125" width="0.225" layer="94"/>
<wire x1="84.754290625" y1="25.87078125" x2="84.76156875" y2="25.79131875" width="0.225" layer="94"/>
<wire x1="84.76156875" y1="25.79131875" x2="84.7664" y2="25.76515" width="0.225" layer="94"/>
<wire x1="84.7664" y1="25.76515" x2="84.773409375" y2="25.7395" width="0.225" layer="94"/>
<wire x1="84.773409375" y1="25.7395" x2="84.784340625" y2="25.7153" width="0.225" layer="94"/>
<wire x1="84.784340625" y1="25.7153" x2="84.803059375" y2="25.6968" width="0.225" layer="94"/>
<wire x1="84.803059375" y1="25.6968" x2="84.82763125" y2="25.686759375" width="0.225" layer="94"/>
<wire x1="84.82763125" y1="25.686759375" x2="84.853609375" y2="25.681059375" width="0.225" layer="94"/>
<wire x1="84.853609375" y1="25.681059375" x2="84.90656875" y2="25.6762" width="0.225" layer="94"/>
<wire x1="84.90656875" y1="25.6762" x2="84.95978125" y2="25.6762" width="0.225" layer="94"/>
<wire x1="88.41505" y1="27.03186875" x2="88.835090625" y2="27.03186875" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="27.03186875" x2="88.835090625" y2="26.864" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="26.864" x2="88.835090625" y2="26.639" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="26.639" x2="88.835090625" y2="26.414" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="26.414" x2="88.835090625" y2="26.189" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="26.189" x2="88.835090625" y2="25.964" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="25.964" x2="88.835090625" y2="25.739" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="25.739" x2="88.835090625" y2="25.514" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="25.514" x2="88.835090625" y2="25.289" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="25.289" x2="88.835090625" y2="25.28548125" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="25.28548125" x2="88.41505" y2="25.28548125" width="0.225" layer="94"/>
<wire x1="88.41505" y1="25.28548125" x2="88.41505" y2="25.289" width="0.225" layer="94"/>
<wire x1="88.41505" y1="25.289" x2="88.41505" y2="25.514" width="0.225" layer="94"/>
<wire x1="88.41505" y1="25.514" x2="88.41505" y2="25.739" width="0.225" layer="94"/>
<wire x1="88.41505" y1="25.739" x2="88.41505" y2="25.964" width="0.225" layer="94"/>
<wire x1="88.41505" y1="25.964" x2="88.41505" y2="26.189" width="0.225" layer="94"/>
<wire x1="88.41505" y1="26.189" x2="88.41505" y2="26.414" width="0.225" layer="94"/>
<wire x1="88.41505" y1="26.414" x2="88.41505" y2="26.639" width="0.225" layer="94"/>
<wire x1="88.41505" y1="26.639" x2="88.41505" y2="26.864" width="0.225" layer="94"/>
<wire x1="88.41505" y1="26.864" x2="88.41505" y2="27.03186875" width="0.225" layer="94"/>
<wire x1="88.41505" y1="27.205890625" x2="88.835090625" y2="27.205890625" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="27.205890625" x2="88.835090625" y2="27.314" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="27.314" x2="88.835090625" y2="27.539" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="27.539" x2="88.835090625" y2="27.68606875" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="27.68606875" x2="88.41505" y2="27.68606875" width="0.225" layer="94"/>
<wire x1="88.41505" y1="27.68606875" x2="88.41505" y2="27.539" width="0.225" layer="94"/>
<wire x1="88.41505" y1="27.539" x2="88.41505" y2="27.314" width="0.225" layer="94"/>
<wire x1="88.41505" y1="27.314" x2="88.41505" y2="27.205890625" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="27.725040625" x2="75.911209375" y2="27.725040625" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="27.725040625" x2="75.911209375" y2="27.539" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="27.539" x2="75.911209375" y2="27.314" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="27.314" x2="75.911209375" y2="27.089" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="27.089" x2="75.911209375" y2="26.864" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="26.864" x2="75.911209375" y2="26.639" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="26.639" x2="75.911209375" y2="26.414" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="26.414" x2="75.911209375" y2="26.189" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="26.189" x2="75.911209375" y2="25.964" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="25.964" x2="75.911209375" y2="25.739" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="25.739" x2="75.911209375" y2="25.514" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="25.514" x2="75.911209375" y2="25.289" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="25.289" x2="75.911209375" y2="25.28548125" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="25.28548125" x2="75.49116875" y2="25.28548125" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="25.28548125" x2="75.49116875" y2="25.289" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="25.289" x2="75.49116875" y2="25.514" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="25.514" x2="75.49116875" y2="25.739" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="25.739" x2="75.49116875" y2="25.964" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="25.964" x2="75.49116875" y2="26.189" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="26.189" x2="75.49116875" y2="26.414" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="26.414" x2="75.49116875" y2="26.639" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="26.639" x2="75.49116875" y2="26.864" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="26.864" x2="75.49116875" y2="27.089" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="27.089" x2="75.49116875" y2="27.314" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="27.314" x2="75.49116875" y2="27.539" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="27.539" x2="75.49116875" y2="27.725040625" width="0.225" layer="94"/>
<wire x1="90.092" y1="25.97718125" x2="90.092" y2="26.189" width="0.225" layer="94"/>
<wire x1="90.092" y1="26.189" x2="90.092" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="90.41016875" y1="26.44828125" x2="90.41016875" y2="26.414" width="0.225" layer="94"/>
<wire x1="90.41016875" y1="26.414" x2="90.41016875" y2="26.30881875" width="0.225" layer="94"/>
<wire x1="90.41016875" y1="26.30881875" x2="90.830209375" y2="26.30881875" width="0.225" layer="94"/>
<wire x1="90.43711875" y1="26.071809375" x2="90.43711875" y2="25.964" width="0.225" layer="94"/>
<wire x1="90.43711875" y1="25.964" x2="90.43711875" y2="25.89925" width="0.225" layer="94"/>
<wire x1="90.43711875" y1="26.071809375" x2="90.86316875" y2="26.071809375" width="0.225" layer="94"/>
<wire x1="90.092" y1="26.34016875" x2="90.09408125" y2="26.45038125" width="0.225" layer="94"/>
<wire x1="90.09408125" y1="26.45038125" x2="90.09926875" y2="26.5163" width="0.225" layer="94"/>
<wire x1="90.09926875" y1="26.5163" x2="90.106540625" y2="26.55976875" width="0.225" layer="94"/>
<wire x1="90.106540625" y1="26.55976875" x2="90.112390625" y2="26.58101875" width="0.225" layer="94"/>
<wire x1="90.112390625" y1="26.58101875" x2="90.12096875" y2="26.6013" width="0.225" layer="94"/>
<wire x1="90.12096875" y1="26.6013" x2="90.135190625" y2="26.617890625" width="0.225" layer="94"/>
<wire x1="90.135190625" y1="26.617890625" x2="90.1551" y2="26.6272" width="0.225" layer="94"/>
<wire x1="90.1551" y1="26.6272" x2="90.17645" y2="26.63263125" width="0.225" layer="94"/>
<wire x1="90.17645" y1="26.63263125" x2="90.19825" y2="26.6359" width="0.225" layer="94"/>
<wire x1="90.19825" y1="26.6359" x2="90.220209375" y2="26.637740625" width="0.225" layer="94"/>
<wire x1="90.220209375" y1="26.637740625" x2="90.242240625" y2="26.63858125" width="0.225" layer="94"/>
<wire x1="90.242240625" y1="26.63858125" x2="90.264290625" y2="26.638640625" width="0.225" layer="94"/>
<wire x1="90.264290625" y1="26.638640625" x2="90.28631875" y2="26.6378" width="0.225" layer="94"/>
<wire x1="90.28631875" y1="26.6378" x2="90.30826875" y2="26.63575" width="0.225" layer="94"/>
<wire x1="90.30826875" y1="26.63575" x2="90.33" y2="26.63208125" width="0.225" layer="94"/>
<wire x1="90.33" y1="26.63208125" x2="90.351140625" y2="26.625909375" width="0.225" layer="94"/>
<wire x1="90.351140625" y1="26.625909375" x2="90.3705" y2="26.615509375" width="0.225" layer="94"/>
<wire x1="90.3705" y1="26.615509375" x2="90.38486875" y2="26.598990625" width="0.225" layer="94"/>
<wire x1="90.38486875" y1="26.598990625" x2="90.40023125" y2="26.557840625" width="0.225" layer="94"/>
<wire x1="90.40023125" y1="26.557840625" x2="90.407140625" y2="26.51431875" width="0.225" layer="94"/>
<wire x1="90.407140625" y1="26.51431875" x2="90.41016875" y2="26.44828125" width="0.225" layer="94"/>
<wire x1="90.092" y1="25.97718125" x2="90.09478125" y2="25.85786875" width="0.225" layer="94"/>
<wire x1="90.09478125" y1="25.85786875" x2="90.09865" y2="25.810290625" width="0.225" layer="94"/>
<wire x1="90.09865" y1="25.810290625" x2="90.11223125" y2="25.74008125" width="0.225" layer="94"/>
<wire x1="90.11223125" y1="25.74008125" x2="90.121240625" y2="25.718009375" width="0.225" layer="94"/>
<wire x1="90.121240625" y1="25.718009375" x2="90.136040625" y2="25.6995" width="0.225" layer="94"/>
<wire x1="90.136040625" y1="25.6995" x2="90.15721875" y2="25.688690625" width="0.225" layer="94"/>
<wire x1="90.15721875" y1="25.688690625" x2="90.18025" y2="25.68248125" width="0.225" layer="94"/>
<wire x1="90.18025" y1="25.68248125" x2="90.20383125" y2="25.678809375" width="0.225" layer="94"/>
<wire x1="90.20383125" y1="25.678809375" x2="90.251459375" y2="25.675840625" width="0.225" layer="94"/>
<wire x1="90.251459375" y1="25.675840625" x2="90.299190625" y2="25.67656875" width="0.225" layer="94"/>
<wire x1="90.299190625" y1="25.67656875" x2="90.34663125" y2="25.681690625" width="0.225" layer="94"/>
<wire x1="90.34663125" y1="25.681690625" x2="90.391890625" y2="25.696190625" width="0.225" layer="94"/>
<wire x1="90.391890625" y1="25.696190625" x2="90.409190625" y2="25.71226875" width="0.225" layer="94"/>
<wire x1="90.409190625" y1="25.71226875" x2="90.419459375" y2="25.733759375" width="0.225" layer="94"/>
<wire x1="90.419459375" y1="25.733759375" x2="90.425940625" y2="25.75671875" width="0.225" layer="94"/>
<wire x1="90.425940625" y1="25.75671875" x2="90.4332" y2="25.80388125" width="0.225" layer="94"/>
<wire x1="90.4332" y1="25.80388125" x2="90.436309375" y2="25.85151875" width="0.225" layer="94"/>
<wire x1="90.436309375" y1="25.85151875" x2="90.43711875" y2="25.89925" width="0.225" layer="94"/>
<wire x1="89.66001875" y1="25.980109375" x2="89.67878125" y2="25.718659375" width="0.225" layer="94" curve="8.208716"/>
<wire x1="89.67878125" y1="25.718659375" x2="89.74463125" y2="25.514" width="0.225" layer="94" curve="19.254744"/>
<wire x1="89.74463125" y1="25.514" x2="89.76749375" y2="25.47364375" width="0.225" layer="94" curve="4.135389"/>
<wire x1="89.767490625" y1="25.473640625" x2="89.966896875" y2="25.309234375" width="0.225" layer="94" curve="37.791863"/>
<wire x1="89.9669" y1="25.309240625" x2="90.02831875" y2="25.289" width="0.225" layer="94" curve="4.744864"/>
<wire x1="90.02831875" y1="25.289" x2="90.22315" y2="25.259475" width="0.225" layer="94" curve="14.492988"/>
<wire x1="90.86316875" y1="26.071809375" x2="90.86316875" y2="25.964" width="0.225" layer="94"/>
<wire x1="90.86316875" y1="25.964" x2="90.86316875" y2="25.89603125" width="0.225" layer="94"/>
<wire x1="90.83383125" y1="25.63598125" x2="90.852559375" y2="25.739" width="0.225" layer="94" curve="5.141671"/>
<wire x1="90.852559375" y1="25.739" x2="90.86316875" y2="25.89603125" width="0.225" layer="94" curve="7.731903"/>
<wire x1="90.710259375" y1="25.40781875" x2="90.83383125" y2="25.63598125" width="0.225" layer="94" curve="31.131749"/>
<wire x1="90.483690625" y1="25.282109375" x2="90.710259375" y2="25.40781875" width="0.225" layer="94" curve="33.941784"/>
<wire x1="90.22315" y1="25.25946875" x2="90.483690625" y2="25.282103125" width="0.225" layer="94" curve="14.175253"/>
<wire x1="89.66001875" y1="25.980109375" x2="89.66001875" y2="26.189" width="0.225" layer="94"/>
<wire x1="89.66001875" y1="26.189" x2="89.66001875" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="89.6773" y1="26.590590625" x2="89.66001875" y2="26.34016875" width="0.225" layer="94" curve="7.895583"/>
<wire x1="89.75726875" y1="26.82721875" x2="89.685840625" y2="26.639" width="0.225" layer="94" curve="17.336884"/>
<wire x1="89.685840625" y1="26.639" x2="89.6773" y2="26.590590625" width="0.225" layer="94" curve="4.218105"/>
<wire x1="89.93936875" y1="26.995190625" x2="89.75726875" y2="26.82721875" width="0.225" layer="94" curve="35.721204"/>
<wire x1="90.18176875" y1="27.05536875" x2="89.93936875" y2="26.995190625" width="0.225" layer="94" curve="21.771833"/>
<wire x1="90.830209375" y1="26.30881875" x2="90.830209375" y2="26.414" width="0.225" layer="94"/>
<wire x1="90.830209375" y1="26.414" x2="90.830209375" y2="26.49631875" width="0.225" layer="94"/>
<wire x1="90.830209375" y1="26.49631875" x2="90.7978375" y2="26.744540625" width="0.225" layer="94" curve="14.861165"/>
<wire x1="90.797840625" y1="26.744540625" x2="90.6616625" y2="26.951284375" width="0.225" layer="94" curve="37.022842"/>
<wire x1="90.661659375" y1="26.95128125" x2="90.432340625" y2="27.047065625" width="0.225" layer="94" curve="30.892228"/>
<wire x1="90.432340625" y1="27.04706875" x2="90.18176875" y2="27.055365625" width="0.225" layer="94" curve="10.656803"/>
<wire x1="68.6921" y1="33.944190625" x2="68.78723125" y2="33.839" width="0.225" layer="94"/>
<wire x1="68.78723125" y1="33.839" x2="69.804659375" y2="32.714" width="0.225" layer="94"/>
<wire x1="69.804659375" y1="32.714" x2="70.21163125" y2="32.264" width="0.225" layer="94"/>
<wire x1="70.21163125" y1="32.264" x2="71.229059375" y2="31.139" width="0.225" layer="94"/>
<wire x1="71.229059375" y1="31.139" x2="71.63603125" y2="30.689" width="0.225" layer="94"/>
<wire x1="71.63603125" y1="30.689" x2="72.653459375" y2="29.564" width="0.225" layer="94"/>
<wire x1="72.653459375" y1="29.564" x2="72.94835" y2="29.23793125" width="0.225" layer="94"/>
<wire x1="72.94835" y1="29.23793125" x2="73.44671875" y2="29.789" width="0.225" layer="94"/>
<wire x1="73.44671875" y1="29.789" x2="73.6502" y2="30.014" width="0.225" layer="94"/>
<wire x1="73.6502" y1="30.014" x2="74.26065" y2="30.689" width="0.225" layer="94"/>
<wire x1="74.26065" y1="30.689" x2="74.46413125" y2="30.914" width="0.225" layer="94"/>
<wire x1="74.46413125" y1="30.914" x2="75.278059375" y2="31.814" width="0.225" layer="94"/>
<wire x1="75.278059375" y1="31.814" x2="75.481540625" y2="32.039" width="0.225" layer="94"/>
<wire x1="75.481540625" y1="32.039" x2="76.29546875" y2="32.939" width="0.225" layer="94"/>
<wire x1="76.29546875" y1="32.939" x2="76.49895" y2="33.164" width="0.225" layer="94"/>
<wire x1="76.49895" y1="33.164" x2="76.70243125" y2="33.389" width="0.225" layer="94"/>
<wire x1="76.70243125" y1="33.389" x2="77.204709375" y2="33.944390625" width="0.225" layer="94"/>
<wire x1="77.204709375" y1="33.944390625" x2="76.036390625" y2="35.00103125" width="0.225" layer="94"/>
<wire x1="76.036390625" y1="35.00103125" x2="74.57861875" y2="33.389" width="0.225" layer="94"/>
<wire x1="74.57861875" y1="33.389" x2="74.37515" y2="33.164" width="0.225" layer="94"/>
<wire x1="74.37515" y1="33.164" x2="74.17168125" y2="32.939" width="0.225" layer="94"/>
<wire x1="74.17168125" y1="32.939" x2="73.764740625" y2="32.489" width="0.225" layer="94"/>
<wire x1="73.764740625" y1="32.489" x2="72.94835" y2="31.58621875" width="0.225" layer="94"/>
<wire x1="72.94835" y1="31.58621875" x2="72.742359375" y2="31.814" width="0.225" layer="94"/>
<wire x1="72.742359375" y1="31.814" x2="72.131940625" y2="32.489" width="0.225" layer="94"/>
<wire x1="72.131940625" y1="32.489" x2="71.724990625" y2="32.939" width="0.225" layer="94"/>
<wire x1="71.724990625" y1="32.939" x2="71.318040625" y2="33.389" width="0.225" layer="94"/>
<wire x1="71.318040625" y1="33.389" x2="70.911090625" y2="33.839" width="0.225" layer="94"/>
<wire x1="70.911090625" y1="33.839" x2="70.504140625" y2="34.289" width="0.225" layer="94"/>
<wire x1="70.504140625" y1="34.289" x2="70.097190625" y2="34.739" width="0.225" layer="94"/>
<wire x1="70.097190625" y1="34.739" x2="69.86021875" y2="35.001040625" width="0.225" layer="94"/>
<wire x1="81.341240625" y1="26.03255" x2="81.74326875" y2="26.03255" width="0.225" layer="94"/>
<wire x1="81.74326875" y1="26.03255" x2="81.74326875" y2="25.964" width="0.225" layer="94"/>
<wire x1="81.74326875" y1="25.964" x2="81.74326875" y2="25.812240625" width="0.225" layer="94"/>
<wire x1="81.723590625" y1="25.602390625" x2="81.743275" y2="25.812240625" width="0.225" layer="94" curve="10.717258"/>
<wire x1="81.637890625" y1="25.41138125" x2="81.7235875" y2="25.602390625" width="0.225" layer="94" curve="26.892331"/>
<wire x1="81.467640625" y1="25.291140625" x2="81.6378875" y2="25.41138125" width="0.225" layer="94" curve="34.316783"/>
<wire x1="81.260009375" y1="25.258559375" x2="81.467640625" y2="25.291140625" width="0.225" layer="94" curve="18.312496"/>
<wire x1="81.05158125" y1="25.28623125" x2="81.260009375" y2="25.258559375" width="0.225" layer="94" curve="14.64866"/>
<wire x1="80.880909375" y1="25.405009375" x2="81.05158125" y2="25.286234375" width="0.225" layer="94" curve="39.895242"/>
<wire x1="80.80178125" y1="25.5987" x2="80.880903125" y2="25.40500625" width="0.225" layer="94" curve="25.996239"/>
<wire x1="80.786140625" y1="25.80901875" x2="80.786140625" y2="25.964" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="25.964" x2="80.786140625" y2="26.189" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="26.189" x2="80.786140625" y2="26.414" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="26.414" x2="80.786140625" y2="26.626990625" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="25.80901875" x2="80.78728125" y2="25.739" width="0.225" layer="94" curve="3.295417"/>
<wire x1="80.78728125" y1="25.739" x2="80.801784375" y2="25.5987" width="0.225" layer="94" curve="6.640297"/>
<wire x1="81.206190625" y1="25.86908125" x2="81.206190625" y2="25.964" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="25.964" x2="81.206190625" y2="26.189" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="26.189" x2="81.206190625" y2="26.414" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="26.414" x2="81.206190625" y2="26.626990625" width="0.225" layer="94"/>
<wire x1="81.341240625" y1="26.03255" x2="81.341240625" y2="25.964" width="0.225" layer="94"/>
<wire x1="81.341240625" y1="25.964" x2="81.341240625" y2="25.779140625" width="0.225" layer="94"/>
<wire x1="81.317509375" y1="25.6654" x2="81.327084375" y2="25.6739125" width="0.225" layer="94" curve="49.818215"/>
<wire x1="81.30451875" y1="25.66235" x2="81.3175125" y2="25.66539375" width="0.225" layer="94" curve="7.103613"/>
<wire x1="81.291240625" y1="25.66093125" x2="81.30451875" y2="25.66235625" width="0.225" layer="94" curve="7.003084"/>
<wire x1="81.277890625" y1="25.660490625" x2="81.291240625" y2="25.660934375" width="0.225" layer="94" curve="1.449937"/>
<wire x1="81.26453125" y1="25.66073125" x2="81.277890625" y2="25.6604875" width="0.225" layer="94" curve="4.441603"/>
<wire x1="81.33501875" y1="25.69935" x2="81.33183125" y2="25.68638125" width="0.225" layer="94"/>
<wire x1="81.33183125" y1="25.68638125" x2="81.327090625" y2="25.673909375" width="0.225" layer="94"/>
<wire x1="81.33501875" y1="25.69935" x2="81.3373" y2="25.712509375" width="0.225" layer="94"/>
<wire x1="81.3373" y1="25.712509375" x2="81.340009375" y2="25.739" width="0.225" layer="94"/>
<wire x1="81.340009375" y1="25.739" x2="81.34001875" y2="25.739090625" width="0.225" layer="94"/>
<wire x1="81.34001875" y1="25.739090625" x2="81.341240625" y2="25.779140625" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="25.86908125" x2="81.20651875" y2="25.818159375" width="0.225" layer="94"/>
<wire x1="81.20651875" y1="25.818159375" x2="81.208009375" y2="25.76726875" width="0.225" layer="94"/>
<wire x1="81.208009375" y1="25.76726875" x2="81.209609375" y2="25.741859375" width="0.225" layer="94"/>
<wire x1="81.209609375" y1="25.741859375" x2="81.21228125" y2="25.716540625" width="0.225" layer="94"/>
<wire x1="81.21228125" y1="25.716540625" x2="81.21671875" y2="25.69148125" width="0.225" layer="94"/>
<wire x1="81.21671875" y1="25.69148125" x2="81.225321875" y2="25.66765" width="0.225" layer="94" curve="19.627877"/>
<wire x1="81.26453125" y1="25.66073125" x2="81.251209375" y2="25.66166875" width="0.225" layer="94"/>
<wire x1="81.238" y1="25.663609375" x2="81.251209375" y2="25.661671875" width="0.225" layer="94" curve="8.648156"/>
<wire x1="81.22531875" y1="25.66765" x2="81.238" y2="25.66360625" width="0.225" layer="94" curve="10.044676"/>
<wire x1="81.7013" y1="26.626990625" x2="81.206190625" y2="26.626990625" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="27.03186875" x2="81.7013" y2="27.03186875" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="27.401009375" x2="81.206190625" y2="27.314" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="27.314" x2="81.206190625" y2="27.089" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="27.089" x2="81.206190625" y2="27.03186875" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="27.03186875" x2="80.786140625" y2="27.089" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="27.089" x2="80.786140625" y2="27.314" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="27.314" x2="80.786140625" y2="27.401009375" width="0.225" layer="94"/>
<wire x1="80.627059375" y1="27.03186875" x2="80.786140625" y2="27.03186875" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="26.626990625" x2="80.627059375" y2="26.626990625" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="27.401009375" x2="81.206190625" y2="27.401009375" width="0.225" layer="94"/>
<wire x1="81.7013" y1="27.03186875" x2="81.7013" y2="26.864" width="0.225" layer="94"/>
<wire x1="81.7013" y1="26.864" x2="81.7013" y2="26.639" width="0.225" layer="94"/>
<wire x1="81.7013" y1="26.639" x2="81.7013" y2="26.626990625" width="0.225" layer="94"/>
<wire x1="80.627059375" y1="27.03186875" x2="80.627059375" y2="26.864" width="0.225" layer="94"/>
<wire x1="80.627059375" y1="26.864" x2="80.627059375" y2="26.639" width="0.225" layer="94"/>
<wire x1="80.627059375" y1="26.639" x2="80.627059375" y2="26.626990625" width="0.225" layer="94"/>
<wire x1="92.6429" y1="26.95253125" x2="92.469709375" y2="27.040159375" width="0.225" layer="94" curve="32.338936"/>
<wire x1="79.95228125" y1="26.071809375" x2="79.52623125" y2="26.071809375" width="0.225" layer="94"/>
<wire x1="79.49928125" y1="26.30881875" x2="79.91931875" y2="26.30881875" width="0.225" layer="94"/>
<wire x1="79.91931875" y1="26.30881875" x2="79.91931875" y2="26.414" width="0.225" layer="94"/>
<wire x1="79.91931875" y1="26.414" x2="79.91931875" y2="26.49631875" width="0.225" layer="94"/>
<wire x1="79.91931875" y1="26.49631875" x2="79.886946875" y2="26.744540625" width="0.225" layer="94" curve="14.861165"/>
<wire x1="79.88695" y1="26.744540625" x2="79.750771875" y2="26.951284375" width="0.225" layer="94" curve="37.022842"/>
<wire x1="79.75076875" y1="26.95128125" x2="79.521459375" y2="27.047065625" width="0.225" layer="94" curve="30.891086"/>
<wire x1="79.521459375" y1="27.04706875" x2="79.27088125" y2="27.055365625" width="0.225" layer="94" curve="10.657067"/>
<wire x1="79.27088125" y1="27.05536875" x2="79.02848125" y2="26.995190625" width="0.225" layer="94" curve="21.019898"/>
<wire x1="79.02848125" y1="26.995190625" x2="78.84638125" y2="26.82721875" width="0.225" layer="94" curve="36.47327"/>
<wire x1="78.84638125" y1="26.82721875" x2="78.77521875" y2="26.639" width="0.225" layer="94" curve="16.726592"/>
<wire x1="78.77521875" y1="26.639" x2="78.7664125" y2="26.590590625" width="0.225" layer="94" curve="4.076419"/>
<wire x1="78.766409375" y1="26.590590625" x2="78.75028125" y2="26.414" width="0.225" layer="94" curve="6.105964"/>
<wire x1="78.75028125" y1="26.414" x2="78.749128125" y2="26.34016875" width="0.225" layer="94" curve="2.541594"/>
<wire x1="79.95228125" y1="26.071809375" x2="79.95228125" y2="25.964" width="0.225" layer="94"/>
<wire x1="79.95228125" y1="25.964" x2="79.95228125" y2="25.89603125" width="0.225" layer="94"/>
<wire x1="79.922940625" y1="25.63598125" x2="79.94166875" y2="25.739" width="0.225" layer="94" curve="5.141671"/>
<wire x1="79.94166875" y1="25.739" x2="79.952278125" y2="25.89603125" width="0.225" layer="94" curve="7.731903"/>
<wire x1="79.79936875" y1="25.40781875" x2="79.922940625" y2="25.63598125" width="0.225" layer="94" curve="31.131749"/>
<wire x1="79.572809375" y1="25.282109375" x2="79.60065" y2="25.289" width="0.225" layer="94" curve="3.702975"/>
<wire x1="79.60065" y1="25.289" x2="79.799371875" y2="25.407815625" width="0.225" layer="94" curve="30.237845"/>
<wire x1="79.312259375" y1="25.25946875" x2="79.572809375" y2="25.28210625" width="0.225" layer="94" curve="14.175788"/>
<wire x1="79.056009375" y1="25.309240625" x2="79.119840625" y2="25.289" width="0.225" layer="94" curve="4.53295"/>
<wire x1="79.119840625" y1="25.289" x2="79.312259375" y2="25.259475" width="0.225" layer="94" curve="13.203582"/>
<wire x1="78.856609375" y1="25.473640625" x2="79.056009375" y2="25.3092375" width="0.225" layer="94" curve="39.29237"/>
<wire x1="78.767890625" y1="25.718659375" x2="78.85660625" y2="25.473640625" width="0.225" layer="94" curve="21.889189"/>
<wire x1="78.74913125" y1="25.980109375" x2="78.76789375" y2="25.718659375" width="0.225" layer="94" curve="9.71004"/>
<wire x1="78.74913125" y1="25.980109375" x2="78.74913125" y2="26.189" width="0.225" layer="94"/>
<wire x1="78.74913125" y1="26.189" x2="78.74913125" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="79.49928125" y1="26.30881875" x2="79.49928125" y2="26.414" width="0.225" layer="94"/>
<wire x1="79.49928125" y1="26.414" x2="79.49928125" y2="26.44828125" width="0.225" layer="94"/>
<wire x1="79.49928125" y1="26.44828125" x2="79.496253125" y2="26.51431875" width="0.225" layer="94" curve="5.249884"/>
<wire x1="79.49625" y1="26.51431875" x2="79.48934375" y2="26.557840625" width="0.225" layer="94" curve="7.529032"/>
<wire x1="79.489340625" y1="26.557840625" x2="79.483196875" y2="26.579" width="0.225" layer="94" curve="6.832689"/>
<wire x1="79.4832" y1="26.579" x2="79.47398125" y2="26.598990625" width="0.225" layer="94" curve="10.282953"/>
<wire x1="79.47398125" y1="26.598990625" x2="79.459609375" y2="26.615509375" width="0.225" layer="94" curve="22.257449"/>
<wire x1="79.459609375" y1="26.615509375" x2="79.440259375" y2="26.625909375" width="0.225" layer="94" curve="19.188066"/>
<wire x1="79.440259375" y1="26.625909375" x2="79.39738125" y2="26.63575" width="0.225" layer="94" curve="11.463239"/>
<wire x1="79.39738125" y1="26.63575" x2="79.3534" y2="26.6386375" width="0.225" layer="94" curve="6.876925"/>
<wire x1="79.3534" y1="26.638640625" x2="79.30931875" y2="26.63774375" width="0.225" layer="94" curve="2.970191"/>
<wire x1="79.30931875" y1="26.637740625" x2="79.265559375" y2="26.632625" width="0.225" layer="94" curve="8.0302"/>
<wire x1="79.265559375" y1="26.63263125" x2="79.244209375" y2="26.6272" width="0.225" layer="94" curve="7.184068"/>
<wire x1="79.244209375" y1="26.6272" x2="79.2243" y2="26.617890625" width="0.225" layer="94" curve="14.385749"/>
<wire x1="79.2243" y1="26.617890625" x2="79.210078125" y2="26.6013" width="0.225" layer="94" curve="34.295039"/>
<wire x1="79.21008125" y1="26.6013" x2="79.191453125" y2="26.53813125" width="0.225" layer="94" curve="14.037627"/>
<wire x1="79.19145" y1="26.53813125" x2="79.1844125" y2="26.472390625" width="0.225" layer="94" curve="6.60122"/>
<wire x1="79.52623125" y1="26.071809375" x2="79.52623125" y2="25.964" width="0.225" layer="94"/>
<wire x1="79.52623125" y1="25.964" x2="79.52623125" y2="25.89925" width="0.225" layer="94"/>
<wire x1="79.51938125" y1="25.780190625" x2="79.526234375" y2="25.89925" width="0.225" layer="94" curve="6.589587"/>
<wire x1="79.50856875" y1="25.733759375" x2="79.519378125" y2="25.780190625" width="0.225" layer="94" curve="13.026604"/>
<wire x1="79.45898125" y1="25.6871" x2="79.508565625" y2="25.733759375" width="0.225" layer="94" curve="54.248333"/>
<wire x1="79.4121" y1="25.6784" x2="79.45898125" y2="25.687103125" width="0.225" layer="94" curve="11.235116"/>
<wire x1="79.292940625" y1="25.678809375" x2="79.4121" y2="25.678396875" width="0.225" layer="94" curve="10.193817"/>
<wire x1="79.24633125" y1="25.688690625" x2="79.292940625" y2="25.6788125" width="0.225" layer="94" curve="13.338819"/>
<wire x1="79.22515" y1="25.6995" x2="79.24633125" y2="25.6886875" width="0.225" layer="94" curve="16.819291"/>
<wire x1="79.21035" y1="25.718009375" x2="79.225146875" y2="25.69949375" width="0.225" layer="94" curve="31.833994"/>
<wire x1="79.195259375" y1="25.763159375" x2="79.21035625" y2="25.7180125" width="0.225" layer="94" curve="8.452794"/>
<wire x1="79.187759375" y1="25.810290625" x2="79.19525625" y2="25.763159375" width="0.225" layer="94" curve="10.449934"/>
<wire x1="79.187759375" y1="25.810290625" x2="79.18275" y2="25.881709375" width="0.225" layer="94"/>
<wire x1="79.18441875" y1="26.472390625" x2="79.181740625" y2="26.406309375" width="0.225" layer="94"/>
<wire x1="79.181740625" y1="26.406309375" x2="79.181109375" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="79.181109375" y1="26.34016875" x2="79.181109375" y2="26.189" width="0.225" layer="94"/>
<wire x1="79.181109375" y1="26.189" x2="79.181109375" y2="25.97718125" width="0.225" layer="94"/>
<wire x1="79.181109375" y1="25.97718125" x2="79.18275" y2="25.881709375" width="0.225" layer="94"/>
<wire x1="77.1742" y1="26.422790625" x2="77.54935" y2="26.422790625" width="0.225" layer="94"/>
<wire x1="77.1742" y1="26.00881875" x2="77.969390625" y2="26.00881875" width="0.225" layer="94"/>
<wire x1="77.969390625" y1="26.00881875" x2="77.969390625" y2="26.189" width="0.225" layer="94"/>
<wire x1="77.969390625" y1="26.189" x2="77.969390625" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="77.969390625" y1="26.34016875" x2="77.948609375" y2="26.61348125" width="0.225" layer="94" curve="8.696144"/>
<wire x1="77.948609375" y1="26.61348125" x2="77.84784375" y2="26.866196875" width="0.225" layer="94" curve="26.085225"/>
<wire x1="77.84785" y1="26.8662" x2="77.62745" y2="27.022209375" width="0.225" layer="94" curve="39.85306"/>
<wire x1="77.62745" y1="27.022209375" x2="77.35661875" y2="27.058828125" width="0.225" layer="94" curve="15.332321"/>
<wire x1="77.35661875" y1="27.05881875" x2="77.08588125" y2="27.02155" width="0.225" layer="94" curve="15.743623"/>
<wire x1="77.08588125" y1="27.02155" x2="76.865265625" y2="26.865728125" width="0.225" layer="94" curve="39.046943"/>
<wire x1="76.865259375" y1="26.86573125" x2="76.864040625" y2="26.864" width="0.225" layer="94" curve="0.204743"/>
<wire x1="76.864040625" y1="26.864" x2="76.763478125" y2="26.61343125" width="0.225" layer="94" curve="26.341038"/>
<wire x1="76.76348125" y1="26.61343125" x2="76.74395" y2="26.414" width="0.225" layer="94" curve="6.208124"/>
<wire x1="76.74395" y1="26.414" x2="76.74221875" y2="26.34016875" width="0.225" layer="94" curve="2.287022"/>
<wire x1="77.561359375" y1="25.98158125" x2="77.966390625" y2="25.98158125" width="0.225" layer="94"/>
<wire x1="77.966390625" y1="25.98158125" x2="77.966390625" y2="25.964" width="0.225" layer="94"/>
<wire x1="77.966390625" y1="25.964" x2="77.966390625" y2="25.8181" width="0.225" layer="94"/>
<wire x1="77.92765" y1="25.565709375" x2="77.966390625" y2="25.8181" width="0.225" layer="94" curve="17.452125"/>
<wire x1="77.77643125" y1="25.363190625" x2="77.92765" y2="25.565709375" width="0.225" layer="94" curve="38.592139"/>
<wire x1="77.53896875" y1="25.271940625" x2="77.618090625" y2="25.289" width="0.225" layer="94" curve="8.168491"/>
<wire x1="77.618090625" y1="25.289" x2="77.77643125" y2="25.363190625" width="0.225" layer="94" curve="17.702304"/>
<wire x1="77.28301875" y1="25.26058125" x2="77.53896875" y2="25.271940625" width="0.225" layer="94" curve="11.086795"/>
<wire x1="77.03403125" y1="25.31643125" x2="77.11308125" y2="25.289" width="0.225" layer="94" curve="6.296209"/>
<wire x1="77.11308125" y1="25.289" x2="77.28301875" y2="25.260578125" width="0.225" layer="94" curve="12.986111"/>
<wire x1="76.844159375" y1="25.483540625" x2="77.034028125" y2="25.316421875" width="0.225" layer="94" curve="38.138006"/>
<wire x1="76.760209375" y1="25.724359375" x2="76.8441625" y2="25.483540625" width="0.225" layer="94" curve="20.717901"/>
<wire x1="76.74221875" y1="25.980109375" x2="76.758" y2="25.739" width="0.225" layer="94" curve="9.114768"/>
<wire x1="76.758" y1="25.739" x2="76.760209375" y2="25.724359375" width="0.225" layer="94" curve="0.557951"/>
<wire x1="76.74221875" y1="25.980109375" x2="76.74221875" y2="26.189" width="0.225" layer="94"/>
<wire x1="76.74221875" y1="26.189" x2="76.74221875" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="77.1742" y1="26.00881875" x2="77.1742" y2="25.97718125" width="0.225" layer="94"/>
<wire x1="77.1742" y1="25.97718125" x2="77.17648125" y2="25.863140625" width="0.225" layer="94"/>
<wire x1="77.17648125" y1="25.863140625" x2="77.179640625" y2="25.81763125" width="0.225" layer="94"/>
<wire x1="77.179640625" y1="25.81763125" x2="77.185759375" y2="25.77241875" width="0.225" layer="94"/>
<wire x1="77.185759375" y1="25.77241875" x2="77.19780625" y2="25.728490625" width="0.225" layer="94" curve="15.249939"/>
<wire x1="77.197809375" y1="25.728490625" x2="77.22771875" y2="25.69585" width="0.225" layer="94" curve="39.077584"/>
<wire x1="77.22771875" y1="25.69585" x2="77.2493" y2="25.688528125" width="0.225" layer="94" curve="18.445063"/>
<wire x1="77.2493" y1="25.68853125" x2="77.45375" y2="25.6834" width="0.225" layer="94" curve="16.158964"/>
<wire x1="77.45375" y1="25.6834" x2="77.498609375" y2="25.691559375" width="0.225" layer="94" curve="7.337797"/>
<wire x1="77.498609375" y1="25.691559375" x2="77.520209375" y2="25.6988375" width="0.225" layer="94" curve="9.286376"/>
<wire x1="77.561359375" y1="25.98158125" x2="77.561359375" y2="25.964" width="0.225" layer="94"/>
<wire x1="77.561359375" y1="25.964" x2="77.561359375" y2="25.82103125" width="0.225" layer="94"/>
<wire x1="77.561359375" y1="25.82103125" x2="77.559340625" y2="25.775459375" width="0.225" layer="94"/>
<wire x1="77.559340625" y1="25.775459375" x2="77.55613125" y2="25.75288125" width="0.225" layer="94"/>
<wire x1="77.520209375" y1="25.698840625" x2="77.556125" y2="25.75288125" width="0.225" layer="94" curve="51.030296"/>
<wire x1="77.184159375" y1="26.547240625" x2="77.174196875" y2="26.422790625" width="0.225" layer="94" curve="9.151585"/>
<wire x1="77.2077" y1="26.6134" x2="77.184159375" y2="26.547240625" width="0.225" layer="94" curve="20.872634"/>
<wire x1="77.54935" y1="26.422790625" x2="77.539775" y2="26.547290625" width="0.225" layer="94" curve="8.794744"/>
<wire x1="77.53978125" y1="26.547290625" x2="77.527525" y2="26.592734375" width="0.225" layer="94" curve="12.594721"/>
<wire x1="77.52751875" y1="26.59273125" x2="77.516803125" y2="26.613665625" width="0.225" layer="94" curve="11.429217"/>
<wire x1="77.516809375" y1="26.61366875" x2="77.5000125" y2="26.629975" width="0.225" layer="94" curve="26.064892"/>
<wire x1="77.500009375" y1="26.62996875" x2="77.47873125" y2="26.63995" width="0.225" layer="94" curve="11.965711"/>
<wire x1="77.47873125" y1="26.63995" x2="77.455990625" y2="26.64610625" width="0.225" layer="94" curve="8.00028"/>
<wire x1="77.455990625" y1="26.646109375" x2="77.38576875" y2="26.653571875" width="0.225" layer="94" curve="10.160203"/>
<wire x1="77.38576875" y1="26.65356875" x2="77.315109375" y2="26.652346875" width="0.225" layer="94" curve="3.952821"/>
<wire x1="77.315109375" y1="26.65235" x2="77.268440625" y2="26.646071875" width="0.225" layer="94" curve="9.390964"/>
<wire x1="77.268440625" y1="26.64606875" x2="77.245740625" y2="26.639796875" width="0.225" layer="94" curve="6.177099"/>
<wire x1="77.245740625" y1="26.6398" x2="77.24345" y2="26.639" width="0.225" layer="94" curve="1.397304"/>
<wire x1="77.24345" y1="26.639" x2="77.22448125" y2="26.629740625" width="0.225" layer="94" curve="12.178653"/>
<wire x1="77.22448125" y1="26.629740625" x2="77.207703125" y2="26.613396875" width="0.225" layer="94" curve="24.279547"/>
<wire x1="73.9395" y1="26.629740625" x2="73.922721875" y2="26.613396875" width="0.225" layer="94" curve="24.279547"/>
<wire x1="73.96075" y1="26.6398" x2="73.958459375" y2="26.639" width="0.225" layer="94" curve="1.397249"/>
<wire x1="73.958459375" y1="26.639" x2="73.939496875" y2="26.62974375" width="0.225" layer="94" curve="12.174138"/>
<wire x1="73.98345" y1="26.64606875" x2="73.96075" y2="26.639796875" width="0.225" layer="94" curve="6.177099"/>
<wire x1="74.03013125" y1="26.65235" x2="73.98345" y2="26.64606875" width="0.225" layer="94" curve="9.393557"/>
<wire x1="74.100790625" y1="26.65356875" x2="74.03013125" y2="26.652346875" width="0.225" layer="94" curve="3.952821"/>
<wire x1="74.171009375" y1="26.646109375" x2="74.100790625" y2="26.653571875" width="0.225" layer="94" curve="10.159745"/>
<wire x1="74.193740625" y1="26.63995" x2="74.171009375" y2="26.646103125" width="0.225" layer="94" curve="7.996993"/>
<wire x1="74.21501875" y1="26.62996875" x2="74.193740625" y2="26.63995" width="0.225" layer="94" curve="11.965711"/>
<wire x1="74.23181875" y1="26.61366875" x2="74.215021875" y2="26.629975" width="0.225" layer="94" curve="26.067092"/>
<wire x1="74.242540625" y1="26.59273125" x2="74.231821875" y2="26.613671875" width="0.225" layer="94" curve="11.432049"/>
<wire x1="74.254790625" y1="26.547290625" x2="74.2425375" y2="26.59273125" width="0.225" layer="94" curve="12.593993"/>
<wire x1="74.264359375" y1="26.422790625" x2="74.254784375" y2="26.547290625" width="0.225" layer="94" curve="8.794744"/>
<wire x1="73.92271875" y1="26.6134" x2="73.899178125" y2="26.547240625" width="0.225" layer="94" curve="20.872634"/>
<wire x1="73.89918125" y1="26.547240625" x2="73.88921875" y2="26.422790625" width="0.225" layer="94" curve="9.151622"/>
<wire x1="74.23521875" y1="25.698840625" x2="74.2678" y2="25.739" width="0.225" layer="94" curve="40.155698"/>
<wire x1="74.2678" y1="25.739" x2="74.271134375" y2="25.75288125" width="0.225" layer="94" curve="10.876304"/>
<wire x1="74.27435" y1="25.775459375" x2="74.271140625" y2="25.75288125" width="0.225" layer="94"/>
<wire x1="74.27638125" y1="25.82103125" x2="74.27435" y2="25.775459375" width="0.225" layer="94"/>
<wire x1="74.27638125" y1="25.98158125" x2="74.27638125" y2="25.964" width="0.225" layer="94"/>
<wire x1="74.27638125" y1="25.964" x2="74.27638125" y2="25.82103125" width="0.225" layer="94"/>
<wire x1="74.21361875" y1="25.691559375" x2="74.23521875" y2="25.6988375" width="0.225" layer="94" curve="9.286507"/>
<wire x1="74.16876875" y1="25.6834" x2="74.21361875" y2="25.691559375" width="0.225" layer="94" curve="7.336311"/>
<wire x1="73.964309375" y1="25.68853125" x2="74.16876875" y2="25.6834" width="0.225" layer="94" curve="16.159693"/>
<wire x1="73.94273125" y1="25.69585" x2="73.964309375" y2="25.68853125" width="0.225" layer="94" curve="18.441658"/>
<wire x1="73.91281875" y1="25.728490625" x2="73.942728125" y2="25.695846875" width="0.225" layer="94" curve="39.07874"/>
<wire x1="73.90078125" y1="25.77241875" x2="73.912828125" y2="25.72849375" width="0.225" layer="94" curve="15.249051"/>
<wire x1="73.894659375" y1="25.81763125" x2="73.90078125" y2="25.77241875" width="0.225" layer="94"/>
<wire x1="73.891490625" y1="25.863140625" x2="73.894659375" y2="25.81763125" width="0.225" layer="94"/>
<wire x1="73.88921875" y1="25.97718125" x2="73.88948125" y2="25.964" width="0.225" layer="94"/>
<wire x1="73.88948125" y1="25.964" x2="73.891490625" y2="25.863140625" width="0.225" layer="94"/>
<wire x1="73.88921875" y1="26.00881875" x2="73.88921875" y2="25.97718125" width="0.225" layer="94"/>
<wire x1="73.457240625" y1="25.980109375" x2="73.457240625" y2="26.189" width="0.225" layer="94"/>
<wire x1="73.457240625" y1="26.189" x2="73.457240625" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="73.457240625" y1="25.980109375" x2="73.47523125" y2="25.724359375" width="0.225" layer="94" curve="9.672758"/>
<wire x1="73.47523125" y1="25.724359375" x2="73.559184375" y2="25.48354375" width="0.225" layer="94" curve="20.717853"/>
<wire x1="73.55918125" y1="25.483540625" x2="73.749046875" y2="25.316425" width="0.225" layer="94" curve="38.137176"/>
<wire x1="73.74905" y1="25.31643125" x2="73.998040625" y2="25.26058125" width="0.225" layer="94" curve="19.282594"/>
<wire x1="73.998040625" y1="25.26058125" x2="74.25398125" y2="25.271940625" width="0.225" layer="94" curve="11.086392"/>
<wire x1="74.25398125" y1="25.271940625" x2="74.3331" y2="25.289" width="0.225" layer="94" curve="8.168179"/>
<wire x1="74.3331" y1="25.289" x2="74.491446875" y2="25.36319375" width="0.225" layer="94" curve="17.703003"/>
<wire x1="74.49145" y1="25.363190625" x2="74.642665625" y2="25.565709375" width="0.225" layer="94" curve="38.59175"/>
<wire x1="74.64266875" y1="25.565709375" x2="74.681409375" y2="25.8181" width="0.225" layer="94" curve="17.451998"/>
<wire x1="74.681409375" y1="25.98158125" x2="74.681409375" y2="25.964" width="0.225" layer="94"/>
<wire x1="74.681409375" y1="25.964" x2="74.681409375" y2="25.8181" width="0.225" layer="94"/>
<wire x1="74.27638125" y1="25.98158125" x2="74.681409375" y2="25.98158125" width="0.225" layer="94"/>
<wire x1="73.4785" y1="26.61343125" x2="73.45896875" y2="26.414" width="0.225" layer="94" curve="6.208134"/>
<wire x1="73.45896875" y1="26.414" x2="73.4572375" y2="26.34016875" width="0.225" layer="94" curve="2.287026"/>
<wire x1="73.58028125" y1="26.86573125" x2="73.4785" y2="26.61343125" width="0.225" layer="94" curve="26.545833"/>
<wire x1="73.8009" y1="27.02155" x2="73.58028125" y2="26.86573125" width="0.225" layer="94" curve="39.047008"/>
<wire x1="74.071640625" y1="27.05881875" x2="73.8009" y2="27.02155" width="0.225" layer="94" curve="15.743809"/>
<wire x1="74.342459375" y1="27.022209375" x2="74.071640625" y2="27.058825" width="0.225" layer="94" curve="15.331635"/>
<wire x1="74.562859375" y1="26.8662" x2="74.342459375" y2="27.022209375" width="0.225" layer="94" curve="39.85306"/>
<wire x1="74.66363125" y1="26.61348125" x2="74.5628625" y2="26.866203125" width="0.225" layer="94" curve="26.085633"/>
<wire x1="74.684409375" y1="26.34016875" x2="74.663628125" y2="26.61348125" width="0.225" layer="94" curve="8.69613"/>
<wire x1="74.684409375" y1="26.00881875" x2="74.684409375" y2="26.189" width="0.225" layer="94"/>
<wire x1="74.684409375" y1="26.189" x2="74.684409375" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="73.88921875" y1="26.00881875" x2="74.684409375" y2="26.00881875" width="0.225" layer="94"/>
<wire x1="73.88921875" y1="26.422790625" x2="74.264359375" y2="26.422790625" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="25.28548125" x2="82.92818125" y2="25.28548125" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="27.03186875" x2="82.80066875" y2="27.03186875" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="27.03186875" x2="82.508140625" y2="26.864" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="26.864" x2="82.508140625" y2="26.639" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="26.639" x2="82.508140625" y2="26.414" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="26.414" x2="82.508140625" y2="26.189" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="26.189" x2="82.508140625" y2="25.964" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="25.964" x2="82.508140625" y2="25.739" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="25.739" x2="82.508140625" y2="25.514" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="25.514" x2="82.508140625" y2="25.289" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="25.289" x2="82.508140625" y2="25.28548125" width="0.225" layer="94"/>
<wire x1="83.18028125" y1="26.30266875" x2="83.585309375" y2="26.30266875" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="26.358340625" x2="82.92818125" y2="26.189" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="26.189" x2="82.92818125" y2="25.964" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="25.964" x2="82.92818125" y2="25.739" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="25.739" x2="82.92818125" y2="25.514" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="25.514" x2="82.92818125" y2="25.289" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="25.289" x2="82.92818125" y2="25.28548125" width="0.225" layer="94"/>
<wire x1="82.8883" y1="26.98991875" x2="82.80066875" y2="27.03186875" width="0.225" layer="94" curve="51.162574"/>
<wire x1="86.810090625" y1="25.28548125" x2="86.810090625" y2="25.289" width="0.225" layer="94"/>
<wire x1="86.810090625" y1="25.289" x2="86.810090625" y2="25.514" width="0.225" layer="94"/>
<wire x1="86.810090625" y1="25.514" x2="86.810090625" y2="25.739" width="0.225" layer="94"/>
<wire x1="86.810090625" y1="25.739" x2="86.810090625" y2="25.964" width="0.225" layer="94"/>
<wire x1="86.810090625" y1="25.964" x2="86.810090625" y2="26.189" width="0.225" layer="94"/>
<wire x1="86.810090625" y1="26.189" x2="86.810090625" y2="26.25023125" width="0.225" layer="94"/>
<wire x1="86.39005" y1="25.28548125" x2="86.39005" y2="25.289" width="0.225" layer="94"/>
<wire x1="86.39005" y1="25.289" x2="86.39005" y2="25.514" width="0.225" layer="94"/>
<wire x1="86.39005" y1="25.514" x2="86.39005" y2="25.739" width="0.225" layer="94"/>
<wire x1="86.39005" y1="25.739" x2="86.39005" y2="25.964" width="0.225" layer="94"/>
<wire x1="86.39005" y1="25.964" x2="86.39005" y2="26.189" width="0.225" layer="94"/>
<wire x1="86.39005" y1="26.189" x2="86.39005" y2="26.414" width="0.225" layer="94"/>
<wire x1="86.39005" y1="26.414" x2="86.39005" y2="26.639" width="0.225" layer="94"/>
<wire x1="86.39005" y1="26.639" x2="86.39005" y2="26.864" width="0.225" layer="94"/>
<wire x1="86.39005" y1="26.864" x2="86.39005" y2="27.03186875" width="0.225" layer="94"/>
<wire x1="86.39005" y1="25.28548125" x2="86.810090625" y2="25.28548125" width="0.225" layer="94"/>
<wire x1="87.1462" y1="25.28548125" x2="87.566240625" y2="25.28548125" width="0.225" layer="94"/>
<wire x1="86.68258125" y1="27.03186875" x2="86.39005" y2="27.03186875" width="0.225" layer="94"/>
<wire x1="86.770490625" y1="26.98956875" x2="86.68258125" y2="27.03186875" width="0.225" layer="94" curve="51.389654"/>
<wire x1="83.585309375" y1="26.30266875" x2="83.585309375" y2="26.414" width="0.225" layer="94"/>
<wire x1="83.585309375" y1="26.414" x2="83.585309375" y2="26.532359375" width="0.225" layer="94"/>
<wire x1="83.585309375" y1="26.532359375" x2="83.57331875" y2="26.7009" width="0.225" layer="94" curve="8.13878"/>
<wire x1="83.57331875" y1="26.7009" x2="83.52315625" y2="26.8617" width="0.225" layer="94" curve="18.374099"/>
<wire x1="83.523159375" y1="26.8617" x2="83.41396875" y2="26.9887" width="0.225" layer="94" curve="28.349931"/>
<wire x1="83.41396875" y1="26.9887" x2="83.25806875" y2="27.050721875" width="0.225" layer="94" curve="26.884991"/>
<wire x1="83.25806875" y1="27.05071875" x2="83.0895" y2="27.055371875" width="0.225" layer="94" curve="13.343084"/>
<wire x1="83.0895" y1="27.05538125" x2="82.888296875" y2="26.989925" width="0.225" layer="94" curve="25.861473"/>
<wire x1="87.566240625" y1="25.28548125" x2="87.566240625" y2="25.289" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="25.289" x2="87.566240625" y2="25.514" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="25.514" x2="87.566240625" y2="25.739" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="25.739" x2="87.566240625" y2="25.964" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="25.964" x2="87.566240625" y2="26.189" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="26.189" x2="87.566240625" y2="26.414" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="26.414" x2="87.566240625" y2="26.418390625" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="26.418390625" x2="87.555215625" y2="26.606540625" width="0.225" layer="94" curve="6.706552"/>
<wire x1="87.55521875" y1="26.606540625" x2="87.509115625" y2="26.788871875" width="0.225" layer="94" curve="14.968121"/>
<wire x1="87.509109375" y1="26.78886875" x2="87.403925" y2="26.943603125" width="0.225" layer="94" curve="25.064472"/>
<wire x1="87.40393125" y1="26.943609375" x2="87.240590625" y2="27.034540625" width="0.225" layer="94" curve="28.312195"/>
<wire x1="87.240590625" y1="27.034540625" x2="87.05421875" y2="27.058825" width="0.225" layer="94" curve="15.049352"/>
<wire x1="87.05421875" y1="27.05881875" x2="86.770490625" y2="26.98956875" width="0.225" layer="94" curve="27.231353"/>
<wire x1="87.1462" y1="25.28548125" x2="87.1462" y2="25.289" width="0.225" layer="94"/>
<wire x1="87.1462" y1="25.289" x2="87.1462" y2="25.514" width="0.225" layer="94"/>
<wire x1="87.1462" y1="25.514" x2="87.1462" y2="25.739" width="0.225" layer="94"/>
<wire x1="87.1462" y1="25.739" x2="87.1462" y2="25.964" width="0.225" layer="94"/>
<wire x1="87.1462" y1="25.964" x2="87.1462" y2="26.189" width="0.225" layer="94"/>
<wire x1="87.1462" y1="26.189" x2="87.1462" y2="26.3642" width="0.225" layer="94"/>
<wire x1="87.1462" y1="26.3642" x2="87.139771875" y2="26.502909375" width="0.225" layer="94" curve="5.307835"/>
<wire x1="87.13976875" y1="26.502909375" x2="87.123915625" y2="26.584609375" width="0.225" layer="94" curve="11.346275"/>
<wire x1="87.12391875" y1="26.584609375" x2="87.11383125" y2="26.61046875" width="0.225" layer="94" curve="9.30383"/>
<wire x1="87.11383125" y1="26.61046875" x2="87.098490625" y2="26.633490625" width="0.225" layer="94" curve="15.447128"/>
<wire x1="87.098490625" y1="26.633490625" x2="87.0752" y2="26.648190625" width="0.225" layer="94" curve="32.662799"/>
<wire x1="87.0752" y1="26.648190625" x2="86.99275" y2="26.656371875" width="0.225" layer="94" curve="20.527762"/>
<wire x1="86.99275" y1="26.65636875" x2="86.9651" y2="26.653715625" width="0.225" layer="94" curve="1.764695"/>
<wire x1="86.9651" y1="26.65371875" x2="86.912034375" y2="26.638084375" width="0.225" layer="94" curve="20.107123"/>
<wire x1="86.91203125" y1="26.638090625" x2="86.870678125" y2="26.601821875" width="0.225" layer="94" curve="29.565942"/>
<wire x1="86.87068125" y1="26.60181875" x2="86.84560625" y2="26.55240625" width="0.225" layer="94" curve="14.114289"/>
<wire x1="86.8456" y1="26.552409375" x2="86.824978125" y2="26.471759375" width="0.225" layer="94" curve="11.013017"/>
<wire x1="86.82498125" y1="26.471759375" x2="86.813053125" y2="26.36131875" width="0.225" layer="94" curve="5.344301"/>
<wire x1="86.81305" y1="26.36131875" x2="86.810090625" y2="26.25023125" width="0.225" layer="94" curve="3.932748"/>
<wire x1="83.18028125" y1="26.30266875" x2="83.18028125" y2="26.414" width="0.225" layer="94"/>
<wire x1="83.18028125" y1="26.414" x2="83.18028125" y2="26.487240625" width="0.225" layer="94"/>
<wire x1="83.18028125" y1="26.487240625" x2="83.176690625" y2="26.56661875" width="0.225" layer="94" curve="5.181151"/>
<wire x1="83.176690625" y1="26.56661875" x2="83.16605625" y2="26.625190625" width="0.225" layer="94" curve="10.220705"/>
<wire x1="83.166059375" y1="26.625190625" x2="83.12728125" y2="26.664134375" width="0.225" layer="94" curve="58.95127"/>
<wire x1="83.12728125" y1="26.66413125" x2="82.99755" y2="26.639" width="0.225" layer="94" curve="53.217192"/>
<wire x1="82.99755" y1="26.639" x2="82.95845" y2="26.593540625" width="0.225" layer="94" curve="23.452395"/>
<wire x1="82.95845" y1="26.593540625" x2="82.95161875" y2="26.574890625" width="0.225" layer="94"/>
<wire x1="82.95161875" y1="26.574890625" x2="82.946159375" y2="26.55578125" width="0.225" layer="94"/>
<wire x1="82.946159375" y1="26.55578125" x2="82.941775" y2="26.5364" width="0.225" layer="94" curve="6.402047"/>
<wire x1="82.94176875" y1="26.5364" x2="82.931515625" y2="26.45761875" width="0.225" layer="94" curve="4.264451"/>
<wire x1="82.93151875" y1="26.45761875" x2="82.930209375" y2="26.437790625" width="0.225" layer="94"/>
<wire x1="82.930209375" y1="26.437790625" x2="82.92865" y2="26.39808125" width="0.225" layer="94"/>
<wire x1="82.92865" y1="26.39808125" x2="82.92818125" y2="26.358340625" width="0.225" layer="94"/>
<wire x1="92.67961875" y1="26.22455" x2="92.54725" y2="26.31605" width="0.225" layer="94" curve="24.406928"/>
<wire x1="92.54725" y1="26.31605" x2="92.391190625" y2="26.358428125" width="0.225" layer="94" curve="14.517731"/>
<wire x1="92.391190625" y1="26.35841875" x2="92.2218" y2="26.3835" width="0.225" layer="94"/>
<wire x1="92.2218" y1="26.3835" x2="92.13726875" y2="26.39681875" width="0.225" layer="94"/>
<wire x1="92.09533125" y1="26.40531875" x2="92.13726875" y2="26.39681875" width="0.225" layer="94" curve="5.000072"/>
<wire x1="92.0546" y1="26.41821875" x2="92.0661" y2="26.414" width="0.225" layer="94" curve="2.068384"/>
<wire x1="92.0661" y1="26.414" x2="92.09533125" y2="26.405321875" width="0.225" layer="94" curve="5.150249"/>
<wire x1="92.054009375" y1="26.430409375" x2="92.054596875" y2="26.41821875" width="0.225" layer="94" curve="5.514941"/>
<wire x1="92.054009375" y1="26.430409375" x2="92.054009375" y2="26.57718125" width="0.225" layer="94"/>
<wire x1="92.05665" y1="26.61703125" x2="92.0540125" y2="26.57718125" width="0.225" layer="94" curve="7.573746"/>
<wire x1="92.061659375" y1="26.63633125" x2="92.056646875" y2="26.61703125" width="0.225" layer="94" curve="13.971436"/>
<wire x1="92.068809375" y1="26.642740625" x2="92.0616625" y2="26.636328125" width="0.225" layer="94" curve="41.625207"/>
<wire x1="92.097790625" y1="26.650090625" x2="92.068809375" y2="26.642740625" width="0.225" layer="94" curve="13.721902"/>
<wire x1="92.15758125" y1="26.653940625" x2="92.097790625" y2="26.650090625" width="0.225" layer="94" curve="7.371099"/>
<wire x1="92.15758125" y1="26.653940625" x2="92.2566" y2="26.653940625" width="0.225" layer="94"/>
<wire x1="92.29483125" y1="26.652240625" x2="92.2566" y2="26.653940625" width="0.225" layer="94" curve="5.089448"/>
<wire x1="92.32143125" y1="26.647590625" x2="92.29483125" y2="26.652240625" width="0.225" layer="94" curve="9.654072"/>
<wire x1="92.331859375" y1="26.64346875" x2="92.32143125" y2="26.6475875" width="0.225" layer="94" curve="13.639596"/>
<wire x1="92.33591875" y1="26.63303125" x2="92.33185625" y2="26.64346875" width="0.225" layer="94" curve="15.08669"/>
<wire x1="92.34036875" y1="26.6065" x2="92.335915625" y2="26.63303125" width="0.225" layer="94" curve="8.390546"/>
<wire x1="92.34215" y1="26.568390625" x2="92.340375" y2="26.6065" width="0.225" layer="94" curve="5.332675"/>
<wire x1="92.34215" y1="26.568390625" x2="92.34215" y2="26.414" width="0.225" layer="94"/>
<wire x1="92.34215" y1="26.414" x2="92.34215" y2="26.413709375" width="0.225" layer="94"/>
<wire x1="92.34215" y1="26.413709375" x2="92.759190625" y2="26.413709375" width="0.225" layer="94"/>
<wire x1="92.759190625" y1="26.413709375" x2="92.759190625" y2="26.414" width="0.225" layer="94"/>
<wire x1="92.759190625" y1="26.414" x2="92.759190625" y2="26.5892" width="0.225" layer="94"/>
<wire x1="92.759190625" y1="26.5892" x2="92.738496875" y2="26.78398125" width="0.225" layer="94" curve="12.129007"/>
<wire x1="92.738490625" y1="26.78398125" x2="92.642896875" y2="26.952528125" width="0.225" layer="94" curve="34.862523"/>
<wire x1="92.469709375" y1="27.040159375" x2="92.27461875" y2="27.058825" width="0.225" layer="94" curve="10.407762"/>
<wire x1="92.27461875" y1="27.05881875" x2="92.139559375" y2="27.05881875" width="0.225" layer="94"/>
<wire x1="92.139559375" y1="27.05881875" x2="91.94439375" y2="27.0367625" width="0.225" layer="94" curve="15.066235"/>
<wire x1="91.944390625" y1="27.03676875" x2="91.76905625" y2="26.950671875" width="0.225" layer="94" curve="24.344315"/>
<wire x1="91.769059375" y1="26.95066875" x2="91.656853125" y2="26.79153125" width="0.225" layer="94" curve="32.9752"/>
<wire x1="91.65685" y1="26.79153125" x2="91.626409375" y2="26.639" width="0.225" layer="94" curve="14.82531"/>
<wire x1="91.626409375" y1="26.639" x2="91.62503125" y2="26.59828125" width="0.225" layer="94" curve="3.873269"/>
<wire x1="91.62503125" y1="26.59828125" x2="91.62503125" y2="26.414" width="0.225" layer="94"/>
<wire x1="91.62503125" y1="26.414" x2="91.62503125" y2="26.39436875" width="0.225" layer="94"/>
<wire x1="91.62503125" y1="26.39436875" x2="91.654403125" y2="26.22111875" width="0.225" layer="94" curve="19.244585"/>
<wire x1="91.6544" y1="26.22111875" x2="91.767428125" y2="26.088775" width="0.225" layer="94" curve="42.510013"/>
<wire x1="91.76743125" y1="26.08878125" x2="91.931890625" y2="26.02583125" width="0.225" layer="94" curve="14.601117"/>
<wire x1="91.931890625" y1="26.02583125" x2="92.105790625" y2="25.992246875" width="0.225" layer="94" curve="5.428189"/>
<wire x1="92.105790625" y1="25.99225" x2="92.16531875" y2="25.98243125" width="0.225" layer="94"/>
<wire x1="92.16531875" y1="25.98243125" x2="92.22491875" y2="25.97266875" width="0.225" layer="94"/>
<wire x1="92.22491875" y1="25.97266875" x2="92.28428125" y2="25.96156875" width="0.225" layer="94"/>
<wire x1="92.327340625" y1="25.94943125" x2="92.28428125" y2="25.9615625" width="0.225" layer="94" curve="10.274433"/>
<wire x1="92.34125" y1="25.94243125" x2="92.32734375" y2="25.9494375" width="0.225" layer="94" curve="11.730311"/>
<wire x1="92.346959375" y1="25.91516875" x2="92.34124375" y2="25.942428125" width="0.225" layer="94" curve="13.906979"/>
<wire x1="92.34815" y1="25.887240625" x2="92.346959375" y2="25.91516875" width="0.225" layer="94" curve="4.888175"/>
<wire x1="92.34815" y1="25.887240625" x2="92.34815" y2="25.7431" width="0.225" layer="94"/>
<wire x1="92.346759375" y1="25.71286875" x2="92.348153125" y2="25.7431" width="0.225" layer="94" curve="5.283856"/>
<wire x1="92.34045" y1="25.683359375" x2="92.34675625" y2="25.71286875" width="0.225" layer="94" curve="13.55484"/>
<wire x1="92.333959375" y1="25.67608125" x2="92.340453125" y2="25.683359375" width="0.225" layer="94" curve="45.791754"/>
<wire x1="92.314840625" y1="25.66976875" x2="92.333959375" y2="25.676084375" width="0.225" layer="94" curve="14.183972"/>
<wire x1="92.274840625" y1="25.66463125" x2="92.314840625" y2="25.669771875" width="0.225" layer="94" curve="7.7277"/>
<wire x1="92.24416875" y1="25.663709375" x2="92.274840625" y2="25.664628125" width="0.225" layer="94" curve="3.484382"/>
<wire x1="92.24416875" y1="25.663709375" x2="92.1096" y2="25.663709375" width="0.225" layer="94"/>
<wire x1="92.07173125" y1="25.66515" x2="92.1096" y2="25.66370625" width="0.225" layer="94" curve="4.363033"/>
<wire x1="92.04318125" y1="25.66956875" x2="92.07173125" y2="25.66514375" width="0.225" layer="94" curve="8.889144"/>
<wire x1="92.03463125" y1="25.672309375" x2="92.04318125" y2="25.66956875" width="0.225" layer="94" curve="9.043139"/>
<wire x1="92.03186875" y1="25.680859375" x2="92.034628125" y2="25.672309375" width="0.225" layer="94" curve="7.986668"/>
<wire x1="92.02805" y1="25.70145" x2="92.031871875" y2="25.680859375" width="0.225" layer="94" curve="6.761788"/>
<wire x1="92.02548125" y1="25.73128125" x2="92.028053125" y2="25.70145" width="0.225" layer="94" curve="4.406702"/>
<wire x1="92.024059375" y1="25.79115" x2="92.025484375" y2="25.73128125" width="0.225" layer="94" curve="2.724283"/>
<wire x1="92.024059375" y1="25.79115" x2="92.024059375" y2="25.964" width="0.225" layer="94"/>
<wire x1="92.024059375" y1="25.964" x2="92.024059375" y2="25.99651875" width="0.225" layer="94"/>
<wire x1="92.024059375" y1="25.99651875" x2="91.60701875" y2="25.99651875" width="0.225" layer="94"/>
<wire x1="91.60701875" y1="25.99651875" x2="91.60701875" y2="25.964" width="0.225" layer="94"/>
<wire x1="91.60701875" y1="25.964" x2="91.60701875" y2="25.739" width="0.225" layer="94"/>
<wire x1="91.60701875" y1="25.739" x2="91.60701875" y2="25.728159375" width="0.225" layer="94"/>
<wire x1="91.60701875" y1="25.728159375" x2="91.6277125" y2="25.53338125" width="0.225" layer="94" curve="12.128778"/>
<wire x1="91.627709375" y1="25.53338125" x2="91.72330625" y2="25.364828125" width="0.225" layer="94" curve="34.863434"/>
<wire x1="91.723309375" y1="25.36483125" x2="91.85098125" y2="25.289" width="0.225" layer="94" curve="24.602823"/>
<wire x1="91.85098125" y1="25.289" x2="91.896490625" y2="25.277203125" width="0.225" layer="94" curve="7.735491"/>
<wire x1="91.896490625" y1="25.2772" x2="92.091590625" y2="25.258534375" width="0.225" layer="94" curve="10.40831"/>
<wire x1="92.091590625" y1="25.25853125" x2="92.262609375" y2="25.25853125" width="0.225" layer="94"/>
<wire x1="92.262609375" y1="25.25853125" x2="92.457790625" y2="25.2805875" width="0.225" layer="94" curve="15.067174"/>
<wire x1="92.457790625" y1="25.280590625" x2="92.633140625" y2="25.36668125" width="0.225" layer="94" curve="24.336812"/>
<wire x1="92.633140625" y1="25.36668125" x2="92.74113125" y2="25.514" width="0.225" layer="94" curve="30.878359"/>
<wire x1="92.74113125" y1="25.514" x2="92.74536875" y2="25.525809375" width="0.225" layer="94" curve="2.095521"/>
<wire x1="92.74536875" y1="25.525809375" x2="92.777203125" y2="25.71908125" width="0.225" layer="94" curve="18.707757"/>
<wire x1="92.777209375" y1="25.71908125" x2="92.777209375" y2="25.739" width="0.225" layer="94"/>
<wire x1="92.777209375" y1="25.739" x2="92.777209375" y2="25.92328125" width="0.225" layer="94"/>
<wire x1="92.777209375" y1="25.92328125" x2="92.756978125" y2="26.083621875" width="0.225" layer="94" curve="14.383055"/>
<wire x1="92.75696875" y1="26.08361875" x2="92.6796125" y2="26.22454375" width="0.225" layer="94" curve="28.759193"/>
<wire x1="64.9221" y1="37.86731875" x2="101.4471" y2="37.86731875" width="0.225" layer="94"/>
<wire x1="101.4471" y1="37.86731875" x2="101.4471" y2="37.664" width="0.225" layer="94"/>
<wire x1="101.4471" y1="37.664" x2="101.4471" y2="37.439" width="0.225" layer="94"/>
<wire x1="101.4471" y1="37.439" x2="101.4471" y2="37.214" width="0.225" layer="94"/>
<wire x1="101.4471" y1="37.214" x2="101.4471" y2="36.989" width="0.225" layer="94"/>
<wire x1="101.4471" y1="36.989" x2="101.4471" y2="36.764" width="0.225" layer="94"/>
<wire x1="101.4471" y1="36.764" x2="101.4471" y2="36.539" width="0.225" layer="94"/>
<wire x1="101.4471" y1="36.539" x2="101.4471" y2="36.314" width="0.225" layer="94"/>
<wire x1="101.4471" y1="36.314" x2="101.4471" y2="36.089" width="0.225" layer="94"/>
<wire x1="101.4471" y1="36.089" x2="101.4471" y2="35.864" width="0.225" layer="94"/>
<wire x1="101.4471" y1="35.864" x2="101.4471" y2="35.639" width="0.225" layer="94"/>
<wire x1="101.4471" y1="35.639" x2="101.4471" y2="35.414" width="0.225" layer="94"/>
<wire x1="101.4471" y1="35.414" x2="101.4471" y2="35.189" width="0.225" layer="94"/>
<wire x1="101.4471" y1="35.189" x2="101.4471" y2="34.964" width="0.225" layer="94"/>
<wire x1="101.4471" y1="34.964" x2="101.4471" y2="34.739" width="0.225" layer="94"/>
<wire x1="101.4471" y1="34.739" x2="101.4471" y2="34.514" width="0.225" layer="94"/>
<wire x1="101.4471" y1="34.514" x2="101.4471" y2="34.289" width="0.225" layer="94"/>
<wire x1="101.4471" y1="34.289" x2="101.4471" y2="34.064" width="0.225" layer="94"/>
<wire x1="101.4471" y1="34.064" x2="101.4471" y2="33.839" width="0.225" layer="94"/>
<wire x1="101.4471" y1="33.839" x2="101.4471" y2="33.614" width="0.225" layer="94"/>
<wire x1="101.4471" y1="33.614" x2="101.4471" y2="33.389" width="0.225" layer="94"/>
<wire x1="101.4471" y1="33.389" x2="101.4471" y2="33.164" width="0.225" layer="94"/>
<wire x1="101.4471" y1="33.164" x2="101.4471" y2="32.939" width="0.225" layer="94"/>
<wire x1="101.4471" y1="32.939" x2="101.4471" y2="32.714" width="0.225" layer="94"/>
<wire x1="101.4471" y1="32.714" x2="101.4471" y2="32.489" width="0.225" layer="94"/>
<wire x1="101.4471" y1="32.489" x2="101.4471" y2="32.264" width="0.225" layer="94"/>
<wire x1="101.4471" y1="32.264" x2="101.4471" y2="32.039" width="0.225" layer="94"/>
<wire x1="101.4471" y1="32.039" x2="101.4471" y2="31.814" width="0.225" layer="94"/>
<wire x1="101.4471" y1="31.814" x2="101.4471" y2="31.589" width="0.225" layer="94"/>
<wire x1="101.4471" y1="31.589" x2="101.4471" y2="31.364" width="0.225" layer="94"/>
<wire x1="101.4471" y1="31.364" x2="101.4471" y2="31.139" width="0.225" layer="94"/>
<wire x1="101.4471" y1="31.139" x2="101.4471" y2="30.914" width="0.225" layer="94"/>
<wire x1="101.4471" y1="30.914" x2="101.4471" y2="30.689" width="0.225" layer="94"/>
<wire x1="101.4471" y1="30.689" x2="101.4471" y2="30.464" width="0.225" layer="94"/>
<wire x1="101.4471" y1="30.464" x2="101.4471" y2="30.239" width="0.225" layer="94"/>
<wire x1="101.4471" y1="30.239" x2="101.4471" y2="30.014" width="0.225" layer="94"/>
<wire x1="101.4471" y1="30.014" x2="101.4471" y2="29.789" width="0.225" layer="94"/>
<wire x1="101.4471" y1="29.789" x2="101.4471" y2="29.564" width="0.225" layer="94"/>
<wire x1="101.4471" y1="29.564" x2="101.4471" y2="29.339" width="0.225" layer="94"/>
<wire x1="101.4471" y1="29.339" x2="101.4471" y2="29.114" width="0.225" layer="94"/>
<wire x1="101.4471" y1="29.114" x2="101.4471" y2="28.889" width="0.225" layer="94"/>
<wire x1="101.4471" y1="28.889" x2="101.4471" y2="28.664" width="0.225" layer="94"/>
<wire x1="101.4471" y1="28.664" x2="101.4471" y2="28.439" width="0.225" layer="94"/>
<wire x1="101.4471" y1="28.439" x2="101.4471" y2="28.214" width="0.225" layer="94"/>
<wire x1="101.4471" y1="28.214" x2="101.4471" y2="27.989" width="0.225" layer="94"/>
<wire x1="101.4471" y1="27.989" x2="101.4471" y2="27.764" width="0.225" layer="94"/>
<wire x1="101.4471" y1="27.764" x2="101.4471" y2="27.539" width="0.225" layer="94"/>
<wire x1="101.4471" y1="27.539" x2="101.4471" y2="27.314" width="0.225" layer="94"/>
<wire x1="101.4471" y1="27.314" x2="101.4471" y2="27.089" width="0.225" layer="94"/>
<wire x1="101.4471" y1="27.089" x2="101.4471" y2="26.864" width="0.225" layer="94"/>
<wire x1="101.4471" y1="26.864" x2="101.4471" y2="26.639" width="0.225" layer="94"/>
<wire x1="101.4471" y1="26.639" x2="101.4471" y2="26.414" width="0.225" layer="94"/>
<wire x1="101.4471" y1="26.414" x2="101.4471" y2="26.189" width="0.225" layer="94"/>
<wire x1="101.4471" y1="26.189" x2="101.4471" y2="25.964" width="0.225" layer="94"/>
<wire x1="101.4471" y1="25.964" x2="101.4471" y2="25.739" width="0.225" layer="94"/>
<wire x1="101.4471" y1="25.739" x2="101.4471" y2="25.514" width="0.225" layer="94"/>
<wire x1="101.4471" y1="25.514" x2="101.4471" y2="25.289" width="0.225" layer="94"/>
<wire x1="101.4471" y1="25.289" x2="101.4471" y2="25.064" width="0.225" layer="94"/>
<wire x1="101.4471" y1="25.064" x2="101.4471" y2="24.839" width="0.225" layer="94"/>
<wire x1="101.4471" y1="24.839" x2="101.4471" y2="24.614" width="0.225" layer="94"/>
<wire x1="101.4471" y1="24.614" x2="101.4471" y2="24.389" width="0.225" layer="94"/>
<wire x1="101.4471" y1="24.389" x2="101.4471" y2="24.164" width="0.225" layer="94"/>
<wire x1="101.4471" y1="24.164" x2="101.4471" y2="23.939" width="0.225" layer="94"/>
<wire x1="101.4471" y1="23.939" x2="101.4471" y2="23.714" width="0.225" layer="94"/>
<wire x1="101.4471" y1="23.714" x2="101.4471" y2="23.489" width="0.225" layer="94"/>
<wire x1="101.4471" y1="23.489" x2="101.4471" y2="23.264" width="0.225" layer="94"/>
<wire x1="101.4471" y1="23.264" x2="101.4471" y2="23.09231875" width="0.225" layer="94"/>
<wire x1="101.4471" y1="23.09231875" x2="64.9221" y2="23.09231875" width="0.225" layer="94"/>
<wire x1="64.9221" y1="23.09231875" x2="64.9221" y2="23.264" width="0.225" layer="94"/>
<wire x1="64.9221" y1="23.264" x2="64.9221" y2="23.489" width="0.225" layer="94"/>
<wire x1="64.9221" y1="23.489" x2="64.9221" y2="23.714" width="0.225" layer="94"/>
<wire x1="64.9221" y1="23.714" x2="64.9221" y2="23.939" width="0.225" layer="94"/>
<wire x1="64.9221" y1="23.939" x2="64.9221" y2="24.164" width="0.225" layer="94"/>
<wire x1="64.9221" y1="24.164" x2="64.9221" y2="24.389" width="0.225" layer="94"/>
<wire x1="64.9221" y1="24.389" x2="64.9221" y2="24.614" width="0.225" layer="94"/>
<wire x1="64.9221" y1="24.614" x2="64.9221" y2="24.839" width="0.225" layer="94"/>
<wire x1="64.9221" y1="24.839" x2="64.9221" y2="25.064" width="0.225" layer="94"/>
<wire x1="64.9221" y1="25.064" x2="64.9221" y2="25.289" width="0.225" layer="94"/>
<wire x1="64.9221" y1="25.289" x2="64.9221" y2="25.514" width="0.225" layer="94"/>
<wire x1="64.9221" y1="25.514" x2="64.9221" y2="25.739" width="0.225" layer="94"/>
<wire x1="64.9221" y1="25.739" x2="64.9221" y2="25.964" width="0.225" layer="94"/>
<wire x1="64.9221" y1="25.964" x2="64.9221" y2="26.189" width="0.225" layer="94"/>
<wire x1="64.9221" y1="26.189" x2="64.9221" y2="26.414" width="0.225" layer="94"/>
<wire x1="64.9221" y1="26.414" x2="64.9221" y2="26.639" width="0.225" layer="94"/>
<wire x1="64.9221" y1="26.639" x2="64.9221" y2="26.864" width="0.225" layer="94"/>
<wire x1="64.9221" y1="26.864" x2="64.9221" y2="27.089" width="0.225" layer="94"/>
<wire x1="64.9221" y1="27.089" x2="64.9221" y2="27.314" width="0.225" layer="94"/>
<wire x1="64.9221" y1="27.314" x2="64.9221" y2="27.539" width="0.225" layer="94"/>
<wire x1="64.9221" y1="27.539" x2="64.9221" y2="27.764" width="0.225" layer="94"/>
<wire x1="64.9221" y1="27.764" x2="64.9221" y2="27.989" width="0.225" layer="94"/>
<wire x1="64.9221" y1="27.989" x2="64.9221" y2="28.214" width="0.225" layer="94"/>
<wire x1="64.9221" y1="28.214" x2="64.9221" y2="28.439" width="0.225" layer="94"/>
<wire x1="64.9221" y1="28.439" x2="64.9221" y2="28.664" width="0.225" layer="94"/>
<wire x1="64.9221" y1="28.664" x2="64.9221" y2="28.889" width="0.225" layer="94"/>
<wire x1="64.9221" y1="28.889" x2="64.9221" y2="29.114" width="0.225" layer="94"/>
<wire x1="64.9221" y1="29.114" x2="64.9221" y2="29.339" width="0.225" layer="94"/>
<wire x1="64.9221" y1="29.339" x2="64.9221" y2="29.564" width="0.225" layer="94"/>
<wire x1="64.9221" y1="29.564" x2="64.9221" y2="29.789" width="0.225" layer="94"/>
<wire x1="64.9221" y1="29.789" x2="64.9221" y2="30.014" width="0.225" layer="94"/>
<wire x1="64.9221" y1="30.014" x2="64.9221" y2="30.239" width="0.225" layer="94"/>
<wire x1="64.9221" y1="30.239" x2="64.9221" y2="30.464" width="0.225" layer="94"/>
<wire x1="64.9221" y1="30.464" x2="64.9221" y2="30.689" width="0.225" layer="94"/>
<wire x1="64.9221" y1="30.689" x2="64.9221" y2="30.914" width="0.225" layer="94"/>
<wire x1="64.9221" y1="30.914" x2="64.9221" y2="31.139" width="0.225" layer="94"/>
<wire x1="64.9221" y1="31.139" x2="64.9221" y2="31.364" width="0.225" layer="94"/>
<wire x1="64.9221" y1="31.364" x2="64.9221" y2="31.589" width="0.225" layer="94"/>
<wire x1="64.9221" y1="31.589" x2="64.9221" y2="31.814" width="0.225" layer="94"/>
<wire x1="64.9221" y1="31.814" x2="64.9221" y2="32.039" width="0.225" layer="94"/>
<wire x1="64.9221" y1="32.039" x2="64.9221" y2="32.264" width="0.225" layer="94"/>
<wire x1="64.9221" y1="32.264" x2="64.9221" y2="32.489" width="0.225" layer="94"/>
<wire x1="64.9221" y1="32.489" x2="64.9221" y2="32.714" width="0.225" layer="94"/>
<wire x1="64.9221" y1="32.714" x2="64.9221" y2="32.939" width="0.225" layer="94"/>
<wire x1="64.9221" y1="32.939" x2="64.9221" y2="33.164" width="0.225" layer="94"/>
<wire x1="64.9221" y1="33.164" x2="64.9221" y2="33.389" width="0.225" layer="94"/>
<wire x1="64.9221" y1="33.389" x2="64.9221" y2="33.614" width="0.225" layer="94"/>
<wire x1="64.9221" y1="33.614" x2="64.9221" y2="33.839" width="0.225" layer="94"/>
<wire x1="64.9221" y1="33.839" x2="64.9221" y2="34.064" width="0.225" layer="94"/>
<wire x1="64.9221" y1="34.064" x2="64.9221" y2="34.289" width="0.225" layer="94"/>
<wire x1="64.9221" y1="34.289" x2="64.9221" y2="34.514" width="0.225" layer="94"/>
<wire x1="64.9221" y1="34.514" x2="64.9221" y2="34.739" width="0.225" layer="94"/>
<wire x1="64.9221" y1="34.739" x2="64.9221" y2="34.964" width="0.225" layer="94"/>
<wire x1="64.9221" y1="34.964" x2="64.9221" y2="35.189" width="0.225" layer="94"/>
<wire x1="64.9221" y1="35.189" x2="64.9221" y2="35.414" width="0.225" layer="94"/>
<wire x1="64.9221" y1="35.414" x2="64.9221" y2="35.639" width="0.225" layer="94"/>
<wire x1="64.9221" y1="35.639" x2="64.9221" y2="35.864" width="0.225" layer="94"/>
<wire x1="64.9221" y1="35.864" x2="64.9221" y2="36.089" width="0.225" layer="94"/>
<wire x1="64.9221" y1="36.089" x2="64.9221" y2="36.314" width="0.225" layer="94"/>
<wire x1="64.9221" y1="36.314" x2="64.9221" y2="36.539" width="0.225" layer="94"/>
<wire x1="64.9221" y1="36.539" x2="64.9221" y2="36.764" width="0.225" layer="94"/>
<wire x1="64.9221" y1="36.764" x2="64.9221" y2="36.989" width="0.225" layer="94"/>
<wire x1="64.9221" y1="36.989" x2="64.9221" y2="37.214" width="0.225" layer="94"/>
<wire x1="64.9221" y1="37.214" x2="64.9221" y2="37.439" width="0.225" layer="94"/>
<wire x1="64.9221" y1="37.439" x2="64.9221" y2="37.664" width="0.225" layer="94"/>
<wire x1="64.9221" y1="37.664" x2="64.9221" y2="37.86731875" width="0.225" layer="94"/>
<wire x1="64.9221" y1="37.664" x2="101.4471" y2="37.664" width="0.225" layer="94"/>
<wire x1="64.9221" y1="37.439" x2="101.4471" y2="37.439" width="0.225" layer="94"/>
<wire x1="64.9221" y1="37.214" x2="101.4471" y2="37.214" width="0.225" layer="94"/>
<wire x1="64.9221" y1="36.989" x2="101.4471" y2="36.989" width="0.225" layer="94"/>
<wire x1="64.9221" y1="36.764" x2="101.4471" y2="36.764" width="0.225" layer="94"/>
<wire x1="64.9221" y1="36.539" x2="101.4471" y2="36.539" width="0.225" layer="94"/>
<wire x1="64.9221" y1="36.314" x2="101.4471" y2="36.314" width="0.225" layer="94"/>
<wire x1="64.9221" y1="36.089" x2="101.4471" y2="36.089" width="0.225" layer="94"/>
<wire x1="64.9221" y1="35.864" x2="101.4471" y2="35.864" width="0.225" layer="94"/>
<wire x1="64.9221" y1="35.639" x2="101.4471" y2="35.639" width="0.225" layer="94"/>
<wire x1="64.9221" y1="35.414" x2="101.4471" y2="35.414" width="0.225" layer="94"/>
<wire x1="64.9221" y1="35.189" x2="101.4471" y2="35.189" width="0.225" layer="94"/>
<wire x1="64.9221" y1="34.964" x2="69.81928125" y2="34.964" width="0.225" layer="94"/>
<wire x1="76.07733125" y1="34.964" x2="101.4471" y2="34.964" width="0.225" layer="94"/>
<wire x1="69.89371875" y1="34.964" x2="76.0029" y2="34.964" width="0.225" layer="94"/>
<wire x1="64.9221" y1="34.739" x2="69.570590625" y2="34.739" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="34.739" x2="101.4471" y2="34.739" width="0.225" layer="94"/>
<wire x1="70.097190625" y1="34.739" x2="75.79943125" y2="34.739" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="34.739" x2="90.66211875" y2="34.739" width="0.225" layer="94"/>
<wire x1="76.326109375" y1="34.739" x2="78.286090625" y2="34.739" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="34.739" x2="87.068640625" y2="34.739" width="0.225" layer="94"/>
<wire x1="64.9221" y1="34.514" x2="69.3219" y2="34.514" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="34.514" x2="101.4471" y2="34.514" width="0.225" layer="94"/>
<wire x1="70.30066875" y1="34.514" x2="75.595959375" y2="34.514" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="34.514" x2="90.66211875" y2="34.514" width="0.225" layer="94"/>
<wire x1="76.574890625" y1="34.514" x2="78.286090625" y2="34.514" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="34.514" x2="87.068640625" y2="34.514" width="0.225" layer="94"/>
<wire x1="64.9221" y1="34.289" x2="69.073209375" y2="34.289" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="34.289" x2="101.4471" y2="34.289" width="0.225" layer="94"/>
<wire x1="70.504140625" y1="34.289" x2="75.3925" y2="34.289" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="34.289" x2="90.66211875" y2="34.289" width="0.225" layer="94"/>
<wire x1="76.82366875" y1="34.289" x2="78.286090625" y2="34.289" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="34.289" x2="87.068640625" y2="34.289" width="0.225" layer="94"/>
<wire x1="64.9221" y1="34.064" x2="68.82451875" y2="34.064" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="34.064" x2="101.4471" y2="34.064" width="0.225" layer="94"/>
<wire x1="70.70761875" y1="34.064" x2="75.18903125" y2="34.064" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="34.064" x2="90.66211875" y2="34.064" width="0.225" layer="94"/>
<wire x1="77.07245" y1="34.064" x2="78.286090625" y2="34.064" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="34.064" x2="87.068640625" y2="34.064" width="0.225" layer="94"/>
<wire x1="64.9221" y1="33.839" x2="68.78723125" y2="33.839" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="33.839" x2="101.4471" y2="33.839" width="0.225" layer="94"/>
<wire x1="70.911090625" y1="33.839" x2="74.985559375" y2="33.839" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="33.839" x2="90.66211875" y2="33.839" width="0.225" layer="94"/>
<wire x1="77.1094" y1="33.839" x2="78.286090625" y2="33.839" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="33.839" x2="87.068640625" y2="33.839" width="0.225" layer="94"/>
<wire x1="64.9221" y1="33.614" x2="68.990709375" y2="33.614" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="33.614" x2="101.4471" y2="33.614" width="0.225" layer="94"/>
<wire x1="71.114559375" y1="33.614" x2="74.782090625" y2="33.614" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="33.614" x2="90.66211875" y2="33.614" width="0.225" layer="94"/>
<wire x1="76.905909375" y1="33.614" x2="78.286090625" y2="33.614" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="33.614" x2="87.068640625" y2="33.614" width="0.225" layer="94"/>
<wire x1="64.9221" y1="33.389" x2="69.1942" y2="33.389" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="33.389" x2="101.4471" y2="33.389" width="0.225" layer="94"/>
<wire x1="71.318040625" y1="33.389" x2="74.57861875" y2="33.389" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="33.389" x2="90.66211875" y2="33.389" width="0.225" layer="94"/>
<wire x1="76.70243125" y1="33.389" x2="78.286090625" y2="33.389" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="33.389" x2="87.068640625" y2="33.389" width="0.225" layer="94"/>
<wire x1="64.9221" y1="33.164" x2="69.397690625" y2="33.164" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="33.164" x2="101.4471" y2="33.164" width="0.225" layer="94"/>
<wire x1="71.521509375" y1="33.164" x2="74.37515" y2="33.164" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="33.164" x2="93.287040625" y2="33.164" width="0.225" layer="94"/>
<wire x1="76.49895" y1="33.164" x2="87.068640625" y2="33.164" width="0.225" layer="94"/>
<wire x1="64.9221" y1="32.939" x2="69.60116875" y2="32.939" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="32.939" x2="101.4471" y2="32.939" width="0.225" layer="94"/>
<wire x1="71.724990625" y1="32.939" x2="74.17168125" y2="32.939" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="32.939" x2="93.287040625" y2="32.939" width="0.225" layer="94"/>
<wire x1="76.29546875" y1="32.939" x2="87.068640625" y2="32.939" width="0.225" layer="94"/>
<wire x1="64.9221" y1="32.714" x2="69.804659375" y2="32.714" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="32.714" x2="101.4471" y2="32.714" width="0.225" layer="94"/>
<wire x1="71.928459375" y1="32.714" x2="73.968209375" y2="32.714" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="32.714" x2="93.287040625" y2="32.714" width="0.225" layer="94"/>
<wire x1="76.091990625" y1="32.714" x2="78.286240625" y2="32.714" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="32.714" x2="87.068640625" y2="32.714" width="0.225" layer="94"/>
<wire x1="64.9221" y1="32.489" x2="70.008140625" y2="32.489" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="32.489" x2="101.4471" y2="32.489" width="0.225" layer="94"/>
<wire x1="72.131940625" y1="32.489" x2="73.764740625" y2="32.489" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="32.489" x2="93.287040625" y2="32.489" width="0.225" layer="94"/>
<wire x1="75.8885" y1="32.489" x2="78.286240625" y2="32.489" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="32.489" x2="87.068640625" y2="32.489" width="0.225" layer="94"/>
<wire x1="64.9221" y1="32.264" x2="70.21163125" y2="32.264" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="32.264" x2="101.4471" y2="32.264" width="0.225" layer="94"/>
<wire x1="72.335409375" y1="32.264" x2="73.56126875" y2="32.264" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="32.264" x2="93.287040625" y2="32.264" width="0.225" layer="94"/>
<wire x1="75.68501875" y1="32.264" x2="78.286240625" y2="32.264" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="32.264" x2="87.068640625" y2="32.264" width="0.225" layer="94"/>
<wire x1="64.9221" y1="32.039" x2="70.415109375" y2="32.039" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="32.039" x2="101.4471" y2="32.039" width="0.225" layer="94"/>
<wire x1="72.53888125" y1="32.039" x2="73.3578" y2="32.039" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="32.039" x2="93.287040625" y2="32.039" width="0.225" layer="94"/>
<wire x1="75.481540625" y1="32.039" x2="78.286240625" y2="32.039" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="32.039" x2="87.068640625" y2="32.039" width="0.225" layer="94"/>
<wire x1="64.9221" y1="31.814" x2="70.6186" y2="31.814" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="31.814" x2="101.4471" y2="31.814" width="0.225" layer="94"/>
<wire x1="72.742359375" y1="31.814" x2="73.15433125" y2="31.814" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="31.814" x2="93.287040625" y2="31.814" width="0.225" layer="94"/>
<wire x1="75.278059375" y1="31.814" x2="78.286240625" y2="31.814" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="31.814" x2="87.068640625" y2="31.814" width="0.225" layer="94"/>
<wire x1="64.9221" y1="31.589" x2="70.822090625" y2="31.589" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="31.589" x2="101.4471" y2="31.589" width="0.225" layer="94"/>
<wire x1="72.94583125" y1="31.589" x2="72.950859375" y2="31.589" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="31.589" x2="93.287040625" y2="31.589" width="0.225" layer="94"/>
<wire x1="75.07458125" y1="31.589" x2="78.286240625" y2="31.589" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="31.589" x2="87.068640625" y2="31.589" width="0.225" layer="94"/>
<wire x1="64.9221" y1="31.364" x2="71.02556875" y2="31.364" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="31.364" x2="101.4471" y2="31.364" width="0.225" layer="94"/>
<wire x1="74.871090625" y1="31.364" x2="87.068640625" y2="31.364" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="31.364" x2="93.287040625" y2="31.364" width="0.225" layer="94"/>
<wire x1="64.9221" y1="31.139" x2="71.229059375" y2="31.139" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="31.139" x2="101.4471" y2="31.139" width="0.225" layer="94"/>
<wire x1="74.667609375" y1="31.139" x2="87.068640625" y2="31.139" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="31.139" x2="93.287040625" y2="31.139" width="0.225" layer="94"/>
<wire x1="64.9221" y1="30.914" x2="71.432540625" y2="30.914" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="30.914" x2="101.4471" y2="30.914" width="0.225" layer="94"/>
<wire x1="74.46413125" y1="30.914" x2="78.286090625" y2="30.914" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="30.914" x2="93.287040625" y2="30.914" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="30.914" x2="87.068640625" y2="30.914" width="0.225" layer="94"/>
<wire x1="64.9221" y1="30.689" x2="71.63603125" y2="30.689" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="30.689" x2="101.4471" y2="30.689" width="0.225" layer="94"/>
<wire x1="74.26065" y1="30.689" x2="78.286090625" y2="30.689" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="30.689" x2="93.287040625" y2="30.689" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="30.689" x2="87.068640625" y2="30.689" width="0.225" layer="94"/>
<wire x1="64.9221" y1="30.464" x2="71.839509375" y2="30.464" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="30.464" x2="101.4471" y2="30.464" width="0.225" layer="94"/>
<wire x1="74.057159375" y1="30.464" x2="78.286090625" y2="30.464" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="30.464" x2="93.287040625" y2="30.464" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="30.464" x2="87.068640625" y2="30.464" width="0.225" layer="94"/>
<wire x1="64.9221" y1="30.239" x2="72.043" y2="30.239" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="30.239" x2="101.4471" y2="30.239" width="0.225" layer="94"/>
<wire x1="73.85368125" y1="30.239" x2="78.286090625" y2="30.239" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="30.239" x2="93.287040625" y2="30.239" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="30.239" x2="87.068640625" y2="30.239" width="0.225" layer="94"/>
<wire x1="64.9221" y1="30.014" x2="72.246490625" y2="30.014" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="30.014" x2="101.4471" y2="30.014" width="0.225" layer="94"/>
<wire x1="73.6502" y1="30.014" x2="78.286090625" y2="30.014" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="30.014" x2="93.287040625" y2="30.014" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="30.014" x2="87.068640625" y2="30.014" width="0.225" layer="94"/>
<wire x1="64.9221" y1="29.789" x2="72.44996875" y2="29.789" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="29.789" x2="101.4471" y2="29.789" width="0.225" layer="94"/>
<wire x1="73.44671875" y1="29.789" x2="78.286090625" y2="29.789" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="29.789" x2="93.287040625" y2="29.789" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="29.789" x2="87.068640625" y2="29.789" width="0.225" layer="94"/>
<wire x1="64.9221" y1="29.564" x2="72.653459375" y2="29.564" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="29.564" x2="101.4471" y2="29.564" width="0.225" layer="94"/>
<wire x1="73.243240625" y1="29.564" x2="78.286090625" y2="29.564" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="29.564" x2="93.287040625" y2="29.564" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="29.564" x2="87.068640625" y2="29.564" width="0.225" layer="94"/>
<wire x1="64.9221" y1="29.339" x2="72.856940625" y2="29.339" width="0.225" layer="94"/>
<wire x1="73.03975" y1="29.339" x2="101.4471" y2="29.339" width="0.225" layer="94"/>
<wire x1="64.9221" y1="29.114" x2="101.4471" y2="29.114" width="0.225" layer="94"/>
<wire x1="64.9221" y1="28.889" x2="101.4471" y2="28.889" width="0.225" layer="94"/>
<wire x1="64.9221" y1="28.664" x2="101.4471" y2="28.664" width="0.225" layer="94"/>
<wire x1="64.9221" y1="28.439" x2="101.4471" y2="28.439" width="0.225" layer="94"/>
<wire x1="64.9221" y1="28.214" x2="101.4471" y2="28.214" width="0.225" layer="94"/>
<wire x1="64.9221" y1="27.989" x2="101.4471" y2="27.989" width="0.225" layer="94"/>
<wire x1="64.9221" y1="27.764" x2="101.4471" y2="27.764" width="0.225" layer="94"/>
<wire x1="64.9221" y1="27.539" x2="75.49116875" y2="27.539" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="27.539" x2="101.4471" y2="27.539" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="27.539" x2="88.41505" y2="27.539" width="0.225" layer="94"/>
<wire x1="64.9221" y1="27.314" x2="75.49116875" y2="27.314" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="27.314" x2="101.4471" y2="27.314" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="27.314" x2="80.786140625" y2="27.314" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="27.314" x2="88.41505" y2="27.314" width="0.225" layer="94"/>
<wire x1="64.9221" y1="27.089" x2="75.49116875" y2="27.089" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="27.089" x2="101.4471" y2="27.089" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="27.089" x2="80.786140625" y2="27.089" width="0.225" layer="94"/>
<wire x1="64.9221" y1="26.864" x2="73.579059375" y2="26.864" width="0.225" layer="94"/>
<wire x1="92.70988125" y1="26.864" x2="101.4471" y2="26.864" width="0.225" layer="94"/>
<wire x1="74.56438125" y1="26.864" x2="75.49116875" y2="26.864" width="0.225" layer="94"/>
<wire x1="90.74325" y1="26.864" x2="91.69128125" y2="26.864" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="26.864" x2="76.864040625" y2="26.864" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="26.864" x2="89.78075" y2="26.864" width="0.225" layer="94"/>
<wire x1="77.84936875" y1="26.864" x2="78.869559375" y2="26.864" width="0.225" layer="94"/>
<wire x1="87.470340625" y1="26.864" x2="88.41505" y2="26.864" width="0.225" layer="94"/>
<wire x1="79.832359375" y1="26.864" x2="80.627059375" y2="26.864" width="0.225" layer="94"/>
<wire x1="85.42721875" y1="26.864" x2="86.39005" y2="26.864" width="0.225" layer="94"/>
<wire x1="81.7013" y1="26.864" x2="82.508140625" y2="26.864" width="0.225" layer="94"/>
<wire x1="83.522" y1="26.864" x2="84.441909375" y2="26.864" width="0.225" layer="94"/>
<wire x1="64.9221" y1="26.639" x2="73.48298125" y2="26.639" width="0.225" layer="94"/>
<wire x1="92.75785" y1="26.639" x2="101.4471" y2="26.639" width="0.225" layer="94"/>
<wire x1="73.958459375" y1="26.639" x2="74.19638125" y2="26.639" width="0.225" layer="94"/>
<wire x1="92.06346875" y1="26.639" x2="92.334359375" y2="26.639" width="0.225" layer="94"/>
<wire x1="74.659159375" y1="26.639" x2="75.49116875" y2="26.639" width="0.225" layer="94"/>
<wire x1="90.81963125" y1="26.639" x2="91.626409375" y2="26.639" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="26.639" x2="76.76796875" y2="26.639" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="26.639" x2="89.685840625" y2="26.639" width="0.225" layer="94"/>
<wire x1="77.24345" y1="26.639" x2="77.48136875" y2="26.639" width="0.225" layer="94"/>
<wire x1="87.55065" y1="26.639" x2="88.41505" y2="26.639" width="0.225" layer="94"/>
<wire x1="77.94415" y1="26.639" x2="78.77521875" y2="26.639" width="0.225" layer="94"/>
<wire x1="86.913890625" y1="26.639" x2="87.09276875" y2="26.639" width="0.225" layer="94"/>
<wire x1="79.90875" y1="26.639" x2="80.627059375" y2="26.639" width="0.225" layer="94"/>
<wire x1="85.522" y1="26.639" x2="86.39005" y2="26.639" width="0.225" layer="94"/>
<wire x1="81.7013" y1="26.639" x2="82.508140625" y2="26.639" width="0.225" layer="94"/>
<wire x1="83.58053125" y1="26.639" x2="84.345909375" y2="26.639" width="0.225" layer="94"/>
<wire x1="82.99755" y1="26.639" x2="83.160159375" y2="26.639" width="0.225" layer="94"/>
<wire x1="64.9221" y1="26.414" x2="73.45896875" y2="26.414" width="0.225" layer="94"/>
<wire x1="92.759190625" y1="26.414" x2="101.4471" y2="26.414" width="0.225" layer="94"/>
<wire x1="74.6829" y1="26.414" x2="75.49116875" y2="26.414" width="0.225" layer="94"/>
<wire x1="92.0661" y1="26.414" x2="92.34215" y2="26.414" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="26.414" x2="76.74395" y2="26.414" width="0.225" layer="94"/>
<wire x1="90.830209375" y1="26.414" x2="91.62503125" y2="26.414" width="0.225" layer="94"/>
<wire x1="77.967890625" y1="26.414" x2="78.75028125" y2="26.414" width="0.225" layer="94"/>
<wire x1="90.0934" y1="26.414" x2="90.41016875" y2="26.414" width="0.225" layer="94"/>
<wire x1="79.18205" y1="26.414" x2="79.49928125" y2="26.414" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="26.414" x2="89.661509375" y2="26.414" width="0.225" layer="94"/>
<wire x1="79.91931875" y1="26.414" x2="80.786140625" y2="26.414" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="26.414" x2="88.41505" y2="26.414" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="26.414" x2="82.508140625" y2="26.414" width="0.225" layer="94"/>
<wire x1="86.817440625" y1="26.414" x2="87.14536875" y2="26.414" width="0.225" layer="94"/>
<wire x1="82.92926875" y1="26.414" x2="83.18028125" y2="26.414" width="0.225" layer="94"/>
<wire x1="85.545740625" y1="26.414" x2="86.39005" y2="26.414" width="0.225" layer="94"/>
<wire x1="83.585309375" y1="26.414" x2="84.32161875" y2="26.414" width="0.225" layer="94"/>
<wire x1="84.754290625" y1="26.414" x2="85.11306875" y2="26.414" width="0.225" layer="94"/>
<wire x1="64.9221" y1="26.189" x2="73.457240625" y2="26.189" width="0.225" layer="94"/>
<wire x1="92.708490625" y1="26.189" x2="101.4471" y2="26.189" width="0.225" layer="94"/>
<wire x1="74.684409375" y1="26.189" x2="75.49116875" y2="26.189" width="0.225" layer="94"/>
<wire x1="90.092" y1="26.189" x2="91.66831875" y2="26.189" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="26.189" x2="76.74221875" y2="26.189" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="26.189" x2="89.66001875" y2="26.189" width="0.225" layer="94"/>
<wire x1="77.969390625" y1="26.189" x2="78.74913125" y2="26.189" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="26.189" x2="88.41505" y2="26.189" width="0.225" layer="94"/>
<wire x1="79.181109375" y1="26.189" x2="80.786140625" y2="26.189" width="0.225" layer="94"/>
<wire x1="86.810090625" y1="26.189" x2="87.1462" y2="26.189" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="26.189" x2="82.508140625" y2="26.189" width="0.225" layer="94"/>
<wire x1="85.54725" y1="26.189" x2="86.39005" y2="26.189" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="26.189" x2="84.32008125" y2="26.189" width="0.225" layer="94"/>
<wire x1="84.752059375" y1="26.189" x2="85.115190625" y2="26.189" width="0.225" layer="94"/>
<wire x1="64.9221" y1="25.964" x2="73.457090625" y2="25.964" width="0.225" layer="94"/>
<wire x1="92.77591875" y1="25.964" x2="101.4471" y2="25.964" width="0.225" layer="94"/>
<wire x1="73.88948125" y1="25.964" x2="74.27638125" y2="25.964" width="0.225" layer="94"/>
<wire x1="92.024059375" y1="25.964" x2="92.27126875" y2="25.964" width="0.225" layer="94"/>
<wire x1="74.681409375" y1="25.964" x2="75.49116875" y2="25.964" width="0.225" layer="94"/>
<wire x1="90.86316875" y1="25.964" x2="91.60701875" y2="25.964" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="25.964" x2="76.74208125" y2="25.964" width="0.225" layer="94"/>
<wire x1="90.092309375" y1="25.964" x2="90.43711875" y2="25.964" width="0.225" layer="94"/>
<wire x1="77.17446875" y1="25.964" x2="77.561359375" y2="25.964" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="25.964" x2="89.660090625" y2="25.964" width="0.225" layer="94"/>
<wire x1="77.966390625" y1="25.964" x2="78.749" y2="25.964" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="25.964" x2="88.41505" y2="25.964" width="0.225" layer="94"/>
<wire x1="79.181340625" y1="25.964" x2="79.52623125" y2="25.964" width="0.225" layer="94"/>
<wire x1="86.810090625" y1="25.964" x2="87.1462" y2="25.964" width="0.225" layer="94"/>
<wire x1="79.95228125" y1="25.964" x2="80.786140625" y2="25.964" width="0.225" layer="94"/>
<wire x1="85.54718125" y1="25.964" x2="86.39005" y2="25.964" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="25.964" x2="81.341240625" y2="25.964" width="0.225" layer="94"/>
<wire x1="84.752340625" y1="25.964" x2="85.114909375" y2="25.964" width="0.225" layer="94"/>
<wire x1="81.74326875" y1="25.964" x2="82.508140625" y2="25.964" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="25.964" x2="84.32015" y2="25.964" width="0.225" layer="94"/>
<wire x1="64.9221" y1="25.739" x2="73.47301875" y2="25.739" width="0.225" layer="94"/>
<wire x1="92.777209375" y1="25.739" x2="101.4471" y2="25.739" width="0.225" layer="94"/>
<wire x1="73.90876875" y1="25.739" x2="74.2678" y2="25.739" width="0.225" layer="94"/>
<wire x1="92.025140625" y1="25.739" x2="92.34813125" y2="25.739" width="0.225" layer="94"/>
<wire x1="74.67768125" y1="25.739" x2="75.49116875" y2="25.739" width="0.225" layer="94"/>
<wire x1="90.852559375" y1="25.739" x2="91.60701875" y2="25.739" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="25.739" x2="76.758" y2="25.739" width="0.225" layer="94"/>
<wire x1="90.11266875" y1="25.739" x2="90.420940625" y2="25.739" width="0.225" layer="94"/>
<wire x1="77.193759375" y1="25.739" x2="77.55278125" y2="25.739" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="25.739" x2="89.675959375" y2="25.739" width="0.225" layer="94"/>
<wire x1="77.962659375" y1="25.739" x2="78.76481875" y2="25.739" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="25.739" x2="88.41505" y2="25.739" width="0.225" layer="94"/>
<wire x1="79.202409375" y1="25.739" x2="79.510359375" y2="25.739" width="0.225" layer="94"/>
<wire x1="86.810090625" y1="25.739" x2="87.1462" y2="25.739" width="0.225" layer="94"/>
<wire x1="79.94166875" y1="25.739" x2="80.78728125" y2="25.739" width="0.225" layer="94"/>
<wire x1="85.531009375" y1="25.739" x2="86.39005" y2="25.739" width="0.225" layer="94"/>
<wire x1="81.209909375" y1="25.739" x2="81.340009375" y2="25.739" width="0.225" layer="94"/>
<wire x1="84.77363125" y1="25.739" x2="85.092790625" y2="25.739" width="0.225" layer="94"/>
<wire x1="81.740890625" y1="25.739" x2="82.508140625" y2="25.739" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="25.739" x2="84.336459375" y2="25.739" width="0.225" layer="94"/>
<wire x1="64.9221" y1="25.514" x2="73.542859375" y2="25.514" width="0.225" layer="94"/>
<wire x1="92.74113125" y1="25.514" x2="101.4471" y2="25.514" width="0.225" layer="94"/>
<wire x1="74.62216875" y1="25.514" x2="75.49116875" y2="25.514" width="0.225" layer="94"/>
<wire x1="90.78793125" y1="25.514" x2="91.6325" y2="25.514" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="25.514" x2="76.82785" y2="25.514" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="25.514" x2="89.74463125" y2="25.514" width="0.225" layer="94"/>
<wire x1="77.90715" y1="25.514" x2="78.834309375" y2="25.514" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="25.514" x2="88.41505" y2="25.514" width="0.225" layer="94"/>
<wire x1="79.877040625" y1="25.514" x2="80.82388125" y2="25.514" width="0.225" layer="94"/>
<wire x1="86.810090625" y1="25.514" x2="87.1462" y2="25.514" width="0.225" layer="94"/>
<wire x1="81.697240625" y1="25.514" x2="82.508140625" y2="25.514" width="0.225" layer="94"/>
<wire x1="85.46248125" y1="25.514" x2="86.39005" y2="25.514" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="25.514" x2="84.40543125" y2="25.514" width="0.225" layer="94"/>
<wire x1="64.9221" y1="25.289" x2="73.828090625" y2="25.289" width="0.225" layer="94"/>
<wire x1="92.48735" y1="25.289" x2="101.4471" y2="25.289" width="0.225" layer="94"/>
<wire x1="74.3331" y1="25.289" x2="75.49116875" y2="25.289" width="0.225" layer="94"/>
<wire x1="90.51153125" y1="25.289" x2="91.85098125" y2="25.289" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="25.289" x2="77.11308125" y2="25.289" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="25.289" x2="90.02831875" y2="25.289" width="0.225" layer="94"/>
<wire x1="77.618090625" y1="25.289" x2="79.119840625" y2="25.289" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="25.289" x2="88.41505" y2="25.289" width="0.225" layer="94"/>
<wire x1="79.60065" y1="25.289" x2="81.04181875" y2="25.289" width="0.225" layer="94"/>
<wire x1="86.810090625" y1="25.289" x2="87.1462" y2="25.289" width="0.225" layer="94"/>
<wire x1="81.460959375" y1="25.289" x2="82.508140625" y2="25.289" width="0.225" layer="94"/>
<wire x1="85.1789" y1="25.289" x2="86.39005" y2="25.289" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="25.289" x2="84.689659375" y2="25.289" width="0.225" layer="94"/>
<wire x1="64.9221" y1="25.064" x2="101.4471" y2="25.064" width="0.225" layer="94"/>
<wire x1="64.9221" y1="24.839" x2="101.4471" y2="24.839" width="0.225" layer="94"/>
<wire x1="64.9221" y1="24.614" x2="101.4471" y2="24.614" width="0.225" layer="94"/>
<wire x1="64.9221" y1="24.389" x2="101.4471" y2="24.389" width="0.225" layer="94"/>
<wire x1="64.9221" y1="24.164" x2="101.4471" y2="24.164" width="0.225" layer="94"/>
<wire x1="64.9221" y1="23.939" x2="101.4471" y2="23.939" width="0.225" layer="94"/>
<wire x1="64.9221" y1="23.714" x2="101.4471" y2="23.714" width="0.225" layer="94"/>
<wire x1="64.9221" y1="23.489" x2="101.4471" y2="23.489" width="0.225" layer="94"/>
<wire x1="64.9221" y1="23.264" x2="101.4471" y2="23.264" width="0.225" layer="94"/>
<rectangle x1="93.287040625" y1="29.45076875" x2="93.399540625" y2="33.34871875" layer="94"/>
<rectangle x1="94.749840625" y1="29.45076875" x2="94.862340625" y2="33.34871875" layer="94"/>
<rectangle x1="73.776721875" y1="26.310290625" x2="74.376859375" y2="26.422790625" layer="94"/>
<rectangle x1="77.0617" y1="26.310290625" x2="77.66185" y2="26.422790625" layer="94"/>
<rectangle x1="73.77671875" y1="26.00881875" x2="74.685" y2="26.12131875" layer="94"/>
<rectangle x1="77.0617" y1="26.00881875" x2="77.985" y2="26.12131875" layer="94"/>
<rectangle x1="81.09369375" y1="26.91936875" x2="81.7013" y2="27.314" layer="94"/>
<rectangle x1="80.627059375" y1="26.91936875" x2="80.898640625" y2="27.314" layer="94"/>
<rectangle x1="80.627059375" y1="26.319371875" x2="80.898640625" y2="26.739490625" layer="94"/>
<rectangle x1="81.09369375" y1="26.319371875" x2="81.7013" y2="26.739490625" layer="94"/>
<rectangle x1="64.77" y1="37.7825" x2="101.6" y2="38.1" layer="94"/>
<rectangle x1="101.2825" y1="22.86" x2="101.6" y2="38.1" layer="94"/>
<rectangle x1="64.77" y1="22.86" x2="65.0875" y2="38.1" layer="94"/>
<rectangle x1="64.77" y1="22.86" x2="101.6" y2="23.1775" layer="94"/>
<text x="50.8" y="12.7" size="5.08" layer="94" ratio="10" align="bottom-center">&gt;VALUE</text>
<text x="1.27" y="33.02" size="1.9304" layer="94">Vehicle Name:</text>
<text x="41.91" y="30.48" size="5.08" layer="94" align="top-center">&gt;VEHICLE_NUMBER</text>
<text x="41.91" y="33.02" size="2.54" layer="94" align="bottom-center">&gt;VEHICLE_NAME</text>
<text x="1.27" y="21.59" size="1.9304" layer="94" align="top-left">Sheet Name:</text>
<text x="1.27" y="24.13" size="1.9304" layer="94">ABRA:</text>
<text x="10.16" y="24.13" size="1.9304" layer="94">&gt;ABRA_GLOBAL</text>
</symbol>
<symbol name="DOCFIELD-AUTA-ECO-LOGO">
<wire x1="0" y1="0" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="101.6" y1="10.16" x2="87.63" y2="10.16" width="0.1016" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="71.12" y2="5.08" width="0.1016" layer="94"/>
<wire x1="0" y1="5.08" x2="0" y2="10.16" width="0.1016" layer="94"/>
<wire x1="101.6" y1="10.16" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="71.12" y2="0" width="0.1016" layer="94"/>
<wire x1="71.12" y1="5.08" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="71.12" y1="0" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="87.63" y1="10.16" x2="87.63" y2="5.08" width="0.1016" layer="94"/>
<wire x1="87.63" y1="10.16" x2="0" y2="10.16" width="0.1016" layer="94"/>
<wire x1="87.63" y1="5.08" x2="101.6" y2="5.08" width="0.1016" layer="94"/>
<wire x1="101.6" y1="5.08" x2="101.6" y2="0" width="0.1016" layer="94"/>
<wire x1="0" y1="10.16" x2="0" y2="22.86" width="0.1016" layer="94"/>
<wire x1="101.6" y1="38.1" x2="64.77" y2="38.1" width="0.1016" layer="94"/>
<wire x1="64.77" y1="38.1" x2="0" y2="38.1" width="0.1016" layer="94"/>
<wire x1="101.6" y1="38.1" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="64.77" y2="22.86" width="0.1016" layer="94"/>
<wire x1="64.77" y1="22.86" x2="101.6" y2="22.86" width="0.1016" layer="94"/>
<wire x1="0" y1="22.86" x2="0" y2="38.1" width="0.1016" layer="94"/>
<wire x1="101.6" y1="22.86" x2="101.6" y2="10.16" width="0.1016" layer="94"/>
<text x="1.27" y="1.27" size="2.54" layer="94">Date:</text>
<text x="15.24" y="1.27" size="2.54" layer="94" font="vector">&gt;LAST_DATE_TIME</text>
<text x="72.39" y="1.27" size="2.54" layer="94">Sheet:</text>
<text x="86.36" y="1.27" size="2.54" layer="94">&gt;SHEET</text>
<text x="88.9" y="6.35" size="2.54" layer="94">REV:</text>
<text x="1.27" y="6.35" size="2.54" layer="94">TITLE:</text>
<text x="1.27" y="30.48" size="1.9304" layer="94" align="top-left">Vehicle Number:</text>
<text x="15.24" y="6.35" size="2.54" layer="94" font="vector">&gt;DRAWING_NAME</text>
<wire x1="64.77" y1="38.1" x2="64.77" y2="22.86" width="0.1016" layer="94"/>
<wire x1="97.487340625" y1="34.81106875" x2="90.66211875" y2="34.81106875" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="34.81106875" x2="90.66211875" y2="34.739" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="34.739" x2="90.66211875" y2="34.514" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="34.514" x2="90.66211875" y2="34.289" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="34.289" x2="90.66211875" y2="34.064" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="34.064" x2="90.66211875" y2="33.839" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="33.839" x2="90.66211875" y2="33.614" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="33.614" x2="90.66211875" y2="33.389" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="33.389" x2="90.66211875" y2="33.23621875" width="0.225" layer="94"/>
<wire x1="90.66211875" y1="33.23621875" x2="93.287040625" y2="33.23621875" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="33.23621875" x2="93.287040625" y2="33.164" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="33.164" x2="93.287040625" y2="32.939" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="32.939" x2="93.287040625" y2="32.714" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="32.714" x2="93.287040625" y2="32.489" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="32.489" x2="93.287040625" y2="32.264" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="32.264" x2="93.287040625" y2="32.039" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="32.039" x2="93.287040625" y2="31.814" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="31.814" x2="93.287040625" y2="31.589" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="31.589" x2="93.287040625" y2="31.364" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="31.364" x2="93.287040625" y2="31.139" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="31.139" x2="93.287040625" y2="30.914" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="30.914" x2="93.287040625" y2="30.689" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="30.689" x2="93.287040625" y2="30.464" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="30.464" x2="93.287040625" y2="30.239" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="30.239" x2="93.287040625" y2="30.014" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="30.014" x2="93.287040625" y2="29.789" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="29.789" x2="93.287040625" y2="29.564" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="29.564" x2="93.287040625" y2="29.45076875" width="0.225" layer="94"/>
<wire x1="93.287040625" y1="29.45076875" x2="94.862340625" y2="29.45076875" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="29.45076875" x2="94.862340625" y2="29.564" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="29.564" x2="94.862340625" y2="29.789" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="29.789" x2="94.862340625" y2="30.014" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="30.014" x2="94.862340625" y2="30.239" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="30.239" x2="94.862340625" y2="30.464" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="30.464" x2="94.862340625" y2="30.689" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="30.689" x2="94.862340625" y2="30.914" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="30.914" x2="94.862340625" y2="31.139" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="31.139" x2="94.862340625" y2="31.364" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="31.364" x2="94.862340625" y2="31.589" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="31.589" x2="94.862340625" y2="31.814" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="31.814" x2="94.862340625" y2="32.039" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="32.039" x2="94.862340625" y2="32.264" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="32.264" x2="94.862340625" y2="32.489" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="32.489" x2="94.862340625" y2="32.714" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="32.714" x2="94.862340625" y2="32.939" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="32.939" x2="94.862340625" y2="33.164" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="33.164" x2="94.862340625" y2="33.23621875" width="0.225" layer="94"/>
<wire x1="94.862340625" y1="33.23621875" x2="97.487340625" y2="33.23621875" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="33.23621875" x2="97.487340625" y2="33.389" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="33.389" x2="97.487340625" y2="33.614" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="33.614" x2="97.487340625" y2="33.839" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="33.839" x2="97.487340625" y2="34.064" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="34.064" x2="97.487340625" y2="34.289" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="34.289" x2="97.487340625" y2="34.514" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="34.514" x2="97.487340625" y2="34.739" width="0.225" layer="94"/>
<wire x1="97.487340625" y1="34.739" x2="97.487340625" y2="34.81106875" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="29.497059375" x2="87.068640625" y2="29.564" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="29.564" x2="87.068640625" y2="29.789" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="29.789" x2="87.068640625" y2="30.014" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="30.014" x2="87.068640625" y2="30.239" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="30.239" x2="87.068640625" y2="30.464" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="30.464" x2="87.068640625" y2="30.689" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="30.689" x2="87.068640625" y2="30.914" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="30.914" x2="87.068640625" y2="31.139" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="31.139" x2="87.068640625" y2="31.364" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="31.364" x2="87.068640625" y2="31.589" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="31.589" x2="87.068640625" y2="31.814" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="31.814" x2="87.068640625" y2="32.039" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="32.039" x2="87.068640625" y2="32.264" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="32.264" x2="87.068640625" y2="32.489" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="32.489" x2="87.068640625" y2="32.714" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="32.714" x2="87.068640625" y2="32.939" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="32.939" x2="87.068640625" y2="33.164" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="33.164" x2="87.068640625" y2="33.389" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="33.389" x2="87.068640625" y2="33.614" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="33.614" x2="87.068640625" y2="33.839" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="33.839" x2="87.068640625" y2="34.064" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="34.064" x2="87.068640625" y2="34.289" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="34.289" x2="87.068640625" y2="34.514" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="34.514" x2="87.068640625" y2="34.739" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="34.739" x2="87.068640625" y2="34.813409375" width="0.225" layer="94"/>
<wire x1="87.068640625" y1="34.813409375" x2="88.64378125" y2="34.813409375" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="34.813409375" x2="88.64378125" y2="34.739" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="34.739" x2="88.64378125" y2="34.514" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="34.514" x2="88.64378125" y2="34.289" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="34.289" x2="88.64378125" y2="34.064" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="34.064" x2="88.64378125" y2="33.839" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="33.839" x2="88.64378125" y2="33.614" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="33.614" x2="88.64378125" y2="33.389" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="33.389" x2="88.64378125" y2="33.164" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="33.164" x2="88.64378125" y2="32.939" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="32.939" x2="88.64378125" y2="32.714" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="32.714" x2="88.64378125" y2="32.489" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="32.489" x2="88.64378125" y2="32.264" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="32.264" x2="88.64378125" y2="32.039" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="32.039" x2="88.64378125" y2="31.814" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="31.814" x2="88.64378125" y2="31.589" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="31.589" x2="88.64378125" y2="31.364" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="31.364" x2="88.64378125" y2="31.139" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="31.139" x2="88.64378125" y2="30.914" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="30.914" x2="88.64378125" y2="30.689" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="30.689" x2="88.64378125" y2="30.464" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="30.464" x2="88.64378125" y2="30.239" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="30.239" x2="88.64378125" y2="30.014" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="30.014" x2="88.64378125" y2="29.789" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="29.789" x2="88.64378125" y2="29.564" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="29.564" x2="88.64378125" y2="29.497059375" width="0.225" layer="94"/>
<wire x1="88.64378125" y1="29.497059375" x2="87.068640625" y2="29.497059375" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="29.49851875" x2="78.286090625" y2="29.564" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="29.564" x2="78.286090625" y2="29.789" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="29.789" x2="78.286090625" y2="30.014" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="30.014" x2="78.286090625" y2="30.239" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="30.239" x2="78.286090625" y2="30.464" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="30.464" x2="78.286090625" y2="30.689" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="30.689" x2="78.286090625" y2="30.914" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="30.914" x2="78.286090625" y2="31.07366875" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="31.07366875" x2="84.81116875" y2="31.07366875" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="31.07366875" x2="84.81116875" y2="30.914" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="30.914" x2="84.81116875" y2="30.689" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="30.689" x2="84.81116875" y2="30.464" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="30.464" x2="84.81116875" y2="30.239" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="30.239" x2="84.81116875" y2="30.014" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="30.014" x2="84.81116875" y2="29.789" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="29.789" x2="84.81116875" y2="29.564" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="29.564" x2="84.81116875" y2="29.49851875" width="0.225" layer="94"/>
<wire x1="84.81116875" y1="29.49851875" x2="78.286090625" y2="29.49851875" width="0.225" layer="94"/>
<wire x1="78.286240625" y1="31.44236875" x2="78.286240625" y2="31.589" width="0.225" layer="94"/>
<wire x1="78.286240625" y1="31.589" x2="78.286240625" y2="31.814" width="0.225" layer="94"/>
<wire x1="78.286240625" y1="31.814" x2="78.286240625" y2="32.039" width="0.225" layer="94"/>
<wire x1="78.286240625" y1="32.039" x2="78.286240625" y2="32.264" width="0.225" layer="94"/>
<wire x1="78.286240625" y1="32.264" x2="78.286240625" y2="32.489" width="0.225" layer="94"/>
<wire x1="78.286240625" y1="32.489" x2="78.286240625" y2="32.714" width="0.225" layer="94"/>
<wire x1="78.286240625" y1="32.714" x2="78.286240625" y2="32.86721875" width="0.225" layer="94"/>
<wire x1="78.286240625" y1="32.86721875" x2="81.96101875" y2="32.86721875" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="32.86721875" x2="81.96101875" y2="32.714" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="32.714" x2="81.96101875" y2="32.489" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="32.489" x2="81.96101875" y2="32.264" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="32.264" x2="81.96101875" y2="32.039" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="32.039" x2="81.96101875" y2="31.814" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="31.814" x2="81.96101875" y2="31.589" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="31.589" x2="81.96101875" y2="31.44236875" width="0.225" layer="94"/>
<wire x1="81.96101875" y1="31.44236875" x2="78.286240625" y2="31.44236875" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="33.23621875" x2="78.286090625" y2="33.389" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="33.389" x2="78.286090625" y2="33.614" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="33.614" x2="78.286090625" y2="33.839" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="33.839" x2="78.286090625" y2="34.064" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="34.064" x2="78.286090625" y2="34.289" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="34.289" x2="78.286090625" y2="34.514" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="34.514" x2="78.286090625" y2="34.739" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="34.739" x2="78.286090625" y2="34.81106875" width="0.225" layer="94"/>
<wire x1="78.286090625" y1="34.81106875" x2="84.81101875" y2="34.81106875" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="34.81106875" x2="84.81101875" y2="34.739" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="34.739" x2="84.81101875" y2="34.514" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="34.514" x2="84.81101875" y2="34.289" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="34.289" x2="84.81101875" y2="34.064" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="34.064" x2="84.81101875" y2="33.839" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="33.839" x2="84.81101875" y2="33.614" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="33.614" x2="84.81101875" y2="33.389" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="33.389" x2="84.81101875" y2="33.23621875" width="0.225" layer="94"/>
<wire x1="84.81101875" y1="33.23621875" x2="78.286090625" y2="33.23621875" width="0.225" layer="94"/>
<wire x1="68.6921" y1="33.944190625" x2="69.073209375" y2="34.289" width="0.225" layer="94"/>
<wire x1="69.073209375" y1="34.289" x2="69.3219" y2="34.514" width="0.225" layer="94"/>
<wire x1="69.3219" y1="34.514" x2="69.570590625" y2="34.739" width="0.225" layer="94"/>
<wire x1="69.570590625" y1="34.739" x2="69.86021875" y2="35.001040625" width="0.225" layer="94"/>
<wire x1="85.115190625" y1="26.34016875" x2="85.115190625" y2="26.189" width="0.225" layer="94"/>
<wire x1="85.115190625" y1="26.189" x2="85.115190625" y2="25.97718125" width="0.225" layer="94"/>
<wire x1="84.32008125" y1="25.980109375" x2="84.341215625" y2="25.70643125" width="0.225" layer="94" curve="8.832242"/>
<wire x1="84.341209375" y1="25.70643125" x2="84.4424375" y2="25.45343125" width="0.225" layer="94" curve="25.94924"/>
<wire x1="84.442440625" y1="25.45343125" x2="84.662590625" y2="25.2962625" width="0.225" layer="94" curve="39.389578"/>
<wire x1="84.662590625" y1="25.296259375" x2="84.93363125" y2="25.258525" width="0.225" layer="94" curve="15.805998"/>
<wire x1="85.52626875" y1="25.706240625" x2="85.547246875" y2="25.980109375" width="0.225" layer="94" curve="8.760059"/>
<wire x1="85.42531875" y1="25.45295" x2="85.46248125" y2="25.514" width="0.225" layer="94" curve="6.74589"/>
<wire x1="85.46248125" y1="25.514" x2="85.526271875" y2="25.706240625" width="0.225" layer="94" curve="19.196299"/>
<wire x1="85.204909375" y1="25.295909375" x2="85.425315625" y2="25.452953125" width="0.225" layer="94" curve="39.656265"/>
<wire x1="84.93363125" y1="25.25853125" x2="85.204909375" y2="25.295909375" width="0.225" layer="94" curve="15.593829"/>
<wire x1="84.32008125" y1="25.980109375" x2="84.32008125" y2="26.189" width="0.225" layer="94"/>
<wire x1="84.32008125" y1="26.189" x2="84.32008125" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="84.341340625" y1="26.61343125" x2="84.32161875" y2="26.414" width="0.225" layer="94" curve="6.503067"/>
<wire x1="84.32161875" y1="26.414" x2="84.320078125" y2="26.34016875" width="0.225" layer="94" curve="2.395218"/>
<wire x1="84.44311875" y1="26.86573125" x2="84.341340625" y2="26.61343125" width="0.225" layer="94" curve="26.142719"/>
<wire x1="84.663740625" y1="27.02155" x2="84.443121875" y2="26.865728125" width="0.225" layer="94" curve="39.450584"/>
<wire x1="84.93363125" y1="27.05881875" x2="84.663740625" y2="27.02155" width="0.225" layer="94" curve="15.292594"/>
<wire x1="85.54725" y1="25.980109375" x2="85.54725" y2="26.189" width="0.225" layer="94"/>
<wire x1="85.54725" y1="26.189" x2="85.54725" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="85.54725" y1="26.34016875" x2="85.52646875" y2="26.61348125" width="0.225" layer="94" curve="8.69613"/>
<wire x1="85.52646875" y1="26.61348125" x2="85.4257" y2="26.8662" width="0.225" layer="94" curve="26.08534"/>
<wire x1="85.4257" y1="26.8662" x2="85.2053" y2="27.02220625" width="0.225" layer="94" curve="39.852867"/>
<wire x1="85.2053" y1="27.022209375" x2="84.93363125" y2="27.058825" width="0.225" layer="94" curve="15.379893"/>
<wire x1="84.752059375" y1="26.34016875" x2="84.752059375" y2="26.189" width="0.225" layer="94"/>
<wire x1="84.752059375" y1="26.189" x2="84.752059375" y2="25.97718125" width="0.225" layer="94"/>
<wire x1="85.115190625" y1="25.97718125" x2="85.11286875" y2="25.87078125" width="0.225" layer="94"/>
<wire x1="85.11286875" y1="25.87078125" x2="85.10523125" y2="25.791359375" width="0.225" layer="94"/>
<wire x1="85.10523125" y1="25.791359375" x2="85.100290625" y2="25.765209375" width="0.225" layer="94"/>
<wire x1="85.100290625" y1="25.765209375" x2="85.09306875" y2="25.73961875" width="0.225" layer="94"/>
<wire x1="85.09306875" y1="25.73961875" x2="85.081940625" y2="25.715509375" width="0.225" layer="94"/>
<wire x1="85.081940625" y1="25.715509375" x2="85.06321875" y2="25.696990625" width="0.225" layer="94"/>
<wire x1="85.06321875" y1="25.696990625" x2="85.0387" y2="25.68683125" width="0.225" layer="94"/>
<wire x1="85.0387" y1="25.68683125" x2="85.012740625" y2="25.68106875" width="0.225" layer="94"/>
<wire x1="85.012740625" y1="25.68106875" x2="84.986340625" y2="25.6778" width="0.225" layer="94"/>
<wire x1="84.986340625" y1="25.6778" x2="84.95978125" y2="25.67618125" width="0.225" layer="94"/>
<wire x1="84.752059375" y1="26.34016875" x2="84.75605" y2="26.47246875" width="0.225" layer="94"/>
<wire x1="84.75605" y1="26.47246875" x2="84.76195" y2="26.52508125" width="0.225" layer="94"/>
<wire x1="84.76195" y1="26.52508125" x2="84.77411875" y2="26.57655" width="0.225" layer="94"/>
<wire x1="84.77411875" y1="26.57655" x2="84.78536875" y2="26.600440625" width="0.225" layer="94"/>
<wire x1="84.78536875" y1="26.600440625" x2="84.804459375" y2="26.618359375" width="0.225" layer="94"/>
<wire x1="84.804459375" y1="26.618359375" x2="84.82903125" y2="26.62806875" width="0.225" layer="94"/>
<wire x1="84.82903125" y1="26.62806875" x2="84.854909375" y2="26.63358125" width="0.225" layer="94"/>
<wire x1="84.854909375" y1="26.63358125" x2="84.8812" y2="26.6367" width="0.225" layer="94"/>
<wire x1="84.8812" y1="26.6367" x2="84.90763125" y2="26.63826875" width="0.225" layer="94"/>
<wire x1="84.90763125" y1="26.63826875" x2="84.96056875" y2="26.63825" width="0.225" layer="94"/>
<wire x1="84.96056875" y1="26.63825" x2="84.987" y2="26.63666875" width="0.225" layer="94"/>
<wire x1="84.987" y1="26.63666875" x2="85.013290625" y2="26.63358125" width="0.225" layer="94"/>
<wire x1="85.013290625" y1="26.63358125" x2="85.039190625" y2="26.628140625" width="0.225" layer="94"/>
<wire x1="85.039190625" y1="26.628140625" x2="85.063809375" y2="26.618559375" width="0.225" layer="94"/>
<wire x1="85.063809375" y1="26.618559375" x2="85.0829" y2="26.60066875" width="0.225" layer="94"/>
<wire x1="85.0829" y1="26.60066875" x2="85.093940625" y2="26.57666875" width="0.225" layer="94"/>
<wire x1="85.093940625" y1="26.57666875" x2="85.105759375" y2="26.52511875" width="0.225" layer="94"/>
<wire x1="85.105759375" y1="26.52511875" x2="85.111390625" y2="26.47248125" width="0.225" layer="94"/>
<wire x1="85.111390625" y1="26.47248125" x2="85.11306875" y2="26.414" width="0.225" layer="94"/>
<wire x1="85.11306875" y1="26.414" x2="85.115190625" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="84.752059375" y1="25.97718125" x2="84.754290625" y2="25.87078125" width="0.225" layer="94"/>
<wire x1="84.754290625" y1="25.87078125" x2="84.76156875" y2="25.79131875" width="0.225" layer="94"/>
<wire x1="84.76156875" y1="25.79131875" x2="84.7664" y2="25.76515" width="0.225" layer="94"/>
<wire x1="84.7664" y1="25.76515" x2="84.773409375" y2="25.7395" width="0.225" layer="94"/>
<wire x1="84.773409375" y1="25.7395" x2="84.784340625" y2="25.7153" width="0.225" layer="94"/>
<wire x1="84.784340625" y1="25.7153" x2="84.803059375" y2="25.6968" width="0.225" layer="94"/>
<wire x1="84.803059375" y1="25.6968" x2="84.82763125" y2="25.686759375" width="0.225" layer="94"/>
<wire x1="84.82763125" y1="25.686759375" x2="84.853609375" y2="25.681059375" width="0.225" layer="94"/>
<wire x1="84.853609375" y1="25.681059375" x2="84.90656875" y2="25.6762" width="0.225" layer="94"/>
<wire x1="84.90656875" y1="25.6762" x2="84.95978125" y2="25.6762" width="0.225" layer="94"/>
<wire x1="88.41505" y1="27.03186875" x2="88.835090625" y2="27.03186875" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="27.03186875" x2="88.835090625" y2="26.864" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="26.864" x2="88.835090625" y2="26.639" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="26.639" x2="88.835090625" y2="26.414" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="26.414" x2="88.835090625" y2="26.189" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="26.189" x2="88.835090625" y2="25.964" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="25.964" x2="88.835090625" y2="25.739" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="25.739" x2="88.835090625" y2="25.514" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="25.514" x2="88.835090625" y2="25.289" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="25.289" x2="88.835090625" y2="25.28548125" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="25.28548125" x2="88.41505" y2="25.28548125" width="0.225" layer="94"/>
<wire x1="88.41505" y1="25.28548125" x2="88.41505" y2="25.289" width="0.225" layer="94"/>
<wire x1="88.41505" y1="25.289" x2="88.41505" y2="25.514" width="0.225" layer="94"/>
<wire x1="88.41505" y1="25.514" x2="88.41505" y2="25.739" width="0.225" layer="94"/>
<wire x1="88.41505" y1="25.739" x2="88.41505" y2="25.964" width="0.225" layer="94"/>
<wire x1="88.41505" y1="25.964" x2="88.41505" y2="26.189" width="0.225" layer="94"/>
<wire x1="88.41505" y1="26.189" x2="88.41505" y2="26.414" width="0.225" layer="94"/>
<wire x1="88.41505" y1="26.414" x2="88.41505" y2="26.639" width="0.225" layer="94"/>
<wire x1="88.41505" y1="26.639" x2="88.41505" y2="26.864" width="0.225" layer="94"/>
<wire x1="88.41505" y1="26.864" x2="88.41505" y2="27.03186875" width="0.225" layer="94"/>
<wire x1="88.41505" y1="27.205890625" x2="88.835090625" y2="27.205890625" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="27.205890625" x2="88.835090625" y2="27.314" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="27.314" x2="88.835090625" y2="27.539" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="27.539" x2="88.835090625" y2="27.68606875" width="0.225" layer="94"/>
<wire x1="88.835090625" y1="27.68606875" x2="88.41505" y2="27.68606875" width="0.225" layer="94"/>
<wire x1="88.41505" y1="27.68606875" x2="88.41505" y2="27.539" width="0.225" layer="94"/>
<wire x1="88.41505" y1="27.539" x2="88.41505" y2="27.314" width="0.225" layer="94"/>
<wire x1="88.41505" y1="27.314" x2="88.41505" y2="27.205890625" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="27.725040625" x2="75.911209375" y2="27.725040625" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="27.725040625" x2="75.911209375" y2="27.539" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="27.539" x2="75.911209375" y2="27.314" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="27.314" x2="75.911209375" y2="27.089" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="27.089" x2="75.911209375" y2="26.864" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="26.864" x2="75.911209375" y2="26.639" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="26.639" x2="75.911209375" y2="26.414" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="26.414" x2="75.911209375" y2="26.189" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="26.189" x2="75.911209375" y2="25.964" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="25.964" x2="75.911209375" y2="25.739" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="25.739" x2="75.911209375" y2="25.514" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="25.514" x2="75.911209375" y2="25.289" width="0.225" layer="94"/>
<wire x1="75.911209375" y1="25.28548125" x2="75.49116875" y2="25.28548125" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="25.28548125" x2="75.49116875" y2="25.289" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="25.289" x2="75.49116875" y2="25.514" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="25.514" x2="75.49116875" y2="25.739" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="25.739" x2="75.49116875" y2="25.964" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="25.964" x2="75.49116875" y2="26.189" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="26.189" x2="75.49116875" y2="26.414" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="26.414" x2="75.49116875" y2="26.639" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="26.639" x2="75.49116875" y2="26.864" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="26.864" x2="75.49116875" y2="27.089" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="27.089" x2="75.49116875" y2="27.314" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="27.314" x2="75.49116875" y2="27.539" width="0.225" layer="94"/>
<wire x1="75.49116875" y1="27.539" x2="75.49116875" y2="27.725040625" width="0.225" layer="94"/>
<wire x1="90.092" y1="25.97718125" x2="90.092" y2="26.189" width="0.225" layer="94"/>
<wire x1="90.092" y1="26.189" x2="90.092" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="90.41016875" y1="26.44828125" x2="90.41016875" y2="26.414" width="0.225" layer="94"/>
<wire x1="90.41016875" y1="26.414" x2="90.41016875" y2="26.30881875" width="0.225" layer="94"/>
<wire x1="90.41016875" y1="26.30881875" x2="90.830209375" y2="26.30881875" width="0.225" layer="94"/>
<wire x1="90.43711875" y1="26.071809375" x2="90.43711875" y2="25.964" width="0.225" layer="94"/>
<wire x1="90.43711875" y1="25.964" x2="90.43711875" y2="25.89925" width="0.225" layer="94"/>
<wire x1="90.43711875" y1="26.071809375" x2="90.86316875" y2="26.071809375" width="0.225" layer="94"/>
<wire x1="90.092" y1="26.34016875" x2="90.09408125" y2="26.45038125" width="0.225" layer="94"/>
<wire x1="90.09408125" y1="26.45038125" x2="90.09926875" y2="26.5163" width="0.225" layer="94"/>
<wire x1="90.09926875" y1="26.5163" x2="90.106540625" y2="26.55976875" width="0.225" layer="94"/>
<wire x1="90.106540625" y1="26.55976875" x2="90.112390625" y2="26.58101875" width="0.225" layer="94"/>
<wire x1="90.112390625" y1="26.58101875" x2="90.12096875" y2="26.6013" width="0.225" layer="94"/>
<wire x1="90.12096875" y1="26.6013" x2="90.135190625" y2="26.617890625" width="0.225" layer="94"/>
<wire x1="90.135190625" y1="26.617890625" x2="90.1551" y2="26.6272" width="0.225" layer="94"/>
<wire x1="90.1551" y1="26.6272" x2="90.17645" y2="26.63263125" width="0.225" layer="94"/>
<wire x1="90.17645" y1="26.63263125" x2="90.19825" y2="26.6359" width="0.225" layer="94"/>
<wire x1="90.19825" y1="26.6359" x2="90.220209375" y2="26.637740625" width="0.225" layer="94"/>
<wire x1="90.220209375" y1="26.637740625" x2="90.242240625" y2="26.63858125" width="0.225" layer="94"/>
<wire x1="90.242240625" y1="26.63858125" x2="90.264290625" y2="26.638640625" width="0.225" layer="94"/>
<wire x1="90.264290625" y1="26.638640625" x2="90.28631875" y2="26.6378" width="0.225" layer="94"/>
<wire x1="90.28631875" y1="26.6378" x2="90.30826875" y2="26.63575" width="0.225" layer="94"/>
<wire x1="90.30826875" y1="26.63575" x2="90.33" y2="26.63208125" width="0.225" layer="94"/>
<wire x1="90.33" y1="26.63208125" x2="90.351140625" y2="26.625909375" width="0.225" layer="94"/>
<wire x1="90.351140625" y1="26.625909375" x2="90.3705" y2="26.615509375" width="0.225" layer="94"/>
<wire x1="90.3705" y1="26.615509375" x2="90.38486875" y2="26.598990625" width="0.225" layer="94"/>
<wire x1="90.38486875" y1="26.598990625" x2="90.40023125" y2="26.557840625" width="0.225" layer="94"/>
<wire x1="90.40023125" y1="26.557840625" x2="90.407140625" y2="26.51431875" width="0.225" layer="94"/>
<wire x1="90.407140625" y1="26.51431875" x2="90.41016875" y2="26.44828125" width="0.225" layer="94"/>
<wire x1="90.092" y1="25.97718125" x2="90.09478125" y2="25.85786875" width="0.225" layer="94"/>
<wire x1="90.09478125" y1="25.85786875" x2="90.09865" y2="25.810290625" width="0.225" layer="94"/>
<wire x1="90.09865" y1="25.810290625" x2="90.11223125" y2="25.74008125" width="0.225" layer="94"/>
<wire x1="90.11223125" y1="25.74008125" x2="90.121240625" y2="25.718009375" width="0.225" layer="94"/>
<wire x1="90.121240625" y1="25.718009375" x2="90.136040625" y2="25.6995" width="0.225" layer="94"/>
<wire x1="90.136040625" y1="25.6995" x2="90.15721875" y2="25.688690625" width="0.225" layer="94"/>
<wire x1="90.15721875" y1="25.688690625" x2="90.18025" y2="25.68248125" width="0.225" layer="94"/>
<wire x1="90.18025" y1="25.68248125" x2="90.20383125" y2="25.678809375" width="0.225" layer="94"/>
<wire x1="90.20383125" y1="25.678809375" x2="90.251459375" y2="25.675840625" width="0.225" layer="94"/>
<wire x1="90.251459375" y1="25.675840625" x2="90.299190625" y2="25.67656875" width="0.225" layer="94"/>
<wire x1="90.299190625" y1="25.67656875" x2="90.34663125" y2="25.681690625" width="0.225" layer="94"/>
<wire x1="90.34663125" y1="25.681690625" x2="90.391890625" y2="25.696190625" width="0.225" layer="94"/>
<wire x1="90.391890625" y1="25.696190625" x2="90.409190625" y2="25.71226875" width="0.225" layer="94"/>
<wire x1="90.409190625" y1="25.71226875" x2="90.419459375" y2="25.733759375" width="0.225" layer="94"/>
<wire x1="90.419459375" y1="25.733759375" x2="90.425940625" y2="25.75671875" width="0.225" layer="94"/>
<wire x1="90.425940625" y1="25.75671875" x2="90.4332" y2="25.80388125" width="0.225" layer="94"/>
<wire x1="90.4332" y1="25.80388125" x2="90.436309375" y2="25.85151875" width="0.225" layer="94"/>
<wire x1="90.436309375" y1="25.85151875" x2="90.43711875" y2="25.89925" width="0.225" layer="94"/>
<wire x1="89.66001875" y1="25.980109375" x2="89.67878125" y2="25.718659375" width="0.225" layer="94" curve="8.208716"/>
<wire x1="89.67878125" y1="25.718659375" x2="89.74463125" y2="25.514" width="0.225" layer="94" curve="19.254744"/>
<wire x1="89.74463125" y1="25.514" x2="89.76749375" y2="25.47364375" width="0.225" layer="94" curve="4.135389"/>
<wire x1="89.767490625" y1="25.473640625" x2="89.966896875" y2="25.309234375" width="0.225" layer="94" curve="37.791863"/>
<wire x1="89.9669" y1="25.309240625" x2="90.02831875" y2="25.289" width="0.225" layer="94" curve="4.744864"/>
<wire x1="90.02831875" y1="25.289" x2="90.22315" y2="25.259475" width="0.225" layer="94" curve="14.492988"/>
<wire x1="90.86316875" y1="26.071809375" x2="90.86316875" y2="25.964" width="0.225" layer="94"/>
<wire x1="90.86316875" y1="25.964" x2="90.86316875" y2="25.89603125" width="0.225" layer="94"/>
<wire x1="90.83383125" y1="25.63598125" x2="90.852559375" y2="25.739" width="0.225" layer="94" curve="5.141671"/>
<wire x1="90.852559375" y1="25.739" x2="90.86316875" y2="25.89603125" width="0.225" layer="94" curve="7.731903"/>
<wire x1="90.710259375" y1="25.40781875" x2="90.83383125" y2="25.63598125" width="0.225" layer="94" curve="31.131749"/>
<wire x1="90.483690625" y1="25.282109375" x2="90.710259375" y2="25.40781875" width="0.225" layer="94" curve="33.941784"/>
<wire x1="90.22315" y1="25.25946875" x2="90.483690625" y2="25.282103125" width="0.225" layer="94" curve="14.175253"/>
<wire x1="89.66001875" y1="25.980109375" x2="89.66001875" y2="26.189" width="0.225" layer="94"/>
<wire x1="89.66001875" y1="26.189" x2="89.66001875" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="89.6773" y1="26.590590625" x2="89.66001875" y2="26.34016875" width="0.225" layer="94" curve="7.895583"/>
<wire x1="89.75726875" y1="26.82721875" x2="89.685840625" y2="26.639" width="0.225" layer="94" curve="17.336884"/>
<wire x1="89.685840625" y1="26.639" x2="89.6773" y2="26.590590625" width="0.225" layer="94" curve="4.218105"/>
<wire x1="89.93936875" y1="26.995190625" x2="89.75726875" y2="26.82721875" width="0.225" layer="94" curve="35.721204"/>
<wire x1="90.18176875" y1="27.05536875" x2="89.93936875" y2="26.995190625" width="0.225" layer="94" curve="21.771833"/>
<wire x1="90.830209375" y1="26.30881875" x2="90.830209375" y2="26.414" width="0.225" layer="94"/>
<wire x1="90.830209375" y1="26.414" x2="90.830209375" y2="26.49631875" width="0.225" layer="94"/>
<wire x1="90.830209375" y1="26.49631875" x2="90.7978375" y2="26.744540625" width="0.225" layer="94" curve="14.861165"/>
<wire x1="90.797840625" y1="26.744540625" x2="90.6616625" y2="26.951284375" width="0.225" layer="94" curve="37.022842"/>
<wire x1="90.661659375" y1="26.95128125" x2="90.432340625" y2="27.047065625" width="0.225" layer="94" curve="30.892228"/>
<wire x1="90.432340625" y1="27.04706875" x2="90.18176875" y2="27.055365625" width="0.225" layer="94" curve="10.656803"/>
<wire x1="68.6921" y1="33.944190625" x2="68.78723125" y2="33.839" width="0.225" layer="94"/>
<wire x1="68.78723125" y1="33.839" x2="69.804659375" y2="32.714" width="0.225" layer="94"/>
<wire x1="69.804659375" y1="32.714" x2="70.21163125" y2="32.264" width="0.225" layer="94"/>
<wire x1="70.21163125" y1="32.264" x2="71.229059375" y2="31.139" width="0.225" layer="94"/>
<wire x1="71.229059375" y1="31.139" x2="71.63603125" y2="30.689" width="0.225" layer="94"/>
<wire x1="71.63603125" y1="30.689" x2="72.653459375" y2="29.564" width="0.225" layer="94"/>
<wire x1="72.653459375" y1="29.564" x2="72.94835" y2="29.23793125" width="0.225" layer="94"/>
<wire x1="72.94835" y1="29.23793125" x2="73.44671875" y2="29.789" width="0.225" layer="94"/>
<wire x1="73.44671875" y1="29.789" x2="73.6502" y2="30.014" width="0.225" layer="94"/>
<wire x1="73.6502" y1="30.014" x2="74.26065" y2="30.689" width="0.225" layer="94"/>
<wire x1="74.26065" y1="30.689" x2="74.46413125" y2="30.914" width="0.225" layer="94"/>
<wire x1="74.46413125" y1="30.914" x2="75.278059375" y2="31.814" width="0.225" layer="94"/>
<wire x1="75.278059375" y1="31.814" x2="75.481540625" y2="32.039" width="0.225" layer="94"/>
<wire x1="75.481540625" y1="32.039" x2="76.29546875" y2="32.939" width="0.225" layer="94"/>
<wire x1="76.29546875" y1="32.939" x2="76.49895" y2="33.164" width="0.225" layer="94"/>
<wire x1="76.49895" y1="33.164" x2="76.70243125" y2="33.389" width="0.225" layer="94"/>
<wire x1="76.70243125" y1="33.389" x2="77.204709375" y2="33.944390625" width="0.225" layer="94"/>
<wire x1="77.204709375" y1="33.944390625" x2="76.036390625" y2="35.00103125" width="0.225" layer="94"/>
<wire x1="76.036390625" y1="35.00103125" x2="74.57861875" y2="33.389" width="0.225" layer="94"/>
<wire x1="74.57861875" y1="33.389" x2="74.37515" y2="33.164" width="0.225" layer="94"/>
<wire x1="74.37515" y1="33.164" x2="74.17168125" y2="32.939" width="0.225" layer="94"/>
<wire x1="74.17168125" y1="32.939" x2="73.764740625" y2="32.489" width="0.225" layer="94"/>
<wire x1="73.764740625" y1="32.489" x2="72.94835" y2="31.58621875" width="0.225" layer="94"/>
<wire x1="72.94835" y1="31.58621875" x2="72.742359375" y2="31.814" width="0.225" layer="94"/>
<wire x1="72.742359375" y1="31.814" x2="72.131940625" y2="32.489" width="0.225" layer="94"/>
<wire x1="72.131940625" y1="32.489" x2="71.724990625" y2="32.939" width="0.225" layer="94"/>
<wire x1="71.724990625" y1="32.939" x2="71.318040625" y2="33.389" width="0.225" layer="94"/>
<wire x1="71.318040625" y1="33.389" x2="70.911090625" y2="33.839" width="0.225" layer="94"/>
<wire x1="70.911090625" y1="33.839" x2="70.504140625" y2="34.289" width="0.225" layer="94"/>
<wire x1="70.504140625" y1="34.289" x2="70.097190625" y2="34.739" width="0.225" layer="94"/>
<wire x1="70.097190625" y1="34.739" x2="69.86021875" y2="35.001040625" width="0.225" layer="94"/>
<wire x1="81.341240625" y1="26.03255" x2="81.74326875" y2="26.03255" width="0.225" layer="94"/>
<wire x1="81.74326875" y1="26.03255" x2="81.74326875" y2="25.964" width="0.225" layer="94"/>
<wire x1="81.74326875" y1="25.964" x2="81.74326875" y2="25.812240625" width="0.225" layer="94"/>
<wire x1="81.723590625" y1="25.602390625" x2="81.743275" y2="25.812240625" width="0.225" layer="94" curve="10.717258"/>
<wire x1="81.637890625" y1="25.41138125" x2="81.7235875" y2="25.602390625" width="0.225" layer="94" curve="26.892331"/>
<wire x1="81.467640625" y1="25.291140625" x2="81.6378875" y2="25.41138125" width="0.225" layer="94" curve="34.316783"/>
<wire x1="81.260009375" y1="25.258559375" x2="81.467640625" y2="25.291140625" width="0.225" layer="94" curve="18.312496"/>
<wire x1="81.05158125" y1="25.28623125" x2="81.260009375" y2="25.258559375" width="0.225" layer="94" curve="14.64866"/>
<wire x1="80.880909375" y1="25.405009375" x2="81.05158125" y2="25.286234375" width="0.225" layer="94" curve="39.895242"/>
<wire x1="80.80178125" y1="25.5987" x2="80.880903125" y2="25.40500625" width="0.225" layer="94" curve="25.996239"/>
<wire x1="80.786140625" y1="25.80901875" x2="80.786140625" y2="25.964" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="25.964" x2="80.786140625" y2="26.189" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="26.189" x2="80.786140625" y2="26.414" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="26.414" x2="80.786140625" y2="26.626990625" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="25.80901875" x2="80.78728125" y2="25.739" width="0.225" layer="94" curve="3.295417"/>
<wire x1="80.78728125" y1="25.739" x2="80.801784375" y2="25.5987" width="0.225" layer="94" curve="6.640297"/>
<wire x1="81.206190625" y1="25.86908125" x2="81.206190625" y2="25.964" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="25.964" x2="81.206190625" y2="26.189" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="26.189" x2="81.206190625" y2="26.414" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="26.414" x2="81.206190625" y2="26.626990625" width="0.225" layer="94"/>
<wire x1="81.341240625" y1="26.03255" x2="81.341240625" y2="25.964" width="0.225" layer="94"/>
<wire x1="81.341240625" y1="25.964" x2="81.341240625" y2="25.779140625" width="0.225" layer="94"/>
<wire x1="81.317509375" y1="25.6654" x2="81.327084375" y2="25.6739125" width="0.225" layer="94" curve="49.818215"/>
<wire x1="81.30451875" y1="25.66235" x2="81.3175125" y2="25.66539375" width="0.225" layer="94" curve="7.103613"/>
<wire x1="81.291240625" y1="25.66093125" x2="81.30451875" y2="25.66235625" width="0.225" layer="94" curve="7.003084"/>
<wire x1="81.277890625" y1="25.660490625" x2="81.291240625" y2="25.660934375" width="0.225" layer="94" curve="1.449937"/>
<wire x1="81.26453125" y1="25.66073125" x2="81.277890625" y2="25.6604875" width="0.225" layer="94" curve="4.441603"/>
<wire x1="81.33501875" y1="25.69935" x2="81.33183125" y2="25.68638125" width="0.225" layer="94"/>
<wire x1="81.33183125" y1="25.68638125" x2="81.327090625" y2="25.673909375" width="0.225" layer="94"/>
<wire x1="81.33501875" y1="25.69935" x2="81.3373" y2="25.712509375" width="0.225" layer="94"/>
<wire x1="81.3373" y1="25.712509375" x2="81.340009375" y2="25.739" width="0.225" layer="94"/>
<wire x1="81.340009375" y1="25.739" x2="81.34001875" y2="25.739090625" width="0.225" layer="94"/>
<wire x1="81.34001875" y1="25.739090625" x2="81.341240625" y2="25.779140625" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="25.86908125" x2="81.20651875" y2="25.818159375" width="0.225" layer="94"/>
<wire x1="81.20651875" y1="25.818159375" x2="81.208009375" y2="25.76726875" width="0.225" layer="94"/>
<wire x1="81.208009375" y1="25.76726875" x2="81.209609375" y2="25.741859375" width="0.225" layer="94"/>
<wire x1="81.209609375" y1="25.741859375" x2="81.21228125" y2="25.716540625" width="0.225" layer="94"/>
<wire x1="81.21228125" y1="25.716540625" x2="81.21671875" y2="25.69148125" width="0.225" layer="94"/>
<wire x1="81.21671875" y1="25.69148125" x2="81.225321875" y2="25.66765" width="0.225" layer="94" curve="19.627877"/>
<wire x1="81.26453125" y1="25.66073125" x2="81.251209375" y2="25.66166875" width="0.225" layer="94"/>
<wire x1="81.238" y1="25.663609375" x2="81.251209375" y2="25.661671875" width="0.225" layer="94" curve="8.648156"/>
<wire x1="81.22531875" y1="25.66765" x2="81.238" y2="25.66360625" width="0.225" layer="94" curve="10.044676"/>
<wire x1="81.7013" y1="26.626990625" x2="81.206190625" y2="26.626990625" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="27.03186875" x2="81.7013" y2="27.03186875" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="27.401009375" x2="81.206190625" y2="27.314" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="27.314" x2="81.206190625" y2="27.089" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="27.089" x2="81.206190625" y2="27.03186875" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="27.03186875" x2="80.786140625" y2="27.089" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="27.089" x2="80.786140625" y2="27.314" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="27.314" x2="80.786140625" y2="27.401009375" width="0.225" layer="94"/>
<wire x1="80.627059375" y1="27.03186875" x2="80.786140625" y2="27.03186875" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="26.626990625" x2="80.627059375" y2="26.626990625" width="0.225" layer="94"/>
<wire x1="80.786140625" y1="27.401009375" x2="81.206190625" y2="27.401009375" width="0.225" layer="94"/>
<wire x1="81.7013" y1="27.03186875" x2="81.7013" y2="26.864" width="0.225" layer="94"/>
<wire x1="81.7013" y1="26.864" x2="81.7013" y2="26.639" width="0.225" layer="94"/>
<wire x1="81.7013" y1="26.639" x2="81.7013" y2="26.626990625" width="0.225" layer="94"/>
<wire x1="80.627059375" y1="27.03186875" x2="80.627059375" y2="26.864" width="0.225" layer="94"/>
<wire x1="80.627059375" y1="26.864" x2="80.627059375" y2="26.639" width="0.225" layer="94"/>
<wire x1="80.627059375" y1="26.639" x2="80.627059375" y2="26.626990625" width="0.225" layer="94"/>
<wire x1="92.6429" y1="26.95253125" x2="92.469709375" y2="27.040159375" width="0.225" layer="94" curve="32.338936"/>
<wire x1="79.95228125" y1="26.071809375" x2="79.52623125" y2="26.071809375" width="0.225" layer="94"/>
<wire x1="79.49928125" y1="26.30881875" x2="79.91931875" y2="26.30881875" width="0.225" layer="94"/>
<wire x1="79.91931875" y1="26.30881875" x2="79.91931875" y2="26.414" width="0.225" layer="94"/>
<wire x1="79.91931875" y1="26.414" x2="79.91931875" y2="26.49631875" width="0.225" layer="94"/>
<wire x1="79.91931875" y1="26.49631875" x2="79.886946875" y2="26.744540625" width="0.225" layer="94" curve="14.861165"/>
<wire x1="79.88695" y1="26.744540625" x2="79.750771875" y2="26.951284375" width="0.225" layer="94" curve="37.022842"/>
<wire x1="79.75076875" y1="26.95128125" x2="79.521459375" y2="27.047065625" width="0.225" layer="94" curve="30.891086"/>
<wire x1="79.521459375" y1="27.04706875" x2="79.27088125" y2="27.055365625" width="0.225" layer="94" curve="10.657067"/>
<wire x1="79.27088125" y1="27.05536875" x2="79.02848125" y2="26.995190625" width="0.225" layer="94" curve="21.019898"/>
<wire x1="79.02848125" y1="26.995190625" x2="78.84638125" y2="26.82721875" width="0.225" layer="94" curve="36.47327"/>
<wire x1="78.84638125" y1="26.82721875" x2="78.77521875" y2="26.639" width="0.225" layer="94" curve="16.726592"/>
<wire x1="78.77521875" y1="26.639" x2="78.7664125" y2="26.590590625" width="0.225" layer="94" curve="4.076419"/>
<wire x1="78.766409375" y1="26.590590625" x2="78.75028125" y2="26.414" width="0.225" layer="94" curve="6.105964"/>
<wire x1="78.75028125" y1="26.414" x2="78.749128125" y2="26.34016875" width="0.225" layer="94" curve="2.541594"/>
<wire x1="79.95228125" y1="26.071809375" x2="79.95228125" y2="25.964" width="0.225" layer="94"/>
<wire x1="79.95228125" y1="25.964" x2="79.95228125" y2="25.89603125" width="0.225" layer="94"/>
<wire x1="79.922940625" y1="25.63598125" x2="79.94166875" y2="25.739" width="0.225" layer="94" curve="5.141671"/>
<wire x1="79.94166875" y1="25.739" x2="79.952278125" y2="25.89603125" width="0.225" layer="94" curve="7.731903"/>
<wire x1="79.79936875" y1="25.40781875" x2="79.922940625" y2="25.63598125" width="0.225" layer="94" curve="31.131749"/>
<wire x1="79.572809375" y1="25.282109375" x2="79.60065" y2="25.289" width="0.225" layer="94" curve="3.702975"/>
<wire x1="79.60065" y1="25.289" x2="79.799371875" y2="25.407815625" width="0.225" layer="94" curve="30.237845"/>
<wire x1="79.312259375" y1="25.25946875" x2="79.572809375" y2="25.28210625" width="0.225" layer="94" curve="14.175788"/>
<wire x1="79.056009375" y1="25.309240625" x2="79.119840625" y2="25.289" width="0.225" layer="94" curve="4.53295"/>
<wire x1="79.119840625" y1="25.289" x2="79.312259375" y2="25.259475" width="0.225" layer="94" curve="13.203582"/>
<wire x1="78.856609375" y1="25.473640625" x2="79.056009375" y2="25.3092375" width="0.225" layer="94" curve="39.29237"/>
<wire x1="78.767890625" y1="25.718659375" x2="78.85660625" y2="25.473640625" width="0.225" layer="94" curve="21.889189"/>
<wire x1="78.74913125" y1="25.980109375" x2="78.76789375" y2="25.718659375" width="0.225" layer="94" curve="9.71004"/>
<wire x1="78.74913125" y1="25.980109375" x2="78.74913125" y2="26.189" width="0.225" layer="94"/>
<wire x1="78.74913125" y1="26.189" x2="78.74913125" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="79.49928125" y1="26.30881875" x2="79.49928125" y2="26.414" width="0.225" layer="94"/>
<wire x1="79.49928125" y1="26.414" x2="79.49928125" y2="26.44828125" width="0.225" layer="94"/>
<wire x1="79.49928125" y1="26.44828125" x2="79.496253125" y2="26.51431875" width="0.225" layer="94" curve="5.249884"/>
<wire x1="79.49625" y1="26.51431875" x2="79.48934375" y2="26.557840625" width="0.225" layer="94" curve="7.529032"/>
<wire x1="79.489340625" y1="26.557840625" x2="79.483196875" y2="26.579" width="0.225" layer="94" curve="6.832689"/>
<wire x1="79.4832" y1="26.579" x2="79.47398125" y2="26.598990625" width="0.225" layer="94" curve="10.282953"/>
<wire x1="79.47398125" y1="26.598990625" x2="79.459609375" y2="26.615509375" width="0.225" layer="94" curve="22.257449"/>
<wire x1="79.459609375" y1="26.615509375" x2="79.440259375" y2="26.625909375" width="0.225" layer="94" curve="19.188066"/>
<wire x1="79.440259375" y1="26.625909375" x2="79.39738125" y2="26.63575" width="0.225" layer="94" curve="11.463239"/>
<wire x1="79.39738125" y1="26.63575" x2="79.3534" y2="26.6386375" width="0.225" layer="94" curve="6.876925"/>
<wire x1="79.3534" y1="26.638640625" x2="79.30931875" y2="26.63774375" width="0.225" layer="94" curve="2.970191"/>
<wire x1="79.30931875" y1="26.637740625" x2="79.265559375" y2="26.632625" width="0.225" layer="94" curve="8.0302"/>
<wire x1="79.265559375" y1="26.63263125" x2="79.244209375" y2="26.6272" width="0.225" layer="94" curve="7.184068"/>
<wire x1="79.244209375" y1="26.6272" x2="79.2243" y2="26.617890625" width="0.225" layer="94" curve="14.385749"/>
<wire x1="79.2243" y1="26.617890625" x2="79.210078125" y2="26.6013" width="0.225" layer="94" curve="34.295039"/>
<wire x1="79.21008125" y1="26.6013" x2="79.191453125" y2="26.53813125" width="0.225" layer="94" curve="14.037627"/>
<wire x1="79.19145" y1="26.53813125" x2="79.1844125" y2="26.472390625" width="0.225" layer="94" curve="6.60122"/>
<wire x1="79.52623125" y1="26.071809375" x2="79.52623125" y2="25.964" width="0.225" layer="94"/>
<wire x1="79.52623125" y1="25.964" x2="79.52623125" y2="25.89925" width="0.225" layer="94"/>
<wire x1="79.51938125" y1="25.780190625" x2="79.526234375" y2="25.89925" width="0.225" layer="94" curve="6.589587"/>
<wire x1="79.50856875" y1="25.733759375" x2="79.519378125" y2="25.780190625" width="0.225" layer="94" curve="13.026604"/>
<wire x1="79.45898125" y1="25.6871" x2="79.508565625" y2="25.733759375" width="0.225" layer="94" curve="54.248333"/>
<wire x1="79.4121" y1="25.6784" x2="79.45898125" y2="25.687103125" width="0.225" layer="94" curve="11.235116"/>
<wire x1="79.292940625" y1="25.678809375" x2="79.4121" y2="25.678396875" width="0.225" layer="94" curve="10.193817"/>
<wire x1="79.24633125" y1="25.688690625" x2="79.292940625" y2="25.6788125" width="0.225" layer="94" curve="13.338819"/>
<wire x1="79.22515" y1="25.6995" x2="79.24633125" y2="25.6886875" width="0.225" layer="94" curve="16.819291"/>
<wire x1="79.21035" y1="25.718009375" x2="79.225146875" y2="25.69949375" width="0.225" layer="94" curve="31.833994"/>
<wire x1="79.195259375" y1="25.763159375" x2="79.21035625" y2="25.7180125" width="0.225" layer="94" curve="8.452794"/>
<wire x1="79.187759375" y1="25.810290625" x2="79.19525625" y2="25.763159375" width="0.225" layer="94" curve="10.449934"/>
<wire x1="79.187759375" y1="25.810290625" x2="79.18275" y2="25.881709375" width="0.225" layer="94"/>
<wire x1="79.18441875" y1="26.472390625" x2="79.181740625" y2="26.406309375" width="0.225" layer="94"/>
<wire x1="79.181740625" y1="26.406309375" x2="79.181109375" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="79.181109375" y1="26.34016875" x2="79.181109375" y2="26.189" width="0.225" layer="94"/>
<wire x1="79.181109375" y1="26.189" x2="79.181109375" y2="25.97718125" width="0.225" layer="94"/>
<wire x1="79.181109375" y1="25.97718125" x2="79.18275" y2="25.881709375" width="0.225" layer="94"/>
<wire x1="77.1742" y1="26.422790625" x2="77.54935" y2="26.422790625" width="0.225" layer="94"/>
<wire x1="77.1742" y1="26.00881875" x2="77.969390625" y2="26.00881875" width="0.225" layer="94"/>
<wire x1="77.969390625" y1="26.00881875" x2="77.969390625" y2="26.189" width="0.225" layer="94"/>
<wire x1="77.969390625" y1="26.189" x2="77.969390625" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="77.969390625" y1="26.34016875" x2="77.948609375" y2="26.61348125" width="0.225" layer="94" curve="8.696144"/>
<wire x1="77.948609375" y1="26.61348125" x2="77.84784375" y2="26.866196875" width="0.225" layer="94" curve="26.085225"/>
<wire x1="77.84785" y1="26.8662" x2="77.62745" y2="27.022209375" width="0.225" layer="94" curve="39.85306"/>
<wire x1="77.62745" y1="27.022209375" x2="77.35661875" y2="27.058828125" width="0.225" layer="94" curve="15.332321"/>
<wire x1="77.35661875" y1="27.05881875" x2="77.08588125" y2="27.02155" width="0.225" layer="94" curve="15.743623"/>
<wire x1="77.08588125" y1="27.02155" x2="76.865265625" y2="26.865728125" width="0.225" layer="94" curve="39.046943"/>
<wire x1="76.865259375" y1="26.86573125" x2="76.864040625" y2="26.864" width="0.225" layer="94" curve="0.204743"/>
<wire x1="76.864040625" y1="26.864" x2="76.763478125" y2="26.61343125" width="0.225" layer="94" curve="26.341038"/>
<wire x1="76.76348125" y1="26.61343125" x2="76.74395" y2="26.414" width="0.225" layer="94" curve="6.208124"/>
<wire x1="76.74395" y1="26.414" x2="76.74221875" y2="26.34016875" width="0.225" layer="94" curve="2.287022"/>
<wire x1="77.561359375" y1="25.98158125" x2="77.966390625" y2="25.98158125" width="0.225" layer="94"/>
<wire x1="77.966390625" y1="25.98158125" x2="77.966390625" y2="25.964" width="0.225" layer="94"/>
<wire x1="77.966390625" y1="25.964" x2="77.966390625" y2="25.8181" width="0.225" layer="94"/>
<wire x1="77.92765" y1="25.565709375" x2="77.966390625" y2="25.8181" width="0.225" layer="94" curve="17.452125"/>
<wire x1="77.77643125" y1="25.363190625" x2="77.92765" y2="25.565709375" width="0.225" layer="94" curve="38.592139"/>
<wire x1="77.53896875" y1="25.271940625" x2="77.618090625" y2="25.289" width="0.225" layer="94" curve="8.168491"/>
<wire x1="77.618090625" y1="25.289" x2="77.77643125" y2="25.363190625" width="0.225" layer="94" curve="17.702304"/>
<wire x1="77.28301875" y1="25.26058125" x2="77.53896875" y2="25.271940625" width="0.225" layer="94" curve="11.086795"/>
<wire x1="77.03403125" y1="25.31643125" x2="77.11308125" y2="25.289" width="0.225" layer="94" curve="6.296209"/>
<wire x1="77.11308125" y1="25.289" x2="77.28301875" y2="25.260578125" width="0.225" layer="94" curve="12.986111"/>
<wire x1="76.844159375" y1="25.483540625" x2="77.034028125" y2="25.316421875" width="0.225" layer="94" curve="38.138006"/>
<wire x1="76.760209375" y1="25.724359375" x2="76.8441625" y2="25.483540625" width="0.225" layer="94" curve="20.717901"/>
<wire x1="76.74221875" y1="25.980109375" x2="76.758" y2="25.739" width="0.225" layer="94" curve="9.114768"/>
<wire x1="76.758" y1="25.739" x2="76.760209375" y2="25.724359375" width="0.225" layer="94" curve="0.557951"/>
<wire x1="76.74221875" y1="25.980109375" x2="76.74221875" y2="26.189" width="0.225" layer="94"/>
<wire x1="76.74221875" y1="26.189" x2="76.74221875" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="77.1742" y1="26.00881875" x2="77.1742" y2="25.97718125" width="0.225" layer="94"/>
<wire x1="77.1742" y1="25.97718125" x2="77.17648125" y2="25.863140625" width="0.225" layer="94"/>
<wire x1="77.17648125" y1="25.863140625" x2="77.179640625" y2="25.81763125" width="0.225" layer="94"/>
<wire x1="77.179640625" y1="25.81763125" x2="77.185759375" y2="25.77241875" width="0.225" layer="94"/>
<wire x1="77.185759375" y1="25.77241875" x2="77.19780625" y2="25.728490625" width="0.225" layer="94" curve="15.249939"/>
<wire x1="77.197809375" y1="25.728490625" x2="77.22771875" y2="25.69585" width="0.225" layer="94" curve="39.077584"/>
<wire x1="77.22771875" y1="25.69585" x2="77.2493" y2="25.688528125" width="0.225" layer="94" curve="18.445063"/>
<wire x1="77.2493" y1="25.68853125" x2="77.45375" y2="25.6834" width="0.225" layer="94" curve="16.158964"/>
<wire x1="77.45375" y1="25.6834" x2="77.498609375" y2="25.691559375" width="0.225" layer="94" curve="7.337797"/>
<wire x1="77.498609375" y1="25.691559375" x2="77.520209375" y2="25.6988375" width="0.225" layer="94" curve="9.286376"/>
<wire x1="77.561359375" y1="25.98158125" x2="77.561359375" y2="25.964" width="0.225" layer="94"/>
<wire x1="77.561359375" y1="25.964" x2="77.561359375" y2="25.82103125" width="0.225" layer="94"/>
<wire x1="77.561359375" y1="25.82103125" x2="77.559340625" y2="25.775459375" width="0.225" layer="94"/>
<wire x1="77.559340625" y1="25.775459375" x2="77.55613125" y2="25.75288125" width="0.225" layer="94"/>
<wire x1="77.520209375" y1="25.698840625" x2="77.556125" y2="25.75288125" width="0.225" layer="94" curve="51.030296"/>
<wire x1="77.184159375" y1="26.547240625" x2="77.174196875" y2="26.422790625" width="0.225" layer="94" curve="9.151585"/>
<wire x1="77.2077" y1="26.6134" x2="77.184159375" y2="26.547240625" width="0.225" layer="94" curve="20.872634"/>
<wire x1="77.54935" y1="26.422790625" x2="77.539775" y2="26.547290625" width="0.225" layer="94" curve="8.794744"/>
<wire x1="77.53978125" y1="26.547290625" x2="77.527525" y2="26.592734375" width="0.225" layer="94" curve="12.594721"/>
<wire x1="77.52751875" y1="26.59273125" x2="77.516803125" y2="26.613665625" width="0.225" layer="94" curve="11.429217"/>
<wire x1="77.516809375" y1="26.61366875" x2="77.5000125" y2="26.629975" width="0.225" layer="94" curve="26.064892"/>
<wire x1="77.500009375" y1="26.62996875" x2="77.47873125" y2="26.63995" width="0.225" layer="94" curve="11.965711"/>
<wire x1="77.47873125" y1="26.63995" x2="77.455990625" y2="26.64610625" width="0.225" layer="94" curve="8.00028"/>
<wire x1="77.455990625" y1="26.646109375" x2="77.38576875" y2="26.653571875" width="0.225" layer="94" curve="10.160203"/>
<wire x1="77.38576875" y1="26.65356875" x2="77.315109375" y2="26.652346875" width="0.225" layer="94" curve="3.952821"/>
<wire x1="77.315109375" y1="26.65235" x2="77.268440625" y2="26.646071875" width="0.225" layer="94" curve="9.390964"/>
<wire x1="77.268440625" y1="26.64606875" x2="77.245740625" y2="26.639796875" width="0.225" layer="94" curve="6.177099"/>
<wire x1="77.245740625" y1="26.6398" x2="77.24345" y2="26.639" width="0.225" layer="94" curve="1.397304"/>
<wire x1="77.24345" y1="26.639" x2="77.22448125" y2="26.629740625" width="0.225" layer="94" curve="12.178653"/>
<wire x1="77.22448125" y1="26.629740625" x2="77.207703125" y2="26.613396875" width="0.225" layer="94" curve="24.279547"/>
<wire x1="73.9395" y1="26.629740625" x2="73.922721875" y2="26.613396875" width="0.225" layer="94" curve="24.279547"/>
<wire x1="73.96075" y1="26.6398" x2="73.958459375" y2="26.639" width="0.225" layer="94" curve="1.397249"/>
<wire x1="73.958459375" y1="26.639" x2="73.939496875" y2="26.62974375" width="0.225" layer="94" curve="12.174138"/>
<wire x1="73.98345" y1="26.64606875" x2="73.96075" y2="26.639796875" width="0.225" layer="94" curve="6.177099"/>
<wire x1="74.03013125" y1="26.65235" x2="73.98345" y2="26.64606875" width="0.225" layer="94" curve="9.393557"/>
<wire x1="74.100790625" y1="26.65356875" x2="74.03013125" y2="26.652346875" width="0.225" layer="94" curve="3.952821"/>
<wire x1="74.171009375" y1="26.646109375" x2="74.100790625" y2="26.653571875" width="0.225" layer="94" curve="10.159745"/>
<wire x1="74.193740625" y1="26.63995" x2="74.171009375" y2="26.646103125" width="0.225" layer="94" curve="7.996993"/>
<wire x1="74.21501875" y1="26.62996875" x2="74.193740625" y2="26.63995" width="0.225" layer="94" curve="11.965711"/>
<wire x1="74.23181875" y1="26.61366875" x2="74.215021875" y2="26.629975" width="0.225" layer="94" curve="26.067092"/>
<wire x1="74.242540625" y1="26.59273125" x2="74.231821875" y2="26.613671875" width="0.225" layer="94" curve="11.432049"/>
<wire x1="74.254790625" y1="26.547290625" x2="74.2425375" y2="26.59273125" width="0.225" layer="94" curve="12.593993"/>
<wire x1="74.264359375" y1="26.422790625" x2="74.254784375" y2="26.547290625" width="0.225" layer="94" curve="8.794744"/>
<wire x1="73.92271875" y1="26.6134" x2="73.899178125" y2="26.547240625" width="0.225" layer="94" curve="20.872634"/>
<wire x1="73.89918125" y1="26.547240625" x2="73.88921875" y2="26.422790625" width="0.225" layer="94" curve="9.151622"/>
<wire x1="74.23521875" y1="25.698840625" x2="74.2678" y2="25.739" width="0.225" layer="94" curve="40.155698"/>
<wire x1="74.2678" y1="25.739" x2="74.271134375" y2="25.75288125" width="0.225" layer="94" curve="10.876304"/>
<wire x1="74.27435" y1="25.775459375" x2="74.271140625" y2="25.75288125" width="0.225" layer="94"/>
<wire x1="74.27638125" y1="25.82103125" x2="74.27435" y2="25.775459375" width="0.225" layer="94"/>
<wire x1="74.27638125" y1="25.98158125" x2="74.27638125" y2="25.964" width="0.225" layer="94"/>
<wire x1="74.27638125" y1="25.964" x2="74.27638125" y2="25.82103125" width="0.225" layer="94"/>
<wire x1="74.21361875" y1="25.691559375" x2="74.23521875" y2="25.6988375" width="0.225" layer="94" curve="9.286507"/>
<wire x1="74.16876875" y1="25.6834" x2="74.21361875" y2="25.691559375" width="0.225" layer="94" curve="7.336311"/>
<wire x1="73.964309375" y1="25.68853125" x2="74.16876875" y2="25.6834" width="0.225" layer="94" curve="16.159693"/>
<wire x1="73.94273125" y1="25.69585" x2="73.964309375" y2="25.68853125" width="0.225" layer="94" curve="18.441658"/>
<wire x1="73.91281875" y1="25.728490625" x2="73.942728125" y2="25.695846875" width="0.225" layer="94" curve="39.07874"/>
<wire x1="73.90078125" y1="25.77241875" x2="73.912828125" y2="25.72849375" width="0.225" layer="94" curve="15.249051"/>
<wire x1="73.894659375" y1="25.81763125" x2="73.90078125" y2="25.77241875" width="0.225" layer="94"/>
<wire x1="73.891490625" y1="25.863140625" x2="73.894659375" y2="25.81763125" width="0.225" layer="94"/>
<wire x1="73.88921875" y1="25.97718125" x2="73.88948125" y2="25.964" width="0.225" layer="94"/>
<wire x1="73.88948125" y1="25.964" x2="73.891490625" y2="25.863140625" width="0.225" layer="94"/>
<wire x1="73.88921875" y1="26.00881875" x2="73.88921875" y2="25.97718125" width="0.225" layer="94"/>
<wire x1="73.457240625" y1="25.980109375" x2="73.457240625" y2="26.189" width="0.225" layer="94"/>
<wire x1="73.457240625" y1="26.189" x2="73.457240625" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="73.457240625" y1="25.980109375" x2="73.47523125" y2="25.724359375" width="0.225" layer="94" curve="9.672758"/>
<wire x1="73.47523125" y1="25.724359375" x2="73.559184375" y2="25.48354375" width="0.225" layer="94" curve="20.717853"/>
<wire x1="73.55918125" y1="25.483540625" x2="73.749046875" y2="25.316425" width="0.225" layer="94" curve="38.137176"/>
<wire x1="73.74905" y1="25.31643125" x2="73.998040625" y2="25.26058125" width="0.225" layer="94" curve="19.282594"/>
<wire x1="73.998040625" y1="25.26058125" x2="74.25398125" y2="25.271940625" width="0.225" layer="94" curve="11.086392"/>
<wire x1="74.25398125" y1="25.271940625" x2="74.3331" y2="25.289" width="0.225" layer="94" curve="8.168179"/>
<wire x1="74.3331" y1="25.289" x2="74.491446875" y2="25.36319375" width="0.225" layer="94" curve="17.703003"/>
<wire x1="74.49145" y1="25.363190625" x2="74.642665625" y2="25.565709375" width="0.225" layer="94" curve="38.59175"/>
<wire x1="74.64266875" y1="25.565709375" x2="74.681409375" y2="25.8181" width="0.225" layer="94" curve="17.451998"/>
<wire x1="74.681409375" y1="25.98158125" x2="74.681409375" y2="25.964" width="0.225" layer="94"/>
<wire x1="74.681409375" y1="25.964" x2="74.681409375" y2="25.8181" width="0.225" layer="94"/>
<wire x1="74.27638125" y1="25.98158125" x2="74.681409375" y2="25.98158125" width="0.225" layer="94"/>
<wire x1="73.4785" y1="26.61343125" x2="73.45896875" y2="26.414" width="0.225" layer="94" curve="6.208134"/>
<wire x1="73.45896875" y1="26.414" x2="73.4572375" y2="26.34016875" width="0.225" layer="94" curve="2.287026"/>
<wire x1="73.58028125" y1="26.86573125" x2="73.4785" y2="26.61343125" width="0.225" layer="94" curve="26.545833"/>
<wire x1="73.8009" y1="27.02155" x2="73.58028125" y2="26.86573125" width="0.225" layer="94" curve="39.047008"/>
<wire x1="74.071640625" y1="27.05881875" x2="73.8009" y2="27.02155" width="0.225" layer="94" curve="15.743809"/>
<wire x1="74.342459375" y1="27.022209375" x2="74.071640625" y2="27.058825" width="0.225" layer="94" curve="15.331635"/>
<wire x1="74.562859375" y1="26.8662" x2="74.342459375" y2="27.022209375" width="0.225" layer="94" curve="39.85306"/>
<wire x1="74.66363125" y1="26.61348125" x2="74.5628625" y2="26.866203125" width="0.225" layer="94" curve="26.085633"/>
<wire x1="74.684409375" y1="26.34016875" x2="74.663628125" y2="26.61348125" width="0.225" layer="94" curve="8.69613"/>
<wire x1="74.684409375" y1="26.00881875" x2="74.684409375" y2="26.189" width="0.225" layer="94"/>
<wire x1="74.684409375" y1="26.189" x2="74.684409375" y2="26.34016875" width="0.225" layer="94"/>
<wire x1="73.88921875" y1="26.00881875" x2="74.684409375" y2="26.00881875" width="0.225" layer="94"/>
<wire x1="73.88921875" y1="26.422790625" x2="74.264359375" y2="26.422790625" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="25.28548125" x2="82.92818125" y2="25.28548125" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="27.03186875" x2="82.80066875" y2="27.03186875" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="27.03186875" x2="82.508140625" y2="26.864" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="26.864" x2="82.508140625" y2="26.639" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="26.639" x2="82.508140625" y2="26.414" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="26.414" x2="82.508140625" y2="26.189" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="26.189" x2="82.508140625" y2="25.964" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="25.964" x2="82.508140625" y2="25.739" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="25.739" x2="82.508140625" y2="25.514" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="25.514" x2="82.508140625" y2="25.289" width="0.225" layer="94"/>
<wire x1="82.508140625" y1="25.289" x2="82.508140625" y2="25.28548125" width="0.225" layer="94"/>
<wire x1="83.18028125" y1="26.30266875" x2="83.585309375" y2="26.30266875" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="26.358340625" x2="82.92818125" y2="26.189" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="26.189" x2="82.92818125" y2="25.964" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="25.964" x2="82.92818125" y2="25.739" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="25.739" x2="82.92818125" y2="25.514" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="25.514" x2="82.92818125" y2="25.289" width="0.225" layer="94"/>
<wire x1="82.92818125" y1="25.289" x2="82.92818125" y2="25.28548125" width="0.225" layer="94"/>
<wire x1="82.8883" y1="26.98991875" x2="82.80066875" y2="27.03186875" width="0.225" layer="94" curve="51.162574"/>
<wire x1="86.810090625" y1="25.28548125" x2="86.810090625" y2="25.289" width="0.225" layer="94"/>
<wire x1="86.810090625" y1="25.289" x2="86.810090625" y2="25.514" width="0.225" layer="94"/>
<wire x1="86.810090625" y1="25.514" x2="86.810090625" y2="25.739" width="0.225" layer="94"/>
<wire x1="86.810090625" y1="25.739" x2="86.810090625" y2="25.964" width="0.225" layer="94"/>
<wire x1="86.810090625" y1="25.964" x2="86.810090625" y2="26.189" width="0.225" layer="94"/>
<wire x1="86.810090625" y1="26.189" x2="86.810090625" y2="26.25023125" width="0.225" layer="94"/>
<wire x1="86.39005" y1="25.28548125" x2="86.39005" y2="25.289" width="0.225" layer="94"/>
<wire x1="86.39005" y1="25.289" x2="86.39005" y2="25.514" width="0.225" layer="94"/>
<wire x1="86.39005" y1="25.514" x2="86.39005" y2="25.739" width="0.225" layer="94"/>
<wire x1="86.39005" y1="25.739" x2="86.39005" y2="25.964" width="0.225" layer="94"/>
<wire x1="86.39005" y1="25.964" x2="86.39005" y2="26.189" width="0.225" layer="94"/>
<wire x1="86.39005" y1="26.189" x2="86.39005" y2="26.414" width="0.225" layer="94"/>
<wire x1="86.39005" y1="26.414" x2="86.39005" y2="26.639" width="0.225" layer="94"/>
<wire x1="86.39005" y1="26.639" x2="86.39005" y2="26.864" width="0.225" layer="94"/>
<wire x1="86.39005" y1="26.864" x2="86.39005" y2="27.03186875" width="0.225" layer="94"/>
<wire x1="86.39005" y1="25.28548125" x2="86.810090625" y2="25.28548125" width="0.225" layer="94"/>
<wire x1="87.1462" y1="25.28548125" x2="87.566240625" y2="25.28548125" width="0.225" layer="94"/>
<wire x1="86.68258125" y1="27.03186875" x2="86.39005" y2="27.03186875" width="0.225" layer="94"/>
<wire x1="86.770490625" y1="26.98956875" x2="86.68258125" y2="27.03186875" width="0.225" layer="94" curve="51.389654"/>
<wire x1="83.585309375" y1="26.30266875" x2="83.585309375" y2="26.414" width="0.225" layer="94"/>
<wire x1="83.585309375" y1="26.414" x2="83.585309375" y2="26.532359375" width="0.225" layer="94"/>
<wire x1="83.585309375" y1="26.532359375" x2="83.57331875" y2="26.7009" width="0.225" layer="94" curve="8.13878"/>
<wire x1="83.57331875" y1="26.7009" x2="83.52315625" y2="26.8617" width="0.225" layer="94" curve="18.374099"/>
<wire x1="83.523159375" y1="26.8617" x2="83.41396875" y2="26.9887" width="0.225" layer="94" curve="28.349931"/>
<wire x1="83.41396875" y1="26.9887" x2="83.25806875" y2="27.050721875" width="0.225" layer="94" curve="26.884991"/>
<wire x1="83.25806875" y1="27.05071875" x2="83.0895" y2="27.055371875" width="0.225" layer="94" curve="13.343084"/>
<wire x1="83.0895" y1="27.05538125" x2="82.888296875" y2="26.989925" width="0.225" layer="94" curve="25.861473"/>
<wire x1="87.566240625" y1="25.28548125" x2="87.566240625" y2="25.289" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="25.289" x2="87.566240625" y2="25.514" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="25.514" x2="87.566240625" y2="25.739" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="25.739" x2="87.566240625" y2="25.964" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="25.964" x2="87.566240625" y2="26.189" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="26.189" x2="87.566240625" y2="26.414" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="26.414" x2="87.566240625" y2="26.418390625" width="0.225" layer="94"/>
<wire x1="87.566240625" y1="26.418390625" x2="87.555215625" y2="26.606540625" width="0.225" layer="94" curve="6.706552"/>
<wire x1="87.55521875" y1="26.606540625" x2="87.509115625" y2="26.788871875" width="0.225" layer="94" curve="14.968121"/>
<wire x1="87.509109375" y1="26.78886875" x2="87.403925" y2="26.943603125" width="0.225" layer="94" curve="25.064472"/>
<wire x1="87.40393125" y1="26.943609375" x2="87.240590625" y2="27.034540625" width="0.225" layer="94" curve="28.312195"/>
<wire x1="87.240590625" y1="27.034540625" x2="87.05421875" y2="27.058825" width="0.225" layer="94" curve="15.049352"/>
<wire x1="87.05421875" y1="27.05881875" x2="86.770490625" y2="26.98956875" width="0.225" layer="94" curve="27.231353"/>
<wire x1="87.1462" y1="25.28548125" x2="87.1462" y2="25.289" width="0.225" layer="94"/>
<wire x1="87.1462" y1="25.289" x2="87.1462" y2="25.514" width="0.225" layer="94"/>
<wire x1="87.1462" y1="25.514" x2="87.1462" y2="25.739" width="0.225" layer="94"/>
<wire x1="87.1462" y1="25.739" x2="87.1462" y2="25.964" width="0.225" layer="94"/>
<wire x1="87.1462" y1="25.964" x2="87.1462" y2="26.189" width="0.225" layer="94"/>
<wire x1="87.1462" y1="26.189" x2="87.1462" y2="26.3642" width="0.225" layer="94"/>
<wire x1="87.1462" y1="26.3642" x2="87.139771875" y2="26.502909375" width="0.225" layer="94" curve="5.307835"/>
<wire x1="87.13976875" y1="26.502909375" x2="87.123915625" y2="26.584609375" width="0.225" layer="94" curve="11.346275"/>
<wire x1="87.12391875" y1="26.584609375" x2="87.11383125" y2="26.61046875" width="0.225" layer="94" curve="9.30383"/>
<wire x1="87.11383125" y1="26.61046875" x2="87.098490625" y2="26.633490625" width="0.225" layer="94" curve="15.447128"/>
<wire x1="87.098490625" y1="26.633490625" x2="87.0752" y2="26.648190625" width="0.225" layer="94" curve="32.662799"/>
<wire x1="87.0752" y1="26.648190625" x2="86.99275" y2="26.656371875" width="0.225" layer="94" curve="20.527762"/>
<wire x1="86.99275" y1="26.65636875" x2="86.9651" y2="26.653715625" width="0.225" layer="94" curve="1.764695"/>
<wire x1="86.9651" y1="26.65371875" x2="86.912034375" y2="26.638084375" width="0.225" layer="94" curve="20.107123"/>
<wire x1="86.91203125" y1="26.638090625" x2="86.870678125" y2="26.601821875" width="0.225" layer="94" curve="29.565942"/>
<wire x1="86.87068125" y1="26.60181875" x2="86.84560625" y2="26.55240625" width="0.225" layer="94" curve="14.114289"/>
<wire x1="86.8456" y1="26.552409375" x2="86.824978125" y2="26.471759375" width="0.225" layer="94" curve="11.013017"/>
<wire x1="86.82498125" y1="26.471759375" x2="86.813053125" y2="26.36131875" width="0.225" layer="94" curve="5.344301"/>
<wire x1="86.81305" y1="26.36131875" x2="86.810090625" y2="26.25023125" width="0.225" layer="94" curve="3.932748"/>
<wire x1="83.18028125" y1="26.30266875" x2="83.18028125" y2="26.414" width="0.225" layer="94"/>
<wire x1="83.18028125" y1="26.414" x2="83.18028125" y2="26.487240625" width="0.225" layer="94"/>
<wire x1="83.18028125" y1="26.487240625" x2="83.176690625" y2="26.56661875" width="0.225" layer="94" curve="5.181151"/>
<wire x1="83.176690625" y1="26.56661875" x2="83.16605625" y2="26.625190625" width="0.225" layer="94" curve="10.220705"/>
<wire x1="83.166059375" y1="26.625190625" x2="83.12728125" y2="26.664134375" width="0.225" layer="94" curve="58.95127"/>
<wire x1="83.12728125" y1="26.66413125" x2="82.99755" y2="26.639" width="0.225" layer="94" curve="53.217192"/>
<wire x1="82.99755" y1="26.639" x2="82.95845" y2="26.593540625" width="0.225" layer="94" curve="23.452395"/>
<wire x1="82.95845" y1="26.593540625" x2="82.95161875" y2="26.574890625" width="0.225" layer="94"/>
<wire x1="82.95161875" y1="26.574890625" x2="82.946159375" y2="26.55578125" width="0.225" layer="94"/>
<wire x1="82.946159375" y1="26.55578125" x2="82.941775" y2="26.5364" width="0.225" layer="94" curve="6.402047"/>
<wire x1="82.94176875" y1="26.5364" x2="82.931515625" y2="26.45761875" width="0.225" layer="94" curve="4.264451"/>
<wire x1="82.93151875" y1="26.45761875" x2="82.930209375" y2="26.437790625" width="0.225" layer="94"/>
<wire x1="82.930209375" y1="26.437790625" x2="82.92865" y2="26.39808125" width="0.225" layer="94"/>
<wire x1="82.92865" y1="26.39808125" x2="82.92818125" y2="26.358340625" width="0.225" layer="94"/>
<wire x1="92.67961875" y1="26.22455" x2="92.54725" y2="26.31605" width="0.225" layer="94" curve="24.406928"/>
<wire x1="92.54725" y1="26.31605" x2="92.391190625" y2="26.358428125" width="0.225" layer="94" curve="14.517731"/>
<wire x1="92.391190625" y1="26.35841875" x2="92.2218" y2="26.3835" width="0.225" layer="94"/>
<wire x1="92.2218" y1="26.3835" x2="92.13726875" y2="26.39681875" width="0.225" layer="94"/>
<wire x1="92.09533125" y1="26.40531875" x2="92.13726875" y2="26.39681875" width="0.225" layer="94" curve="5.000072"/>
<wire x1="92.0546" y1="26.41821875" x2="92.0661" y2="26.414" width="0.225" layer="94" curve="2.068384"/>
<wire x1="92.0661" y1="26.414" x2="92.09533125" y2="26.405321875" width="0.225" layer="94" curve="5.150249"/>
<wire x1="92.054009375" y1="26.430409375" x2="92.054596875" y2="26.41821875" width="0.225" layer="94" curve="5.514941"/>
<wire x1="92.054009375" y1="26.430409375" x2="92.054009375" y2="26.57718125" width="0.225" layer="94"/>
<wire x1="92.05665" y1="26.61703125" x2="92.0540125" y2="26.57718125" width="0.225" layer="94" curve="7.573746"/>
<wire x1="92.061659375" y1="26.63633125" x2="92.056646875" y2="26.61703125" width="0.225" layer="94" curve="13.971436"/>
<wire x1="92.068809375" y1="26.642740625" x2="92.0616625" y2="26.636328125" width="0.225" layer="94" curve="41.625207"/>
<wire x1="92.097790625" y1="26.650090625" x2="92.068809375" y2="26.642740625" width="0.225" layer="94" curve="13.721902"/>
<wire x1="92.15758125" y1="26.653940625" x2="92.097790625" y2="26.650090625" width="0.225" layer="94" curve="7.371099"/>
<wire x1="92.15758125" y1="26.653940625" x2="92.2566" y2="26.653940625" width="0.225" layer="94"/>
<wire x1="92.29483125" y1="26.652240625" x2="92.2566" y2="26.653940625" width="0.225" layer="94" curve="5.089448"/>
<wire x1="92.32143125" y1="26.647590625" x2="92.29483125" y2="26.652240625" width="0.225" layer="94" curve="9.654072"/>
<wire x1="92.331859375" y1="26.64346875" x2="92.32143125" y2="26.6475875" width="0.225" layer="94" curve="13.639596"/>
<wire x1="92.33591875" y1="26.63303125" x2="92.33185625" y2="26.64346875" width="0.225" layer="94" curve="15.08669"/>
<wire x1="92.34036875" y1="26.6065" x2="92.335915625" y2="26.63303125" width="0.225" layer="94" curve="8.390546"/>
<wire x1="92.34215" y1="26.568390625" x2="92.340375" y2="26.6065" width="0.225" layer="94" curve="5.332675"/>
<wire x1="92.34215" y1="26.568390625" x2="92.34215" y2="26.414" width="0.225" layer="94"/>
<wire x1="92.34215" y1="26.414" x2="92.34215" y2="26.413709375" width="0.225" layer="94"/>
<wire x1="92.34215" y1="26.413709375" x2="92.759190625" y2="26.413709375" width="0.225" layer="94"/>
<wire x1="92.759190625" y1="26.413709375" x2="92.759190625" y2="26.414" width="0.225" layer="94"/>
<wire x1="92.759190625" y1="26.414" x2="92.759190625" y2="26.5892" width="0.225" layer="94"/>
<wire x1="92.759190625" y1="26.5892" x2="92.738496875" y2="26.78398125" width="0.225" layer="94" curve="12.129007"/>
<wire x1="92.738490625" y1="26.78398125" x2="92.642896875" y2="26.952528125" width="0.225" layer="94" curve="34.862523"/>
<wire x1="92.469709375" y1="27.040159375" x2="92.27461875" y2="27.058825" width="0.225" layer="94" curve="10.407762"/>
<wire x1="92.27461875" y1="27.05881875" x2="92.139559375" y2="27.05881875" width="0.225" layer="94"/>
<wire x1="92.139559375" y1="27.05881875" x2="91.94439375" y2="27.0367625" width="0.225" layer="94" curve="15.066235"/>
<wire x1="91.944390625" y1="27.03676875" x2="91.76905625" y2="26.950671875" width="0.225" layer="94" curve="24.344315"/>
<wire x1="91.769059375" y1="26.95066875" x2="91.656853125" y2="26.79153125" width="0.225" layer="94" curve="32.9752"/>
<wire x1="91.65685" y1="26.79153125" x2="91.626409375" y2="26.639" width="0.225" layer="94" curve="14.82531"/>
<wire x1="91.626409375" y1="26.639" x2="91.62503125" y2="26.59828125" width="0.225" layer="94" curve="3.873269"/>
<wire x1="91.62503125" y1="26.59828125" x2="91.62503125" y2="26.414" width="0.225" layer="94"/>
<wire x1="91.62503125" y1="26.414" x2="91.62503125" y2="26.39436875" width="0.225" layer="94"/>
<wire x1="91.62503125" y1="26.39436875" x2="91.654403125" y2="26.22111875" width="0.225" layer="94" curve="19.244585"/>
<wire x1="91.6544" y1="26.22111875" x2="91.767428125" y2="26.088775" width="0.225" layer="94" curve="42.510013"/>
<wire x1="91.76743125" y1="26.08878125" x2="91.931890625" y2="26.02583125" width="0.225" layer="94" curve="14.601117"/>
<wire x1="91.931890625" y1="26.02583125" x2="92.105790625" y2="25.992246875" width="0.225" layer="94" curve="5.428189"/>
<wire x1="92.105790625" y1="25.99225" x2="92.16531875" y2="25.98243125" width="0.225" layer="94"/>
<wire x1="92.16531875" y1="25.98243125" x2="92.22491875" y2="25.97266875" width="0.225" layer="94"/>
<wire x1="92.22491875" y1="25.97266875" x2="92.28428125" y2="25.96156875" width="0.225" layer="94"/>
<wire x1="92.327340625" y1="25.94943125" x2="92.28428125" y2="25.9615625" width="0.225" layer="94" curve="10.274433"/>
<wire x1="92.34125" y1="25.94243125" x2="92.32734375" y2="25.9494375" width="0.225" layer="94" curve="11.730311"/>
<wire x1="92.346959375" y1="25.91516875" x2="92.34124375" y2="25.942428125" width="0.225" layer="94" curve="13.906979"/>
<wire x1="92.34815" y1="25.887240625" x2="92.346959375" y2="25.91516875" width="0.225" layer="94" curve="4.888175"/>
<wire x1="92.34815" y1="25.887240625" x2="92.34815" y2="25.7431" width="0.225" layer="94"/>
<wire x1="92.346759375" y1="25.71286875" x2="92.348153125" y2="25.7431" width="0.225" layer="94" curve="5.283856"/>
<wire x1="92.34045" y1="25.683359375" x2="92.34675625" y2="25.71286875" width="0.225" layer="94" curve="13.55484"/>
<wire x1="92.333959375" y1="25.67608125" x2="92.340453125" y2="25.683359375" width="0.225" layer="94" curve="45.791754"/>
<wire x1="92.314840625" y1="25.66976875" x2="92.333959375" y2="25.676084375" width="0.225" layer="94" curve="14.183972"/>
<wire x1="92.274840625" y1="25.66463125" x2="92.314840625" y2="25.669771875" width="0.225" layer="94" curve="7.7277"/>
<wire x1="92.24416875" y1="25.663709375" x2="92.274840625" y2="25.664628125" width="0.225" layer="94" curve="3.484382"/>
<wire x1="92.24416875" y1="25.663709375" x2="92.1096" y2="25.663709375" width="0.225" layer="94"/>
<wire x1="92.07173125" y1="25.66515" x2="92.1096" y2="25.66370625" width="0.225" layer="94" curve="4.363033"/>
<wire x1="92.04318125" y1="25.66956875" x2="92.07173125" y2="25.66514375" width="0.225" layer="94" curve="8.889144"/>
<wire x1="92.03463125" y1="25.672309375" x2="92.04318125" y2="25.66956875" width="0.225" layer="94" curve="9.043139"/>
<wire x1="92.03186875" y1="25.680859375" x2="92.034628125" y2="25.672309375" width="0.225" layer="94" curve="7.986668"/>
<wire x1="92.02805" y1="25.70145" x2="92.031871875" y2="25.680859375" width="0.225" layer="94" curve="6.761788"/>
<wire x1="92.02548125" y1="25.73128125" x2="92.028053125" y2="25.70145" width="0.225" layer="94" curve="4.406702"/>
<wire x1="92.024059375" y1="25.79115" x2="92.025484375" y2="25.73128125" width="0.225" layer="94" curve="2.724283"/>
<wire x1="92.024059375" y1="25.79115" x2="92.024059375" y2="25.964" width="0.225" layer="94"/>
<wire x1="92.024059375" y1="25.964" x2="92.024059375" y2="25.99651875" width="0.225" layer="94"/>
<wire x1="92.024059375" y1="25.99651875" x2="91.60701875" y2="25.99651875" width="0.225" layer="94"/>
<wire x1="91.60701875" y1="25.99651875" x2="91.60701875" y2="25.964" width="0.225" layer="94"/>
<wire x1="91.60701875" y1="25.964" x2="91.60701875" y2="25.739" width="0.225" layer="94"/>
<wire x1="91.60701875" y1="25.739" x2="91.60701875" y2="25.728159375" width="0.225" layer="94"/>
<wire x1="91.60701875" y1="25.728159375" x2="91.6277125" y2="25.53338125" width="0.225" layer="94" curve="12.128778"/>
<wire x1="91.627709375" y1="25.53338125" x2="91.72330625" y2="25.364828125" width="0.225" layer="94" curve="34.863434"/>
<wire x1="91.723309375" y1="25.36483125" x2="91.85098125" y2="25.289" width="0.225" layer="94" curve="24.602823"/>
<wire x1="91.85098125" y1="25.289" x2="91.896490625" y2="25.277203125" width="0.225" layer="94" curve="7.735491"/>
<wire x1="91.896490625" y1="25.2772" x2="92.091590625" y2="25.258534375" width="0.225" layer="94" curve="10.40831"/>
<wire x1="92.091590625" y1="25.25853125" x2="92.262609375" y2="25.25853125" width="0.225" layer="94"/>
<wire x1="92.262609375" y1="25.25853125" x2="92.457790625" y2="25.2805875" width="0.225" layer="94" curve="15.067174"/>
<wire x1="92.457790625" y1="25.280590625" x2="92.633140625" y2="25.36668125" width="0.225" layer="94" curve="24.336812"/>
<wire x1="92.633140625" y1="25.36668125" x2="92.74113125" y2="25.514" width="0.225" layer="94" curve="30.878359"/>
<wire x1="92.74113125" y1="25.514" x2="92.74536875" y2="25.525809375" width="0.225" layer="94" curve="2.095521"/>
<wire x1="92.74536875" y1="25.525809375" x2="92.777203125" y2="25.71908125" width="0.225" layer="94" curve="18.707757"/>
<wire x1="92.777209375" y1="25.71908125" x2="92.777209375" y2="25.739" width="0.225" layer="94"/>
<wire x1="92.777209375" y1="25.739" x2="92.777209375" y2="25.92328125" width="0.225" layer="94"/>
<wire x1="92.777209375" y1="25.92328125" x2="92.756978125" y2="26.083621875" width="0.225" layer="94" curve="14.383055"/>
<wire x1="92.75696875" y1="26.08361875" x2="92.6796125" y2="26.22454375" width="0.225" layer="94" curve="28.759193"/>
<wire x1="72.94583125" y1="31.589" x2="72.950859375" y2="31.589" width="0.225" layer="94"/>
<wire x1="73.958459375" y1="26.639" x2="74.19638125" y2="26.639" width="0.225" layer="94"/>
<wire x1="92.06346875" y1="26.639" x2="92.334359375" y2="26.639" width="0.225" layer="94"/>
<wire x1="77.24345" y1="26.639" x2="77.48136875" y2="26.639" width="0.225" layer="94"/>
<wire x1="86.913890625" y1="26.639" x2="87.09276875" y2="26.639" width="0.225" layer="94"/>
<wire x1="82.99755" y1="26.639" x2="83.160159375" y2="26.639" width="0.225" layer="94"/>
<wire x1="92.0661" y1="26.414" x2="92.34215" y2="26.414" width="0.225" layer="94"/>
<wire x1="82.92926875" y1="26.414" x2="83.18028125" y2="26.414" width="0.225" layer="94"/>
<wire x1="73.88948125" y1="25.964" x2="74.27638125" y2="25.964" width="0.225" layer="94"/>
<wire x1="92.024059375" y1="25.964" x2="92.27126875" y2="25.964" width="0.225" layer="94"/>
<wire x1="77.17446875" y1="25.964" x2="77.561359375" y2="25.964" width="0.225" layer="94"/>
<wire x1="81.206190625" y1="25.964" x2="81.341240625" y2="25.964" width="0.225" layer="94"/>
<wire x1="73.90876875" y1="25.739" x2="74.2678" y2="25.739" width="0.225" layer="94"/>
<wire x1="92.025140625" y1="25.739" x2="92.34813125" y2="25.739" width="0.225" layer="94"/>
<wire x1="77.193759375" y1="25.739" x2="77.55278125" y2="25.739" width="0.225" layer="94"/>
<wire x1="81.209909375" y1="25.739" x2="81.340009375" y2="25.739" width="0.225" layer="94"/>
<rectangle x1="93.287040625" y1="29.45076875" x2="93.399540625" y2="33.34871875" layer="94"/>
<rectangle x1="94.749840625" y1="29.45076875" x2="94.862340625" y2="33.34871875" layer="94"/>
<rectangle x1="73.776721875" y1="26.310290625" x2="74.376859375" y2="26.422790625" layer="94"/>
<rectangle x1="77.0617" y1="26.310290625" x2="77.66185" y2="26.422790625" layer="94"/>
<rectangle x1="73.77671875" y1="26.00881875" x2="74.685" y2="26.12131875" layer="94"/>
<rectangle x1="77.0617" y1="26.00881875" x2="77.985" y2="26.12131875" layer="94"/>
<text x="50.8" y="12.7" size="5.08" layer="94" ratio="10" align="bottom-center">&gt;VALUE</text>
<text x="1.27" y="33.02" size="1.9304" layer="94">Vehicle Name:</text>
<text x="41.91" y="30.48" size="5.08" layer="94" align="top-center">&gt;VEHICLE_NUMBER</text>
<text x="41.91" y="33.02" size="2.54" layer="94" align="bottom-center">&gt;VEHICLE_NAME</text>
<text x="1.27" y="21.59" size="1.9304" layer="94" align="top-left">Sheet Name:</text>
<text x="1.27" y="24.13" size="1.9304" layer="94">ABRA:</text>
<text x="10.16" y="24.13" size="1.9304" layer="94">&gt;ABRA_GLOBAL</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="DINA3_L-AUTO" prefix="FRAME" uservalue="yes">
<description>&lt;b&gt;VEIT FRAME&lt;/b&gt;&lt;p&gt;
DIN A3, landscape with extra doc field,&lt;br&gt;
customized for vehicle schematics</description>
<gates>
<gate name="G$1" symbol="DINA3_L" x="0" y="0"/>
<gate name="G$2" symbol="DOCFIELD-AUTA" x="287.02" y="0" addlevel="request"/>
<gate name="G$3" symbol="DOCFIELD-AUTA-ECO-LOGO" x="388.62" y="0" addlevel="request"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="AUTO-schema">
<packages>
</packages>
<symbols>
<symbol name="PRUCHODKA">
<wire x1="0.635" y1="0.635" x2="0.9525" y2="0.635" width="0.254" layer="94"/>
<wire x1="0.9525" y1="0.635" x2="1.27" y2="0.635" width="0.254" layer="94"/>
<wire x1="-0.635" y1="-0.635" x2="0.635" y2="-0.635" width="0.254" layer="94"/>
<wire x1="0.635" y1="-0.635" x2="0.9525" y2="-0.635" width="0.254" layer="94"/>
<wire x1="0.9525" y1="-0.635" x2="1.27" y2="-0.635" width="0.254" layer="94"/>
<wire x1="0.635" y1="-1.27" x2="1.27" y2="-0.635" width="0.254" layer="94" curve="90"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.254" layer="94" curve="-90"/>
<wire x1="0.635" y1="0.635" x2="-0.635" y2="0.635" width="0.254" layer="94"/>
<wire x1="-0.635" y1="0.635" x2="-0.635" y2="1.27" width="0.254" layer="94"/>
<wire x1="0.635" y1="0.635" x2="0.635" y2="0.9525" width="0.254" layer="94"/>
<wire x1="0.635" y1="0.9525" x2="0.635" y2="1.27" width="0.254" layer="94"/>
<wire x1="0.635" y1="-1.27" x2="0.635" y2="-0.9525" width="0.254" layer="94"/>
<wire x1="0.635" y1="-0.9525" x2="0.635" y2="-0.635" width="0.254" layer="94"/>
<wire x1="-0.635" y1="-1.27" x2="-0.635" y2="-0.635" width="0.254" layer="94"/>
<wire x1="0.635" y1="-0.9525" x2="0.9525" y2="-0.635" width="0.254" layer="94"/>
<wire x1="0.635" y1="-1.27" x2="1.27" y2="-0.635" width="0.254" layer="94"/>
<wire x1="1.27" y1="0.635" x2="0.635" y2="1.27" width="0.254" layer="94"/>
<wire x1="0.9525" y1="0.635" x2="0.635" y2="0.9525" width="0.254" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PRUCHODKA" prefix="PRUCHODKA">
<gates>
<gate name="G$1" symbol="PRUCHODKA" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="CMI">
<description>Project library CMI</description>
<packages>
<package name="1X01">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="1.27" x2="1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="0.635" x2="1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="1.27" y1="-0.635" x2="0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-1.27" y1="-0.635" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-1.27" x2="-0.635" y2="-1.27" width="0.1524" layer="21"/>
<pad name="1" x="0" y="0" drill="1.016" shape="octagon"/>
<text x="-1.3462" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.27" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="JISTIC_DUMMY">
<wire x1="-2.54" y1="0" x2="3.175" y2="1.016" width="0.254" layer="94"/>
<wire x1="-2.54" y1="0" x2="-5.08" y2="0" width="0.1524" layer="94"/>
<circle x="-2.54" y="0" radius="0.254" width="0.508" layer="94"/>
<wire x1="5.08" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<text x="0" y="2.54" size="1.778" layer="94" align="bottom-center">&gt;VALUE</text>
<wire x1="1.016" y1="0.635" x2="0.889" y2="1.397" width="0.254" layer="94"/>
<wire x1="2.413" y1="0.889" x2="2.286" y2="1.651" width="0.254" layer="94"/>
<wire x1="0.889" y1="1.397" x2="2.286" y2="1.651" width="0.254" layer="94"/>
</symbol>
<symbol name="PINHD1">
<wire x1="-6.35" y1="-2.54" x2="1.27" y2="-2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="-2.54" x2="1.27" y2="2.54" width="0.4064" layer="94"/>
<wire x1="1.27" y1="2.54" x2="-6.35" y2="2.54" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="2.54" x2="-6.35" y2="-2.54" width="0.4064" layer="94"/>
<text x="2.54" y="0" size="1.778" layer="96" align="center-left">&gt;VALUE</text>
<pin name="1" x="-2.54" y="0" visible="off" length="short" direction="pas" function="dot"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="JISTIC_DUMMY" uservalue="yes">
<gates>
<gate name="G$1" symbol="JISTIC_DUMMY" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="PINHD-1X1" prefix="JP" uservalue="yes">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="G$1" symbol="PINHD1" x="0" y="0"/>
</gates>
<devices>
<device name="" package="1X01">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="SUPPLY2">
<packages>
</packages>
<symbols>
<symbol name="GND">
<wire x1="-1.27" y1="2.54" x2="1.27" y2="2.54" width="0.254" layer="94"/>
<wire x1="1.27" y1="2.54" x2="0" y2="1.27" width="0.254" layer="94"/>
<wire x1="0" y1="1.27" x2="-1.27" y2="2.54" width="0.254" layer="94"/>
<text x="-1.905" y="-0.635" size="1.524" layer="94">GND</text>
<pin name="GND" x="0" y="5.08" visible="off" length="short" direction="sup" rot="R270"/>
</symbol>
<symbol name="+24V">
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="0.635" x2="0" y2="1.905" width="0.1524" layer="94"/>
<circle x="0" y="1.27" radius="1.27" width="0.254" layer="94"/>
<text x="-2.54" y="3.175" size="1.778" layer="96">&gt;VALUE</text>
<pin name="+24V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="AKU">
<wire x1="-0.635" y1="3.81" x2="0.635" y2="3.81" width="0.1524" layer="94"/>
<wire x1="0" y1="3.175" x2="0" y2="4.445" width="0.1524" layer="94"/>
<circle x="0" y="3.81" radius="1.27" width="0.254" layer="94"/>
<text x="-1.778" y="5.715" size="1.524" layer="94">AKU</text>
<pin name="AKU" x="0" y="0" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
<symbol name="V--&gt;">
<wire x1="15.24" y1="0" x2="13.97" y2="1.27" width="0.1524" layer="94"/>
<wire x1="13.97" y1="1.27" x2="0" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="1.27" x2="0" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="-1.27" x2="13.97" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="15.24" y1="0" x2="13.97" y2="-1.27" width="0.1524" layer="94"/>
<text x="1.27" y="-0.762" size="1.524" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="+24V@2">
<wire x1="-0.635" y1="1.27" x2="0.635" y2="1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="0.635" x2="0" y2="1.905" width="0.1524" layer="94"/>
<circle x="0" y="1.27" radius="1.27" width="0.254" layer="94"/>
<text x="-2.54" y="3.175" size="1.524" layer="94">+24V</text>
<pin name="+24V" x="0" y="-2.54" visible="off" length="short" direction="sup" rot="R90"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="GND">
<gates>
<gate name="GND" symbol="GND" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+24V" prefix="SUPPLY">
<description>&lt;b&gt;SUPPLY SYMBOL&lt;/b&gt;</description>
<gates>
<gate name="+24V" symbol="+24V" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="AKU">
<gates>
<gate name="G$1" symbol="AKU" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="V--&gt;" prefix="V" uservalue="yes">
<gates>
<gate name="G$1" symbol="V--&gt;" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="+24V@2">
<gates>
<gate name="+24V" symbol="+24V@2" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="Svorkovnice-3patra">
<packages>
</packages>
<symbols>
<symbol name="3SVORKA-00">
<circle x="2.54" y="-7.62" radius="0.762" width="0.1524" layer="94"/>
<circle x="0" y="-5.08" radius="0.762" width="0.1524" layer="94"/>
<circle x="-2.54" y="-2.54" radius="0.762" width="0.1524" layer="94"/>
<circle x="2.54" y="7.62" radius="0.762" width="0.1524" layer="94"/>
<circle x="0" y="5.08" radius="0.762" width="0.1524" layer="94"/>
<circle x="-2.54" y="2.54" radius="0.762" width="0.1524" layer="94"/>
<pin name="1" x="2.54" y="10.16" visible="off" length="point" direction="pas" rot="R270"/>
<pin name="2" x="0" y="7.62" visible="off" length="point" direction="pas" rot="R270"/>
<pin name="3" x="-2.54" y="5.08" visible="off" length="point" direction="pas" rot="R270"/>
<pin name="4" x="-2.54" y="-5.08" visible="off" length="point" direction="pas" rot="R90"/>
<pin name="5" x="0" y="-7.62" visible="off" length="point" direction="pas" rot="R90"/>
<pin name="6" x="2.54" y="-10.16" visible="off" length="point" direction="pas" rot="R90"/>
<wire x1="2.54" y1="10.16" x2="2.54" y2="8.382" width="0.1524" layer="94"/>
<wire x1="0" y1="7.62" x2="0" y2="5.842" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="3.302" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-10.16" x2="2.54" y2="-8.382" width="0.1524" layer="94"/>
<wire x1="0" y1="-7.62" x2="0" y2="-5.842" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-2.54" y2="-3.302" width="0.1524" layer="94"/>
<text x="2.54" y="7.62" size="1.016" layer="94" rot="MR270" align="center">1</text>
<text x="0" y="5.08" size="1.016" layer="94" rot="MR270" align="center">2</text>
<text x="-2.54" y="2.54" size="1.016" layer="94" rot="MR270" align="center">3</text>
<text x="-2.54" y="-2.54" size="1.016" layer="94" rot="MR270" align="center">4</text>
<text x="0" y="-5.08" size="1.016" layer="94" rot="MR270" align="center">5</text>
<text x="2.54" y="-7.62" size="1.016" layer="94" rot="MR270" align="center">6</text>
<wire x1="-3.81" y1="3.81" x2="-1.27" y2="4.445" width="0.3048" layer="94"/>
<wire x1="-1.27" y1="4.445" x2="-1.27" y2="6.35" width="0.3048" layer="94"/>
<wire x1="-1.27" y1="6.35" x2="1.27" y2="6.985" width="0.3048" layer="94"/>
<wire x1="1.27" y1="6.985" x2="1.27" y2="8.89" width="0.3048" layer="94"/>
<wire x1="1.27" y1="8.89" x2="3.81" y2="9.525" width="0.3048" layer="94"/>
<wire x1="3.81" y1="9.525" x2="3.81" y2="-9.525" width="0.3048" layer="94"/>
<wire x1="3.81" y1="-9.525" x2="1.27" y2="-8.89" width="0.3048" layer="94"/>
<wire x1="1.27" y1="-8.89" x2="1.27" y2="-6.985" width="0.3048" layer="94"/>
<wire x1="1.27" y1="-6.985" x2="-1.27" y2="-6.35" width="0.3048" layer="94"/>
<wire x1="-1.27" y1="-6.35" x2="-1.27" y2="-4.445" width="0.3048" layer="94"/>
<wire x1="-1.27" y1="-4.445" x2="-3.81" y2="-3.81" width="0.3048" layer="94"/>
<wire x1="-3.81" y1="-3.81" x2="-3.81" y2="3.81" width="0.3048" layer="94"/>
<text x="2.286" y="0" size="1.778" layer="96" rot="R90" align="center">&gt;VALUE</text>
<wire x1="-2.54" y1="-1.778" x2="-2.54" y2="1.778" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-4.318" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="4.318" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="1.27" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-6.858" x2="2.54" y2="-3.175" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-3.175" x2="0" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="0" y1="-0.635" x2="0" y2="0.635" width="0.1524" layer="94"/>
<wire x1="0" y1="0.635" x2="2.54" y2="3.175" width="0.1524" layer="94"/>
<wire x1="2.54" y1="3.175" x2="2.54" y2="6.858" width="0.1524" layer="94"/>
</symbol>
<symbol name="3SVORKA-00-SHADOW-XREF">
<circle x="2.54" y="-7.62" radius="0.762" width="0.1524" layer="97"/>
<circle x="0" y="-5.08" radius="0.762" width="0.1524" layer="97"/>
<circle x="-2.54" y="-2.54" radius="0.762" width="0.1524" layer="97"/>
<circle x="2.54" y="7.62" radius="0.762" width="0.1524" layer="97"/>
<circle x="0" y="5.08" radius="0.762" width="0.1524" layer="97"/>
<circle x="-2.54" y="2.54" radius="0.762" width="0.1524" layer="97"/>
<text x="2.54" y="7.62" size="1.016" layer="97" rot="MR270" align="center">1</text>
<text x="0" y="5.08" size="1.016" layer="97" rot="MR270" align="center">2</text>
<text x="-2.54" y="2.54" size="1.016" layer="97" rot="MR270" align="center">3</text>
<text x="-2.54" y="-2.54" size="1.016" layer="97" rot="MR270" align="center">4</text>
<text x="0" y="-5.08" size="1.016" layer="97" rot="MR270" align="center">5</text>
<text x="2.54" y="-7.62" size="1.016" layer="97" rot="MR270" align="center">6</text>
<wire x1="-3.81" y1="3.81" x2="-1.27" y2="4.445" width="0.3048" layer="97" style="shortdash"/>
<wire x1="-1.27" y1="4.445" x2="-1.27" y2="6.35" width="0.3048" layer="97" style="shortdash"/>
<wire x1="-1.27" y1="6.35" x2="1.27" y2="6.985" width="0.3048" layer="97" style="shortdash"/>
<wire x1="1.27" y1="6.985" x2="1.27" y2="8.89" width="0.3048" layer="97" style="shortdash"/>
<wire x1="1.27" y1="8.89" x2="3.81" y2="9.525" width="0.3048" layer="97" style="shortdash"/>
<wire x1="3.81" y1="9.525" x2="3.81" y2="-9.525" width="0.3048" layer="97" style="shortdash"/>
<wire x1="3.81" y1="-9.525" x2="1.27" y2="-8.89" width="0.3048" layer="97" style="shortdash"/>
<wire x1="1.27" y1="-8.89" x2="1.27" y2="-6.985" width="0.3048" layer="97" style="shortdash"/>
<wire x1="1.27" y1="-6.985" x2="-1.27" y2="-6.35" width="0.3048" layer="97" style="shortdash"/>
<wire x1="-1.27" y1="-6.35" x2="-1.27" y2="-4.445" width="0.3048" layer="97" style="shortdash"/>
<wire x1="-1.27" y1="-4.445" x2="-3.81" y2="-3.81" width="0.3048" layer="97" style="shortdash"/>
<wire x1="-3.81" y1="-3.81" x2="-3.81" y2="3.81" width="0.3048" layer="97" style="shortdash"/>
<text x="2.286" y="0" size="1.778" layer="96" rot="R90" align="center">&gt;VALUE</text>
<text x="0" y="26.67" size="1.778" layer="94" rot="R90">&gt;XREF</text>
<text x="0" y="25.4" size="1.778" layer="94" rot="R90" align="bottom-right">Použito na listu:</text>
</symbol>
<symbol name="3SVORKA-01">
<circle x="2.54" y="-7.62" radius="0.762" width="0.1524" layer="94"/>
<circle x="0" y="-5.08" radius="0.762" width="0.1524" layer="94"/>
<circle x="-2.54" y="-2.54" radius="0.762" width="0.1524" layer="94"/>
<circle x="2.54" y="7.62" radius="0.762" width="0.1524" layer="94"/>
<circle x="0" y="5.08" radius="0.762" width="0.1524" layer="94"/>
<circle x="-2.54" y="2.54" radius="0.762" width="0.1524" layer="94"/>
<pin name="1" x="2.54" y="10.16" visible="off" length="point" direction="pas" rot="R270"/>
<pin name="2" x="0" y="7.62" visible="off" length="point" direction="pas" rot="R270"/>
<pin name="3" x="-2.54" y="5.08" visible="off" length="point" direction="pas" rot="R270"/>
<pin name="4" x="-2.54" y="-5.08" visible="off" length="point" direction="pas" rot="R90"/>
<pin name="5" x="0" y="-7.62" visible="off" length="point" direction="pas" rot="R90"/>
<pin name="6" x="2.54" y="-10.16" visible="off" length="point" direction="pas" rot="R90"/>
<wire x1="2.54" y1="10.16" x2="2.54" y2="8.382" width="0.1524" layer="94"/>
<wire x1="0" y1="7.62" x2="0" y2="5.842" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="3.302" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-10.16" x2="2.54" y2="-8.382" width="0.1524" layer="94"/>
<wire x1="0" y1="-7.62" x2="0" y2="-5.842" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-2.54" y2="-3.302" width="0.1524" layer="94"/>
<text x="2.54" y="7.62" size="1.016" layer="94" rot="MR270" align="center">1</text>
<text x="0" y="5.08" size="1.016" layer="94" rot="MR270" align="center">2</text>
<text x="-2.54" y="2.54" size="1.016" layer="94" rot="MR270" align="center">3</text>
<text x="-2.54" y="-2.54" size="1.016" layer="94" rot="MR270" align="center">4</text>
<text x="0" y="-5.08" size="1.016" layer="94" rot="MR270" align="center">5</text>
<text x="2.54" y="-7.62" size="1.016" layer="94" rot="MR270" align="center">6</text>
<wire x1="-3.81" y1="3.81" x2="-1.27" y2="4.445" width="0.3048" layer="94"/>
<wire x1="-1.27" y1="4.445" x2="-1.27" y2="6.35" width="0.3048" layer="94"/>
<wire x1="-1.27" y1="6.35" x2="1.27" y2="6.985" width="0.3048" layer="94"/>
<wire x1="1.27" y1="6.985" x2="1.27" y2="8.89" width="0.3048" layer="94"/>
<wire x1="1.27" y1="8.89" x2="3.81" y2="9.525" width="0.3048" layer="94"/>
<wire x1="3.81" y1="9.525" x2="3.81" y2="-9.525" width="0.3048" layer="94"/>
<wire x1="3.81" y1="-9.525" x2="1.27" y2="-8.89" width="0.3048" layer="94"/>
<wire x1="1.27" y1="-8.89" x2="1.27" y2="-6.985" width="0.3048" layer="94"/>
<wire x1="1.27" y1="-6.985" x2="-1.27" y2="-6.35" width="0.3048" layer="94"/>
<wire x1="-1.27" y1="-6.35" x2="-1.27" y2="-4.445" width="0.3048" layer="94"/>
<wire x1="-1.27" y1="-4.445" x2="-3.81" y2="-3.81" width="0.3048" layer="94"/>
<wire x1="-3.81" y1="-3.81" x2="-3.81" y2="3.81" width="0.3048" layer="94"/>
<text x="2.286" y="0" size="1.778" layer="96" rot="R90" align="center">&gt;VALUE</text>
<wire x1="-2.54" y1="-1.778" x2="-2.54" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-0.635" x2="-2.54" y2="1.778" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-4.318" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="4.318" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="1.27" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="-0.635" x2="-1.27" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-6.858" x2="2.54" y2="-3.175" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-3.175" x2="0" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="0" y1="-0.635" x2="0" y2="0.635" width="0.1524" layer="94"/>
<wire x1="0" y1="0.635" x2="2.54" y2="3.175" width="0.1524" layer="94"/>
<wire x1="2.54" y1="3.175" x2="2.54" y2="6.858" width="0.1524" layer="94"/>
<wire x1="0" y1="0.635" x2="-1.27" y2="0.635" width="0.3048" layer="94"/>
<circle x="-1.27" y="0.635" radius="0.4064" width="0" layer="94"/>
<circle x="0" y="0.635" radius="0.4064" width="0" layer="94"/>
</symbol>
<symbol name="3SVORKA-11">
<circle x="2.54" y="-7.62" radius="0.762" width="0.1524" layer="94"/>
<circle x="0" y="-5.08" radius="0.762" width="0.1524" layer="94"/>
<circle x="-2.54" y="-2.54" radius="0.762" width="0.1524" layer="94"/>
<circle x="2.54" y="7.62" radius="0.762" width="0.1524" layer="94"/>
<circle x="0" y="5.08" radius="0.762" width="0.1524" layer="94"/>
<circle x="-2.54" y="2.54" radius="0.762" width="0.1524" layer="94"/>
<pin name="1" x="2.54" y="10.16" visible="off" length="point" direction="pas" rot="R270"/>
<pin name="2" x="0" y="7.62" visible="off" length="point" direction="pas" rot="R270"/>
<pin name="3" x="-2.54" y="5.08" visible="off" length="point" direction="pas" rot="R270"/>
<pin name="4" x="-2.54" y="-5.08" visible="off" length="point" direction="pas" rot="R90"/>
<pin name="5" x="0" y="-7.62" visible="off" length="point" direction="pas" rot="R90"/>
<pin name="6" x="2.54" y="-10.16" visible="off" length="point" direction="pas" rot="R90"/>
<wire x1="2.54" y1="10.16" x2="2.54" y2="8.382" width="0.1524" layer="94"/>
<wire x1="0" y1="7.62" x2="0" y2="5.842" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="3.302" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-10.16" x2="2.54" y2="-8.382" width="0.1524" layer="94"/>
<wire x1="0" y1="-7.62" x2="0" y2="-5.842" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-2.54" y2="-3.302" width="0.1524" layer="94"/>
<text x="2.54" y="7.62" size="1.016" layer="94" rot="MR270" align="center">1</text>
<text x="0" y="5.08" size="1.016" layer="94" rot="MR270" align="center">2</text>
<text x="-2.54" y="2.54" size="1.016" layer="94" rot="MR270" align="center">3</text>
<text x="-2.54" y="-2.54" size="1.016" layer="94" rot="MR270" align="center">4</text>
<text x="0" y="-5.08" size="1.016" layer="94" rot="MR270" align="center">5</text>
<text x="2.54" y="-7.62" size="1.016" layer="94" rot="MR270" align="center">6</text>
<wire x1="-3.81" y1="3.81" x2="-1.27" y2="4.445" width="0.3048" layer="94"/>
<wire x1="-1.27" y1="4.445" x2="-1.27" y2="6.35" width="0.3048" layer="94"/>
<wire x1="-1.27" y1="6.35" x2="1.27" y2="6.985" width="0.3048" layer="94"/>
<wire x1="1.27" y1="6.985" x2="1.27" y2="8.89" width="0.3048" layer="94"/>
<wire x1="1.27" y1="8.89" x2="3.81" y2="9.525" width="0.3048" layer="94"/>
<wire x1="3.81" y1="9.525" x2="3.81" y2="-9.525" width="0.3048" layer="94"/>
<wire x1="3.81" y1="-9.525" x2="1.27" y2="-8.89" width="0.3048" layer="94"/>
<wire x1="1.27" y1="-8.89" x2="1.27" y2="-6.985" width="0.3048" layer="94"/>
<wire x1="1.27" y1="-6.985" x2="-1.27" y2="-6.35" width="0.3048" layer="94"/>
<wire x1="-1.27" y1="-6.35" x2="-1.27" y2="-4.445" width="0.3048" layer="94"/>
<wire x1="-1.27" y1="-4.445" x2="-3.81" y2="-3.81" width="0.3048" layer="94"/>
<wire x1="-3.81" y1="-3.81" x2="-3.81" y2="3.81" width="0.3048" layer="94"/>
<text x="2.286" y="0" size="1.778" layer="96" rot="R90" align="center">&gt;VALUE</text>
<wire x1="-2.54" y1="-1.778" x2="-2.54" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-0.635" x2="-2.54" y2="1.778" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-4.318" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="4.318" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="1.27" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="-0.635" x2="-1.27" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-6.858" x2="2.54" y2="-3.175" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-3.175" x2="0" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="0" y1="-0.635" x2="0" y2="0.635" width="0.1524" layer="94"/>
<wire x1="0" y1="0.635" x2="2.54" y2="3.175" width="0.1524" layer="94"/>
<wire x1="2.54" y1="3.175" x2="2.54" y2="6.858" width="0.1524" layer="94"/>
<wire x1="0" y1="0.635" x2="-1.27" y2="0.635" width="0.3048" layer="94"/>
<wire x1="-1.27" y1="-0.635" x2="-2.54" y2="-0.635" width="0.3048" layer="94"/>
<circle x="-1.27" y="-0.635" radius="0.4064" width="0" layer="94"/>
<circle x="-2.54" y="-0.635" radius="0.4064" width="0" layer="94"/>
<circle x="-1.27" y="0.635" radius="0.4064" width="0" layer="94"/>
<circle x="0" y="0.635" radius="0.4064" width="0" layer="94"/>
</symbol>
<symbol name="3SVORKA-10">
<circle x="2.54" y="-7.62" radius="0.762" width="0.1524" layer="94"/>
<circle x="0" y="-5.08" radius="0.762" width="0.1524" layer="94"/>
<circle x="-2.54" y="-2.54" radius="0.762" width="0.1524" layer="94"/>
<circle x="2.54" y="7.62" radius="0.762" width="0.1524" layer="94"/>
<circle x="0" y="5.08" radius="0.762" width="0.1524" layer="94"/>
<circle x="-2.54" y="2.54" radius="0.762" width="0.1524" layer="94"/>
<pin name="1" x="2.54" y="10.16" visible="off" length="point" direction="pas" rot="R270"/>
<pin name="2" x="0" y="7.62" visible="off" length="point" direction="pas" rot="R270"/>
<pin name="3" x="-2.54" y="5.08" visible="off" length="point" direction="pas" rot="R270"/>
<pin name="4" x="-2.54" y="-5.08" visible="off" length="point" direction="pas" rot="R90"/>
<pin name="5" x="0" y="-7.62" visible="off" length="point" direction="pas" rot="R90"/>
<pin name="6" x="2.54" y="-10.16" visible="off" length="point" direction="pas" rot="R90"/>
<wire x1="2.54" y1="10.16" x2="2.54" y2="8.382" width="0.1524" layer="94"/>
<wire x1="0" y1="7.62" x2="0" y2="5.842" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="5.08" x2="-2.54" y2="3.302" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-10.16" x2="2.54" y2="-8.382" width="0.1524" layer="94"/>
<wire x1="0" y1="-7.62" x2="0" y2="-5.842" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-5.08" x2="-2.54" y2="-3.302" width="0.1524" layer="94"/>
<text x="2.54" y="7.62" size="1.016" layer="94" rot="MR270" align="center">1</text>
<text x="0" y="5.08" size="1.016" layer="94" rot="MR270" align="center">2</text>
<text x="-2.54" y="2.54" size="1.016" layer="94" rot="MR270" align="center">3</text>
<text x="-2.54" y="-2.54" size="1.016" layer="94" rot="MR270" align="center">4</text>
<text x="0" y="-5.08" size="1.016" layer="94" rot="MR270" align="center">5</text>
<text x="2.54" y="-7.62" size="1.016" layer="94" rot="MR270" align="center">6</text>
<wire x1="-3.81" y1="3.81" x2="-1.27" y2="4.445" width="0.3048" layer="94"/>
<wire x1="-1.27" y1="4.445" x2="-1.27" y2="6.35" width="0.3048" layer="94"/>
<wire x1="-1.27" y1="6.35" x2="1.27" y2="6.985" width="0.3048" layer="94"/>
<wire x1="1.27" y1="6.985" x2="1.27" y2="8.89" width="0.3048" layer="94"/>
<wire x1="1.27" y1="8.89" x2="3.81" y2="9.525" width="0.3048" layer="94"/>
<wire x1="3.81" y1="9.525" x2="3.81" y2="-9.525" width="0.3048" layer="94"/>
<wire x1="3.81" y1="-9.525" x2="1.27" y2="-8.89" width="0.3048" layer="94"/>
<wire x1="1.27" y1="-8.89" x2="1.27" y2="-6.985" width="0.3048" layer="94"/>
<wire x1="1.27" y1="-6.985" x2="-1.27" y2="-6.35" width="0.3048" layer="94"/>
<wire x1="-1.27" y1="-6.35" x2="-1.27" y2="-4.445" width="0.3048" layer="94"/>
<wire x1="-1.27" y1="-4.445" x2="-3.81" y2="-3.81" width="0.3048" layer="94"/>
<wire x1="-3.81" y1="-3.81" x2="-3.81" y2="3.81" width="0.3048" layer="94"/>
<text x="2.286" y="0" size="1.778" layer="96" rot="R90" align="center">&gt;VALUE</text>
<wire x1="-2.54" y1="-1.778" x2="-2.54" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-0.635" x2="-2.54" y2="1.778" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-4.318" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="4.318" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="-1.27" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="-1.27" y2="1.27" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="1.27" x2="-1.27" y2="0.635" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="0.635" x2="-1.27" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="-0.635" x2="-1.27" y2="-1.27" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-6.858" x2="2.54" y2="-3.175" width="0.1524" layer="94"/>
<wire x1="2.54" y1="-3.175" x2="0" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="0" y1="-0.635" x2="0" y2="0.635" width="0.1524" layer="94"/>
<wire x1="0" y1="0.635" x2="2.54" y2="3.175" width="0.1524" layer="94"/>
<wire x1="2.54" y1="3.175" x2="2.54" y2="6.858" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="-0.635" x2="-2.54" y2="-0.635" width="0.3048" layer="94"/>
<circle x="-1.27" y="-0.635" radius="0.4064" width="0" layer="94"/>
<circle x="-2.54" y="-0.635" radius="0.4064" width="0" layer="94"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="3SVORKA-00" prefix="SVORKA" uservalue="yes">
<description>3patrová svorkovnice - bez propojů&lt;P&gt;

Svorka řadová &lt;B&gt;ZDLD 2.5-2VN šedá&lt;/B&gt;</description>
<gates>
<gate name="G$1" symbol="3SVORKA-00" x="0" y="0" addlevel="must"/>
<gate name="G$2" symbol="3SVORKA-00-SHADOW-XREF" x="17.78" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name="">
<attribute name="ABRA" value="MELE1616"/>
<attribute name="MPN" value="1782320000"/>
<attribute name="_EXTERNAL_" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="3SVORKA-01" prefix="SVORKA" uservalue="yes">
<description>3patrová svorkovnice - střední propojena se spodní&lt;P&gt;

Svorka řadová &lt;B&gt;ZDLD 2.5-2VN šedá&lt;/B&gt;</description>
<gates>
<gate name="G$1" symbol="3SVORKA-01" x="0" y="0" addlevel="must"/>
<gate name="G$2" symbol="3SVORKA-00-SHADOW-XREF" x="17.78" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name="">
<attribute name="ABRA" value="MELE1616"/>
<attribute name="MPN" value="1782320000"/>
<attribute name="_EXTERNAL_" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="3SVORKA-11" prefix="SVORKA" uservalue="yes">
<description>3patrová svorkovnice - vše propojeno&lt;P&gt;

Svorka řadová &lt;B&gt;ZDLD 2.5-2VN šedá&lt;/B&gt;</description>
<gates>
<gate name="G$1" symbol="3SVORKA-11" x="0" y="0" addlevel="must"/>
<gate name="G$2" symbol="3SVORKA-00-SHADOW-XREF" x="17.78" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name="">
<attribute name="ABRA" value="MELE1616"/>
<attribute name="MPN" value="1782320000"/>
<attribute name="_EXTERNAL_" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="3SVORKA-10" prefix="SVORKA" uservalue="yes">
<description>3patrová svorkovnice - střední propojena s vrchní&lt;P&gt;

Svorka řadová &lt;B&gt;ZDLD 2.5-2VN šedá&lt;/B&gt;</description>
<gates>
<gate name="G$1" symbol="3SVORKA-10" x="0" y="0" addlevel="must"/>
<gate name="G$2" symbol="3SVORKA-00-SHADOW-XREF" x="17.78" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name="">
<attribute name="ABRA" value="MELE1616"/>
<attribute name="MPN" value="1782320000"/>
<attribute name="_EXTERNAL_" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="TMC">
<packages>
</packages>
<symbols>
<symbol name="TMC-PIN1">
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="NAZEV_PINU_1" x="33.02" y="0" visible="pin" length="point" direction="pas" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN2">
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="NAZEV_PINU_2" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN3">
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="NAZEV_PINU_3" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN4">
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="NAZEV_PINU_4" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN5">
<description>&lt;B&gt;DV8&lt;/B&gt; plamen topení 2; x,24V; open - tma, 24V - plamen - od verze 4 kabelu TMC není na tomto pinu vodič (přesunut na AV10)</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="FLAME2" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN6">
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="NAZEV_PINU_6" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN7">
<description>&lt;B&gt;DV7&lt;/B&gt; chod motoru vozidla; x,24V; open - stop, 24V - motor ON</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="D+" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN8">
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="NAZEV_PINU_8" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN9">
<description>&lt;B&gt;DV6&lt;/B&gt; porucha ventilátorů; x,24V; open - OK, GND - chyba vent.</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="FAILVENT" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN10">
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="NAZEV_PINU_10" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN11">
<description>&lt;B&gt;DV5&lt;/B&gt; plamen topení 1; x,24V; open - tma, 24V - plamen</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="FLAME1" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN12">
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="NAZEV_PINU_12" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN13">
<description>&lt;B&gt;DV4&lt;/B&gt; porucha topení a chlazení; x,24V; open - chyba, 24V - OK</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="FAILH/C" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN14">
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="NAZEV_PINU_14" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN15">
<description>&lt;B&gt;DV3&lt;/B&gt; přep. Jednotka/rozv.;  X,GND; open - nouzový přepínač, GND - ovládání skrz TMC</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="EMERGENCY" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN16">
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="NAZEV_PINU_16" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN17">
<description>&lt;B&gt;DV2&lt;/B&gt; diesel zapnuté zapalování; x,24V; open - Kubota OFF, 24V - Kubota ON</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="MOTOR" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN18">
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="NAZEV_PINU_18" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN19">
<description>&lt;B&gt;DV1&lt;/B&gt; 1.alternátor běží; x,24V; open - chyba, 24V - OK</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="ALT" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN20">
<description>&lt;B&gt;AVY5&lt;/B&gt; Ovládání serv přední; 2-10V</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="CONFLOOR" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN21">
<description>&lt;B&gt;AV9&lt;/B&gt; tlak filtry; 1-10V</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="PRESSFILTER" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN22">
<description>&lt;B&gt;AVY3+4&lt;/B&gt; Ovládání serva topení; 2-10V</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="CONHEAT" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN23">
<description>&lt;B&gt;AV10&lt;/B&gt; dezinfekce připravena; 1-10V; pokud U je pod 2V=dezinfekce není nainstalována (není v menu),
pokud dezinfekce je tak U je nad 6V= LOGO ready, následně U&lt;6V=konec cyklu.</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="DIS_READY" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN24">
<description>&lt;B&gt;AVY1+2&lt;/B&gt; Ovládání serv nápor,rec.; 2-10V</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="CONI_R" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN25">
<description>&lt;B&gt;AV12&lt;/B&gt; Výstup serva topení; 2-10V</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="CHKHEAT" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN26">
<description>&lt;B&gt;DVY1&lt;/B&gt; Napájení dezinfekce</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="LOGO_ON" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN27">
<description>&lt;B&gt;AV13&lt;/B&gt; Výstup serva nápor 1; 2-10V</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="CHKI1" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN28">
<description>&lt;B&gt;DVY2&lt;/B&gt; body disinfection</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="BODYDIS" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN29">
<description>&lt;B&gt;AV14&lt;/B&gt; Výstup serva nápor 2; 2-10V</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="CHKI2" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN30">
<description>&lt;B&gt;DVY3&lt;/B&gt; wheel disinfection</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="WHEELDIS" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN31">
<description>&lt;B&gt;AV15&lt;/B&gt; Výstup serva recirkulace; 2-10V</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="CHKREC" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN32">
<description>&lt;B&gt;DVY4&lt;/B&gt; Houkačka/ALERT, ovládání hlasité sirény GND - aktivní</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="ALERT" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN33">
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="NAZEV_PINU_33" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN34">
<description>&lt;B&gt;DVY5&lt;/B&gt; Zapínání chlazení</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="COOL" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN35">
<description>&lt;B&gt;AV1&lt;/B&gt; tlak sání chlazení; 0-10V; 20°C=4,7BAR=8,5V</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="LP" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN36">
<description>&lt;B&gt;DVY6&lt;/B&gt; Zapínání topení</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="HEAT" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN37">
<description>&lt;B&gt;AV2&lt;/B&gt; kontrola serva podlaha; 2-10V</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="CHKFLOOR" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN38">
<description>&lt;B&gt;DVY7&lt;/B&gt; Zapínání ventilace</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="VENTILATION" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN39">
<description>&lt;B&gt;AV3&lt;/B&gt; tlak výtlak chlazení; 0-10V; 20°C=4,7BAR=3,5V</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="HP" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN40">
<description>&lt;B&gt;DVY8&lt;/B&gt; ovládní čerpadla topení</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="PUMP" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN41">
<description>&lt;B&gt;AV4&lt;/B&gt; teplota vody v topení; 0-10V</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="HEATTEMP" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN42">
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="NAZEV_PINU_42" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN43">
<description>&lt;B&gt;AV5&lt;/B&gt; čidlo CO2; 0-10V</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="CO2" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN44">
<description>Čidlo teploty T6</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="T6" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN45">
<description>&lt;B&gt;AV6&lt;/B&gt; čidlo vlhkosti; 0-10V</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="HUM" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN46">
<description>Čidlo teploty T5</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="T5" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN47">
<description>&lt;B&gt;AV7&lt;/B&gt; Teplota dieselu; 1-10V; bargraf teploty motoru (není zobrazen pokud MOTOR DV2 v "0"), &gt;9,5V= plný bliká, 9,5 - 4V prázdný (60oC), 3V - dva dílky z 10, 2V - 5 z 10, 1V - 8 z 10, 0,5V plný, &lt;0,4V plný bliká, 0V - nezobrazuje se.</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="MTEMP" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN48">
<description>Čidlo teploty T4</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="T4" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN49">
<description>&lt;B&gt;AV8&lt;/B&gt; Výkon ventilace; 0-24V</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="FANCONTROL" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN50">
<description>Čidlo teploty T3</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="T3" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN51">
<description>Rx</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="RX" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN52">
<description>Čidlo teploty T2</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="T2" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN53">
<description>Tx</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="TX" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN54">
<description>Čidlo teploty T1</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="T1" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN55">
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="NAZEV_PINU_55" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN56">
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="NAZEV_PINU_56" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN57">
<description>GND @ 57</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="GND" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN58">
<description>GND @ 58</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="GND" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN59">
<description>POWERTMC @ 59</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="POWERTMC" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
<symbol name="TMC-PIN60">
<description>POWERTMC @ 60</description>
<text x="2.54" y="0" size="1.4224" layer="94" align="center">&gt;VALUE</text>
<text x="5.715" y="0" size="1.4224" layer="94" align="center-left">Pin:</text>
<wire x1="5.08" y1="-1.27" x2="5.08" y2="1.27" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-1.27" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="1.27" x2="5.08" y2="1.27" width="0.3048" layer="94"/>
<wire x1="5.08" y1="1.27" x2="12.7" y2="1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="1.27" x2="31.75" y2="1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="1.27" x2="31.75" y2="0" width="0.3048" layer="94"/>
<wire x1="31.75" y1="0" x2="31.75" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="31.75" y1="-1.27" x2="12.7" y2="-1.27" width="0.3048" layer="94"/>
<pin name="POWERTMC" x="33.02" y="0" visible="pin" length="point" direction="sup" rot="R180"/>
<wire x1="12.7" y1="-1.27" x2="5.08" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-1.27" x2="12.7" y2="1.27" width="0.1524" layer="94"/>
<wire x1="33.02" y1="0" x2="31.75" y2="0" width="0.1524" layer="94"/>
<text x="12.065" y="0" size="1.4224" layer="96" align="center-right">&gt;GATE</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="TMC" prefix="TMC">
<description>&lt;B&gt; TMC &lt;/B&gt; piny</description>
<gates>
<gate name="1" symbol="TMC-PIN1" x="0" y="0" addlevel="can"/>
<gate name="2" symbol="TMC-PIN2" x="0" y="-2.54" addlevel="can"/>
<gate name="3" symbol="TMC-PIN3" x="0" y="-5.08" addlevel="can"/>
<gate name="4" symbol="TMC-PIN4" x="0" y="-7.62" addlevel="can"/>
<gate name="5" symbol="TMC-PIN5" x="0" y="-10.16" addlevel="can"/>
<gate name="6" symbol="TMC-PIN6" x="0" y="-12.7" addlevel="can"/>
<gate name="7" symbol="TMC-PIN7" x="0" y="-15.24" addlevel="can"/>
<gate name="8" symbol="TMC-PIN8" x="0" y="-17.78" addlevel="can"/>
<gate name="9" symbol="TMC-PIN9" x="0" y="-20.32" addlevel="can"/>
<gate name="10" symbol="TMC-PIN10" x="0" y="-22.86" addlevel="can"/>
<gate name="11" symbol="TMC-PIN11" x="0" y="-25.4" addlevel="can"/>
<gate name="12" symbol="TMC-PIN12" x="0" y="-27.94" addlevel="can"/>
<gate name="13" symbol="TMC-PIN13" x="0" y="-30.48" addlevel="can"/>
<gate name="14" symbol="TMC-PIN14" x="0" y="-33.02" addlevel="can"/>
<gate name="15" symbol="TMC-PIN15" x="0" y="-35.56" addlevel="can"/>
<gate name="16" symbol="TMC-PIN16" x="0" y="-38.1" addlevel="can"/>
<gate name="17" symbol="TMC-PIN17" x="0" y="-40.64" addlevel="can"/>
<gate name="18" symbol="TMC-PIN18" x="0" y="-43.18" addlevel="can"/>
<gate name="19" symbol="TMC-PIN19" x="0" y="-45.72" addlevel="can"/>
<gate name="20" symbol="TMC-PIN20" x="0" y="-48.26" addlevel="can"/>
<gate name="21" symbol="TMC-PIN21" x="0" y="-50.8" addlevel="can"/>
<gate name="22" symbol="TMC-PIN22" x="0" y="-53.34" addlevel="can"/>
<gate name="23" symbol="TMC-PIN23" x="0" y="-55.88" addlevel="can"/>
<gate name="24" symbol="TMC-PIN24" x="0" y="-58.42" addlevel="can"/>
<gate name="25" symbol="TMC-PIN25" x="0" y="-60.96" addlevel="can"/>
<gate name="26" symbol="TMC-PIN26" x="0" y="-63.5" addlevel="can"/>
<gate name="27" symbol="TMC-PIN27" x="0" y="-66.04" addlevel="can"/>
<gate name="28" symbol="TMC-PIN28" x="0" y="-68.58" addlevel="can"/>
<gate name="29" symbol="TMC-PIN29" x="0" y="-71.12" addlevel="can"/>
<gate name="30" symbol="TMC-PIN30" x="0" y="-73.66" addlevel="can"/>
<gate name="31" symbol="TMC-PIN31" x="0" y="-76.2" addlevel="can"/>
<gate name="32" symbol="TMC-PIN32" x="0" y="-78.74" addlevel="can"/>
<gate name="33" symbol="TMC-PIN33" x="0" y="-81.28" addlevel="can"/>
<gate name="34" symbol="TMC-PIN34" x="0" y="-83.82" addlevel="can"/>
<gate name="35" symbol="TMC-PIN35" x="0" y="-86.36" addlevel="can"/>
<gate name="36" symbol="TMC-PIN36" x="0" y="-88.9" addlevel="can"/>
<gate name="37" symbol="TMC-PIN37" x="0" y="-91.44" addlevel="can"/>
<gate name="38" symbol="TMC-PIN38" x="0" y="-93.98" addlevel="can"/>
<gate name="39" symbol="TMC-PIN39" x="0" y="-96.52" addlevel="can"/>
<gate name="40" symbol="TMC-PIN40" x="0" y="-99.06" addlevel="can"/>
<gate name="41" symbol="TMC-PIN41" x="0" y="-101.6" addlevel="can"/>
<gate name="42" symbol="TMC-PIN42" x="0" y="-104.14" addlevel="can"/>
<gate name="43" symbol="TMC-PIN43" x="0" y="-106.68" addlevel="can"/>
<gate name="44" symbol="TMC-PIN44" x="0" y="-109.22" addlevel="can"/>
<gate name="45" symbol="TMC-PIN45" x="0" y="-111.76" addlevel="can"/>
<gate name="46" symbol="TMC-PIN46" x="0" y="-114.3" addlevel="can"/>
<gate name="47" symbol="TMC-PIN47" x="0" y="-116.84" addlevel="can"/>
<gate name="48" symbol="TMC-PIN48" x="0" y="-119.38" addlevel="can"/>
<gate name="49" symbol="TMC-PIN49" x="0" y="-121.92" addlevel="can"/>
<gate name="50" symbol="TMC-PIN50" x="0" y="-124.46" addlevel="can"/>
<gate name="51" symbol="TMC-PIN51" x="0" y="-127" addlevel="can"/>
<gate name="52" symbol="TMC-PIN52" x="0" y="-129.54" addlevel="can"/>
<gate name="53" symbol="TMC-PIN53" x="0" y="-132.08" addlevel="can"/>
<gate name="54" symbol="TMC-PIN54" x="0" y="-134.62" addlevel="can"/>
<gate name="55" symbol="TMC-PIN55" x="0" y="-137.16" addlevel="can"/>
<gate name="56" symbol="TMC-PIN56" x="0" y="-139.7" addlevel="can"/>
<gate name="57" symbol="TMC-PIN57" x="0" y="-142.24" addlevel="can"/>
<gate name="58" symbol="TMC-PIN58" x="0" y="-144.78" addlevel="can"/>
<gate name="59" symbol="TMC-PIN59" x="0" y="-147.32" addlevel="can"/>
<gate name="60" symbol="TMC-PIN60" x="0" y="-149.86" addlevel="can"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="DEUTZ">
<packages>
</packages>
<symbols>
<symbol name="FUEL_PUMP">
<text x="0" y="1.27" size="2.54" layer="95" align="bottom-center">DEUTZ</text>
<text x="0" y="-2.54" size="2.54" layer="95" align="bottom-center">FUEL PUMP</text>
<text x="0" y="-6.35" size="1.4224" layer="97" align="bottom-center">&gt;DAN</text>
<wire x1="-10.16" y1="7.62" x2="10.16" y2="7.62" width="0.4064" layer="94"/>
<wire x1="10.16" y1="7.62" x2="10.16" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="10.16" y1="-7.62" x2="-10.16" y2="-7.62" width="0.4064" layer="94"/>
<wire x1="-10.16" y1="-7.62" x2="-10.16" y2="7.62" width="0.4064" layer="94"/>
</symbol>
<symbol name="WATER_IN_FUEL_SW">
<wire x1="12.7" y1="-7.62" x2="-12.7" y2="-7.62" width="0.1524" layer="94" style="shortdash"/>
<wire x1="12.7" y1="7.62" x2="12.7" y2="0" width="0.1524" layer="94" style="shortdash"/>
<wire x1="12.7" y1="0" x2="12.7" y2="-2.54" width="0.1524" layer="94" style="shortdash"/>
<wire x1="12.7" y1="-2.54" x2="12.7" y2="-7.62" width="0.1524" layer="94" style="shortdash"/>
<wire x1="-12.7" y1="7.62" x2="12.7" y2="7.62" width="0.1524" layer="94" style="shortdash"/>
<wire x1="-12.7" y1="-7.62" x2="-12.7" y2="7.62" width="0.1524" layer="94" style="shortdash"/>
<text x="0" y="-10.16" size="1.778" layer="94" align="bottom-center">Water in Fuel Level SW</text>
<text x="0" y="10.16" size="1.778" layer="94" align="top-center">Spínač na palivovém filtru</text>
<wire x1="-2.54" y1="-2.54" x2="3.175" y2="-3.556" width="0.254" layer="94"/>
<circle x="-2.54" y="-2.54" radius="0.254" width="0.508" layer="94"/>
<wire x1="2.54" y1="-2.54" x2="2.54" y2="-3.81" width="0.1524" layer="94"/>
<pin name="P$1" x="17.78" y="0" visible="off" length="middle" rot="R180"/>
<pin name="P$2" x="17.78" y="-2.54" visible="off" length="middle" rot="R180"/>
<wire x1="12.7" y1="-2.54" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-7.62" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="12.7" y1="0" x2="-7.62" y2="0" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="0" x2="-7.62" y2="-2.54" width="0.1524" layer="94"/>
<text x="-12.065" y="6.985" size="1.27" layer="97" align="top-left">Externí senzor
zapojený dodatečně
samostatným kabelem</text>
</symbol>
<symbol name="AIRFILTER_SW">
<wire x1="12.7" y1="-5.08" x2="12.7" y2="-7.62" width="0.1524" layer="94" style="shortdash"/>
<wire x1="12.7" y1="-7.62" x2="-12.7" y2="-7.62" width="0.1524" layer="94" style="shortdash"/>
<wire x1="12.7" y1="7.62" x2="12.7" y2="-2.54" width="0.1524" layer="94" style="shortdash"/>
<wire x1="12.7" y1="-2.54" x2="12.7" y2="-5.08" width="0.1524" layer="94" style="shortdash"/>
<wire x1="-12.7" y1="7.62" x2="12.7" y2="7.62" width="0.1524" layer="94" style="shortdash"/>
<wire x1="-12.7" y1="-7.62" x2="-12.7" y2="7.62" width="0.1524" layer="94" style="shortdash"/>
<text x="0" y="-10.16" size="1.778" layer="94" align="bottom-center">AirFilter Difference Pressure SW</text>
<text x="0" y="10.16" size="1.778" layer="94" align="top-center">Spínač na vzduchovém filtru</text>
<wire x1="-2.54" y1="-2.54" x2="3.175" y2="-1.524" width="0.254" layer="94"/>
<circle x="-2.54" y="-2.54" radius="0.254" width="0.508" layer="94"/>
<pin name="P$1" x="17.78" y="-5.08" visible="off" length="middle" direction="pas" rot="R180"/>
<pin name="P$2" x="17.78" y="-2.54" visible="off" length="middle" direction="pas" rot="R180"/>
<wire x1="12.7" y1="-2.54" x2="2.54" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-2.54" x2="-7.62" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="12.7" y1="-5.08" x2="-7.62" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="-7.62" y2="-2.54" width="0.1524" layer="94"/>
<text x="-12.065" y="6.985" size="1.27" layer="97" align="top-left">Externí senzor
zapojený dodatečně
samostatným kabelem</text>
</symbol>
<symbol name="COOLANT_LEVEL_SENSOR">
<wire x1="12.7" y1="-8.89" x2="-12.7" y2="-8.89" width="0.1524" layer="94" style="shortdash"/>
<wire x1="12.7" y1="8.89" x2="12.7" y2="5.08" width="0.1524" layer="94" style="shortdash"/>
<wire x1="12.7" y1="5.08" x2="12.7" y2="0" width="0.1524" layer="94" style="shortdash"/>
<wire x1="12.7" y1="0" x2="12.7" y2="-5.08" width="0.1524" layer="94" style="shortdash"/>
<wire x1="12.7" y1="-5.08" x2="12.7" y2="-8.89" width="0.1524" layer="94" style="shortdash"/>
<wire x1="-12.7" y1="8.89" x2="12.7" y2="8.89" width="0.1524" layer="94" style="shortdash"/>
<wire x1="-12.7" y1="-8.89" x2="-12.7" y2="8.89" width="0.1524" layer="94" style="shortdash"/>
<text x="0" y="-11.43" size="1.778" layer="94" align="bottom-center">Coolant Level Sensor</text>
<text x="0" y="11.43" size="1.778" layer="94" align="top-center">Snímač hladiny na chladiči</text>
<text x="-12.065" y="8.255" size="1.27" layer="97" align="top-left">Externí senzor
zapojený dodatečně
samostatným kabelem</text>
<wire x1="-2.54" y1="0" x2="3.175" y2="-1.016" width="0.254" layer="94"/>
<circle x="-2.54" y="0" radius="0.254" width="0.508" layer="94"/>
<wire x1="2.54" y1="0" x2="2.54" y2="-1.27" width="0.1524" layer="94"/>
<pin name="P$1" x="17.78" y="-5.08" visible="off" length="middle" rot="R180"/>
<pin name="P$2" x="17.78" y="0" visible="off" length="middle" rot="R180"/>
<wire x1="12.7" y1="0" x2="2.54" y2="0" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="0" x2="-10.16" y2="0" width="0.1524" layer="94"/>
<wire x1="12.7" y1="-5.08" x2="8.89" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="8.89" y1="-5.08" x2="-10.16" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="-10.16" y1="-5.08" x2="-10.16" y2="0" width="0.1524" layer="94"/>
<pin name="P$3" x="17.78" y="5.08" visible="off" length="middle" rot="R180"/>
<circle x="8.89" y="-5.08" radius="0.254" width="0.508" layer="94"/>
<wire x1="8.89" y1="-5.08" x2="8.89" y2="-7.62" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-7.62" x2="10.16" y2="-7.62" width="0.1524" layer="94"/>
<wire x1="12.7" y1="5.08" x2="8.89" y2="5.08" width="0.1524" layer="94"/>
<wire x1="8.89" y1="5.08" x2="8.89" y2="6.35" width="0.1524" layer="94"/>
<circle x="8.89" y="6.985" radius="0.635" width="0.1524" layer="94"/>
<text x="10.795" y="0.635" size="1.27" layer="94" align="bottom-center">S</text>
<wire x1="10.16" y1="6.985" x2="11.43" y2="6.985" width="0.1524" layer="94"/>
<wire x1="10.795" y1="7.62" x2="10.795" y2="6.35" width="0.1524" layer="94"/>
</symbol>
<symbol name="DS">
<wire x1="-1.27" y1="3.81" x2="0" y2="3.81" width="0.254" layer="94"/>
<wire x1="0" y1="3.81" x2="1.27" y2="3.81" width="0.254" layer="94"/>
<wire x1="1.27" y1="3.81" x2="1.27" y2="8.89" width="0.254" layer="94"/>
<wire x1="1.27" y1="8.89" x2="0" y2="8.89" width="0.254" layer="94"/>
<wire x1="0" y1="8.89" x2="-1.27" y2="8.89" width="0.254" layer="94"/>
<wire x1="-1.27" y1="8.89" x2="-1.27" y2="3.81" width="0.254" layer="94"/>
<text x="-1.905" y="6.35" size="1.4224" layer="95" rot="R270" align="top-center">120R</text>
<text x="1.905" y="6.35" size="1.4224" layer="96" rot="R270" align="bottom-center">0,5W</text>
<text x="0" y="17.78" size="1.778" layer="94" rot="R180" align="bottom-center">Diagnostická zásuvka</text>
<wire x1="-10.16" y1="15.24" x2="10.16" y2="15.24" width="0.3048" layer="94"/>
<wire x1="10.16" y1="15.24" x2="10.16" y2="-15.24" width="0.3048" layer="94"/>
<wire x1="10.16" y1="-15.24" x2="-10.16" y2="-15.24" width="0.3048" layer="94"/>
<wire x1="-10.16" y1="-15.24" x2="-10.16" y2="15.24" width="0.3048" layer="94"/>
<text x="7.62" y="12.7" size="1.778" layer="94" align="center-left">A</text>
<text x="7.62" y="2.54" size="1.778" layer="94" align="center-left">F</text>
<text x="7.62" y="10.16" size="1.778" layer="94" align="center-left">M</text>
<text x="7.62" y="-2.54" size="1.778" layer="94" align="center-left">H</text>
<text x="7.62" y="-10.16" size="1.778" layer="94" align="center-left">G</text>
<text x="7.62" y="-12.7" size="1.778" layer="94" align="center-left">B</text>
<wire x1="-1.27" y1="-8.89" x2="0" y2="-8.89" width="0.254" layer="94"/>
<wire x1="0" y1="-8.89" x2="1.27" y2="-8.89" width="0.254" layer="94"/>
<wire x1="1.27" y1="-8.89" x2="1.27" y2="-3.81" width="0.254" layer="94"/>
<wire x1="1.27" y1="-3.81" x2="0" y2="-3.81" width="0.254" layer="94"/>
<wire x1="0" y1="-3.81" x2="-1.27" y2="-3.81" width="0.254" layer="94"/>
<wire x1="-1.27" y1="-3.81" x2="-1.27" y2="-8.89" width="0.254" layer="94"/>
<text x="-1.905" y="-6.35" size="1.4224" layer="95" rot="R270" align="top-center">120R</text>
<text x="1.905" y="-6.35" size="1.4224" layer="96" rot="R270" align="bottom-center">0,5W</text>
<text x="-13.97" y="-17.78" size="1.778" layer="94">DEUTZ Diagnostic Socket</text>
<circle x="5.08" y="12.7" radius="0.898025" width="0.254" layer="94"/>
<circle x="5.08" y="10.16" radius="0.898025" width="0.254" layer="94"/>
<circle x="5.08" y="2.54" radius="0.898025" width="0.254" layer="94"/>
<circle x="5.08" y="-2.54" radius="0.898025" width="0.254" layer="94"/>
<circle x="5.08" y="-10.16" radius="0.898025" width="0.254" layer="94"/>
<circle x="5.08" y="-12.7" radius="0.898025" width="0.254" layer="94"/>
<circle x="0" y="10.16" radius="0.254" width="0.508" layer="94"/>
<circle x="0" y="2.54" radius="0.254" width="0.508" layer="94"/>
<circle x="0" y="-2.54" radius="0.254" width="0.508" layer="94"/>
<circle x="0" y="-10.16" radius="0.254" width="0.508" layer="94"/>
<pin name="M" x="-12.7" y="7.62" visible="off" length="middle" direction="pas"/>
<pin name="F" x="-12.7" y="5.08" visible="off" length="middle" direction="pas"/>
<pin name="H" x="-12.7" y="-5.08" visible="off" length="middle" direction="pas"/>
<pin name="G" x="-12.7" y="-7.62" visible="off" length="middle" direction="pas"/>
<pin name="B" x="-12.7" y="-12.7" visible="off" length="middle" direction="pas"/>
<pin name="A" x="-12.7" y="12.7" visible="off" length="middle" direction="pas"/>
<wire x1="4.1275" y1="10.16" x2="0" y2="10.16" width="0.1524" layer="94"/>
<wire x1="0" y1="10.16" x2="-7.62" y2="10.16" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="10.16" x2="-7.62" y2="7.62" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="5.08" x2="-7.62" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="2.54" x2="0" y2="2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="4.1275" y2="2.54" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="12.7" x2="4.1275" y2="12.7" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="-7.62" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="-2.54" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="4.1275" y2="-2.54" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="-7.62" x2="-7.62" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="-10.16" x2="0" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="0" y1="-10.16" x2="4.1275" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="-12.7" x2="4.1275" y2="-12.7" width="0.1524" layer="94"/>
<text x="0" y="0" size="1.27" layer="97" align="center">Rezistory nutno doplnit!</text>
<wire x1="0" y1="8.89" x2="0" y2="10.16" width="0.1524" layer="94"/>
<wire x1="0" y1="2.54" x2="0" y2="3.81" width="0.1524" layer="94"/>
<wire x1="0" y1="-10.16" x2="0" y2="-8.89" width="0.1524" layer="94"/>
<wire x1="0" y1="-3.81" x2="0" y2="-2.54" width="0.1524" layer="94"/>
<text x="-13.97" y="-22.86" size="1.27" layer="97">Karta ABRA:</text>
<text x="-2.54" y="-22.86" size="1.27" layer="97">&gt;ABRA</text>
<text x="-13.97" y="-20.32" size="1.27" layer="97">Kód výrobce:</text>
<text x="-2.54" y="-20.32" size="1.27" layer="97">&gt;MPN</text>
</symbol>
<symbol name="CAN_ARMATUR-SMALL">
<wire x1="0" y1="-26.67" x2="5.08" y2="-26.67" width="0.3048" layer="94"/>
<wire x1="5.08" y1="-26.67" x2="7.62" y2="-26.67" width="0.3048" layer="94"/>
<wire x1="7.62" y1="-26.67" x2="10.16" y2="-26.67" width="0.3048" layer="94"/>
<wire x1="10.16" y1="-26.67" x2="12.7" y2="-26.67" width="0.3048" layer="94"/>
<wire x1="12.7" y1="-26.67" x2="15.24" y2="-26.67" width="0.3048" layer="94"/>
<wire x1="15.24" y1="-26.67" x2="17.78" y2="-26.67" width="0.3048" layer="94"/>
<wire x1="17.78" y1="-26.67" x2="20.32" y2="-26.67" width="0.3048" layer="94"/>
<wire x1="20.32" y1="-26.67" x2="27.94" y2="-26.67" width="0.3048" layer="94"/>
<wire x1="27.94" y1="-26.67" x2="27.94" y2="-10.16" width="0.3048" layer="94"/>
<wire x1="27.94" y1="-5.08" x2="27.94" y2="0" width="0.3048" layer="94"/>
<wire x1="0" y1="-5.08" x2="0" y2="-10.16" width="0.3048" layer="94"/>
<wire x1="0" y1="-10.16" x2="0" y2="-26.67" width="0.3048" layer="94"/>
<wire x1="0" y1="-5.08" x2="3.81" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="3.81" y1="-5.08" x2="24.13" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="24.13" y1="-5.08" x2="27.94" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="27.94" y1="-5.08" x2="27.94" y2="-10.16" width="0.3048" layer="94"/>
<wire x1="27.94" y1="-10.16" x2="24.13" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="24.13" y1="-10.16" x2="3.81" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="3.81" y1="-10.16" x2="0" y2="-10.16" width="0.1524" layer="94"/>
<wire x1="3.81" y1="-10.16" x2="3.81" y2="-5.08" width="0.1524" layer="94"/>
<wire x1="24.13" y1="-10.16" x2="24.13" y2="-5.08" width="0.1524" layer="94"/>
<circle x="20.955" y="-17.78" radius="4.572" width="0.1524" layer="94"/>
<circle x="20.955" y="-17.78" radius="4.0132" width="0.1524" layer="94"/>
<wire x1="20.32" y1="-14.605" x2="20.32" y2="-20.955" width="0.1524" layer="94"/>
<wire x1="20.32" y1="-20.955" x2="21.59" y2="-20.955" width="0.1524" layer="94"/>
<wire x1="21.59" y1="-20.955" x2="21.59" y2="-14.605" width="0.1524" layer="94"/>
<wire x1="21.59" y1="-14.605" x2="20.32" y2="-14.605" width="0.1524" layer="94"/>
<wire x1="3.81" y1="-15.24" x2="4.445" y2="-15.875" width="0.1524" layer="94"/>
<wire x1="4.445" y1="-15.875" x2="5.715" y2="-14.605" width="0.1524" layer="94"/>
<wire x1="5.715" y1="-14.605" x2="6.35" y2="-15.24" width="0.1524" layer="94"/>
<wire x1="6.35" y1="-15.24" x2="6.35" y2="-13.335" width="0.1524" layer="94"/>
<wire x1="6.35" y1="-13.335" x2="4.445" y2="-13.335" width="0.1524" layer="94"/>
<wire x1="4.445" y1="-13.335" x2="5.08" y2="-13.97" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-13.97" x2="3.81" y2="-15.24" width="0.1524" layer="94"/>
<wire x1="6.35" y1="-20.32" x2="5.715" y2="-19.685" width="0.1524" layer="94"/>
<wire x1="5.715" y1="-19.685" x2="4.445" y2="-20.955" width="0.1524" layer="94"/>
<wire x1="4.445" y1="-20.955" x2="3.81" y2="-20.32" width="0.1524" layer="94"/>
<wire x1="3.81" y1="-20.32" x2="3.81" y2="-22.225" width="0.1524" layer="94"/>
<wire x1="3.81" y1="-22.225" x2="5.715" y2="-22.225" width="0.1524" layer="94"/>
<wire x1="5.715" y1="-22.225" x2="5.08" y2="-21.59" width="0.1524" layer="94"/>
<wire x1="5.08" y1="-21.59" x2="6.35" y2="-20.32" width="0.1524" layer="94"/>
<text x="14.2875" y="-7.62" size="2.1844" layer="94" font="vector" align="center">CANarmatur</text>
<pin name="1" x="5.08" y="-27.94" length="point" direction="pas" rot="R90"/>
<pin name="5" x="10.16" y="-27.94" length="point" direction="pas" rot="R90"/>
<pin name="14" x="15.24" y="-27.94" length="point" direction="pas" rot="R90"/>
<pin name="18" x="20.32" y="-27.94" length="point" direction="pas" rot="R90"/>
<pin name="8" x="12.7" y="-27.94" length="point" direction="pas" rot="R90"/>
<pin name="3" x="7.62" y="-27.94" length="point" direction="pas" rot="R90"/>
<text x="11.43" y="-20.955" size="1.4224" layer="94" align="center">set</text>
<wire x1="5.08" y1="-27.94" x2="5.08" y2="-26.67" width="0.1524" layer="94"/>
<wire x1="7.62" y1="-27.94" x2="7.62" y2="-26.67" width="0.1524" layer="94"/>
<wire x1="10.16" y1="-27.94" x2="10.16" y2="-26.67" width="0.1524" layer="94"/>
<wire x1="12.7" y1="-27.94" x2="12.7" y2="-26.67" width="0.1524" layer="94"/>
<wire x1="17.78" y1="-27.94" x2="17.78" y2="-26.67" width="0.1524" layer="94"/>
<wire x1="20.32" y1="-27.94" x2="20.32" y2="-26.67" width="0.1524" layer="94"/>
<wire x1="0" y1="0" x2="0" y2="-0.3175" width="0.3048" layer="94"/>
<wire x1="0" y1="-0.3175" x2="0" y2="-0.635" width="0.3048" layer="94"/>
<wire x1="0" y1="-0.635" x2="0" y2="-0.9525" width="0.3048" layer="94"/>
<wire x1="0" y1="-0.9525" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="-5.08" width="0.3048" layer="94"/>
<wire x1="27.94" y1="0" x2="1.27" y2="0" width="0.3048" layer="94"/>
<wire x1="1.27" y1="0" x2="0.9525" y2="0" width="0.3048" layer="94"/>
<wire x1="0.9525" y1="0" x2="0.635" y2="0" width="0.3048" layer="94"/>
<wire x1="0.635" y1="0" x2="0.3175" y2="0" width="0.3048" layer="94"/>
<wire x1="0.3175" y1="0" x2="0" y2="0" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="1.27" y2="0" width="0.3048" layer="94"/>
<wire x1="0" y1="-0.9525" x2="0.9525" y2="0" width="0.3048" layer="94"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.3048" layer="94"/>
<wire x1="0" y1="-0.3175" x2="0.3175" y2="0" width="0.3048" layer="94"/>
<pin name="16" x="17.78" y="-27.94" length="point" direction="pas" rot="R90"/>
<wire x1="15.24" y1="-27.94" x2="15.24" y2="-26.67" width="0.1524" layer="94"/>
<text x="4.826" y="-4.445" size="1.27" layer="97">Karta ABRA:</text>
<text x="23.114" y="-4.445" size="1.27" layer="97" align="bottom-right">&gt;ABRA</text>
<text x="4.826" y="-1.905" size="1.27" layer="97">Kód výrobce:</text>
<text x="23.114" y="-1.905" size="1.27" layer="97" align="bottom-right">&gt;MPN</text>
</symbol>
<symbol name="TEPLOTNI_CIDLO_KUBOTA">
<wire x1="-3.175" y1="-13.335" x2="3.175" y2="-13.335" width="0.1524" layer="94"/>
<wire x1="3.175" y1="-13.335" x2="5.08" y2="1.905" width="0.1524" layer="94"/>
<wire x1="5.08" y1="1.905" x2="2.54" y2="1.905" width="0.1524" layer="94"/>
<wire x1="2.54" y1="1.905" x2="-2.54" y2="1.905" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="1.905" x2="-5.08" y2="1.905" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="1.905" x2="-3.175" y2="-13.335" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="1.905" x2="-6.35" y2="1.905" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="1.905" x2="-7.62" y2="1.905" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="4.445" width="0.1524" layer="94"/>
<wire x1="-7.62" y1="4.445" x2="-6.35" y2="4.445" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="4.445" x2="-5.08" y2="4.445" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="4.445" x2="-2.54" y2="4.445" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="4.445" x2="2.54" y2="4.445" width="0.1524" layer="94"/>
<wire x1="2.54" y1="4.445" x2="5.08" y2="4.445" width="0.1524" layer="94"/>
<wire x1="5.08" y1="4.445" x2="6.35" y2="4.445" width="0.1524" layer="94"/>
<wire x1="6.35" y1="4.445" x2="7.62" y2="4.445" width="0.1524" layer="94"/>
<wire x1="7.62" y1="4.445" x2="7.62" y2="1.905" width="0.1524" layer="94"/>
<wire x1="7.62" y1="1.905" x2="6.35" y2="1.905" width="0.1524" layer="94"/>
<wire x1="6.35" y1="1.905" x2="5.08" y2="1.905" width="0.1524" layer="94"/>
<wire x1="-6.35" y1="4.445" x2="-6.35" y2="1.905" width="0.1524" layer="94"/>
<wire x1="6.35" y1="1.905" x2="6.35" y2="4.445" width="0.1524" layer="94"/>
<wire x1="2.54" y1="4.445" x2="2.54" y2="1.905" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="4.445" x2="-2.54" y2="1.905" width="0.1524" layer="94"/>
<wire x1="-5.08" y1="4.445" x2="-3.175" y2="6.35" width="0.1524" layer="94"/>
<wire x1="-3.175" y1="6.35" x2="3.175" y2="6.35" width="0.1524" layer="94"/>
<wire x1="3.175" y1="6.35" x2="5.08" y2="4.445" width="0.1524" layer="94"/>
<wire x1="-3.175" y1="6.35" x2="-3.175" y2="10.795" width="0.1524" layer="94"/>
<wire x1="-3.175" y1="10.795" x2="0" y2="10.795" width="0.1524" layer="94"/>
<wire x1="0" y1="10.795" x2="3.175" y2="10.795" width="0.1524" layer="94"/>
<wire x1="3.175" y1="10.795" x2="3.175" y2="6.35" width="0.1524" layer="94"/>
<wire x1="-1.905" y1="-20.32" x2="0" y2="-20.32" width="0.1524" layer="94"/>
<wire x1="0" y1="-20.32" x2="1.905" y2="-20.32" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-20.955" x2="-1.27" y2="-20.955" width="0.1524" layer="94"/>
<wire x1="0.635" y1="-21.59" x2="-0.635" y2="-21.59" width="0.1524" layer="94"/>
<wire x1="0" y1="-0.635" x2="0" y2="10.795" width="0.1524" layer="94"/>
<wire x1="0" y1="-20.32" x2="0" y2="-18.415" width="0.1524" layer="94"/>
<wire x1="0" y1="-18.415" x2="-1.27" y2="-17.78" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="-17.78" x2="0.635" y2="-17.145" width="0.1524" layer="94"/>
<wire x1="0.635" y1="-17.145" x2="-1.27" y2="-16.51" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="-16.51" x2="0.635" y2="-15.875" width="0.1524" layer="94"/>
<wire x1="0.635" y1="-15.875" x2="-1.27" y2="-15.24" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="-15.24" x2="0" y2="-14.605" width="0.1524" layer="94"/>
<wire x1="0" y1="-10.795" x2="0" y2="-14.605" width="0.1524" layer="94"/>
<wire x1="0" y1="-0.635" x2="-1.27" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="-0.635" x2="-1.27" y2="-4.445" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="-4.445" x2="-1.27" y2="-9.525" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="-9.525" x2="1.27" y2="-9.525" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-9.525" x2="1.27" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="1.27" y1="-0.635" x2="0" y2="-0.635" width="0.1524" layer="94"/>
<wire x1="0" y1="-10.795" x2="-2.54" y2="-10.795" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-10.795" x2="-2.54" y2="-4.445" width="0.1524" layer="94"/>
<wire x1="-2.54" y1="-4.445" x2="-1.27" y2="-4.445" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="-4.445" x2="-1.905" y2="-3.81" width="0.1524" layer="94"/>
<wire x1="-1.27" y1="-4.445" x2="-1.905" y2="-5.08" width="0.1524" layer="94"/>
<text x="10.16" y="-7.62" size="1.778" layer="94" rot="R90" align="bottom-center">KUBOTA
Thermo Sensor</text>
<pin name="P$1" x="0" y="12.7" visible="off" length="point" direction="pas" rot="R90"/>
<wire x1="0" y1="12.7" x2="0" y2="10.795" width="0.1524" layer="94"/>
<text x="0" y="-22.86" size="1.27" layer="97" align="top-center">Karta ABRA:</text>
<text x="0" y="-25.4" size="1.27" layer="97" align="center">&gt;ABRA</text>
</symbol>
</symbols>
<devicesets>
<deviceset name="FUEL_PUMP">
<gates>
<gate name="G$1" symbol="FUEL_PUMP" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name="">
<attribute name="DAN" value="3101 3212"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="WATER_IN_FUEL_SW">
<gates>
<gate name="G$1" symbol="WATER_IN_FUEL_SW" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name="">
<attribute name="_EXTERNAL_" value=""/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="AIRFILTER_SW">
<gates>
<gate name="G$1" symbol="AIRFILTER_SW" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name="">
<attribute name="_EXTERNAL_" value=""/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="COOLANT_LEVEL_SENSOR">
<gates>
<gate name="G$1" symbol="COOLANT_LEVEL_SENSOR" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name="">
<attribute name="_EXTERNAL_" value=""/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="DS" prefix="DS">
<gates>
<gate name="G$1" symbol="DS" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name="">
<attribute name="ABRA" value="dodáno s motorem"/>
<attribute name="MPN" value="192900-0649"/>
<attribute name="_EXTERNAL_" value=""/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAN_ARMATUR-SMALL" prefix="CAN_ARMATUR">
<gates>
<gate name="G$1" symbol="CAN_ARMATUR-SMALL" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name="">
<attribute name="ABRA" value="MOST0721"/>
<attribute name="MPN" value="ehb5160F"/>
<attribute name="_EXTERNAL_" value=""/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="TEPLOTNI_CIDLO_KUBOTA">
<gates>
<gate name="G$1" symbol="TEPLOTNI_CIDLO_KUBOTA" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name="">
<attribute name="ABRA" value="MOST0224"/>
<attribute name="_EXTERNAL_" value=""/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="resistor">
<description>&lt;b&gt;Resistors, Capacitors, Inductors&lt;/b&gt;&lt;p&gt;
Based on the previous libraries:
&lt;ul&gt;
&lt;li&gt;r.lbr
&lt;li&gt;cap.lbr 
&lt;li&gt;cap-fe.lbr
&lt;li&gt;captant.lbr
&lt;li&gt;polcap.lbr
&lt;li&gt;ipc-smd.lbr
&lt;/ul&gt;
All SMD packages are defined according to the IPC specifications and  CECC&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;&lt;p&gt;
&lt;p&gt;
for Electrolyt Capacitors see also :&lt;p&gt;
www.bccomponents.com &lt;p&gt;
www.panasonic.com&lt;p&gt;
www.kemet.com&lt;p&gt;
&lt;p&gt;
for trimmer refence see : &lt;u&gt;www.electrospec-inc.com/cross_references/trimpotcrossref.asp&lt;/u&gt;&lt;p&gt;

&lt;map name="nav_main"&gt;
&lt;area shape="rect" coords="0,1,140,23" href="../military_specs.asp" title=""&gt;
&lt;area shape="rect" coords="0,24,140,51" href="../about.asp" title=""&gt;
&lt;area shape="rect" coords="1,52,140,77" href="../rfq.asp" title=""&gt;
&lt;area shape="rect" coords="0,78,139,103" href="../products.asp" title=""&gt;
&lt;area shape="rect" coords="1,102,138,128" href="../excess_inventory.asp" title=""&gt;
&lt;area shape="rect" coords="1,129,138,150" href="../edge.asp" title=""&gt;
&lt;area shape="rect" coords="1,151,139,178" href="../industry_links.asp" title=""&gt;
&lt;area shape="rect" coords="0,179,139,201" href="../comments.asp" title=""&gt;
&lt;area shape="rect" coords="1,203,138,231" href="../directory.asp" title=""&gt;
&lt;area shape="default" nohref&gt;
&lt;/map&gt;

&lt;html&gt;

&lt;title&gt;&lt;/title&gt;

 &lt;LINK REL="StyleSheet" TYPE="text/css" HREF="style-sheet.css"&gt;

&lt;body bgcolor="#ffffff" text="#000000" marginwidth="0" marginheight="0" topmargin="0" leftmargin="0"&gt;
&lt;table border=0 cellspacing=0 cellpadding=0 width="100%" cellpaddding=0 height="55%"&gt;
&lt;tr valign="top"&gt;

&lt;/td&gt;
&lt;! &lt;td width="10"&gt;&amp;nbsp;&lt;/td&gt;
&lt;td width="90%"&gt;

&lt;b&gt;&lt;font color="#0000FF" size="4"&gt;TRIM-POT CROSS REFERENCE&lt;/font&gt;&lt;/b&gt;
&lt;P&gt;
&lt;TABLE BORDER=0 CELLSPACING=1 CELLPADDING=2&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;RECTANGULAR MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;BOURNS&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;BI&amp;nbsp;TECH&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;DALE-VISHAY&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;PHILIPS/MEPCO&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;MURATA&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;PANASONIC&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;SPECTROL&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;B&gt;
      &lt;FONT SIZE=3 FACE=ARIAL color="#FF0000"&gt;MILSPEC&lt;/FONT&gt;
      &lt;/B&gt;
    &lt;/TD&gt;&lt;TD&gt;&amp;nbsp;&lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3 &gt;
      3005P&lt;BR&gt;
      3006P&lt;BR&gt;
      3006W&lt;BR&gt;
      3006Y&lt;BR&gt;
      3009P&lt;BR&gt;
      3009W&lt;BR&gt;
      3009Y&lt;BR&gt;
      3057J&lt;BR&gt;
      3057L&lt;BR&gt;
      3057P&lt;BR&gt;
      3057Y&lt;BR&gt;
      3059J&lt;BR&gt;
      3059L&lt;BR&gt;
      3059P&lt;BR&gt;
      3059Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      89P&lt;BR&gt;
      89W&lt;BR&gt;
      89X&lt;BR&gt;
      89PH&lt;BR&gt;
      76P&lt;BR&gt;
      89XH&lt;BR&gt;
      78SLT&lt;BR&gt;
      78L&amp;nbsp;ALT&lt;BR&gt;
      56P&amp;nbsp;ALT&lt;BR&gt;
      78P&amp;nbsp;ALT&lt;BR&gt;
      T8S&lt;BR&gt;
      78L&lt;BR&gt;
      56P&lt;BR&gt;
      78P&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      T18/784&lt;BR&gt;
      783&lt;BR&gt;
      781&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      2199&lt;BR&gt;
      1697/1897&lt;BR&gt;
      1680/1880&lt;BR&gt;
      2187&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      8035EKP/CT20/RJ-20P&lt;BR&gt;
      -&lt;BR&gt;
      RJ-20X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      1211L&lt;BR&gt;
      8012EKQ&amp;nbsp;ALT&lt;BR&gt;
      8012EKR&amp;nbsp;ALT&lt;BR&gt;
      1211P&lt;BR&gt;
      8012EKJ&lt;BR&gt;
      8012EKL&lt;BR&gt;
      8012EKQ&lt;BR&gt;
      8012EKR&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      2101P&lt;BR&gt;
      2101W&lt;BR&gt;
      2101Y&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      2102L&lt;BR&gt;
      2102S&lt;BR&gt;
      2102Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      EVMCOG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      43P&lt;BR&gt;
      43W&lt;BR&gt;
      43Y&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      40L&lt;BR&gt;
      40P&lt;BR&gt;
      40Y&lt;BR&gt;
      70Y-T602&lt;BR&gt;
      70L&lt;BR&gt;
      70P&lt;BR&gt;
      70Y&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      RT/RTR12&lt;BR&gt;
      -&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;
      RJ/RJR12&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SQUARE MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
   &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MURATA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;SPECTROL&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MILSPEC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3250L&lt;BR&gt;
      3250P&lt;BR&gt;
      3250W&lt;BR&gt;
      3250X&lt;BR&gt;
      3252P&lt;BR&gt;
      3252W&lt;BR&gt;
      3252X&lt;BR&gt;
      3260P&lt;BR&gt;
      3260W&lt;BR&gt;
      3260X&lt;BR&gt;
      3262P&lt;BR&gt;
      3262W&lt;BR&gt;
      3262X&lt;BR&gt;
      3266P&lt;BR&gt;
      3266W&lt;BR&gt;
      3266X&lt;BR&gt;
      3290H&lt;BR&gt;
      3290P&lt;BR&gt;
      3290W&lt;BR&gt;
      3292P&lt;BR&gt;
      3292W&lt;BR&gt;
      3292X&lt;BR&gt;
      3296P&lt;BR&gt;
      3296W&lt;BR&gt;
      3296X&lt;BR&gt;
      3296Y&lt;BR&gt;
      3296Z&lt;BR&gt;
      3299P&lt;BR&gt;
      3299W&lt;BR&gt;
      3299X&lt;BR&gt;
      3299Y&lt;BR&gt;
      3299Z&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      -&lt;BR&gt;
      64W&amp;nbsp;ALT&lt;BR&gt;
      -&lt;BR&gt;
      64P&amp;nbsp;ALT&lt;BR&gt;
      64W&amp;nbsp;ALT&lt;BR&gt;
      64X&amp;nbsp;ALT&lt;BR&gt;
      64P&lt;BR&gt;
      64W&lt;BR&gt;
      64X&lt;BR&gt;
      66X&amp;nbsp;ALT&lt;BR&gt;
      66P&amp;nbsp;ALT&lt;BR&gt;
      66W&amp;nbsp;ALT&lt;BR&gt;
      66P&lt;BR&gt;
      66W&lt;BR&gt;
      66X&lt;BR&gt;
      67P&lt;BR&gt;
      67W&lt;BR&gt;
      67X&lt;BR&gt;
      67Y&lt;BR&gt;
      67Z&lt;BR&gt;
      68P&lt;BR&gt;
      68W&lt;BR&gt;
      68X&lt;BR&gt;
      67Y&amp;nbsp;ALT&lt;BR&gt;
      67Z&amp;nbsp;ALT&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      5050&lt;BR&gt;
      5091&lt;BR&gt;
      5080&lt;BR&gt;
      5087&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T63YB&lt;BR&gt;
      T63XB&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      5887&lt;BR&gt;
      5891&lt;BR&gt;
      5880&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T93Z&lt;BR&gt;
      T93YA&lt;BR&gt;
      T93XA&lt;BR&gt;
      T93YB&lt;BR&gt;
      T93XB&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8026EKP&lt;BR&gt;
      8026EKW&lt;BR&gt;
      8026EKM&lt;BR&gt;
      8026EKP&lt;BR&gt;
      8026EKB&lt;BR&gt;
      8026EKM&lt;BR&gt;
      1309X&lt;BR&gt;
      1309P&lt;BR&gt;
      1309W&lt;BR&gt;
      8024EKP&lt;BR&gt;
      8024EKW&lt;BR&gt;
      8024EKN&lt;BR&gt;
      RJ-9P/CT9P&lt;BR&gt;
      RJ-9W&lt;BR&gt;
      RJ-9X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3103P&lt;BR&gt;
      3103Y&lt;BR&gt;
      3103Z&lt;BR&gt;
      3103P&lt;BR&gt;
      3103Y&lt;BR&gt;
      3103Z&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3105P/3106P&lt;BR&gt;
      3105W/3106W&lt;BR&gt;
      3105X/3106X&lt;BR&gt;
      3105Y/3106Y&lt;BR&gt;
      3105Z/3105Z&lt;BR&gt;
      3102P&lt;BR&gt;
      3102W&lt;BR&gt;
      3102X&lt;BR&gt;
      3102Y&lt;BR&gt;
      3102Z&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMCBG&lt;BR&gt;
      EVMCCG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      55-1-X&lt;BR&gt;
      55-4-X&lt;BR&gt;
      55-3-X&lt;BR&gt;
      55-2-X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      50-2-X&lt;BR&gt;
      50-4-X&lt;BR&gt;
      50-3-X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      64P&lt;BR&gt;
      64W&lt;BR&gt;
      64X&lt;BR&gt;
      64Y&lt;BR&gt;
      64Z&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RT/RTR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RJ/RJR22&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RT/RTR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RJ/RJR26&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RT/RTR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      RJ/RJR24&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=8&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SINGLE TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MURATA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;SPECTROL&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD ALIGN=CENTER&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;MILSPEC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3323P&lt;BR&gt;
      3323S&lt;BR&gt;
      3323W&lt;BR&gt;
      3329H&lt;BR&gt;
      3329P&lt;BR&gt;
      3329W&lt;BR&gt;
      3339H&lt;BR&gt;
      3339P&lt;BR&gt;
      3339W&lt;BR&gt;
      3352E&lt;BR&gt;
      3352H&lt;BR&gt;
      3352K&lt;BR&gt;
      3352P&lt;BR&gt;
      3352T&lt;BR&gt;
      3352V&lt;BR&gt;
      3352W&lt;BR&gt;
      3362H&lt;BR&gt;
      3362M&lt;BR&gt;
      3362P&lt;BR&gt;
      3362R&lt;BR&gt;
      3362S&lt;BR&gt;
      3362U&lt;BR&gt;
      3362W&lt;BR&gt;
      3362X&lt;BR&gt;
      3386B&lt;BR&gt;
      3386C&lt;BR&gt;
      3386F&lt;BR&gt;
      3386H&lt;BR&gt;
      3386K&lt;BR&gt;
      3386M&lt;BR&gt;
      3386P&lt;BR&gt;
      3386S&lt;BR&gt;
      3386W&lt;BR&gt;
      3386X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      25P&lt;BR&gt;
      25S&lt;BR&gt;
      25RX&lt;BR&gt;
      82P&lt;BR&gt;
      82M&lt;BR&gt;
      82PA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      91E&lt;BR&gt;
      91X&lt;BR&gt;
      91T&lt;BR&gt;
      91B&lt;BR&gt;
      91A&lt;BR&gt;
      91V&lt;BR&gt;
      91W&lt;BR&gt;
      25W&lt;BR&gt;
      25V&lt;BR&gt;
      25P&lt;BR&gt;
      -&lt;BR&gt;
      25S&lt;BR&gt;
      25U&lt;BR&gt;
      25RX&lt;BR&gt;
      25X&lt;BR&gt;
      72XW&lt;BR&gt;
      72XL&lt;BR&gt;
      72PM&lt;BR&gt;
      72RX&lt;BR&gt;
      -&lt;BR&gt;
      72PX&lt;BR&gt;
      72P&lt;BR&gt;
      72RXW&lt;BR&gt;
      72RXL&lt;BR&gt;
      72X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      T7YB&lt;BR&gt;
      T7YA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      TXD&lt;BR&gt;
      TYA&lt;BR&gt;
      TYP&lt;BR&gt;
      -&lt;BR&gt;
      TYD&lt;BR&gt;
      TX&lt;BR&gt;
      -&lt;BR&gt;
      150SX&lt;BR&gt;
      100SX&lt;BR&gt;
      102T&lt;BR&gt;
      101S&lt;BR&gt;
      190T&lt;BR&gt;
      150TX&lt;BR&gt;
      101&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      101SX&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ET6P&lt;BR&gt;
      ET6S&lt;BR&gt;
      ET6X&lt;BR&gt;
      RJ-6W/8014EMW&lt;BR&gt;
      RJ-6P/8014EMP&lt;BR&gt;
      RJ-6X/8014EMX&lt;BR&gt;
      TM7W&lt;BR&gt;
      TM7P&lt;BR&gt;
      TM7X&lt;BR&gt;
      -&lt;BR&gt;
      8017SMS&lt;BR&gt;
      -&lt;BR&gt;
      8017SMB&lt;BR&gt;
      8017SMA&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      CT-6W&lt;BR&gt;
      CT-6H&lt;BR&gt;
      CT-6P&lt;BR&gt;
      CT-6R&lt;BR&gt;
      -&lt;BR&gt;
      CT-6V&lt;BR&gt;
      CT-6X&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8038EKV&lt;BR&gt;
      -&lt;BR&gt;
      8038EKX&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      8038EKP&lt;BR&gt;
      8038EKZ&lt;BR&gt;
      8038EKW&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3321H&lt;BR&gt;
      3321P&lt;BR&gt;
      3321N&lt;BR&gt;
      1102H&lt;BR&gt;
      1102P&lt;BR&gt;
      1102T&lt;BR&gt;
      RVA0911V304A&lt;BR&gt;
      -&lt;BR&gt;
      RVA0911H413A&lt;BR&gt;
      RVG0707V100A&lt;BR&gt;
      RVA0607V(H)306A&lt;BR&gt;
      RVA1214H213A&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      3104B&lt;BR&gt;
      3104C&lt;BR&gt;
      3104F&lt;BR&gt;
      3104H&lt;BR&gt;
      -&lt;BR&gt;
      3104M&lt;BR&gt;
      3104P&lt;BR&gt;
      3104S&lt;BR&gt;
      3104W&lt;BR&gt;
      3104X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      EVMQ0G&lt;BR&gt;
      EVMQIG&lt;BR&gt;
      EVMQ3G&lt;BR&gt;
      EVMS0G&lt;BR&gt;
      EVMQ0G&lt;BR&gt;
      EVMG0G&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMK4GA00B&lt;BR&gt;
      EVM30GA00B&lt;BR&gt;
      EVMK0GA00B&lt;BR&gt;
      EVM38GA00B&lt;BR&gt;
      EVMB6&lt;BR&gt;
      EVLQ0&lt;BR&gt;
      -&lt;BR&gt;
      EVMMSG&lt;BR&gt;
      EVMMBG&lt;BR&gt;
      EVMMAG&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMMCS&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM1&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM0&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      EVMM3&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      62-3-1&lt;BR&gt;
      62-1-2&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      67R&lt;BR&gt;
      -&lt;BR&gt;
      67P&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      67X&lt;BR&gt;
      63V&lt;BR&gt;
      63S&lt;BR&gt;
      63M&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      63H&lt;BR&gt;
      63P&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      63X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      RJ/RJR50&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
&lt;/TABLE&gt;
&lt;P&gt;&amp;nbsp;&lt;P&gt;
&lt;TABLE BORDER=0 CELLSPACING=1 CELLPADDING=3&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;
      &lt;FONT color="#0000FF" SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SMD TRIM-POT CROSS REFERENCE&lt;/B&gt;&lt;/FONT&gt;
      &lt;P&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;MULTI-TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURNS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;TOCOS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;AUX/KYOCERA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3224G&lt;BR&gt;
      3224J&lt;BR&gt;
      3224W&lt;BR&gt;
      3269P&lt;BR&gt;
      3269W&lt;BR&gt;
      3269X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      44G&lt;BR&gt;
      44J&lt;BR&gt;
      44W&lt;BR&gt;
      84P&lt;BR&gt;
      84W&lt;BR&gt;
      84X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST63Z&lt;BR&gt;
      ST63Y&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST5P&lt;BR&gt;
      ST5W&lt;BR&gt;
      ST5X&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;&amp;nbsp;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD COLSPAN=7&gt;
      &lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;SINGLE TURN&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BOURNS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;BI&amp;nbsp;TECH&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;DALE-VISHAY&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PHILIPS/MEPCO&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;PANASONIC&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;TOCOS&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD&gt;
      &lt;FONT SIZE=3 FACE=ARIAL&gt;&lt;B&gt;AUX/KYOCERA&lt;/B&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
  &lt;TR&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      3314G&lt;BR&gt;
      3314J&lt;BR&gt;
      3364A/B&lt;BR&gt;
      3364C/D&lt;BR&gt;
      3364W/X&lt;BR&gt;
      3313G&lt;BR&gt;
      3313J&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      23B&lt;BR&gt;
      23A&lt;BR&gt;
      21X&lt;BR&gt;
      21W&lt;BR&gt;
      -&lt;BR&gt;
      22B&lt;BR&gt;
      22A&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ST5YL/ST53YL&lt;BR&gt;
      ST5YJ/5T53YJ&lt;BR&gt;
      ST-23A&lt;BR&gt;
      ST-22B&lt;BR&gt;
      ST-22&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      ST-4B&lt;BR&gt;
      ST-4A&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      ST-3B&lt;BR&gt;
      ST-3A&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      EVM-6YS&lt;BR&gt;
      EVM-1E&lt;BR&gt;
      EVM-1G&lt;BR&gt;
      EVM-1D&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      G4B&lt;BR&gt;
      G4A&lt;BR&gt;
      TR04-3S1&lt;BR&gt;
      TRG04-2S1&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
    &lt;TD BGCOLOR="#cccccc" ALIGN=CENTER&gt;&lt;FONT FACE=ARIAL SIZE=3&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;
      DVR-43A&lt;BR&gt;
      CVR-42C&lt;BR&gt;
      CVR-42A/C&lt;BR&gt;
      -&lt;BR&gt;
      -&lt;BR&gt;&lt;/FONT&gt;
    &lt;/TD&gt;
  &lt;/TR&gt;
&lt;/TABLE&gt;
&lt;P&gt;
&lt;FONT SIZE=4 FACE=ARIAL&gt;&lt;B&gt;ALT =&amp;nbsp;ALTERNATE&lt;/B&gt;&lt;/FONT&gt;
&lt;P&gt;

&amp;nbsp;
&lt;P&gt;
&lt;/td&gt;
&lt;/tr&gt;
&lt;/table&gt;
&lt;/BODY&gt;&lt;/HTML&gt;</description>
<packages>
<package name="R0402">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.245" y1="0.224" x2="0.245" y2="0.224" width="0.1524" layer="51"/>
<wire x1="0.245" y1="-0.224" x2="-0.245" y2="-0.224" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.483" x2="1.473" y2="0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.483" x2="1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.483" x2="-1.473" y2="-0.483" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.483" x2="-1.473" y2="0.483" width="0.0508" layer="39"/>
<smd name="1" x="-0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<smd name="2" x="0.65" y="0" dx="0.7" dy="0.9" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.554" y1="-0.3048" x2="-0.254" y2="0.2951" layer="51"/>
<rectangle x1="0.2588" y1="-0.3048" x2="0.5588" y2="0.2951" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R0603">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.432" y1="-0.356" x2="0.432" y2="-0.356" width="0.1524" layer="51"/>
<wire x1="0.432" y1="0.356" x2="-0.432" y2="0.356" width="0.1524" layer="51"/>
<wire x1="-1.473" y1="0.983" x2="1.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="0.983" x2="1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.473" y1="-0.983" x2="-1.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.473" y1="-0.983" x2="-1.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1" dy="1.1" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1" dy="1.1" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4318" y1="-0.4318" x2="0.8382" y2="0.4318" layer="51"/>
<rectangle x1="-0.8382" y1="-0.4318" x2="-0.4318" y2="0.4318" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
</package>
<package name="R0805">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="R0805W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; wave soldering&lt;p&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.0525" y="0" dx="1.5" dy="1" layer="1"/>
<smd name="2" x="1.0525" y="0" dx="1.5" dy="1" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5001" x2="0.1999" y2="0.5001" layer="35"/>
</package>
<package name="R1206">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R1206W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R1210">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-0.8999" x2="0.3" y2="0.8999" layer="35"/>
</package>
<package name="R1210W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="R2010">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="R2010W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.311" y="0" dx="2" dy="1.8" layer="1"/>
<smd name="2" x="2.311" y="0" dx="2" dy="1.8" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
</package>
<package name="R2012">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<smd name="2" x="0.85" y="0" dx="1.3" dy="1.5" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1001" y1="-0.5999" x2="0.1001" y2="0.5999" layer="35"/>
</package>
<package name="R2012W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.41" y1="0.635" x2="0.41" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-0.41" y1="-0.635" x2="0.41" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-0.94" y="0" dx="1.5" dy="1" layer="1"/>
<smd name="2" x="0.94" y="0" dx="1.5" dy="1" layer="1"/>
<text x="-0.635" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="0.4064" y1="-0.6985" x2="1.0564" y2="0.7015" layer="51"/>
<rectangle x1="-1.0668" y1="-0.6985" x2="-0.4168" y2="0.7015" layer="51"/>
<rectangle x1="-0.1001" y1="-0.5999" x2="0.1001" y2="0.5999" layer="35"/>
</package>
<package name="R2512">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<smd name="2" x="2.8" y="0" dx="1.8" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R2512W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-2.896" y="0" dx="2" dy="2.1" layer="1"/>
<smd name="2" x="2.896" y="0" dx="2" dy="2.1" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R3216">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R3216W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="0.8" x2="0.888" y2="0.8" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-0.8" x2="0.888" y2="-0.8" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.2" layer="1"/>
<text x="-1.905" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-0.8763" x2="-0.9009" y2="0.8738" layer="51"/>
<rectangle x1="0.889" y1="-0.8763" x2="1.6391" y2="0.8738" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="R3225">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2.7" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-1" x2="0.3" y2="1" layer="35"/>
</package>
<package name="R3225W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-0.913" y1="1.219" x2="0.939" y2="1.219" width="0.1524" layer="51"/>
<wire x1="-0.913" y1="-1.219" x2="0.939" y2="-1.219" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<smd name="2" x="1.499" y="0" dx="1.8" dy="1.8" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.905" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-1.3081" x2="-0.9009" y2="1.2918" layer="51"/>
<rectangle x1="0.9144" y1="-1.3081" x2="1.6645" y2="1.2918" layer="51"/>
<rectangle x1="-0.3" y1="-1" x2="0.3" y2="1" layer="35"/>
</package>
<package name="R5025">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<smd name="2" x="2.2" y="0" dx="1.8" dy="2.7" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R5025W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
wave soldering</description>
<wire x1="-1.662" y1="1.245" x2="1.662" y2="1.245" width="0.1524" layer="51"/>
<wire x1="-1.637" y1="-1.245" x2="1.687" y2="-1.245" width="0.1524" layer="51"/>
<wire x1="-3.473" y1="1.483" x2="3.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="1.483" x2="3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="3.473" y1="-1.483" x2="-3.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-3.473" y1="-1.483" x2="-3.473" y2="1.483" width="0.0508" layer="39"/>
<smd name="1" x="-2.311" y="0" dx="2" dy="1.8" layer="1"/>
<smd name="2" x="2.311" y="0" dx="2" dy="1.8" layer="1"/>
<text x="-3.175" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.175" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-2.4892" y1="-1.3208" x2="-1.6393" y2="1.3292" layer="51"/>
<rectangle x1="1.651" y1="-1.3208" x2="2.5009" y2="1.3292" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R6332">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
Source: http://download.siliconexpert.com/pdfs/2005/02/24/Semi_Ap/2/VSH/Resistor/dcrcwfre.pdf</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-3.1" y="0" dx="1" dy="3.2" layer="1"/>
<smd name="2" x="3.1" y="0" dx="1" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="R6332W">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; wave soldering&lt;p&gt;
Source: http://download.siliconexpert.com/pdfs/2005/02/24/Semi_Ap/2/VSH/Resistor/dcrcwfre.pdf</description>
<wire x1="-2.362" y1="1.473" x2="2.387" y2="1.473" width="0.1524" layer="51"/>
<wire x1="-2.362" y1="-1.473" x2="2.387" y2="-1.473" width="0.1524" layer="51"/>
<wire x1="-3.973" y1="1.983" x2="3.973" y2="1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="1.983" x2="3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="3.973" y1="-1.983" x2="-3.973" y2="-1.983" width="0.0508" layer="39"/>
<wire x1="-3.973" y1="-1.983" x2="-3.973" y2="1.983" width="0.0508" layer="39"/>
<smd name="1" x="-3.196" y="0" dx="1.2" dy="3.2" layer="1"/>
<smd name="2" x="3.196" y="0" dx="1.2" dy="3.2" layer="1"/>
<text x="-2.54" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.175" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.2004" y1="-1.5494" x2="-2.3505" y2="1.5507" layer="51"/>
<rectangle x1="2.3622" y1="-1.5494" x2="3.2121" y2="1.5507" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="M0805">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.10 W</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="0.7112" y1="0.635" x2="-0.7112" y2="0.635" width="0.1524" layer="51"/>
<wire x1="0.7112" y1="-0.635" x2="-0.7112" y2="-0.635" width="0.1524" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0414" y1="-0.7112" x2="-0.6858" y2="0.7112" layer="51"/>
<rectangle x1="0.6858" y1="-0.7112" x2="1.0414" y2="0.7112" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5999" x2="0.1999" y2="0.5999" layer="35"/>
</package>
<package name="M1206">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="1.143" y1="0.8382" x2="-1.143" y2="0.8382" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.8382" x2="-1.143" y2="-0.8382" width="0.1524" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.9144" x2="-1.1176" y2="0.9144" layer="51"/>
<rectangle x1="1.1176" y1="-0.9144" x2="1.7018" y2="0.9144" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="M1406">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.12 W</description>
<wire x1="-2.973" y1="0.983" x2="2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-0.983" x2="-2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-0.983" x2="-2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="0.983" x2="2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.3208" y1="0.762" x2="-1.3208" y2="0.762" width="0.1524" layer="51"/>
<wire x1="1.3208" y1="-0.762" x2="-1.3208" y2="-0.762" width="0.1524" layer="51"/>
<smd name="1" x="-1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<smd name="2" x="1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.8542" y1="-0.8382" x2="-1.2954" y2="0.8382" layer="51"/>
<rectangle x1="1.2954" y1="-0.8382" x2="1.8542" y2="0.8382" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
</package>
<package name="M2012">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.10 W</description>
<wire x1="-1.973" y1="0.983" x2="1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="-0.983" x2="-1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-1.973" y1="-0.983" x2="-1.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="1.973" y1="0.983" x2="1.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="0.7112" y1="0.635" x2="-0.7112" y2="0.635" width="0.1524" layer="51"/>
<wire x1="0.7112" y1="-0.635" x2="-0.7112" y2="-0.635" width="0.1524" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<smd name="2" x="0.95" y="0" dx="1.3" dy="1.6" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.0414" y1="-0.7112" x2="-0.6858" y2="0.7112" layer="51"/>
<rectangle x1="0.6858" y1="-0.7112" x2="1.0414" y2="0.7112" layer="51"/>
<rectangle x1="-0.1999" y1="-0.5999" x2="0.1999" y2="0.5999" layer="35"/>
</package>
<package name="M2309">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-4.473" y1="1.483" x2="4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="-1.483" x2="-4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-4.473" y1="-1.483" x2="-4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="1.483" x2="4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.413" y1="1.1684" x2="-2.4384" y2="1.1684" width="0.1524" layer="51"/>
<wire x1="2.413" y1="-1.1684" x2="-2.413" y2="-1.1684" width="0.1524" layer="51"/>
<smd name="1" x="-2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<smd name="2" x="2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.048" y1="-1.2446" x2="-2.3876" y2="1.2446" layer="51"/>
<rectangle x1="2.3876" y1="-1.2446" x2="3.048" y2="1.2446" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="M3216">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-2.473" y1="1.483" x2="2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-1.483" x2="-2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-1.483" x2="-2.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="2.473" y1="1.483" x2="2.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="1.143" y1="0.8382" x2="-1.143" y2="0.8382" width="0.1524" layer="51"/>
<wire x1="1.143" y1="-0.8382" x2="-1.143" y2="-0.8382" width="0.1524" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="2" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="2" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.9144" x2="-1.1176" y2="0.9144" layer="51"/>
<rectangle x1="1.1176" y1="-0.9144" x2="1.7018" y2="0.9144" layer="51"/>
<rectangle x1="-0.3" y1="-0.8001" x2="0.3" y2="0.8001" layer="35"/>
</package>
<package name="M3516">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.12 W</description>
<wire x1="-2.973" y1="0.983" x2="2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="-0.983" x2="-2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.973" y1="-0.983" x2="-2.973" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.973" y1="0.983" x2="2.973" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="1.3208" y1="0.762" x2="-1.3208" y2="0.762" width="0.1524" layer="51"/>
<wire x1="1.3208" y1="-0.762" x2="-1.3208" y2="-0.762" width="0.1524" layer="51"/>
<smd name="1" x="-1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<smd name="2" x="1.7" y="0" dx="1.4" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.8542" y1="-0.8382" x2="-1.2954" y2="0.8382" layer="51"/>
<rectangle x1="1.2954" y1="-0.8382" x2="1.8542" y2="0.8382" layer="51"/>
<rectangle x1="-0.4001" y1="-0.7" x2="0.4001" y2="0.7" layer="35"/>
</package>
<package name="M5923">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
MELF 0.25 W</description>
<wire x1="-4.473" y1="1.483" x2="4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="-1.483" x2="-4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="-4.473" y1="-1.483" x2="-4.473" y2="1.483" width="0.0508" layer="39"/>
<wire x1="4.473" y1="1.483" x2="4.473" y2="-1.483" width="0.0508" layer="39"/>
<wire x1="2.413" y1="1.1684" x2="-2.4384" y2="1.1684" width="0.1524" layer="51"/>
<wire x1="2.413" y1="-1.1684" x2="-2.413" y2="-1.1684" width="0.1524" layer="51"/>
<smd name="1" x="-2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<smd name="2" x="2.85" y="0" dx="1.5" dy="2.6" layer="1"/>
<text x="-1.905" y="1.905" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-3.048" y1="-1.2446" x2="-2.3876" y2="1.2446" layer="51"/>
<rectangle x1="2.3876" y1="-1.2446" x2="3.048" y2="1.2446" layer="51"/>
<rectangle x1="-0.5001" y1="-1" x2="0.5001" y2="1" layer="35"/>
</package>
<package name="0204/5">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0" x2="-2.032" y2="0" width="0.508" layer="51"/>
<wire x1="-1.778" y1="0.635" x2="-1.524" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.778" y1="-0.635" x2="-1.524" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="1.524" y1="-0.889" x2="1.778" y2="-0.635" width="0.1524" layer="21" curve="90"/>
<wire x1="1.524" y1="0.889" x2="1.778" y2="0.635" width="0.1524" layer="21" curve="-90"/>
<wire x1="-1.778" y1="-0.635" x2="-1.778" y2="0.635" width="0.1524" layer="51"/>
<wire x1="-1.524" y1="0.889" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="0.762" x2="-1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-1.524" y1="-0.889" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="-1.143" y1="-0.762" x2="-1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="0.762" x2="-1.143" y2="0.762" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.143" y1="-0.762" x2="-1.143" y2="-0.762" width="0.1524" layer="21"/>
<wire x1="1.524" y1="0.889" x2="1.27" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.524" y1="-0.889" x2="1.27" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.635" x2="1.778" y2="0.635" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.0066" y="1.1684" size="0.9906" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.1336" y="-2.3114" size="0.9906" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-0.254" x2="-1.778" y2="0.254" layer="51"/>
<rectangle x1="1.778" y1="-0.254" x2="2.032" y2="0.254" layer="51"/>
</package>
<package name="0204/7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 7.5 mm</description>
<wire x1="3.81" y1="0" x2="2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-3.81" y1="0" x2="-2.921" y2="0" width="0.508" layer="51"/>
<wire x1="-2.54" y1="0.762" x2="-2.286" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.286" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="-1.016" x2="2.54" y2="-0.762" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="1.016" x2="2.54" y2="0.762" width="0.1524" layer="21" curve="-90"/>
<wire x1="-2.54" y1="-0.762" x2="-2.54" y2="0.762" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="1.016" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="0.889" x2="-1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.016" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-1.778" y1="-0.889" x2="-1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="0.889" x2="-1.778" y2="0.889" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="1.778" y1="-0.889" x2="-1.778" y2="-0.889" width="0.1524" layer="21"/>
<wire x1="2.286" y1="1.016" x2="1.905" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.286" y1="-1.016" x2="1.905" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-0.762" x2="2.54" y2="0.762" width="0.1524" layer="21"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.2954" size="0.9906" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.6256" y="-0.4826" size="0.9906" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="2.54" y1="-0.254" x2="2.921" y2="0.254" layer="21"/>
<rectangle x1="-2.921" y1="-0.254" x2="-2.54" y2="0.254" layer="21"/>
</package>
<package name="0204V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0204, grid 2.5 mm</description>
<wire x1="-1.27" y1="0" x2="1.27" y2="0" width="0.508" layer="51"/>
<wire x1="-0.127" y1="0" x2="0.127" y2="0" width="0.508" layer="21"/>
<circle x="-1.27" y="0" radius="0.889" width="0.1524" layer="51"/>
<circle x="-1.27" y="0" radius="0.635" width="0.0508" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.1336" y="1.1684" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.1336" y="-2.3114" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0207/10">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 10 mm</description>
<wire x1="5.08" y1="0" x2="4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-5.08" y1="0" x2="-4.064" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.048" y="1.524" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.2606" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
</package>
<package name="0207/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 12 mm</description>
<wire x1="6.35" y1="0" x2="5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.334" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="4.445" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-4.445" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="4.445" y1="-0.3048" x2="5.3086" y2="0.3048" layer="21"/>
<rectangle x1="-5.3086" y1="-0.3048" x2="-4.445" y2="0.3048" layer="21"/>
</package>
<package name="0207/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 15mm</description>
<wire x1="7.62" y1="0" x2="6.604" y2="0" width="0.6096" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.604" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="21"/>
<wire x1="5.715" y1="0" x2="4.064" y2="0" width="0.6096" layer="21"/>
<wire x1="-5.715" y1="0" x2="-4.064" y2="0" width="0.6096" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="0.8128" shape="octagon"/>
<text x="-3.175" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="3.175" y1="-0.3048" x2="4.0386" y2="0.3048" layer="21"/>
<rectangle x1="-4.0386" y1="-0.3048" x2="-3.175" y2="0.3048" layer="21"/>
<rectangle x1="5.715" y1="-0.3048" x2="6.5786" y2="0.3048" layer="21"/>
<rectangle x1="-6.5786" y1="-0.3048" x2="-5.715" y2="0.3048" layer="21"/>
</package>
<package name="0207/2V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 2.5 mm</description>
<wire x1="-1.27" y1="0" x2="-0.381" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.254" y1="0" x2="0.254" y2="0" width="0.6096" layer="21"/>
<wire x1="0.381" y1="0" x2="1.27" y2="0" width="0.6096" layer="51"/>
<circle x="-1.27" y="0" radius="1.27" width="0.1524" layer="21"/>
<circle x="-1.27" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-0.0508" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.0508" y="-2.2352" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0207/5V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 5 mm</description>
<wire x1="-2.54" y1="0" x2="-0.889" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.762" y1="0" x2="0.762" y2="0" width="0.6096" layer="21"/>
<wire x1="0.889" y1="0" x2="2.54" y2="0" width="0.6096" layer="51"/>
<circle x="-2.54" y="0" radius="1.27" width="0.1016" layer="21"/>
<circle x="-2.54" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="0.8128" shape="octagon"/>
<text x="-1.143" y="0.889" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.143" y="-2.159" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="0207/7">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0207, grid 7.5 mm</description>
<wire x1="-3.81" y1="0" x2="-3.429" y2="0" width="0.6096" layer="51"/>
<wire x1="-3.175" y1="0.889" x2="-2.921" y2="1.143" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-2.921" y2="-1.143" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="-1.143" x2="3.175" y2="-0.889" width="0.1524" layer="21" curve="90"/>
<wire x1="2.921" y1="1.143" x2="3.175" y2="0.889" width="0.1524" layer="21" curve="-90"/>
<wire x1="-3.175" y1="-0.889" x2="-3.175" y2="0.889" width="0.1524" layer="51"/>
<wire x1="-2.921" y1="1.143" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="1.016" x2="-2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="-2.921" y1="-1.143" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="-2.413" y1="-1.016" x2="-2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="1.016" x2="-2.413" y2="1.016" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="2.413" y1="-1.016" x2="-2.413" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="2.921" y1="1.143" x2="2.54" y2="1.143" width="0.1524" layer="21"/>
<wire x1="2.921" y1="-1.143" x2="2.54" y2="-1.143" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-0.889" x2="3.175" y2="0.889" width="0.1524" layer="51"/>
<wire x1="3.429" y1="0" x2="3.81" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-3.81" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="3.81" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.54" y="1.397" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.286" y="-0.5588" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-3.429" y1="-0.3048" x2="-3.175" y2="0.3048" layer="51"/>
<rectangle x1="3.175" y1="-0.3048" x2="3.429" y2="0.3048" layer="51"/>
</package>
<package name="0309/10">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 10mm</description>
<wire x1="-4.699" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.318" y1="1.27" x2="-4.064" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.064" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="-1.524" x2="4.318" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="1.524" x2="4.318" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.318" y2="1.27" width="0.1524" layer="51"/>
<wire x1="-4.064" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.397" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.524" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.397" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="-3.302" y2="1.397" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="-3.302" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="4.064" y1="1.524" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="4.064" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-1.27" x2="4.318" y2="1.27" width="0.1524" layer="51"/>
<wire x1="5.08" y1="0" x2="4.699" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="0.8128" shape="octagon"/>
<text x="-4.191" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.6228" y1="-0.3048" x2="-4.318" y2="0.3048" layer="51"/>
<rectangle x1="4.318" y1="-0.3048" x2="4.6228" y2="0.3048" layer="51"/>
</package>
<package name="0309/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 12.5 mm</description>
<wire x1="6.35" y1="0" x2="5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="-4.318" y1="1.27" x2="-4.064" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.064" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="-1.524" x2="4.318" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="4.064" y1="1.524" x2="4.318" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="-4.318" y1="-1.27" x2="-4.318" y2="1.27" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="1.524" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="1.397" x2="-3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="-4.064" y1="-1.524" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="-3.302" y1="-1.397" x2="-3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="1.397" x2="-3.302" y2="1.397" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="3.302" y1="-1.397" x2="-3.302" y2="-1.397" width="0.1524" layer="21"/>
<wire x1="4.064" y1="1.524" x2="3.429" y2="1.524" width="0.1524" layer="21"/>
<wire x1="4.064" y1="-1.524" x2="3.429" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.318" y1="-1.27" x2="4.318" y2="1.27" width="0.1524" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.8128" shape="octagon"/>
<text x="-4.191" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.6858" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="4.318" y1="-0.3048" x2="5.1816" y2="0.3048" layer="21"/>
<rectangle x1="-5.1816" y1="-0.3048" x2="-4.318" y2="0.3048" layer="21"/>
</package>
<package name="0309V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0309, grid 2.5 mm</description>
<wire x1="1.27" y1="0" x2="0.635" y2="0" width="0.6096" layer="51"/>
<wire x1="-0.635" y1="0" x2="-1.27" y2="0" width="0.6096" layer="51"/>
<circle x="-1.27" y="0" radius="1.524" width="0.1524" layer="21"/>
<circle x="-1.27" y="0" radius="0.762" width="0.1524" layer="51"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="0.254" y="1.016" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0.254" y="-2.2098" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="0.254" y1="-0.3048" x2="0.5588" y2="0.3048" layer="51"/>
<rectangle x1="-0.635" y1="-0.3048" x2="-0.3302" y2="0.3048" layer="51"/>
<rectangle x1="-0.3302" y1="-0.3048" x2="0.254" y2="0.3048" layer="21"/>
</package>
<package name="0411/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 12.5 mm</description>
<wire x1="6.35" y1="0" x2="5.461" y2="0" width="0.762" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.461" y2="0" width="0.762" layer="51"/>
<wire x1="5.08" y1="-1.651" x2="5.08" y2="1.651" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.032" x2="5.08" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="-1.651" x2="-4.699" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="4.699" y1="-2.032" x2="5.08" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.651" x2="-4.699" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="2.032" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="1.905" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.032" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="-1.905" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="3.937" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="3.937" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.651" x2="-5.08" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="2.032" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-2.032" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<pad name="1" x="-6.35" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="0.9144" shape="octagon"/>
<text x="-5.08" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.5814" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.3594" y1="-0.381" x2="-5.08" y2="0.381" layer="21"/>
<rectangle x1="5.08" y1="-0.381" x2="5.3594" y2="0.381" layer="21"/>
</package>
<package name="0411/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 15 mm</description>
<wire x1="5.08" y1="-1.651" x2="5.08" y2="1.651" width="0.1524" layer="21"/>
<wire x1="4.699" y1="2.032" x2="5.08" y2="1.651" width="0.1524" layer="21" curve="-90"/>
<wire x1="-5.08" y1="-1.651" x2="-4.699" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="4.699" y1="-2.032" x2="5.08" y2="-1.651" width="0.1524" layer="21" curve="90"/>
<wire x1="-5.08" y1="1.651" x2="-4.699" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="2.032" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="1.905" x2="4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-2.032" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="3.937" y1="-1.905" x2="4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="1.905" x2="3.937" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-3.937" y1="-1.905" x2="3.937" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.651" x2="-5.08" y2="-1.651" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="2.032" x2="-4.064" y2="2.032" width="0.1524" layer="21"/>
<wire x1="-4.699" y1="-2.032" x2="-4.064" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="0" x2="-6.35" y2="0" width="0.762" layer="51"/>
<wire x1="6.35" y1="0" x2="7.62" y2="0" width="0.762" layer="51"/>
<pad name="1" x="-7.62" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="0.9144" shape="octagon"/>
<text x="-5.08" y="2.413" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.5814" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="5.08" y1="-0.381" x2="6.477" y2="0.381" layer="21"/>
<rectangle x1="-6.477" y1="-0.381" x2="-5.08" y2="0.381" layer="21"/>
</package>
<package name="0411V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0411, grid 3.81 mm</description>
<wire x1="1.27" y1="0" x2="0.3048" y2="0" width="0.762" layer="51"/>
<wire x1="-1.5748" y1="0" x2="-2.54" y2="0" width="0.762" layer="51"/>
<circle x="-2.54" y="0" radius="2.032" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.016" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="0.9144" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.9144" shape="octagon"/>
<text x="-0.508" y="1.143" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.5334" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.4732" y1="-0.381" x2="0.2032" y2="0.381" layer="21"/>
</package>
<package name="0414/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0414, grid 15 mm</description>
<wire x1="7.62" y1="0" x2="6.604" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.604" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.096" y1="1.905" x2="-5.842" y2="2.159" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.096" y1="-1.905" x2="-5.842" y2="-2.159" width="0.1524" layer="21" curve="90"/>
<wire x1="5.842" y1="-2.159" x2="6.096" y2="-1.905" width="0.1524" layer="21" curve="90"/>
<wire x1="5.842" y1="2.159" x2="6.096" y2="1.905" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.096" y1="-1.905" x2="-6.096" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-5.842" y1="2.159" x2="-4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="2.032" x2="-4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="-5.842" y1="-2.159" x2="-4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="-4.826" y1="-2.032" x2="-4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="2.032" x2="-4.826" y2="2.032" width="0.1524" layer="21"/>
<wire x1="4.826" y1="-2.032" x2="4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="4.826" y1="-2.032" x2="-4.826" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="5.842" y1="2.159" x2="4.953" y2="2.159" width="0.1524" layer="21"/>
<wire x1="5.842" y1="-2.159" x2="4.953" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-1.905" x2="6.096" y2="1.905" width="0.1524" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.096" y="2.5654" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="6.096" y1="-0.4064" x2="6.5024" y2="0.4064" layer="21"/>
<rectangle x1="-6.5024" y1="-0.4064" x2="-6.096" y2="0.4064" layer="21"/>
</package>
<package name="0414V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0414, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-2.54" y1="0" x2="-1.397" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="2.159" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.381" y="1.1684" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.381" y="-2.3622" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.2954" y1="-0.4064" x2="1.2954" y2="0.4064" layer="21"/>
</package>
<package name="0617/17">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 17.5 mm</description>
<wire x1="-8.89" y1="0" x2="-8.636" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.874" y1="3.048" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="2.794" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="-3.048" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="-2.794" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="-6.731" y2="2.794" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="-6.731" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="7.874" y1="3.048" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="7.874" y1="-3.048" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="-2.667" x2="-8.255" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="1.016" x2="-8.255" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-8.255" y1="1.016" x2="-8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-2.667" x2="8.255" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="8.255" y1="1.016" x2="8.255" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="8.255" y1="1.016" x2="8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="8.636" y1="0" x2="8.89" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="2.667" x2="-7.874" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="7.874" y1="3.048" x2="8.255" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.255" y1="-2.667" x2="-7.874" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="7.874" y1="-3.048" x2="8.255" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-8.89" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="8.89" y="0" drill="1.016" shape="octagon"/>
<text x="-8.128" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.096" y="-0.7112" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-8.5344" y1="-0.4064" x2="-8.2296" y2="0.4064" layer="51"/>
<rectangle x1="8.2296" y1="-0.4064" x2="8.5344" y2="0.4064" layer="51"/>
</package>
<package name="0617/22">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 22.5 mm</description>
<wire x1="-10.287" y1="0" x2="-11.43" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="-2.667" x2="-8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="3.048" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="2.794" x2="-6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-7.874" y1="-3.048" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.731" y1="-2.794" x2="-6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="2.794" x2="-6.731" y2="2.794" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.794" x2="-6.731" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="7.874" y1="3.048" x2="6.985" y2="3.048" width="0.1524" layer="21"/>
<wire x1="7.874" y1="-3.048" x2="6.985" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-2.667" x2="8.255" y2="2.667" width="0.1524" layer="21"/>
<wire x1="11.43" y1="0" x2="10.287" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.255" y1="2.667" x2="-7.874" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.255" y1="-2.667" x2="-7.874" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="7.874" y1="3.048" x2="8.255" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="7.874" y1="-3.048" x2="8.255" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-8.255" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.477" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.1854" y1="-0.4064" x2="-8.255" y2="0.4064" layer="21"/>
<rectangle x1="8.255" y1="-0.4064" x2="10.1854" y2="0.4064" layer="21"/>
</package>
<package name="0617V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0617, grid 5 mm</description>
<wire x1="-2.54" y1="0" x2="-1.27" y2="0" width="0.8128" layer="51"/>
<wire x1="1.27" y1="0" x2="2.54" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="3.048" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="0.635" y="1.4224" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0.635" y="-2.6162" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.3208" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="0922/22">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0922, grid 22.5 mm</description>
<wire x1="11.43" y1="0" x2="10.795" y2="0" width="0.8128" layer="51"/>
<wire x1="-11.43" y1="0" x2="-10.795" y2="0" width="0.8128" layer="51"/>
<wire x1="-10.16" y1="-4.191" x2="-10.16" y2="4.191" width="0.1524" layer="21"/>
<wire x1="-9.779" y1="4.572" x2="-8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="-8.636" y1="4.318" x2="-8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="-9.779" y1="-4.572" x2="-8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="-8.636" y1="-4.318" x2="-8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="4.318" x2="8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="4.318" x2="-8.636" y2="4.318" width="0.1524" layer="21"/>
<wire x1="8.636" y1="-4.318" x2="8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="8.636" y1="-4.318" x2="-8.636" y2="-4.318" width="0.1524" layer="21"/>
<wire x1="9.779" y1="4.572" x2="8.89" y2="4.572" width="0.1524" layer="21"/>
<wire x1="9.779" y1="-4.572" x2="8.89" y2="-4.572" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-4.191" x2="10.16" y2="4.191" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-4.191" x2="-9.779" y2="-4.572" width="0.1524" layer="21" curve="90"/>
<wire x1="-10.16" y1="4.191" x2="-9.779" y2="4.572" width="0.1524" layer="21" curve="-90"/>
<wire x1="9.779" y1="-4.572" x2="10.16" y2="-4.191" width="0.1524" layer="21" curve="90"/>
<wire x1="9.779" y1="4.572" x2="10.16" y2="4.191" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-10.16" y="5.1054" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.477" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.7188" y1="-0.4064" x2="-10.16" y2="0.4064" layer="51"/>
<rectangle x1="10.16" y1="-0.4064" x2="10.3124" y2="0.4064" layer="21"/>
<rectangle x1="-10.3124" y1="-0.4064" x2="-10.16" y2="0.4064" layer="21"/>
<rectangle x1="10.16" y1="-0.4064" x2="10.7188" y2="0.4064" layer="51"/>
</package>
<package name="P0613V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0613, grid 5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-2.54" y1="0" x2="-1.397" y2="0" width="0.8128" layer="51"/>
<circle x="-2.54" y="0" radius="2.286" width="0.1524" layer="21"/>
<circle x="-2.54" y="0" radius="1.143" width="0.1524" layer="51"/>
<pad name="1" x="-2.54" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.254" y="1.143" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.254" y="-2.413" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-1.2954" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="P0613/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0613, grid 15 mm</description>
<wire x1="7.62" y1="0" x2="6.985" y2="0" width="0.8128" layer="51"/>
<wire x1="-7.62" y1="0" x2="-6.985" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.477" y1="2.032" x2="-6.223" y2="2.286" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.477" y1="-2.032" x2="-6.223" y2="-2.286" width="0.1524" layer="21" curve="90"/>
<wire x1="6.223" y1="-2.286" x2="6.477" y2="-2.032" width="0.1524" layer="21" curve="90"/>
<wire x1="6.223" y1="2.286" x2="6.477" y2="2.032" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.223" y1="2.286" x2="-5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="2.159" x2="-5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="-6.223" y1="-2.286" x2="-5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="-5.207" y1="-2.159" x2="-5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="2.159" x2="5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="2.159" x2="-5.207" y2="2.159" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.159" x2="5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="5.207" y1="-2.159" x2="-5.207" y2="-2.159" width="0.1524" layer="21"/>
<wire x1="6.223" y1="2.286" x2="5.334" y2="2.286" width="0.1524" layer="21"/>
<wire x1="6.223" y1="-2.286" x2="5.334" y2="-2.286" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-0.635" x2="6.477" y2="-2.032" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-0.635" x2="6.477" y2="0.635" width="0.1524" layer="51"/>
<wire x1="6.477" y1="2.032" x2="6.477" y2="0.635" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="-2.032" x2="-6.477" y2="-0.635" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="0.635" x2="-6.477" y2="-0.635" width="0.1524" layer="51"/>
<wire x1="-6.477" y1="0.635" x2="-6.477" y2="2.032" width="0.1524" layer="21"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.477" y="2.6924" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.7112" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-7.0358" y1="-0.4064" x2="-6.477" y2="0.4064" layer="51"/>
<rectangle x1="6.477" y1="-0.4064" x2="7.0358" y2="0.4064" layer="51"/>
</package>
<package name="P0817/22">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0817, grid 22.5 mm</description>
<wire x1="-10.414" y1="0" x2="-11.43" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.509" y1="-3.429" x2="-8.509" y2="3.429" width="0.1524" layer="21"/>
<wire x1="-8.128" y1="3.81" x2="-7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="3.556" x2="-7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="-8.128" y1="-3.81" x2="-7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="-3.556" x2="-7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="3.556" x2="7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="3.556" x2="-6.985" y2="3.556" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-3.556" x2="7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="6.985" y1="-3.556" x2="-6.985" y2="-3.556" width="0.1524" layer="21"/>
<wire x1="8.128" y1="3.81" x2="7.239" y2="3.81" width="0.1524" layer="21"/>
<wire x1="8.128" y1="-3.81" x2="7.239" y2="-3.81" width="0.1524" layer="21"/>
<wire x1="8.509" y1="-3.429" x2="8.509" y2="3.429" width="0.1524" layer="21"/>
<wire x1="11.43" y1="0" x2="10.414" y2="0" width="0.8128" layer="51"/>
<wire x1="-8.509" y1="3.429" x2="-8.128" y2="3.81" width="0.1524" layer="21" curve="-90"/>
<wire x1="-8.509" y1="-3.429" x2="-8.128" y2="-3.81" width="0.1524" layer="21" curve="90"/>
<wire x1="8.128" y1="3.81" x2="8.509" y2="3.429" width="0.1524" layer="21" curve="-90"/>
<wire x1="8.128" y1="-3.81" x2="8.509" y2="-3.429" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-11.43" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="11.43" y="0" drill="1.016" shape="octagon"/>
<text x="-8.382" y="4.2164" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-6.223" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="6.604" y="-2.2606" size="1.27" layer="51" ratio="10" rot="R90">0817</text>
<rectangle x1="8.509" y1="-0.4064" x2="10.3124" y2="0.4064" layer="21"/>
<rectangle x1="-10.3124" y1="-0.4064" x2="-8.509" y2="0.4064" layer="21"/>
</package>
<package name="P0817V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0817, grid 6.35 mm</description>
<wire x1="-3.81" y1="0" x2="-5.08" y2="0" width="0.8128" layer="51"/>
<wire x1="1.27" y1="0" x2="0" y2="0" width="0.8128" layer="51"/>
<circle x="-5.08" y="0" radius="3.81" width="0.1524" layer="21"/>
<circle x="-5.08" y="0" radius="1.27" width="0.1524" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="1.016" shape="octagon"/>
<text x="-1.016" y="1.27" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-1.016" y="-2.54" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-6.858" y="2.032" size="1.016" layer="21" ratio="12">0817</text>
<rectangle x1="-3.81" y1="-0.4064" x2="0" y2="0.4064" layer="21"/>
</package>
<package name="V234/12">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V234, grid 12.5 mm</description>
<wire x1="-4.953" y1="1.524" x2="-4.699" y2="1.778" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="1.778" x2="4.953" y2="1.524" width="0.1524" layer="21" curve="-90"/>
<wire x1="4.699" y1="-1.778" x2="4.953" y2="-1.524" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.953" y1="-1.524" x2="-4.699" y2="-1.778" width="0.1524" layer="21" curve="90"/>
<wire x1="-4.699" y1="1.778" x2="4.699" y2="1.778" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="1.524" x2="-4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="4.699" y1="-1.778" x2="-4.699" y2="-1.778" width="0.1524" layer="21"/>
<wire x1="4.953" y1="1.524" x2="4.953" y2="-1.524" width="0.1524" layer="21"/>
<wire x1="6.35" y1="0" x2="5.461" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.35" y1="0" x2="-5.461" y2="0" width="0.8128" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="1.016" shape="octagon"/>
<text x="-4.953" y="2.159" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.81" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="4.953" y1="-0.4064" x2="5.4102" y2="0.4064" layer="21"/>
<rectangle x1="-5.4102" y1="-0.4064" x2="-4.953" y2="0.4064" layer="21"/>
</package>
<package name="V235/17">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V235, grid 17.78 mm</description>
<wire x1="-6.731" y1="2.921" x2="6.731" y2="2.921" width="0.1524" layer="21"/>
<wire x1="-7.112" y1="2.54" x2="-7.112" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="6.731" y1="-2.921" x2="-6.731" y2="-2.921" width="0.1524" layer="21"/>
<wire x1="7.112" y1="2.54" x2="7.112" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="8.89" y1="0" x2="7.874" y2="0" width="1.016" layer="51"/>
<wire x1="-7.874" y1="0" x2="-8.89" y2="0" width="1.016" layer="51"/>
<wire x1="-7.112" y1="-2.54" x2="-6.731" y2="-2.921" width="0.1524" layer="21" curve="90"/>
<wire x1="6.731" y1="2.921" x2="7.112" y2="2.54" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.731" y1="-2.921" x2="7.112" y2="-2.54" width="0.1524" layer="21" curve="90"/>
<wire x1="-7.112" y1="2.54" x2="-6.731" y2="2.921" width="0.1524" layer="21" curve="-90"/>
<pad name="1" x="-8.89" y="0" drill="1.1938" shape="octagon"/>
<pad name="2" x="8.89" y="0" drill="1.1938" shape="octagon"/>
<text x="-6.858" y="3.302" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-5.842" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="7.112" y1="-0.508" x2="7.747" y2="0.508" layer="21"/>
<rectangle x1="-7.747" y1="-0.508" x2="-7.112" y2="0.508" layer="21"/>
</package>
<package name="V526-0">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type V526-0, grid 2.5 mm</description>
<wire x1="-2.54" y1="1.016" x2="-2.286" y2="1.27" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.286" y1="1.27" x2="2.54" y2="1.016" width="0.1524" layer="21" curve="-90"/>
<wire x1="2.286" y1="-1.27" x2="2.54" y2="-1.016" width="0.1524" layer="21" curve="90"/>
<wire x1="-2.54" y1="-1.016" x2="-2.286" y2="-1.27" width="0.1524" layer="21" curve="90"/>
<wire x1="2.286" y1="1.27" x2="-2.286" y2="1.27" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.016" x2="2.54" y2="1.016" width="0.1524" layer="21"/>
<wire x1="-2.286" y1="-1.27" x2="2.286" y2="-1.27" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.016" x2="-2.54" y2="-1.016" width="0.1524" layer="21"/>
<pad name="1" x="-1.27" y="0" drill="0.8128" shape="octagon"/>
<pad name="2" x="1.27" y="0" drill="0.8128" shape="octagon"/>
<text x="-2.413" y="1.651" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-2.413" y="-2.794" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0102R">
<description>&lt;b&gt;CECC Size RC2211&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1" y1="-0.5" x2="1" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="-0.5" x2="1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="0.5" x2="-1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.5" x2="-1" y2="-0.5" width="0.2032" layer="51"/>
<smd name="1" x="-0.9" y="0" dx="0.5" dy="1.3" layer="1"/>
<smd name="2" x="0.9" y="0" dx="0.5" dy="1.3" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0102W">
<description>&lt;b&gt;CECC Size RC2211&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1" y1="-0.5" x2="1" y2="-0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="-0.5" x2="1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="1" y1="0.5" x2="-1" y2="0.5" width="0.2032" layer="51"/>
<wire x1="-1" y1="0.5" x2="-1" y2="-0.5" width="0.2032" layer="51"/>
<smd name="1" x="-0.95" y="0" dx="0.6" dy="1.3" layer="1"/>
<smd name="2" x="0.95" y="0" dx="0.6" dy="1.3" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0204R">
<description>&lt;b&gt;CECC Size RC3715&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1.7" y1="-0.6" x2="1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="-0.6" x2="1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="0.6" x2="-1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="-1.7" y1="0.6" x2="-1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="0.938" y1="0.6" x2="-0.938" y2="0.6" width="0.2032" layer="21"/>
<wire x1="-0.938" y1="-0.6" x2="0.938" y2="-0.6" width="0.2032" layer="21"/>
<smd name="1" x="-1.5" y="0" dx="0.8" dy="1.6" layer="1"/>
<smd name="2" x="1.5" y="0" dx="0.8" dy="1.6" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0204W">
<description>&lt;b&gt;CECC Size RC3715&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-1.7" y1="-0.6" x2="1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="-0.6" x2="1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="1.7" y1="0.6" x2="-1.7" y2="0.6" width="0.2032" layer="51"/>
<wire x1="-1.7" y1="0.6" x2="-1.7" y2="-0.6" width="0.2032" layer="51"/>
<wire x1="0.684" y1="0.6" x2="-0.684" y2="0.6" width="0.2032" layer="21"/>
<wire x1="-0.684" y1="-0.6" x2="0.684" y2="-0.6" width="0.2032" layer="21"/>
<smd name="1" x="-1.5" y="0" dx="1.2" dy="1.6" layer="1"/>
<smd name="2" x="1.5" y="0" dx="1.2" dy="1.6" layer="1"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0207R">
<description>&lt;b&gt;CECC Size RC6123&lt;/b&gt; Reflow Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-2.8" y1="-1" x2="2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="-1" x2="2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="1" x2="-2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="-2.8" y1="1" x2="-2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="1.2125" y1="1" x2="-1.2125" y2="1" width="0.2032" layer="21"/>
<wire x1="-1.2125" y1="-1" x2="1.2125" y2="-1" width="0.2032" layer="21"/>
<smd name="1" x="-2.25" y="0" dx="1.6" dy="2.5" layer="1"/>
<smd name="2" x="2.25" y="0" dx="1.6" dy="2.5" layer="1"/>
<text x="-2.2225" y="1.5875" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.2225" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="MINI_MELF-0207W">
<description>&lt;b&gt;CECC Size RC6123&lt;/b&gt; Wave Soldering&lt;p&gt;
source Beyschlag</description>
<wire x1="-2.8" y1="-1" x2="2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="-1" x2="2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="2.8" y1="1" x2="-2.8" y2="1" width="0.2032" layer="51"/>
<wire x1="-2.8" y1="1" x2="-2.8" y2="-1" width="0.2032" layer="51"/>
<wire x1="1.149" y1="1" x2="-1.149" y2="1" width="0.2032" layer="21"/>
<wire x1="-1.149" y1="-1" x2="1.149" y2="-1" width="0.2032" layer="21"/>
<smd name="1" x="-2.6" y="0" dx="2.4" dy="2.5" layer="1"/>
<smd name="2" x="2.6" y="0" dx="2.4" dy="2.5" layer="1"/>
<text x="-2.54" y="1.5875" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="0922V">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type 0922, grid 7.5 mm</description>
<wire x1="2.54" y1="0" x2="1.397" y2="0" width="0.8128" layer="51"/>
<wire x1="-5.08" y1="0" x2="-3.81" y2="0" width="0.8128" layer="51"/>
<circle x="-5.08" y="0" radius="4.572" width="0.1524" layer="21"/>
<circle x="-5.08" y="0" radius="1.905" width="0.1524" layer="21"/>
<pad name="1" x="-5.08" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="2.54" y="0" drill="1.016" shape="octagon"/>
<text x="-0.508" y="1.6764" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-0.508" y="-2.9972" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="-6.858" y="2.54" size="1.016" layer="21" ratio="12">0922</text>
<rectangle x1="-3.81" y1="-0.4064" x2="1.3208" y2="0.4064" layer="21"/>
</package>
<package name="RDH/15">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;&lt;p&gt;
type RDH, grid 15 mm</description>
<wire x1="-7.62" y1="0" x2="-6.858" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.096" y1="3.048" x2="-5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="2.794" x2="-5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="-6.096" y1="-3.048" x2="-5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-4.953" y1="-2.794" x2="-5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.794" x2="5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="2.794" x2="-4.953" y2="2.794" width="0.1524" layer="21"/>
<wire x1="4.953" y1="-2.794" x2="5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="4.953" y1="-2.794" x2="-4.953" y2="-2.794" width="0.1524" layer="21"/>
<wire x1="6.096" y1="3.048" x2="5.207" y2="3.048" width="0.1524" layer="21"/>
<wire x1="6.096" y1="-3.048" x2="5.207" y2="-3.048" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="-2.667" x2="-6.477" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="-6.477" y1="1.016" x2="-6.477" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="-6.477" y1="1.016" x2="-6.477" y2="2.667" width="0.1524" layer="21"/>
<wire x1="6.477" y1="-2.667" x2="6.477" y2="-1.016" width="0.1524" layer="21"/>
<wire x1="6.477" y1="1.016" x2="6.477" y2="-1.016" width="0.1524" layer="51"/>
<wire x1="6.477" y1="1.016" x2="6.477" y2="2.667" width="0.1524" layer="21"/>
<wire x1="6.858" y1="0" x2="7.62" y2="0" width="0.8128" layer="51"/>
<wire x1="-6.477" y1="2.667" x2="-6.096" y2="3.048" width="0.1524" layer="21" curve="-90"/>
<wire x1="6.096" y1="3.048" x2="6.477" y2="2.667" width="0.1524" layer="21" curve="-90"/>
<wire x1="-6.477" y1="-2.667" x2="-6.096" y2="-3.048" width="0.1524" layer="21" curve="90"/>
<wire x1="6.096" y1="-3.048" x2="6.477" y2="-2.667" width="0.1524" layer="21" curve="90"/>
<pad name="1" x="-7.62" y="0" drill="1.016" shape="octagon"/>
<pad name="2" x="7.62" y="0" drill="1.016" shape="octagon"/>
<text x="-6.35" y="3.4544" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.318" y="-0.5842" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<text x="4.572" y="-1.7272" size="1.27" layer="51" ratio="10" rot="R90">RDH</text>
<rectangle x1="-6.7564" y1="-0.4064" x2="-6.4516" y2="0.4064" layer="51"/>
<rectangle x1="6.4516" y1="-0.4064" x2="6.7564" y2="0.4064" layer="51"/>
</package>
<package name="MINI_MELF-0102AX">
<description>&lt;b&gt;Mini MELF 0102 Axial&lt;/b&gt;</description>
<circle x="0" y="0" radius="0.6" width="0" layer="51"/>
<circle x="0" y="0" radius="0.6" width="0" layer="52"/>
<smd name="1" x="0" y="0" dx="1.9" dy="1.9" layer="1" roundness="100"/>
<smd name="2" x="0" y="0" dx="1.9" dy="1.9" layer="16" roundness="100"/>
<text x="-1.27" y="0.9525" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.2225" size="1.27" layer="27">&gt;VALUE</text>
<hole x="0" y="0" drill="1.3"/>
</package>
<package name="R0201">
<description>&lt;b&gt;RESISTOR&lt;/b&gt; chip&lt;p&gt;
Source: http://www.vishay.com/docs/20008/dcrcw.pdf</description>
<smd name="1" x="-0.255" y="0" dx="0.28" dy="0.43" layer="1"/>
<smd name="2" x="0.255" y="0" dx="0.28" dy="0.43" layer="1"/>
<text x="-0.635" y="0.635" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.635" y="-1.905" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.3" y1="-0.15" x2="-0.15" y2="0.15" layer="51"/>
<rectangle x1="0.15" y1="-0.15" x2="0.3" y2="0.15" layer="51"/>
<rectangle x1="-0.15" y1="-0.15" x2="0.15" y2="0.15" layer="21"/>
</package>
<package name="VTA52">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR52&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-15.24" y1="0" x2="-13.97" y2="0" width="0.6096" layer="51"/>
<wire x1="12.6225" y1="0.025" x2="12.6225" y2="4.725" width="0.1524" layer="21"/>
<wire x1="12.6225" y1="4.725" x2="-12.6225" y2="4.725" width="0.1524" layer="21"/>
<wire x1="-12.6225" y1="4.725" x2="-12.6225" y2="0.025" width="0.1524" layer="21"/>
<wire x1="-12.6225" y1="0.025" x2="-12.6225" y2="-4.65" width="0.1524" layer="21"/>
<wire x1="-12.6225" y1="-4.65" x2="12.6225" y2="-4.65" width="0.1524" layer="21"/>
<wire x1="12.6225" y1="-4.65" x2="12.6225" y2="0.025" width="0.1524" layer="21"/>
<wire x1="13.97" y1="0" x2="15.24" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-15.24" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="15.24" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="5.08" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-13.97" y1="-0.3048" x2="-12.5675" y2="0.3048" layer="21"/>
<rectangle x1="12.5675" y1="-0.3048" x2="13.97" y2="0.3048" layer="21"/>
</package>
<package name="VTA53">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR53&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-12.065" y1="0" x2="-10.795" y2="0" width="0.6096" layer="51"/>
<wire x1="9.8975" y1="0" x2="9.8975" y2="4.7" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="4.7" x2="-9.8975" y2="4.7" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="4.7" x2="-9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="0" x2="-9.8975" y2="-4.675" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="-4.675" x2="9.8975" y2="-4.675" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="-4.675" x2="9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="10.795" y1="0" x2="12.065" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-12.065" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="12.065" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="5.08" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.795" y1="-0.3048" x2="-9.8425" y2="0.3048" layer="21"/>
<rectangle x1="9.8425" y1="-0.3048" x2="10.795" y2="0.3048" layer="21"/>
</package>
<package name="VTA54">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR54&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-12.065" y1="0" x2="-10.795" y2="0" width="0.6096" layer="51"/>
<wire x1="9.8975" y1="0" x2="9.8975" y2="3.3" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="3.3" x2="-9.8975" y2="3.3" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="3.3" x2="-9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="0" x2="-9.8975" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="-9.8975" y1="-3.3" x2="9.8975" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="9.8975" y1="-3.3" x2="9.8975" y2="0" width="0.1524" layer="21"/>
<wire x1="10.795" y1="0" x2="12.065" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-12.065" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="12.065" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-10.795" y1="-0.3048" x2="-9.8425" y2="0.3048" layer="21"/>
<rectangle x1="9.8425" y1="-0.3048" x2="10.795" y2="0.3048" layer="21"/>
</package>
<package name="VTA55">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR55&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-8.255" y1="0" x2="-6.985" y2="0" width="0.6096" layer="51"/>
<wire x1="6.405" y1="0" x2="6.405" y2="3.3" width="0.1524" layer="21"/>
<wire x1="6.405" y1="3.3" x2="-6.405" y2="3.3" width="0.1524" layer="21"/>
<wire x1="-6.405" y1="3.3" x2="-6.405" y2="0" width="0.1524" layer="21"/>
<wire x1="-6.405" y1="0" x2="-6.405" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="-6.405" y1="-3.3" x2="6.405" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="6.405" y1="-3.3" x2="6.405" y2="0" width="0.1524" layer="21"/>
<wire x1="6.985" y1="0" x2="8.255" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-8.255" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="8.255" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-6.985" y1="-0.3048" x2="-6.35" y2="0.3048" layer="21"/>
<rectangle x1="6.35" y1="-0.3048" x2="6.985" y2="0.3048" layer="21"/>
</package>
<package name="VTA56">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RBR56&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-6.35" y1="0" x2="-5.08" y2="0" width="0.6096" layer="51"/>
<wire x1="4.5" y1="0" x2="4.5" y2="3.3" width="0.1524" layer="21"/>
<wire x1="4.5" y1="3.3" x2="-4.5" y2="3.3" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="3.3" x2="-4.5" y2="0" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="0" x2="-4.5" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="-4.5" y1="-3.3" x2="4.5" y2="-3.3" width="0.1524" layer="21"/>
<wire x1="4.5" y1="-3.3" x2="4.5" y2="0" width="0.1524" layer="21"/>
<wire x1="5.08" y1="0" x2="6.35" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="1.1" shape="octagon"/>
<text x="-3.81" y="3.81" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.08" y1="-0.3048" x2="-4.445" y2="0.3048" layer="21"/>
<rectangle x1="4.445" y1="-0.3048" x2="5.08" y2="0.3048" layer="21"/>
</package>
<package name="VMTA55">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RNC55&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-5.08" y1="0" x2="-4.26" y2="0" width="0.6096" layer="51"/>
<wire x1="3.3375" y1="-1.45" x2="3.3375" y2="1.45" width="0.1524" layer="21"/>
<wire x1="3.3375" y1="1.45" x2="-3.3625" y2="1.45" width="0.1524" layer="21"/>
<wire x1="-3.3625" y1="1.45" x2="-3.3625" y2="-1.45" width="0.1524" layer="21"/>
<wire x1="-3.3625" y1="-1.45" x2="3.3375" y2="-1.45" width="0.1524" layer="21"/>
<wire x1="4.235" y1="0" x2="5.08" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-5.08" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="5.08" y="0" drill="1.1" shape="octagon"/>
<text x="-3.175" y="1.905" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-3.175" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-4.26" y1="-0.3048" x2="-3.3075" y2="0.3048" layer="21"/>
<rectangle x1="3.2825" y1="-0.3048" x2="4.235" y2="0.3048" layer="21"/>
</package>
<package name="VMTB60">
<description>&lt;b&gt;Bulk Metal® Foil Technology&lt;/b&gt;, Tubular Axial Lead Resistors, Meets or Exceeds MIL-R-39005 Requirements&lt;p&gt;
MIL SIZE RNC60&lt;br&gt;
Source: VISHAY .. vta56.pdf</description>
<wire x1="-6.35" y1="0" x2="-5.585" y2="0" width="0.6096" layer="51"/>
<wire x1="4.6875" y1="-1.95" x2="4.6875" y2="1.95" width="0.1524" layer="21"/>
<wire x1="4.6875" y1="1.95" x2="-4.6875" y2="1.95" width="0.1524" layer="21"/>
<wire x1="-4.6875" y1="1.95" x2="-4.6875" y2="-1.95" width="0.1524" layer="21"/>
<wire x1="-4.6875" y1="-1.95" x2="4.6875" y2="-1.95" width="0.1524" layer="21"/>
<wire x1="5.585" y1="0" x2="6.35" y2="0" width="0.6096" layer="51"/>
<pad name="1" x="-6.35" y="0" drill="1.1" shape="octagon"/>
<pad name="2" x="6.35" y="0" drill="1.1" shape="octagon"/>
<text x="-4.445" y="2.54" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-4.445" y="-0.635" size="1.27" layer="27" ratio="10">&gt;VALUE</text>
<rectangle x1="-5.585" y1="-0.3048" x2="-4.6325" y2="0.3048" layer="21"/>
<rectangle x1="4.6325" y1="-0.3048" x2="5.585" y2="0.3048" layer="21"/>
</package>
<package name="R4527">
<description>&lt;b&gt;Package 4527&lt;/b&gt;&lt;p&gt;
Source: http://www.vishay.com/docs/31059/wsrhigh.pdf</description>
<wire x1="-5.675" y1="-3.375" x2="5.65" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="5.65" y1="-3.375" x2="5.65" y2="3.375" width="0.2032" layer="51"/>
<wire x1="5.65" y1="3.375" x2="-5.675" y2="3.375" width="0.2032" layer="21"/>
<wire x1="-5.675" y1="3.375" x2="-5.675" y2="-3.375" width="0.2032" layer="51"/>
<smd name="1" x="-4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<smd name="2" x="4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<text x="-5.715" y="3.81" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.715" y="-5.08" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC0001">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3.075" y1="-1.8" x2="3.075" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.075" y1="-1.8" x2="3.075" y2="1.8" width="0.2032" layer="51"/>
<wire x1="3.075" y1="1.8" x2="-3.075" y2="1.8" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="1.606" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="-1.606" x2="-3.075" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.075" y1="1.606" x2="3.075" y2="1.8" width="0.2032" layer="21"/>
<wire x1="3.075" y1="-1.8" x2="3.075" y2="-1.606" width="0.2032" layer="21"/>
<smd name="1" x="-2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<smd name="2" x="2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<text x="-2.544" y="2.229" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.544" y="-3.501" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC0002">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-5.55" y1="3.375" x2="-5.55" y2="-3.375" width="0.2032" layer="51"/>
<wire x1="-5.55" y1="-3.375" x2="5.55" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="5.55" y1="-3.375" x2="5.55" y2="3.375" width="0.2032" layer="51"/>
<wire x1="5.55" y1="3.375" x2="-5.55" y2="3.375" width="0.2032" layer="21"/>
<smd name="1" x="-4.575" y="0.025" dx="3.94" dy="5.84" layer="1"/>
<smd name="2" x="4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<text x="-5.65" y="3.9" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.65" y="-5.15" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC01/2">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-2.45" y1="1.475" x2="-2.45" y2="-1.475" width="0.2032" layer="51"/>
<wire x1="-2.45" y1="-1.475" x2="2.45" y2="-1.475" width="0.2032" layer="21"/>
<wire x1="2.45" y1="-1.475" x2="2.45" y2="1.475" width="0.2032" layer="51"/>
<wire x1="2.45" y1="1.475" x2="-2.45" y2="1.475" width="0.2032" layer="21"/>
<wire x1="-2.45" y1="1.475" x2="-2.45" y2="1.106" width="0.2032" layer="21"/>
<wire x1="-2.45" y1="-1.106" x2="-2.45" y2="-1.475" width="0.2032" layer="21"/>
<wire x1="2.45" y1="1.106" x2="2.45" y2="1.475" width="0.2032" layer="21"/>
<wire x1="2.45" y1="-1.475" x2="2.45" y2="-1.106" width="0.2032" layer="21"/>
<smd name="1" x="-2.1" y="0" dx="2.16" dy="1.78" layer="1"/>
<smd name="2" x="2.1" y="0" dx="2.16" dy="1.78" layer="1"/>
<text x="-2.544" y="1.904" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.544" y="-3.176" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC2515">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="-1.8" width="0.2032" layer="51"/>
<wire x1="-3.075" y1="-1.8" x2="3.05" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.05" y1="-1.8" x2="3.05" y2="1.8" width="0.2032" layer="51"/>
<wire x1="3.05" y1="1.8" x2="-3.075" y2="1.8" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="1.8" x2="-3.075" y2="1.606" width="0.2032" layer="21"/>
<wire x1="-3.075" y1="-1.606" x2="-3.075" y2="-1.8" width="0.2032" layer="21"/>
<wire x1="3.05" y1="1.606" x2="3.05" y2="1.8" width="0.2032" layer="21"/>
<wire x1="3.05" y1="-1.8" x2="3.05" y2="-1.606" width="0.2032" layer="21"/>
<smd name="1" x="-2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<smd name="2" x="2.675" y="0" dx="2.29" dy="2.92" layer="1"/>
<text x="-3.2" y="2.15" size="1.27" layer="25">&gt;NAME</text>
<text x="-3.2" y="-3.4" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC4527">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-5.675" y1="3.4" x2="-5.675" y2="-3.375" width="0.2032" layer="51"/>
<wire x1="-5.675" y1="-3.375" x2="5.675" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="5.675" y1="-3.375" x2="5.675" y2="3.4" width="0.2032" layer="51"/>
<wire x1="5.675" y1="3.4" x2="-5.675" y2="3.4" width="0.2032" layer="21"/>
<smd name="1" x="-4.575" y="0.025" dx="3.94" dy="5.84" layer="1"/>
<smd name="2" x="4.575" y="0" dx="3.94" dy="5.84" layer="1"/>
<text x="-5.775" y="3.925" size="1.27" layer="25">&gt;NAME</text>
<text x="-5.775" y="-5.15" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="WSC6927">
<description>&lt;b&gt;Wirewound Resistors, Precision Power&lt;/b&gt;&lt;p&gt;
Source: VISHAY wscwsn.pdf</description>
<wire x1="-8.65" y1="3.375" x2="-8.65" y2="-3.375" width="0.2032" layer="51"/>
<wire x1="-8.65" y1="-3.375" x2="8.65" y2="-3.375" width="0.2032" layer="21"/>
<wire x1="8.65" y1="-3.375" x2="8.65" y2="3.375" width="0.2032" layer="51"/>
<wire x1="8.65" y1="3.375" x2="-8.65" y2="3.375" width="0.2032" layer="21"/>
<smd name="1" x="-7.95" y="0.025" dx="3.94" dy="5.97" layer="1"/>
<smd name="2" x="7.95" y="0" dx="3.94" dy="5.97" layer="1"/>
<text x="-8.75" y="3.9" size="1.27" layer="25">&gt;NAME</text>
<text x="-8.75" y="-5.15" size="1.27" layer="27">&gt;VALUE</text>
</package>
<package name="R1218">
<description>&lt;b&gt;CRCW1218 Thick Film, Rectangular Chip Resistors&lt;/b&gt;&lt;p&gt;
Source: http://www.vishay.com .. dcrcw.pdf</description>
<wire x1="-0.913" y1="-2.219" x2="0.939" y2="-2.219" width="0.1524" layer="51"/>
<wire x1="0.913" y1="2.219" x2="-0.939" y2="2.219" width="0.1524" layer="51"/>
<smd name="1" x="-1.475" y="0" dx="1.05" dy="4.9" layer="1"/>
<smd name="2" x="1.475" y="0" dx="1.05" dy="4.9" layer="1"/>
<text x="-2.54" y="2.54" size="1.27" layer="25">&gt;NAME</text>
<text x="-2.54" y="-3.81" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.651" y1="-2.3" x2="-0.9009" y2="2.3" layer="51"/>
<rectangle x1="0.9144" y1="-2.3" x2="1.6645" y2="2.3" layer="51"/>
</package>
<package name="1812X7R">
<description>&lt;b&gt;Chip Monolithic Ceramic Capacitors&lt;/b&gt; Medium Voltage High Capacitance for General Use&lt;p&gt;
Source: http://www.murata.com .. GRM43DR72E224KW01.pdf</description>
<wire x1="-1.1" y1="1.5" x2="1.1" y2="1.5" width="0.2032" layer="51"/>
<wire x1="1.1" y1="-1.5" x2="-1.1" y2="-1.5" width="0.2032" layer="51"/>
<wire x1="-0.6" y1="1.5" x2="0.6" y2="1.5" width="0.2032" layer="21"/>
<wire x1="0.6" y1="-1.5" x2="-0.6" y2="-1.5" width="0.2032" layer="21"/>
<smd name="1" x="-1.425" y="0" dx="0.8" dy="3.5" layer="1"/>
<smd name="2" x="1.425" y="0" dx="0.8" dy="3.5" layer="1" rot="R180"/>
<text x="-1.9456" y="1.9958" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.9456" y="-3.7738" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.4" y1="-1.6" x2="-1.1" y2="1.6" layer="51"/>
<rectangle x1="1.1" y1="-1.6" x2="1.4" y2="1.6" layer="51" rot="R180"/>
</package>
<package name="PRL1632">
<description>&lt;b&gt;PRL1632 are realized as 1W for 3.2 × 1.6mm(1206)&lt;/b&gt;&lt;p&gt;
Source: http://www.mouser.com/ds/2/392/products_18-2245.pdf</description>
<wire x1="0.7275" y1="-1.5228" x2="-0.7277" y2="-1.5228" width="0.1524" layer="51"/>
<wire x1="0.7275" y1="1.5228" x2="-0.7152" y2="1.5228" width="0.1524" layer="51"/>
<smd name="2" x="0.822" y="0" dx="1" dy="3.2" layer="1"/>
<smd name="1" x="-0.822" y="0" dx="1" dy="3.2" layer="1"/>
<text x="-1.4" y="1.8" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.4" y="-3" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.8" y1="-1.6" x2="-0.4" y2="1.6" layer="51"/>
<rectangle x1="0.4" y1="-1.6" x2="0.8" y2="1.6" layer="51"/>
</package>
<package name="R01005">
<smd name="1" x="-0.1625" y="0" dx="0.2" dy="0.25" layer="1"/>
<smd name="2" x="0.1625" y="0" dx="0.2" dy="0.25" layer="1"/>
<text x="-0.4" y="0.3" size="1.27" layer="25">&gt;NAME</text>
<text x="-0.4" y="-1.6" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-0.2" y1="-0.1" x2="-0.075" y2="0.1" layer="51"/>
<rectangle x1="0.075" y1="-0.1" x2="0.2" y2="0.1" layer="51"/>
<rectangle x1="-0.15" y1="0.05" x2="0.15" y2="0.1" layer="51"/>
<rectangle x1="-0.15" y1="-0.1" x2="0.15" y2="-0.05" layer="51"/>
</package>
</packages>
<symbols>
<symbol name="R-EU-1">
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="R-EU_" prefix="R" uservalue="yes">
<description>&lt;B&gt;RESISTOR&lt;/B&gt;, European symbol</description>
<gates>
<gate name="G$1" symbol="R-EU-1" x="0" y="0"/>
</gates>
<devices>
<device name="R0402" package="R0402">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0603" package="R0603">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0805" package="R0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0805W" package="R0805W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1206" package="R1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1206W" package="R1206W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1210" package="R1210">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1210W" package="R1210W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2010" package="R2010">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2010W" package="R2010W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2012" package="R2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2012W" package="R2012W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2512" package="R2512">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R2512W" package="R2512W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3216" package="R3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3216W" package="R3216W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3225" package="R3225">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R3225W" package="R3225W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R5025" package="R5025">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R5025W" package="R5025W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R6332" package="R6332">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R6332W" package="R6332W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M0805" package="M0805">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M1206" package="M1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M1406" package="M1406">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M2012" package="M2012">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M2309" package="M2309">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M3216" package="M3216">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M3516" package="M3516">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="M5923" package="M5923">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204/5" package="0204/5">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204/7" package="0204/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0204/2V" package="0204V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/10" package="0207/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/12" package="0207/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/15" package="0207/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/2V" package="0207/2V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/5V" package="0207/5V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0207/7" package="0207/7">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0309/10" package="0309/10">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0309/12" package="0309/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0309/V" package="0309V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0411/12" package="0411/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0411/15" package="0411/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0411/3V" package="0411V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0414/15" package="0414/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0414/5V" package="0414V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0617/17" package="0617/17">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0617/22" package="0617/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0617/5V" package="0617V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0922/22" package="0922/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0613/5V" package="P0613V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0613/15" package="P0613/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0817/22" package="P0817/22">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0817/7V" package="P0817V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="V234/12" package="V234/12">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="V235/17" package="V235/17">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="V526-0" package="V526-0">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0102R" package="MINI_MELF-0102R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0102W" package="MINI_MELF-0102W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0204R" package="MINI_MELF-0204R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0204W" package="MINI_MELF-0204W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0207R" package="MINI_MELF-0207R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0207W" package="MINI_MELF-0207W">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="0922V" package="0922V">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="RDH/15" package="RDH/15">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MELF0102AX" package="MINI_MELF-0102AX">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R0201" package="R0201">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA52" package="VTA52">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA53" package="VTA53">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA54" package="VTA54">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA55" package="VTA55">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VTA56" package="VTA56">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VMTA55" package="VMTA55">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="VMTB60" package="VMTB60">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R4527" package="R4527">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC0001" package="WSC0001">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC0002" package="WSC0002">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC01/2" package="WSC01/2">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC2515" package="WSC2515">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC4527" package="WSC4527">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WSC6927" package="WSC6927">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="R1218" package="R1218">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="1812X7R" package="1812X7R">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="PRL1632" package="PRL1632">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="01005" package="R01005">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="AUTOSTART">
<packages>
</packages>
<symbols>
<symbol name="AUTOSTART-DEUTZ-KONEKTORY">
<wire x1="87.63" y1="-7.62" x2="87.63" y2="-7.3025" width="0.254" layer="94"/>
<wire x1="87.63" y1="-7.3025" x2="87.63" y2="-6.985" width="0.254" layer="94"/>
<wire x1="87.63" y1="-6.985" x2="87.63" y2="-5.08" width="0.254" layer="94"/>
<wire x1="87.63" y1="-5.08" x2="85.09" y2="-5.08" width="0.254" layer="94"/>
<wire x1="85.09" y1="-5.08" x2="82.55" y2="-5.08" width="0.254" layer="94"/>
<wire x1="82.55" y1="-5.08" x2="80.01" y2="-5.08" width="0.254" layer="94"/>
<wire x1="80.01" y1="-5.08" x2="77.47" y2="-5.08" width="0.254" layer="94"/>
<wire x1="77.47" y1="-5.08" x2="74.93" y2="-5.08" width="0.254" layer="94"/>
<wire x1="74.93" y1="-5.08" x2="72.39" y2="-5.08" width="0.254" layer="94"/>
<wire x1="72.39" y1="-5.08" x2="72.39" y2="-7.62" width="0.254" layer="94"/>
<text x="86.36" y="-8.89" size="1.4224" layer="94" rot="R90" align="center">1</text>
<text x="83.82" y="-8.89" size="1.4224" layer="94" rot="R90" align="center">2</text>
<text x="81.28" y="-8.89" size="1.4224" layer="94" rot="R90" align="center">3</text>
<text x="78.74" y="-8.89" size="1.4224" layer="94" rot="R90" align="center">4</text>
<text x="76.2" y="-8.89" size="1.4224" layer="94" rot="R90" align="center">5</text>
<text x="73.66" y="-8.89" size="1.4224" layer="94" rot="R90" align="center">6</text>
<text x="86.36" y="-6.35" size="1.4224" layer="94" rot="R90" align="center">7</text>
<text x="83.82" y="-6.35" size="1.4224" layer="94" rot="R90" align="center">8</text>
<text x="81.28" y="-6.35" size="1.4224" layer="94" rot="R90" align="center">9</text>
<text x="78.74" y="-6.35" size="1.4224" layer="94" rot="R90" align="center">10</text>
<text x="71.12" y="-7.62" size="1.27" layer="96" rot="MR90" align="top-center">MINIFIT12</text>
<text x="88.9" y="-7.62" size="1.778" layer="95" rot="MR0" align="center-right">K3</text>
<text x="76.2" y="-6.35" size="1.4224" layer="94" rot="R90" align="center">11</text>
<text x="73.66" y="-6.35" size="1.4224" layer="94" rot="R90" align="center">12</text>
<pin name="K3.1" x="86.36" y="-12.7" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="K3.2" x="83.82" y="-12.7" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="K3.3" x="81.28" y="-12.7" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="K3.4" x="78.74" y="-12.7" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="K3.5" x="76.2" y="-12.7" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="K3.6" x="73.66" y="-12.7" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="K3.7" x="86.36" y="-2.54" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="K3.8" x="83.82" y="-2.54" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="K3.9" x="81.28" y="-2.54" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="K3.10" x="78.74" y="-2.54" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="K3.11" x="76.2" y="-2.54" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="K3.12" x="73.66" y="-2.54" visible="off" length="short" direction="pas" rot="R270"/>
<rectangle x1="79.629" y1="-5.715" x2="80.391" y2="-3.683" layer="94" rot="R90"/>
<wire x1="87.63" y1="-7.62" x2="87.63" y2="-10.16" width="0.254" layer="94"/>
<wire x1="87.63" y1="-10.16" x2="85.09" y2="-10.16" width="0.254" layer="94"/>
<wire x1="85.09" y1="-10.16" x2="84.7725" y2="-10.16" width="0.254" layer="94"/>
<wire x1="84.7725" y1="-10.16" x2="84.455" y2="-10.16" width="0.254" layer="94"/>
<wire x1="84.455" y1="-10.16" x2="83.185" y2="-10.16" width="0.254" layer="94"/>
<wire x1="83.185" y1="-10.16" x2="82.8675" y2="-10.16" width="0.254" layer="94"/>
<wire x1="82.8675" y1="-10.16" x2="82.55" y2="-10.16" width="0.254" layer="94"/>
<wire x1="82.55" y1="-10.16" x2="82.2325" y2="-10.16" width="0.254" layer="94"/>
<wire x1="82.2325" y1="-10.16" x2="81.915" y2="-10.16" width="0.254" layer="94"/>
<wire x1="81.915" y1="-10.16" x2="80.645" y2="-10.16" width="0.254" layer="94"/>
<wire x1="80.645" y1="-10.16" x2="80.3275" y2="-10.16" width="0.254" layer="94"/>
<wire x1="80.3275" y1="-10.16" x2="80.01" y2="-10.16" width="0.254" layer="94"/>
<wire x1="80.01" y1="-10.16" x2="77.47" y2="-10.16" width="0.254" layer="94"/>
<wire x1="77.47" y1="-10.16" x2="74.93" y2="-10.16" width="0.254" layer="94"/>
<wire x1="74.93" y1="-10.16" x2="74.6125" y2="-10.16" width="0.254" layer="94"/>
<wire x1="74.6125" y1="-10.16" x2="74.295" y2="-10.16" width="0.254" layer="94"/>
<wire x1="74.295" y1="-10.16" x2="73.025" y2="-10.16" width="0.254" layer="94"/>
<wire x1="73.025" y1="-10.16" x2="72.7075" y2="-10.16" width="0.254" layer="94"/>
<wire x1="72.7075" y1="-10.16" x2="72.39" y2="-10.16" width="0.254" layer="94"/>
<wire x1="72.39" y1="-10.16" x2="72.39" y2="-9.8425" width="0.254" layer="94"/>
<wire x1="72.39" y1="-9.8425" x2="72.39" y2="-9.525" width="0.254" layer="94"/>
<wire x1="72.39" y1="-9.525" x2="72.39" y2="-7.62" width="0.254" layer="94"/>
<wire x1="72.39" y1="-7.62" x2="74.93" y2="-7.62" width="0.254" layer="94"/>
<wire x1="74.93" y1="-7.62" x2="75.2475" y2="-7.62" width="0.254" layer="94"/>
<wire x1="75.2475" y1="-7.62" x2="75.565" y2="-7.62" width="0.254" layer="94"/>
<wire x1="75.565" y1="-7.62" x2="76.835" y2="-7.62" width="0.254" layer="94"/>
<wire x1="76.835" y1="-7.62" x2="77.1525" y2="-7.62" width="0.254" layer="94"/>
<wire x1="77.1525" y1="-7.62" x2="77.47" y2="-7.62" width="0.254" layer="94"/>
<wire x1="77.47" y1="-7.62" x2="77.7875" y2="-7.62" width="0.254" layer="94"/>
<wire x1="77.7875" y1="-7.62" x2="78.105" y2="-7.62" width="0.254" layer="94"/>
<wire x1="78.105" y1="-7.62" x2="79.375" y2="-7.62" width="0.254" layer="94"/>
<wire x1="79.375" y1="-7.62" x2="79.6925" y2="-7.62" width="0.254" layer="94"/>
<wire x1="79.6925" y1="-7.62" x2="80.01" y2="-7.62" width="0.254" layer="94"/>
<wire x1="80.01" y1="-7.62" x2="82.55" y2="-7.62" width="0.254" layer="94"/>
<wire x1="82.55" y1="-7.62" x2="85.09" y2="-7.62" width="0.254" layer="94"/>
<wire x1="85.09" y1="-7.62" x2="85.4075" y2="-7.62" width="0.254" layer="94"/>
<wire x1="85.4075" y1="-7.62" x2="85.725" y2="-7.62" width="0.254" layer="94"/>
<wire x1="85.725" y1="-7.62" x2="86.995" y2="-7.62" width="0.254" layer="94"/>
<wire x1="86.995" y1="-7.62" x2="87.3125" y2="-7.62" width="0.254" layer="94"/>
<wire x1="87.3125" y1="-7.62" x2="87.63" y2="-7.62" width="0.254" layer="94"/>
<wire x1="85.09" y1="-10.16" x2="85.09" y2="-9.8425" width="0.254" layer="94"/>
<wire x1="85.09" y1="-9.8425" x2="85.09" y2="-9.525" width="0.254" layer="94"/>
<wire x1="85.09" y1="-9.525" x2="85.09" y2="-7.62" width="0.254" layer="94"/>
<wire x1="85.09" y1="-7.62" x2="85.09" y2="-7.3025" width="0.254" layer="94"/>
<wire x1="85.09" y1="-7.3025" x2="85.09" y2="-6.985" width="0.254" layer="94"/>
<wire x1="85.09" y1="-6.985" x2="85.09" y2="-5.08" width="0.254" layer="94"/>
<wire x1="82.55" y1="-10.16" x2="82.55" y2="-9.8425" width="0.254" layer="94"/>
<wire x1="82.55" y1="-9.8425" x2="82.55" y2="-9.525" width="0.254" layer="94"/>
<wire x1="82.55" y1="-9.525" x2="82.55" y2="-7.62" width="0.254" layer="94"/>
<wire x1="82.55" y1="-7.62" x2="82.55" y2="-5.08" width="0.254" layer="94"/>
<wire x1="80.01" y1="-10.16" x2="80.01" y2="-9.8425" width="0.254" layer="94"/>
<wire x1="80.01" y1="-9.8425" x2="80.01" y2="-9.525" width="0.254" layer="94"/>
<wire x1="80.01" y1="-9.525" x2="80.01" y2="-7.62" width="0.254" layer="94"/>
<wire x1="80.01" y1="-7.62" x2="80.01" y2="-7.3025" width="0.254" layer="94"/>
<wire x1="80.01" y1="-7.3025" x2="80.01" y2="-6.985" width="0.254" layer="94"/>
<wire x1="80.01" y1="-6.985" x2="80.01" y2="-5.08" width="0.254" layer="94"/>
<wire x1="77.47" y1="-10.16" x2="77.47" y2="-7.62" width="0.254" layer="94"/>
<wire x1="77.47" y1="-7.62" x2="77.47" y2="-7.3025" width="0.254" layer="94"/>
<wire x1="77.47" y1="-7.3025" x2="77.47" y2="-6.985" width="0.254" layer="94"/>
<wire x1="77.47" y1="-6.985" x2="77.47" y2="-5.08" width="0.254" layer="94"/>
<wire x1="74.93" y1="-10.16" x2="74.93" y2="-9.8425" width="0.254" layer="94"/>
<wire x1="74.93" y1="-9.8425" x2="74.93" y2="-9.525" width="0.254" layer="94"/>
<wire x1="74.93" y1="-9.525" x2="74.93" y2="-7.62" width="0.254" layer="94"/>
<wire x1="74.93" y1="-7.62" x2="74.93" y2="-7.3025" width="0.254" layer="94"/>
<wire x1="74.93" y1="-7.3025" x2="74.93" y2="-6.985" width="0.254" layer="94"/>
<wire x1="74.93" y1="-6.985" x2="74.93" y2="-5.08" width="0.254" layer="94"/>
<wire x1="87.63" y1="-7.3025" x2="87.3125" y2="-7.62" width="0.254" layer="94"/>
<wire x1="86.995" y1="-7.62" x2="87.63" y2="-6.985" width="0.254" layer="94"/>
<wire x1="85.4075" y1="-7.62" x2="85.09" y2="-7.3025" width="0.254" layer="94"/>
<wire x1="85.725" y1="-7.62" x2="85.09" y2="-6.985" width="0.254" layer="94"/>
<wire x1="85.09" y1="-9.8425" x2="84.7725" y2="-10.16" width="0.254" layer="94"/>
<wire x1="85.09" y1="-9.525" x2="84.455" y2="-10.16" width="0.254" layer="94"/>
<wire x1="82.8675" y1="-10.16" x2="82.55" y2="-9.8425" width="0.254" layer="94"/>
<wire x1="83.185" y1="-10.16" x2="82.55" y2="-9.525" width="0.254" layer="94"/>
<wire x1="80.3275" y1="-10.16" x2="80.01" y2="-9.8425" width="0.254" layer="94"/>
<wire x1="80.645" y1="-10.16" x2="80.01" y2="-9.525" width="0.254" layer="94"/>
<wire x1="82.55" y1="-9.8425" x2="82.2325" y2="-10.16" width="0.254" layer="94"/>
<wire x1="82.55" y1="-9.525" x2="81.915" y2="-10.16" width="0.254" layer="94"/>
<wire x1="80.01" y1="-7.3025" x2="79.6925" y2="-7.62" width="0.254" layer="94"/>
<wire x1="80.01" y1="-6.985" x2="79.375" y2="-7.62" width="0.254" layer="94"/>
<wire x1="77.7875" y1="-7.62" x2="77.47" y2="-7.3025" width="0.254" layer="94"/>
<wire x1="78.105" y1="-7.62" x2="77.47" y2="-6.985" width="0.254" layer="94"/>
<wire x1="77.47" y1="-7.3025" x2="77.1525" y2="-7.62" width="0.254" layer="94"/>
<wire x1="77.47" y1="-6.985" x2="76.835" y2="-7.62" width="0.254" layer="94"/>
<wire x1="75.2475" y1="-7.62" x2="74.93" y2="-7.3025" width="0.254" layer="94"/>
<wire x1="75.565" y1="-7.62" x2="74.93" y2="-6.985" width="0.254" layer="94"/>
<wire x1="74.93" y1="-9.8425" x2="74.6125" y2="-10.16" width="0.254" layer="94"/>
<wire x1="74.93" y1="-9.525" x2="74.295" y2="-10.16" width="0.254" layer="94"/>
<wire x1="72.7075" y1="-10.16" x2="72.39" y2="-9.8425" width="0.254" layer="94"/>
<wire x1="73.025" y1="-10.16" x2="72.39" y2="-9.525" width="0.254" layer="94"/>
<wire x1="19.05" y1="-76.2" x2="16.51" y2="-76.2" width="0.254" layer="94"/>
<wire x1="6.35" y1="-76.2" x2="6.35" y2="-78.74" width="0.254" layer="94"/>
<wire x1="6.35" y1="-78.74" x2="6.35" y2="-79.0575" width="0.254" layer="94"/>
<wire x1="6.35" y1="-79.0575" x2="6.35" y2="-79.375" width="0.254" layer="94"/>
<wire x1="6.35" y1="-79.375" x2="6.35" y2="-81.28" width="0.254" layer="94"/>
<wire x1="6.35" y1="-81.28" x2="8.89" y2="-81.28" width="0.254" layer="94"/>
<wire x1="8.89" y1="-81.28" x2="11.43" y2="-81.28" width="0.254" layer="94"/>
<wire x1="11.43" y1="-81.28" x2="13.97" y2="-81.28" width="0.254" layer="94"/>
<wire x1="13.97" y1="-81.28" x2="16.51" y2="-81.28" width="0.254" layer="94"/>
<wire x1="16.51" y1="-81.28" x2="19.05" y2="-81.28" width="0.254" layer="94"/>
<wire x1="19.05" y1="-81.28" x2="19.05" y2="-79.375" width="0.254" layer="94"/>
<wire x1="19.05" y1="-79.375" x2="19.05" y2="-79.0575" width="0.254" layer="94"/>
<wire x1="19.05" y1="-78.74" x2="19.05" y2="-76.835" width="0.254" layer="94"/>
<wire x1="19.05" y1="-76.835" x2="19.05" y2="-76.5175" width="0.254" layer="94"/>
<wire x1="19.05" y1="-76.5175" x2="19.05" y2="-76.2" width="0.254" layer="94"/>
<wire x1="19.05" y1="-78.74" x2="18.796" y2="-78.74" width="0.254" layer="94"/>
<wire x1="18.7325" y1="-78.74" x2="18.415" y2="-78.74" width="0.254" layer="94"/>
<wire x1="18.415" y1="-78.74" x2="17.145" y2="-78.74" width="0.254" layer="94"/>
<wire x1="17.145" y1="-78.74" x2="16.8275" y2="-78.74" width="0.254" layer="94"/>
<wire x1="16.8275" y1="-78.74" x2="16.1925" y2="-78.74" width="0.254" layer="94"/>
<wire x1="16.1925" y1="-78.74" x2="15.875" y2="-78.74" width="0.254" layer="94"/>
<wire x1="15.875" y1="-78.74" x2="14.605" y2="-78.74" width="0.254" layer="94"/>
<wire x1="14.605" y1="-78.74" x2="14.2875" y2="-78.74" width="0.254" layer="94"/>
<wire x1="16.51" y1="-76.2" x2="16.51" y2="-79.0575" width="0.254" layer="94"/>
<wire x1="16.51" y1="-79.0575" x2="16.51" y2="-79.375" width="0.254" layer="94"/>
<wire x1="16.51" y1="-79.375" x2="16.51" y2="-81.28" width="0.254" layer="94"/>
<wire x1="13.97" y1="-81.28" x2="13.97" y2="-79.375" width="0.254" layer="94"/>
<wire x1="13.97" y1="-79.375" x2="13.97" y2="-79.0575" width="0.254" layer="94"/>
<wire x1="19.05" y1="-79.0575" x2="19.05" y2="-78.74" width="0.254" layer="94"/>
<wire x1="21.59" y1="-78.74" x2="21.59" y2="-76.835" width="0.254" layer="94"/>
<wire x1="21.59" y1="-76.835" x2="21.59" y2="-76.5175" width="0.254" layer="94"/>
<wire x1="21.59" y1="-76.5175" x2="21.59" y2="-76.2" width="0.254" layer="94"/>
<wire x1="21.59" y1="-81.28" x2="21.59" y2="-78.74" width="0.254" layer="94"/>
<wire x1="21.59" y1="-76.2" x2="21.2725" y2="-76.2" width="0.254" layer="94"/>
<wire x1="21.2725" y1="-76.2" x2="20.955" y2="-76.2" width="0.254" layer="94"/>
<wire x1="20.955" y1="-76.2" x2="19.685" y2="-76.2" width="0.254" layer="94"/>
<wire x1="19.685" y1="-76.2" x2="19.3675" y2="-76.2" width="0.254" layer="94"/>
<wire x1="19.3675" y1="-76.2" x2="19.05" y2="-76.2" width="0.254" layer="94"/>
<wire x1="19.05" y1="-81.28" x2="21.59" y2="-81.28" width="0.254" layer="94"/>
<wire x1="21.59" y1="-78.74" x2="18.7325" y2="-78.74" width="0.254" layer="94"/>
<text x="7.62" y="-77.47" size="1.4224" layer="94" rot="R270" align="center">1</text>
<text x="10.16" y="-77.47" size="1.4224" layer="94" rot="R270" align="center">2</text>
<text x="12.7" y="-77.47" size="1.4224" layer="94" rot="R270" align="center">3</text>
<text x="15.24" y="-77.47" size="1.4224" layer="94" rot="R270" align="center">4</text>
<text x="17.78" y="-77.47" size="1.4224" layer="94" rot="R270" align="center">5</text>
<text x="20.32" y="-77.47" size="1.4224" layer="94" rot="R270" align="center">6</text>
<text x="22.86" y="-77.47" size="1.4224" layer="94" rot="R270" align="center">7</text>
<text x="7.62" y="-80.01" size="1.4224" layer="94" rot="R270" align="center">8</text>
<text x="10.16" y="-80.01" size="1.4224" layer="94" rot="R270" align="center">9</text>
<text x="12.7" y="-80.01" size="1.4224" layer="94" rot="R270" align="center">10</text>
<text x="25.4" y="-78.74" size="1.27" layer="96" rot="R90" align="top-center">MINIFIT14</text>
<text x="5.08" y="-78.74" size="1.778" layer="95" align="center-right">K1</text>
<text x="15.24" y="-80.01" size="1.4224" layer="94" rot="R270" align="center">11</text>
<text x="17.78" y="-80.01" size="1.4224" layer="94" rot="R270" align="center">12</text>
<pin name="K1.1" x="7.62" y="-73.66" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="K1.2" x="10.16" y="-73.66" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="K1.3" x="12.7" y="-73.66" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="K1.4" x="15.24" y="-73.66" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="K1.5" x="17.78" y="-73.66" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="K1.6" x="20.32" y="-73.66" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="K1.7" x="22.86" y="-73.66" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="K1.8" x="7.62" y="-83.82" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="K1.9" x="10.16" y="-83.82" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="K1.10" x="12.7" y="-83.82" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="K1.11" x="15.24" y="-83.82" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="K1.12" x="17.78" y="-83.82" visible="off" length="short" direction="pas" rot="R90"/>
<rectangle x1="14.859" y1="-82.677" x2="15.621" y2="-80.645" layer="94" rot="R270"/>
<text x="22.86" y="-80.01" size="1.4224" layer="94" rot="R270" align="center">14</text>
<text x="20.32" y="-80.01" size="1.4224" layer="94" rot="R270" align="center">13</text>
<pin name="K1.13" x="20.32" y="-83.82" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="K1.14" x="22.86" y="-83.82" visible="off" length="short" direction="pas" rot="R90"/>
<wire x1="24.13" y1="-81.28" x2="24.13" y2="-78.74" width="0.254" layer="94"/>
<wire x1="24.13" y1="-78.74" x2="24.13" y2="-76.835" width="0.254" layer="94"/>
<wire x1="24.13" y1="-76.835" x2="24.13" y2="-76.5175" width="0.254" layer="94"/>
<wire x1="24.13" y1="-76.5175" x2="24.13" y2="-76.2" width="0.254" layer="94"/>
<wire x1="21.59" y1="-81.28" x2="24.13" y2="-81.28" width="0.254" layer="94"/>
<wire x1="21.59" y1="-78.74" x2="24.13" y2="-78.74" width="0.254" layer="94"/>
<wire x1="21.59" y1="-76.2" x2="21.9075" y2="-76.2" width="0.254" layer="94"/>
<wire x1="21.9075" y1="-76.2" x2="22.225" y2="-76.2" width="0.254" layer="94"/>
<wire x1="22.225" y1="-76.2" x2="23.495" y2="-76.2" width="0.254" layer="94"/>
<wire x1="23.495" y1="-76.2" x2="23.8125" y2="-76.2" width="0.254" layer="94"/>
<wire x1="23.8125" y1="-76.2" x2="24.13" y2="-76.2" width="0.254" layer="94"/>
<wire x1="23.495" y1="-76.2" x2="24.13" y2="-76.835" width="0.254" layer="94"/>
<wire x1="22.225" y1="-76.2" x2="21.59" y2="-76.835" width="0.254" layer="94"/>
<wire x1="20.955" y1="-76.2" x2="21.59" y2="-76.835" width="0.254" layer="94"/>
<wire x1="19.05" y1="-76.835" x2="19.685" y2="-76.2" width="0.254" layer="94"/>
<wire x1="21.59" y1="-76.5175" x2="21.2725" y2="-76.2" width="0.254" layer="94"/>
<wire x1="21.59" y1="-76.5175" x2="21.9075" y2="-76.2" width="0.254" layer="94"/>
<wire x1="23.8125" y1="-76.2" x2="24.13" y2="-76.5175" width="0.254" layer="94"/>
<wire x1="19.3675" y1="-76.2" x2="19.05" y2="-76.5175" width="0.254" layer="94"/>
<wire x1="18.7325" y1="-78.74" x2="19.05" y2="-79.0575" width="0.254" layer="94"/>
<wire x1="18.415" y1="-78.74" x2="19.05" y2="-79.375" width="0.254" layer="94"/>
<wire x1="16.51" y1="-79.0575" x2="16.8275" y2="-78.74" width="0.254" layer="94"/>
<wire x1="16.51" y1="-79.375" x2="17.145" y2="-78.74" width="0.254" layer="94"/>
<wire x1="15.875" y1="-78.74" x2="16.51" y2="-79.375" width="0.254" layer="94"/>
<wire x1="16.1925" y1="-78.74" x2="16.51" y2="-79.0575" width="0.254" layer="94"/>
<wire x1="13.97" y1="-79.0575" x2="14.2875" y2="-78.74" width="0.254" layer="94"/>
<wire x1="13.97" y1="-79.375" x2="14.605" y2="-78.74" width="0.254" layer="94"/>
<wire x1="6.35" y1="-76.2" x2="8.89" y2="-76.2" width="0.254" layer="94"/>
<wire x1="8.89" y1="-76.2" x2="9.2075" y2="-76.2" width="0.254" layer="94"/>
<wire x1="9.2075" y1="-76.2" x2="9.525" y2="-76.2" width="0.254" layer="94"/>
<wire x1="9.525" y1="-76.2" x2="10.795" y2="-76.2" width="0.254" layer="94"/>
<wire x1="10.795" y1="-76.2" x2="11.1125" y2="-76.2" width="0.254" layer="94"/>
<wire x1="11.1125" y1="-76.2" x2="11.43" y2="-76.2" width="0.254" layer="94"/>
<wire x1="11.43" y1="-76.2" x2="11.7475" y2="-76.2" width="0.254" layer="94"/>
<wire x1="11.7475" y1="-76.2" x2="12.065" y2="-76.2" width="0.254" layer="94"/>
<wire x1="12.065" y1="-76.2" x2="13.335" y2="-76.2" width="0.254" layer="94"/>
<wire x1="13.335" y1="-76.2" x2="13.6525" y2="-76.2" width="0.254" layer="94"/>
<wire x1="13.6525" y1="-76.2" x2="13.97" y2="-76.2" width="0.254" layer="94"/>
<wire x1="13.97" y1="-76.2" x2="16.51" y2="-76.2" width="0.254" layer="94"/>
<wire x1="13.97" y1="-79.0575" x2="13.97" y2="-78.74" width="0.254" layer="94"/>
<wire x1="13.97" y1="-78.74" x2="14.2875" y2="-78.74" width="0.254" layer="94"/>
<wire x1="13.97" y1="-78.74" x2="13.97" y2="-76.835" width="0.254" layer="94"/>
<wire x1="13.97" y1="-76.835" x2="13.97" y2="-76.5175" width="0.254" layer="94"/>
<wire x1="13.97" y1="-76.5175" x2="13.97" y2="-76.2" width="0.254" layer="94"/>
<wire x1="6.35" y1="-78.74" x2="6.6675" y2="-78.74" width="0.254" layer="94"/>
<wire x1="6.6675" y1="-78.74" x2="6.985" y2="-78.74" width="0.254" layer="94"/>
<wire x1="6.985" y1="-78.74" x2="8.255" y2="-78.74" width="0.254" layer="94"/>
<wire x1="8.255" y1="-78.74" x2="8.5725" y2="-78.74" width="0.254" layer="94"/>
<wire x1="8.5725" y1="-78.74" x2="8.89" y2="-78.74" width="0.254" layer="94"/>
<wire x1="8.89" y1="-78.74" x2="11.43" y2="-78.74" width="0.254" layer="94"/>
<wire x1="11.43" y1="-78.74" x2="13.97" y2="-78.74" width="0.254" layer="94"/>
<wire x1="11.43" y1="-76.2" x2="11.43" y2="-76.5175" width="0.254" layer="94"/>
<wire x1="11.43" y1="-76.5175" x2="11.43" y2="-76.835" width="0.254" layer="94"/>
<wire x1="11.43" y1="-76.835" x2="11.43" y2="-78.74" width="0.254" layer="94"/>
<wire x1="11.43" y1="-78.74" x2="11.43" y2="-81.28" width="0.254" layer="94"/>
<wire x1="8.89" y1="-76.2" x2="8.89" y2="-76.5175" width="0.254" layer="94"/>
<wire x1="8.89" y1="-76.5175" x2="8.89" y2="-76.835" width="0.254" layer="94"/>
<wire x1="8.89" y1="-76.835" x2="8.89" y2="-78.74" width="0.254" layer="94"/>
<wire x1="8.89" y1="-78.74" x2="8.89" y2="-79.0575" width="0.254" layer="94"/>
<wire x1="8.89" y1="-79.0575" x2="8.89" y2="-79.375" width="0.254" layer="94"/>
<wire x1="8.89" y1="-79.375" x2="8.89" y2="-81.28" width="0.254" layer="94"/>
<wire x1="6.35" y1="-79.0575" x2="6.6675" y2="-78.74" width="0.254" layer="94"/>
<wire x1="6.985" y1="-78.74" x2="6.35" y2="-79.375" width="0.254" layer="94"/>
<wire x1="8.5725" y1="-78.74" x2="8.89" y2="-79.0575" width="0.254" layer="94"/>
<wire x1="8.255" y1="-78.74" x2="8.89" y2="-79.375" width="0.254" layer="94"/>
<wire x1="8.89" y1="-76.5175" x2="9.2075" y2="-76.2" width="0.254" layer="94"/>
<wire x1="9.525" y1="-76.2" x2="8.89" y2="-76.835" width="0.254" layer="94"/>
<wire x1="11.1125" y1="-76.2" x2="11.43" y2="-76.5175" width="0.254" layer="94"/>
<wire x1="10.795" y1="-76.2" x2="11.43" y2="-76.835" width="0.254" layer="94"/>
<wire x1="11.43" y1="-76.5175" x2="11.7475" y2="-76.2" width="0.254" layer="94"/>
<wire x1="11.43" y1="-76.835" x2="12.065" y2="-76.2" width="0.254" layer="94"/>
<wire x1="13.6525" y1="-76.2" x2="13.97" y2="-76.5175" width="0.254" layer="94"/>
<wire x1="13.335" y1="-76.2" x2="13.97" y2="-76.835" width="0.254" layer="94"/>
<wire x1="11.43" y1="-10.16" x2="13.97" y2="-10.16" width="0.254" layer="94"/>
<wire x1="24.13" y1="-10.16" x2="24.13" y2="-7.62" width="0.254" layer="94"/>
<wire x1="24.13" y1="-7.62" x2="24.13" y2="-7.3025" width="0.254" layer="94"/>
<wire x1="24.13" y1="-7.3025" x2="24.13" y2="-6.985" width="0.254" layer="94"/>
<wire x1="24.13" y1="-6.985" x2="24.13" y2="-5.08" width="0.254" layer="94"/>
<wire x1="24.13" y1="-5.08" x2="21.59" y2="-5.08" width="0.254" layer="94"/>
<wire x1="21.59" y1="-5.08" x2="19.05" y2="-5.08" width="0.254" layer="94"/>
<wire x1="19.05" y1="-5.08" x2="16.51" y2="-5.08" width="0.254" layer="94"/>
<wire x1="16.51" y1="-5.08" x2="13.97" y2="-5.08" width="0.254" layer="94"/>
<wire x1="13.97" y1="-5.08" x2="11.43" y2="-5.08" width="0.254" layer="94"/>
<wire x1="11.43" y1="-5.08" x2="11.43" y2="-6.985" width="0.254" layer="94"/>
<wire x1="11.43" y1="-6.985" x2="11.43" y2="-7.3025" width="0.254" layer="94"/>
<wire x1="11.43" y1="-7.62" x2="11.43" y2="-9.525" width="0.254" layer="94"/>
<wire x1="11.43" y1="-9.525" x2="11.43" y2="-9.8425" width="0.254" layer="94"/>
<wire x1="11.43" y1="-9.8425" x2="11.43" y2="-10.16" width="0.254" layer="94"/>
<wire x1="11.43" y1="-7.62" x2="11.684" y2="-7.62" width="0.254" layer="94"/>
<wire x1="11.7475" y1="-7.62" x2="12.065" y2="-7.62" width="0.254" layer="94"/>
<wire x1="12.065" y1="-7.62" x2="13.335" y2="-7.62" width="0.254" layer="94"/>
<wire x1="13.335" y1="-7.62" x2="13.6525" y2="-7.62" width="0.254" layer="94"/>
<wire x1="13.6525" y1="-7.62" x2="14.2875" y2="-7.62" width="0.254" layer="94"/>
<wire x1="14.2875" y1="-7.62" x2="14.605" y2="-7.62" width="0.254" layer="94"/>
<wire x1="14.605" y1="-7.62" x2="15.875" y2="-7.62" width="0.254" layer="94"/>
<wire x1="15.875" y1="-7.62" x2="16.1925" y2="-7.62" width="0.254" layer="94"/>
<wire x1="13.97" y1="-10.16" x2="13.97" y2="-7.3025" width="0.254" layer="94"/>
<wire x1="13.97" y1="-7.3025" x2="13.97" y2="-6.985" width="0.254" layer="94"/>
<wire x1="13.97" y1="-6.985" x2="13.97" y2="-5.08" width="0.254" layer="94"/>
<wire x1="16.51" y1="-5.08" x2="16.51" y2="-6.985" width="0.254" layer="94"/>
<wire x1="16.51" y1="-6.985" x2="16.51" y2="-7.3025" width="0.254" layer="94"/>
<wire x1="11.43" y1="-7.3025" x2="11.43" y2="-7.62" width="0.254" layer="94"/>
<wire x1="8.89" y1="-7.62" x2="8.89" y2="-9.525" width="0.254" layer="94"/>
<wire x1="8.89" y1="-9.525" x2="8.89" y2="-9.8425" width="0.254" layer="94"/>
<wire x1="8.89" y1="-9.8425" x2="8.89" y2="-10.16" width="0.254" layer="94"/>
<wire x1="8.89" y1="-5.08" x2="8.89" y2="-7.62" width="0.254" layer="94"/>
<wire x1="8.89" y1="-10.16" x2="9.2075" y2="-10.16" width="0.254" layer="94"/>
<wire x1="9.2075" y1="-10.16" x2="9.525" y2="-10.16" width="0.254" layer="94"/>
<wire x1="9.525" y1="-10.16" x2="10.795" y2="-10.16" width="0.254" layer="94"/>
<wire x1="10.795" y1="-10.16" x2="11.1125" y2="-10.16" width="0.254" layer="94"/>
<wire x1="11.1125" y1="-10.16" x2="11.43" y2="-10.16" width="0.254" layer="94"/>
<wire x1="11.43" y1="-5.08" x2="8.89" y2="-5.08" width="0.254" layer="94"/>
<wire x1="8.89" y1="-7.62" x2="11.7475" y2="-7.62" width="0.254" layer="94"/>
<text x="22.86" y="-8.89" size="1.4224" layer="94" rot="R90" align="center">1</text>
<text x="20.32" y="-8.89" size="1.4224" layer="94" rot="R90" align="center">2</text>
<text x="17.78" y="-8.89" size="1.4224" layer="94" rot="R90" align="center">3</text>
<text x="15.24" y="-8.89" size="1.4224" layer="94" rot="R90" align="center">4</text>
<text x="12.7" y="-8.89" size="1.4224" layer="94" rot="R90" align="center">5</text>
<text x="10.16" y="-8.89" size="1.4224" layer="94" rot="R90" align="center">6</text>
<text x="7.62" y="-8.89" size="1.4224" layer="94" rot="R90" align="center">7</text>
<text x="22.86" y="-6.35" size="1.4224" layer="94" rot="R90" align="center">8</text>
<text x="20.32" y="-6.35" size="1.4224" layer="94" rot="R90" align="center">9</text>
<text x="17.78" y="-6.35" size="1.4224" layer="94" rot="R90" align="center">10</text>
<text x="5.08" y="-7.62" size="1.27" layer="96" rot="MR90" align="top-center">MINIFIT14</text>
<text x="25.4" y="-7.62" size="1.778" layer="95" rot="MR0" align="center-right">K2</text>
<text x="15.24" y="-6.35" size="1.4224" layer="94" rot="R90" align="center">11</text>
<text x="12.7" y="-6.35" size="1.4224" layer="94" rot="R90" align="center">12</text>
<pin name="K2.1" x="22.86" y="-12.7" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="K2.2" x="20.32" y="-12.7" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="K2.3" x="17.78" y="-12.7" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="K2.4" x="15.24" y="-12.7" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="K2.5" x="12.7" y="-12.7" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="K2.6" x="10.16" y="-12.7" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="K2.7" x="7.62" y="-12.7" visible="off" length="short" direction="pas" rot="R90"/>
<pin name="K2.8" x="22.86" y="-2.54" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="K2.9" x="20.32" y="-2.54" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="K2.10" x="17.78" y="-2.54" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="K2.11" x="15.24" y="-2.54" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="K2.12" x="12.7" y="-2.54" visible="off" length="short" direction="pas" rot="R270"/>
<rectangle x1="14.859" y1="-5.715" x2="15.621" y2="-3.683" layer="94" rot="R90"/>
<text x="7.62" y="-6.35" size="1.4224" layer="94" rot="R90" align="center">14</text>
<text x="10.16" y="-6.35" size="1.4224" layer="94" rot="R90" align="center">13</text>
<pin name="K2.13" x="10.16" y="-2.54" visible="off" length="short" direction="pas" rot="R270"/>
<pin name="K2.14" x="7.62" y="-2.54" visible="off" length="short" direction="pas" rot="R270"/>
<wire x1="6.35" y1="-5.08" x2="6.35" y2="-7.62" width="0.254" layer="94"/>
<wire x1="6.35" y1="-7.62" x2="6.35" y2="-9.525" width="0.254" layer="94"/>
<wire x1="6.35" y1="-9.525" x2="6.35" y2="-9.8425" width="0.254" layer="94"/>
<wire x1="6.35" y1="-9.8425" x2="6.35" y2="-10.16" width="0.254" layer="94"/>
<wire x1="8.89" y1="-5.08" x2="6.35" y2="-5.08" width="0.254" layer="94"/>
<wire x1="8.89" y1="-7.62" x2="6.35" y2="-7.62" width="0.254" layer="94"/>
<wire x1="8.89" y1="-10.16" x2="8.5725" y2="-10.16" width="0.254" layer="94"/>
<wire x1="8.5725" y1="-10.16" x2="8.255" y2="-10.16" width="0.254" layer="94"/>
<wire x1="8.255" y1="-10.16" x2="6.985" y2="-10.16" width="0.254" layer="94"/>
<wire x1="6.985" y1="-10.16" x2="6.6675" y2="-10.16" width="0.254" layer="94"/>
<wire x1="6.6675" y1="-10.16" x2="6.35" y2="-10.16" width="0.254" layer="94"/>
<wire x1="6.985" y1="-10.16" x2="6.35" y2="-9.525" width="0.254" layer="94"/>
<wire x1="8.255" y1="-10.16" x2="8.89" y2="-9.525" width="0.254" layer="94"/>
<wire x1="9.525" y1="-10.16" x2="8.89" y2="-9.525" width="0.254" layer="94"/>
<wire x1="11.43" y1="-9.525" x2="10.795" y2="-10.16" width="0.254" layer="94"/>
<wire x1="8.89" y1="-9.8425" x2="9.2075" y2="-10.16" width="0.254" layer="94"/>
<wire x1="8.89" y1="-9.8425" x2="8.5725" y2="-10.16" width="0.254" layer="94"/>
<wire x1="6.6675" y1="-10.16" x2="6.35" y2="-9.8425" width="0.254" layer="94"/>
<wire x1="11.1125" y1="-10.16" x2="11.43" y2="-9.8425" width="0.254" layer="94"/>
<wire x1="11.7475" y1="-7.62" x2="11.43" y2="-7.3025" width="0.254" layer="94"/>
<wire x1="12.065" y1="-7.62" x2="11.43" y2="-6.985" width="0.254" layer="94"/>
<wire x1="13.97" y1="-7.3025" x2="13.6525" y2="-7.62" width="0.254" layer="94"/>
<wire x1="13.97" y1="-6.985" x2="13.335" y2="-7.62" width="0.254" layer="94"/>
<wire x1="14.605" y1="-7.62" x2="13.97" y2="-6.985" width="0.254" layer="94"/>
<wire x1="14.2875" y1="-7.62" x2="13.97" y2="-7.3025" width="0.254" layer="94"/>
<wire x1="16.51" y1="-7.3025" x2="16.1925" y2="-7.62" width="0.254" layer="94"/>
<wire x1="16.51" y1="-6.985" x2="15.875" y2="-7.62" width="0.254" layer="94"/>
<wire x1="24.13" y1="-10.16" x2="21.59" y2="-10.16" width="0.254" layer="94"/>
<wire x1="21.59" y1="-10.16" x2="21.2725" y2="-10.16" width="0.254" layer="94"/>
<wire x1="21.2725" y1="-10.16" x2="20.955" y2="-10.16" width="0.254" layer="94"/>
<wire x1="20.955" y1="-10.16" x2="19.685" y2="-10.16" width="0.254" layer="94"/>
<wire x1="19.685" y1="-10.16" x2="19.3675" y2="-10.16" width="0.254" layer="94"/>
<wire x1="19.3675" y1="-10.16" x2="19.05" y2="-10.16" width="0.254" layer="94"/>
<wire x1="19.05" y1="-10.16" x2="18.7325" y2="-10.16" width="0.254" layer="94"/>
<wire x1="18.7325" y1="-10.16" x2="18.415" y2="-10.16" width="0.254" layer="94"/>
<wire x1="18.415" y1="-10.16" x2="17.145" y2="-10.16" width="0.254" layer="94"/>
<wire x1="17.145" y1="-10.16" x2="16.8275" y2="-10.16" width="0.254" layer="94"/>
<wire x1="16.8275" y1="-10.16" x2="16.51" y2="-10.16" width="0.254" layer="94"/>
<wire x1="16.51" y1="-10.16" x2="13.97" y2="-10.16" width="0.254" layer="94"/>
<wire x1="16.51" y1="-7.3025" x2="16.51" y2="-7.62" width="0.254" layer="94"/>
<wire x1="16.51" y1="-7.62" x2="16.1925" y2="-7.62" width="0.254" layer="94"/>
<wire x1="16.51" y1="-7.62" x2="16.51" y2="-9.525" width="0.254" layer="94"/>
<wire x1="16.51" y1="-9.525" x2="16.51" y2="-9.8425" width="0.254" layer="94"/>
<wire x1="16.51" y1="-9.8425" x2="16.51" y2="-10.16" width="0.254" layer="94"/>
<wire x1="24.13" y1="-7.62" x2="23.8125" y2="-7.62" width="0.254" layer="94"/>
<wire x1="23.8125" y1="-7.62" x2="23.495" y2="-7.62" width="0.254" layer="94"/>
<wire x1="23.495" y1="-7.62" x2="22.225" y2="-7.62" width="0.254" layer="94"/>
<wire x1="22.225" y1="-7.62" x2="21.9075" y2="-7.62" width="0.254" layer="94"/>
<wire x1="21.9075" y1="-7.62" x2="21.59" y2="-7.62" width="0.254" layer="94"/>
<wire x1="21.59" y1="-7.62" x2="19.05" y2="-7.62" width="0.254" layer="94"/>
<wire x1="19.05" y1="-7.62" x2="16.51" y2="-7.62" width="0.254" layer="94"/>
<wire x1="19.05" y1="-10.16" x2="19.05" y2="-9.8425" width="0.254" layer="94"/>
<wire x1="19.05" y1="-9.8425" x2="19.05" y2="-9.525" width="0.254" layer="94"/>
<wire x1="19.05" y1="-9.525" x2="19.05" y2="-7.62" width="0.254" layer="94"/>
<wire x1="19.05" y1="-7.62" x2="19.05" y2="-5.08" width="0.254" layer="94"/>
<wire x1="21.59" y1="-10.16" x2="21.59" y2="-9.8425" width="0.254" layer="94"/>
<wire x1="21.59" y1="-9.8425" x2="21.59" y2="-9.525" width="0.254" layer="94"/>
<wire x1="21.59" y1="-9.525" x2="21.59" y2="-7.62" width="0.254" layer="94"/>
<wire x1="21.59" y1="-7.62" x2="21.59" y2="-7.3025" width="0.254" layer="94"/>
<wire x1="21.59" y1="-7.3025" x2="21.59" y2="-6.985" width="0.254" layer="94"/>
<wire x1="21.59" y1="-6.985" x2="21.59" y2="-5.08" width="0.254" layer="94"/>
<wire x1="24.13" y1="-7.3025" x2="23.8125" y2="-7.62" width="0.254" layer="94"/>
<wire x1="23.495" y1="-7.62" x2="24.13" y2="-6.985" width="0.254" layer="94"/>
<wire x1="21.9075" y1="-7.62" x2="21.59" y2="-7.3025" width="0.254" layer="94"/>
<wire x1="22.225" y1="-7.62" x2="21.59" y2="-6.985" width="0.254" layer="94"/>
<wire x1="21.59" y1="-9.8425" x2="21.2725" y2="-10.16" width="0.254" layer="94"/>
<wire x1="20.955" y1="-10.16" x2="21.59" y2="-9.525" width="0.254" layer="94"/>
<wire x1="19.3675" y1="-10.16" x2="19.05" y2="-9.8425" width="0.254" layer="94"/>
<wire x1="19.685" y1="-10.16" x2="19.05" y2="-9.525" width="0.254" layer="94"/>
<wire x1="19.05" y1="-9.8425" x2="18.7325" y2="-10.16" width="0.254" layer="94"/>
<wire x1="19.05" y1="-9.525" x2="18.415" y2="-10.16" width="0.254" layer="94"/>
<wire x1="16.8275" y1="-10.16" x2="16.51" y2="-9.8425" width="0.254" layer="94"/>
<wire x1="17.145" y1="-10.16" x2="16.51" y2="-9.525" width="0.254" layer="94"/>
<wire x1="0" y1="-85.725" x2="0.635" y2="-86.36" width="0.3048" layer="94"/>
<wire x1="0.635" y1="-86.36" x2="93.98" y2="-86.36" width="0.3048" layer="94"/>
<wire x1="93.98" y1="-86.36" x2="93.98" y2="-0.635" width="0.3048" layer="94"/>
<wire x1="93.98" y1="-0.635" x2="93.345" y2="0" width="0.3048" layer="94"/>
<wire x1="0" y1="-85.725" x2="93.345" y2="-85.725" width="0.3048" layer="94"/>
<wire x1="93.345" y1="-85.725" x2="93.345" y2="0" width="0.3048" layer="94"/>
<wire x1="93.345" y1="-85.725" x2="93.98" y2="-86.36" width="0.3048" layer="94"/>
<text x="48.26" y="-2.54" size="1.778" layer="94" rot="MR0" align="top-center">Deska
Autostart DEUTZ</text>
<text x="35.56" y="-22.86" size="1.27" layer="97">Karta ABRA:</text>
<text x="50.8" y="-22.86" size="1.27" layer="97">&gt;ABRA</text>
<text x="35.56" y="-20.32" size="1.27" layer="97">Označení desky:</text>
<text x="50.8" y="-20.32" size="1.27" layer="97">&gt;VALUE</text>
<text x="92.075" y="-1.27" size="1.27" layer="95" align="top-right">&gt;PART</text>
<text x="48.26" y="-12.7" size="1.27" layer="94" align="center">Čislování konektorů na desce
při horním pohledu</text>
<wire x1="78.105" y1="-80.645" x2="74.295" y2="-80.645" width="0.254" layer="94"/>
<wire x1="74.295" y1="-80.645" x2="74.295" y2="-78.74" width="0.254" layer="94"/>
<wire x1="74.295" y1="-78.74" x2="74.295" y2="-73.66" width="0.254" layer="94"/>
<wire x1="74.295" y1="-73.66" x2="74.295" y2="-68.58" width="0.254" layer="94"/>
<wire x1="74.295" y1="-68.58" x2="74.295" y2="-63.5" width="0.254" layer="94"/>
<wire x1="74.295" y1="-63.5" x2="74.295" y2="-61.595" width="0.254" layer="94"/>
<wire x1="74.295" y1="-61.595" x2="78.105" y2="-61.595" width="0.254" layer="94"/>
<wire x1="78.105" y1="-61.595" x2="78.105" y2="-63.5" width="0.254" layer="94"/>
<wire x1="78.105" y1="-63.5" x2="78.105" y2="-68.58" width="0.254" layer="94"/>
<wire x1="78.105" y1="-68.58" x2="78.105" y2="-73.66" width="0.254" layer="94"/>
<wire x1="78.105" y1="-73.66" x2="78.105" y2="-78.74" width="0.254" layer="94"/>
<wire x1="78.105" y1="-78.74" x2="78.105" y2="-80.645" width="0.254" layer="94"/>
<wire x1="76.2" y1="-77.47" x2="76.2" y2="-80.01" width="0.254" layer="94" curve="180"/>
<wire x1="76.2" y1="-62.23" x2="76.2" y2="-64.77" width="0.254" layer="94" curve="180"/>
<wire x1="76.2" y1="-77.47" x2="77.47" y2="-77.47" width="0.254" layer="94"/>
<wire x1="77.47" y1="-77.47" x2="77.47" y2="-80.01" width="0.254" layer="94"/>
<wire x1="77.47" y1="-80.01" x2="76.2" y2="-80.01" width="0.254" layer="94"/>
<wire x1="76.2" y1="-62.23" x2="77.47" y2="-62.23" width="0.254" layer="94"/>
<wire x1="77.47" y1="-62.23" x2="77.47" y2="-64.77" width="0.254" layer="94"/>
<wire x1="77.47" y1="-64.77" x2="76.2" y2="-64.77" width="0.254" layer="94"/>
<circle x="76.2" y="-73.66" radius="1.4199" width="0.254" layer="94"/>
<circle x="76.2" y="-68.58" radius="1.4199" width="0.254" layer="94"/>
<text x="76.2" y="-81.28" size="1.778" layer="95" rot="MR180" align="bottom-center">K4</text>
<text x="76.2" y="-78.74" size="1.27" layer="94" rot="R270" align="center">1</text>
<text x="76.2" y="-73.66" size="1.27" layer="94" rot="R90" align="center">2</text>
<text x="76.2" y="-68.58" size="1.27" layer="94" rot="R90" align="center">3</text>
<text x="76.2" y="-63.5" size="1.27" layer="94" rot="R270" align="center">4</text>
<pin name="K4.1" x="81.28" y="-78.74" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="K4.2" x="81.28" y="-73.66" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="K4.3" x="81.28" y="-68.58" visible="off" length="short" direction="pas" rot="R180"/>
<pin name="K4.4" x="81.28" y="-63.5" visible="off" length="short" direction="pas" rot="R180"/>
<wire x1="78.74" y1="-63.5" x2="78.105" y2="-63.5" width="0.1524" layer="94"/>
<wire x1="78.74" y1="-68.58" x2="78.105" y2="-68.58" width="0.1524" layer="94"/>
<wire x1="78.74" y1="-73.66" x2="78.105" y2="-73.66" width="0.1524" layer="94"/>
<wire x1="78.74" y1="-78.74" x2="78.105" y2="-78.74" width="0.1524" layer="94"/>
<wire x1="53.34" y1="-36.83" x2="53.34" y2="-35.8775" width="0.4064" layer="94"/>
<wire x1="53.34" y1="-35.8775" x2="53.34" y2="-30.1625" width="0.4064" layer="94"/>
<wire x1="53.34" y1="-30.1625" x2="53.34" y2="-29.21" width="0.4064" layer="94"/>
<wire x1="53.34" y1="-29.21" x2="54.61" y2="-29.21" width="0.4064" layer="94"/>
<wire x1="54.61" y1="-29.21" x2="54.9275" y2="-29.21" width="0.4064" layer="94"/>
<wire x1="54.9275" y1="-29.21" x2="61.9125" y2="-29.21" width="0.4064" layer="94"/>
<wire x1="61.9125" y1="-29.21" x2="62.23" y2="-29.21" width="0.4064" layer="94"/>
<wire x1="62.23" y1="-29.21" x2="63.5" y2="-29.21" width="0.4064" layer="94"/>
<wire x1="63.5" y1="-29.21" x2="63.5" y2="-35.8775" width="0.4064" layer="94"/>
<wire x1="63.5" y1="-35.8775" x2="63.5" y2="-36.83" width="0.4064" layer="94"/>
<wire x1="63.5" y1="-36.83" x2="62.23" y2="-36.83" width="0.4064" layer="94"/>
<wire x1="62.23" y1="-36.83" x2="61.9125" y2="-36.83" width="0.4064" layer="94"/>
<wire x1="61.9125" y1="-36.83" x2="54.9275" y2="-36.83" width="0.4064" layer="94"/>
<wire x1="54.9275" y1="-36.83" x2="54.61" y2="-36.83" width="0.4064" layer="94"/>
<wire x1="54.61" y1="-36.83" x2="53.34" y2="-36.83" width="0.4064" layer="94"/>
<wire x1="54.61" y1="-36.83" x2="54.61" y2="-29.21" width="0.1524" layer="94"/>
<wire x1="62.23" y1="-36.83" x2="62.23" y2="-29.21" width="0.1524" layer="94"/>
<wire x1="54.9275" y1="-36.83" x2="54.9275" y2="-29.21" width="0.1524" layer="94"/>
<wire x1="61.9125" y1="-36.83" x2="61.9125" y2="-29.21" width="0.1524" layer="94"/>
<wire x1="53.34" y1="-35.8775" x2="47.625" y2="-35.8775" width="0.4064" layer="94"/>
<wire x1="47.625" y1="-35.8775" x2="46.355" y2="-34.6075" width="0.4064" layer="94"/>
<wire x1="46.355" y1="-34.6075" x2="46.355" y2="-33.02" width="0.4064" layer="94"/>
<wire x1="46.355" y1="-33.02" x2="46.355" y2="-31.4325" width="0.4064" layer="94"/>
<wire x1="46.355" y1="-31.4325" x2="47.625" y2="-30.1625" width="0.4064" layer="94"/>
<wire x1="47.625" y1="-30.1625" x2="53.34" y2="-30.1625" width="0.4064" layer="94"/>
<wire x1="63.5" y1="-30.1625" x2="69.215" y2="-30.1625" width="0.4064" layer="94"/>
<wire x1="69.215" y1="-30.1625" x2="70.485" y2="-31.4325" width="0.4064" layer="94"/>
<wire x1="70.485" y1="-31.4325" x2="70.485" y2="-33.02" width="0.4064" layer="94"/>
<wire x1="70.485" y1="-33.02" x2="70.485" y2="-34.6075" width="0.4064" layer="94"/>
<wire x1="70.485" y1="-34.6075" x2="69.215" y2="-35.8775" width="0.4064" layer="94"/>
<wire x1="69.215" y1="-35.8775" x2="63.5" y2="-35.8775" width="0.4064" layer="94"/>
<circle x="49.2125" y="-33.02" radius="1.7097" width="0.3048" layer="94"/>
<circle x="67.6275" y="-33.02" radius="1.7097" width="0.3048" layer="94"/>
<wire x1="56.8325" y1="-33.02" x2="59.69" y2="-33.02" width="0.1524" layer="94"/>
<wire x1="57.4675" y1="-33.3375" x2="59.055" y2="-33.3375" width="0.1524" layer="94"/>
<wire x1="59.055" y1="-33.3375" x2="59.055" y2="-32.7025" width="0.1524" layer="94"/>
<wire x1="59.055" y1="-32.7025" x2="57.4675" y2="-32.7025" width="0.1524" layer="94"/>
<wire x1="57.4675" y1="-32.7025" x2="57.4675" y2="-33.3375" width="0.1524" layer="94"/>
<wire x1="46.355" y1="-33.02" x2="43.18" y2="-33.02" width="0.4064" layer="94"/>
<pin name="P1" x="43.18" y="-33.02" visible="pin" length="short" direction="pas"/>
<pin name="L1" x="73.66" y="-33.02" visible="pin" length="short" direction="pas" rot="R180"/>
<wire x1="53.34" y1="-46.99" x2="53.34" y2="-46.0375" width="0.4064" layer="94"/>
<wire x1="53.34" y1="-46.0375" x2="53.34" y2="-40.3225" width="0.4064" layer="94"/>
<wire x1="53.34" y1="-40.3225" x2="53.34" y2="-39.37" width="0.4064" layer="94"/>
<wire x1="53.34" y1="-39.37" x2="54.61" y2="-39.37" width="0.4064" layer="94"/>
<wire x1="54.61" y1="-39.37" x2="54.9275" y2="-39.37" width="0.4064" layer="94"/>
<wire x1="54.9275" y1="-39.37" x2="61.9125" y2="-39.37" width="0.4064" layer="94"/>
<wire x1="61.9125" y1="-39.37" x2="62.23" y2="-39.37" width="0.4064" layer="94"/>
<wire x1="62.23" y1="-39.37" x2="63.5" y2="-39.37" width="0.4064" layer="94"/>
<wire x1="63.5" y1="-39.37" x2="63.5" y2="-46.0375" width="0.4064" layer="94"/>
<wire x1="63.5" y1="-46.0375" x2="63.5" y2="-46.99" width="0.4064" layer="94"/>
<wire x1="63.5" y1="-46.99" x2="62.23" y2="-46.99" width="0.4064" layer="94"/>
<wire x1="62.23" y1="-46.99" x2="61.9125" y2="-46.99" width="0.4064" layer="94"/>
<wire x1="61.9125" y1="-46.99" x2="54.9275" y2="-46.99" width="0.4064" layer="94"/>
<wire x1="54.9275" y1="-46.99" x2="54.61" y2="-46.99" width="0.4064" layer="94"/>
<wire x1="54.61" y1="-46.99" x2="53.34" y2="-46.99" width="0.4064" layer="94"/>
<wire x1="54.61" y1="-46.99" x2="54.61" y2="-39.37" width="0.1524" layer="94"/>
<wire x1="62.23" y1="-46.99" x2="62.23" y2="-39.37" width="0.1524" layer="94"/>
<wire x1="54.9275" y1="-46.99" x2="54.9275" y2="-39.37" width="0.1524" layer="94"/>
<wire x1="61.9125" y1="-46.99" x2="61.9125" y2="-39.37" width="0.1524" layer="94"/>
<wire x1="53.34" y1="-46.0375" x2="47.625" y2="-46.0375" width="0.4064" layer="94"/>
<wire x1="47.625" y1="-46.0375" x2="46.355" y2="-44.7675" width="0.4064" layer="94"/>
<wire x1="46.355" y1="-44.7675" x2="46.355" y2="-43.18" width="0.4064" layer="94"/>
<wire x1="46.355" y1="-43.18" x2="46.355" y2="-41.5925" width="0.4064" layer="94"/>
<wire x1="46.355" y1="-41.5925" x2="47.625" y2="-40.3225" width="0.4064" layer="94"/>
<wire x1="47.625" y1="-40.3225" x2="53.34" y2="-40.3225" width="0.4064" layer="94"/>
<wire x1="63.5" y1="-40.3225" x2="69.215" y2="-40.3225" width="0.4064" layer="94"/>
<wire x1="69.215" y1="-40.3225" x2="70.485" y2="-41.5925" width="0.4064" layer="94"/>
<wire x1="70.485" y1="-41.5925" x2="70.485" y2="-43.18" width="0.4064" layer="94"/>
<wire x1="70.485" y1="-43.18" x2="70.485" y2="-44.7675" width="0.4064" layer="94"/>
<wire x1="70.485" y1="-44.7675" x2="69.215" y2="-46.0375" width="0.4064" layer="94"/>
<wire x1="69.215" y1="-46.0375" x2="63.5" y2="-46.0375" width="0.4064" layer="94"/>
<circle x="49.2125" y="-43.18" radius="1.7097" width="0.3048" layer="94"/>
<circle x="67.6275" y="-43.18" radius="1.7097" width="0.3048" layer="94"/>
<wire x1="46.355" y1="-43.18" x2="43.18" y2="-43.18" width="0.4064" layer="94"/>
<pin name="P2" x="43.18" y="-43.18" visible="pin" length="short" direction="pas"/>
<wire x1="53.34" y1="-57.15" x2="53.34" y2="-56.1975" width="0.4064" layer="94"/>
<wire x1="53.34" y1="-56.1975" x2="53.34" y2="-50.4825" width="0.4064" layer="94"/>
<wire x1="53.34" y1="-50.4825" x2="53.34" y2="-49.53" width="0.4064" layer="94"/>
<wire x1="53.34" y1="-49.53" x2="54.61" y2="-49.53" width="0.4064" layer="94"/>
<wire x1="54.61" y1="-49.53" x2="54.9275" y2="-49.53" width="0.4064" layer="94"/>
<wire x1="54.9275" y1="-49.53" x2="61.9125" y2="-49.53" width="0.4064" layer="94"/>
<wire x1="61.9125" y1="-49.53" x2="62.23" y2="-49.53" width="0.4064" layer="94"/>
<wire x1="62.23" y1="-49.53" x2="63.5" y2="-49.53" width="0.4064" layer="94"/>
<wire x1="63.5" y1="-49.53" x2="63.5" y2="-56.1975" width="0.4064" layer="94"/>
<wire x1="63.5" y1="-56.1975" x2="63.5" y2="-57.15" width="0.4064" layer="94"/>
<wire x1="63.5" y1="-57.15" x2="62.23" y2="-57.15" width="0.4064" layer="94"/>
<wire x1="62.23" y1="-57.15" x2="61.9125" y2="-57.15" width="0.4064" layer="94"/>
<wire x1="61.9125" y1="-57.15" x2="54.9275" y2="-57.15" width="0.4064" layer="94"/>
<wire x1="54.9275" y1="-57.15" x2="54.61" y2="-57.15" width="0.4064" layer="94"/>
<wire x1="54.61" y1="-57.15" x2="53.34" y2="-57.15" width="0.4064" layer="94"/>
<wire x1="54.61" y1="-57.15" x2="54.61" y2="-49.53" width="0.1524" layer="94"/>
<wire x1="62.23" y1="-57.15" x2="62.23" y2="-49.53" width="0.1524" layer="94"/>
<wire x1="54.9275" y1="-57.15" x2="54.9275" y2="-49.53" width="0.1524" layer="94"/>
<wire x1="61.9125" y1="-57.15" x2="61.9125" y2="-49.53" width="0.1524" layer="94"/>
<wire x1="53.34" y1="-56.1975" x2="47.625" y2="-56.1975" width="0.4064" layer="94"/>
<wire x1="47.625" y1="-56.1975" x2="46.355" y2="-54.9275" width="0.4064" layer="94"/>
<wire x1="46.355" y1="-54.9275" x2="46.355" y2="-53.34" width="0.4064" layer="94"/>
<wire x1="46.355" y1="-53.34" x2="46.355" y2="-51.7525" width="0.4064" layer="94"/>
<wire x1="46.355" y1="-51.7525" x2="47.625" y2="-50.4825" width="0.4064" layer="94"/>
<wire x1="47.625" y1="-50.4825" x2="53.34" y2="-50.4825" width="0.4064" layer="94"/>
<wire x1="63.5" y1="-50.4825" x2="69.215" y2="-50.4825" width="0.4064" layer="94"/>
<wire x1="69.215" y1="-50.4825" x2="70.485" y2="-51.7525" width="0.4064" layer="94"/>
<wire x1="70.485" y1="-51.7525" x2="70.485" y2="-53.34" width="0.4064" layer="94"/>
<wire x1="70.485" y1="-53.34" x2="70.485" y2="-54.9275" width="0.4064" layer="94"/>
<wire x1="70.485" y1="-54.9275" x2="69.215" y2="-56.1975" width="0.4064" layer="94"/>
<wire x1="69.215" y1="-56.1975" x2="63.5" y2="-56.1975" width="0.4064" layer="94"/>
<circle x="49.2125" y="-53.34" radius="1.7097" width="0.3048" layer="94"/>
<circle x="67.6275" y="-53.34" radius="1.7097" width="0.3048" layer="94"/>
<wire x1="46.355" y1="-53.34" x2="43.18" y2="-53.34" width="0.4064" layer="94"/>
<pin name="P3" x="43.18" y="-53.34" visible="pin" length="short" direction="pas"/>
<text x="55.40375" y="-36.5125" size="1.778" layer="94" rot="R90" align="top-left">F1</text>
<wire x1="70.485" y1="-33.02" x2="73.66" y2="-33.02" width="0.4064" layer="94"/>
<text x="61.595" y="-29.68625" size="1.27" layer="94" rot="R90" align="bottom-right">30A</text>
<wire x1="56.8325" y1="-43.18" x2="59.69" y2="-43.18" width="0.1524" layer="94"/>
<wire x1="57.4675" y1="-43.4975" x2="59.055" y2="-43.4975" width="0.1524" layer="94"/>
<wire x1="59.055" y1="-43.4975" x2="59.055" y2="-42.8625" width="0.1524" layer="94"/>
<wire x1="59.055" y1="-42.8625" x2="57.4675" y2="-42.8625" width="0.1524" layer="94"/>
<wire x1="57.4675" y1="-42.8625" x2="57.4675" y2="-43.4975" width="0.1524" layer="94"/>
<text x="55.40375" y="-46.6725" size="1.778" layer="94" rot="R90" align="top-left">F2</text>
<text x="61.595" y="-39.84625" size="1.27" layer="94" rot="R90" align="bottom-right">30A</text>
<wire x1="56.8325" y1="-53.34" x2="59.69" y2="-53.34" width="0.1524" layer="94"/>
<wire x1="57.4675" y1="-53.6575" x2="59.055" y2="-53.6575" width="0.1524" layer="94"/>
<wire x1="59.055" y1="-53.6575" x2="59.055" y2="-53.0225" width="0.1524" layer="94"/>
<wire x1="59.055" y1="-53.0225" x2="57.4675" y2="-53.0225" width="0.1524" layer="94"/>
<wire x1="57.4675" y1="-53.0225" x2="57.4675" y2="-53.6575" width="0.1524" layer="94"/>
<text x="55.40375" y="-56.8325" size="1.778" layer="94" rot="R90" align="top-left">F3</text>
<text x="61.595" y="-50.00625" size="1.27" layer="94" rot="R90" align="bottom-right">30A</text>
<wire x1="0" y1="0" x2="0" y2="-0.3175" width="0.3048" layer="94"/>
<wire x1="0" y1="-0.3175" x2="0" y2="-0.635" width="0.3048" layer="94"/>
<wire x1="0" y1="-0.635" x2="0" y2="-0.9525" width="0.3048" layer="94"/>
<wire x1="0" y1="-0.9525" x2="0" y2="-1.27" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="0" y2="-85.725" width="0.3048" layer="94"/>
<wire x1="93.345" y1="0" x2="1.27" y2="0" width="0.3048" layer="94"/>
<wire x1="1.27" y1="0" x2="0.9525" y2="0" width="0.3048" layer="94"/>
<wire x1="0.9525" y1="0" x2="0.635" y2="0" width="0.3048" layer="94"/>
<wire x1="0.635" y1="0" x2="0.3175" y2="0" width="0.3048" layer="94"/>
<wire x1="0.3175" y1="0" x2="0" y2="0" width="0.3048" layer="94"/>
<wire x1="0" y1="-1.27" x2="1.27" y2="0" width="0.3048" layer="94"/>
<wire x1="0" y1="-0.9525" x2="0.9525" y2="0" width="0.3048" layer="94"/>
<wire x1="0" y1="-0.635" x2="0.635" y2="0" width="0.3048" layer="94"/>
<wire x1="0" y1="-0.3175" x2="0.3175" y2="0" width="0.3048" layer="94"/>
<pin name="GLOW2" x="86.36" y="-48.26" visible="pin" length="point" direction="pas" rot="R180"/>
<wire x1="85.4075" y1="-45.72" x2="85.4075" y2="-50.8" width="0.1524" layer="94"/>
<wire x1="85.4075" y1="-50.8" x2="87.3125" y2="-50.8" width="0.1524" layer="94"/>
<wire x1="87.3125" y1="-50.8" x2="87.3125" y2="-45.72" width="0.1524" layer="94"/>
<wire x1="87.3125" y1="-45.72" x2="85.4075" y2="-45.72" width="0.1524" layer="94"/>
<wire x1="85.4075" y1="-45.72" x2="84.7725" y2="-45.72" width="0.1524" layer="94"/>
<wire x1="84.7725" y1="-45.72" x2="84.7725" y2="-50.8" width="0.1524" layer="94"/>
<wire x1="84.7725" y1="-50.8" x2="85.4075" y2="-50.8" width="0.1524" layer="94"/>
<wire x1="87.3125" y1="-50.8" x2="87.9475" y2="-50.8" width="0.1524" layer="94"/>
<wire x1="87.9475" y1="-50.8" x2="87.9475" y2="-45.72" width="0.1524" layer="94"/>
<wire x1="87.9475" y1="-45.72" x2="87.3125" y2="-45.72" width="0.1524" layer="94"/>
<circle x="86.36" y="-48.26" radius="0.635" width="0.1524" layer="94"/>
<wire x1="73.66" y1="-43.18" x2="79.375" y2="-42.164" width="0.4064" layer="94"/>
<wire x1="73.66" y1="-43.18" x2="70.485" y2="-43.18" width="0.4064" layer="94"/>
<circle x="73.66" y="-43.18" radius="0.254" width="0.508" layer="94"/>
<text x="76.2" y="-45.72" size="1.778" layer="94" align="bottom-center">RE1</text>
<wire x1="73.66" y1="-53.34" x2="79.375" y2="-52.324" width="0.4064" layer="94"/>
<wire x1="73.66" y1="-53.34" x2="70.485" y2="-53.34" width="0.4064" layer="94"/>
<circle x="73.66" y="-53.34" radius="0.254" width="0.508" layer="94"/>
<text x="76.2" y="-55.88" size="1.778" layer="94" align="bottom-center">RE2</text>
<pin name="GLOW1" x="86.36" y="-38.1" visible="pin" length="point" direction="pas" rot="R180"/>
<wire x1="85.4075" y1="-35.56" x2="85.4075" y2="-40.64" width="0.1524" layer="94"/>
<wire x1="85.4075" y1="-40.64" x2="87.3125" y2="-40.64" width="0.1524" layer="94"/>
<wire x1="87.3125" y1="-40.64" x2="87.3125" y2="-35.56" width="0.1524" layer="94"/>
<wire x1="87.3125" y1="-35.56" x2="85.4075" y2="-35.56" width="0.1524" layer="94"/>
<wire x1="85.4075" y1="-35.56" x2="84.7725" y2="-35.56" width="0.1524" layer="94"/>
<wire x1="84.7725" y1="-35.56" x2="84.7725" y2="-40.64" width="0.1524" layer="94"/>
<wire x1="84.7725" y1="-40.64" x2="85.4075" y2="-40.64" width="0.1524" layer="94"/>
<wire x1="87.3125" y1="-40.64" x2="87.9475" y2="-40.64" width="0.1524" layer="94"/>
<wire x1="87.9475" y1="-40.64" x2="87.9475" y2="-35.56" width="0.1524" layer="94"/>
<wire x1="87.9475" y1="-35.56" x2="87.3125" y2="-35.56" width="0.1524" layer="94"/>
<circle x="86.36" y="-38.1" radius="0.635" width="0.1524" layer="94"/>
<wire x1="78.74" y1="-43.18" x2="86.36" y2="-43.18" width="0.4064" layer="94"/>
<wire x1="78.74" y1="-53.34" x2="86.36" y2="-53.34" width="0.4064" layer="94"/>
<wire x1="66.04" y1="-73.66" x2="71.755" y2="-72.644" width="0.254" layer="94"/>
<wire x1="66.04" y1="-73.66" x2="63.5" y2="-73.66" width="0.1524" layer="94"/>
<circle x="66.04" y="-73.66" radius="0.254" width="0.508" layer="94"/>
<text x="68.58" y="-76.2" size="1.778" layer="94" align="bottom-center">RE3</text>
<wire x1="71.12" y1="-73.66" x2="74.295" y2="-73.66" width="0.1524" layer="94"/>
<wire x1="52.07" y1="-76.2" x2="52.07" y2="-71.12" width="0.254" layer="94"/>
<wire x1="52.07" y1="-71.12" x2="54.61" y2="-71.12" width="0.254" layer="94"/>
<wire x1="54.61" y1="-71.12" x2="54.61" y2="-76.2" width="0.254" layer="94"/>
<wire x1="54.61" y1="-76.2" x2="52.07" y2="-76.2" width="0.254" layer="94"/>
<wire x1="46.99" y1="-76.2" x2="46.99" y2="-71.12" width="0.254" layer="94"/>
<wire x1="46.99" y1="-71.12" x2="49.53" y2="-71.12" width="0.254" layer="94"/>
<wire x1="49.53" y1="-71.12" x2="49.53" y2="-76.2" width="0.254" layer="94"/>
<wire x1="49.53" y1="-76.2" x2="46.99" y2="-76.2" width="0.254" layer="94"/>
<wire x1="74.295" y1="-78.74" x2="53.34" y2="-78.74" width="0.1524" layer="94"/>
<wire x1="53.34" y1="-78.74" x2="53.34" y2="-68.58" width="0.1524" layer="94"/>
<wire x1="53.34" y1="-68.58" x2="63.5" y2="-68.58" width="0.1524" layer="94"/>
<wire x1="63.5" y1="-68.58" x2="63.5" y2="-73.66" width="0.1524" layer="94"/>
<circle x="53.34" y="-78.74" radius="0.254" width="0.508" layer="94"/>
<wire x1="53.34" y1="-78.74" x2="48.26" y2="-78.74" width="0.1524" layer="94"/>
<wire x1="48.26" y1="-78.74" x2="48.26" y2="-69.215" width="0.1524" layer="94"/>
<text x="48.26" y="-80.01" size="1.778" layer="94" align="top-center">F4</text>
<text x="53.34" y="-80.01" size="1.778" layer="94" align="top-center">F5</text>
<wire x1="68.58" y1="-66.04" x2="68.58" y2="-68.58" width="0.1524" layer="94"/>
<wire x1="67.31" y1="-68.58" x2="68.58" y2="-68.58" width="0.4064" layer="94"/>
<wire x1="68.58" y1="-68.58" x2="69.85" y2="-68.58" width="0.4064" layer="94"/>
<wire x1="68.58" y1="-66.04" x2="72.39" y2="-66.04" width="0.1524" layer="94"/>
<wire x1="72.39" y1="-66.04" x2="72.39" y2="-68.58" width="0.1524" layer="94"/>
<wire x1="72.39" y1="-68.58" x2="74.295" y2="-68.58" width="0.1524" layer="94"/>
<wire x1="72.39" y1="-66.04" x2="72.39" y2="-63.5" width="0.1524" layer="94"/>
<wire x1="72.39" y1="-63.5" x2="74.295" y2="-63.5" width="0.1524" layer="94"/>
<circle x="72.39" y="-66.04" radius="0.254" width="0.508" layer="94"/>
<text x="48.26" y="-66.675" size="1.778" layer="94" align="center">+</text>
<circle x="48.26" y="-68.58" radius="0.635" width="0.254" layer="94"/>
<text x="45.72" y="-73.66" size="1.778" layer="94" align="center-right">10A</text>
<text x="55.88" y="-73.66" size="1.778" layer="94" align="center-left">10A</text>
<wire x1="86.36" y1="-53.34" x2="86.36" y2="-52.07" width="0.4064" layer="94"/>
<wire x1="86.36" y1="-43.18" x2="86.36" y2="-41.91" width="0.4064" layer="94"/>
<rectangle x1="86.1568" y1="-53.34" x2="86.5632" y2="-50.8" layer="94"/>
<rectangle x1="86.1568" y1="-43.18" x2="86.5632" y2="-40.64" layer="94"/>
<text x="42.545" y="-62.23" size="1.778" layer="97" align="bottom-center">Napájení desky
a CANarmatur</text>
<wire x1="46.99" y1="-66.04" x2="43.815" y2="-62.865" width="0.1524" layer="97"/>
<wire x1="46.99" y1="-66.04" x2="46.99" y2="-65.405" width="0.1524" layer="97"/>
<wire x1="46.99" y1="-66.04" x2="46.355" y2="-66.04" width="0.1524" layer="97"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="AUTOSTART-DEUTZ" prefix="DPS">
<gates>
<gate name="G$1" symbol="AUTOSTART-DEUTZ-KONEKTORY" x="0" y="0"/>
</gates>
<devices>
<device name="">
<technologies>
<technology name="">
<attribute name="ABRA" value="VAUV0227"/>
<attribute name="_EXTERNAL_" value=""/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
<attribute name="ABRA_GLOBAL" value="VAUTxxxx"/>
<attribute name="VEHICLE_NAME" value="xxxxxxxxxx"/>
<attribute name="VEHICLE_NUMBER" value="xxxxx"/>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
</classes>
<parts>
<part name="FRAME2" library="VEIT_logotyp" deviceset="DINA3_L-AUTO" device="" value="Autostart DEUTZ"/>
<part name="DPS1" library="AUTOSTART" deviceset="AUTOSTART-DEUTZ" device=""/>
<part name="U$6" library="CMI" deviceset="JISTIC_DUMMY" device="" value="30A"/>
<part name="U$7" library="SUPPLY2" deviceset="+24V" device=""/>
<part name="U$13" library="SUPPLY2" deviceset="GND" device=""/>
<part name="PRUCHODKA2" library="AUTO-schema" deviceset="PRUCHODKA" device=""/>
<part name="U$14" library="DEUTZ" deviceset="FUEL_PUMP" device=""/>
<part name="V31" library="SUPPLY2" deviceset="V--&gt;" device="" value="GLOW1"/>
<part name="V1" library="SUPPLY2" deviceset="V--&gt;" device="" value="GLOW1"/>
<part name="PRUCHODKA4" library="AUTO-schema" deviceset="PRUCHODKA" device=""/>
<part name="PRUCHODKA5" library="AUTO-schema" deviceset="PRUCHODKA" device=""/>
<part name="U$97" library="SUPPLY2" deviceset="+24V@2" device="" value="+24V"/>
<part name="U$18" library="SUPPLY2" deviceset="AKU" device=""/>
<part name="SVORKA6" library="Svorkovnice-3patra" deviceset="3SVORKA-00" device="" value="42"/>
<part name="SVORKA7" library="Svorkovnice-3patra" deviceset="3SVORKA-10" device="" value="41"/>
<part name="SVORKA9" library="Svorkovnice-3patra" deviceset="3SVORKA-00" device="" value="43"/>
<part name="JP46" library="CMI" deviceset="PINHD-1X1" device="" value="X23.2:06"/>
<part name="JP47" library="CMI" deviceset="PINHD-1X1" device="" value="X23.2:07"/>
<part name="JP48" library="CMI" deviceset="PINHD-1X1" device="" value="X23.2:04"/>
<part name="JP49" library="CMI" deviceset="PINHD-1X1" device="" value="X23.2:05"/>
<part name="JP44" library="CMI" deviceset="PINHD-1X1" device="" value="X23.1:26"/>
<part name="JP50" library="CMI" deviceset="PINHD-1X1" device="" value="X23.1:25"/>
<part name="JP90" library="CMI" deviceset="PINHD-1X1" device="" value="X23.1:62"/>
<part name="JP91" library="CMI" deviceset="PINHD-1X1" device="" value="X23.1:61"/>
<part name="JP66" library="CMI" deviceset="PINHD-1X1" device="" value="X23.1:24"/>
<part name="JP68" library="CMI" deviceset="PINHD-1X1" device="" value="X23.1:09"/>
<part name="JP63" library="CMI" deviceset="PINHD-1X1" device="" value="X23.1:33"/>
<part name="JP64" library="CMI" deviceset="PINHD-1X1" device="" value="X23.1:32"/>
<part name="JP69" library="CMI" deviceset="PINHD-1X1" device="" value="X23.1:31"/>
<part name="JP70" library="CMI" deviceset="PINHD-1X1" device="" value="X23.1:58"/>
<part name="JP83" library="CMI" deviceset="PINHD-1X1" device="" value="X23.1:22"/>
<part name="JP84" library="CMI" deviceset="PINHD-1X1" device="" value="X23.1:23"/>
<part name="JP92" library="CMI" deviceset="PINHD-1X1" device="" value="X23.1:42"/>
<part name="JP93" library="CMI" deviceset="PINHD-1X1" device="" value="X23.1:02"/>
<part name="JP74" library="CMI" deviceset="PINHD-1X1" device="" value="X23.1:05"/>
<part name="JP76" library="CMI" deviceset="PINHD-1X1" device="" value="X23.1:57"/>
<part name="JP77" library="CMI" deviceset="PINHD-1X1" device="" value="X23.1:08"/>
<part name="JP71" library="CMI" deviceset="PINHD-1X1" device="" value="X23.1:51"/>
<part name="JP72" library="CMI" deviceset="PINHD-1X1" device="" value="X23.1:50"/>
<part name="JP73" library="CMI" deviceset="PINHD-1X1" device="" value="X23.1:47"/>
<part name="JP75" library="CMI" deviceset="PINHD-1X1" device="" value="X23.1:46"/>
<part name="JP78" library="CMI" deviceset="PINHD-1X1" device="" value="X23.1:49"/>
<part name="JP138" library="CMI" deviceset="PINHD-1X1" device="" value="X23.1:48"/>
<part name="RTERM4" library="resistor" deviceset="R-EU_" device="0207/10" value="120R/0,5W"/>
<part name="JP139" library="CMI" deviceset="PINHD-1X1" device="" value="X23.1:03"/>
<part name="JP140" library="CMI" deviceset="PINHD-1X1" device="" value="X23.1:13"/>
<part name="JP141" library="CMI" deviceset="PINHD-1X1" device="" value="X23.1:40"/>
<part name="JP142" library="CMI" deviceset="PINHD-1X1" device="" value="X23.1:52"/>
<part name="JP144" library="CMI" deviceset="PINHD-1X1" device="" value="X23.1:37"/>
<part name="JP82" library="CMI" deviceset="PINHD-1X1" device="" value="X23.1:55"/>
<part name="JP145" library="CMI" deviceset="PINHD-1X1" device="" value="X23.1:56"/>
<part name="JP147" library="CMI" deviceset="PINHD-1X1" device="" value="X23.1:53"/>
<part name="PRUCHODKA7" library="AUTO-schema" deviceset="PRUCHODKA" device=""/>
<part name="PRUCHODKA8" library="AUTO-schema" deviceset="PRUCHODKA" device=""/>
<part name="U$27" library="DEUTZ" deviceset="AIRFILTER_SW" device=""/>
<part name="U$29" library="DEUTZ" deviceset="COOLANT_LEVEL_SENSOR" device=""/>
<part name="U$42" library="DEUTZ" deviceset="WATER_IN_FUEL_SW" device=""/>
<part name="PRUCHODKA9" library="AUTO-schema" deviceset="PRUCHODKA" device=""/>
<part name="PRUCHODKA10" library="AUTO-schema" deviceset="PRUCHODKA" device=""/>
<part name="PRUCHODKA11" library="AUTO-schema" deviceset="PRUCHODKA" device=""/>
<part name="PRUCHODKA12" library="AUTO-schema" deviceset="PRUCHODKA" device=""/>
<part name="PRUCHODKA13" library="AUTO-schema" deviceset="PRUCHODKA" device=""/>
<part name="U$19" library="SUPPLY2" deviceset="GND" device=""/>
<part name="U$20" library="SUPPLY2" deviceset="GND" device=""/>
<part name="DS1" library="DEUTZ" deviceset="DS" device=""/>
<part name="CAN_ARMATUR2" library="DEUTZ" deviceset="CAN_ARMATUR-SMALL" device=""/>
<part name="U$25" library="SUPPLY2" deviceset="GND" device=""/>
<part name="U$26" library="SUPPLY2" deviceset="GND" device=""/>
<part name="PRUCHODKA14" library="AUTO-schema" deviceset="PRUCHODKA" device=""/>
<part name="U$21" library="DEUTZ" deviceset="TEPLOTNI_CIDLO_KUBOTA" device=""/>
<part name="SVORKA10" library="Svorkovnice-3patra" deviceset="3SVORKA-01" device="" value="8"/>
<part name="TMC2" library="TMC" deviceset="TMC" device=""/>
<part name="SVORKA11" library="Svorkovnice-3patra" deviceset="3SVORKA-11" device="" value="30"/>
<part name="U$28" library="SUPPLY2" deviceset="+24V@2" device="" value="+24V"/>
<part name="U$30" library="SUPPLY2" deviceset="GND" device=""/>
</parts>
<sheets>
<sheet>
<description>Autostart DEUTZ</description>
<plain>
<text x="238.76" y="124.46" size="1.778" layer="94" align="bottom-center">Jistič dieselu</text>
<text x="284.48" y="2.54" size="1.778" layer="94" rot="R180" align="top-left">Neoznačené vodiče 0,75 mm2 (případně 0,5 mm2).
Konektory označené X23.1 a X23.2 jsou konektory kaneláže DEUTZ (černý a zelený).

Vysvětlení prefixů:
AS - vodiče jdoucí na desku Autostartu
CA - vodiče jdoucí na CANarmatur
DS - vodiče jdoucí na Diagnostic Socket (konektor diagnostiky)
MCF - vodiče jdoucí na MCflex
KUB - vodiče jdoucí na motor KUBOTA

Přerušené popsané vodiče jsou propojeny v rámci listu.
Vodiče s vlaječkou pokračují na jiných listech.</text>
<text x="266.7" y="210.82" size="1.27" layer="94">(X23.1.02)</text>
<text x="259.08" y="198.12" size="1.27" layer="94" rot="MR0">(TMC_GND)</text>
<text x="43.18" y="215.9" size="1.778" layer="94" align="center">Twisted pair cable (MELE1621)</text>
<wire x1="59.69" y1="259.08" x2="62.23" y2="259.08" width="0.1524" layer="94"/>
<wire x1="60.96" y1="257.81" x2="60.96" y2="260.35" width="0.1524" layer="94"/>
<wire x1="38.1" y1="248.92" x2="35.56" y2="250.19" width="0.1524" layer="94"/>
<wire x1="35.56" y1="257.81" x2="38.1" y2="259.08" width="0.1524" layer="94"/>
<wire x1="35.56" y1="250.19" x2="35.56" y2="257.81" width="0.1524" layer="94"/>
<text x="30.48" y="254" size="1.778" layer="94" rot="MR270" align="bottom-center">Diagnostic
Lamp</text>
<text x="271.78" y="63.5" size="1.778" layer="94" align="center-left">Pozn.: senzor tlaku paliva na palivovém filtru se zapojuje
          na konektor přímo na motoru (do kabeláže motoru)</text>
<wire x1="59.69" y1="187.96" x2="62.23" y2="187.96" width="0.1524" layer="94"/>
<wire x1="60.96" y1="186.69" x2="60.96" y2="189.23" width="0.1524" layer="94"/>
<text x="30.48" y="223.52" size="1.778" layer="94" rot="MR270" align="bottom-center">Neutral
Switch</text>
<text x="35.56" y="198.12" size="1.778" layer="94" rot="MR270" align="bottom-center">Diagnostic</text>
<text x="35.56" y="185.42" size="1.778" layer="94" rot="MR270" align="bottom-center">U-BAT</text>
<text x="30.48" y="175.26" size="1.778" layer="94" rot="MR270" align="bottom-center">Engine
Stop</text>
<wire x1="38.1" y1="170.18" x2="35.56" y2="171.45" width="0.1524" layer="94"/>
<wire x1="35.56" y1="179.07" x2="38.1" y2="180.34" width="0.1524" layer="94"/>
<wire x1="35.56" y1="171.45" x2="35.56" y2="179.07" width="0.1524" layer="94"/>
<wire x1="368.3" y1="104.14" x2="370.84" y2="102.87" width="0.1524" layer="94"/>
<wire x1="370.84" y1="44.45" x2="368.3" y2="43.18" width="0.1524" layer="94"/>
<wire x1="370.84" y1="102.87" x2="370.84" y2="44.45" width="0.1524" layer="94"/>
<wire x1="269.24" y1="119.38" x2="269.24" y2="38.1" width="0.1524" layer="94" style="longdash"/>
<text x="373.38" y="73.66" size="1.778" layer="94" rot="MR90" align="center">Čidla s dodatečnou kabeláží</text>
<wire x1="59.69" y1="132.08" x2="62.23" y2="132.08" width="0.1524" layer="94"/>
<wire x1="60.96" y1="130.81" x2="60.96" y2="133.35" width="0.1524" layer="94"/>
<wire x1="38.1" y1="132.08" x2="35.56" y2="133.35" width="0.1524" layer="94"/>
<wire x1="35.56" y1="140.97" x2="38.1" y2="142.24" width="0.1524" layer="94"/>
<wire x1="35.56" y1="133.35" x2="35.56" y2="140.97" width="0.1524" layer="94"/>
<text x="30.48" y="137.16" size="1.778" layer="94" rot="MR270" align="bottom-center">Engine Running
Lamp</text>
<wire x1="368.3" y1="170.18" x2="370.84" y2="171.45" width="0.1524" layer="94"/>
<wire x1="370.84" y1="171.45" x2="370.84" y2="199.39" width="0.1524" layer="94"/>
<wire x1="370.84" y1="199.39" x2="368.3" y2="200.66" width="0.1524" layer="94"/>
<text x="373.38" y="185.42" size="1.778" layer="94" rot="MR90" align="bottom-center">ECU Power Supply</text>
<text x="337.82" y="185.42" size="1.778" layer="94" rot="MR180" align="center">4x 2,5mm2</text>
<text x="289.56" y="154.94" size="1.778" layer="94" rot="MR0" align="center-right">2x 10mm2</text>
<text x="327.66" y="154.94" size="1.778" layer="94" rot="MR180" align="center-left">Heater Plugs</text>
<text x="347.98" y="144.78" size="1.778" layer="94" align="top-left">4 žhavící svíčky,
2 oddělené sekce,
každá sekce 0,9 Ohm na zem</text>
<wire x1="346.71" y1="146.05" x2="340.36" y2="152.4" width="0.1524" layer="94"/>
<wire x1="368.3" y1="238.76" x2="370.84" y2="237.49" width="0.1524" layer="94"/>
<wire x1="370.84" y1="229.87" x2="368.3" y2="228.6" width="0.1524" layer="94"/>
<wire x1="370.84" y1="237.49" x2="370.84" y2="229.87" width="0.1524" layer="94"/>
<wire x1="346.71" y1="228.6" x2="344.17" y2="228.6" width="0.1524" layer="94"/>
<wire x1="345.44" y1="229.87" x2="345.44" y2="227.33" width="0.1524" layer="94"/>
<text x="375.92" y="233.68" size="1.778" layer="94" rot="MR90" align="bottom-center">Fuel Pump
Control</text>
<text x="289.56" y="142.24" size="1.778" layer="94" rot="MR0" align="center-right">2x 1,5 mm2</text>
<wire x1="368.3" y1="223.52" x2="370.84" y2="222.25" width="0.1524" layer="94"/>
<wire x1="370.84" y1="209.55" x2="368.3" y2="208.28" width="0.1524" layer="94"/>
<wire x1="370.84" y1="222.25" x2="370.84" y2="209.55" width="0.1524" layer="94"/>
<wire x1="346.71" y1="208.28" x2="344.17" y2="208.28" width="0.1524" layer="94"/>
<wire x1="345.44" y1="209.55" x2="345.44" y2="207.01" width="0.1524" layer="94"/>
<text x="375.92" y="215.9" size="1.778" layer="94" rot="MR90" align="bottom-center">Heater
Control</text>
<text x="322.58" y="226.06" size="1.778" layer="94" rot="MR0" align="center-right">5x 0,75mm2</text>
<wire x1="340.36" y1="152.4" x2="341.63" y2="152.4" width="0.1524" layer="94"/>
<wire x1="340.36" y1="152.4" x2="340.36" y2="151.13" width="0.1524" layer="94"/>
<text x="35.56" y="109.22" size="1.778" layer="94" rot="R90" align="bottom-right">Ignition
to ECU</text>
<text x="35.56" y="114.3" size="1.778" layer="94" rot="R90">Start signal
to ECU</text>
<wire x1="62.23" y1="232.41" x2="60.96" y2="232.41" width="0.1524" layer="94"/>
<text x="33.02" y="238.76" size="1.778" layer="94" rot="MR270" align="bottom-center">Speed Setpoint</text>
<wire x1="60.96" y1="232.41" x2="59.69" y2="232.41" width="0.1524" layer="94"/>
<wire x1="38.1" y1="233.68" x2="35.56" y2="234.95" width="0.1524" layer="94"/>
<wire x1="35.56" y1="242.57" x2="38.1" y2="243.84" width="0.1524" layer="94"/>
<wire x1="35.56" y1="234.95" x2="35.56" y2="242.57" width="0.1524" layer="94"/>
<wire x1="60.96" y1="232.41" x2="60.96" y2="234.95" width="0.1524" layer="94"/>
<text x="15.24" y="243.84" size="1.778" layer="94" rot="MR180" align="bottom-center">Přepínání otáček:
-----------------------
sepnuto    = 1200 rpm
rozepnuto = 2200 rpm</text>
<wire x1="263.525" y1="214.63" x2="262.255" y2="215.265" width="0.1524" layer="94"/>
<wire x1="262.255" y1="215.265" x2="262.89" y2="213.995" width="0.1524" layer="94"/>
<wire x1="262.255" y1="215.265" x2="266.065" y2="211.455" width="0.1524" layer="94"/>
<text x="154.94" y="144.78" size="1.778" layer="94" rot="R180" align="center-right">6x 1,5 mm2</text>
<text x="83.82" y="17.78" size="1.778" layer="94" rot="R180" align="bottom-center">CAN0</text>
<wire x1="78.74" y1="22.86" x2="80.01" y2="20.32" width="0.1524" layer="94"/>
<wire x1="87.63" y1="20.32" x2="88.9" y2="22.86" width="0.1524" layer="94"/>
<wire x1="80.01" y1="20.32" x2="87.63" y2="20.32" width="0.1524" layer="94"/>
<text x="99.06" y="17.78" size="1.778" layer="94" rot="R180" align="bottom-center">CAN0</text>
<wire x1="93.98" y1="22.86" x2="95.25" y2="20.32" width="0.1524" layer="94"/>
<wire x1="102.87" y1="20.32" x2="104.14" y2="22.86" width="0.1524" layer="94"/>
<wire x1="95.25" y1="20.32" x2="102.87" y2="20.32" width="0.1524" layer="94"/>
<text x="109.22" y="5.08" size="1.778" layer="94" align="center">ECU</text>
<wire x1="81.28" y1="7.62" x2="81.28" y2="2.54" width="0.1524" layer="94" style="shortdash"/>
<wire x1="81.28" y1="2.54" x2="137.16" y2="2.54" width="0.1524" layer="94" style="shortdash"/>
<wire x1="137.16" y1="7.62" x2="91.44" y2="7.62" width="0.1524" layer="94" style="shortdash"/>
<text x="119.38" y="17.78" size="1.778" layer="94" rot="R180" align="bottom-center">CAN1</text>
<wire x1="91.44" y1="7.62" x2="81.28" y2="7.62" width="0.1524" layer="94" style="shortdash"/>
<wire x1="114.3" y1="22.86" x2="115.57" y2="20.32" width="0.1524" layer="94"/>
<wire x1="123.19" y1="20.32" x2="124.46" y2="22.86" width="0.1524" layer="94"/>
<wire x1="115.57" y1="20.32" x2="123.19" y2="20.32" width="0.1524" layer="94"/>
<text x="134.62" y="17.78" size="1.778" layer="94" rot="R180" align="bottom-center">CAN1</text>
<wire x1="129.54" y1="22.86" x2="130.81" y2="20.32" width="0.1524" layer="94"/>
<wire x1="138.43" y1="20.32" x2="139.7" y2="22.86" width="0.1524" layer="94"/>
<wire x1="130.81" y1="20.32" x2="138.43" y2="20.32" width="0.1524" layer="94"/>
<wire x1="137.16" y1="2.54" x2="137.16" y2="7.62" width="0.1524" layer="94" style="shortdash"/>
<text x="91.44" y="11.43" size="1.778" layer="94" align="bottom-center">CAN0</text>
<text x="127" y="11.43" size="1.778" layer="94" align="bottom-center">CAN1</text>
<text x="107.95" y="48.26" size="1.778" layer="94" rot="MR270">Nezapojovat</text>
<text x="110.49" y="48.26" size="1.778" layer="94" rot="MR270">Let it open!</text>
<wire x1="110.49" y1="49.53" x2="107.95" y2="52.07" width="0.1524" layer="94"/>
<wire x1="110.49" y1="52.07" x2="107.95" y2="49.53" width="0.1524" layer="94"/>
<text x="40.64" y="12.7" size="1.778" layer="94" align="center">Twisted pair cable (MELE1621)</text>
<text x="104.14" y="201.93" size="1.778" layer="94" rot="R180">Nezapojovat</text>
<text x="104.14" y="204.47" size="1.778" layer="94" rot="R180">Let it open!</text>
<wire x1="97.79" y1="199.39" x2="100.33" y2="196.85" width="0.1524" layer="94"/>
<wire x1="97.79" y1="196.85" x2="100.33" y2="199.39" width="0.1524" layer="94"/>
<wire x1="83.82" y1="15.24" x2="83.82" y2="12.7" width="0.762" layer="94" style="shortdash"/>
<wire x1="83.82" y1="12.7" x2="86.36" y2="10.16" width="0.762" layer="94" style="shortdash"/>
<wire x1="86.36" y1="10.16" x2="91.44" y2="10.16" width="0.762" layer="94" style="shortdash"/>
<wire x1="91.44" y1="10.16" x2="96.52" y2="10.16" width="0.762" layer="94" style="shortdash"/>
<wire x1="96.52" y1="10.16" x2="99.06" y2="12.7" width="0.762" layer="94" style="shortdash"/>
<wire x1="99.06" y1="12.7" x2="99.06" y2="15.24" width="0.762" layer="94" style="shortdash"/>
<wire x1="91.44" y1="10.16" x2="91.44" y2="7.62" width="0.762" layer="94" style="shortdash"/>
<wire x1="119.38" y1="15.24" x2="119.38" y2="12.7" width="0.762" layer="94" style="shortdash"/>
<wire x1="119.38" y1="12.7" x2="121.92" y2="10.16" width="0.762" layer="94" style="shortdash"/>
<wire x1="121.92" y1="10.16" x2="127" y2="10.16" width="0.762" layer="94" style="shortdash"/>
<wire x1="127" y1="10.16" x2="132.08" y2="10.16" width="0.762" layer="94" style="shortdash"/>
<wire x1="132.08" y1="10.16" x2="134.62" y2="12.7" width="0.762" layer="94" style="shortdash"/>
<wire x1="134.62" y1="12.7" x2="134.62" y2="15.24" width="0.762" layer="94" style="shortdash"/>
<wire x1="127" y1="10.16" x2="127" y2="7.62" width="0.762" layer="94" style="shortdash"/>
<text x="129.54" y="88.9" size="1.778" layer="94" align="center">Twisted pair cable (MELE1621)</text>
<text x="129.54" y="76.2" size="1.778" layer="94" align="center">Twisted pair cable (MELE1621)</text>
<text x="45.72" y="157.48" size="1.778" layer="94" rot="MR180" align="center-right">Alternator DEUTZ
(D+ terminal)</text>
<text x="83.82" y="154.94" size="1.778" layer="94" rot="R180" align="bottom-center">1,5 mm2 na alternátor
(mechanická odolnost)</text>
<text x="223.52" y="149.86" size="1.778" layer="94" align="center-right">2,5 mm2</text>
<text x="223.52" y="129.54" size="1.778" layer="94" align="center-right">2,5 mm2</text>
<wire x1="215.9" y1="119.38" x2="215.9" y2="38.1" width="0.1524" layer="94" style="longdash"/>
<wire x1="215.9" y1="119.38" x2="269.24" y2="119.38" width="0.1524" layer="94" style="longdash"/>
<wire x1="215.9" y1="38.1" x2="269.24" y2="38.1" width="0.1524" layer="94" style="longdash"/>
<text x="259.08" y="63.5" size="1.778" layer="94" align="center">Umístěno
v motorovém
prostoru</text>
<wire x1="281.94" y1="165.1" x2="383.54" y2="165.1" width="0.1524" layer="94" style="longdash"/>
<wire x1="281.94" y1="127" x2="281.94" y2="165.1" width="0.1524" layer="94" style="longdash"/>
<wire x1="281.94" y1="127" x2="383.54" y2="127" width="0.1524" layer="94" style="longdash"/>
<wire x1="383.54" y1="127" x2="383.54" y2="165.1" width="0.1524" layer="94" style="longdash"/>
<text x="381" y="129.54" size="1.778" layer="94" align="center-right">Umístěno v motorovém prostoru</text>
<wire x1="22.86" y1="165.1" x2="66.04" y2="165.1" width="0.1524" layer="94" style="longdash"/>
<wire x1="22.86" y1="149.86" x2="66.04" y2="149.86" width="0.1524" layer="94" style="longdash"/>
<wire x1="22.86" y1="149.86" x2="22.86" y2="165.1" width="0.1524" layer="94" style="longdash"/>
<wire x1="66.04" y1="149.86" x2="66.04" y2="165.1" width="0.1524" layer="94" style="longdash"/>
<text x="45.72" y="152.4" size="1.778" layer="94" align="center">Umístěno v motorovém prostoru</text>
<text x="190.5" y="246.38" size="1.778" layer="94" align="bottom-center">TMC</text>
<wire x1="208.28" y1="259.08" x2="208.28" y2="238.76" width="0.1524" layer="94" style="shortdash"/>
<wire x1="208.28" y1="238.76" x2="172.72" y2="238.76" width="0.1524" layer="94" style="shortdash"/>
<wire x1="172.72" y1="238.76" x2="172.72" y2="259.08" width="0.1524" layer="94" style="shortdash"/>
<wire x1="172.72" y1="259.08" x2="208.28" y2="259.08" width="0.1524" layer="94" style="shortdash"/>
<text x="134.62" y="248.92" size="1.27" layer="94" align="top-left">Měření napětí baterie</text>
<wire x1="67.31" y1="234.95" x2="69.85" y2="237.49" width="0.1524" layer="94"/>
<wire x1="67.31" y1="237.49" x2="69.85" y2="234.95" width="0.1524" layer="94"/>
<text x="83.82" y="114.3" size="1.778" layer="98">green</text>
<text x="83.82" y="111.76" size="1.778" layer="98">green</text>
<text x="96.52" y="187.96" size="1.778" layer="98">green</text>
<text x="106.68" y="241.3" size="1.778" layer="98">green</text>
<text x="106.68" y="226.06" size="1.778" layer="98">green</text>
<text x="165.1" y="172.72" size="1.778" layer="98">red</text>
<text x="165.1" y="167.64" size="1.778" layer="98">red</text>
<text x="165.1" y="162.56" size="1.778" layer="98">red</text>
<text x="246.38" y="162.56" size="1.778" layer="98">red</text>
<text x="246.38" y="172.72" size="1.778" layer="98">red</text>
<text x="246.38" y="177.8" size="1.778" layer="98">red</text>
<text x="246.38" y="180.34" size="1.778" layer="98">red</text>
<text x="228.6" y="132.08" size="1.778" layer="98">red</text>
<text x="162.56" y="104.14" size="1.778" layer="98" rot="R90">red</text>
<text x="83.82" y="134.62" size="1.778" layer="98">red</text>
<text x="83.82" y="93.98" size="1.778" layer="98">red</text>
<text x="246.38" y="193.04" size="1.778" layer="98">red</text>
<text x="335.28" y="172.72" size="1.778" layer="98">blue</text>
<text x="335.28" y="180.34" size="1.778" layer="98">blue</text>
<text x="335.28" y="190.5" size="1.778" layer="98">red</text>
<text x="335.28" y="198.12" size="1.778" layer="98">red</text>
<text x="228.6" y="147.32" size="1.778" layer="98">blue</text>
<text x="228.6" y="142.24" size="1.778" layer="98">blue</text>
<text x="228.6" y="137.16" size="1.778" layer="98">red</text>
<text x="83.82" y="139.7" size="1.778" layer="98">brown</text>
<text x="137.16" y="144.78" size="1.778" layer="98">brown</text>
<text x="83.82" y="157.48" size="1.778" layer="98">brown</text>
<text x="116.84" y="121.92" size="1.778" layer="98">brown</text>
<text x="83.82" y="96.52" size="1.778" layer="98">blue</text>
<text x="160.02" y="104.14" size="1.778" layer="98" rot="R90">blue</text>
<text x="127" y="124.46" size="1.778" layer="98" rot="R90">brown</text>
<text x="83.82" y="104.14" size="1.778" layer="98">brown</text>
<text x="96.52" y="182.88" size="1.778" layer="98">green</text>
<text x="106.68" y="251.46" size="1.778" layer="98">brown</text>
<text x="96.52" y="185.42" size="1.778" layer="98">red</text>
<text x="152.4" y="226.06" size="1.778" layer="98" rot="R90" align="center-left">brown</text>
<text x="106.68" y="256.54" size="1.778" layer="98">brown</text>
<text x="198.12" y="233.68" size="1.778" layer="98">brown</text>
<text x="127" y="246.38" size="1.778" layer="98">red</text>
<text x="139.7" y="227.33" size="1.778" layer="98" rot="R90" align="center-left">brown</text>
<text x="198.12" y="236.22" size="1.778" layer="98">brown</text>
<text x="246.38" y="195.58" size="1.778" layer="98">blue</text>
<text x="246.38" y="190.5" size="1.778" layer="98">red</text>
<text x="246.38" y="187.96" size="1.778" layer="98">green</text>
<text x="195.58" y="200.66" size="1.778" layer="98" rot="R90">brown</text>
<text x="198.12" y="200.66" size="1.778" layer="98" rot="R90">brown</text>
<text x="246.38" y="215.9" size="1.778" layer="98">blue</text>
<text x="246.38" y="218.44" size="1.778" layer="98">brown</text>
<text x="246.38" y="220.98" size="1.778" layer="98">brown</text>
<text x="198.12" y="223.52" size="1.778" layer="98">brown</text>
<text x="198.12" y="226.06" size="1.778" layer="98">brown</text>
<text x="198.12" y="228.6" size="1.778" layer="98">brown</text>
<text x="307.34" y="251.46" size="1.778" layer="98">green</text>
<text x="152.4" y="63.5" size="1.778" layer="98">white</text>
<text x="152.4" y="78.74" size="1.778" layer="98">white</text>
<text x="121.92" y="58.42" size="1.778" layer="98" rot="R90">white</text>
<text x="96.52" y="58.42" size="1.778" layer="98" rot="R90">white</text>
<text x="81.28" y="58.42" size="1.778" layer="98" rot="R90">white</text>
<text x="106.68" y="193.04" size="1.778" layer="98">white</text>
<text x="33.02" y="38.1" size="1.778" layer="98" rot="R90">white</text>
<text x="340.36" y="101.6" size="1.778" layer="98">blue</text>
<text x="340.36" y="104.14" size="1.778" layer="98">blue</text>
<text x="335.28" y="81.28" size="1.778" layer="98">red</text>
<text x="335.28" y="50.8" size="1.778" layer="98">green</text>
<text x="271.78" y="50.8" size="1.778" layer="98">green</text>
<text x="271.78" y="81.28" size="1.778" layer="98">red</text>
<text x="271.78" y="76.2" size="1.778" layer="98">blue</text>
<text x="271.78" y="78.74" size="1.778" layer="98">green</text>
<text x="335.28" y="78.74" size="1.778" layer="98">green</text>
<wire x1="346.71" y1="97.79" x2="345.44" y2="97.79" width="0.1524" layer="94"/>
<wire x1="345.44" y1="97.79" x2="344.17" y2="97.79" width="0.1524" layer="94"/>
<wire x1="345.44" y1="97.79" x2="345.44" y2="100.33" width="0.1524" layer="94"/>
<text x="271.78" y="104.14" size="1.778" layer="98">blue</text>
<text x="106.68" y="195.58" size="1.778" layer="98">brown</text>
<text x="35.56" y="38.1" size="1.778" layer="98" rot="R90">brown</text>
<text x="86.36" y="58.42" size="1.778" layer="98" rot="R90">brown</text>
<text x="101.6" y="58.42" size="1.778" layer="98" rot="R90">brown</text>
<text x="116.84" y="58.42" size="1.778" layer="98" rot="R90">brown</text>
<text x="152.4" y="66.04" size="1.778" layer="98">brown</text>
<text x="152.4" y="76.2" size="1.778" layer="98">brown</text>
<text x="83.82" y="101.6" size="1.778" layer="98">green</text>
<text x="83.82" y="99.06" size="1.778" layer="98">green</text>
<text x="335.28" y="48.26" size="1.778" layer="98">green</text>
<text x="271.78" y="48.26" size="1.778" layer="98">green</text>
<text x="60.96" y="236.22" size="1.778" layer="98">black</text>
<text x="271.78" y="101.6" size="1.778" layer="98">green</text>
<text x="335.28" y="83.82" size="1.778" layer="98">green</text>
<text x="312.42" y="220.98" size="1.778" layer="98">brown</text>
<text x="312.42" y="236.22" size="1.778" layer="98">brown</text>
<text x="312.42" y="231.14" size="1.778" layer="98">red</text>
<text x="312.42" y="215.9" size="1.778" layer="98">green</text>
<text x="312.42" y="210.82" size="1.778" layer="98">red</text>
</plain>
<instances>
<instance part="FRAME2" gate="G$1" x="0" y="0"/>
<instance part="FRAME2" gate="G$2" x="287.02" y="0"/>
<instance part="DPS1" gate="G$1" x="132.08" y="210.82"/>
<instance part="U$6" gate="G$1" x="238.76" y="132.08" smashed="yes">
<attribute name="VALUE" x="238.76" y="129.54" size="1.778" layer="94" rot="R180" align="bottom-center"/>
</instance>
<instance part="U$7" gate="+24V" x="246.38" y="132.08" smashed="yes" rot="R270">
<attribute name="VALUE" x="252.73" y="132.08" size="1.778" layer="96" rot="R180" align="center"/>
</instance>
<instance part="U$13" gate="GND" x="248.92" y="147.32" rot="R90"/>
<instance part="PRUCHODKA2" gate="G$1" x="281.94" y="139.7"/>
<instance part="U$14" gate="G$1" x="317.5" y="139.7"/>
<instance part="V31" gate="G$1" x="325.12" y="157.48" rot="R180"/>
<instance part="V1" gate="G$1" x="325.12" y="152.4" rot="R180"/>
<instance part="PRUCHODKA4" gate="G$1" x="281.94" y="152.4"/>
<instance part="PRUCHODKA5" gate="G$1" x="281.94" y="157.48"/>
<instance part="U$97" gate="+24V" x="116.84" y="165.1" rot="MR270"/>
<instance part="U$18" gate="G$1" x="119.38" y="172.72" rot="MR270"/>
<instance part="SVORKA6" gate="G$1" x="309.88" y="81.28" rot="R270"/>
<instance part="SVORKA7" gate="G$1" x="309.88" y="101.6" rot="R270"/>
<instance part="SVORKA9" gate="G$1" x="309.88" y="50.8" rot="R270"/>
<instance part="JP63" gate="G$1" x="50.8" y="251.46" rot="MR0"/>
<instance part="JP64" gate="G$1" x="50.8" y="256.54" rot="MR0"/>
<instance part="JP139" gate="G$1" x="355.6" y="53.34" rot="MR180"/>
<instance part="JP140" gate="G$1" x="355.6" y="45.72" rot="MR180"/>
<instance part="PRUCHODKA7" gate="G$1" x="269.24" y="50.8" rot="R180"/>
<instance part="PRUCHODKA8" gate="G$1" x="269.24" y="48.26" rot="R180"/>
<instance part="JP74" gate="G$1" x="50.8" y="226.06" rot="MR0"/>
<instance part="JP76" gate="G$1" x="50.8" y="185.42" rot="MR0"/>
<instance part="JP77" gate="G$1" x="50.8" y="198.12" rot="MR0"/>
<instance part="JP92" gate="G$1" x="50.8" y="175.26" rot="MR0"/>
<instance part="JP93" gate="G$1" x="355.6" y="101.6" rot="MR180"/>
<instance part="JP141" gate="G$1" x="355.6" y="73.66" rot="MR180"/>
<instance part="JP142" gate="G$1" x="355.6" y="81.28" rot="MR180"/>
<instance part="JP144" gate="G$1" x="355.6" y="88.9" rot="MR180"/>
<instance part="U$27" gate="G$1" x="236.22" y="106.68"/>
<instance part="U$29" gate="G$1" x="236.22" y="78.74"/>
<instance part="U$42" gate="G$1" x="236.22" y="50.8"/>
<instance part="PRUCHODKA9" gate="G$1" x="269.24" y="76.2" rot="R180"/>
<instance part="PRUCHODKA10" gate="G$1" x="269.24" y="78.74" rot="R180"/>
<instance part="PRUCHODKA11" gate="G$1" x="269.24" y="81.28" rot="R180"/>
<instance part="PRUCHODKA12" gate="G$1" x="269.24" y="101.6" rot="R180"/>
<instance part="PRUCHODKA13" gate="G$1" x="269.24" y="104.14" rot="R180"/>
<instance part="JP69" gate="G$1" x="50.8" y="139.7" rot="MR0"/>
<instance part="JP70" gate="G$1" x="50.8" y="134.62" rot="MR0"/>
<instance part="JP46" gate="G$1" x="355.6" y="190.5"/>
<instance part="JP47" gate="G$1" x="355.6" y="198.12"/>
<instance part="JP48" gate="G$1" x="355.6" y="172.72"/>
<instance part="JP49" gate="G$1" x="355.6" y="180.34"/>
<instance part="U$19" gate="GND" x="325.12" y="172.72" rot="R270"/>
<instance part="U$20" gate="GND" x="325.12" y="180.34" rot="R270"/>
<instance part="JP44" gate="G$1" x="355.6" y="231.14" rot="MR180"/>
<instance part="JP50" gate="G$1" x="355.6" y="236.22" rot="MR180"/>
<instance part="JP82" gate="G$1" x="355.6" y="210.82" rot="MR180"/>
<instance part="JP145" gate="G$1" x="355.6" y="220.98" rot="MR180"/>
<instance part="JP147" gate="G$1" x="355.6" y="215.9" rot="MR180"/>
<instance part="JP66" gate="G$1" x="50.8" y="114.3" rot="MR0"/>
<instance part="JP68" gate="G$1" x="50.8" y="109.22" rot="MR0"/>
<instance part="JP83" gate="G$1" x="50.8" y="241.3" rot="MR0"/>
<instance part="JP84" gate="G$1" x="50.8" y="236.22" rot="MR0"/>
<instance part="JP90" gate="G$1" x="86.36" y="33.02" rot="R270"/>
<instance part="JP91" gate="G$1" x="81.28" y="33.02" rot="R270"/>
<instance part="JP71" gate="G$1" x="101.6" y="33.02" rot="R270"/>
<instance part="JP72" gate="G$1" x="96.52" y="33.02" rot="R270"/>
<instance part="JP73" gate="G$1" x="121.92" y="33.02" rot="R270"/>
<instance part="JP75" gate="G$1" x="116.84" y="33.02" rot="R270"/>
<instance part="JP78" gate="G$1" x="137.16" y="33.02" rot="R270"/>
<instance part="JP138" gate="G$1" x="132.08" y="33.02" rot="R270"/>
<instance part="RTERM4" gate="G$1" x="134.62" y="63.5" smashed="yes">
<attribute name="NAME" x="130.81" y="64.9986" size="1.778" layer="95"/>
<attribute name="VALUE" x="128.27" y="60.198" size="1.778" layer="96"/>
</instance>
<instance part="DS1" gate="G$1" x="195.58" y="71.12"/>
<instance part="CAN_ARMATUR2" gate="G$1" x="27.94" y="81.28"/>
<instance part="U$25" gate="GND" x="177.8" y="68.58"/>
<instance part="U$26" gate="GND" x="177.8" y="50.8"/>
<instance part="PRUCHODKA14" gate="G$1" x="66.04" y="157.48" rot="R180"/>
<instance part="U$21" gate="G$1" x="347.98" y="251.46" rot="R90"/>
<instance part="SVORKA10" gate="G$1" x="243.84" y="251.46" rot="R270"/>
<instance part="TMC2" gate="17" x="175.26" y="243.84"/>
<instance part="TMC2" gate="19" x="175.26" y="254"/>
<instance part="TMC2" gate="47" x="175.26" y="251.46"/>
<instance part="SVORKA11" gate="G$1" x="243.84" y="241.3" rot="R270"/>
<instance part="U$28" gate="+24V" x="121.92" y="246.38" rot="MR270"/>
<instance part="U$30" gate="GND" x="256.54" y="195.58" rot="R90"/>
</instances>
<busses>
<bus name="FUELPUMP+,FUELPUMP-">
<segment>
<wire x1="256.54" y1="139.7" x2="307.34" y2="139.7" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="CA-24V,CA-GND,CA-IG,CA-START,CA-ERROR">
<segment>
<wire x1="63.5" y1="40.64" x2="63.5" y2="101.6" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="CAN0-H,CAN0-L,CA-SHIELD">
<segment>
<wire x1="7.62" y1="45.72" x2="7.62" y2="210.82" width="0.762" layer="92"/>
<wire x1="7.62" y1="210.82" x2="10.16" y2="213.36" width="0.762" layer="92"/>
<wire x1="10.16" y1="213.36" x2="73.66" y2="213.36" width="0.762" layer="92"/>
<wire x1="73.66" y1="213.36" x2="76.2" y2="210.82" width="0.762" layer="92"/>
<wire x1="76.2" y1="210.82" x2="76.2" y2="195.58" width="0.762" layer="92"/>
</segment>
<segment>
<wire x1="10.16" y1="10.16" x2="68.58" y2="10.16" width="0.762" layer="92"/>
<wire x1="10.16" y1="10.16" x2="7.62" y2="12.7" width="0.762" layer="92"/>
<wire x1="7.62" y1="12.7" x2="7.62" y2="33.02" width="0.762" layer="92"/>
<wire x1="68.58" y1="10.16" x2="71.12" y2="12.7" width="0.762" layer="92"/>
<wire x1="71.12" y1="12.7" x2="71.12" y2="68.58" width="0.762" layer="92"/>
<wire x1="71.12" y1="68.58" x2="73.66" y2="71.12" width="0.762" layer="92"/>
<wire x1="73.66" y1="71.12" x2="86.36" y2="71.12" width="0.762" layer="92"/>
</segment>
<segment>
<wire x1="96.52" y1="71.12" x2="106.68" y2="71.12" width="0.762" layer="92"/>
<wire x1="106.68" y1="71.12" x2="109.22" y2="73.66" width="0.762" layer="92"/>
<wire x1="109.22" y1="73.66" x2="109.22" y2="83.82" width="0.762" layer="92"/>
<wire x1="109.22" y1="83.82" x2="111.76" y2="86.36" width="0.762" layer="92"/>
<wire x1="111.76" y1="86.36" x2="147.32" y2="86.36" width="0.762" layer="92"/>
<wire x1="147.32" y1="86.36" x2="149.86" y2="83.82" width="0.762" layer="92"/>
<wire x1="149.86" y1="83.82" x2="149.86" y2="76.2" width="0.762" layer="92"/>
</segment>
</bus>
<bus name="CAN1-H,CAN1-L,CAN1-SHIELD">
<segment>
<wire x1="116.84" y1="73.66" x2="147.32" y2="73.66" width="0.762" layer="92"/>
<wire x1="147.32" y1="73.66" x2="149.86" y2="71.12" width="0.762" layer="92"/>
<wire x1="149.86" y1="71.12" x2="149.86" y2="58.42" width="0.762" layer="92"/>
</segment>
</bus>
</busses>
<nets>
<net name="N$3" class="0">
<segment>
<pinref part="DPS1" gate="G$1" pin="K4.1"/>
<wire x1="213.36" y1="132.08" x2="233.68" y2="132.08" width="0.1524" layer="91"/>
</segment>
</net>
<net name="GND" class="0">
<segment>
<pinref part="DPS1" gate="G$1" pin="K4.4"/>
<wire x1="213.36" y1="147.32" x2="243.84" y2="147.32" width="0.1524" layer="91"/>
<pinref part="U$13" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="DPS1" gate="G$1" pin="K3.1"/>
<wire x1="218.44" y1="198.12" x2="218.44" y2="195.58" width="0.1524" layer="91"/>
<wire x1="218.44" y1="195.58" x2="251.46" y2="195.58" width="0.1524" layer="91"/>
<label x="228.6" y="195.58" size="1.27" layer="95"/>
<pinref part="U$30" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="JP48" gate="G$1" pin="1"/>
<wire x1="353.06" y1="172.72" x2="330.2" y2="172.72" width="0.4064" layer="91"/>
<pinref part="U$19" gate="GND" pin="GND"/>
</segment>
<segment>
<pinref part="JP49" gate="G$1" pin="1"/>
<wire x1="353.06" y1="180.34" x2="330.2" y2="180.34" width="0.4064" layer="91"/>
<pinref part="U$20" gate="GND" pin="GND"/>
</segment>
</net>
<net name="FUELPUMP+" class="0">
<segment>
<pinref part="DPS1" gate="G$1" pin="K4.2"/>
<wire x1="213.36" y1="137.16" x2="254" y2="137.16" width="0.1524" layer="91"/>
<wire x1="256.54" y1="139.7" x2="254" y2="137.16" width="0.1524" layer="91"/>
<label x="251.46" y="137.16" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="FUELPUMP-" class="0">
<segment>
<pinref part="DPS1" gate="G$1" pin="K4.3"/>
<wire x1="213.36" y1="142.24" x2="254" y2="142.24" width="0.1524" layer="91"/>
<wire x1="256.54" y1="139.7" x2="254" y2="142.24" width="0.1524" layer="91"/>
<label x="251.46" y="142.24" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="GLOW1" class="0">
<segment>
<pinref part="DPS1" gate="G$1" pin="GLOW1"/>
<label x="228.6" y="172.72" size="1.778" layer="95"/>
<wire x1="218.44" y1="172.72" x2="274.32" y2="172.72" width="0.4064" layer="91"/>
<wire x1="274.32" y1="172.72" x2="274.32" y2="157.48" width="0.4064" layer="91"/>
<wire x1="274.32" y1="157.48" x2="309.88" y2="157.48" width="0.4064" layer="91"/>
</segment>
</net>
<net name="GLOW2" class="0">
<segment>
<pinref part="DPS1" gate="G$1" pin="GLOW2"/>
<label x="228.6" y="162.56" size="1.778" layer="95"/>
<wire x1="218.44" y1="162.56" x2="269.24" y2="162.56" width="0.4064" layer="91"/>
<wire x1="269.24" y1="162.56" x2="269.24" y2="152.4" width="0.4064" layer="91"/>
<wire x1="269.24" y1="152.4" x2="309.88" y2="152.4" width="0.4064" layer="91"/>
</segment>
</net>
<net name="N$1" class="0">
<segment>
<pinref part="DPS1" gate="G$1" pin="L1"/>
<wire x1="205.74" y1="177.8" x2="208.28" y2="180.34" width="0.4064" layer="91"/>
<junction x="205.74" y="177.8"/>
<wire x1="208.28" y1="180.34" x2="314.96" y2="180.34" width="0.4064" layer="91"/>
<wire x1="205.74" y1="177.8" x2="320.04" y2="177.8" width="0.4064" layer="91"/>
<wire x1="314.96" y1="180.34" x2="314.96" y2="198.12" width="0.4064" layer="91"/>
<wire x1="314.96" y1="198.12" x2="353.06" y2="198.12" width="0.4064" layer="91"/>
<pinref part="JP47" gate="G$1" pin="1"/>
<wire x1="320.04" y1="177.8" x2="320.04" y2="190.5" width="0.4064" layer="91"/>
<wire x1="320.04" y1="190.5" x2="353.06" y2="190.5" width="0.4064" layer="91"/>
<pinref part="JP46" gate="G$1" pin="1"/>
</segment>
</net>
<net name="CA-CAN0-L" class="0">
<segment>
<pinref part="DPS1" gate="G$1" pin="K2.7"/>
<wire x1="139.7" y1="198.12" x2="139.7" y2="195.58" width="0.1524" layer="91"/>
<wire x1="76.2" y1="198.12" x2="78.74" y2="195.58" width="0.1524" layer="91"/>
<wire x1="78.74" y1="195.58" x2="139.7" y2="195.58" width="0.1524" layer="91"/>
<label x="81.28" y="195.58" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="7.62" y1="27.94" x2="10.16" y2="30.48" width="0.1524" layer="91"/>
<wire x1="10.16" y1="30.48" x2="30.48" y2="30.48" width="0.1524" layer="91"/>
<label x="12.7" y="30.48" size="1.778" layer="95"/>
<wire x1="30.48" y1="30.48" x2="35.56" y2="35.56" width="0.1524" layer="91"/>
<pinref part="CAN_ARMATUR2" gate="G$1" pin="3"/>
<wire x1="35.56" y1="53.34" x2="35.56" y2="50.8" width="0.1524" layer="91"/>
<wire x1="35.56" y1="50.8" x2="30.48" y2="45.72" width="0.1524" layer="91"/>
<wire x1="30.48" y1="45.72" x2="10.16" y2="45.72" width="0.1524" layer="91"/>
<wire x1="7.62" y1="48.26" x2="10.16" y2="45.72" width="0.1524" layer="91"/>
<label x="12.7" y="45.72" size="1.778" layer="95"/>
<wire x1="35.56" y1="35.56" x2="35.56" y2="50.8" width="0.1524" layer="91"/>
<junction x="35.56" y="50.8"/>
</segment>
<segment>
<pinref part="JP90" gate="G$1" pin="1"/>
<wire x1="86.36" y1="35.56" x2="86.36" y2="68.58" width="0.1524" layer="91"/>
<wire x1="86.36" y1="68.58" x2="83.82" y2="71.12" width="0.1524" layer="91"/>
<label x="86.36" y="40.64" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="CA-START" class="0">
<segment>
<pinref part="DPS1" gate="G$1" pin="K1.5"/>
<wire x1="149.86" y1="149.86" x2="149.86" y2="137.16" width="0.1524" layer="91"/>
<wire x1="149.86" y1="149.86" x2="104.14" y2="149.86" width="0.1524" layer="91"/>
<wire x1="104.14" y1="149.86" x2="104.14" y2="99.06" width="0.1524" layer="91"/>
<wire x1="104.14" y1="99.06" x2="66.04" y2="99.06" width="0.1524" layer="91"/>
<wire x1="66.04" y1="99.06" x2="63.5" y2="96.52" width="0.1524" layer="91"/>
<label x="68.58" y="99.06" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="63.5" y1="43.18" x2="60.96" y2="40.64" width="0.1524" layer="91"/>
<wire x1="60.96" y1="40.64" x2="40.64" y2="40.64" width="0.1524" layer="91"/>
<pinref part="CAN_ARMATUR2" gate="G$1" pin="8"/>
<wire x1="40.64" y1="40.64" x2="40.64" y2="53.34" width="0.1524" layer="91"/>
<label x="58.42" y="40.64" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="CAN0-SHIELD" class="0">
<segment>
<wire x1="76.2" y1="200.66" x2="78.74" y2="198.12" width="0.1524" layer="91"/>
<wire x1="78.74" y1="198.12" x2="99.06" y2="198.12" width="0.1524" layer="91"/>
<label x="81.28" y="198.12" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="7.62" y1="33.02" x2="10.16" y2="35.56" width="0.1524" layer="91"/>
<wire x1="10.16" y1="35.56" x2="30.48" y2="35.56" width="0.1524" layer="91"/>
<wire x1="30.48" y1="35.56" x2="30.48" y2="43.18" width="0.1524" layer="91"/>
<wire x1="7.62" y1="45.72" x2="10.16" y2="43.18" width="0.1524" layer="91"/>
<wire x1="10.16" y1="43.18" x2="30.48" y2="43.18" width="0.1524" layer="91"/>
<label x="12.7" y="43.18" size="1.778" layer="95"/>
<label x="12.7" y="35.56" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="86.36" y1="71.12" x2="88.9" y2="68.58" width="0.1524" layer="91"/>
<wire x1="88.9" y1="68.58" x2="88.9" y2="50.8" width="0.1524" layer="91"/>
<wire x1="88.9" y1="50.8" x2="93.98" y2="50.8" width="0.1524" layer="91"/>
<wire x1="93.98" y1="50.8" x2="93.98" y2="68.58" width="0.1524" layer="91"/>
<wire x1="93.98" y1="68.58" x2="96.52" y2="71.12" width="0.1524" layer="91"/>
<label x="88.9" y="53.34" size="1.778" layer="95" rot="R90"/>
<label x="93.98" y="53.34" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="149.86" y1="76.2" x2="152.4" y2="73.66" width="0.1524" layer="91"/>
<wire x1="152.4" y1="73.66" x2="177.8" y2="73.66" width="0.1524" layer="91"/>
<label x="177.8" y="73.66" size="1.778" layer="95" rot="MR0"/>
<pinref part="U$25" gate="GND" pin="GND"/>
</segment>
</net>
<net name="+24V" class="0">
<segment>
<pinref part="DPS1" gate="G$1" pin="P3"/>
<wire x1="175.26" y1="157.48" x2="172.72" y2="157.48" width="0.4064" layer="91"/>
<wire x1="172.72" y1="157.48" x2="172.72" y2="162.56" width="0.4064" layer="91"/>
<wire x1="172.72" y1="162.56" x2="121.92" y2="162.56" width="0.4064" layer="91"/>
<pinref part="U$97" gate="+24V" pin="+24V"/>
<wire x1="121.92" y1="162.56" x2="119.38" y2="165.1" width="0.4064" layer="91"/>
<pinref part="DPS1" gate="G$1" pin="P2"/>
<wire x1="175.26" y1="167.64" x2="121.92" y2="167.64" width="0.4064" layer="91"/>
<wire x1="121.92" y1="167.64" x2="119.38" y2="165.1" width="0.4064" layer="91"/>
<junction x="119.38" y="165.1"/>
</segment>
<segment>
<pinref part="DPS1" gate="G$1" pin="K2.13"/>
<wire x1="124.46" y1="246.38" x2="142.24" y2="246.38" width="0.1524" layer="91"/>
<wire x1="142.24" y1="246.38" x2="142.24" y2="208.28" width="0.1524" layer="91"/>
<pinref part="U$28" gate="+24V" pin="+24V"/>
</segment>
</net>
<net name="AKU" class="0">
<segment>
<pinref part="DPS1" gate="G$1" pin="P1"/>
<wire x1="175.26" y1="177.8" x2="172.72" y2="177.8" width="0.4064" layer="91"/>
<wire x1="172.72" y1="177.8" x2="172.72" y2="172.72" width="0.4064" layer="91"/>
<wire x1="172.72" y1="172.72" x2="119.38" y2="172.72" width="0.4064" layer="91"/>
<pinref part="U$18" gate="G$1" pin="AKU"/>
</segment>
</net>
<net name="!VENTILATION_ON" class="0">
<segment>
<pinref part="DPS1" gate="G$1" pin="K3.6"/>
<wire x1="205.74" y1="198.12" x2="198.12" y2="198.12" width="0.1524" layer="91"/>
<wire x1="198.12" y1="198.12" x2="198.12" y2="218.44" width="0.1524" layer="91"/>
<wire x1="198.12" y1="218.44" x2="193.04" y2="218.44" width="0.1524" layer="91"/>
<label x="193.04" y="218.44" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="X23.1.53" class="0">
<segment>
<pinref part="JP147" gate="G$1" pin="1"/>
<pinref part="DPS1" gate="G$1" pin="K3.4"/>
<wire x1="210.82" y1="187.96" x2="210.82" y2="198.12" width="0.1524" layer="91"/>
<wire x1="302.26" y1="187.96" x2="210.82" y2="187.96" width="0.1524" layer="91"/>
<wire x1="302.26" y1="187.96" x2="302.26" y2="215.9" width="0.1524" layer="91"/>
<label x="325.12" y="215.9" size="1.778" layer="95"/>
<wire x1="302.26" y1="215.9" x2="353.06" y2="215.9" width="0.1524" layer="91"/>
<label x="228.6" y="187.96" size="1.27" layer="95"/>
</segment>
</net>
<net name="X23.1.58+" class="0">
<segment>
<pinref part="DPS1" gate="G$1" pin="K1.1"/>
<wire x1="121.92" y1="139.7" x2="139.7" y2="139.7" width="0.1524" layer="91"/>
<wire x1="139.7" y1="139.7" x2="139.7" y2="137.16" width="0.1524" layer="91"/>
<wire x1="121.92" y1="139.7" x2="121.92" y2="134.62" width="0.1524" layer="91"/>
<pinref part="JP70" gate="G$1" pin="1"/>
<wire x1="121.92" y1="134.62" x2="53.34" y2="134.62" width="0.1524" layer="91"/>
<label x="68.58" y="134.62" size="1.778" layer="95"/>
</segment>
</net>
<net name="X23.1.25" class="0">
<segment>
<pinref part="DPS1" gate="G$1" pin="K3.8"/>
<wire x1="215.9" y1="208.28" x2="215.9" y2="218.44" width="0.1524" layer="91"/>
<wire x1="215.9" y1="218.44" x2="284.48" y2="218.44" width="0.1524" layer="91"/>
<wire x1="284.48" y1="218.44" x2="284.48" y2="236.22" width="0.1524" layer="91"/>
<pinref part="JP50" gate="G$1" pin="1"/>
<wire x1="353.06" y1="236.22" x2="284.48" y2="236.22" width="0.1524" layer="91"/>
<label x="325.12" y="236.22" size="1.778" layer="95"/>
<label x="228.6" y="218.44" size="1.27" layer="95"/>
</segment>
</net>
<net name="X23.1.55" class="0">
<segment>
<pinref part="DPS1" gate="G$1" pin="K3.3"/>
<wire x1="307.34" y1="210.82" x2="307.34" y2="190.5" width="0.1524" layer="91"/>
<wire x1="307.34" y1="190.5" x2="213.36" y2="190.5" width="0.1524" layer="91"/>
<wire x1="213.36" y1="190.5" x2="213.36" y2="198.12" width="0.1524" layer="91"/>
<pinref part="JP82" gate="G$1" pin="1"/>
<wire x1="353.06" y1="210.82" x2="307.34" y2="210.82" width="0.1524" layer="91"/>
<label x="325.12" y="210.82" size="1.778" layer="95"/>
<label x="228.6" y="190.5" size="1.27" layer="95"/>
</segment>
</net>
<net name="X23.1.56" class="0">
<segment>
<pinref part="DPS1" gate="G$1" pin="K3.9"/>
<wire x1="213.36" y1="208.28" x2="213.36" y2="220.98" width="0.1524" layer="91"/>
<wire x1="213.36" y1="220.98" x2="353.06" y2="220.98" width="0.1524" layer="91"/>
<pinref part="JP145" gate="G$1" pin="1"/>
<label x="325.12" y="220.98" size="1.778" layer="95"/>
<label x="228.6" y="220.98" size="1.27" layer="95"/>
</segment>
</net>
<net name="X23.1.26" class="0">
<segment>
<pinref part="DPS1" gate="G$1" pin="K3.2"/>
<wire x1="289.56" y1="231.14" x2="289.56" y2="193.04" width="0.1524" layer="91"/>
<wire x1="289.56" y1="193.04" x2="215.9" y2="193.04" width="0.1524" layer="91"/>
<wire x1="215.9" y1="193.04" x2="215.9" y2="198.12" width="0.1524" layer="91"/>
<pinref part="JP44" gate="G$1" pin="1"/>
<wire x1="353.06" y1="231.14" x2="289.56" y2="231.14" width="0.1524" layer="91"/>
<label x="325.12" y="231.14" size="1.778" layer="95"/>
<label x="228.6" y="193.04" size="1.27" layer="95"/>
</segment>
</net>
<net name="X23.1.57(U-BAT)" class="0">
<segment>
<pinref part="DPS1" gate="G$1" pin="K2.4"/>
<wire x1="147.32" y1="198.12" x2="147.32" y2="185.42" width="0.1524" layer="91"/>
<wire x1="147.32" y1="185.42" x2="53.34" y2="185.42" width="0.1524" layer="91"/>
<pinref part="JP76" gate="G$1" pin="1"/>
<label x="68.58" y="185.42" size="1.778" layer="95"/>
</segment>
</net>
<net name="X23.1.33(DIAGNOSTIC_LAMP)" class="0">
<segment>
<pinref part="DPS1" gate="G$1" pin="K2.3"/>
<wire x1="149.86" y1="198.12" x2="149.86" y2="195.58" width="0.1524" layer="91"/>
<wire x1="149.86" y1="195.58" x2="165.1" y2="195.58" width="0.1524" layer="91"/>
<wire x1="165.1" y1="195.58" x2="165.1" y2="251.46" width="0.1524" layer="91"/>
<pinref part="JP63" gate="G$1" pin="1"/>
<wire x1="165.1" y1="251.46" x2="53.34" y2="251.46" width="0.1524" layer="91"/>
<label x="68.58" y="251.46" size="1.778" layer="95"/>
</segment>
</net>
<net name="X23.1.22(SPEED_SET_POINT)" class="0">
<segment>
<pinref part="DPS1" gate="G$1" pin="K2.8"/>
<wire x1="53.34" y1="241.3" x2="154.94" y2="241.3" width="0.1524" layer="91"/>
<wire x1="154.94" y1="241.3" x2="154.94" y2="208.28" width="0.1524" layer="91"/>
<label x="68.58" y="241.3" size="1.778" layer="95"/>
<pinref part="JP83" gate="G$1" pin="1"/>
</segment>
</net>
<net name="X23.1.08(DIAGNOSTIC)" class="0">
<segment>
<pinref part="DPS1" gate="G$1" pin="K2.5"/>
<wire x1="144.78" y1="198.12" x2="144.78" y2="187.96" width="0.1524" layer="91"/>
<wire x1="144.78" y1="187.96" x2="66.04" y2="187.96" width="0.1524" layer="91"/>
<wire x1="66.04" y1="187.96" x2="66.04" y2="198.12" width="0.1524" layer="91"/>
<pinref part="JP77" gate="G$1" pin="1"/>
<wire x1="66.04" y1="198.12" x2="53.34" y2="198.12" width="0.1524" layer="91"/>
<label x="68.58" y="187.96" size="1.778" layer="95"/>
</segment>
</net>
<net name="X23.1.42(STOP)" class="0">
<segment>
<pinref part="DPS1" gate="G$1" pin="K2.2"/>
<wire x1="152.4" y1="198.12" x2="152.4" y2="182.88" width="0.1524" layer="91"/>
<wire x1="152.4" y1="182.88" x2="66.04" y2="182.88" width="0.1524" layer="91"/>
<wire x1="66.04" y1="182.88" x2="66.04" y2="175.26" width="0.1524" layer="91"/>
<pinref part="JP92" gate="G$1" pin="1"/>
<wire x1="66.04" y1="175.26" x2="53.34" y2="175.26" width="0.1524" layer="91"/>
<label x="68.58" y="182.88" size="1.778" layer="95"/>
</segment>
</net>
<net name="X23.1.05(NEUTRAL)" class="0">
<segment>
<pinref part="DPS1" gate="G$1" pin="K2.10"/>
<wire x1="149.86" y1="226.06" x2="149.86" y2="208.28" width="0.1524" layer="91"/>
<pinref part="JP74" gate="G$1" pin="1"/>
<wire x1="149.86" y1="226.06" x2="53.34" y2="226.06" width="0.1524" layer="91"/>
<label x="68.58" y="226.06" size="1.778" layer="95"/>
</segment>
</net>
<net name="IG" class="0">
<segment>
<pinref part="DPS1" gate="G$1" pin="K1.13"/>
<wire x1="152.4" y1="111.76" x2="152.4" y2="127" width="0.1524" layer="91"/>
<wire x1="152.4" y1="111.76" x2="63.5" y2="111.76" width="0.1524" layer="91"/>
<wire x1="63.5" y1="111.76" x2="63.5" y2="109.22" width="0.1524" layer="91"/>
<pinref part="JP68" gate="G$1" pin="1"/>
<wire x1="63.5" y1="109.22" x2="53.34" y2="109.22" width="0.1524" layer="91"/>
<label x="68.58" y="111.76" size="1.778" layer="95"/>
</segment>
</net>
<net name="START" class="0">
<segment>
<pinref part="DPS1" gate="G$1" pin="K1.12"/>
<wire x1="149.86" y1="114.3" x2="149.86" y2="127" width="0.1524" layer="91"/>
<pinref part="JP66" gate="G$1" pin="1"/>
<wire x1="149.86" y1="114.3" x2="53.34" y2="114.3" width="0.1524" layer="91"/>
<label x="68.58" y="114.3" size="1.778" layer="95"/>
</segment>
</net>
<net name="!TMC_EMERGENCY" class="0">
<segment>
<pinref part="DPS1" gate="G$1" pin="K3.5"/>
<wire x1="208.28" y1="195.58" x2="208.28" y2="198.12" width="0.1524" layer="91"/>
<wire x1="208.28" y1="195.58" x2="195.58" y2="195.58" width="0.1524" layer="91"/>
<wire x1="195.58" y1="195.58" x2="195.58" y2="215.9" width="0.1524" layer="91"/>
<wire x1="195.58" y1="215.9" x2="193.04" y2="215.9" width="0.1524" layer="91"/>
<label x="193.04" y="215.9" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="!TMC_COOL" class="0">
<segment>
<pinref part="DPS1" gate="G$1" pin="K3.12"/>
<wire x1="193.04" y1="223.52" x2="205.74" y2="223.52" width="0.1524" layer="91"/>
<wire x1="205.74" y1="223.52" x2="205.74" y2="208.28" width="0.1524" layer="91"/>
<label x="193.04" y="223.52" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="X23.1.31" class="0">
<segment>
<pinref part="DPS1" gate="G$1" pin="K1.2"/>
<wire x1="116.84" y1="142.24" x2="142.24" y2="142.24" width="0.1524" layer="91"/>
<wire x1="142.24" y1="142.24" x2="142.24" y2="137.16" width="0.1524" layer="91"/>
<wire x1="116.84" y1="142.24" x2="116.84" y2="139.7" width="0.1524" layer="91"/>
<pinref part="JP69" gate="G$1" pin="1"/>
<wire x1="116.84" y1="139.7" x2="53.34" y2="139.7" width="0.1524" layer="91"/>
<label x="68.58" y="139.7" size="1.778" layer="95"/>
</segment>
</net>
<net name="CA-ERROR" class="0">
<segment>
<pinref part="DPS1" gate="G$1" pin="K2.1"/>
<wire x1="154.94" y1="198.12" x2="154.94" y2="180.34" width="0.1524" layer="91"/>
<wire x1="154.94" y1="180.34" x2="99.06" y2="180.34" width="0.1524" layer="91"/>
<wire x1="99.06" y1="180.34" x2="99.06" y2="104.14" width="0.1524" layer="91"/>
<wire x1="99.06" y1="104.14" x2="66.04" y2="104.14" width="0.1524" layer="91"/>
<label x="68.58" y="104.14" size="1.778" layer="95"/>
<wire x1="63.5" y1="101.6" x2="66.04" y2="104.14" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="63.5" y1="48.26" x2="60.96" y2="45.72" width="0.1524" layer="91"/>
<wire x1="60.96" y1="45.72" x2="45.72" y2="45.72" width="0.1524" layer="91"/>
<pinref part="CAN_ARMATUR2" gate="G$1" pin="16"/>
<wire x1="45.72" y1="45.72" x2="45.72" y2="53.34" width="0.1524" layer="91"/>
<label x="58.42" y="45.72" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="CA-24V" class="0">
<segment>
<pinref part="DPS1" gate="G$1" pin="K1.14"/>
<wire x1="154.94" y1="93.98" x2="154.94" y2="127" width="0.3048" layer="91"/>
<wire x1="154.94" y1="93.98" x2="66.04" y2="93.98" width="0.3048" layer="91"/>
<wire x1="66.04" y1="93.98" x2="63.5" y2="91.44" width="0.3048" layer="91"/>
<label x="68.58" y="93.98" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="63.5" y1="45.72" x2="60.96" y2="43.18" width="0.3048" layer="91"/>
<wire x1="60.96" y1="43.18" x2="43.18" y2="43.18" width="0.3048" layer="91"/>
<pinref part="CAN_ARMATUR2" gate="G$1" pin="14"/>
<wire x1="43.18" y1="43.18" x2="43.18" y2="53.34" width="0.3048" layer="91"/>
<label x="58.42" y="43.18" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="MTEMP" class="0">
<segment>
<pinref part="SVORKA10" gate="G$1" pin="6"/>
<wire x1="228.6" y1="248.92" x2="233.68" y2="248.92" width="0.1524" layer="91"/>
<label x="228.6" y="248.92" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="!ELMOT" class="0">
<segment>
<label x="152.4" y="215.9" size="1.27" layer="95" rot="R90" xref="yes"/>
<pinref part="DPS1" gate="G$1" pin="K2.9"/>
<wire x1="152.4" y1="215.9" x2="152.4" y2="208.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="X23.1.32+(DIAGNOSTIC_LAMP)" class="0">
<segment>
<pinref part="DPS1" gate="G$1" pin="K2.11"/>
<pinref part="JP64" gate="G$1" pin="1"/>
<wire x1="53.34" y1="256.54" x2="162.56" y2="256.54" width="0.1524" layer="91"/>
<wire x1="162.56" y1="256.54" x2="162.56" y2="213.36" width="0.1524" layer="91"/>
<wire x1="162.56" y1="213.36" x2="147.32" y2="213.36" width="0.1524" layer="91"/>
<wire x1="147.32" y1="213.36" x2="147.32" y2="208.28" width="0.1524" layer="91"/>
<label x="68.58" y="256.54" size="1.778" layer="95"/>
</segment>
</net>
<net name="D_ALT" class="0">
<segment>
<label x="55.88" y="157.48" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="DPS1" gate="G$1" pin="K1.4"/>
<wire x1="55.88" y1="157.48" x2="147.32" y2="157.48" width="0.3048" layer="91"/>
<wire x1="147.32" y1="157.48" x2="147.32" y2="137.16" width="0.3048" layer="91"/>
</segment>
</net>
<net name="D+" class="0">
<segment>
<label x="114.3" y="121.92" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="DPS1" gate="G$1" pin="K1.8"/>
<wire x1="114.3" y1="121.92" x2="139.7" y2="121.92" width="0.1524" layer="91"/>
<wire x1="139.7" y1="121.92" x2="139.7" y2="127" width="0.1524" layer="91"/>
</segment>
</net>
<net name="!HORN_ON" class="0">
<segment>
<label x="139.7" y="213.36" size="1.27" layer="95" rot="R90" xref="yes"/>
<pinref part="DPS1" gate="G$1" pin="K2.14"/>
<wire x1="139.7" y1="213.36" x2="139.7" y2="208.28" width="0.1524" layer="91"/>
</segment>
</net>
<net name="VOLNY_VSTUP" class="0">
<segment>
<label x="121.92" y="144.78" size="1.27" layer="95" rot="R180" xref="yes"/>
<pinref part="DPS1" gate="G$1" pin="K1.3"/>
<wire x1="121.92" y1="144.78" x2="144.78" y2="144.78" width="0.1524" layer="91"/>
<wire x1="144.78" y1="144.78" x2="144.78" y2="137.16" width="0.1524" layer="91"/>
</segment>
</net>
<net name="MANUAL_SWITCH_8_PIN" class="0">
<segment>
<pinref part="DPS1" gate="G$1" pin="K3.11"/>
<wire x1="193.04" y1="226.06" x2="208.28" y2="226.06" width="0.1524" layer="91"/>
<wire x1="208.28" y1="226.06" x2="208.28" y2="208.28" width="0.1524" layer="91"/>
<label x="193.04" y="226.06" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="!COOL" class="0">
<segment>
<pinref part="DPS1" gate="G$1" pin="K3.10"/>
<wire x1="193.04" y1="228.6" x2="210.82" y2="228.6" width="0.1524" layer="91"/>
<wire x1="210.82" y1="228.6" x2="210.82" y2="208.28" width="0.1524" layer="91"/>
<label x="193.04" y="228.6" size="1.27" layer="95" rot="R180" xref="yes"/>
</segment>
</net>
<net name="ECU_GND" class="0">
<segment>
<pinref part="SVORKA7" gate="G$1" pin="2"/>
<label x="322.58" y="101.6" size="1.778" layer="95"/>
<pinref part="JP93" gate="G$1" pin="1"/>
<wire x1="317.5" y1="101.6" x2="353.06" y2="101.6" width="0.1524" layer="91"/>
</segment>
</net>
<net name="X23.1.37" class="0">
<segment>
<pinref part="SVORKA6" gate="G$1" pin="3"/>
<wire x1="314.96" y1="83.82" x2="342.9" y2="83.82" width="0.1524" layer="91"/>
<label x="322.58" y="83.82" size="1.778" layer="95"/>
<pinref part="JP144" gate="G$1" pin="1"/>
<wire x1="353.06" y1="88.9" x2="342.9" y2="88.9" width="0.1524" layer="91"/>
<wire x1="342.9" y1="88.9" x2="342.9" y2="83.82" width="0.1524" layer="91"/>
</segment>
</net>
<net name="X23.1.52" class="0">
<segment>
<pinref part="SVORKA6" gate="G$1" pin="2"/>
<wire x1="317.5" y1="81.28" x2="353.06" y2="81.28" width="0.1524" layer="91"/>
<label x="322.58" y="81.28" size="1.778" layer="95"/>
<pinref part="JP142" gate="G$1" pin="1"/>
</segment>
</net>
<net name="X23.1.40" class="0">
<segment>
<pinref part="SVORKA6" gate="G$1" pin="1"/>
<wire x1="320.04" y1="78.74" x2="342.9" y2="78.74" width="0.1524" layer="91"/>
<label x="322.58" y="78.74" size="1.778" layer="95"/>
<wire x1="342.9" y1="78.74" x2="342.9" y2="73.66" width="0.1524" layer="91"/>
<pinref part="JP141" gate="G$1" pin="1"/>
<wire x1="342.9" y1="73.66" x2="353.06" y2="73.66" width="0.1524" layer="91"/>
</segment>
</net>
<net name="X23.1.13" class="0">
<segment>
<label x="322.58" y="48.26" size="1.778" layer="95"/>
<pinref part="SVORKA9" gate="G$1" pin="1"/>
<wire x1="342.9" y1="48.26" x2="320.04" y2="48.26" width="0.1524" layer="91"/>
<wire x1="342.9" y1="48.26" x2="342.9" y2="45.72" width="0.1524" layer="91"/>
<pinref part="JP140" gate="G$1" pin="1"/>
<wire x1="342.9" y1="45.72" x2="353.06" y2="45.72" width="0.1524" layer="91"/>
</segment>
</net>
<net name="X23.1.03" class="0">
<segment>
<label x="322.58" y="50.8" size="1.778" layer="95"/>
<pinref part="SVORKA9" gate="G$1" pin="2"/>
<wire x1="317.5" y1="50.8" x2="342.9" y2="50.8" width="0.1524" layer="91"/>
<wire x1="342.9" y1="50.8" x2="342.9" y2="53.34" width="0.1524" layer="91"/>
<pinref part="JP139" gate="G$1" pin="1"/>
<wire x1="342.9" y1="53.34" x2="353.06" y2="53.34" width="0.1524" layer="91"/>
</segment>
</net>
<net name="WATERIN-GND" class="0">
<segment>
<pinref part="SVORKA9" gate="G$1" pin="5"/>
<wire x1="254" y1="50.8" x2="302.26" y2="50.8" width="0.1524" layer="91"/>
<pinref part="U$42" gate="G$1" pin="P$1"/>
<label x="297.18" y="50.8" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="WATERIN-SW" class="0">
<segment>
<pinref part="SVORKA9" gate="G$1" pin="6"/>
<wire x1="299.72" y1="48.26" x2="254" y2="48.26" width="0.1524" layer="91"/>
<pinref part="U$42" gate="G$1" pin="P$2"/>
<label x="297.18" y="48.26" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="COOLLEV-SUP" class="0">
<segment>
<pinref part="SVORKA6" gate="G$1" pin="5"/>
<pinref part="U$29" gate="G$1" pin="P$3"/>
<wire x1="302.26" y1="81.28" x2="256.54" y2="81.28" width="0.1524" layer="91"/>
<wire x1="256.54" y1="81.28" x2="254" y2="83.82" width="0.1524" layer="91"/>
<label x="297.18" y="81.28" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="COOLLEV-SIG" class="0">
<segment>
<pinref part="SVORKA6" gate="G$1" pin="6"/>
<pinref part="U$29" gate="G$1" pin="P$2"/>
<wire x1="299.72" y1="78.74" x2="254" y2="78.74" width="0.1524" layer="91"/>
<label x="297.18" y="78.74" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="COOLLEV-GND" class="0">
<segment>
<pinref part="U$29" gate="G$1" pin="P$1"/>
<pinref part="SVORKA7" gate="G$1" pin="5"/>
<wire x1="254" y1="73.66" x2="256.54" y2="76.2" width="0.1524" layer="91"/>
<wire x1="256.54" y1="76.2" x2="279.4" y2="76.2" width="0.1524" layer="91"/>
<wire x1="279.4" y1="76.2" x2="279.4" y2="101.6" width="0.1524" layer="91"/>
<wire x1="279.4" y1="101.6" x2="302.26" y2="101.6" width="0.1524" layer="91"/>
<label x="297.18" y="101.6" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="AIRFIL-SW" class="0">
<segment>
<pinref part="SVORKA6" gate="G$1" pin="4"/>
<wire x1="276.86" y1="101.6" x2="276.86" y2="83.82" width="0.1524" layer="91"/>
<wire x1="276.86" y1="83.82" x2="304.8" y2="83.82" width="0.1524" layer="91"/>
<pinref part="U$27" gate="G$1" pin="P$1"/>
<wire x1="276.86" y1="101.6" x2="254" y2="101.6" width="0.1524" layer="91"/>
<label x="297.18" y="83.82" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="AIRFIL-GND" class="0">
<segment>
<pinref part="SVORKA7" gate="G$1" pin="4"/>
<pinref part="U$27" gate="G$1" pin="P$2"/>
<wire x1="304.8" y1="104.14" x2="254" y2="104.14" width="0.1524" layer="91"/>
<label x="297.18" y="104.14" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="N$4" class="0">
<segment>
<pinref part="JP84" gate="G$1" pin="1"/>
<wire x1="53.34" y1="236.22" x2="68.58" y2="236.22" width="0.1524" layer="91"/>
</segment>
</net>
<net name="AS-ECU_GND" class="0">
<segment>
<pinref part="DPS1" gate="G$1" pin="K3.7"/>
<wire x1="218.44" y1="208.28" x2="218.44" y2="215.9" width="0.1524" layer="91"/>
<label x="228.6" y="215.9" size="1.27" layer="95"/>
<wire x1="218.44" y1="215.9" x2="261.62" y2="215.9" width="0.1524" layer="91"/>
<wire x1="261.62" y1="215.9" x2="261.62" y2="121.92" width="0.1524" layer="91"/>
<wire x1="261.62" y1="121.92" x2="345.44" y2="121.92" width="0.1524" layer="91"/>
<pinref part="SVORKA7" gate="G$1" pin="3"/>
<wire x1="314.96" y1="104.14" x2="345.44" y2="104.14" width="0.1524" layer="91"/>
<wire x1="345.44" y1="104.14" x2="345.44" y2="121.92" width="0.1524" layer="91"/>
<label x="322.58" y="104.14" size="1.778" layer="95"/>
</segment>
</net>
<net name="CA-GND" class="0">
<segment>
<pinref part="DPS1" gate="G$1" pin="K1.10"/>
<wire x1="144.78" y1="127" x2="144.78" y2="96.52" width="0.3048" layer="91"/>
<wire x1="144.78" y1="96.52" x2="66.04" y2="96.52" width="0.3048" layer="91"/>
<wire x1="66.04" y1="96.52" x2="63.5" y2="93.98" width="0.3048" layer="91"/>
<label x="68.58" y="96.52" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="63.5" y1="40.64" x2="60.96" y2="38.1" width="0.3048" layer="91"/>
<wire x1="60.96" y1="38.1" x2="38.1" y2="38.1" width="0.3048" layer="91"/>
<pinref part="CAN_ARMATUR2" gate="G$1" pin="5"/>
<wire x1="38.1" y1="38.1" x2="38.1" y2="53.34" width="0.3048" layer="91"/>
<label x="58.42" y="38.1" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="DS-GND" class="0">
<segment>
<pinref part="DPS1" gate="G$1" pin="K1.9"/>
<wire x1="142.24" y1="127" x2="142.24" y2="121.92" width="0.3048" layer="91"/>
<wire x1="142.24" y1="121.92" x2="160.02" y2="121.92" width="0.3048" layer="91"/>
<wire x1="160.02" y1="121.92" x2="160.02" y2="58.42" width="0.3048" layer="91"/>
<pinref part="DS1" gate="G$1" pin="B"/>
<wire x1="182.88" y1="58.42" x2="160.02" y2="58.42" width="0.3048" layer="91"/>
<label x="177.8" y="58.42" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="DS-IG" class="0">
<segment>
<pinref part="DPS1" gate="G$1" pin="K1.7"/>
<wire x1="154.94" y1="139.7" x2="154.94" y2="137.16" width="0.3048" layer="91"/>
<wire x1="154.94" y1="139.7" x2="162.56" y2="139.7" width="0.3048" layer="91"/>
<wire x1="162.56" y1="139.7" x2="162.56" y2="83.82" width="0.3048" layer="91"/>
<pinref part="DS1" gate="G$1" pin="A"/>
<wire x1="162.56" y1="83.82" x2="182.88" y2="83.82" width="0.3048" layer="91"/>
<label x="177.8" y="83.82" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="CA-IG" class="0">
<segment>
<pinref part="DPS1" gate="G$1" pin="K1.6"/>
<wire x1="152.4" y1="152.4" x2="152.4" y2="137.16" width="0.3048" layer="91"/>
<wire x1="152.4" y1="152.4" x2="101.6" y2="152.4" width="0.3048" layer="91"/>
<wire x1="101.6" y1="152.4" x2="101.6" y2="101.6" width="0.3048" layer="91"/>
<wire x1="101.6" y1="101.6" x2="66.04" y2="101.6" width="0.3048" layer="91"/>
<wire x1="66.04" y1="101.6" x2="63.5" y2="99.06" width="0.3048" layer="91"/>
<label x="68.58" y="101.6" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="63.5" y1="50.8" x2="60.96" y2="48.26" width="0.3048" layer="91"/>
<wire x1="60.96" y1="48.26" x2="48.26" y2="48.26" width="0.3048" layer="91"/>
<pinref part="CAN_ARMATUR2" gate="G$1" pin="18"/>
<wire x1="48.26" y1="48.26" x2="48.26" y2="53.34" width="0.3048" layer="91"/>
<label x="58.42" y="48.26" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="CAN1-L" class="0">
<segment>
<wire x1="124.46" y1="73.66" x2="121.92" y2="71.12" width="0.1524" layer="91"/>
<pinref part="JP73" gate="G$1" pin="1"/>
<wire x1="121.92" y1="71.12" x2="121.92" y2="35.56" width="0.1524" layer="91"/>
<label x="121.92" y="40.64" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="DS1" gate="G$1" pin="G"/>
<wire x1="149.86" y1="66.04" x2="152.4" y2="63.5" width="0.1524" layer="91"/>
<wire x1="152.4" y1="63.5" x2="182.88" y2="63.5" width="0.1524" layer="91"/>
<label x="177.8" y="63.5" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="CAN1-H" class="0">
<segment>
<pinref part="JP75" gate="G$1" pin="1"/>
<wire x1="119.38" y1="73.66" x2="116.84" y2="71.12" width="0.1524" layer="91"/>
<wire x1="116.84" y1="71.12" x2="116.84" y2="35.56" width="0.1524" layer="91"/>
<label x="116.84" y="40.64" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="DS1" gate="G$1" pin="H"/>
<wire x1="149.86" y1="68.58" x2="152.4" y2="66.04" width="0.1524" layer="91"/>
<wire x1="152.4" y1="66.04" x2="182.88" y2="66.04" width="0.1524" layer="91"/>
<label x="177.8" y="66.04" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="CAN1-SHIELD" class="0">
<segment>
<wire x1="116.84" y1="73.66" x2="114.3" y2="71.12" width="0.1524" layer="91"/>
<wire x1="114.3" y1="71.12" x2="114.3" y2="50.8" width="0.1524" layer="91"/>
<label x="114.3" y="53.34" size="1.778" layer="95" rot="R90"/>
<wire x1="114.3" y1="50.8" x2="109.22" y2="50.8" width="0.1524" layer="91"/>
</segment>
<segment>
<wire x1="152.4" y1="55.88" x2="177.8" y2="55.88" width="0.1524" layer="91"/>
<label x="177.8" y="55.88" size="1.778" layer="95" rot="MR0"/>
<pinref part="U$26" gate="GND" pin="GND"/>
<wire x1="152.4" y1="55.88" x2="149.86" y2="58.42" width="0.1524" layer="91"/>
</segment>
</net>
<net name="CAN1-RTH" class="0">
<segment>
<pinref part="JP138" gate="G$1" pin="1"/>
<wire x1="132.08" y1="35.56" x2="132.08" y2="55.88" width="0.1524" layer="91"/>
<label x="132.08" y="40.64" size="1.778" layer="95" rot="R90"/>
<wire x1="132.08" y1="55.88" x2="127" y2="55.88" width="0.1524" layer="91"/>
<wire x1="127" y1="55.88" x2="127" y2="63.5" width="0.1524" layer="91"/>
<wire x1="127" y1="63.5" x2="129.54" y2="63.5" width="0.1524" layer="91"/>
<pinref part="RTERM4" gate="G$1" pin="1"/>
</segment>
</net>
<net name="CAN1-RTL" class="0">
<segment>
<pinref part="JP78" gate="G$1" pin="1"/>
<wire x1="137.16" y1="35.56" x2="137.16" y2="55.88" width="0.1524" layer="91"/>
<label x="137.16" y="40.64" size="1.778" layer="95" rot="R90"/>
<wire x1="139.7" y1="63.5" x2="142.24" y2="63.5" width="0.1524" layer="91"/>
<wire x1="142.24" y1="63.5" x2="142.24" y2="55.88" width="0.1524" layer="91"/>
<wire x1="142.24" y1="55.88" x2="137.16" y2="55.88" width="0.1524" layer="91"/>
<pinref part="RTERM4" gate="G$1" pin="2"/>
</segment>
</net>
<net name="CAN0-H" class="0">
<segment>
<wire x1="99.06" y1="71.12" x2="96.52" y2="68.58" width="0.1524" layer="91"/>
<pinref part="JP72" gate="G$1" pin="1"/>
<wire x1="96.52" y1="68.58" x2="96.52" y2="35.56" width="0.1524" layer="91"/>
<label x="96.52" y="40.64" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<pinref part="DS1" gate="G$1" pin="M"/>
<wire x1="149.86" y1="81.28" x2="152.4" y2="78.74" width="0.1524" layer="91"/>
<wire x1="152.4" y1="78.74" x2="182.88" y2="78.74" width="0.1524" layer="91"/>
<label x="177.8" y="78.74" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="CAN0-L" class="0">
<segment>
<wire x1="104.14" y1="71.12" x2="101.6" y2="68.58" width="0.1524" layer="91"/>
<pinref part="JP71" gate="G$1" pin="1"/>
<wire x1="101.6" y1="68.58" x2="101.6" y2="35.56" width="0.1524" layer="91"/>
<label x="101.6" y="40.64" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="149.86" y1="78.74" x2="152.4" y2="76.2" width="0.1524" layer="91"/>
<pinref part="DS1" gate="G$1" pin="F"/>
<wire x1="152.4" y1="76.2" x2="182.88" y2="76.2" width="0.1524" layer="91"/>
<label x="177.8" y="76.2" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="CA-CAN0-H" class="0">
<segment>
<wire x1="78.74" y1="71.12" x2="81.28" y2="68.58" width="0.1524" layer="91"/>
<pinref part="JP91" gate="G$1" pin="1"/>
<wire x1="81.28" y1="68.58" x2="81.28" y2="35.56" width="0.1524" layer="91"/>
<label x="81.28" y="40.64" size="1.778" layer="95" rot="R90"/>
</segment>
<segment>
<wire x1="76.2" y1="195.58" x2="78.74" y2="193.04" width="0.1524" layer="91"/>
<pinref part="DPS1" gate="G$1" pin="K2.6"/>
<wire x1="142.24" y1="198.12" x2="142.24" y2="193.04" width="0.1524" layer="91"/>
<wire x1="78.74" y1="193.04" x2="142.24" y2="193.04" width="0.1524" layer="91"/>
<label x="81.28" y="193.04" size="1.778" layer="95"/>
</segment>
<segment>
<wire x1="7.62" y1="30.48" x2="10.16" y2="33.02" width="0.1524" layer="91"/>
<wire x1="10.16" y1="33.02" x2="30.48" y2="33.02" width="0.1524" layer="91"/>
<label x="12.7" y="33.02" size="1.778" layer="95"/>
<wire x1="30.48" y1="33.02" x2="33.02" y2="35.56" width="0.1524" layer="91"/>
<wire x1="7.62" y1="50.8" x2="10.16" y2="48.26" width="0.1524" layer="91"/>
<pinref part="CAN_ARMATUR2" gate="G$1" pin="1"/>
<wire x1="33.02" y1="53.34" x2="33.02" y2="50.8" width="0.1524" layer="91"/>
<wire x1="33.02" y1="50.8" x2="30.48" y2="48.26" width="0.1524" layer="91"/>
<wire x1="30.48" y1="48.26" x2="10.16" y2="48.26" width="0.1524" layer="91"/>
<label x="12.7" y="48.26" size="1.778" layer="95"/>
<wire x1="33.02" y1="35.56" x2="33.02" y2="50.8" width="0.1524" layer="91"/>
<junction x="33.02" y="50.8"/>
</segment>
</net>
<net name="AS-MOTOR" class="0">
<segment>
<pinref part="DPS1" gate="G$1" pin="K1.11"/>
<wire x1="127" y1="116.84" x2="147.32" y2="116.84" width="0.1524" layer="91"/>
<wire x1="147.32" y1="116.84" x2="147.32" y2="127" width="0.1524" layer="91"/>
<label x="142.24" y="116.84" size="1.778" layer="95" rot="MR0"/>
<wire x1="127" y1="116.84" x2="127" y2="236.22" width="0.1524" layer="91"/>
<wire x1="127" y1="236.22" x2="213.36" y2="236.22" width="0.1524" layer="91"/>
<pinref part="SVORKA11" gate="G$1" pin="5"/>
<label x="231.14" y="241.3" size="1.778" layer="95" rot="MR0"/>
<wire x1="213.36" y1="241.3" x2="236.22" y2="241.3" width="0.1524" layer="91"/>
<wire x1="213.36" y1="236.22" x2="213.36" y2="241.3" width="0.1524" layer="91"/>
</segment>
</net>
<net name="ALT" class="0">
<segment>
<wire x1="248.92" y1="254" x2="274.32" y2="254" width="0.1524" layer="91"/>
<label x="256.54" y="254" size="1.778" layer="95"/>
<pinref part="SVORKA10" gate="G$1" pin="3"/>
<label x="274.32" y="254" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
<net name="KUB-MTEMP" class="0">
<segment>
<wire x1="251.46" y1="251.46" x2="335.28" y2="251.46" width="0.1524" layer="91"/>
<pinref part="SVORKA10" gate="G$1" pin="2"/>
<label x="256.54" y="251.46" size="1.778" layer="95"/>
<pinref part="U$21" gate="G$1" pin="P$1"/>
</segment>
</net>
<net name="AS-MTEMP" class="0">
<segment>
<pinref part="DPS1" gate="G$1" pin="K2.12"/>
<wire x1="144.78" y1="208.28" x2="144.78" y2="233.68" width="0.1524" layer="91"/>
<wire x1="144.78" y1="233.68" x2="271.78" y2="233.68" width="0.1524" layer="91"/>
<wire x1="271.78" y1="233.68" x2="271.78" y2="248.92" width="0.1524" layer="91"/>
<label x="256.54" y="248.92" size="1.778" layer="95"/>
<wire x1="271.78" y1="248.92" x2="254" y2="248.92" width="0.1524" layer="91"/>
<pinref part="SVORKA10" gate="G$1" pin="1"/>
<label x="144.78" y="213.36" size="1.778" layer="95" rot="R90"/>
</segment>
</net>
<net name="TMC-ALT" class="0">
<segment>
<pinref part="TMC2" gate="19" pin="ALT"/>
<pinref part="SVORKA10" gate="G$1" pin="4"/>
<wire x1="208.28" y1="254" x2="238.76" y2="254" width="0.1524" layer="91"/>
<label x="231.14" y="254" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="TMC-MTEMP" class="0">
<segment>
<pinref part="TMC2" gate="47" pin="MTEMP"/>
<pinref part="SVORKA10" gate="G$1" pin="5"/>
<wire x1="208.28" y1="251.46" x2="236.22" y2="251.46" width="0.1524" layer="91"/>
<label x="231.14" y="251.46" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="TMC-MOTOR" class="0">
<segment>
<pinref part="SVORKA11" gate="G$1" pin="4"/>
<pinref part="TMC2" gate="17" pin="MOTOR"/>
<wire x1="238.76" y1="243.84" x2="208.28" y2="243.84" width="0.1524" layer="91"/>
<label x="231.14" y="243.84" size="1.778" layer="95" rot="MR0"/>
</segment>
</net>
<net name="MOTOR" class="0">
<segment>
<pinref part="SVORKA11" gate="G$1" pin="3"/>
<wire x1="248.92" y1="243.84" x2="256.54" y2="243.84" width="0.1524" layer="91"/>
<label x="256.54" y="243.84" size="1.27" layer="95" xref="yes"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
</compatibility>
</eagle>
